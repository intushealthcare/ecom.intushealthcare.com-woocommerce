<?php

// Load WP
require_once( __DIR__ . '/wp/wp-load.php' );

if ( function_exists( 'wp_cache_clean_cache' ) ) {
    // Check Version
    global $wp_version;
    $gt_4_6 = version_compare( $wp_version, '4.6.0', '>=' );

    // Get Blogs - public is a site that is not set to hide from search engines.
    $args  = [
        'public' => 1
    ];
    $blogs = $gt_4_6 ? get_sites( $args ) : @wp_get_sites( $args ); // >= 4.6
    echo "Cleaning WP-Supercache: " . PHP_EOL;
    foreach ( $blogs as $blog ) {
        global $cache_path;
        //The blog domain doesn't seem to include www. So we need to manually append it...
        $wpsc_directory = $cache_path . 'supercache/www.' . $blog->domain;
        echo "Cleaning " . $wpsc_directory. PHP_EOL;
        echo wpsc_delete_files( $wpsc_directory );
        echo prune_super_cache( $wpsc_directory, true );
    }
}


