<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
/**
 * Iconic_WSB_Settings.
 *
 * Any methods related to the plugin settings.
 *
 * @class    Iconic_WSB_Settings
 * @version  1.0.0
 */
class Iconic_WSB_Settings {
	/**
	 * Run.
	 */
	public static function run() {
		add_action( 'init', array( __CLASS__, 'init' ) );
		add_filter( 'woocommerce_json_search_found_products', array( __CLASS__, 'format_ajax_search_results' ) );
	}

	/**
	 * Init.
	 */
	public static function init() {
		global $iconic_wsb_class;

		if ( ! $iconic_wsb_class ) {
			return;
		}

		$iconic_wsb_class->set_settings();
	}

	/**
	 * Add all plugin settings tab
	 *
	 * @param array $settings
	 *
	 * @return array
	 */
	public static function init_tabs( $settings ) {
		$settings['tabs']['order-bump'] = array(
			'id'    => 'order_bump',
			'title' => __( 'Design', 'iconic-wsb' ),
		);

		return $settings;
	}

	/**
	 * Format AJAX Search Results.
	 *
	 * @param array $products Products.
	 * @return array
	 */
	public static function format_ajax_search_results( $products ) {
		foreach ( $products as $product_id => &$product ) {
			$product_object = wc_get_product( $product_id );
			$product        = esc_html( Iconic_WSB_Helpers::get_formatted_name( $product_object ) );
		}
		return $products;
	}
}
