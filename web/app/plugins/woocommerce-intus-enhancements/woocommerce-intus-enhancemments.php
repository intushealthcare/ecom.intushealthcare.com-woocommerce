<?php

namespace WooCommerce_Intus_Enhancements;


/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://sweet-apple.co.uk
 * @since             0.0.2
 * @package           WooCommerce Intus Ehancements
 *
 * @wordpress-plugin
 * Plugin Name:        WooCommerce Intus Enhancements
 * Plugin URI:        https://www.sweet-apple.co.uk/custom-wordpress-plugin-development/
 * Description:       Add a variety of useful enhancements to WooCommerce
 * Version:           0.0.2
 * Author:            Clive Sweeting, Sweet-Apple
 * Author URI:        https://sweet-apple.co.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woocommerce_intus_shipment_tracking
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( !defined( 'WPINC' ) ) {
    die;
}

/**
 * Current plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 */
define( 'WOOCOMMERCE_INTUS_ENHANCEMENTS', '0.0.1' );

/**
 * Schedule the daily event if necessary.
 */
function schedule_delete_expired_coupons() {
    if ( !wp_next_scheduled( 'delete_expired_coupons' ) ) {
        wp_schedule_event( time(), 'daily', 'delete_expired_coupons' );
    }
}

add_action( 'init', __NAMESPACE__ . '\\schedule_delete_expired_coupons' );

function find_all_used_coupons() {
    global $wpdb;
    return $wpdb->get_col( "
        SELECT DISTINCT(oi.`order_item_name`)
        FROM $wpdb->posts AS p
        INNER JOIN {$wpdb->prefix}woocommerce_order_items AS oi
        ON p.ID = oi.order_id
        WHERE p.post_type = 'shop_order'
        AND oi.order_item_type = 'coupon'
        GROUP BY oi.`order_item_name`"
    );
}

/**
 * Trash all expired coupons when the event is triggered.
 */
function delete_expired_coupons() {
    $args = array(
        'posts_per_page' => - 1,
        'post_type'      => 'shop_coupon',
        'post_status'    => 'publish',
        'meta_query'     => array(
            'relation' => 'AND',
            array(
                'key'     => 'date_expires',
                'value'   => current_time( 'timestamp' ),
                'compare' => '<='
            ),
            array(
                'key'     => 'date_expires',
                'value'   => '',
                'compare' => '!='
            )
        )
    );

    $coupons = get_posts( $args );

    if ( !empty( $coupons ) ) {
        $coupons_used = find_all_used_coupons();

        foreach ( $coupons as $coupon ) {
            if ( !in_array( $coupon->post_title, $coupons_used ) ) {
                wp_trash_post( $coupon->ID );
            }
        }
    }
}

//add_action( 'delete_expired_coupons', __NAMESPACE__ . '\\delete_expired_coupons' );


/**
 * @param int $post_id
 * @param     $post
 *
 * @return void
 */
function regenerate_post_html_cache( int $post_id, $post ) {
    $cachable_post_types = [ 'post', 'page', 'product' ];

    if ( !in_array( $post->post_type, $cachable_post_types ) ) {
        return;
    }
    //Fix clear any problematic cached entities not handled by the Caching plugins
    clear_grouped_parent_html_cache( $post_id, $post );

    $cache_clean_function = get_cache_cleaning_function();
    if ( $cache_clean_function !== null ) {

        $permalink = get_permalink( $post_id );
        if ( $permalink ) {
            $args = [
                'timeout'   => 10,
                'sslverify' => false,
                'cookies'   => [],
                [ 'headers' => [
                    'user-agent' => 'Mozilla/5.0 (Linux; Android 8.0.0;) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Mobile Safari/537.36' ]
                ]
            ];
            wp_remote_get( $permalink, $args );
            if ( $post->post_type == 'product' ) {
                //Regenerate any parent products...
                $grouped_parents = get_grouped_parents_from_product( $post_id );
                if ( is_array( $grouped_parents ) ) {
                    foreach ( $grouped_parents as $grouped_parent ) {
                        wp_remote_get( get_permalink( $grouped_parent->ID ), $args );
                    }
                }
                //Regenerate any related categories
                $terms = get_the_terms( $post_id, 'product_cat' );
                foreach ( $terms as $term ) {
                    $product_cat_id = $term->term_id;
                    wp_remote_get( get_term_link( $product_cat_id, 'product_cat' ), $args );

                }
            }
        }
    }
}

/**
 * @param int $post_id
 * @param     $post
 *
 * @return void
 */
function clear_grouped_parent_html_cache( int $post_id, $post ) {
    $cachable_post_types = [ 'product' ];
    //Bail early if we aren't dealing with a product...
    if ( !in_array( $post->post_type, $cachable_post_types ) ) {
        return;
    }
    // Do toy have a supported caching plugin installed?
    $cache_clean_function = get_cache_cleaning_function();
    if ( $cache_clean_function !== null ) {
        // Clear from the cache any related parent grouped products
        $grouped_parents = get_grouped_parents_from_product( $post_id );
        if ( is_array( $grouped_parents ) ) {
            foreach ( $grouped_parents as $grouped_parent ) {
                $cache_clean_function( $grouped_parent->ID );
            }
        }
    }

}


/**
 * @param int $post_id
 *
 * @return int[]|\WP_Post[]|null
 */
function get_grouped_parents_from_product( int $post_id ) {
    $args  = array(
        'post_type'     => 'product',
        'post_status'   => 'publish',
        'no_found_rows' => 1,
        'meta_query'    => [
            array(
                'key'     => '_children',
                'compare' => 'LIKE',
                'value'   => $post_id
            ),
        ]
    );
    $query = new \WP_Query( $args );
    if ( $query->have_posts() ) {
        return $query->get_posts();
    }
    return null;
}

/**
 * @return string|null
 */
function get_cache_cleaning_function()
{
    $cache_clean_function = null;
    if ( function_exists( '\wpsc_delete_post_cache' ) ) {
        $cache_clean_function = '\wpsc_delete_post_cache';
    }
    if ( function_exists( '\rocket_clean_post' ) ) {
        $cache_clean_function = '\rocket_clean_post';
    }
    return $cache_clean_function;
}


add_action( 'woocommerce_update_product', __NAMESPACE__ . '\\regenerate_post_html_cache', 999999, 2 );

