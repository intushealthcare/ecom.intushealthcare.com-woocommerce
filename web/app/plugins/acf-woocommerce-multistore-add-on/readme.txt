=== WooMultistore Woocommerce ACF Add-On ===
Contributors: lykkemedia
Tags: multistore, multisite, woocommerce, wpml, ecommerce, web store, web shop, custom fields
Requires at least: 4.0
Tested up to: 5.5
Stable tag: 1.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Republish Advanced Custom Fields when using WooMultistore.

== Description ==
Republish custom fields and custom field groups made with the plugin Advanced Custom Fields (ACF) to stores when using WooMultistore.

This addon requires the [WooMultistore plugin](https://woomultistore.com) and [ACF](https://www.advancedcustomfields.com/).

== Screenshots ==

== Changelog ==

= 1.0.3 =
* Updated plugin main file name

= 1.0.2 =
* Added regular WordPress support

= 1.0.1 =
* Fixed syncing issue

= 1.0.0 =
* Initial release