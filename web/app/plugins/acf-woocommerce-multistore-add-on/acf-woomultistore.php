<?php
/*
 * Plugin Name: WooMultistore Woocommerce ACF Add-On
 * Plugin URI: http://www.lykkemedia.no
 * Description: Adds integration with WooCommerce Multistore. Requires ACF.
 * Version: 1.0.3
 * Author: Lykke Media AS
*/

final class ACF_WM_Add_On {
	public function __construct() {

		register_activation_hook( __FILE__, array( $this, 'create_sync_directory' ) );

		if ( false === $this->check_if_plugins_active() ) {
			return;
		}

		add_filter( 'acf/settings/load_json', array( $this, 'acf_json_load_point' ) );
		add_action( 'acf/update_field_group', array( $this, 'acf_write_json_field_group' ) );

		if ( is_multisite() ) {
			add_action( 'acf/save_post', array( $this, 'update_acf_fields' ), PHP_INT_MAX );
		} else {
			// Add ACF fields to product JSON.
			add_action( 'WOO_MSTORE_SYNC/process_json/product', array( $this, 'add_acf_fields_to_json' ), 10, 3 );
			add_action( 'WOO_MSTORE_SYNC/sync_child/complete', array( $this, 'save_acf_fields_to_json' ), 10, 3 );
		}

		add_action( 'WOO_MSTORE_SYNC/CUSTOM/acf_json', array( $this, 'sync_field_groups' ), PHP_INT_MAX, 1 );
	}

	/**
	 * * Create the sync directory
	 **/
	public function create_sync_directory() {
		$sync_dir = $this->get_sync_dir();

		@mkdir( $sync_dir );

		if ( $file = fopen( $sync_dir . 'index.php', 'w' ) ) {
			fwrite( $file, '<?php // Silence is golden.' );
			fclose( $file );

			chmod( $sync_dir . 'index.php', 0644 );
		}
	}

	public function update_acf_fields( $master_product_id ) {
		global $WOO_MSTORE;

		$master_product = wc_get_product( $master_product_id );
		if ( empty( $master_product ) ) {
			return;
		}

		if ( $WOO_MSTORE->product_interface->is_slave_product( $master_product ) ) {
			return;
		}

		$master_product_field_objects = get_field_objects( $master_product_id );
		if ( empty( $master_product_field_objects ) ) {
			return;
		}

		$master_product_blog_id = get_current_blog_id();
		$blog_ids               = $WOO_MSTORE->functions->get_active_woocommerce_blog_ids();
		foreach ( $blog_ids as $slave_product_blog_id ) {
			if (
				$master_product_blog_id == $slave_product_blog_id
				||
				'yes' !== $master_product->get_meta( '_woonet_publish_to_' . $slave_product_blog_id )
			) {
				continue;
			}

			switch_to_blog( $slave_product_blog_id );

			if ( ! $this->is_acf_active() ) {
				continue;
			}

				$slave_product_id = $WOO_MSTORE->product_interface->get_slave_product_id( $master_product_blog_id, $master_product->get_id() );
			if ( empty( $slave_product_id ) ) {
				continue;
			}

			foreach ( $master_product_field_objects as $key => $master_product_field_object ) {
				acf_update_value( $master_product_field_object['value'], $slave_product_id, $master_product_field_object );
			}

			restore_current_blog();
		}
	}

	private function is_acf_active() {
		// Makes sure the plugin is defined before trying to use it
		if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
			require_once ABSPATH . '/wp-admin/includes/plugin.php';
		}

		return ( is_plugin_active( 'advanced-custom-fields/acf.php' ) || is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) );
	}

	public function acf_write_json_field_group( $field_group ) {

		/**
		 * Regular WordPress version, child sites can not update field groups to master.
		 */
		if ( false === is_multisite() && get_option( 'woonet_network_type' ) != 'master' ) {
			return;
		}

		$field_group['fields'] = acf_get_fields( $field_group );

		// save file
		add_filter( 'acf/settings/save_json', array( $this, 'acf_json_save_point' ) );
		acf_write_json_field_group( $field_group );
		remove_filter( 'acf/settings/save_json', array( $this, 'acf_json_save_point' ) );

		if ( false === is_multisite() && get_option( 'woonet_network_type' ) == 'master' ) {
			// Send the JSON to the single site.
			$this->send_acf_json( $field_group['key'] );
		}
	}

	/**
	 * Run on the regular WordPress version, master to send the generated JSON
	 * with field gorups to the child sites.
	 *
	 * @since 1.0.2
	 * @param string $file_name
	 * @return void
	 */
	public function send_acf_json( $file_name ) {
		$data = array();
		$file = $this->get_sync_dir() . $file_name . '.json';

		if ( file_exists( $file ) ) {
			$data['payload_type']     = 'acf_json';
			$data['field_group_key']  = $file_name;
			$data['payload_contents'] = file_get_contents( $file );

			$_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();
			$_engine->send_payload( $data );
		}
	}

	public function acf_json_save_point() {
		return $this->get_sync_dir();
	}

	/**
	 * * Load the main site path
	 **/
	public function acf_json_load_point( $paths ) {
		$paths[] = $this->get_sync_dir();

		return $paths;
	}

	/**
	 * * Get the sync directory, where json is saved
	 **/
	private function get_sync_dir() {

		if ( is_multisite() ) {
			$main_site_id = get_main_site_id();
			switch_to_blog( $main_site_id );

			$uploads  = wp_upload_dir();
			$sync_dir = $uploads['basedir'] . DIRECTORY_SEPARATOR . strtolower( __CLASS__ ) . DIRECTORY_SEPARATOR;

			restore_current_blog();

		} else {
			$uploads  = wp_upload_dir();
			$sync_dir = $uploads['basedir'] . DIRECTORY_SEPARATOR . strtolower( __CLASS__ ) . DIRECTORY_SEPARATOR;
		}

		return $sync_dir;
	}

	/**
	 * Check if required plugins are active.
	 *
	 * @return void
	 */
	public function check_if_plugins_active() {
		if ( ! function_exists( 'is_plugin_active' ) ) {
			include_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		if ( ! is_plugin_active( 'advanced-custom-fields-pro/acf.php' )
			|| ! is_plugin_active( 'advanced-custom-fields/acf.php' )
			|| ! is_plugin_active( 'woocommerce-multistore/woocommerce-multistore.php' ) ) {
			return;
		}

		return true;
	}

	public function sync_field_groups( $resp ) {
		$file_name     = sanitize_text_field( $_POST['data']['field_group_key'] ) . '.json';
		$file_contents = sanitize_text_field( $_POST['data']['payload_contents'] );

		@file_put_contents( $this->get_sync_dir() . $file_name, stripslashes( $file_contents ) );
	}

	public function add_acf_fields_to_json( $product, $wc_product, $product_id ) {
		$_fields = get_field_objects( $product_id );

		if ( ! empty( $_fields ) ) {
			$product['acf_fields'] = $_fields;
		}

		return $product;
	}

	public function save_acf_fields_to_json( $wc_product_id, $parent_id, $product ) {
		if ( ! empty( $product['acf_fields'] ) ) {
			foreach ( $product['acf_fields'] as $key => $field_object ) {
				acf_update_value( $field_object['value'], $wc_product_id, $field_object );
			}
		}
	}
}

new ACF_WM_Add_On();
