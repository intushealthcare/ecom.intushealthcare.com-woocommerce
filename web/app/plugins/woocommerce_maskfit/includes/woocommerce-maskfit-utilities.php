<?php


/**
 * Log errors to separate file
 */
if( !function_exists( 'maskfit_wooplugin_error_log' ) ) {
    function maskfit_wooplugin_error_log( $error, $path = WOO_MASKFIT_LOG_PATH, $meta = null){
        $message_type = 1;
        if( WP_DEBUG == true ) {
            $message_type = 3;
        }
        $timestamp = "[" .date("d-M-Y H:i:s", $_SERVER['REQUEST_TIME'] ) . "]";
        error_log( $timestamp . PHP_EOL, $message_type ,$path );
        error_log( $error . PHP_EOL, $message_type ,$path );
    }
}


/**
 * Get template.
 *
 * Search for the template and include the file.
 *
 * @since 1.0.0
 *
 * @see maskfit_wooplugin_products_locate_template()
 *
 * @param string 	$template_name			Template to load.
 * @param array 	$args					Args passed for the template file.
 * @param string 	$string $template_path	Path to templates.
 * @param string	$default_path			Default path to template files.
 */
function maskfit_wooplugin_get_template( $template_name, $args = array(), $tempate_path = '', $default_path = '' ) {

    if ( is_array( $args ) && isset( $args ) ) :
        extract( $args );
    endif;

    $template_file = maskfit_wooplugin_locate_template( $template_name, $tempate_path, $default_path );

    if ( ! file_exists( $template_file ) ) :
        _doing_it_wrong( __FUNCTION__, sprintf( '<code>%s</code> does not exist.', $template_file ), '1.0.0' );
        return;
    endif;
    include $template_file;
}


/**
 * @param        $template_name
 * @param array  $args
 * @param string $tempate_path
 * @param string $default_path
 *
 * @return false|string
 */
function maskfit_wooplugin_get_template_html( $template_name, $args = array(), $tempate_path = '', $default_path = '' ) {
    ob_start();
    maskfit_wooplugin_get_template( $template_name, $args, $tempate_path, $default_path );
    return ob_get_clean();
}


/**
 * Locate template.
 *
 * Locate the called template.
 * Search Order:
 * 1. /themes/theme/woo-mapplic-templates/$template_name
 * 2. /themes/theme/$template_name
 * 3. /plugins/woocommerce-plugin-templates/templates/$template_name.
 *
 * @since 1.0.0
 *
 * @param 	string 	$template_name			Template to load.
 * @param 	string 	$string $template_path	Path to templates.
 * @param 	string	$default_path			Default path to template files.
 * @return 	string 							Path to the template file.
 */
function maskfit_wooplugin_locate_template( $template_name, $template_path = '', $default_path = '' ) {

    // Set variable to search in woocommerce-plugin-templates folder of theme.
    if ( ! $template_path ) :
        $template_path = 'woo-mapplic-templates/';
    endif;

    // Set default plugin templates path.
    if ( ! $default_path ) :
        $default_path = WOO_MASKFIT_ROOT; // Path to the template folder
    endif;

    // Search template file in theme folder.
    $template = locate_template( array(
        $template_path . $template_name,
        $template_name
    ) );

    // Get plugins template file.
    if ( ! $template ) :
        $template = $default_path . $template_name;
    endif;

    return apply_filters( 'maskfit_wooplugin_locate_template', $template, $template_name, $template_path, $default_path );

}



/**
 * @param $product \WC_Product
 *
 * @return int:null
 */
function maskfit_wooplugin_get_parent_product_id( \WC_Product $product ) {
    /* @var int $product_id */
    $product_id = $product->get_id();
    $parent_product_id = ( $product->is_type( 'variation' ) ) ? $product->get_parent_id() : $product->get_id();
    if( $product_id != $parent_product_id ){
        return $parent_product_id;
    }
    return null;
}


/**
 * @param $product \WC_Product
 *
 * @return boolean
 */
function maskfit_wooplugin_has_parent_product( \WC_Product $product ) {
    if( maskfit_wooplugin_get_parent_product_id( $product ) ){
        return true;
    }
    return false;
}


/**
 * @param $product \WC_Product
 *
 * @return \WC_Product|null
 */
function maskfit_wooplugin_get_parent_product( \WC_Product $product ) {
    if( $parent_product_id = maskfit_wooplugin_get_parent_product_id( $product ) ){
        return wc_get_product( $parent_product_id );
    }
    return null;
}


/**
 * A link to product that includes SKU parameters to prepopulate product options
 * @param $product \WC_Product
 *
 * @return void
 */
function maskfit_wooplugin_get_product_direct_link( $product ) {
    $url = $product->get_permalink();
    if( maskfit_wooplugin_has_parent_product( $product ) ){
        $parent_product = maskfit_wooplugin_get_parent_product( $product );
        $url = $parent_product->get_permalink();
        $variations = $product->get_variation_attributes();
        $params = ['sku' => $product->get_sku() ];
        foreach ( $variations as $key => $value){
            $params[$key] = $value;
        }
        $url .= "?=" . http_build_query($params);
    }
    return $url;
}
