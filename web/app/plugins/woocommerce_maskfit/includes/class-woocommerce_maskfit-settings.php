<?php

/**
 *
 * @link       http://example.com
 * @since      0.1.1
 *
 * @package    Woocommerce_So_Coupons
 * @subpackage Woocommerce_So_Coupons/includes
 */

/**
 * WooCommerce Settings
 *
 * This class defines all code necessary to set and retrieve plugin settings.
 *
 * @since      0.1.1
 * @package    Woocommerce_So_Coupons
 * @subpackage Woocommerce_So_Coupons/includes
 * @author     Your Name <email@example.com>
 */
class Woocommerce_Maskfit_Settings {

    const SETTINGS_TAB = 'maskfit_api';

    /**
     * Bootstraps the class and hooks required actions & filters.
     *
     */
    public static function init()
    {
        add_filter( 'woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab', 50 );
        add_action( 'woocommerce_settings_tabs_' . self::SETTINGS_TAB, __CLASS__ . '::settings_tab' );
        add_action( 'woocommerce_update_options_' . self::SETTINGS_TAB, __CLASS__ . '::update_settings' );
        add_filter( 'plugin_action_links_' . WOO_MASKFIT_BASENAME, __CLASS__ . '::add_plugin_page_settings_link' );
    }


    /**
     * Add the plugin settings link
     * @param $links
     *
     * @return mixed
     */
    public static function add_plugin_page_settings_link( $links )
    {
        $links[] = '<a href="' .
                   admin_url( 'admin.php?page=wc-settings&tab='. self::SETTINGS_TAB ) .
                   '">' . __( 'Settings' ) . '</a>';
        return $links;
    }


    /**
     * Add a new settings tab to the WooCommerce settings tabs array.
     *
     * @param array $settings_tabs Array of WooCommerce setting tabs & their labels, excluding the Subscription tab.
     *
     * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including the Subscription tab.
     */
    public static function add_settings_tab( $settings_tabs )
    {
        $settings_tabs[ self::SETTINGS_TAB ] = __( 'Maskfit Integration', 'woocommerce_maskfit' );
        return $settings_tabs;
    }

    /**
     * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
     *
     * @uses woocommerce_admin_fields()
     * @uses self::get_settings()
     */
    public static function settings_tab()
    {
        woocommerce_admin_fields( self::get_settings() );
    }

    /**
     * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
     *
     * @uses woocommerce_update_options()
     * @uses self::get_settings()
     */
    public static function update_settings()
    {
        woocommerce_update_options( self::get_settings() );
    }

    /**
     * Get all the settings for this plugin for @return array Array of settings for @see woocommerce_admin_fields() function.
     * @see woocommerce_admin_fields() function.
     *
     */
    public static function get_settings()
    {
        $settings = array(
            'section_title_api' => array(
                'name' => __( 'Maskfit API Details', 'woocommerce_maskfit' ),
                'type' => 'title',
                'desc' => __( 'Enable integration of WooCommerce with the Maskfit Embedded Form Service.', 'woocommerce_maskfit' ),
                'id'   => WOO_MASKFIT_OPTION_NAME . '_api_section_title',
            ),

            'enabled' => array(
                'title'     => __( 'Enable/Disable', 'woocommerce_maskfit' ),
                'desc'       => __( 'Enable Maskfit API integration', 'woocommerce_maskfit' ),
                'id'       => WOO_MASKFIT_OPTION_NAME . '_maskfit_api_enabled',
                'default'  => 'no',
                'type'     => 'checkbox',
            ),

            'section_title_api_end' => array(
                'type' => 'sectionend',
                'id'   => WOO_MASKFIT_OPTION_NAME . '_api_section_title',
            ),

            'section_title_maskfit_api_urls' => array(
                'name' => __( 'Maskfit API Urls', 'woocommerce_maskfit' ),
                'type' => 'title',
                'id'   => WOO_MASKFIT_OPTION_NAME . '_maskfit_api_urls_section_title',
            ),


            'maskfit_api_rest_url' => array(
                'title'     => __( 'REST Web API url', 'woocommerce_maskfit' ),
                'desc'       => __( 'The url to the Maskfit API. Do NOT include a trailing slash', 'woocommerce_maskfit' ),
                'id'       => WOO_MASKFIT_OPTION_NAME . '_maskfit_api_url',
                'default'  => 'https://stage.maskfitar.com/api',
                'type'     => 'text',
            ),

            'section_title_maskfit_api_urls_end' => array(
                'type' => 'sectionend',
                'id'   => WOO_MASKFIT_OPTION_NAME . '_maskfit_api_urls_section_title',
            ),

            'section_title_credentials' => array(
                'name' => __( 'API Credentials', 'woocommerce_maskfit' ),
                'type' => 'title',
                'desc' => __( 'Enter the details to required to authenticate with the Maskfit API', 'woocommerce_maskfit' ),
                'id'   => WOO_MASKFIT_OPTION_NAME . '_maskfit_api_credentials_section_title',
            ),

            'maskfit_api_live_app_token' => array(
                'title'     => __( 'LIVE Mode App Key Token', 'woocommerce_maskfit' ),
                'desc'       => __( 'Get your API Toekn from Maskfit', 'woocommerce_maskfit' ),
                'desc_tip' => true,
                'id'       => WOO_MASKFIT_OPTION_NAME . '_maskfit_api_token_live',
                'default'  => '',
                'type'     => 'text',
            ),

            'section_title_credentials_end' => array(
                'type' => 'sectionend',
                'id'   => WOO_MASKFIT_OPTION_NAME . '_maskfit_api_credentials_section_title',
            ),


            'section_title_maskfit_restriction_ids' => array(
                'name' => __( 'Control the display of the Maskfit form', 'woocommerce_maskfit' ),
                'desc' => __( 'The Maskfit form will be displayed according to the  Allow / Deny rules listed below. Deny rules will always take precedence over Allows', 'woocommerce_maskfit' ),
                'type' => 'title',
                'id'   => WOO_MASKFIT_OPTION_NAME . '_maskfit_restriction_ids_section_title',
            ),

            'maskfit_api_category_ids_allowed' => array(
                'title'     => __( 'Allowed Category IDs', 'woocommerce_maskfit' ),
                'desc'       => __( 'Enter the numeric Category IDs ( comma separated )', 'woocommerce_maskfit' ),
                'id'       => WOO_MASKFIT_OPTION_NAME . '_category_ids_allowed',
                'default'  => null,
                'type'     => 'text',
            ),

            'maskfit_api_category_ids_disallowed' => array(
                'title'     => __( 'Disallowed Category IDs', 'woocommerce_maskfit' ),
                'desc'       => __( 'Enter the numeric Category IDs ( comma separated )', 'woocommerce_maskfit' ),
                'id'       => WOO_MASKFIT_OPTION_NAME . '_category_ids_disallowed',
                'default'  => null,
                'type'     => 'text',
            ),

            'maskfit_api_product_ids_allowed' => array(
                'title'     => __( 'Allowed Product IDs', 'woocommerce_maskfit' ),
                'desc'       => __( 'Enter the numeric Product IDs ( comma separated )', 'woocommerce_maskfit' ),
                'id'       => WOO_MASKFIT_OPTION_NAME . '_product_ids_allowed',
                'default'  => null,
                'type'     => 'text',
            ),

            'maskfit_api_product_ids_disallowed' => array(
                'title'     => __( 'Disallowed Product IDs', 'woocommerce_maskfit' ),
                'desc'       => __( 'Enter the numeric Product IDs ( comma separated )', 'woocommerce_maskfit' ),
                'id'       => WOO_MASKFIT_OPTION_NAME . '_product_ids_disallowed',
                'default'  => null,
                'type'     => 'text',
            ),


            'section_title_tesco_product_ids_end' => array(
                'type' => 'sectionend',
                'id'   => WOO_MASKFIT_OPTION_NAME . '_maskfit_restriction_ids_section_title',
            ),


            'section_title_maskfit_return_url' => array(
                'name' => __( 'Url to display Maskfit approved products', 'woocommerce_maskfit' ),
                'desc'       => __( 'Once the customer has completed their Maskfit assessment they need to be returned to a url on your website that will contains the Maskfit results shortcode.<br>Create a new Page and within that page insert <strong>[woo_maskfit_results]</strong>. This will display the products suitable for them.', 'woocommerce_maskfit' ),
                'type' => 'title',
                'id'   => WOO_MASKFIT_OPTION_NAME . '_maskfit_return_url_section_title',
            ),


            'maskfit_api_results_url' => array(
                'title'     => __( 'Results display url', 'woocommerce_maskfit' ),
                'desc'       => __( 'Enter the url to the page that contains the [woo_maskfit_results] shortcode', 'woocommerce_maskfit' ),
                'id'       => WOO_MASKFIT_OPTION_NAME . '_maskfit_results_url',
                'default'  => null,
                'type'     => 'url',
            ),

            'section_title_maskfit_return_url_end' => array(
                'type' => 'sectionend',
                'id'   => WOO_MASKFIT_OPTION_NAME . '_maskfit_return_url_section_title',
            ),

        );

        return apply_filters( 'maskfit_api_wc_settings_tab_settings', $settings );
    }

}

Woocommerce_Maskfit_Settings::init();

/**
 * Is the Tesco integration enabled?
 * @return bool
 */
function woo_maskfit_api_enabled()
{
    if ( 'yes' === get_option( WOO_MASKFIT_OPTION_NAME . "_maskfit_api_enabled" ) ) {
        return true;
    }
    return false;
}


/**
 * @return stdClass
 */
function woo_maskfit_api_get_api_details()
{
    $settings = new stdClass();
    $setting_suffix = 'live';
//    $settings->key =  get_option( WOO_MASKFIT_OPTION_NAME . "_maskfit_api_key_" . $setting_suffix );
    $settings->token = get_option( WOO_MASKFIT_OPTION_NAME . "_maskfit_api_token_" . $setting_suffix );
    $settings->mode = $setting_suffix;
    return $settings;
}

/**
 * @return false|mixed|void
 */
function woo_maskfit_api_get_api_url()
{
    $rest_url = get_option( WOO_MASKFIT_OPTION_NAME . "_maskfit_api_url" );
    if ( !empty( $rest_url  ) ) {
        return $rest_url;
    }
    return false;
}


/**
 * @return false|mixed|void
 */
function woo_maskfit_api_get_results_url()
{
    $result_urls = get_option( WOO_MASKFIT_OPTION_NAME . "_maskfit_results_url" );
    if ( !empty( $result_urls  ) ) {
        return $result_urls;
    }
    return false;
}


/**
 * @return stdClass
 */
function woo_maskfit_api_product_and_category_restrictions()
{
    $settings = new stdClass();
    $settings->product_ids_allowed = [];
    $settings->product_ids_disallowed = [];
    $settings->category_ids_allowed = [];
    $settings->category_ids_disallowed = [];

    $product_ids_allowed = get_option( WOO_MASKFIT_OPTION_NAME . "_product_ids_allowed" );
    if ( !empty( $product_ids_allowed  ) ) {
        $settings->product_ids_allowed = array_map( 'trim',  explode( ',', $product_ids_allowed ) );
    }

    $product_ids_disallowed = get_option( WOO_MASKFIT_OPTION_NAME . "_product_ids_disallowed" );
    if ( !empty( $product_ids_disallowed  ) ) {
        $settings->product_ids_disallowed = array_map( 'trim',  explode( ',', $product_ids_disallowed ) );
    }

    $category_ids_allowed = get_option( WOO_MASKFIT_OPTION_NAME . "_category_ids_allowed" );
    if ( !empty( $category_ids_allowed  ) ) {
        $settings->category_ids_allowed = array_map( 'trim',  explode( ',', $category_ids_allowed ) );
    }

    $category_ids_disallowed = get_option( WOO_MASKFIT_OPTION_NAME . "_category_ids_disallowed" );
    if ( !empty( $category_ids_disallowed  ) ) {
        $settings->category_ids_disallowed = array_map( 'trim',  explode( ',', $category_ids_disallowed ) );
    }
    return $settings;
}
