<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://sweet-apple.co.uk
 * @since      0.1.0
 *
 * @package    Woocommerce_maskfit
 * @subpackage Woocommerce_maskfit/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      0.1.0
 * @package    Woocommerce_maskfit
 * @subpackage Woocommerce_maskfit/includes
 * @author     Clive Sweeting, Sweet-Apple <info@test.co.co.uk>
 */
class Woocommerce_Maskfit_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1.0
	 */
	public static function deactivate() {

	}

}
