<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://sweet-apple.co.uk
 * @since      0.1.0
 *
 * @package    Woocommerce_maskfit
 * @subpackage Woocommerce_maskfit/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      0.1.0
 * @package    Woocommerce_maskfit
 * @subpackage Woocommerce_maskfit/includes
 * @author     Clive Sweeting, Sweet-Apple <info@test.co.co.uk>
 */
class Woocommerce_maskfit {

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    0.1.0
     * @access   protected
     * @var      Woocommerce_Maskfit_Loader $loader Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    0.1.0
     * @access   protected
     * @var      string $plugin_name The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    0.1.0
     * @access   protected
     * @var      string $version The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    0.1.0
     */
    public function __construct()
    {
        if ( defined( 'WOOCOMMERCE_MASKFIT_VERSION' ) )
        {
            $this->version = WOOCOMMERCE_MASKFIT_VERSION;
        } else
        {
            $this->version = '0.1.0';
        }
        $this->plugin_name = 'woocommerce_maskfit';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();
        $this->define_rest_hooks();

    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Woocommerce_Maskfit_Loader. Orchestrates the hooks of the plugin.
     * - Woocommerce_Maskfit_i18n. Defines internationalization functionality.
     * - Woocommerce_Maskfit_Admin. Defines all hooks for the admin area.
     * - Woocommerce_Maskfit_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    0.1.0
     * @access   private
     */
    private function load_dependencies()
    {

        /**
         * A collection of utility function and/or methods used in this plugin
         * of the plugin.
         */
        require_once plugin_dir_path( __DIR__ ) . 'includes/woocommerce-maskfit-utilities.php';

        /**
         * The class responsible for defining and retrieving WooCommerce Settings
         * of the plugin.
         */
        require_once plugin_dir_path( __DIR__ ) . 'includes/class-woocommerce_maskfit-settings.php';

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce_maskfit-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce_maskfit-i18n.php';

        /**
         * The class responsible for custom REST endpoints
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce_maskfit-rest-controller.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-woocommerce_maskfit-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-woocommerce_maskfit-public.php';

        $this->loader = new Woocommerce_Maskfit_Loader();

    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Woocommerce_Maskfit_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    0.1.0
     * @access   private
     */
    private function set_locale()
    {

        $plugin_i18n = new Woocommerce_Maskfit_i18n();

        $this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    0.1.0
     * @access   private
     */
    private function define_admin_hooks()
    {

        $plugin_admin = new Woocommerce_Maskfit_Admin( $this->get_plugin_name(), $this->get_version() );

        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    0.1.0
     * @access   private
     */
    private function define_public_hooks()
    {
        $plugin_public = new Woocommerce_Maskfit_Public( $this->get_plugin_name(), $this->get_version() );
        if( woo_maskfit_api_enabled() ) {
            $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
            $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
            $this->loader->add_action( 'woocommerce_single_product_summary', $plugin_public, 'product_form', 20 );
            $this->loader->add_action( 'wp_ajax_woo-maskfit-apply', $plugin_public, 'ajax_maskfit_apply', 20 );
            $this->loader->add_action( 'wp_ajax_nopriv_woo-maskfit-apply', $plugin_public, 'ajax_maskfit_apply', 20 );
            $this->loader->add_shortcode( 'woo_maskfit_results', $plugin_public, 'mask_results' );
        }
    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    0.1.0
     * @access   private
     */
    private function define_rest_hooks()
    {
        if( woo_maskfit_api_enabled() ) {
            $plugin_rest = new Woocommerce_Maskfit_Rest_Controller( $this->get_plugin_name(), $this->get_version() );
            $plugin_rest->init();
        }
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    0.1.0
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @return    string    The name of the plugin.
     * @since     0.1.0
     */
    public function get_plugin_name()
    {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @return    Woocommerce_Maskfit_Loader    Orchestrates the hooks of the plugin.
     * @since     0.1.0
     */
    public function get_loader()
    {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @return    string    The version number of the plugin.
     * @since     0.1.0
     */
    public function get_version()
    {
        return $this->version;
    }

}
