<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://sweet-apple.co.uk
 * @since      0.1.0
 *
 * @package    Woocommerce_maskfit
 * @subpackage Woocommerce_maskfit/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.1.0
 * @package    Woocommerce_maskfit
 * @subpackage Woocommerce_maskfit/includes
 * @author     Clive Sweeting, Sweet-Apple <info@test.co.co.uk>
 */
class Woocommerce_Maskfit_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.1.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'woocommerce_maskfit',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}

}
