<?php

use SEOPressElementorFAQScheme\FAQ_Schema;

class Woocommerce_Maskfit_Rest_Controller {

    /**
     * You can extend this class with
     * WP_REST_Controller / WC_REST_Controller / WC_REST_Products_V2_Controller / WC_REST_CRUD_Controller etc.
     * Found in packages/woocommerce-rest-api/src/Controllers/
     */
    const NAMESPACE = 'wc/v3';

    const ROUTE = 'maskfit';

    /**
     * @return void
     */
    public function init()
    {
        add_filter( 'woocommerce_rest_api_get_rest_namespaces', __CLASS__ . '::add_rest_api_controller' );
    }

    /**
     * Add this REST Controller to the known controllers
     * @param $controllers
     *
     * @return array
     */
    public static function add_rest_api_controller( $controllers )
    {
        $controllers[ self::NAMESPACE ][ SELF::ROUTE ] = __CLASS__;
        return $controllers;
    }

    /**
     * Register the maskfit route
     * @return void
     */
    public function register_routes()
    {
        register_rest_route(
            self::NAMESPACE,
            '/' . self::ROUTE . "/categories",
            [
                'methods'  => 'GET',
                'callback' => array( $this, 'get_maskfit_categories' ),
                'permission_callback' => '__return_true',
            ]
        );
        register_rest_route(
            self::NAMESPACE,
            '/' . self::ROUTE . "/products",
            [
                'methods'  => 'GET',
                'callback' => array( $this, 'get_maskfit_products' ),
                'permission_callback' => '__return_true',
            ]
        );
    }

    /**
     *
     * @param WP_REST_Request $request
     *
     * @return string|null
     */
    public function get_maskfit_categories( WP_REST_Request $request )
    {
        $restrictions = woo_maskfit_api_product_and_category_restrictions();
        $allowed_categories = $restrictions->category_ids_allowed;
        $disallowed_categories = $restrictions->category_ids_disallowed;
        $category_ids = array_diff($allowed_categories, $disallowed_categories);
        $response = new stdClass();
        $response->categories = $category_ids;
        return $response;
    }


    /**
     *
     * @param WP_REST_Request $request
     *
     * @return string|null
     */
    public function get_maskfit_products( WP_REST_Request $request )
    {
        $restrictions = woo_maskfit_api_product_and_category_restrictions();
        $allowed_categories = $restrictions->category_ids_allowed;
        $disallowed_categories = $restrictions->category_ids_disallowed;
        $category_ids = array_diff($allowed_categories, $disallowed_categories);

        //Annoyingly we cannot get products from category ID, we need the slug...
        $product_term_args = [
            'taxonomy' => 'product_cat',
            'include' => $category_ids,
            'orderby'  => 'include'
        ];
        $product_terms = get_terms($product_term_args);

        $product_term_slugs = [];
        foreach ($product_terms as $product_term) {
            $product_term_slugs[] = $product_term->slug;
        }

        // Ref: https://github.com/woocommerce/woocommerce/wiki/wc_get_products-and-WC_Product_Query
        // we use wc_get_products because "building custom WP_Queries or database queries is likely to break your code in future versions of WooCommerce"
        $product_args = [
            'post_status' => 'publish',
            'limit' => -1,
            'category' => $product_term_slugs,
        ];
        $products_from_categories = wc_get_products($product_args);

        $product_ids = [];
        foreach ( $products_from_categories as $product_from_category )
        {
            $product_ids[] = $product_from_category->get_id();
        }

        //Now let's add any explicitly allowed products...
        $product_ids = array_merge( $product_ids, $restrictions->product_ids_allowed );
        //Now let's remove any explicitly disallowed products...
        $product_ids = array_diff( $product_ids, $restrictions->product_ids_disallowed );

        $response = new stdClass();
        $response->categories = $category_ids;
        $response->products = $product_ids;
        return $response;
    }

}
