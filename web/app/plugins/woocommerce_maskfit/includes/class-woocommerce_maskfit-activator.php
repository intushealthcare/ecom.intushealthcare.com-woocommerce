<?php

/**
 * Fired during plugin activation
 *
 * @link       https://sweet-apple.co.uk
 * @since      0.1.0
 *
 * @package    Woocommerce_maskfit
 * @subpackage Woocommerce_maskfit/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.1.0
 * @package    Woocommerce_maskfit
 * @subpackage Woocommerce_maskfit/includes
 * @author     Clive Sweeting, Sweet-Apple <info@test.co.co.uk>
 */
class Woocommerce_Maskfit_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1.0
	 */
	public static function activate() {

	}

}
