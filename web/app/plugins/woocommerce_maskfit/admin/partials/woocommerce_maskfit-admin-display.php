<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://sweet-apple.co.uk
 * @since      0.1.0
 *
 * @package    Woocommerce_maskfit
 * @subpackage Woocommerce_maskfit/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
