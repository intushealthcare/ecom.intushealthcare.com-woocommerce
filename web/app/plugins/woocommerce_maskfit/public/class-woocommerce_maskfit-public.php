<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://sweet-apple.co.uk
 * @since      0.1.0
 *
 * @package    Woocommerce_maskfit
 * @subpackage Woocommerce_maskfit/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Woocommerce_maskfit
 * @subpackage Woocommerce_maskfit/public
 * @author     Clive Sweeting, Sweet-Apple <info@test.co.co.uk>
 */
class Woocommerce_Maskfit_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}


    public function getMaskFitApiEndpoint()
    {
        return woo_maskfit_api_get_api_url();
    }


    public function getMaskFitApiToken()
    {
        $api_details = woo_maskfit_api_get_api_details();
        return $api_details->token;
    }


    public function getMaskFitResultsUrl()
    {
        return woo_maskfit_api_get_results_url();
    }


	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Maskfit_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Maskfit_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/woocommerce_maskfit-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    0.1.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Maskfit_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Maskfit_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/woocommerce_maskfit-public.js', array( 'jquery' ), $this->version, false );

	}


    /**
     * @return void
     */
    public function product_form()
    {
        global $product;
        if ( $product )
        {
            if( $this->canShowMaskForm( $product ) ) {
                echo maskfit_wooplugin_get_template_html( 'public/partials/form.php', [ 'product' => $product ] );
            }
        }
    }


    /**
     * Should the Maskfit Form should be shown for this product?
     * @param WC_Product $product
     *
     * @return void
     */
    private function canShowMaskForm( WC_Product $product )
    {
        $product_id = $product->get_id();
        $product_and_category_restrictions = woo_maskfit_api_product_and_category_restrictions();
        //Is this product explicitly excluded?
        if( in_array( $product_id, $product_and_category_restrictions->product_ids_disallowed ) ){
            return false;
        }

        //Is this product explicitly included?
        if( in_array( $product_id, $product_and_category_restrictions->product_ids_allowed ) ){
            return true;
        }

        //Now let's do the same for categories...
        $terms = get_the_terms( $product->get_id(), 'product_cat' );

        foreach ($terms as $term) {
            if( in_array( $term->term_id, $product_and_category_restrictions->category_ids_disallowed ) ){
                return false;
            }
        }

        foreach ($terms as $term) {
            if( in_array( $term->term_id, $product_and_category_restrictions->category_ids_allowed ) ){
                return true;
            }
        }

        return false;
    }



    /**
     * Sned data to Maskfit API
     * @return void
     */
    public function ajax_maskfit_apply()
    {
        $maskfitEndpoint = $this->getMaskFitApiEndpoint() . "/tp/add-anon-patient/";
        $requestParams   = [
            'email',
            'phone',
        ];
        //If we are asking about a mask size, append the SKU to the request
        if( $_POST[ 'type' ] == 'size'  ){
            $requestParams[]  = 'sku';
        }
        $request_body = [];
        //Add the url the customer is sent to on getting the Mask suggestions...
        $request_body[ 'url' ] = $this->getMaskFitResultsUrl();
        //Add the  email,phone and sku to the Maskfit API request
        foreach ( $requestParams as $requestParam )
        {
            $requestValue = $_POST[ $requestParam ] ?? null;
            if ( $requestValue )
            {
                $request_body[ $requestParam ] = $requestValue;
            }
        }

        //Build the remote request...
        $args            = [
            'timeout' => 1,
            'headers' => [
                'Authorization' => $this->getMaskFitApiToken(),
                'Content-Type'  => 'application/x-www-form-urlencoded',
            ],
            'body'    => $request_body,
        ];
        $maskfitResponse = wp_remote_post( $maskfitEndpoint, $args );

        $responseData     = new \stdClass();
        $httpResponseCode = $maskfitResponse[ 'response' ][ 'code' ];
        if ( ( $httpResponseCode >= 200 ) && ( $httpResponseCode < 300 ) )
        {
            $responseData->success = true;
            $responseBody = json_decode( $maskfitResponse[ 'body' ] );
            $responseData->url     = "https://{$responseBody->url}";
        } else
        {
            $responseData->success = false;
        }
        wp_send_json( $responseData, $httpResponseCode );
    }


    /**
     * @return string|void
     */
    public function mask_results() {

        if( !is_admin() ){
            global $woocommerce;

            //Suitable Masks are passed in via querystring params...
            $skus = $_GET[ 'sku' ] ?? null;
            if ( ! $skus )
            {
                return "No results found";
            }
            $products = [];
            $skus     = explode( ',', $skus );
            try
            {
                foreach ( $skus as $sku )
                {
                    $product_id = wc_get_product_id_by_sku( $sku );
                    $product    = wc_get_product( $product_id );
                    if ( $product )
                    {
                        $products[ $product->get_id() ] = $product;
                    }
                }
                echo maskfit_wooplugin_get_template_html( 'public/partials/results.php', [ 'products' => $products ]  );
            }
            catch ( Exception $ex )
            {
                maskfit_wooplugin_error_log($ex->getFile() . "- Line: " . $ex->getLine() . " Message:" . $ex->getMessage());
            }
        }

    }

}
