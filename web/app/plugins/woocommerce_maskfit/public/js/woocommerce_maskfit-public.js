;(function ($) {

    $(document).ready(function () {

        let maskFitForm = $('#maskfit-form');
        if (maskFitForm.length) {
            maskFitFormActions();
        }

        function maskFitFormActions() {
            let btn = $('.btn-maskfit-form-submit');
            let messages = $('.product-maskfit-enquiry-messages');
            let container = $('.product-maskfit-enquiry-form-container');
            maskFitForm.on('submit', (e) => {
                e.preventDefault();
                let requestData = maskFitForm.serialize();
                requestData+= '&action=woo-maskfit-apply';
                btn.text('Please wait...');
                $.ajax({
                    url: MyAjax.ajaxurl,
                    type: 'POST',
                    dataType: 'html',
                    data: requestData,
                    success: (data) => {
                        console.log('d',data);
                        let successMessage = $('<h2>Succcess!</h2><p>Please check your email or text messages for a link to the online questionaire.</p>');
                        messages.html(successMessage);
                        messages.addClass('success').removeClass('d-none');
                        container.addClass('d-none');
                    },
                    complete: () => {
                        console.log('maskfitForm Complete');
                        btn.text('Let\'s Go!');
                    },
                    error: (errorThrown) => {
                        console.log(errorThrown);
                        let successMessage = $('<h2>Sorry, we had a problem :-(</h2><p>Please check the email or phone number entered is correct</p>');
                        messages.html(successMessage);
                        messages.addClass('error').removeClass('d-none');
                    },
                });
            });
        }

    });

})(jQuery);

