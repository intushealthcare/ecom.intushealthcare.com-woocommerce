<?php
if ( count($products) > 0 ) : ?>
    <div class="container psy-1 product-listing-container">

        <div id="products-container" class="products-container">

            <ul class="maskfit-products products columns-4">
                <?php foreach ($products as $product) : ?>
                    <li <?= wc_product_class('maskfit-product', $product) ?>>
                        <?php
                        /* @var $product WC_Product */
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_parent_id() ), 'woocommerce_thumbnail' );
                        $srcset = wp_get_attachment_image_srcset( get_post_thumbnail_id( $product->get_parent_id() ) );
                        ?>
                        <a href="<?= esc_attr( maskfit_wooplugin_get_product_direct_link( $product ) ) ?>">
                            <?= $product->get_image( 'woocommerce_thumbnail' ) ?>
                        </a>
                        <h3 class="product__title p pt-4 mb-2 color-secondary text-bold">
                            <a href="<?= esc_attr( maskfit_wooplugin_get_product_direct_link( $product ) ) ?>"><?= $product->get_name() ?></a>
                        </h3>
                        <span class="price">
                    <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol">£</span><?= number_format( $product->get_price(), 2 ) ?>
                    </span>
                </span>
                    </li>
                <?php endforeach; ?>
            </ul><!-- .maskfit-products -->

        </div><!-- #products-container -->

    </div><!-- .product-listing-container -->
<?php endif;
