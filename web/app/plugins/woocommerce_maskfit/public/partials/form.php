<?php
/* @var $product WC_Product */
?>
<div class="product-maskfit-enquiry">

    <div class="product-maskfit-enquiry-messages d-none"></div>

    <div class="product-maskfit-enquiry-form-container">

        <div class="d-flex flex-column flex-md-row">
            <div class="mb-4 mb-md-0 mr-md-5">
                <i class="far fa-3x fa-info-circle"></i>
            </div>
            <div class="pb-4">
                <h2><?php echo __( 'Need help choosing a size or mask?', 'woocommerce_maskfit' ); ?></h2>
            </div>
        </div>

        <div class="pb-4">
            <p><?php echo __( 'Our online tool can help! Answer a few quick questions about your CPAP prescription, then use your phone\'s camera or webcam to scan your face.', 'woocommerce_maskfit' ); ?></p>
            <p><?php echo __( 'You\'ll receive personalised recommendations for suitable sizes of this mask, or suggestions for other suitable masks and size.', 'woocommerce_maskfit' ); ?></p>
            <p><?php echo __( 'Simply enter your email or mobile number and we\'ll send you a link', 'woocommerce_maskfit' ); ?></p>
        </div>

        <form id="maskfit-form" class="product-maskfit-form">
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label"><?php echo __( 'Your email address', 'woocommerce_maskfit' ); ?></label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label"><?php echo __( 'Your mobile', 'woocommerce_maskfit' ); ?></label>
                <input type="phone" name="phone" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="mb-3">
                <fieldset>
                    <legend class="form-label"><?php echo __( 'I want to see', 'woocommerce_maskfit' ); ?></legend>
                    <div>
                        <input type="radio" id="mask_size" name="type" value="size">
                        <label class="form-label" for="mask_size"><?php echo __( 'Recommended size of this mask for my face', 'woocommerce_maskfit' ); ?></label>
                    </div>

                    <div>
                        <input type="radio" id="mask_range" name="type" value="range">
                        <label class="form-label" for="mask_range"><?php echo __( 'Recommended masks for my face', 'woocommerce_maskfit' ); ?></label>
                    </div>
                </fieldset>
            </div>
            <div class="product-maskfit-form-actions">
                <input type="hidden" name="sku" value="<?= $product->get_sku() ?>"/>
                <button type="submit" id="maskfit-form-submit" class="btn btn-primary btn-maskfit-form-submit"><?php echo __( 'Let\'s Go!', 'woocommerce_maskfit' ); ?></button>
            </div>
        </form>
    </div><!-- .product-maskfit-enquiry-form-container -->

</div><!-- .product-maskfit-enquiry -->
