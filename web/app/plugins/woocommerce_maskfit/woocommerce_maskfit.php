<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://sweet-apple.co.uk
 * @since             0.1.1
 * @package           Woocommerce_maskfit
 *
 * @wordpress-plugin
 * Plugin Name:       WooCommerce Maskfit
 * Plugin URI:        https://www.sweet-apple.co.uk/custom-wordpress-theme-development/
 * Description:       Integrate WooCommerce with Maskfit
 * Version:           0.1.1
 * Author:            Clive Sweeting, Sweet-Apple
 * Author URI:        https://sweet-apple.co.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woocommerce_maskfit
 * Domain Path:       /languages
 */

define( 'WOO_MASKFIT_FILE',        __FILE__ );
define( 'WOO_MASKFIT_BASENAME',    plugin_basename(__FILE__) );
define( 'WOO_MASKFIT_ROOT',        plugin_dir_path( __FILE__ ) );
define( 'WOO_MASKFIT_ROOT_URL',    plugin_dir_url( __FILE__ ) );
define( 'WOO_MASKFIT_INC_PATH',    WOO_MASKFIT_ROOT . "/includes/" );
define( 'WOO_MASKFIT_TEXTDOMAIN',  'woocommerce_maskfit' );
define( 'WOO_MASKFIT_VERSION',      '0.1.1' );
define( 'WOO_MASKFIT_OPTION_NAME', "woo_maskfit_wooplugin_options" );
define( 'WOO_MASKFIT_LOG_PATH', WP_CONTENT_DIR . "/uploads/debug-" .WOO_MASKFIT_TEXTDOMAIN .".log" );


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Currently plugin version.
 * Start at version 0.1.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WOOCOMMERCE_MASKFIT_VERSION', '0.1.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woocommerce_maskfit-activator.php
 */
function activate_woocommerce_maskfit() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce_maskfit-activator.php';
	Woocommerce_Maskfit_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woocommerce_maskfit-deactivator.php
 */
function deactivate_woocommerce_maskfit() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce_maskfit-deactivator.php';
	Woocommerce_Maskfit_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_woocommerce_maskfit' );
register_deactivation_hook( __FILE__, 'deactivate_woocommerce_maskfit' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce_maskfit.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.1.0
 */
function run_woocommerce_maskfit() {

	$plugin = new Woocommerce_maskfit();
	$plugin->run();

}
run_woocommerce_maskfit();
