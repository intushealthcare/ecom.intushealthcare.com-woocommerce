<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'eeb371495f4cf56fb9827116936dcb243b81261f',
        'name' => 'rextheme/best-woocommerce-feed',
        'dev' => false,
    ),
    'versions' => array(
        'philipnewcomer/wp-ajax-helper' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../philipnewcomer/wp-ajax-helper',
            'aliases' => array(),
            'reference' => '3416943af039cbca4eba1efbf4d9f728860e3a5e',
            'dev_requirement' => false,
        ),
        'rextheme/best-woocommerce-feed' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'eeb371495f4cf56fb9827116936dcb243b81261f',
            'dev_requirement' => false,
        ),
    ),
);
