<?php

class Rex_Product_Feed_Pro_License_Security {
    /**
     * @var $ciphering
     */
    private $ciphering;
    /**
     * @var $iv_length
     */
    private $iv_length;
    /**
     * @var $options
     */
    private $options;
    /**
     * @var $encryption_iv
     */
    private $encryption_iv;
    /**
     * @var $encryption_key
     */
    private $encryption_key;

    /**
     * @desc Initialize the class and set its properties.
     */
    public function __construct() {
        $this->ciphering = 'AES-128-CTR';
        $this->iv_length = openssl_cipher_iv_length( $this->ciphering );
        $this->options = 0;
        $this->encryption_iv = '2767360247209320';
        $this->encryption_key = defined( 'WPFM_SL_ITEM_ID' ) ? WPFM_SL_ITEM_ID : 7532;
    }

    /**
     * @desc Encrypting given string license
     *
     * @param $license
     * @return false|string
     */
    public function encrypt( $license ) {
        return openssl_encrypt( $license, $this->ciphering, $this->encryption_key, $this->options, $this->encryption_iv );
    }

    /**
     * @desc Decrypting given string license
     *
     * @param $license
     * @return false|string
     */
    public function decrypt( $license ) {
        return openssl_decrypt( $license, $this->ciphering, $this->encryption_key, $this->options, $this->encryption_iv );
    }
}