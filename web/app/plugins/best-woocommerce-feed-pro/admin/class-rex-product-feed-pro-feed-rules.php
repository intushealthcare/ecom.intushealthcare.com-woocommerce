<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://rextheme.com
 *
 * @package    Rex_Product_Feed_Rules
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines all the filter options for Feed Rules
 *
 * @package    Rex_Product_Feed_Rules
 * @author     RexTheme <info@rextheme.com>
 */
class Rex_Product_Feed_Rules {
    /**
     * The Feed Attributes.
     *
     * @since    1.1.10
     * @access   protected
     * @var      Rex_Product_Filter    attributes    Feed Attributes.
     */
    protected $product_meta_keys;

    /**
     * The Feed Attributes.
     *
     * @access   protected
     * @var      Rex_Product_Filter    attributes    Feed Attributes.
     */
    protected $product_rule_meta_keys;


    /**
     * The Feed Condition.
     *
     * @since    1.1.10
     * @access   protected
     * @var      Rex_Product_Filter    condition    Feed Condition.
     */
    protected $condition;


    /**
     * The Feed Condition Then.
     *
     * @since    1.1.10
     * @access   protected
     * @var      Rex_Product_Filter    then    Feed Condition Then.
     */
    protected $then;


    /**
     * The Feed Rules
     *
     * @since    3.5
     * @access   protected
     * @var      Rex_Product_Filter    then    Feed Condition Then.
     */
    protected $rules;


    /**
     * The Feed Filter Mappings Attributes and associated value and other constraints.
     *
     * @since    1.1.10
     * @access   protected
     * @var      Rex_Product_Filter    rule_mappings    Feed Filter mapping for template generation.
     */
    protected $rule_mappings;


    /**
     * Set the rules and condition.
     *
     * @since    1.1.10
     * @param array $feed_rules
     */
    public function __construct( $feed_rules = array() ){
        $this->init_feed_rule_mappings( $feed_rules );
        $this->init_product_meta_keys();
        $this->init_product_rule_conditions();
    }


    /**
     * Initialize Filter from feed post_meta.
     *
     * @since    1.1.10
     * @param array $feed_rules The Conditions Of Feeds
     */
    protected function init_feed_rule_mappings( $feed_rules ){
        $this->rule_mappings = !empty($feed_rules) ? $feed_rules : $this->init_default_rule_mappings();
    }

    /**
     * Get Filter Attributes
     * @return array $attributes
     */
    protected function getFilterAttribute () {
        return array(
            'Primary Attributes'        => array(
                'id'                        => 'Product Id',
                'title'                     => 'Product Title',
                'description'               => 'Product Description',
                'short_description'         => 'Product Short Description',
                'total_sales'               => 'Total Sales',
                'featured_image'            => 'Featured Image',
                'product_cats'              => 'Product Categories',
                'sku'                       => 'SKU',
                'availability'              => 'Availability',
                'quantity'                  => 'Quantity',
                'price'                     => 'Reguler Price',
                'sale_price'                => 'Sale price',
                'weight'                    => 'Weight',
                'width'                     => 'Width',
                'height'                    => 'Height',
                'length'                    => 'Length',
                'rating_total'              => 'Total Rating',
                'rating_average'            => 'Average Rating',
                'product_tags'              => 'Tags',
                'sale_price_dates_from'     => 'Sale Start Date',
                'sale_price_dates_to'       => 'Sale End Date',
                'manufacturer'              => 'Manufacturer',
            ),
        );
    }


    /**
     * Initialize Product Meta Attributes
     *
     * @since    1.1.10
     */
    protected function init_product_meta_keys() {
        $this->product_meta_keys   = $this->getFilterAttribute();
        $product_attributes        = self::get_product_attributes();

        $this->product_meta_keys = array_merge( $this->product_meta_keys, $product_attributes );

        $this->product_rule_meta_keys = Rex_Feed_Attributes::get_attributes();

        if( isset( $this->product_rule_meta_keys[ 'Attributes Separator' ] ) ) {
            unset( $this->product_rule_meta_keys[ 'Attributes Separator' ] );
        }
    }


    /**
     * Initialize Product Filter Condition
     *
     * @since    1.1.10
     */
    protected function init_product_rule_conditions() {
        $this->condition = array(
            '' => array(
                'find_and_replace'   => __( 'Find & Replace', 'rex-product-feed-pro' ),
                'contain'            => __( 'Contains', 'rex-product-feed-pro' ),
                'dn_contain'         => __( 'Does not contain', 'rex-product-feed-pro' ),
                'equal_to'           => __( 'Is equal to', 'rex-product-feed-pro' ),
                'nequal_to'          => __( 'Is not equal to', 'rex-product-feed-pro' ),
                'greater_than'       => __( 'Greater than', 'rex-product-feed-pro' ),
                'greater_than_equal' => __( 'Greater than or equal to', 'rex-product-feed-pro' ),
                'less_than'          => __( 'Less than', 'rex-product-feed-pro' ),
                'less_than_equal'    => __( 'Less than or equal to', 'rex-product-feed-pro' ),
                'any'                => __( 'Is Any', 'rex-product-feed-pro' ),
            )
        );
    }


    /**
     * Initialize Default Filter Mappings with Attributes.
     *
     * @since    1.1.10
     */
    protected function init_default_rule_mappings(){
        return array(
            array(
                'rules_if'        => '',
                'rules_condition' => '',
                'value'     => '',
                'rules_then'      => '',
            ),
        );
    }


    /**
     * Return the rule_mappings
     *
     * @since    1.1.10
     */
    public function getFilterMappings(){
        return $this->rule_mappings;
    }


    /**
     * Print attributes as select dropdown.
     *
     * @since    1.0.0
     * @param $key
     * @param $name
     * @param string $selected
     */
    public function printSelectDropdown( $key, $name, $name_prefix = 'ff', $selected = '', $class = '', $style = '' ){

        if ( $name === 'rules_if' || $name === 'rules_then' ) {
            $items = $this->product_rule_meta_keys;
        }
        elseif ( $name === 'rules_replace' ) {
            $items = $this->product_rule_meta_keys;
        }
        elseif ( $name === 'rules_condition' ) {
            $items = $this->condition;
        }
        else{
            return;
        }

        echo '<select class="' .esc_attr( $class ). '" name="'.esc_attr( $name_prefix ).'['.esc_attr( $key ).'][' . esc_attr( $name ) . ']" style="' . esc_attr( $style ) . '">';
        if($name === 'rules')
            echo "<option value='or'>Please Select</option>";
        else
            echo "<option value=''>Please Select</option>";



        foreach ($items as $groupLabel => $group) {
            if ( !empty($groupLabel)) {
                echo "<optgroup label='".esc_html($groupLabel)."'>";
            }

            foreach ($group as $key => $item) {
                if ( $selected == $key ) {
                    echo "<option value='".esc_attr($key)."' selected='selected'>".esc_html($item)."</option>";
                }else{
                    echo "<option value='".esc_attr($key)."'>".esc_html($item)."</option>";
                }
            }

            if ( !empty($groupLabel)) {
                echo "</optgroup>";
            }
        }

        echo "</select>";
    }


    /**
     * Print Prefix input.
     *
     * @since    1.0.0
     * @param $key
     * @param string $name
     * @param string $val
     */
    public function printInput( $key, $name, $name_prefix = 'ff', $val = '', $class = '', $style = '' ){
        echo '<input type="text" class="'. esc_attr( $class ) .'" name="'.esc_html( $name_prefix ).'['.esc_attr( $key ).'][' . esc_attr( $name ) . ']" value="' . esc_attr( $val ) . '" style="' . esc_attr( $style ) . '">';
    }


    /**
     * @desc Gets WooCommerce product attributes [Global]
     * @since 7.2.18
     * @return array
     */
    protected static function get_product_attributes() {
        $taxonomies = wpfm_get_cached_data( 'product_attributes_custom_filter' );
        if( !is_array( $taxonomies ) && empty( $taxonomies ) ) {
            $taxonomies = [];
            $product_attributes = wc_get_attribute_taxonomies();

            if( is_array( $product_attributes ) && !empty( $product_attributes ) ) {
                foreach( $product_attributes as $attribute ) {
                    if( isset( $attribute->attribute_name, $attribute->attribute_label ) && $attribute->attribute_name && $attribute->attribute_label ) {
                        $taxonomies[ 'Product Attributes' ][ 'pa_' . $attribute->attribute_name ] = $attribute->attribute_label;
                    }
                }
            }
            wpfm_set_cached_data( 'product_attributes_custom_filter', $taxonomies );
        }
        return $taxonomies;
    }
}
