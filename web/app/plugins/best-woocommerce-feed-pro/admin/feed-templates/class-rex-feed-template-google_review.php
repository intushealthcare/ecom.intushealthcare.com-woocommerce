<?php

/**
 *
 * Defines the attributes and template for Google Review feed.
 *
 * @package    Rex_Product_Feed
 * @subpackage Rex_Product_Feed/admin/feed-templates/Rex_Feed_Template_Google_Review
 * @author     RexTheme <info@rextheme.com>
 */
class Rex_Feed_Template_Google_review extends Rex_Feed_Abstract_Template
{

    protected function init_atts()
    {
        $this->attributes = array(
            'Required Information' => array(
                'product_name' => 'Product Name',
                'product_url' => 'Product URL',
                'gtins' => 'GTIN',
                'mpns' => 'MPN',
                'skus' => 'SKU',
                'brands' => 'Brands',
                'review_url' => 'Review URL',
                
            ),
            'Additional Information'        => array(
                'pros'              => 'Pros',
                'cons'              => 'Cons',
                'reviewer_images'   => 'Reviewer Images',
            ),
        );
    }

    protected function init_default_template_mappings()
    {
        $this->template_mappings = array(
            array(
                'attr' => 'product_name',
                'type' => 'meta',
                'meta_key' => 'title',
                'st_value' => '',
                'prefix' => '',
                'suffix' => '',
                'escape' => 'default',
                'limit' => 0
            ),
            array(
                'attr' => 'product_url',
                'type' => 'meta',
                'meta_key' => 'link',
                'st_value' => '',
                'prefix' => '',
                'suffix' => '',
                'escape' => 'default',
                'limit' => 0
            ),
            array(
                'attr' => 'gtins',
                'type' => 'static',
                'meta_key' => '',
                'st_value' => '',
                'prefix' => '',
                'suffix' => '',
                'escape' => 'default',
                'limit' => 0
            ),
            array(
                'attr' => 'mpns',
                'type' => 'static',
                'meta_key' => '',
                'st_value' => '',
                'prefix' => '',
                'suffix' => '',
                'escape' => 'default',
                'limit' => 0
            ),
            array(
                'attr' => 'skus',
                'type' => 'meta',
                'meta_key' => 'sku',
                'st_value' => '',
                'prefix' => '',
                'suffix' => '',
                'escape' => 'default',
                'limit' => 0
            ),
            array(
                'attr' => 'brands',
                'type' => 'static',
                'meta_key' => '',
                'st_value' => '',
                'prefix' => '',
                'suffix' => '',
                'escape' => 'default',
                'limit' => 0
            ),
            array(
                'attr' => 'review_url',
                'type' => 'meta',
                'meta_key' => 'review_url',
                'st_value' => '',
                'prefix' => '',
                'suffix' => '',
                'escape' => 'default',
                'limit' => 0
            )
        );
    }

}