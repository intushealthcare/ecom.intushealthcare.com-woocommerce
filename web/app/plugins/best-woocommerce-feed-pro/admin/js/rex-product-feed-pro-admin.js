(function ($) {
    'use strict';

    /**
     * All of the code for your admin-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
     *
     * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
     *
     * });
     *
     * ...and/or other possibilities.
     *
     * Ideally, it is not considered best practise to attach more than a
     * single DOM-ready or window-load handler for a particular page.
     * Although scripts in the WordPress core, Plugins and Themes may be
     * practising this, we should strive to set a better example in our own work.
     */


    $(document).on('ready', function (event) {
        rex_feed_load_feed_rules(event);
    });

    /**
     * add new rule
     */
    $( document ).on( 'click', '#rex-new-rules', function () {
        let rowId = $( this ).siblings( '#rex-feed-config-rules .rex__filter-table' ).children( '#config-table' ).find( 'tbody tr' ).last().attr( 'data-row-id' );
        rowId = parseInt( rowId ) + 1;
        let lastrow = $( this ).siblings( '#rex-feed-config-rules .rex__filter-table' ).children( '#config-table' ).find( 'tbody tr:last' );

        $(this).siblings('#rex-feed-config-rules .rex__filter-table').children('#config-table').find('tbody tr:first')
            .clone()
            .insertAfter(lastrow)
            .attr('data-row-id', rowId)
            .show();

        let $row = $( this ).siblings( '#rex-feed-config-rules .rex__filter-table' ).children( '#config-table' ).find( "[data-row-id='" + rowId + "']" );
        $row.find( '#rex-feed-config-rules ul.dropdown-content.select-dropdown, .caret, .select-dropdown ' ).remove();
        updateFormNameAtts( $row, rowId, true );

        $row.find( 'select' ).select2();
    } );

    /**
     * add new custom rule
     */
    $(document).on('click', '#rex-new-custom-rules', function () {
        var rowId = $(this).siblings('#rex-feed-config-rules .rex__filter-table').children('#config-table').find('tbody tr').last().attr('data-row-id');
        rowId = parseInt(rowId) + 1;
        var lastrow = $(this).siblings('#rex-feed-config-rules .rex__filter-table').children('#config-table').find('tbody tr:last');

        $(this).siblings('#rex-feed-config-rules .rex__filter-table').children('#config-table').find('tbody tr:first')
            .clone()
            .insertAfter(lastrow)
            .attr('data-row-id', rowId)
            .show();

        var $row = $(this).siblings('#rex-feed-config-rules .rex__filter-table').children('#config-table').find("[data-row-id='" + rowId + "']");
        $row.find('#rex-feed-config-rules ul.dropdown-content.select-dropdown, .caret, .select-dropdown ').remove();


        $row.find( 'td:eq(0)' ).empty();
        $row.find( 'td:eq(0)' ).append( '<input type="text" name="fr['+rowId+'][cust_rules_if]" value="">' );
        updateFormNameAtts( $row, rowId, true );

        $row.find( 'select' ).select2();
    } );


    /**
     * Function for updating select and input box name
     * attribute under a table-row.
     */
    function updateFormNameAtts($row, rowId, filter) {
        let name, $el, id, labelFor;
        $el = $row.find('input, select, label');
        $el.each(function (index, item) {
            name = $(item).attr('name');
            if ($(item).parent().hasClass('static-input')) {
                $(item).parent().hide();
            }
            if ( undefined !== name ) {
                // get new name via regex
                if (filter) {
                    name = name.replace(/^ff\[\d+\]/, 'ff[' + rowId + ']');
                    name = name.replace(/^fr\[\d+\]/, 'fr[' + rowId + ']');
                    $(item).attr('name', name);
                } else {
                    name = name.replace(/^fc\[\d+\]/, 'fc[' + rowId + ']');
                    $(item).attr('name', name);
                }
            }
            id = $(item).attr('id');
            if ( undefined !== id && -1 !== id.search('rex_feed_rule_static_') ) {
                $(item).attr('id', 'rex_feed_rule_static_' + rowId);
            }
            labelFor = $(item).attr('for');
            if ( undefined !== labelFor && -1 !== labelFor.search('rex_feed_rule_static_') ) {
                $(item).attr('for', 'rex_feed_rule_static_' + rowId);
            }
        });
    }


    /*
    ** Custom field
     */
    function product_custom_field_settings() {
        var payload = {};
        if ($('#rex-product-custom-field').is(":checked")) {
            payload = {
                security: wpfmProObj.ajax_nonce,
                custom_field: 'yes',
            };
        } else {
            payload = {
                security: wpfmProObj.ajax_nonce,
                custom_field: 'no',
            };
        }
        wpAjaxHelperRequest('rex-product-custom-field', payload)
            .success(function (response) {
                if (response.data === 'yes') {
                    $('.wpfm-custom-field-frontend').removeClass('is-hidden');
                } else {
                    $('.wpfm-custom-field-frontend').addClass('is-hidden');
                }
                console.log('Woohoo!');
            })
            .error(function (response) {
                console.log('Uh, oh!');
                console.log(response.statusText);
            });
    }

    $(document).on('change', '#rex-product-custom-field', product_custom_field_settings);


    /**
     * Custom product attributes field
     */
    function product_pa_settings() {
        var payload = {};
        if ($('#rex-product-pa-field').is(":checked")) {
            payload = {
                pa_field: 'yes',
            };
        } else {
            payload = {
                pa_field: 'no',
            };
        }
        wpAjaxHelperRequest('rex-product-pa-field', payload)
            .success(function (response) {
                console.log('Woohoo!');
            })
            .error(function (response) {
                console.log('Uh, oh!');
                console.log(response.statusText);
            });
    }

    $(document).on('change', '#rex-product-pa-field', product_pa_settings);


    /*
     ** Custom field
	 */
    function product_structured_data_settings() {
        var payload = {};
        if ($('#rex-product-structured-data').is(":checked")) {
            payload = {
                structured_data: 'yes',
            };
        } else {
            payload = {
                structured_data: 'no',
            };
        }
        wpAjaxHelperRequest('rex-product-structured-data', payload)
            .success(function (response) {
                console.log('Woohoo!');
            })
            .error(function (response) {
                console.log('Uh, oh!');
                console.log(response.statusText);
            });
    }

    $(document).on('change', '#rex-product-structured-data', product_structured_data_settings);


    /**
     * Exclude tax ajax
     */
    function exclude_tax() {
        var payload = {};
        if ($('#rex-product-exclude-tax').is(":checked")) {
            payload = {
                exclude_tax: 'yes',
            };
        } else {
            payload = {
                exclude_tax: 'no',
            };
        }
        wpAjaxHelperRequest('rex-product-exclude-tax', payload)
            .success(function (response) {
                console.log('Woohoo!');
            })
            .error(function (response) {
                console.log('Uh, oh!');
                console.log(response.statusText);
            });
    }

    $(document).on('change', '#rex-product-exclude-tax', exclude_tax);


    /**
     * Enable/disable drm pixel
     * @param event
     */
    function enable_drm_pixel(event) {
        event.preventDefault();
        var payload = {};
        if ($(this).is(":checked")) {
            payload = {
                wpfm_drm_pixel_enabled: 'yes',
            };
        } else {
            payload = {
                wpfm_drm_pixel_enabled: 'no',
            };
        }
        wpAjaxHelperRequest('wpfm-enable-drm-pixel', payload)
            .success(function (response) {
                if (response.data == 'enabled') {
                    $('.wpfm-drm-pixel-field').removeClass('is-hidden');
                } else {
                    $('.wpfm-drm-pixel-field').addClass('is-hidden');
                }

            })
            .error(function (response) {
                console.log('Uh, oh!');
                console.log(response.statusText);
            });
    }

    $(document).on('change', '#wpfm_drm_pixel', enable_drm_pixel);

    /**
     * Save DRM pixel ID
     * @param e
     */
    function save_drm_pixel_id(e) {
        e.preventDefault();
        var $form = $(this);
        $form.find("button.save-drm-pixel span").text("");
        $form.find("button.save-drm-pixel i").show();
        var value = $form.find('#wpfm_drm_pixel').val();
        wpAjaxHelperRequest('save-drm-pixel-value', value)
            .success(function (response) {
                $form.find("button.save-drm-pixel i").hide();
                $form.find("button.save-drm-pixel span").text("saved");
                setTimeout(function () {
                    $form.find("button.save-drm-pixel span").text("save");
                }, 1000);
                console.log('woohoo!');
            })
            .error(function (response) {
                $form.find("button.save-drm-pixel i").hide();
                $form.find("button.save-drm-pixel span").text("failed");
                setTimeout(function () {
                    $form.find("button.save-drm-pixel span").text("save");
                }, 1000);
                console.log('uh, oh!');
                console.log(response.statusText);
            });
    }

    $(document).on("submit", "#wpfm-drm-pixel", save_drm_pixel_id);


    /**
     * Enable amazon fields
     */
    function wpfm_amazon_custom_fields() {
        var payload = {};
        if ($(this).is(":checked")) {
            payload = {
                enable_amazon_fields: 'yes',
            };
        } else {
            payload = {
                enable_amazon_fields: 'no',
            };
        }
        wpAjaxHelperRequest('rex-amazon-fields', payload)
            .success(function (response) {
                console.log('Woohoo!');
            })
            .error(function (response) {
                console.log('Uh, oh!');
                console.log(response.statusText);
            });
    }

    $(document).on('change', '#rex-product-custom-field-amazon', wpfm_amazon_custom_fields);


    $('.wpfm-product-search').select2({
        placeholder: "Select your products",
        minimumInputLength: 3,
        allowClear: true,
        ajax: {
            url: wpfmProObj.ajax_url,
            data: function (params) {
                return {
                    term: params.term,
                    action: 'woocommerce_json_search_products_and_variations',
                    security: $(this).attr('data-security'),
                };
            }, processResults: function (data) {
                var terms = [];
                if (data) {
                    $.each(data, function (id, text) {
                        terms.push({id: id, text: text});
                    });
                }
                return {results: terms};
            }, cache: true
        }
    });


    $(document).on('click', '.rex-feed-rule-static', wpfm_show_manage_rules_replace_field);

    function wpfm_show_manage_rules_replace_field() {
        let check_box = $( this ).is( ':checked' );

        if ( check_box ) {
            $( this ).parent().next().children('.rules_dropdown_replace').hide();
            $( this ).parent().next().children('span.select2-container').remove();
            $( this ).parent().next().children( '.rules_static_replace' ).show();
        }
        else {
            $( this ).parent().next().children( '.rules_static_replace' ).hide();
            $( this ).parent().next().children('.rules_dropdown_replace').show().select2();
        }
    }

    /**
     * Event listener for Attribute type change functionality.
     */
    $( document ).on( 'change', 'select.type-dropdown', function () {
        let selected = $( this ).find( 'option:selected' ).val();
        $( this ).parent().parent().removeClass( 'combined-field-edit' );

        if ( selected === 'static' ) {
            $( this ).closest( 'td' ).next( 'td' ).find( '.meta-dropdown' ).hide();
            $( this ).closest( 'td' ).next( 'td' ).find( '.combined-dropdown' ).hide();
            $( this ).closest( 'td' ).next( 'td' ).find( '.static-input' ).show();
        }
        else if ( selected === 'combined' ) {
            $( this ).parent().next( 'td[data-title="Value : "]' ).children( 'div.meta-dropdown' ).children( 'select' ).find( 'optgroup[label="Attributes Separator"]' ).show();
            $( this ).closest( 'td' ).next( 'td' ).find( '.static-input' ).hide();
            $( this ).closest( 'td' ).next( 'td' ).find( '.meta-dropdown' ).hide();
            $( this ).closest( 'td' ).next( 'td' ).find( '.combined-dropdown' ).show().children('select').select2();
            $( this ).closest( 'td' ).next( 'td' ).find( '.combined-dropdown' ).children('input').removeClass( 'disable-custom-id-dropdown' ).removeAttr( 'readonly' );

            $( this ).parent().parent().addClass( 'combined-field-edit' );

            let combined_keys = $( this ).parent().next( 'td[data-title="Value : "]' ).children( 'div.combined-dropdown' ).children( 'input' ).val();

            if ( combined_keys === '' ) {
                let meta_mey = $( this ).parent().next( 'td[data-title="Value : "]' ).children( 'div.meta-dropdown' ).children( 'select' ).find( 'option:selected' ).val();
                $( this ).parent().next( 'td[data-title="Value : "]' ).children( 'div.combined-dropdown' ).children( 'input' ).val( '{' + meta_mey + '}' );
            }
        } else {
            $(this).parent().next('td[data-title="Value : "]').children('div.meta-dropdown').children('select').find('optgroup[label="Attributes Separator"]').hide();
            $(this).closest('td').next('td').find('.static-input').hide();
            $(this).closest('td').next('td').find('.combined-dropdown').hide();
            $(this).closest('td').next('td').find('.meta-dropdown').show();
        }
    });

    /**
     * Event listener for combined field
     */
    $(document).on('change', 'select.combined-attr-val-dropdown', function () {
        const $this = this;
        let val = $.trim($($this).find('option:selected').val());
        let input_val = $.trim($($this).prev('input').val());
        let separators = ['-', ':', ';', ',', '+'];

        if ($.inArray(val, separators) === -1) {
            val = '{' + val + '}';
        }

        $(this).prev('input').val(input_val + ' ' + val);

        setTimeout(function () {
            $($this).find('option:selected').removeAttr('selected').trigger( 'change.select2' );
        }, 700)
    });

    $(document).on("submit", "form#wpfm-user-email", rex_feed_save_user_email);

    /**
     * @desc save user/admin email in settings dashboard
     * @since 6.2.1
     * @param event
     */
    function rex_feed_save_user_email(event) {
        event.preventDefault();

        let form = $(this);
        let email = $('input[name=wpfm_user_email]').val();

        $(this).find('button.save-user-email').find('span').hide();
        $(this).find('button.save-user-email').find('i').show();

        wpAjaxHelperRequest('rex-feed-save-email', email)
            .success(function (response) {
                $(form).find('button.save-user-email').find('i').hide();
                $(form).find('button.save-user-email').find('span').show();
            })
            .error(function (response) {
                console.log('Uh, oh! Not Awesome!!');
            });
    }

    $(document).on("submit", "form#rex_feed_import_feed_form", rex_feed_save_imported_feed);

    /**
     * @desc Import button action process
     * @since 6.3.0
     * @param event
     */
    function rex_feed_save_imported_feed(event) {
        event.preventDefault();

        let file_data = $(this)[0];
        let form_data = new FormData(file_data);
        let $import_button = $('button#rex_feed_import_feed_btn');

        form_data.append("action", 'rex_feed_save_import_xml_feed');
        form_data.append("security", wpfmProObj.ajax_nonce);
        $import_button.find('span').hide();
        $import_button.find('i').show();

        if ('form_data') {
            $.ajax({
                type: "POST",
                url: wpfmProObj.ajax_url,
                data: form_data,
                dataType: 'JSON',
                processData: false,
                contentType: false,
                cache: false,

                success: function (response) {
                    $import_button.find('i').hide();
                    $import_button.find('span').show();
                    $('input#rex_feed_import_feed').val('');

                    if (response.success) {
                        $('#rex_feed_export_import_message').text(response.data.feeds + ' Feeds Imported Successfully!');
                        $('#rex_feed_export_import_message').css('padding-top', '5px');


                        setTimeout(function () {
                            $('#rex_feed_export_import_message').text('');

                        }, 2000);
                    } else if ('undefined' !== typeof (response.data.message)) {
                        $('#rex_feed_export_import_message').text(response.data.message);

                        setTimeout(function () {
                            $('#rex_feed_export_import_message').text('');
                        }, 2000);
                    }
                },
                error: function (response) {
                    console.log('Oh! Not Okay');
                }
            });
        }
    }


    /**
     * @desc Export the selected feeds on clicking `Export Now` button
     * @since 6.3.2
     */
    $(document).on("click", "a#rex_feed_export_now_button", rex_feed_export_feed);

    /**
     * @desc Export button action process
     * @since 6.3.0
     * @param event
     */
    function rex_feed_export_feed(event) {
        event.preventDefault();
        let feed_ids = $('input[name="rex_feed_export_feeds[]"]:checked').map(function () {
            return $(this).val();
        }).toArray();
        let $export_button = $('a#rex_feed_export_now_button');
        $export_button.find('span').hide();
        $export_button.find('i').show();

        wpAjaxHelperRequest('rex-feed-export-feed', feed_ids)
            .success(function (response) {
                if (response.success && response.data.status) {
                    $export_button.find('i').hide();
                    $export_button.find('span').show();
                    $('section.rex-export-popup').fadeOut();
                    $('a#rex-feed-download-exported-feed-url').attr('href', response.data.feed_path);
                    $('a#rex-feed-download-exported-feed-url')[0].click();
                    $('a#rex-feed-download-exported-feed-url').removeAttr('href');
                } else {
                    $('.rex-export-popup__notice-area').fadeIn();
                    setTimeout(function () {
                        $('.rex-export-popup__notice-area').fadeOut();
                        $export_button.find('i').hide();
                        $export_button.find('span').show();
                    }, 2000);
                }
            })
            .error(function (response) {
                console.log('Uh, oh! Not Awesome!!');
            });
    }


    /**
     * @desc Save plugin license key clicking
     * on activate/deactivate button
     * @since 7.2.18
     */
    $(document).on('click', '#wpfm_pro_license_deactivate, #wpfm_pro_license_activate', function () {
        let license = $('input#wpfm_pro_license_key').val();
        wpAjaxHelperRequest('rex-feed-save-pro-license', license)
            .success(function (response) {
                console.log('WOW! Awesome!!');
            })
            .error(function (response) {
                console.log('Uh, oh! Not Awesome!!');
            });
    });

    /**
     * @desc Show export pop-up on export button click
     * @since 6.3.2
     */
    $(document).on("click", "button#rex_feed_export_feed_btn", function (e) {
        $('section.rex-export-popup').fadeIn();
    });


    /**
     * @desc Hides export pop-up on cancel/cross button click
     * @since 6.3.2
     */
    $(document).on("click", "a#rex_feed_export_cancel_btn, span.rex-export-popup__close-btn", function (e) {
        $('section.rex-export-popup').fadeOut();
    });


    /**
     * @desc Checks all feed input fields
     * @since 6.3.2
     */
    $(document).on("change", "input#rex_feed_export_check_all_btn", function (e) {
        if ($(this).is(':checked')) {
            $('input.rex_feed_export_feeds').prop('checked', true)
        } else {
            $('input.rex_feed_export_feeds').prop('checked', false)
        }
    });


    /**
     * @desc  Add click dropdown show
     * @since 6.3.2
     */

    $(document).on("click", ".dropdown-add-row", function (e) {
        $('ul').toggleClass('show-condition');
    });


     /**
     * add new custom add row
     */
     $( document ).on( 'click', '.add-row', function () {
         let parent = $(this).parent().parent();

         if (0 !== $(parent).next().length) {
             const lastSibling = $(parent).siblings(':last');
             if (0 < lastSibling?.length && !$(lastSibling).hasClass('rexfeed-hidded-section')) {
                 parent = lastSibling;
             }
         }

         const parentId = $(parent).attr('data-row-id');
         let newRowId = parseInt(parentId) + 1;

         const new_row = $(parent)
             .siblings('div.flex-table-row:first')
             .clone()
             .insertAfter(parent)
             .attr('data-row-id', newRowId)
             .show()
             .children();

         $(new_row).parent().removeClass('rexfeed-hidded-section');
         $(new_row).parent().siblings( ':first' ).next().children().find( 'a.delete-row' ).show();

         $(new_row)
             .find('select')
             .select2()

         updateFormNameAtts(new_row, newRowId, true);
    } );

     /**
     * Delete a flex-table-row
     * 
     */

    $( document ).on( 'click', 'div.rex-feed-rules-area div.flex-row a.delete-row', function () {
        const parent = $(this).parent().parent();

        if ( 2 === $(parent).siblings()?.length ) {
            $(parent).siblings( ':last' ).children().find( 'a.delete-row' ).hide();
        }

        $(parent).remove();
    } );

    $( document ).on( 'click', '#rex_feed_rules_button', rex_feed_load_feed_rules);

    $( document ).on( 'click', '.rex-feed-rules-area__delete', function() {
        $( this ).parent().parent().parent().fadeOut();
        $( this ).parent().parent().parent().children().find('span.select2-container--default').remove();
        $( '#rex_feed_rules_button' ).show();
        $( this ).siblings( 'div.accordion__list' ).children().find( 'div.flex-table-body' ).empty();
        $( 'input[name="rex_feed_feed_rules_button"]' ).val( 'removed' );
    });

    $( document ).on( 'click', 'input.rex-feed-rule-static', function () {
        if ( $( this ).is( ':checked' ) ) {
            $( this ).parent().parent().next().children( 'span' ).remove();
            $( this ).parent().parent().next().children( 'select' ).hide();
            $( this ).parent().parent().next().children( 'input' ).show();
        }
        else {
            $( this ).parent().parent().next().children( 'input' ).hide();
            $( this ).parent().parent().next().children( 'select' ).show().select2();
        }
    } );

    function rex_feed_get_feed_id() {
        let feed_id = 0;
        let url = window.location.href;

        if ( url.includes( 'post-new.php?post_type=product-feed' ) ) {
            return feed_id;
        }

        url = url.split( '?' );
        url = url[1].split( '&' );

        for (const key in url) {
            if( url[key].search( 'post' ) > -1 ) {
                feed_id = url[key].split( '=' );
                return feed_id[1];
            }
        }
        return feed_id;
    }

    function rex_feed_load_feed_rules( event ) {
        const $this = $(this);
        const feed_id = rex_feed_get_feed_id();
        const event_type = event.type;
        let payload = {
            feed_id: feed_id,
            event: event_type
        };

        wpAjaxHelperRequest( 'rex-feed-handle-feed-rules-content', payload )
            .success( function ( response ) {
                if ( response.status && response.markups ) {
                    $this.hide();
                    $('.rex-feed-rules-area')
                        .fadeIn()
                        .children()
                        .find('div.flex-table-body')
                        .empty()
                        .append( response.markups )
                        .children()
                        .find('select.rules-select2')
                        .select2();

                    if ( 'click' === event_type ) {
                        $('.rex-feed-rules-area')
                            .children()
                            .find('div.accordion')
                            .addClass('accordion__active');
                    }
                    $('input[name="rex_feed_feed_rules_button"]').val('added');

                    if ( 'ready' === event_type ) {
                        localStorage.setItem('rex_feed_form_init_data', $('form').serialize());
                        localStorage.setItem("rex_feed_form_init_filters", $('#rex_feed_product_filters').clone().get(0).outerHTML);
                    }
                }
            } )
            .error( function ( response ) {
                console.log('Failed to load!')
            } );
    }
})(jQuery);
