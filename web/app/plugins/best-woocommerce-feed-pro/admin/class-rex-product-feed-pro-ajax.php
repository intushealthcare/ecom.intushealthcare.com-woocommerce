<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://rextheme.com
 * @since      1.0.0
 *
 * @package    Rex_Product_Metabox
 * @subpackage Rex_Product_Feed/admin
 */


class Rex_Product_Feed_Pro_Ajax {

    /**
     * Hook in ajax handlers.
     *
     * @since    1.0.0
     */
    public static function init() {
        $validations = array(
            'logged_in' => true,
            'user_can'  => 'manage_options',
        );

        // Add custom field to product.
        wp_ajax_helper()->handle( 'rex-product-custom-field' )
                        ->with_callback( array( 'Rex_Product_Feed_Pro_Ajax', 'rex_product_custom_field' ) )
                        ->with_validation( $validations );

        // Add custom product detailed attributes field to product.
        wp_ajax_helper()->handle( 'rex-product-pa-field' )
                        ->with_callback( array( 'Rex_Product_Feed_Pro_Ajax', 'rex_product_pa_field' ) )
                        ->with_validation( $validations );

        // Add Structured data to product.
        wp_ajax_helper()->handle( 'rex-product-structured-data' )
                        ->with_callback( array( 'Rex_Product_Feed_Pro_Ajax', 'rex_product_structured_data' ) )
                        ->with_validation( $validations );

        // Exclude tax to product.
        wp_ajax_helper()->handle( 'rex-product-exclude-tax' )
                        ->with_callback( array( 'Rex_Product_Feed_Pro_Ajax', 'rex_product_exclude_tax' ) )
                        ->with_validation( $validations );

        // Fetch eBay seller category.
        wp_ajax_helper()->handle( 'rex-wpfm-fetch-ebay-category' )
                        ->with_callback( array( 'Rex_Product_Feed_Pro_Ajax', 'rex_product_ebay_category' ) )
                        ->with_validation( $validations );

        wp_ajax_helper()->handle( 'wpfm-enable-drm-pixel' )
                        ->with_callback( array( 'Rex_Product_Feed_Pro_Ajax', 'wpfm_enable_drm_pixel' ) )
                        ->with_validation( $validations );

        wp_ajax_helper()->handle( 'save-drm-pixel-value' )
                        ->with_callback( array( 'Rex_Product_Feed_Pro_Ajax', 'save_drm_pixel_value' ) )
                        ->with_validation( $validations );

        wp_ajax_helper()->handle( 'rex-amazon-fields' )
                        ->with_callback( array( 'Rex_Product_Feed_Pro_Ajax', 'amazon_fields' ) )
                        ->with_validation( $validations );

        wp_ajax_helper()->handle( 'rex-feed-save-email' )
                        ->with_callback( array( 'Rex_Product_Feed_Pro_Ajax', 'rex_feed_save_email' ) )
                        ->with_validation( $validations );

        wp_ajax_helper()->handle( 'rex-feed-export-feed' )
                        ->with_callback( array( 'Rex_Product_Feed_Pro_Ajax', 'rex_feed_export_feed' ) )
                        ->with_validation( $validations );

        wp_ajax_helper()->handle( 'rex-feed-save-pro-license' )
                        ->with_callback( array( 'Rex_Product_Feed_Pro_Ajax', 'rex_feed_save_pro_license' ) )
                        ->with_validation( $validations );

        wp_ajax_helper()->handle( 'rex-feed-handle-feed-rules-content' )
                        ->with_callback( array( 'Rex_Product_Feed_Pro_Ajax', 'rex_feed_get_feed_rules_content' ) )
                        ->with_validation( $validations );
    }


    /**
     * WPFM custom field
     * @param $payload
     */
    public static function rex_product_custom_field($payload) {
        $custom_field = isset( $payload['custom_field'] ) ? $payload['custom_field'] : '';

        if( $custom_field !== '' ) {
            update_option( 'rex-wpfm-product-custom-field', $custom_field );

            if( $custom_field !== 'yes' ) {
                delete_option( 'wpfm_product_custom_fields_frontend' );
            }
            wp_send_json_success( $custom_field );
        }
        else{
            delete_option( 'rex-wpfm-product-custom-field' );
            delete_option( 'wpfm_product_custom_fields_frontend' );
            wp_send_json_error();
        }
        wp_send_json_error();
    }


    /**
     * WPFM custom field
     * @param $payload
     * @return string
     */
    public static function rex_product_pa_field($payload) {
        update_option('rex-wpfm-product-pa-field', $payload['pa_field']);
        return 'success';
    }


    /**
     * WPFM custom field
     * @param $payload
     * @return string
     */
    public static function rex_product_structured_data($payload) {
        update_option('rex-wpfm-product-structured-data', $payload['structured_data']);
        return 'success';
    }


    /**
     * @param $payload
     * @return string
     */
    public static function rex_product_exclude_tax($payload) {
        update_option('rex-wpfm-product-structured-data-exclude-tax', $payload['exclude_tax']);
        return 'success';
    }


    /**
     * eBay category list
     * @param $payload
     * @return array
     */
    public static function rex_product_ebay_category($payload) {
        $matches = array();
        $s = $payload['term'];
        $file = plugin_dir_url(__FILE__).'partials/ebay-seller-category.txt';
        $matches = array();
        $handle = @fopen($file, "r");
        while (!feof($handle)) {
            $cat = fgets($handle);
            $haystack = strtolower($cat);
            $needle = strtolower($s);
            if (strpos($haystack, $needle) !== FALSE)
                $matches[] = $cat;
        }
        fclose($handle);
        return json_encode($matches);
    }


    /**
     * @param $payload
     * @return array
     */
    public static function wpfm_enable_drm_pixel($payload) {
        if($payload['wpfm_drm_pixel_enabled'] == 'yes') {
            update_option('wpfm_drm_pixel_enabled', 'yes');
            return array(
                'success' => true,
                'data'  => 'enabled'
            );
        }else if ($payload['wpfm_drm_pixel_enabled'] == 'no') {
            update_option('wpfm_drm_pixel_enabled', 'no');
            return array(
                'success' => true,
                'data'  => 'disabled'
            );
        }
    }


    /**
     * @param $payload
     * @return array
     */
    public static function save_drm_pixel_value($payload) {
        update_option('wpfm_drm_pixel_value', $payload);
        return array(
            'success' => true,
        );
    }


    /**
     * amazon fields
     * @param $payload
     * @return array
     */
    public static function amazon_fields($payload) {
        if($payload['enable_amazon_fields'] === 'yes') {
            update_option('wpfm_amazon_fields', 'yes');
            return array(
                'success' => true,
                'data'  => 'enabled'
            );
        }
        else if ($payload['enable_amazon_fields'] === 'no') {
            update_option('wpfm_amazon_fields', 'no');
            return array(
                'success' => true,
                'data'  => 'disabled'
            );
        }
    }


    /**
     * @desc save user/admin email in settings dashboard
     * @since 6.2.1
     * @param $email
     * @return void
     */
    public static function rex_feed_save_email( $email ) {
        update_option('wpfm_user_email', $email );
        wp_send_json_success();
        wp_die();
    }


    /**
     * Save imported feeds from xml
     *
     * @return void
     * @since 6.3.0
     */
	public static function rex_feed_save_import_xml_feed() {
		check_ajax_referer( 'rex-wpfm-pro-ajax', 'security' );

		if ( empty( $_FILES[ 'rex_feed_import_feed' ][ 'error' ] ) ) {
			$path    = wp_upload_dir();
			$baseurl = $path[ 'baseurl' ] ?? '';
			$basepath = $path[ 'basedir' ] ?? '';

			$feed_data = [];
			$xml_file = $_FILES[ 'rex_feed_import_feed' ] ?? '';
			$file_name = 'temp_' . $xml_file[ 'name' ] ?? '';
			$folder_path = trailingslashit( $basepath ) . 'rex-feed/';

			// make directory if not exist
			if ( !file_exists( $folder_path ) ) {
				wp_mkdir_p( $folder_path );
			}

			$file_path = $folder_path . $file_name;
			if ( !file_exists( $file_path ) ) {
				move_uploaded_file( $xml_file[ 'tmp_name' ], $file_path );
			}

			$file_url = trailingslashit( $baseurl ) . 'rex-feed/' . $file_name;

			if ( function_exists( 'simplexml_load_file' ) ) {
				$feed_data = simplexml_load_file( $file_url, 'SimpleXMLElement', LIBXML_NOCDATA );
				$feed_data = !$feed_data ? simplexml_load_file( $file_path, 'SimpleXMLElement', LIBXML_NOCDATA ) : $feed_data;
			}

			if ( file_exists( $file_path ) && !empty( $feed_data ) ) {
				$feed_title = !empty( $feed_data->title ) ? (array)$feed_data->title : [];
				$feed_title = $feed_title[ 0 ] ?? '';
				if ( $feed_title === 'Rex Product Feeds' ) {
					$feeds = isset($feed_data->feed) ? $feed_data->feed : [];
					foreach ($feeds as $feed) {
						$feed_title = $feed->feed_title ?? '';
						$feed_meta = !empty($feed->feed_meta) ? (array)$feed->feed_meta : [];

						$args = [
							'post_author' => get_current_user_id(),
							'post_title' => $feed_title . ' - Imported',
							'post_content' => '',
							'post_type' => 'product-feed',
							'post_status' => 'publish'
						];
						$post_id = wp_insert_post($args);

						foreach ($feed_meta as $meta_key => $meta_value) {
							$unserialized = @unserialize($meta_value);
							if ($unserialized) {
								update_post_meta($post_id, $meta_key, $unserialized);
							} else {
								update_post_meta($post_id, $meta_key, $meta_value);
							}
						}

						if (isset($feed->feed_cat_ids)) {
							wp_set_object_terms($post_id, unserialize($feed->feed_cat_ids), 'product_cat');
						}

						if (isset($feed->feed_tag_ids)) {
							wp_set_object_terms($post_id, unserialize($feed->feed_tag_ids), 'product_tag');
						}
					}

					$response = [
						'message' => 'Successfully fetched!',
						'feeds' => sizeof( $feeds )
					];
				}
				else {
					$response = [ 'message' => 'Invalid file!' ];
				}
				unlink( $file_path );
				wp_send_json_success( $response );
				wp_die();
			}
		}

		wp_send_json_error( [ 'message' => 'Select a valid file' ] );
		wp_die();
	}

	private function move_uploaded_file() {

	}


    /**
     * @desc Export all published feeds
     * @since 6.3.0
     * @return void
     */
    public static function rex_feed_export_feed( $feed_ids ) {
        $all_feed_data = [];
        $xml_path = false;

        if( is_array( $feed_ids ) && !empty( $feed_ids ) ) {
            foreach( $feed_ids as $id ) {
                $current_feed_data = [
                    'feed_title' => get_the_title( $id ),
                    'feed_meta'  => get_post_meta( $id )
                ];
                $feed_cats         = get_the_terms( $id, 'product_cat' );
                $cats              = [];

                if( !empty( $feed_cats ) ) {
                    foreach( $feed_cats as $cat ) {
                        if( isset( $cat->term_id ) ) {
                            $cats[] = $cat->term_id;
                        }
                    }
                    $cats = serialize( $cats ); //phpcs:ignore
                }

                $feed_tags = get_the_terms( $id, 'product_tag' );
                $tags      = [];

                if( !empty( $feed_tags ) ) {
                    foreach( $feed_tags as $tag ) {
                        if( isset( $tag->term_id ) ) {
                            $tags[] = $tag->term_id;
                        }
                    }
                    $tags = serialize( $tags ); //phpcs:ignore
                }
                if( !empty( $cats ) ) {
                    $current_feed_data[ 'feed_cat_ids' ] = $cats;
                }
                if( !empty( $tags ) ) {
                    $current_feed_data[ 'feed_tag_ids' ] = $tags;
                }
                $all_feed_data[] = $current_feed_data;
            }
        }

        if( is_array( $all_feed_data ) && !empty( $all_feed_data ) ) {
            $xml      = self::generate_xml( $all_feed_data );
            $xml_path = self::download_xml( $xml );
        }

        if ( $xml_path ) {
            wp_send_json_success( [ 'status' => true, 'feeds' => sizeof( $feed_ids ), 'feed_path' => $xml_path ] );
            wp_die();
        }
        wp_send_json_error( [ 'status' => false ] );
        wp_die();
    }


    /**
     * @desc Generate xml3
     * @since 6.3.0
     * @param $all_feeds
     * @return bool|string
     */
    private static function generate_xml( $all_feeds ) {
        $xml = new SimpleXMLElement('<rex_feeds/>');
        $xml->addChild('title', 'Rex Product Feeds' );

        foreach ( $all_feeds as $feed ) {
            if ( is_array( $feed ) && !empty( $feed ) ) {
                $feed_item = $xml->addChild( 'feed' );
                foreach ( $feed as $node => $feed_data ) {
                    if ( is_array( $feed_data ) && !empty( $feed_data ) ) {
                        $feed_meta = $feed_item->addChild( 'feed_meta' );
                        foreach ( $feed_data as $meta_key => $meta_value ) {
                            $meta_value = isset( $meta_value[ 0 ] ) ? $meta_value[ 0 ] : '';
                            if ( '' === $meta_value ) {
                                continue;
                            }
                            $feed_meta->addChild( $meta_key, $meta_value );
                        }
                    }
                    else {
                        if ( '' === $feed_data ) {
                            continue;
                        }
                        $feed_item->addChild( $node, $feed_data );
                    }
                }
            }
        }

        return $xml->asXML();
    }


    /**
     * @desc Save the xml file in /uploads/rex-feed/ folder
     * and gets the url to download.
     * @since 6.3.0
     * @param $xml
     * @return false|string
     */
    private static function download_xml( $xml ) {
        $path    = wp_upload_dir();
        $baseurl = $path[ 'baseurl' ];
        $basepath = $path[ 'basedir' ];
        $file_path = trailingslashit( $basepath ) . 'rex-feed/' . get_bloginfo( 'name' ) . '-rex-product-feeds-import.xml';
        $file_url = trailingslashit( $baseurl ) . 'rex-feed/' . get_bloginfo( 'name' ) . '-rex-product-feeds-import.xml';
        return file_put_contents( $file_path, $xml ) ? $file_url : false;
    }


    /**
     * Save new license key on
     * activate/deactivate license key button
     *
     * @param string $license License key.
     *
     * @return false[]|true[]
     * @since 7.2.18
     */
    public static function rex_feed_save_pro_license( $license ) {
        $msg = [ 'status' => true ];

        $old = get_option( 'wpfm_pro_license_key', '' );
        $old = ( new Rex_Product_Feed_Pro_License_Security() )->encrypt( $old );

        if( $old && $old === $license ) {
            return $msg;
        }

        if( '' === $license || !is_string( $license ) ) {
            return [ 'status' => false ];
        }

        update_option( 'wpfm_pro_license_key', $license );
        return $msg;
    }

    /**
     * Get feed rules row content
     *
     * @param $payload
     *
     * @return array
     * @since 6.4.1
     */
    public static function rex_feed_get_feed_rules_content( $payload ) {
        $status = 'added';
        if( 'click' !== $payload[ 'event' ] ) {
            $status = get_post_meta( $payload[ 'feed_id' ], '_rex_feed_feed_rules_button', true ) ?: 'added';
        }

        if( !apply_filters('wpfm_is_premium', false) || 'added' !== $status ) {
            return [ 'status' => false, 'markups' => '' ];
        }
        $feed_filter_rules = get_post_meta( $payload[ 'feed_id' ], '_rex_feed_feed_config_rules', true ) ?: get_post_meta( $payload[ 'feed_id' ], 'rex_feed_feed_config_rules', true );
        $feed_filter_rules = new Rex_Product_Feed_Rules( $feed_filter_rules );
        ob_start();
        include_once plugin_dir_path(__FILE__) . '/partials/rex-product-feed-pro-feed-rules-body.php';
        $markups = ob_get_contents();
        ob_end_clean();
        return [ 'status' => true, 'markups' => $markups ];
    }
}
