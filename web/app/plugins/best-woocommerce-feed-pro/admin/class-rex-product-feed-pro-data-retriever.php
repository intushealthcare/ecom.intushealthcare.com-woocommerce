<?php
/**
 * Class for retrieving product data based on user selected feed configuration.
 *
 * Get the product data based on feed config selected by user.
 *
 * @package    Rex_Product_Data_Retriever_Pro
 * @subpackage Rex_Product_Feed/admin
 * @author     RexTheme <info@rextheme.com>
 */
class Rex_Product_Data_Retriever_Pro extends Rex_Product_Data_Retriever {

    /**
     * @desc Set value for a single feed rule.
     * @since    7.3.0
     * @param $rule
     * @return array|mixed|string|string[]|null
     */
    public function set_val( $rule )
    {
        if ( 'combined' === $rule[ 'type' ] && isset( $rule[ 'combined_key' ] ) ) {
            $combined_attr_actual = $rule[ 'combined_key' ];
            $combined_attr = self::get_separated_combined_attr( $rule[ 'combined_key' ] );

            if ( !empty( $combined_attr ) ) {
                foreach ( $combined_attr as $attr ) {
                    $rule[ 'type' ] = 'meta';
                    $rule[ 'meta_key' ] = $attr;
                    $combined_attr_actual = str_replace( '{' . $attr . '}', parent::set_val( $rule ), $combined_attr_actual );
                }
            }
            return $combined_attr_actual;
        }
        else {
            $val = parent::set_val($rule);
        }
        // maybe replace
        return $this->maybe_replaced( $rule, $val, $this->feed_rules );
    }

    /**
     * Replace a string from an attribute
     * @since 7.3.0
     * @param $attr
     * @param $value
     * @param $feed_rules
     * @return array|false|int|mixed|string|string[]|WC_DateTime|NULL
     */
    protected function maybe_replaced( $attr, $value, $feed_rules )
    {
        if( !$this->feed_rules_option ) {
            return $value;
        }
        $price_attrs = [
            'price',
            'current_price',
            'sale_price',
            'price_with_tax',
            'current_price_with_tax',
            'sale_price_with_tax',
            'price_excl_tax',
            'current_price_excl_tax',
            'sale_price_excl_tax',
            'price_db',
            'current_price_db',
            'sale_price_db',
        ];
        $attr_name = !empty( $attr[ 'meta_key' ] ) ? $attr[ 'meta_key' ] : '';
        $attr_name = $attr_name === '' && isset( $attr[ 'attr' ] ) ? $attr[ 'attr' ] : $attr_name;
        $attr_name = $attr_name === '' && isset( $attr[ 'cust_attr' ] ) ? $attr[ 'cust_attr' ] : $attr_name;

        if( is_array( $feed_rules ) && !empty( $feed_rules ) ) {
            foreach( $feed_rules as $rule ) {
                $rule_then = !empty( $rule['rules_then'] ) ? $rule['rules_then'] : '';

                if( $attr_name === $rule_then ) {
                    $rule_if = '';
                    $rule_replace = '';

                    if( isset( $rule['rules_if'] ) && $rule['rules_if'] !== '' ) {
                        $rule_if = $rule['rules_if'];
                    }
                    elseif( isset( $rule['cust_rules_if'] ) && $rule['cust_rules_if'] !== '' ) {
                        $rule_if = $rule['cust_rules_if'];
                    }

                    if( isset( $rule['rules_replace'] ) && !isset( $rule['rules_static'] ) ) {
                        $rule_replace = $rule['rules_replace'];
                        $rule_replace = $this->get_rules_new_value( $rule_replace );
                    }
                    elseif( isset( $rule['rules_static_replace'] ) && isset( $rule['rules_static'] ) && 'on' === $rule['rules_static'] ) {
                        $rule_replace = $rule['rules_static_replace'];
                        if( in_array( $rule_then, $price_attrs, true ) && isset( $rule_replace[0] ) && in_array( $rule_replace[0], [ '+', '-', '*', '/' ] ) ) {
                            $rule_replace = $this->get_dynamic_price( [ 'limit' => $rule_replace ], $value );
                        }
                    }

                    $if_attr_val = $this->get_rules_new_value( $rule_if );
                    $rule_condition = !empty( $rule[ 'rules_condition' ] ) ? $rule[ 'rules_condition' ] : '';
                    $rule_find = !empty( $rule['rules_find'] ) ? $rule['rules_find'] : '';

                    switch( $rule_condition ) {
                        case 'contain':
                            if ( preg_match('/' . $rule_find . '/i', $if_attr_val ) ) {
                                $value = $rule_replace;
                            }
                            break;
                        case 'dn_contain':
                            $str_contains = preg_match( "/$rule_find\b/", $if_attr_val);
                            $value = ! $str_contains ? $rule_replace : $value;
                            break;
                        case 'equal_to':
                            $value = $if_attr_val == $rule_find ? $rule_replace : $value;
                            break;
                        case 'nequal_to':
                            $value = $if_attr_val != $rule_find ? $rule_replace : $value;
                            break;
                        case 'greater_than':
                            $value = $if_attr_val > $rule_find ? $rule_replace : $value;
                            break;
                        case 'greater_than_equal':
                            $value = $if_attr_val >= $rule_find ? $rule_replace : $value;
                            break;
                        case 'less_than':
                            $value = $if_attr_val < $rule_find ? $rule_replace : $value;
                            break;
                        case 'less_than_equal':
                            $value = $if_attr_val <= $rule_find ? $rule_replace : $value;
                            break;
                        case 'find_and_replace':
                            $value = str_replace( $rule_find, $rule_replace, $value );
                            break;
                        default:
                            $value = $rule_replace;
                            break;
                    }
                }
            }
        }
        return $value;
    }


    /**
     * @desc Get new attribute value for feed rules
     * @since 7.3.0
     * @param $type
     * @param $key
     * @return false|int|mixed|string|WC_DateTime|NULL
     */
    protected function get_rules_new_value( $key, $type = 'meta' ) {
        $rule = [
            'type'     => $type,
            'meta_key' => $key
        ];
        return parent::set_val( $rule );
    }


    /**
     * @desc Retrieves dynamic price value
     * @since 7.3.0
     * @param $rule
     * @param $price
     * @return float|string
     */
    public function get_dynamic_price( $rule, $price )
    {
        if( apply_filters( 'wpfm_is_premium', false ) ) {
            $price      = $price === '' ? 0 : $price;
            $expression = !empty( $rule[ 'limit' ] ) ? $rule[ 'limit' ] : '';
            $operators  = preg_split( '([^\\+\\-*\\/\^])', trim( $expression ) );
            $operators  = array_values( array_filter( $operators ) );
            $numbers    = preg_split( '/[^0-9\%]/', trim( $expression ) );
            $numbers    = array_values( array_filter( $numbers ) );
            $i          = 0;

            foreach( $operators as $operator ) {
                switch( $operator ) {
                    case '+':
                        $price += $this->calculate_percentage( $numbers[ $i++ ], $price );
                        break;
                    case '-':
                        $price -= $this->calculate_percentage( $numbers[ $i++ ], $price );
                        break;
                    case '*':
                        $price *= $this->calculate_percentage( $numbers[ $i++ ], $price );
                        break;
                    case '/':
                        $price /= $this->calculate_percentage( $numbers[ $i++ ], $price );
                        break;
                    default:
                        break;
                }
            }
        }

        return $price == 0 ? '' : wc_format_decimal( $price, wc_get_price_decimals() );
    }


    /**
     * Helper function for dynamic price
     *
     * @param string $number Number that may contain special character
     * @param string|int|float $price Product price.
     *
     * @return float
     * @since 7.3.0
     */
    public static function calculate_percentage( $number, $price )
    {
        // Checks if % sign is available and count them.
        $percentage_sign = preg_match_all( "/%/i", $number );
        // Checks for any [except %] special characters.
        $other_special_char = preg_match( "/(?=.*[!@#$^&*(){}\[\]\\\|\"\'\:\:\,<>\.\?\/\"])/i", $number );

        if( $other_special_char || $percentage_sign > 1 ) {
            return 0;
        }

        if( $percentage_sign ) {
            $number = str_replace( '%', '', $number );
            return is_numeric( $number ) ? (float) $price * ( (float) $number / 100.00 ) : $number;
        }
        return is_numeric( $number ) ? (float) $number : 0;
    }


    /**
     * @desc Separates the combined attributes from a string and
     * returns in an array
     * @since 6.2.2
     * @param $attributes
     * @return string[]
     */
    private static function get_separated_combined_attr( $attributes ) {
        $attributes = preg_split('/({|})/', $attributes);
        return array_values( array_filter( $attributes ) );
    }
}