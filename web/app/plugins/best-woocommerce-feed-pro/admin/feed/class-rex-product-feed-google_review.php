<?php

/**
 * The file that generates xml feed for any merchant with custom configuration.
 *
 * A class definition that includes functions used for generating xml feed.
 *
 * @link       https://rextheme.com
 * @since      1.0.0
 *
 * @package    Rex_Product_Feed_Google
 * @subpackage Rex_Product_Feed_Google/includes
 * @author     RexTheme <info@rextheme.com>
 */

use RexTheme\RexGoogleReview\Containers\RexGoogleReview;

class Rex_Product_Feed_Google_review extends Rex_Product_Feed_Abstract_Generator {

	private $feed_merchants = array(
		"nextag" => array(
			'item_wrapper'  => 'product',
			'items_wrapper' => 'products',
		),
	);


	/**
	 * Create Feed
	 *
	 * @return boolean
	 * @author
	 **/
	public function make_feed() {
		RexGoogleReview::$container = null;
		RexGoogleReview::init(
			true,
			$this->setItemWrapper(),
			'http://www.w3.org/2007/XMLSchema-versioning',
			'http://www.w3.org/2001/XMLSchema-instance',
			'http://www.google.com/shopping/reviews/schema/product/2.2/product_reviews.xsd',
			$this->setItemsWrapper()
		);
		RexGoogleReview::version( '2.3' );
		RexGoogleReview::aggregator( $this->title );
		RexGoogleReview::publisher( get_bloginfo( 'name' ) );

		// Generate feed for both simple and variable products.
		$this->generate_product_feed();

		$this->feed = $this->returnFinalProduct();

		if( $this->batch >= $this->tbatch ) {
			$this->save_feed( $this->feed_format );
			return array(
				'msg' => 'finish'
			);
		}
		else {
			return $this->save_feed( $this->feed_format );
		}
	}


	/**
	 * @desc Generate feed object
	 * @return void
	 * @since 5.0.0
	 */
	private function generate_product_feed() {
		$product_meta_keys = Rex_Feed_Attributes::get_attributes();
		$total_products = get_post_meta($this->id, '_rex_feed_total_products', true);
		$total_products = $total_products ?: get_post_meta($this->id, 'rex_feed_total_products', true);
		$simple_products = [];
		$variation_products = [];
		$variable_parent = [];
		$group_products = [];
		$total_products     = $total_products ?: array(
			'total'           => 0,
			'total_reviews'   => 0,
			'simple'          => 0,
			'variable'        => 0,
			'variable_parent' => 0,
			'group'           => 0,
		);

		if( $this->batch == 1 ) {
			$total_products = array(
				'total'           => 0,
				'total_reviews'   => 0,
				'simple'          => 0,
				'variable'        => 0,
				'variable_parent' => 0,
				'group'           => 0,
			);
		}

		$is_review_enabled = get_option( 'woocommerce_enable_reviews', 'no' );

		if( 'yes' === $is_review_enabled ) {
			foreach( $this->products as $productId ) {
				$review_count = 0;
				$product = wc_get_product( $productId );

				if( !is_object( $product ) ) {
					continue;
				}

				if( !$product->is_visible() ) {
					continue;
				}

				if(
					( !$this->include_out_of_stock )
					&& ( !$product->is_in_stock()
						|| $product->is_on_backorder()
						|| ( is_integer( $product->get_stock_quantity() ) && 0 >= $product->get_stock_quantity() )
					)
				) {
					continue;
				}

				// ==============================================================

				if( $product->is_type( 'variable' ) && $product->has_child() ) {
					if( $this->variable_product ) {
						$products = $this->add_to_feed( $product, $product_meta_keys );
						if( !empty( $products[ 'variable_parent' ][ 0 ] ) ) {
							$variable_parent[] = $products[ 'variable_parent' ][ 0 ];
						}
						if( isset( $products[ 'total_reviews' ] ) && $products[ 'total_reviews' ] ) {
							$review_count += (int)$products[ 'total_reviews' ];
						}
					}
					if( 'product_cat' === $this->product_scope || 'product_tag' === $this->product_scope || $this->custom_filter_var_exclude ) {
						if( $this->variations ) {
							if( $this->exclude_hidden_products ) {
								$variations = $product->get_visible_children();
							}
							else {
								$variations = $product->get_children();
							}
							if( $variations ) {
								foreach( $variations as $variation ) {
									$variation_product = wc_get_product( $variation );
									if(
										( !$this->include_out_of_stock )
										&& ( !$variation_product->is_in_stock()
											|| $variation_product->is_on_backorder()
											|| ( is_integer( $variation_product->get_stock_quantity() ) && 0 >= $variation_product->get_stock_quantity() )
										)
									) {
										continue;
									}
									$products = $this->add_to_feed( $variation_product, $product_meta_keys );
									if( isset( $products[ 'total_reviews' ] ) && $products[ 'total_reviews' ] ) {
										$review_count += (int)$products[ 'total_reviews' ];
									}

									if( !empty( $products[ 'variation_products' ][ 0 ] ) ) {
										$variation_products[] = $products[ 'variation_products' ][ 0 ];
									}
								}
							}
						}
					}
				}
				else {
					$products = $this->add_to_feed( $product, $product_meta_keys );
					if( !empty( $products[ 'simple_products' ][ 0 ] ) ) {
						$simple_products[] = $products[ 'simple_products' ][ 0 ];
					}
					if( !empty( $products[ 'group_products' ][ 0 ] ) ) {
						$group_products[] = $products[ 'group_products' ][ 0 ];
					}
					if( !empty( $products[ 'variation_products' ][ 0 ] ) ) {
						$variation_products[] = $products[ 'variation_products' ][ 0 ];
					}
					if( !empty( $products[ 'total_reviews' ] ) ) {
						$review_count += (int)$products[ 'total_reviews' ];
					}
				}
				// ==============================================================
			}
		}

		$total_products = array(
			'total'           => (int)$total_products[ 'total' ] + count( $simple_products ) + count( $variation_products ) + count( $group_products ) + count( $variable_parent ),
			'total_reviews'   => (int)$total_products[ 'total_reviews' ] + ($review_count ?? 0),
			'simple'          => (int)$total_products[ 'simple' ] + count( $simple_products ),
			'variable'        => (int)$total_products[ 'variable' ] + count( $variation_products ),
			'variable_parent' => (int)$total_products[ 'variable_parent' ] + (int)count( $variable_parent ),
			'group'           => (int)$total_products[ 'group' ] + count( $group_products ),
		);

		update_post_meta( $this->id, '_rex_feed_total_products', $total_products );
		if( $this->tbatch === $this->batch ) {
			update_post_meta( $this->id, '_rex_feed_total_products_for_all_feed', $total_products[ 'total' ] );
		}
	}

	/**
	 * @desc Adding items to feed
	 * @param WC_Product $product
	 * @param $meta_keys
	 * @return array[]
	 * @since 6.3.1
	 */
	private function add_to_feed( WC_Product $product, $meta_keys ) {
		$simple_products    = [];
		$variation_products = [];
		$variable_parent    = [];
		$group_products     = [];
		$review_count       = 0;

		if( !is_wp_error( $product ) && $product ) {
			$atts = $this->get_product_data( $product, $meta_keys );

			$out_atts = array();
			if( isset( $atts[ 'pros' ] ) ) {
				$out_atts[ 'pros' ] = $atts[ 'pros' ];
				unset( $atts[ 'pros' ] );
			}
			if( isset( $atts[ 'cons' ] ) ) {
				$out_atts[ 'cons' ] = $atts[ 'cons' ];
				unset( $atts[ 'cons' ] );
			}
			if( isset( $atts[ 'reviewer_images' ] ) ) {
				$out_atts[ 'reviewer_images' ] = $atts[ 'reviewer_images' ];
				unset( $atts[ 'reviewer_images' ] );
			}
			$product_id = 'variation' === $product->get_type() && $product->get_parent_id() ? $product->get_parent_id() : $product->get_id();
			$args       = array(
				'post_type'        => 'product',
				'post_id'          => $product_id,
				'comment_type'     => 'review',
				'comment_approved' => 1,
				'parent'           => 0,
			);

			$pr_reviews     = get_comments( $args );
			$google_reviews = array();
			$image_id       = $product->get_image_id();
			$image_url      = wp_get_attachment_image_url( $image_id, 'full' );

			if( $pr_reviews ) {
				$index               = 0;
				$product_avg_ratings = $product->get_average_rating();
				if( 'simple' === $product->get_type() ) {
					$simple_products[] = $product->get_id();
				}
				elseif( 'variable' === $product->get_type() ) {
					$variable_parent[] = $product->get_id();
				}
				elseif( 'variation' === $product->get_type() ) {
					$variation_products[] = $product->get_id();
					$product_avg_ratings  = 0;
					$parent_product       = $product_id ? wc_get_product( $product_id ) : false;
					if( !is_wp_error( $parent_product ) && $parent_product ) {
						$product_avg_ratings = $parent_product->get_average_rating();
					}
				}
				elseif( 'grouped' === $product->get_type() ) {
					$group_products[] = $product->get_id();
				}

				foreach( $pr_reviews as $review ) {
					if ( !$review->comment_approved ) {
						continue;
					}
					$review_count++;
					$review_id          = !empty( $review->comment_ID ) ? $review->comment_ID : '';
					$review_date        = !empty( $review->comment_date ) ? $review->comment_date : '';
					$review_date        = explode( ' ', $review_date );
					$review_format_date = isset( $review_date[ 0 ] ) ? $review_date[ 0 ] . 'T' : '';
					$review_format_date .= isset( $review_date[ 1 ] ) ? $review_date[ 1 ] . 'Z' : '';
					$google_reviews[]   = array(
						'review_id'        => $review_id,
						'reviewer'         => array(
							'name'        => $this->clean_string( isset( $review->comment_author ) ? $review->comment_author : '' ),
							'reviewer_id' => !empty( $review->user_id ) ? $review->user_id : '',
						),
						'review_timestamp' => $review_format_date,
						'title'            => !empty( $atts[ 'product_name' ] ) ? $atts[ 'product_name' ] : get_the_title( $product ),
						'content'          => !empty( $review->comment_content ) ? $review->comment_content : '',
						'review_url'       => !empty( $atts[ 'review_url' ] ) ? $atts[ 'review_url' ] . "#comment-$review_id" : get_permalink( $product ) . "#comment-$review_id",
						'ratings'          => $product_avg_ratings,
						'reviewer_images'  => $image_url,
					);

					foreach( $out_atts as $key => $value ) {
						$google_reviews[ $index++ ][ $key ] = $value;
					}
					$google_reviews[ $index++ ][ 'product_info' ] = $atts;
				}

				// add all attributes for each product.
				if( is_array( $google_reviews ) && !empty( $google_reviews ) ) {
					foreach( $google_reviews as $review ) {
						$item = RexGoogleReview::createItem();
						if( is_array( $review ) && !empty( $review ) ) {
							foreach( $review as $key => $value ) {
								$item->$key( $value ); // invoke $key as method of $item object.
							}
						}
					}
				}
			}
		}

		return [
			'simple_products'    => $simple_products,
			'variation_products' => $variation_products,
			'variable_parent'    => $variable_parent,
			'group_products'     => $group_products,
			'total_reviews'      => $review_count,
		];
	}


	/**
	 * Check if the merchants is valid or not
	 * @param $feed_merchants
	 * @return bool
	 */
	public function is_valid_merchant() {
		return array_key_exists( $this->merchant, $this->feed_merchants ) ? true : false;
	}


	/**
	 * @return string
	 */
	public function setItemWrapper() {
		return 'reviews';
	}

	public function setItemsWrapper() {
		return 'feed';
	}

	/**
	 * Format string that should contain only lowercase letter with 8 characters limit
	 *
	 * @param string $string String that needs to clean.
	 *
	 * @return string
	 * @since 6.4.2
	 */
	public function clean_string( $string )
	{
		// allow only letters
		$res = preg_replace( "/[^a-zA-Z]/", "", $string );

		// trim what's left to 8 chars
		$res = substr( $res, 0, 8 );

		// make lowercase
		return strtolower( $res );
	}

	/**
	 * Return Feed
	 *
	 * @return array|bool|string
	 */
	public function returnFinalProduct() {
		if( $this->feed_format === 'xml' ) {
			return RexGoogleReview::asRss();
		}
		elseif( $this->feed_format === 'text' ) {
			return RexGoogleReview::asTxt();
		}
		elseif( $this->feed_format === 'csv' ) {
			return RexGoogleReview::asCsv();
		}
		return RexGoogleReview::asRss();
	}

	/**
	 * Replace XML footer
	 *
	 * @return void
	 * @since Unknown
	 */
	public function footer_replace() {
		$this->feed = str_replace( '</reviews></feed>', '', $this->feed );
	}
}