<?php

namespace RexTheme\RexShoppingFeedCustom\EbayMip;
use SimpleXMLElement;

class EbayMipFeed extends \RexTheme\RexShoppingFeed\Feed
{

    /**
     * @var $product_type
     */
    protected $product_type;


    /**
     * @var $localization
     */
    protected $localization;


    protected function setProductType($type) {
        $this->product_type = $type;
    }

    /**
     * @param string $localization
     */
    public function localization($localization){
        $this->localization = (string)$localization;
    }


    /**
     * Adds items to feed
     *
     * @since 6.4.5
     */
    private function addItemsToFeedCSV() {
        if( !empty( $this->items ) ) {
            $this->items_row[] = array_map( 'ucwords', array_keys( end( $this->items )->nodes() ) );
            foreach( $this->items as $item ) {
                $row = [];
                foreach( $item->nodes() as $itemNode ) {
                    if( is_array( $itemNode ) ) {
                        foreach( $itemNode as $node ) {
                            $row[] = str_replace( [ "\r\n", "\n", "\r" ], ' ', $node->get( 'value' ) );
                        }
                    }
                    else {
                        $row[] = str_replace( [ "\r\n", "\n", "\r" ], ' ', $itemNode->get( 'value' ) );
                    }
                }
                $this->items_row[] = $row;
            }
        }
        return $this->items_row;
    }


    /**
     * Generate CSV feed
     *
     * @param bool $output Output data.
     *
     * @return string
     * @since 6.4.5
     */
    public function asCsv( $output = false ) {
        if( ob_get_contents() ) {
            ob_end_clean();
        }
        $data = $this->addItemsToFeedCSV();
        if( $output ) {
            wp_die( $data );
        }
        return $data;
    }
}
