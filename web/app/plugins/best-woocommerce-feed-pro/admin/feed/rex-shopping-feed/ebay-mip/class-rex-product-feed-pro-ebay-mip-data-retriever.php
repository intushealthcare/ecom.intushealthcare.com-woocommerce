<?php

/**
 * Class for retriving product data based on user selected feed configuration.
 *
 * Get the product data based on feed config selected by user.
 *
 * @package    Rex_Product_Ebay_Mip_Data_Retriever
 * @subpackage Rex_Product_Feed/admin
 * @author     RexTheme <info@rextheme.com>
 */
class Rex_Product_Ebay_Mip_Data_Retriever extends Rex_Product_Data_Retriever {


    /**
     * Retrive and setup all data for every feed rules.
     *
     * @since    3.0
     */
    public function set_all_value() {
        $this->data = array();
        $i = 0;

        foreach ($this->feed_rules as $key => $rule) {
            if($rule['type'] === 'ebay_mip_attr') {
                $i += 1;
                $rule['type'] = 'meta';
                $this->data[ 'variation_specific_name_'.$i ] = $this->set_dynamic_attribute_name( $rule['meta_key'] );
                $this->data[ 'variation_specific_value_'.$i ] = $this->set_val( $rule );
            }else {
                if(array_key_exists('attr', $rule)) {
                    if($rule['attr']) {
                        if($rule['attr'] === 'attributes') {
                            $this->data[ $rule['attr']][] = array(
                                'name' => str_replace( 'bwf_attr_pa_', '', $key),
                                'value' => $this->set_val( $rule )
                            );
                        }else {
                            $this->data[ $rule['attr'] ] = $this->set_val( $rule );
                        }
                    }
                }elseif (array_key_exists('cust_attr', $rule)) {
                    if($rule['cust_attr']) {
                        $this->data[ preg_replace('/\s+/', '_', $rule['cust_attr']) ] = $this->set_val( $rule );
                    }
                }else {
                    $this->data[ $rule['attr'] ] = $this->set_val( $rule );
                }
            }
        }
    }


    public function set_dynamic_attribute_name($key) {
        $name = $key;

        if ( $this->is_primary_attr( $key ) ) {
            $name = ucfirst($key);
        }
        elseif ( $this->is_product_attr( $key ) ) {
            $name = ucfirst($key);
        }
        elseif ( $this->is_product_dynamic_attr( $key ) ) {
            $name = ucfirst(str_replace( 'pa_', '', $key));
        }
        elseif ( $this->is_product_custom_attr( $key ) ) {
            $name = str_replace( 'bwf_attr_pa_', '', $key);
        }

        return $name;
    }


    public function get_random_key() {
        return md5(uniqid(rand(), true));
    }
}