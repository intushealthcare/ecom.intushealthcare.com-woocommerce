<?php

namespace RexTheme\RexGoogleReview\Containers;

use RexTheme\RexGoogleReview\Feed;

class RexGoogleReview
{
    /**
     * Feed container
     * @var Feed
     */
    public static $container = null;

    /**
     * Feed namespace
     * @var Feed
     */
    public static $vc = null;

    /**
     * Feed namespace
     * @var Feed
     */
    public static $xsi = null;


    /**
     * Feed namespace
     * @var Feed
     */
    public static $noNamespaceSchemaLocation = null;


    /**
     * Feed rss version
     * @var Feed
     */
    public static $version = '';

    /**
     * Feed products wrapper
     * @var Feed
     */
    public static $wrapper = false;

    /**
     * Feed product item wrapper
     * @var Feed
     */
    public static $itemName = 'item';


    public static $rss = 'rss';



    /**
     * Return feed container
     * @return Feed
     */
    public static function container()
    {
        if (is_null(static::$container)) {
            static::$container = new Feed( static::$wrapper, static::$itemName, static::$vc, static::$xsi , static::$noNamespaceSchemaLocation, static::$rss);
        }
        
        return static::$container;
    }

    /**
     * Init Feed Configuration
     * @return Feed
     */
    public static function init( $wrapper = false, $itemName = 'item', $vc = null, $xsi = null, $noNamespaceSchemaLocation = null, $rss = 'rss')
    {
	    static::$vc                        = $vc;
	    static::$wrapper                   = $wrapper;
	    static::$itemName                  = $itemName;
	    static::$xsi                       = $xsi;
	    static::$noNamespaceSchemaLocation = $noNamespaceSchemaLocation;
	    static::$rss                       = $rss;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return call_user_func_array(array(static::container(), $name), $arguments);
    }
}
