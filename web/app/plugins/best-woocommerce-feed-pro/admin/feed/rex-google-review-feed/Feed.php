<?php

namespace RexTheme\RexGoogleReview;

use SimpleXMLElement;
use RexTheme\RexGoogleReview\Item;
//use Gregwar\Cache\Cache;

class Feed
{

    /**
     * Define Google Namespace url
     * @var string
     */
    protected $namespace = null;

    /**
     * Define Google Namespace url
     * @var string
     */
    protected $vc;

    /**
     * [$version description]
     * @var string
     */
    protected $xsi;


    /**
     * [$version description]
     * @var string
     */
    protected $noNamespaceSchemaLocation;

    /**
     * Stores the list of items for the feed
     * @var Item[]
     */
    protected $items = array();

    /**
     * Stores the list of items for the feed
     * @var Item[]
     */
    protected $items_row = array();

    /**
     * [$wrapper description]
     * @var boolean
     */
    protected $wrapper;

    /**
     * [$channelName description]
     * @var boolean
     */
    protected $channelName;


    /**
     * [$itemName description]
     * @var boolean
     */
    protected $itemName;

    /**
     * [$channelCreated description]
     * @var boolean
     */
    protected $channelCreated = false;

    /**
     * The base for the feed
     * @var SimpleXMLElement
     */
    protected $feed = null;

    /**
     * [$title description]
     * @var string
     */
    protected $title = '';

    /**
     * [$cacheDir description]
     * @var string
     */
    protected $cacheDir = 'cache';

    /**
     * [$description description]
     * @var string
     */
    protected $description = '';

    /**
     * [$link description]
     * @var string
     */
    protected $link = '';


    /**
     * [$datetime]
     * @var string
     */
    protected $datetime = '';

    protected $rss = 'rss';
    
    protected $stand_alone = false;
    
    protected $version;
    protected $publisher;
    protected $reviews;

    /**
     * Feed constructor
     */
    public function __construct($wrapper = false, $itemName = 'item', $vc = null, $xsi = null, $noNamespaceSchemaLocation = null, $rss = 'rss' )
    {
	    $this->vc                        = $vc;
	    $this->wrapper                   = $wrapper;
	    $this->xsi                       = $xsi;
	    $this->noNamespaceSchemaLocation = $noNamespaceSchemaLocation;
	    $this->itemName                  = $itemName;
	    $this->rss                       = $rss;
	    $vc                              = $this->vc && !empty( $this->vc ) ? " xmlns:vc='$this->vc'" : '';
	    $xsi                             = $this->xsi && !empty( $this->xsi ) ? " xmlns:xsi='$this->xsi'" : '';
	    $noNamespaceSchemaLocation       = $this->noNamespaceSchemaLocation && !empty( $this->noNamespaceSchemaLocation ) ? " xsi:noNamespaceSchemaLocation='$this->noNamespaceSchemaLocation'" : '';

	    $this->feed                      = new SimpleXMLElement( "<$rss $vc $xsi $noNamespaceSchemaLocation></$rss>" );
    }

    /**
     * @param string $title
     */
    public function version($version)
    {
        $this->version = (string)$version;
    }


    /**
     * @param string $title
     */
    public function aggregator($title)
    {
        $this->title = (string)$title;
    }

    /**
     * @param string $description
     */
    public function publisher($publisher)
    {
        $this->publisher = (string)$publisher;
    }

    /**
     * @param string $link
     */
    public function link($link)
    {
        $this->link = (string)$link;
    }


    /**
     * @param string $link
     */
    public function datetime($datetime)
    {
        $this->datetime = (string)$datetime;
    }

    /**
     * [channel description]
     */
    private function channel()
    {
	    if ( !$this->channelCreated ) {
		    $channel = $this->channelName ? $this->feed->addChild( $this->channelName ) : $this->feed;
		    ! $this->version ? : $channel->addChild( 'version', $this->version );

		    $aggregator = $channel->addChild( 'aggregator' );
		    $aggregator->addChild( 'name', $this->title );

		    $publisher = $channel->addChild( 'publisher' );
		    $publisher->addChild( 'name', $this->publisher );
		    $publisher->addChild( 'favicon' );

		    $this->channelCreated = true;
	    }
    }

    /**
     * @return Item
     */
    public function createItem()
    {
	    $item                  = new Item( $this->namespace );
	    $index                 = 'index_' . md5( microtime() );
	    $this->items[ $index ] = $item;

	    $item->setIndex( $index );
        
        return $item;
    }

    /**
     * @param int $index
     */
    public function removeItemByIndex($index)
    {
        unset($this->items[$index]);
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function standardiseSizeVarient($value)
    {
        return $value;
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function standardiseColourVarient($value)
    {
        return $value;
    }

    /**
     * @param string $group
     * @return bool|string
     */
    public function isVariant($group)
    {
        if (preg_match("#^\s*colou?rs?\s*$#is", trim($group))) {
            return 'color';
        }
        if (preg_match("#^\s*sizes?\s*$#is", trim($group))) {
            return 'size';
        }
        if (preg_match("#^\s*materials?\s*$#is", trim($group))) {
            return 'material';
        }
        return false;
    }

    /**
     * Adds items to xml feed
     *
     * @since 6.4.4
     */
    private function addItemsToFeed()
    {
	    $this->channel();
	    /** @var SimpleXMLElement $feedItemNode */
	    if ( ! empty( $this->items ) ) {
		    if ( $this->channelName && !empty( $this->channelName ) ) {
			    $reviews = $this->feed->{$this->channelName}->addChild( $this->itemName );
		    }
		    else {
			    $reviews = $this->feed->addChild( $this->itemName );
		    }
	    }

	    foreach ( $this->items as $item ) {
		    $review = $reviews->addChild( 'review' );

		    foreach ( $item->nodes() as $itemNode ) {

			    switch ( $itemNode->get( 'name' ) ) {
				    case 'reviewer':
					    $reviwer = $review->addChild( 'reviewer' );
					    $name = $itemNode->get( 'value' )[ 'name' ] !== '' ? $itemNode->get( 'value' )[ 'name' ] : 'Anonymous';
					    $is_anonymous = $name === 'Anonymous' ? 'true' : 'false';
					    $reviwer_name = $reviwer->addChild( 'name', $name );
					    $reviwer_name->addAttribute( 'is_anonymous', $is_anonymous );
					    $reviwer->addChild( 'reviewer_id', $itemNode->get( 'value' )[ 'reviewer_id' ] );
					    break;
				    case 'review_url':
					    $review_url = $review->addChild( 'review_url', htmlspecialchars( $itemNode->get( 'value' ) ) );
					    $review_url->addAttribute( 'type', 'singleton' );
					    break;
				    case 'ratings':
					    $ratings = $review->addChild( 'ratings' );
					    $overAll = $ratings->addChild( 'overall', $itemNode->get( 'value' ) );
					    $overAll->addAttribute( 'min', '1' );
					    $overAll->addAttribute( 'max', '5' );
					    break;
				    case 'pros':
					    $pros = $review->addChild( 'pros' );
					    $pros->addChild( 'pro', $itemNode->get( 'value' ) );
					    break;
				    case 'cons':
					    $cons = $review->addChild( 'cons' );
					    $cons->addChild( 'con', $itemNode->get( 'value' ) );
					    break;
				    case 'reviewer_images':
					    $reviewer_images = $review->addChild( 'reviewer_images' );
					    $value = $itemNode->get( 'value' );
						if ( is_array( $value ) ) {
							foreach ( $value as $key => $val ) {
								$reviewer_image = $reviewer_images->addChild( 'reviewer_image' );
								$reviewer_image->addChild( 'url', $val );
							}
						}
						else {
							$reviewer_image = $reviewer_images->addChild( 'reviewer_image' );
							$reviewer_image->addChild( 'url', $value );
						}
					    break;
				    case 'product_info':
					    $products    = $review->addChild( 'products' );
					    $product     = $products->addChild( 'product' );
					    $product_ids = $product->addChild( 'product_ids' );
					    $value       = $itemNode->get( 'value' );
					    if ( is_array( $value ) ) {
						    foreach ( $value as $key => $val ) {
							    switch ( $key ) {
								    case 'gtins':
								    	$gtins = $product_ids->addChild( $key );
									    $gtins->addChild( 'gtin', $val );
									    break;
								    case 'mpns':
									    $mpns = $product_ids->addChild( $key );
									    $mpns->addChild( 'mpn', $val );
									    break;
								    case 'skus':
								    	$skus = $product_ids->addChild( $key );
									    $skus->addChild( 'sku', $val );
									    break;
								    case 'brands':
									    $brands = $product_ids->addChild( $key );
									    $brands->addChild( 'brand', $val );
									    break;
								    case 'asins':
									    $asins = $product_ids->addChild( $key );
									    $asins->addChild( 'asin', $val );
									    break;
								    default:
									    break;
							    }
						    }
					    }
                        if( isset( $value[ 'product_name' ] ) ) {
                            $product->addChild( 'product_name', $value[ 'product_name' ] );
                        }
                        if( isset( $value[ 'product_url' ] ) ) {
                            $product->addChild( 'product_url', htmlspecialchars( $value[ 'product_url' ] ) );
                        }
					    break;
				    default:
					    $itemNode->attachNodeTo( $review );
					    break;
			    }
		    }
	    }
    }

    private function addItemsToFeedText() {
        $str = '';
        if(count($this->items)){
            $this->items_row[] = array_keys(end($this->items)->nodes());
            foreach ($this->items as $item) {
                $row = array();
                foreach ($item->nodes() as $itemNode) {
                    if (is_array($itemNode)) {
                        foreach ($itemNode as $node) {
                            $row[] = str_replace(array("\r\n", "\n", "\r"), ' ', $node->get('value'));
                        }
                    } else {
                        $row[] = str_replace(array("\r\n", "\n", "\r"), ' ', $itemNode->get('value'));
                    }
                }
                $this->items_row[] = $row;
            }
            foreach ($this->items_row as $fields) {
                $str .= implode("\t", $fields) . "\n";
            }
        }
        return $str;
    }

    private function addItemsToFeedCSV(){

        if(count($this->items)){

            $this->items_row[] = array_keys(end($this->items)->nodes());
            foreach ($this->items as $item) {
                $row = array();
                foreach ($item->nodes() as $itemNode) {
                    if (is_array($itemNode)) {
                        foreach ($itemNode as $node) {
                            $row[] = str_replace(array("\r\n", "\n", "\r"), ' ', $node->get('value'));
                        }
                    } else {
                        $row[] = str_replace(array("\r\n", "\n", "\r"), ' ', $itemNode->get('value'));
                    }
                }
                $this->items_row[] = $row;
            }
            
            $str = '';
            foreach ($this->items_row as $fields) {
                $str .= implode("\t", $fields) . "\n";
            }
        }

        return $this->items_row;
    }

    /**
     * Retrieve Google product categories from internet and cache the result
     * @return array
     */
    public function categories()
    {
        $cache = new Cache;
        $cache->setCacheDirectory($this->cacheDir);
        $data = $cache->getOrCreate('google-feed-taxonomy.txt', array( 'max-age' => '86400' ), function () {
            return file_get_contents("http://www.google.com/basepages/producttype/taxonomy.en-GB.txt");
        });
        return explode("\n", trim($data));
    }

    /**
     * Build an HTML select containing Google taxonomy categories
     * @param string $selected
     * @return string
     */
    public function categoriesAsSelect($selected = '')
    {
        $categories = $this->categories();
        unset($categories[0]);
        $select = '<select name="google_category">';
        $select .= '<option value="">Please select a Google Category</option>';
        foreach ($categories as $category) {
            $select .= '<option ' . ($category == $selected ? 'selected' : '') . ' name="' . $category . '">' . $category . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    /**
     * Generate RSS feed
     * @param bool $output
     * @param string/bool $merchant
     * @return string
     */
    public function asRss($output = false)
    {
        if( ob_get_contents() ) {
            ob_end_clean();
        }
        $this->addItemsToFeed();

        $data = $this->feed->asXml();
        if ($output) {
            header('Content-Type: application/xml; charset=utf-8');
            die($data);
        }

        return $data;
    }

    /**
     * Generate Txt feed
     * @param bool $output
     * @return string
     */
    public function asTxt($output = false)
    {
        if (ob_get_contents()) ob_end_clean();
        $this->addItemsToFeedText();
        $data = html_entity_decode($this->feed->asXml());
        if ($output) {
            die($data);
        }
        return $data;
    }

    /**
     * Generate CSV feed
     * @param bool $output
     * @return string
     */
    public function asCsv($output = false)
    {

        ob_end_clean();
        $data = $this->addItemsToFeedCSV();
        if ($output) {
            die($data);
        }
        return $data;
    }


    /**
     * Remove last inserted item
     */
    public function removeLastItem()
    {
        array_pop($this->items);
    }
}
