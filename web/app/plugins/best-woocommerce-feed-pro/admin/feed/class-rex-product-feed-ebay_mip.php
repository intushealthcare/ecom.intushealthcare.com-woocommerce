<?php

/**
 * The file that generates xml feed for any merchant with custom configuration.
 *
 * A class definition that includes functions used for generating xml feed.
 *
 * @link       https://rextheme.com
 * @since      1.0.0
 *
 * @package    Rex_Product_Feed_Ebay_mip
 * @subpackage Rex_Product_Feed_Ebay_mip/includes
 * @author     RexTheme <info@rextheme.com>
 */

use RexTheme\RexShoppingFeedCustom\EbayMip\Containers\RexShoppingCustom;


class Rex_Product_Feed_Ebay_mip extends Rex_Product_Feed_Abstract_Generator {


    /**
     * Create Feed
     *
     * @return boolean
     * @author
     **/
    public function make_feed() {

        RexShoppingCustom::$container = null;
        RexShoppingCustom::init(false, 'product', null, '', 'productRequest');

        // Generate feed for both simple and variable products.
        $this->generate_product_feed();

        $this->feed = $this->returnFinalProduct();

        if ($this->batch >= $this->tbatch ) {
            $this->save_feed($this->feed_format);
            return array(
                'msg' => 'finish'
            );
        }else {
            return $this->save_feed($this->feed_format);
        }
    }


    /**
     * Generate product feed object
     *
     * @since 6.4.5
     */
    protected function generate_product_feed() {
        $product_meta_keys  = Rex_Feed_Attributes::get_attributes();
        $simple_products    = [];
        $variation_products = [];
        $variable_parent    = [];
        $group_products     = [];
        $total_products     = get_post_meta( $this->id, '_rex_feed_total_products', true ) ?: get_post_meta( $this->id, 'rex_feed_total_products', true );
        $total_products     = $total_products ?: array(
            'total'           => 0,
            'simple'          => 0,
            'variable'        => 0,
            'variable_parent' => 0,
            'group'           => 0,
        );

        if( $this->batch == 1 ) {
            $total_products = array(
                'total'           => 0,
                'simple'          => 0,
                'variable'        => 0,
                'variable_parent' => 0,
                'group'           => 0,
            );
        }

        foreach( $this->products as $productId ) {
            $product = wc_get_product( $productId );

            if( !is_object( $product ) ) {
                continue;
            }

            if( $this->exclude_hidden_products && !$product->is_visible() ) {
                continue;
            }

            if(
                ( !$this->include_out_of_stock )
                && ( !$product->is_in_stock()
                    || $product->is_on_backorder()
                    || ( is_integer( $product->get_stock_quantity() ) && 0 >= $product->get_stock_quantity() )
                )
            ) {
                continue;
            }

            if( !$this->include_zero_priced ) {
                $product_price = rex_feed_get_product_price( $product );
                if( 0 == $product_price || '' == $product_price ) {
                    continue;
                }
            }

            if( $product->is_type( 'variable' ) && $product->has_child() ) {
                if( $this->variable_product ) {
                    $variable_parent[] = $productId;
                    $variable_product  = new WC_Product_Variable( $productId );
                    $this->add_to_feed( $variable_product, $product_meta_keys, 'variable' );
                }

                if( $this->product_scope === 'product_cat' || $this->product_scope === 'product_tag' || $this->product_scope === 'filter' ) {
                    if( $this->exclude_hidden_products ) {
                        $variations = $product->get_visible_children();
                    }
                    else {
                        $variations = $product->get_children();
                    }
                    if( $variations ) {
                        foreach( $variations as $variation ) {
                            if( $this->variations ) {
                                $variation_products[] = $variation;
                                $variation_product    = wc_get_product( $variation );
                                if(
                                    ( !$this->include_out_of_stock )
                                    && ( !$variation_product->is_in_stock()
                                        || $variation_product->is_on_backorder()
                                        || ( is_integer( $variation_product->get_stock_quantity() ) && 0 >= $variation_product->get_stock_quantity() )
                                    )
                                ) {
                                    continue;
                                }
                                $this->add_to_feed( $product, $product_meta_keys, 'variation' );
                            }
                        }
                    }
                }
            }

            if( $product->is_type( 'simple' ) || $product->is_type( 'external' ) || $product->is_type( 'composite' ) || $product->is_type( 'bundle' ) || $product->is_type( 'woosb' ) ) {
                $simple_products[] = $productId;
                $this->add_to_feed( $product, $product_meta_keys );
            }

            if( ( $this->product_scope === 'all' || $this->product_scope == 'product_filter' || $this->custom_filter_option ) && $product->get_type() == 'variation' ) {
                $variation_products[] = $productId;
                $this->add_to_feed( $product, $product_meta_keys, 'variation' );
            }

            if( $product->is_type( 'grouped' ) && $this->parent_product ) {
                $group_products[] = $productId;
                $this->add_to_feed( $product, $product_meta_keys, 'grouped' );
            }

        }

        $total_products = array(
            'total'           => (int)$total_products[ 'total' ] + (int)count( $simple_products ) + (int)count( $variation_products ) + (int)count( $group_products ) + (int)count( $variable_parent ),
            'simple'          => (int)$total_products[ 'simple' ] + (int)count( $simple_products ),
            'variable'        => (int)$total_products[ 'variable' ] + (int)count( $variation_products ),
            'variable_parent' => (int)$total_products[ 'variable_parent' ] + (int)count( $variable_parent ),
            'group'           => (int)$total_products[ 'group' ] + (int)count( $group_products ),
        );
        update_post_meta( $this->id, '_rex_feed_total_products', $total_products );
        if( $this->tbatch === $this->batch ) {
            update_post_meta( $this->id, '_rex_feed_total_products_for_all_feed', $total_products[ 'total' ] );
        }
    }

    /**
     * Adds product attributes to the feed item for the specified product.
     *
     * @param \WC_Product $product The WooCommerce product object.
     * @param array $meta_keys Array of meta keys containing product data.
     * @param string $product_type Type of product, e.g., 'variation'.
     *
     * @return void
     * @since 6.4.5
     */
    private function add_to_feed( \WC_Product $product, $meta_keys, $product_type = '' ) {
        // Get product attributes and variation attributes if applicable.
        $attributes = $this->get_product_data( $product, $meta_keys );
        $attributes = 'variation' === $product_type ? $this->get_variation_attributes( $product, $attributes ) : $attributes;

        // Create a feed item.
        $item = RexShoppingCustom::createItem();

        if( is_array( $attributes ) && !empty( $attributes ) ) {
            foreach( $attributes as $key => $value ) {
                if( $this->rex_feed_skip_row && $this->feed_format === 'xml' ) {
                    // Skip empty values in XML format if configured.
                    if( $value != '' ) {
                        $item->$key( $value ); // Invoke $key as method of $item object.
                    }
                }
                else {
                    if( 'Variation Group ID' === $key && 'variation' === $product_type ) {
                        $product_id = $product->get_parent_id();
                        $value      = is_numeric( $value ) ? $product_id : get_post_meta( $product_id, '_sku', true );
                    }
                    elseif( preg_match( '/Attribute/i', $key ) ) {
                        $value = '';
                    }
                    $item->$key( $value ); // Invoke $key as method of $item object.
                }
            }
        }
    }

    /**
     * Formats variation attributes for inclusion in the feed attributes.
     *
     * @param WC_Product $product The WooCommerce product object.
     * @param array $feed_attributes Array of attributes for the feed.
     *
     * @return array Formatted variation attributes for the feed.
     * @since 6.4.5
     */
    private function get_variation_attributes( $product, $feed_attributes ) {
        // Get variation attributes from the product.
        $variation_atts = $product->get_variation_attributes();

        $formatted_variation_atts = [];

        // Format variation attributes if they exist.
        if( is_array( $variation_atts ) && !empty( $variation_atts ) ) {
            foreach( $variation_atts as $_key => $_val ) {
                $searchVal                  = [ 'attribute_pa_', 'pa_' ];
                $name                       = str_replace( $searchVal, '', $_key );
                $formatted_variation_atts[] = array(
                    'name'  => ucfirst( $name ),
                    'value' => ucfirst( $_val )
                );
            }
        }

        // Populate feed attributes with formatted variation attributes.
        for( $i = 1; $i <= 5; $i++ ) {
            if( isset( $formatted_variation_atts[ $i - 1 ] ) ) {
                $feed_attributes[ "Variation Specific Name {$i}" ]  = $formatted_variation_atts[ $i - 1 ][ 'name' ];
                $feed_attributes[ "Variation Specific Value {$i}" ] = $formatted_variation_atts[ $i - 1 ][ 'value' ];
            }
            else {
                $feed_attributes[ "Variation Specific Name {$i}" ]  = '';
                $feed_attributes[ "Variation Specific Value {$i}" ] = '';
            }
        }

        return $feed_attributes;
    }

    /**
     * Check if the merchants is valid or not
     * @param $feed_merchants
     * @return bool
     */
    public function is_valid_merchant(){
        return array_key_exists($this->merchant, $this->feed_merchants)? true : false;
    }


    /**
     * Return Feed
     *
     * @return array|bool|string
     */
    public function returnFinalProduct(){
    	if ( $this->feed_format === 'csv' ) {
		    return RexShoppingCustom::asCsv();
	    }
    }

    public function footer_replace() {
        $this->feed = str_replace('</productRequest>', '', $this->feed);
    }


}
