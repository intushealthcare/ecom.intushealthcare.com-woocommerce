<?php
// Add you markups here
$features = [
    '<a href="'. esc_url( 'https://rextheme.com/docs/how-to-generate-a-custom-product-feed-easily/' ) .'" target ="_blank" >' . esc_attr__( 'Generate Unlimited Products', 'rex-product-feed-pro' ) . '</a>',
    '<a href="'. esc_url( 'https://rextheme.com/docs/wpfm-extensive-custom-fields-unique-identifiers/' ) .'">' . esc_attr__( 'Unique Product Identifiers (Brand, GTIN, MPN, etc.)', 'rex-product-feed-pro' ) . '</a>',
    '<a href="'. esc_url( 'https://rextheme.com/docs/wpfm-custom-fields-detailed-product-attributes/' ) .'">' . esc_attr__( 'Detailed Product Attributes (Size, Gender, Material, etc.)', 'rex-product-feed-pro' ) . '</a>',
    '<a href="'. esc_url( 'https://rextheme.com/docs/how-to-use-the-dynamic-pricing-feature-to-manipulate-your-product-price/' ) .'">' . esc_attr__( 'Access to Feed Rule Feature', 'rex-product-feed-pro' ) . '</a>',
    '<a href="'. esc_url( 'https://rextheme.com/docs/how-to-merge-multiple-attributes-values-together-with-the-combined-fields-feature/' ) .'">' . esc_attr__( 'Access to Combined Fields Feature', 'rex-product-feed-pro' ) . '</a>',
    '<a href="'. esc_url( 'https://rextheme.com/docs/how-to-use-the-dynamic-pricing-feature-to-manipulate-your-product-price/' ) .'">' . esc_attr__( 'Access to Dynamic Pricing Feature', 'rex-product-feed-pro' ) . '</a>',
    '<a href="'. esc_url( 'https://rextheme.com/docs/wpfm-exclude-tax-structured-data-prices/' ) .'">' . esc_attr__( 'Exclude Tax From Structured Data Prices', 'rex-product-feed-pro' ) . '</a>',
    '<a href="'. esc_url( 'https://rextheme.com/docs/wpfm-fix-json-ld-structured-data/' ) .'">' . esc_attr__( 'Fix WooCommerce’s (Json-Ld) Structured Data Bug', 'rex-product-feed-pro' ) . '</a>',
    '<a href="'. esc_url( 'https://rextheme.com/support/' ) .'">' . esc_attr__( 'Access To An Elite Support Team.', 'rex-product-feed-pro' ) . '</a>',
];

echo '<div class="rex_feed_features_text_pro">';
echo '<h2>' . esc_html__('Premium Version Features', 'rex-product-feed-pro' ) . '</h2>';
echo '<ul class="parent">';

foreach ( $features as $feature ) {
    echo '<li class="item">';
    include WPFM_PLUGIN_ASSETS_FOLDER_PATH . 'icon/icon-svg/right-check.php';
    echo $feature; //phpcs:ignore
    echo '</li>';
}

echo '</ul>';
echo '</div>';