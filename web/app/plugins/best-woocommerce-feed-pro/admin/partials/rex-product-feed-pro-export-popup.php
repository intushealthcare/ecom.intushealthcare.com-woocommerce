<!-- `rex-export-popup` block -->
<section class="rex-export-popup" style="display: none">
    <div class="rex-export-popup__wrapper">
        <!-- `rex-export-popup__body` element in the `rex-export-popup` block  -->
        <div class="rex-export-popup__body">
            <span class="rex-export-popup__close-btn">
                <svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M10.9989 2.80005L9.17616 0.977356L5.98801 4.16551L2.79985 0.977356L0.977158 2.80005L4.16531 5.98821L0.977158 9.17636L2.79985 10.9991L5.98801 7.8109L9.17616 10.9991L10.9989 9.17636L7.81071 5.98821L10.9989 2.80005Z" fill="#E56829"/>
                </svg>

            </span>

            <!-- `rex-export-popup__message` element in the `rex-export-popup` block  -->
            <div class="rex-export-popup__message">
                <h4 class="rex-export-popup__heading"><?php esc_html_e('Export Feed','rex-product-feed-pro')?></h4>

                <div class="rex-export-popup__message-content">
                    <div class="rex-export-popup__checkbox-all">
                        <input type="checkbox" id="rex_feed_export_check_all_btn" name="rex_feed_export_check_all_btn" value="1">
                        <label for="rex_feed_export_check_all_btn">Check All</label>
                    </div>

                    <ul class="rex-export-popup__checkbox-area">
                        <?php
                        if( is_array( $feed_ids ) && !empty( $feed_ids ) ) {
                            foreach( $feed_ids as $id ) {
                        ?>
                        <li class="rex-export-popup__checkbox">
                            <input type="checkbox" class="rex_feed_export_feeds" id="rex_feed_export_feed<?php echo '-' . esc_attr( $id );?>" name="rex_feed_export_feeds[]" value="<?php echo esc_attr( $id );?>">
                            <?php
                            $feed_title = get_the_title( $id ) ?: 'Untitled';
                            $feed_title = 'Untitled' === $feed_title ? $feed_title . ' - ' . $id : $feed_title;
                            ?>
                            <label for="rex_feed_export_feed<?php echo '-' . esc_attr( $id );?>"><?php echo esc_attr( $feed_title );?></label>
                        </li>
                        <?php
                            }
                        }
                        ?>
                    </ul>

                </div>

                <div class="rex-export-popup__notice-area" style="display: none">
                    <h6 class="rex-export-popup__heading--notice"><?php esc_html_e('Please, select at least one feed!','rex-product-feed-pro')?></h6>
                </div>

                <div class="rex-export-popup__btn-area">
                    <a class="rex-export-popup__btn rex-export-popup__btn--green" id="rex_feed_export_cancel_btn" target="_self" role="button">
                        <?php esc_html_e('Cancel','rex-product-feed-pro') ?>
                    </a>

                    <a class="rex-export-popup__btn" id="rex_feed_export_now_button" target="_self" role="button">
                        <span><?php esc_html_e('Export Now','rex-product-feed-pro') ?></span>
                        <i class="fa fa-spinner fa-pulse fa-fw" style="display: none"></i>
                    </a>
                </div>

            </div>

        </div>

    </div>
</section>
<!-- `rex-export-popup` block  end -->