<?php
$status = get_post_meta( get_the_ID(), '_rex_feed_feed_rules_button', true );
$style = 'added' !== $status ? 'style="display: none;"' : '';
?>

<div class="rex-feed-rules-area" <?php echo $style;?>>

    <div class="rex-feed-rules-area__content">

        <div class="rex-feed-rules-area__fine-replace rex-feed-rules-area__fine-brp">
            <div class="rex-feed-rules-area__delete" title="Delete all rules">
                <?php include plugin_dir_path(__FILE__) . '../assets/icon/icon-svg/section-delete.php';?>
            </div>
            <!-- .rex-feed-rules-area__delete end -->

            <div class="accordion__list">
                <div class="accordion">
                    <span class="accordion__title">
                        <span class="accordion__arrow"></span>
                        <label for="<?php echo 'rex_feed_filer-rules';?>">
                            <?php _e('Feed Rules', 'rex-product-feed-pro' )?>
                        </label>
                        <span class="rex_feed-tooltip">
                            <?php include plugin_dir_path(__FILE__) . '../assets/icon/icon-svg/icon-question.php';?>
                            <p><?php esc_html_e( 'Manipulate your feed data with your preferred condition', 'rex-product-feed' ); ?></p>
                        </span>
                    </span>
                    <!-- .accordion__title end -->

                    <div class="accordion__content-wrap">
                        <div class="accordion__content">
                            <div class="accordion__table-container" role="table" aria-label="condition table">

                                <div class="flex-table-header" role="rowgroup">
                                    <div class="flex-row" role="columnheader">
                                        <p class="rex-feed-rule-labels"><?php echo __('If', 'rex-product-feed') ?></p>
                                        <span>*</span>
                                    </div>

                                    <div class="flex-row" role="columnheader">
                                        <p class="rex-feed-rule-labels"><?php echo __('Condition', 'rex-product-feed') ?></p>
                                        <span>*</span>
                                    </div>

                                    <div class="flex-row" role="columnheader">
                                        <p class="rex-feed-rule-labels"><?php echo __('Find', 'rex-product-feed') ?></p>
                                        <span class="rex_feed-tooltip">
                                            <?php include plugin_dir_path(__FILE__) . '../assets/icon/icon-svg/icon-question.php';?>
                                            <p>
                                                <?php esc_html_e( 'Set any value (case-sensitive) to find, leave blank if you want to search empty value.', 'rex-product-feed' ); ?>
                                                <br><br>
                                                <?php esc_html_e( 'Provide appropriate slug in case of searching for product attributes, categories, and tags.', 'rex-product-feed' ); ?>
                                            </p>
                                        </span>
                                    </div>

                                    <div class="flex-row" role="columnheader">
                                        <p class="rex-feed-rule-labels"><?php echo __('Then', 'rex-product-feed') ?></p>
                                        <span>*</span>
                                    </div>

                                    <div class="flex-row" role="columnheader">
                                        <p class="rex-feed-rule-labels"><?php echo __('Static', 'rex-product-feed') ?></p>
                                    </div>

                                    <div class="flex-row" role="columnheader">
                                        <p class="rex-feed-rule-labels"><?php echo __('Replace / Operation', 'rex-product-feed') ?></p>
                                        <span class="rex_feed-tooltip">
                                            <?php include plugin_dir_path(__FILE__) . '../assets/icon/icon-svg/icon-question.php';?>
                                            <p>
                                                <?php esc_html_e( 'Select an attribute or put a static value.', 'rex-product-feed' ); ?>
                                                <br><br>
                                                <?php esc_html_e( 'You can also put mathematical operations in the static field to manipulate price values. In such cases, ', 'rex-product-feed' ); ?>
                                                <br><br>
                                                <i>
                                                    <?php esc_html_e( 'In such cases, your operations should always start with a mathematical operator (+, -, *, /)', 'rex-product-feed' ); ?>
                                                    <br><br>
                                                    <?php esc_html_e( 'i.e. +15%', 'rex-product-feed' ); ?>
                                                </i>
                                            </p>
                                        </span>
                                    </div>

                                    <div class="flex-row" role="columnheader">
                                        <p class="rex-feed-rule-labels"><?php echo __('Action', 'rex-product-feed') ?></p>
                                    </div>

                                </div>
                                <!-- .flex-table-header end -->

                                <div class="flex-table-body" role="rowgroup"></div>
                                <!-- .flex-table-body end -->
                            </div>

                        </div>
                        <!-- .accordion__content end -->
                    </div>
                    <!-- .accordion__content-wrap end -->

                </div>
            </div>
            <!-- .accordion__list end -->
        </div>
        <!-- .rex-feed-rules-area__fine-replace end -->

    </div>
    <!-- .rex-feed-rules-area__content end -->
</div>
<!-- .rex-feed-rules-area end -->