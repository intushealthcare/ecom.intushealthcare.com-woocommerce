<div class="rex-feed-product-filter-ids__area" style="display: none">
		<div class="rex-feed-product-filter-ids">	
			<label for="<?php echo $prefix . 'product_filter_ids';?>"><?php _e('Product Filter', 'rex-product-feed-pro' )?>
				<span class="rex_feed-tooltip">
					<?php include plugin_dir_path(__FILE__) . '../assets/icon/icon-svg/icon-question.php';?>
					<p><?php esc_html_e( 'Include or remove any specific products', 'rex-product-feed-pro' ); ?></p>
				</span>
			</label>

            <a href="<?php echo esc_url('https://rextheme.com/docs/product-filter-option-to-include-exclude-specific-products/' )?>" target="_blank">
                <?php esc_html_e('Learn How', 'rex-product-feed-pro' )?>
            </a>
        </div>

	
	<div class="rex-feed-product-filter-selected__area">

		<div class="rex-feed-product-search-wrapper">
		
			<select multiple id="rex_feed_product_filter_ids" class="wpfm-product-search" style="width: 95%" name="rex_feed_product_filter_ids[]" data-security="<?php echo wp_create_nonce('search-products')?>">
				<?php
				foreach ( $selected_ids as $id ) {
					$product = wc_get_product( $id );
					if ( $product ) {
						echo '<option value="' .$id. '" selected>' .$product->get_name(). '</option>';
					}
				}
				?>
			</select>
		</div>

		<?php
		$condition = get_post_meta(get_the_ID(),'_rex_feed_product_condition', true ) ?: get_post_meta(get_the_ID(),'rex_feed_product_condition', true );

		if($condition){

			if( $condition == 'inc'){
				echo '<select name="product_filter_condition" class="product_filter_condition"><option value="">Please select</option><option selected value="inc">Include</option><option value="exc">Exclude</option></select> ';
			}
			elseif($condition == 'exc'){
				echo '<select name="product_filter_condition" class="product_filter_condition"><option value="">Please select</option><option value="inc">Include</option><option selected value="exc">Exclude</option></select> ';
			}else{
				echo '<select name="product_filter_condition" class="product_filter_condition"><option value="">Please select</option><option value="inc">Include</option><option value="exc">Exclude</option></select> ';
			}

		}else{
			echo '<select name="product_filter_condition" class="product_filter_condition"><option value="">Please select</option><option value="inc">Include</option><option value="exc">Exclude</option></select> ';
		}

	echo '</div>';

echo '</div>';