<?php
$feed_rules = $feed_filter_rules->getFilterMappings();
$rand_key = rand(999, 3000);
?>
    <div class="flex-table-row rexfeed-hidded-section"  data-row-id="<?php echo esc_html( $rand_key ); ?>" style="display: none;">
        <div class="flex-row first" role="cell">
            <?php $feed_filter_rules->printSelectDropdown( $rand_key, 'rules_if', 'fr', '', 'rules_dropdown_if', '' ); ?>
        </div>

        <div class="flex-row" role="cell">
            <?php $feed_filter_rules->printSelectDropdown( $rand_key, 'rules_condition', 'fr', '' ); ?>
        </div>

        <div class="flex-row" role="cell">
            <?php $feed_filter_rules->printInput( $rand_key, 'rules_find', 'fr', '' ); ?>
        </div>

        <div class="flex-row" role="cell">
            <?php $feed_filter_rules->printSelectDropdown( $rand_key, 'rules_then', 'fr', '', '', '', '' ); ?>
        </div>

        <div class="flex-row checkout-pos" role="cell">
            <span class="wpfm-checkbox">
                <input type="checkbox" name="fr[<?php echo $rand_key;?>][rules_static]" class="rex-feed-rule-static" id="rex_feed_rule_static_<?php echo $rand_key?>">
                <label for="rex_feed_rule_static_<?php echo $rand_key?>"></label>
            </span>
        </div>

        <div class="flex-row" role="cell">
            <?php $feed_filter_rules->printInput( $rand_key, 'rules_static_replace', 'fr', '', 'rules_static_replace', 'display: none' ); ?>
            <?php $feed_filter_rules->printSelectDropdown( $rand_key, 'rules_replace', 'fr', '', 'rules_dropdown_replace', '' ); ?>
        </div>

        <div class="flex-row condition-icon" role="cell">
            <a class="add-row" title="add field">
                <?php include plugin_dir_path(__FILE__) . '../assets/icon/icon-svg/add.php';?>
            </a>

            <a class="delete-row" title="Delete">
                <?php include plugin_dir_path(__FILE__) . '../assets/icon/icon-svg/single-delete.php';?>
            </a>
        </div>
    </div>
    <!-- .flex-table-row end -->

<?php foreach( $feed_rules as $key => $item ):?>
    <?php
    $rules_if             = !empty( $item[ 'rules_if' ] ) ? $item[ 'rules_if' ] : '';
    $rules_condition      = !empty( $item[ 'rules_condition' ] ) ? $item[ 'rules_condition' ] : '';
    $rules_find           = !empty( $item[ 'rules_find' ] ) ? $item[ 'rules_find' ] : '';
    $rules_then           = !empty( $item[ 'rules_then' ] ) ? $item[ 'rules_then' ] : '';
    $rules_static_replace = !empty( $item[ 'rules_static_replace' ] ) ? $item[ 'rules_static_replace' ] : '';
    $rules_replace        = !empty( $item[ 'rules_replace' ] ) ? $item[ 'rules_replace' ] : '';
    $rules_static         = !empty( $item[ 'rules_static' ] ) ? 'checked' : '';
    $input_style          = 'display: none;';
    $select_style         = '';
    $static_select_class  = 'rules-select2';
    if( 'checked' === $rules_static ) {
        $input_style  = '';
        $select_style = 'display: none;';
        $static_select_class = '';
    }
    ?>

    <div class="flex-table-row"  data-row-id="<?php echo esc_html( $key ); ?>">
        <div class="flex-row first" role="cell">
            <?php $feed_filter_rules->printSelectDropdown( $key, 'rules_if', 'fr', $rules_if, 'rules_dropdown_if rules-select2', '' ); ?>
        </div>

        <div class="flex-row" role="cell">
            <?php $feed_filter_rules->printSelectDropdown( $key, 'rules_condition', 'fr', $rules_condition, 'rules-select2' ); ?>
        </div>

        <div class="flex-row" role="cell">
            <?php $feed_filter_rules->printInput( $key, 'rules_find', 'fr', $rules_find ); ?>
        </div>

        <div class="flex-row" role="cell">
            <?php $feed_filter_rules->printSelectDropdown( $key, 'rules_then', 'fr', $rules_then, 'rules-select2', '', '' ); ?>
        </div>

        <div class="flex-row checkout-pos" role="cell">
            <span class="wpfm-checkbox">
                <input type="checkbox" name="fr[<?php echo $key;?>][rules_static]" class="rex-feed-rule-static" <?php echo $rules_static?> id="rex_feed_rule_static_<?php echo $key?>">
                <label for="rex_feed_rule_static_<?php echo $key?>"></label>
            </span>
        </div>

        <div class="flex-row" role="cell">
            <?php $feed_filter_rules->printInput( $key, 'rules_static_replace', 'fr', $rules_static_replace, 'rules_static_replace', $input_style ); ?>
            <?php $feed_filter_rules->printSelectDropdown( $key, 'rules_replace', 'fr', $rules_replace, "rules_dropdown_replace {$static_select_class}", $select_style ); ?>
        </div>

        <div class="flex-row condition-icon" role="cell">
            <a class="add-row" title="add field">
                <?php include plugin_dir_path(__FILE__) . '../assets/icon/icon-svg/add.php';?>
            </a>
            <a class="delete-row" title="Delete" <?php if(sizeof($feed_rules) === 1) echo 'style="display: none;"'; ?>>
                <?php include plugin_dir_path(__FILE__) . '../assets/icon/icon-svg/single-delete.php';?>
            </a>
        </div>
    </div>
    <!-- .flex-table-row end -->

<?php endforeach;?>