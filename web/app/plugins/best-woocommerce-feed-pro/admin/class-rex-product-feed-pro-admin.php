<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://rextheme.com
 * @since      1.0.0
 *
 * @package    Rex_Product_Feed_Pro
 * @subpackage Rex_Product_Feed_Pro/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Rex_Product_Feed_Pro
 * @subpackage Rex_Product_Feed_Pro/admin
 * @author     RexTheme <#>
 */
class Rex_Product_Feed_Pro_Admin
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Settings Page
	 *
	 * @since    2.2.5
	 * @access   private
	 * @var      string
	 */
	private $wpfm_settings_page = null;


	private $pro_condition;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param string $plugin_name The name of this plugin.
	 * @param string $version The version of this plugin.
	 * @since    1.0.0
	 */
	public function __construct( $plugin_name, $version )
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}


	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rex_Product_Feed_Pro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rex_Product_Feed_Pro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/rex-product-feed-pro-admin.css', array(), $this->version, 'all');
	}


	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rex_Product_Feed_Pro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rex_Product_Feed_Pro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
        $screen = get_current_screen();

        $allowed_screen = array( 'product-feed_page_wpfm_dashboard', 'product-feed_page_merchant_settings', 'product-feed_page_wpfm-license' );

        if ( ($screen->post_type === 'product-feed' && isset($_GET['action']) && $_GET['action'] === 'edit' ) || (isset($_GET['post_type']) && $_GET['post_type'] === 'product-feed' && $screen->base !== 'edit') || (in_array($screen->id, apply_filters('wpfm_page_hooks', $allowed_screen)))) {
            wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/rex-product-feed-pro-admin.js', array('jquery', 'jquery-ui-autocomplete'), $this->version, true);

            wp_localize_script( $this->plugin_name, 'wpfmProObj',
                array(
                    'ajax_url' => admin_url( 'admin-ajax.php' ),
                    'ajax_nonce' => wp_create_nonce('rex-wpfm-pro-ajax'),
                    'wpfm_pro_license_active' => apply_filters('wpfm_is_premium', false),
                )
            );
        }
		//enqueue script for product filter ends
	}


	/**
	 * @return array
	 */
	public function wpfm_get_total_number_of_products( $product, $feed_id )
	{
		$totalProducts = $this->wpfm_get_ids($feed_id);

		if ($this->wpfm_is_premium()) {
			$products = array(
				'products' => $totalProducts,
			);
		} else {
			if ($totalProducts > WPFM_FREE_MAX_PRODUCT_LIMIT) {
				$products = array(
					'products' => WPFM_FREE_MAX_PRODUCT_LIMIT
				);
			} else {
				$products = array(
					'products' => $totalProducts
				);
			}
		}
		return $products;
	}


    /**
     * Get total number of product ids
     *
     * @param $feed_id
     * @return string|null
     */
    protected function wpfm_get_ids( $feed_id ) {
        global $wpdb;
        $join = 'WHERE 1=1 ';

        if ( function_exists( 'wpfm_is_wpml_active' ) && wpfm_is_wpml_active() ) {
            global $sitepress;
            $language = get_post_meta( $feed_id, '_rex_feed_wpml_language', true ) ?: get_post_meta( $feed_id, 'rex_feed_wpml_language', true );

			if( !$language ) {
				$language = defined( 'ICL_LANGUAGE_CODE' ) ? ICL_LANGUAGE_CODE : '';
			}

            $sitepress->switch_lang( $language );
            $join = "LEFT JOIN {$wpdb->prefix}icl_translations AS wpml_language ";
            $join .= "ON wpml_language.element_id = {$wpdb->prefix}posts.ID ";
            $join .= "WHERE wpml_language.language_code = '{$language}' ";
            $join .= "AND wpml_language.element_type IN ('post_product', 'post_product_variation') ";
        }

        $request = "SELECT DISTINCT COUNT({$wpdb->prefix}posts.ID) ";
        $request .= "FROM {$wpdb->prefix}posts ";
        $request .= $join;
        $request .= "AND {$wpdb->prefix}posts.post_type IN ('product', 'product_variation') ";
        $request .= "AND {$wpdb->prefix}posts.post_status = 'publish'";

        return $wpdb->get_var( $request );
    }


	/**
	 * is premium?
	 * @return bool
	 */
	public function wpfm_is_premium()
	{
		$is_premium = get_option('wpfm_is_premium', 'no');

		return $is_premium === 'yes' ? true : false;
	}


	/**
	 * Is premium Plugin activated
	 * @return bool
	 */
	public function wpfm_is_premium_activate()
	{
		return true;
	}


	/**
	 * Support link
	 * @return bool
	 */
	public function wpfm_support_link()
	{
		return 'https://rextheme.com/support/';
	}


	/**
	 * premium merchants
	 * @return array
	 */
	public function wpfm_merchant_array($array)
	{
		$merchat_array = array(
			'bol', 'wish', 'leguide', 'connexity', 'drm', 'google_review'
		);
		foreach ($merchat_array as $merchant) {
			$array[] = $merchant;
		}
		return $array;
	}


	/**
	 * those merchants use 'other' template
	 * @return array
	 */
	public function wpfm_merchant_custom($array)
	{
		$merchat_array = array(
			'bol', 'leguide', 'connexity', 'drm'
		);
		foreach ($merchat_array as $merchant) {
			$array[] = $merchant;
		}
		return $array;
	}


	/**
	 * Which has the specific format
	 * @param $array
	 * @return array
	 */
	public function wpfm_merchant_fixed_format($array)
	{
		$merchat_array = array(
			'bol', 'wish', 'leguide', 'connexity', 'drm'
		);
		foreach ($merchat_array as $merchant) {
			$array[] = $merchant;
		}
		return $array;
	}


    /**
     * @desc Add combined attributes type in config table type dropdown.
     * @param $types
     * @return mixed
     */
	public function wpfm_pro_feed_attribute_type_render( $types )
	{
		$types[ 'combined' ]               = 'Combined Attributes';
		$types[ 'ebay_mip_attr' ]          = 'eBay MIP Attribute';
		$types[ 'ebay_mip_shipping_attr' ] = 'eBay MIP Shipping Attribute';
		return $types;
	}

	/**
	 * Set eBay attribute types for meta type list.
	 *
	 * This function adds eBay attribute types to the list of supported meta types.
	 *
	 * @param array $meta_types An array of existing meta types.
	 *
	 * @return array Updated array of meta types including eBay attribute types.
	 * @since 7.3.11
	 */
	public function set_ebay_attribute_types( $meta_types ) {
		$meta_types[] = 'ebay_mip_attr';
		$meta_types[] = 'ebay_mip_shipping_attr';
		return $meta_types;
	}


	/**
	 * Merchant array that has custom config
	 * @param $array
	 * @return array
	 */
	public function wpfm_has_custom_feed_config($array)
	{
		$custom_merchant = array('ebay_mip', 'ebay_seller', 'amazon_sell');
		foreach ($custom_merchant as $merchant) {
			$array[] = $merchant;
		}
		return $array;
	}


	public function wpfm_custom_metabox_display_ebay_mip($merchant, $feed_template)
	{
		include plugin_dir_path(__FILE__) . 'partials/' . $merchant . '/feed-config-metabox-display.php';
	}

	public function wpfm_custom_metabox_display_ebay_seller($merchant, $feed_template)
	{
		include plugin_dir_path(__FILE__) . 'partials/' . $merchant . '/feed-config-metabox-display.php';
	}

	public function wpfm_custom_metabox_display_amazon_sell($merchant, $feed_template)
	{
		include plugin_dir_path(__FILE__) . 'partials/' . $merchant . '/feed-config-metabox-display.php';
	}


	/**
	 * @return array
	 */
	public function wpfm_page_hooks($array)
	{
		$array[] = $this->wpfm_settings_page;
		return array_reverse($array);
	}


	/*
	 * Register woocommerce custom fields
	 */
	public function rex_product_custom_fields()
	{
		$custom_field  = get_option( 'rex-wpfm-product-custom-field', 'no' );
		$pa_field      = get_option( 'rex-wpfm-product-pa-field' );

		global $post;

		echo '<div id="wpfm_product_meta" class="panel woocommerce_options_panel hidden">';
		if ( $custom_field === "yes" && $this->wpfm_is_premium() ) {
			echo "<div class='wpfm-custom-fields-wrapper' style='border-bottom: 1px solid #eee; '>";
			echo "<p><strong>Unique Product Identifiers</strong></p>";
			echo "<div class='wpfm-attr options_group no-border' style='border-bottom: none;'>";
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_brand',
					'label' => __( 'Product Brand', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_brand', true),
					'description' => __( 'Enter the product Brand here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_gtin',
					'label' => __( 'Product GTIN', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_gtin', true),
					'description' => __( 'Enter the product GTIN value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_mpn',
					'label' => __( 'Product MPN', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_mpn', true),
					'description' => __( 'Enter the product MPN value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_upc',
					'label' => __( 'Product UPC', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_upc', true),
					'description' => __( 'Enter the product UPC value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_ean',
					'label' => __( 'Product EAN', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_ean', true),
					'description' => __( 'Enter the product EAN value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_jan',
					'label' => __( 'Product JAN', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_jan', true),
					'description' => __( 'Enter the product JAN value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_isbn',
					'label' => __( 'Product ISBN', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_isbn', true),
					'description' => __( 'Enter the product ISBN value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_itf',
					'label' => __( 'Product ITF14', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_itf', true),
					'description' => __( 'Enter the product ITF14 value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_offer_price',
					'label' => __( 'Offer price', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_offer_price', true),
					'description' => __( 'Enter the product offer price here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'type' => 'date',
					'id' => '_wpfm_product_offer_effective_date',
					'label' => __( 'Offer effective date', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_offer_effective_date', true),
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_additional_info',
					'label' => __( 'Additional information (if any)', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_additional_info', true),
				)
			);
			echo "</div>";
			echo "</div>";
		}
		elseif ( !$this->wpfm_is_premium() ) {
			$url = '<a href="' .admin_url('admin.php?page=wpfm-license').'" target="_blank">activate your license key</a>';
			echo '<div><strong>' . __( 'You need to '.$url.' to enjoy the pro features for Product Feed Manager for WooCommerce Pro.', 'rex-product-feed-pro' ) . '</strong></div>';
        }
		else {
			$url = '<a href="' .admin_url('admin.php?page=wpfm_dashboard').'" target="_blank">enable</a>';
			echo '<div class="wpfm-unique-product"><strong>' . __( 'You need to '.$url.' Unique Product Identifiers fields from WPFM settings page.', 'rex-product-feed-pro' ) . '</strong></div>';
		}

		if ($pa_field == "yes") {
			echo "<div class='wpfm-custom-fields-wrapper show_if_simple show_if_external' style='border-bottom: 1px solid #eee; '>";
			echo "<p><strong>Detailed Product Attributes</strong></p>";
			echo "<div class='options_group show_if_simple show_if_external'>";
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_color',
					'label' => __( 'Product Color', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_color', true),
					'description' => __( 'Enter the product color value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_size',
					'label' => __( 'Product Size', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_size', true),
					'description' => __( 'Enter the product size value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_pattern',
					'label' => __( 'Product Pattern', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_pattern', true),
					'description' => __( 'Enter pattern value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_material',
					'label' => __( 'Material', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_material', true),
					'description' => __( 'Enter material value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_age_group',
					'label' => __( 'Age group', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_age_group', true),
					'description' => __( 'Enter age group here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => '_wpfm_product_gender',
					'label' => __( 'Gender', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($post->ID, '_wpfm_product_gender', true),
					'description' => __( 'Enter gender here.', 'rex-product-feed-pro' )
				)
			);
			echo "</div>";
			echo "</div>";
		}

		echo '</div>';
	}


	/**
	 * Register custom tab
	 */
	public function rex_product_custom_tabs($tabs)
	{
		$tabs['wpfm_custom_fields'] = array(
			'label' => 'WPFM Custom Fields',
			'target' => 'wpfm_product_meta',
			'class' => array('wpfm_wc_custom_tabs'),
		);
		return $tabs;

	}


	/*
	 * Save woocommerce custom fields
	 */
    public function rex_product_custom_fields_save( $post_id )
    {
        $post = rex_feed_get_sanitized_get_post();
        $post = is_array( $post ) && !is_wp_error( $post ) && isset( $post[ 'post' ] ) ? $post[ 'post' ] : [];
        if( isset( $post[ '_wpfm_product_brand' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_brand', esc_attr( sanitize_text_field( $post[ '_wpfm_product_brand' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_gtin' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_gtin', esc_attr( sanitize_text_field( $post[ '_wpfm_product_gtin' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_mpn' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_mpn', esc_attr( sanitize_text_field( $post[ '_wpfm_product_mpn' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_upc' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_upc', esc_attr( sanitize_text_field( $post[ '_wpfm_product_upc' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_ean' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_ean', esc_attr( sanitize_text_field( $post[ '_wpfm_product_ean' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_jan' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_jan', esc_attr( sanitize_text_field( $post[ '_wpfm_product_jan' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_isbn' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_isbn', esc_attr( sanitize_text_field( $post[ '_wpfm_product_isbn' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_itf' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_itf', esc_attr( sanitize_text_field( $post[ '_wpfm_product_itf' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_offer_price' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_offer_price', esc_attr( sanitize_text_field( $post[ '_wpfm_product_offer_price' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_offer_effective_date' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_offer_effective_date', esc_attr( sanitize_text_field( $post[ '_wpfm_product_offer_effective_date' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_additional_info' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_additional_info', esc_attr( sanitize_text_field( $post[ '_wpfm_product_additional_info' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_color' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_color', esc_attr( sanitize_text_field( $post[ '_wpfm_product_color' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_size' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_size', esc_attr( sanitize_text_field( $post[ '_wpfm_product_size' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_pattern' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_pattern', esc_attr( sanitize_text_field( $post[ '_wpfm_product_pattern' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_material' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_material', esc_attr( sanitize_text_field( $post[ '_wpfm_product_material' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_age_group' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_age_group', esc_attr( sanitize_text_field( $post[ '_wpfm_product_age_group' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_gender' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_gender', esc_attr( sanitize_text_field( $post[ '_wpfm_product_gender' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_item_type' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_item_type', esc_attr( sanitize_text_field( $post[ '_wpfm_product_item_type' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_bullet_point_1' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_bullet_point_1', esc_attr( sanitize_text_field( $post[ '_wpfm_product_bullet_point_1' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_bullet_point_2' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_bullet_point_2', esc_attr( sanitize_text_field( $post[ '_wpfm_product_bullet_point_2' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_bullet_point_3' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_bullet_point_3', esc_attr( sanitize_text_field( $post[ '_wpfm_product_bullet_point_3' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_bullet_point_4' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_bullet_point_4', esc_attr( sanitize_text_field( $post[ '_wpfm_product_bullet_point_4' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_bullet_point_5' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_bullet_point_5', esc_attr( sanitize_text_field( $post[ '_wpfm_product_bullet_point_5' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_search_terms_1' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_search_terms_1', esc_attr( sanitize_text_field( $post[ '_wpfm_product_search_terms_1' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_search_terms_2' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_search_terms_2', esc_attr( sanitize_text_field( $post[ '_wpfm_product_search_terms_2' ] ) ));
        }
        if( isset( $post[ '_wpfm_product_search_terms_3' ] ) ) {
            update_post_meta($post_id, '_wpfm_product_search_terms_3', esc_attr( sanitize_text_field( $post[ '_wpfm_product_search_terms_3' ] ) ));
        }
    }


	/*
	 * Register woocommerce custom fields for variable products
	 */
	public function rex_product_variable_custom_fields($loop, $variation_data, $variation)
	{
		$custom_field = get_option('rex-wpfm-product-custom-field');
		$pa_field = get_option('rex-wpfm-product-pa-field');
		$amazon_fields = get_option('wpfm_amazon_fields', 'no');
		if ($custom_field == "yes") {
			echo "<div class='wpfm-custom-fields-wrapper' style='border-bottom: 1px solid #eee; border-top: 1px solid #eee;'>";
			echo "<p><strong>Unique Product Identifiers</strong></p>";
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_gtin[{$loop}]",
					'name' => "_wpfm_variable_product_gtin[{$loop}]",
					'label' => __( 'Product GTIN', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_gtin', true),
					'description' => __( 'Enter the product GTIN attribute here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_mpn[{$loop}]",
					'name' => "_wpfm_variable_product_mpn[{$loop}]",
					'label' => __( 'Product MPN', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_mpn', true),
					'description' => __( 'Enter the product MPN attribute here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_upc[{$loop}]",
					'name' => "_wpfm_variable_product_upc[{$loop}]",
					'label' => __( 'Product UPC', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_upc', true),
					'description' => __( 'Enter the product UPC attribute here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_ean[{$loop}]",
					'name' => "_wpfm_variable_product_ean[{$loop}]",
					'label' => __( 'Product EAN', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_ean', true),
					'description' => __( 'Enter the product EAN attribute here.', 'rex-product-feed-pro' )
				)
			);

			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_jan[{$loop}]",
					'name' => "_wpfm_variable_product_jan[{$loop}]",
					'label' => __( 'Product JAN', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_jan', true),
					'description' => __( 'Enter the product JAN attribute here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_isbn[{$loop}]",
					'name' => "_wpfm_variable_product_isbn[{$loop}]",
					'label' => __( 'Product ISBN', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_isbn', true),
					'description' => __( 'Enter the product ICBN attribute here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_itf[{$loop}]",
					'name' => "_wpfm_variable_product_itf[{$loop}]",
					'label' => __( 'Product ITF-14', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_itf', true),
					'description' => __( 'Enter the product ITF-14 attribute here.', 'rex-product-feed-pro' )
				)
			);


			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_offer_price[{$loop}]",
					'label' => __( 'Offer price', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_offer_price', true),
					'description' => __( 'Enter the product offer price here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'type' => 'date',
					'id' => "_wpfm_variable_product_offer_effective_date[{$loop}]",
					'label' => __( 'Offer effective date', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_offer_effective_date', true),
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_additional_info[{$loop}]",
					'label' => __( 'Additional information (if any)', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_additional_info', true),
				)
			);

			echo "</div>";
		}


		if ($pa_field == "yes") {

			echo "<div class='wpfm-custom-fields-wrapper' style='border-bottom: 1px solid #eee; '>";
			echo "<p><strong>Detailed Product Attributes</strong></p>";
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_color[{$loop}]",
					'name' => "_wpfm_variable_product_color[{$loop}]",
					'label' => __( 'Product Color', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_color', true),
					'description' => __( 'Enter the product color value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_size[{$loop}]",
					'name' => "_wpfm_variable_product_size[{$loop}]",
					'label' => __( 'Product Size', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_size', true),
					'description' => __( 'Enter the product size value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_pattern[{$loop}]",
					'name' => "_wpfm_variable_product_pattern[{$loop}]",
					'label' => __( 'Product Pattern', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_pattern', true),
					'description' => __( 'Enter pattern value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_material[{$loop}]",
					'name' => "_wpfm_variable_product_material[{$loop}]",
					'label' => __( 'Material', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_material', true),
					'description' => __( 'Enter material value here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_age_group[{$loop}]",
					'name' => "_wpfm_variable_product_age_group[{$loop}]",
					'label' => __( 'Age group', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_age_group', true),
					'description' => __( 'Enter age group here.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_gender[{$loop}]",
					'name' => "_wpfm_variable_product_gender[{$loop}]",
					'label' => __( 'Gender', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_gender', true),
					'description' => __( 'Enter gender here.', 'rex-product-feed-pro' )
				)
			);
			echo "</div>";


		}


		if ($amazon_fields == "yes") {

			echo "<div class='wpfm-custom-fields-wrapper show_if_simple show_if_external' style='border-bottom: 1px solid #eee; '>";
			echo "<p><strong>Custom fields for Amazon.</strong></p>";
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_item_type[{$loop}]",
					'name' => "_wpfm_variable_product_item_type[{$loop}]",
					'label' => __( 'Item type', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_item_type', true),
					'description' => __( 'Enter the item type.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_bullet_point_1[{$loop}]",
					'name' => "_wpfm_variable_product_bullet_point_1[{$loop}]",
					'label' => __( 'Key feature (bullet point 1)', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_bullet_point_1', true),
					'description' => __( 'Enter bullet point.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_bullet_point_2[{$loop}]",
					'name' => "_wpfm_variable_product_bullet_point_2[{$loop}]",
					'label' => __( 'Key feature (bullet point 2)', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_bullet_point_2', true),
					'description' => __( 'Enter bullet point.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_bullet_point_3[{$loop}]",
					'name' => "_wpfm_variable_product_bullet_point_3[{$loop}]",
					'label' => __( 'Key feature (bullet point 3)', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_bullet_point_3', true),
					'description' => __( 'Enter bullet point.', 'rex-product-feed-pro' )
				)
			);

			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_bullet_point_4[{$loop}]",
					'name' => "_wpfm_variable_product_bullet_point_4[{$loop}]",
					'label' => __( 'Key feature (bullet point 4)', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_bullet_point_4', true),
					'description' => __( 'Enter bullet point.', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_bullet_point_5[{$loop}]",
					'name' => "_wpfm_variable_product_bullet_point_5[{$loop}]",
					'label' => __( 'Key feature (bullet point 5)', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_bullet_point_5', true),
					'description' => __( 'Enter bullet point.', 'rex-product-feed-pro' )
				)
			);

			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_search_terms_1[{$loop}]",
					'name' => "_wpfm_variable_product_search_terms_1[{$loop}]",
					'label' => __( 'Generic keywords (search terms 1)', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_search_terms_1', true),
					'description' => __( 'Enter search terms', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_search_terms_2[{$loop}]",
					'name' => "_wpfm_variable_product_search_terms_2[{$loop}]",
					'label' => __( 'Generic keywords (search terms 2)', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_search_terms_2', true),
					'description' => __( 'Enter search terms', 'rex-product-feed-pro' )
				)
			);
			woocommerce_wp_text_input(
				array(
					'id' => "_wpfm_variable_product_search_terms_3[{$loop}]",
					'name' => "_wpfm_variable_product_search_terms_3[{$loop}]",
					'label' => __( 'Generic keywords (search terms 3)', 'rex-product-feed-pro' ),
					'desc_tip' => 'true',
					'value' => get_post_meta($variation->ID, '_wpfm_product_search_terms_3', true),
					'description' => __( 'Enter search terms', 'rex-product-feed-pro' )
				)
			);
			echo "</div>";


		}
	}


	/*
	 * Save woocommerce custom fields for variable products
	 */
    public function rex_product_variable_custom_fields_save( $variation_id, $loop )
    {
        $post = rex_feed_get_sanitized_get_post();
        $post = is_array( $post ) && !is_wp_error( $post ) && isset( $post[ 'post' ] ) ? $post[ 'post' ] : [];
        if( isset( $post[ '_wpfm_variable_product_brand' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_brand', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_brand' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_gtin' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_gtin', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_gtin' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_mpn' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_mpn', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_mpn' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_upc' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_upc', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_upc' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_ean' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_ean', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_ean' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_jan' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_jan', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_jan' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_isbn' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_isbn', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_isbn' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_itf' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_itf', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_itf' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_offer_price' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_offer_price', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_offer_price' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_offer_effective_date' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_offer_effective_date', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_offer_effective_date' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_additional_info' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_additional_info', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_additional_info' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_color' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_color', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_color' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_size' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_size', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_size' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_pattern' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_pattern', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_pattern' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_material' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_material', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_material' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_age_group' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_age_group', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_age_group' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_gender' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_gender', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_gender' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_item_type' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_item_type', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_item_type' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_bullet_point_1' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_bullet_point_1', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_bullet_point_1' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_bullet_point_2' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_bullet_point_2', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_bullet_point_2' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_bullet_point_3' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_bullet_point_3', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_bullet_point_3' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_bullet_point_4' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_bullet_point_4', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_bullet_point_4' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_bullet_point_5' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_bullet_point_5', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_bullet_point_5' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_search_terms_1' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_search_terms_1', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_search_terms_1' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_search_terms_2' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_search_terms_2', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_search_terms_2' ][ $loop ] ) ) );
        }
        if( isset( $post[ '_wpfm_variable_product_search_terms_3' ][ $loop ] ) ) {
            update_post_meta( $variation_id, '_wpfm_product_search_terms_3', esc_attr( sanitize_text_field( $post[ '_wpfm_variable_product_search_terms_3' ][ $loop ] ) ) );
        }
    }


	/**
	 * product filter select dropdown
     *
	 * @param $prefix
	 */
	public function wpfm_product_filter_field( $prefix )
	{
	    if ( apply_filters( 'wpfm_is_premium', false ) ) {
		    $selected_ids = get_post_meta( get_the_ID(), '_rex_feed_product_filter_ids', true ) ?: get_post_meta( get_the_ID(), 'rex_feed_product_filter_ids', true );
		    $selected_ids = is_array($selected_ids ) ? $selected_ids : array();
		    require_once plugin_dir_path(__FILE__) . 'partials/rex-feed-pro-product-filter-sections.php';
        }
	}


	/**
     * Updates product filter options
     *
	 * @param $options
	 * @return mixed
	 */
	public function wpfm_update_options( $options )
	{
	    if ( apply_filters( 'wpfm_is_premium', false ) ) {
		    $options[ 'product_filter' ] = __( 'Product Filter', 'rex-product-feed-pro' );
	    }
		return $options;
	}


    /**
     * @desc Activate plugin license
     * @since 1.0.0
     * @return void
     */
    public function wpfm_pro_activate_license() {
        if( !wp_next_scheduled( 'rex_wpfm_license_check' ) ) {
            wp_schedule_event( time(), 'monthly', 'rex_wpfm_license_check' );
        }

        // listen for our activate button to be clicked
        if( isset( $_POST[ 'wpfm_pro_license_activate' ] ) ) {
            // run a quick security check
            if( !check_admin_referer( 'wpfm_pro_nonce', 'wpfm_pro_nonce' ) ) {
                return; // get out if we didn't click the Activate button
            }
            // retrieve the license from the database
            $license = $this->get_license();
            // data to send in our API request
            $api_params = array(
                'edd_action' => 'activate_license',
                'license'    => $license,
                'item_id'    => WPFM_SL_ITEM_ID, // The ID of the item in EDD
                'url'        => home_url()
            );

            // Call the custom API.
            $response = wp_remote_post( WPFM_SL_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

            // make sure the response came back okay
            if( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {

                $message = ( is_wp_error( $response ) && !empty( $response->get_error_message() ) ) ? $response->get_error_message() : __( 'An error occurred, please try again.', 'rex-product-feed-pro' );

            }
            else {

                $license_data = json_decode( wp_remote_retrieve_body( $response ) );

                if( false === $license_data->success ) {
                    switch( $license_data->error ) {
                        case 'expired' :
                            $message = sprintf(
                                __( 'Your license key expired on %s.', 'rex-product-feed-pro' ),
                                date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) )
                            );
                            break;

                        case 'revoked' :
                            $message = __( 'Your license key has been disabled.', 'rex-product-feed-pro' );
                            break;

                        case 'missing' :
                            $message = __( 'Invalid license.', 'rex-product-feed-pro' );
                            break;

                        case 'invalid' :
                        case 'site_inactive' :
                            $message = __( 'Your license is not active for this URL.', 'rex-product-feed-pro' );
                            break;

                        case 'item_name_mismatch' :
                            $message = sprintf( __( 'This appears to be an invalid license key for %s.', 'rex-product-feed-pro' ), 'Product Feed Manager for WooCommerce' );
                            break;

                        case 'no_activations_left':
                            $message = __( 'Your license key has reached its activation limit.', 'rex-product-feed-pro' );
                            break;

                        default :
                            $message = __( 'An error occurred, please try again.', 'rex-product-feed-pro' );
                            break;
                    }
                }
            }

            // Check if anything passed on a message constituting a failure
            if( !empty( $message ) ) {
                $base_url = admin_url( 'admin.php?page=wpfm-license' );
                $redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );

                wp_redirect( $redirect );
                exit();
            }

            // $license_data->license will be either "valid" or "invalid"
            update_option( 'wpfm_pro_license_status', $license_data->license );
            update_option( "wpfm_pro_license_data", serialize( json_decode( json_encode( $license_data ), true ) ) );

            if( $license_data->license == 'valid' ) {
                update_option( 'wpfm_is_premium', 'yes' );
            }

            wp_redirect( admin_url( 'admin.php?page=wpfm-license' ) );
            exit();
        }
    }


    /**
     * @desc Deactivate plugin license
     * @since 1.0.0
     * @return void
     */
    public function wpfm_pro_deactivate_license() {
        if( isset( $_POST[ 'wpfm_pro_license_deactivate' ] ) ) {
            if( !check_admin_referer( 'wpfm_pro_nonce', 'wpfm_pro_nonce' ) ) {
                return;
            }

            $license = $this->get_license();

            $api_params = array(
                'edd_action' => 'deactivate_license',
                'license'    => $license,
                'item_id'    => WPFM_SL_ITEM_ID,
                'url'        => home_url()
            );

            // Call the custom API.
            $response = wp_remote_post( WPFM_SL_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

            // make sure the response came back okay
            if( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
                $message = ( is_wp_error( $response ) && !empty( $response->get_error_message() ) ) ? $response->get_error_message() : __( 'An error occurred, please try again.', 'rex-product-feed-pro' );
            }
            else {
                $license_data = json_decode( wp_remote_retrieve_body( $response ) );

                if( false === $license_data->success ) {
                    switch( $license_data->license ) {
                        case 'deactivated':
                            $message = __( 'Your license successfully deactivate.', 'rex-product-feed-pro' );
                            break;
                        case 'failed':
                            $message = __( 'Your license deactivation failed.', 'rex-product-feed-pro' );
                            break;
                    }
                }
            }

            // Check if anything passed on a message constituting a failure
            if( !empty( $message ) ) {
                $base_url = admin_url( 'admin.php?page=wpfm-license' );
                $redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );

                wp_redirect( $redirect );
                exit();
            }

            // $license_data->license will be either "valid" or "invalid"
            update_option( 'wpfm_pro_license_status', $license_data->license );
            update_option( 'wpfm_is_premium', 'no' );
            update_option( "wpfm_pro_license_data", serialize( json_decode( json_encode( $license_data ), true ) ) );
            wp_redirect( admin_url( 'admin.php?page=wpfm-license' ) );
            exit();
        }
    }


    /**
     * @desc Get license key
     * @since 7.2.18
     * @return false|string
     */
    public function get_license() {
        $new = rex_feed_get_sanitized_get_post();
        $new = isset( $new[ 'post' ][ 'wpfm_pro_license_key' ] ) ? $new[ 'post' ][ 'wpfm_pro_license_key' ] : '';
        $new = trim( $new );
        $old = trim( get_option( 'wpfm_pro_license_key', '' ) );
        $old = ( new Rex_Product_Feed_Pro_License_Security() )->encrypt( $old );

        if ( $old ) {
            if ( $old !== $new ) {
                return $new;
            }
            return ( new Rex_Product_Feed_Pro_License_Security() )->decrypt( $old );
        }
        return $new;
    }

    /**
     * License related admin notices
     *
     * @return void
     */
	public function wpfm_pro_edd_admin_notices()
	{
		if (isset($_GET['sl_activation']) && !empty($_GET['message'])) {
			switch ($_GET['sl_activation']) {
				case 'false':
					$message = urldecode($_GET['message']);
					?>
                    <div class="error">
                        <p><?php echo $message; ?></p>
                    </div>
					<?php
					break;
				case 'true':
				default:
					break;
			}
		}

		$status = get_option('wpfm_pro_license_status');
		if ( !$status || 'deactivated' === $status ) {
			$msg = __( 'Please %1$sactivate your license%2$s key to enjoy the pro features for %3$s.', 'rex-product-feed-pro' );
			$msg = sprintf($msg, '<a href="' . admin_url('admin.php?page=wpfm-license">'), '</a>', '<strong>Product Feed Manager for WooCommerce Pro</strong>');
			echo '<div class="notice notice-error is-dismissible rex-feed-notice"><p>' . $msg . '</p></div>';
		}
	}


	public function wpfm_autoload_file_array($arr)
	{
		$arr[] = plugin_dir_path(dirname(__FILE__)) . 'vendor/autoload.php';
		return $arr;
	}


	/**
	 * Register weekly events
	 * @param $schedules
	 * @return mixed
	 */
	public function wpfm_weekly_cron_schedule($schedules)
	{
		$schedules['weekly'] = array(
			'interval' => 60 * 60 * 24 * 7, # 604,800, seconds in a week
			'display' => __( 'Weekly'));

		$schedules['monthly'] = array(
			'interval' => 60 * 60 * 24 * 30, # monthly
			'display' => __( 'Monthly'),
		);
		return $schedules;
	}

	/**
	 * Check if license is valid [in the background]
	 *
	 * @return void
	 */
    public function rex_wpfm_license_check_callback() {

	    /**
	     * Fires before the monthly license validation check is performed.
	     *
	     * This hook allows developers to execute custom code or actions
	     * just before WordPress or a plugin checks the validity of a monthly license.
	     *
	     * @since 6.4.6
	     */
		do_action( 'rexfeed_before_monthly_license_validation_check' );

        $license    = get_option( 'wpfm_pro_license_key', null );

        $api_params = array(
            'edd_action' => 'check_license',
            'license'    => $license,
            'item_id'    => WPFM_SL_ITEM_ID,
            'url'        => home_url()
        );

        // Call the custom API.
        $response = wp_remote_post( WPFM_SL_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

        // Make sure the response came back okay.
        if( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
            update_option( 'wpfm_is_premium', 'no' );
			update_option( 'wpfm_pro_license_status', 'invalid' );
            wp_die();
        }
        else {
            $license_data = json_decode( wp_remote_retrieve_body( $response ) );

            if( !$license_data->success ) {
                update_option( 'wpfm_is_premium', 'no' );
                update_option( 'wpfm_pro_license_status', $license_data->license );
                update_option( 'wpfm_pro_license_data', serialize( $license_data ) );
                wp_die();
            }
        }

        update_option( 'wpfm_is_premium', 'yes' );
        update_option( 'wpfm_pro_license_status', $license_data->license );
        update_option( 'wpfm_pro_license_data', serialize( $license_data ) );

	    /**
	     * Fires after the monthly license validation check is performed.
	     *
	     * This hook allows developers to execute custom code or actions
	     * just after WordPress or a plugin checks the validity of a monthly license.
	     *
	     * @since 6.4.6
	     */
	    do_action( 'rexfeed_after_monthly_license_validation_check' );

        wp_die();
    }


	/**
	 * This function runs when WordPress completes its upgrade process
	 * It iterates through each plugin updated to see if ours is included
	 * @param $upgrader_object Array
	 * @param $options Array
	 */
	public function wpfm_pro_update_process_completed_callback($upgrader_object, $options)
	{
		$our_plugin = plugin_basename(__FILE__);
		if ($options['action'] == 'update' && $options['type'] == 'plugin' && isset($options['plugins'])) {
			foreach ($options['plugins'] as $plugin) {
				if ($plugin == $our_plugin) {
					if (!wp_next_scheduled('rex_wpfm_license_check')) {
						wp_schedule_event(time(), 'monthly', 'rex_wpfm_license_check');
					}
				}
			}
		}
	}


	/**
	 * Add drm support
	 * @throws Exception
	 */
	public function wpfm_enable_drm_pixel()
	{
		global $product;
		$wpfm_fb_pixel_enabled = get_option('wpfm_drm_pixel_enabled', 'no');
		if ($wpfm_fb_pixel_enabled == 'yes') {
			$wpfm_drm_pixel_data = get_option('wpfm_drm_pixel_value');
			if (is_product()) {
				$ecomm_prodid = $product->get_id();
				$ecomm_pagetype = 'product';
				$price = $product->get_price();
				$ecomm_totalvalue = wc_format_decimal($price, wc_get_price_decimals());
				if (!empty($ecomm_prodid)) {
					if ($product->is_type('variable')) {
						$variation_id = $this->wpfm_find_matching_product_variation($product, $_GET);
						$total_get = count($_GET);
						if ($total_get > 0 && $variation_id > 0) {
							$variable_product = wc_get_product($variation_id);
							$ecomm_prodid = $variation_id;
							if (is_object($variable_product)) {
								$price = $variable_product->get_price();
								$ecomm_totalvalue = wc_format_decimal($price, wc_get_price_decimals());
							} else {
								$prices = $product->get_variation_prices();
								$lowest = reset($prices['price']);
								$ecomm_totalvalue = wc_format_decimal($lowest, wc_get_price_decimals());
							}
						} else {
							$prices = $product->get_variation_prices();
							$lowest = reset($prices['price']);
							$ecomm_totalvalue = wc_format_decimal($lowest, wc_get_price_decimals());

							$variation_ids = $product->get_visible_children();
							$product_ids = '';
							foreach ($variation_ids as $variation) {
								$product_ids .= "'" . $variation . "'" . ',';
							}
							$product_id = rtrim($product_ids, ",");
							$ecomm_prodid = "[" . $product_id . "]";
						}
					} else {
						$ecomm_totalvalue = wc_format_decimal($price, wc_get_price_decimals());
					}
				} ?>

                <script type="text/javascript">
                    var google_tag_params = {
                        ecomm_prodid: <?php echo "$ecomm_prodid";?>,
                        ecomm_pagetype: '<?php echo "$ecomm_pagetype";?>',
                        ecomm_totalvalue: <?php echo "$ecomm_totalvalue";?>,
                    };
                </script>

				<?php
			} elseif (is_cart() || is_checkout()) {
				if (is_checkout() && !empty(is_wc_endpoint_url('order-received'))) {
					$order_key = sanitize_text_field($_GET['key']);
					if (!empty($order_key)) {
						$order_id = wc_get_order_id_by_order_key($order_key);
						$order = wc_get_order($order_id);
						$order_items = $order->get_items();
						$order_real = 0;
						$ecomm_prodid = '';
						$product_ids = '';
						if (!is_wp_error($order_items)) {
							foreach ($order_items as $item_id => $order_item) {
								$prod_id = $order_item->get_product_id();
								$product_ids .= "'" . $prod_id . "'" . ',';
								$order_subtotal = $order_item->get_subtotal();
								$order_subtotal_tax = $order_item->get_subtotal_tax();
								$order_real += number_format(($order_subtotal + $order_subtotal_tax), 2);
							}
							$product_ids = rtrim($product_ids, ",");
							$ecomm_prodid = "[" . $product_ids . "]";
							$ecomm_totalvalue = wc_format_decimal($order_real, wc_get_price_decimals()); ?>
                            <script type="text/javascript">
                                var google_tag_params = {
                                    ecomm_prodid: <?php echo "$ecomm_prodid";?>,
                                    ecomm_pagetype: '<?php echo "purchase";?>',
                                    ecomm_totalvalue: '<?php echo $ecomm_totalvalue; ?>',
                                };
                            </script>
						<?php }
					}
				} else {
					$ecomm_prodid = '';
					$product_ids = '';
					$ecomm_totalvalue = 0;
					foreach (WC()->cart->get_cart() as $cart_item) {
						$product_id = $cart_item['product_id'];
						if ($cart_item['variation_id'] > 0) {
							$product_id = $cart_item['variation_id'];
						}
						$product_ids .= "'" . $product_id . "'" . ',';
						$line_total = $cart_item['line_total'];
						$line_tax = $cart_item['line_tax'];
						$ecomm_totalvalue += number_format(($line_total + $line_tax), 2);
					}
					$product_ids = rtrim($product_ids, ",");
					$ecomm_prodid = "[" . $product_ids . "]";
					$ecomm_totalvalue = wc_format_decimal($ecomm_totalvalue, wc_get_price_decimals()); ?>
                    <script type="text/javascript">
                        var google_tag_params = {
                            ecomm_prodid: <?php echo "$ecomm_prodid";?>,
                            ecomm_pagetype: '<?php echo "cart";?>',
                            ecomm_totalvalue: '<?php echo $ecomm_totalvalue; ?>',
                        };
                    </script>
				<?php }
			} else {
				?>
                <script type="text/javascript">
                    var google_tag_params = {
                        ecomm_pagetype: '<?php print "other";?>',
                    };
                </script>
			<?php } ?>
            <!-- Google-code – Remarketing tag added by RexTheme.com -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = <?php echo "$wpfm_drm_pixel_data";?>;
                var google_custom_params = window.google_tag_params;
                var google_remarketing_only = true;
                /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display:inline;">
                    <img height=”1″ width=”1″ style=”border-style:none;” alt=””
                         src=”//googleads.g.doubleclick.net/pagead/viewthroughconversion/XXXXXXXXXX/?value=0&amp;label=YYYYYYYYYY&amp;guid=ON&amp;script=0″/>
                </div>
            </noscript>
            <!-- End Google Remarketing Pixel Code -->
		<?php }
	}


	/**
	 * Find matching product variation
	 *
	 * @param WC_Product $product
	 * @param array $attributes
	 * @return int Matching variation ID or 0.
	 * @throws Exception
	 */
	protected function wpfm_find_matching_product_variation($product, $attributes)
	{
		foreach ($attributes as $key => $value) {
			if (strpos($key, 'attribute_') === 0) {
				continue;
			}
			unset($attributes[$key]);
			$attributes[sprintf('attribute_%s', $key)] = $value;
		}
		if (class_exists('WC_Data_Store')) {
			$data_store = WC_Data_Store::load('product');
			return $data_store->find_matching_product_variation($product, $attributes);

		} else {
			return $product->get_matching_variation($attributes);
		}
	}


	/**
	 * populate saved options
	 *
	 * @param $field_type_object
	 * @param $value
	 * @return string
	 */
	public function get_selected_products( $field_type_object, $value ) {
		$selected_items = '';
		$other_items = '';
		if($value) {
			foreach ($value as $product_id) {
				$product = wc_get_product( $product_id );
				if ( is_object( $product ) ) {
					$product_name = $product->get_formatted_name();
					$option = array(
						'value' => $product_id,
						'label' => $product_name,
					);
					$option['checked'] = true;
					$selected_items .= $field_type_object->select_option( $option );
				}
			}
			return $selected_items . $other_items;
		}
	}


    /**
     * Renders UPI by WPFM control page fields markups
     */
    public function render_upi_setting_fields_markups() {
        if( apply_filters('wpfm_is_premium', false) ) {
            $custom_field  = get_option( 'rex-wpfm-product-custom-field', 'no' );
            $custom_fields = get_option( 'wpfm_product_custom_fields_frontend', array() );
        ?>
            <div class="single-merchant wpfm-custom-field-frontend <?php if( $custom_field === 'no' ) echo 'is-hidden'; ?>">
                <span class="title"><?php echo __('Show WPFM Custom fields in Front-end [Single Product Page]', 'rex-product-feed-pro' ); ?></span>
                <form id="wpfm-frontend-fields" class="wpfm-frontend-fields">
                    <div class="wpfm-custom-fields">
                        <div class="single-meta-field">
                            <input id="wpfm_product_brand" type="checkbox" name="wpfm_product_custom_fields_frontend[]" value="brand" <?php if( in_array( 'brand', $custom_fields ) ) echo 'checked';?>>
                            <span class="brand"><?php echo __('Brand', 'rex-product-feed-pro' ); ?></span>
                        </div>
                        <div class="single-meta-field">
                            <input id="wpfm_product_gtin" type="checkbox" name="wpfm_product_custom_fields_frontend[]" value="gtin" <?php if( in_array( 'gtin', $custom_fields ) ) echo 'checked';?>>
                            <span class="gtin"><?php echo __('GTIN', 'rex-product-feed-pro' ); ?></span>
                        </div>
                        <div class="single-meta-field">
                            <input id="wpfm_product_mpn" type="checkbox" name="wpfm_product_custom_fields_frontend[]" value="mpn" <?php if( in_array( 'mpn', $custom_fields ) ) echo 'checked';?>>
                            <span class="mpn"><?php echo __('MPN', 'rex-product-feed-pro' ); ?></span>
                        </div>
                        <div class="single-meta-field">
                            <input id="wpfm_product_upc" type="checkbox" name="wpfm_product_custom_fields_frontend[]" value="upc" <?php if( in_array( 'upc', $custom_fields ) ) echo 'checked';?>>
                            <span class="upc"><?php echo __('UPC', 'rex-product-feed-pro' ); ?></span>
                        </div>
                        <div class="single-meta-field">
                            <input id="wpfm_product_ean" type="checkbox" name="wpfm_product_custom_fields_frontend[]" value="ean" <?php if( in_array( 'ean', $custom_fields ) ) echo 'checked';?>>
                            <span class="ean"><?php echo __('EAN', 'rex-product-feed-pro' ); ?></span>
                        </div>
                        <div class="single-meta-field">
                            <input id="wpfm_product_jan" type="checkbox" name="wpfm_product_custom_fields_frontend[]" value="jan" <?php if( in_array( 'jan', $custom_fields ) ) echo 'checked';?>>
                            <span class="jan"><?php echo __('JAN', 'rex-product-feed-pro' ); ?></span>
                        </div>
                        <div class="single-meta-field">
                            <input id="wpfm_product_isbn" type="checkbox" name="wpfm_product_custom_fields_frontend[]" value="itf" <?php if( in_array( 'itf', $custom_fields ) ) echo 'checked';?>>
                            <span class="isbn"><?php echo __('ISBN', 'rex-product-feed-pro' ); ?></span>
                        </div>
                        <div class="single-meta-field">
                            <input id="wpfm_product_itf" type="checkbox" name="wpfm_product_custom_fields_frontend[]" value="itf" <?php if( in_array( 'itf', $custom_fields ) ) echo 'checked';?>>
                            <span class="itf"><?php echo __('ITF14', 'rex-product-feed-pro' ); ?></span>
                        </div>
                        <div class="single-meta-field">
                            <input id="wpfm_product_offer_price" type="checkbox" name="wpfm_product_custom_fields_frontend[]" value="offer_price" <?php if( in_array( 'offer_price', $custom_fields ) ) echo 'checked';?>>
                            <span class="offer-price"><?php echo __('Offer Price', 'rex-product-feed-pro' ); ?></span>
                        </div>
                        <div class="single-meta-field">
                            <input id="wpfm_product_effective_date" type="checkbox" name="wpfm_product_custom_fields_frontend[]" value="offer_effective_date" <?php if( in_array( 'offer_effective_date', $custom_fields ) ) echo 'checked';?>>
                            <span class="effective-date"><?php echo __('Effective Date', 'rex-product-feed-pro' ); ?></span>
                        </div>
                        <div class="single-meta-field">
                            <input id="wpfm_product_additional_info" type="checkbox" name="wpfm_product_custom_fields_frontend[]" value="additional_info" <?php if( in_array( 'additional_info', $custom_fields ) ) echo 'checked';?>>
                            <span class="additional-info"><?php echo __('Additional Info', 'rex-product-feed-pro' ); ?></span>
                        </div>
                    </div>
                    <button type="submit" class="save-wpfm-fields-show">
                        <span>save</span>
                        <i class="fa fa-spinner fa-pulse fa-fw"></i>
                    </button>
                </form>
            </div>
        <?php
        }
    }


    /**
     * @desc Add WPFM Custom Fields to WooCommerce
     * CSV Products Import Attribute Lists
     *
     * @since 7.3.0
     * @param $options
     * @param $items
     * @return mixed
     */
    public function update_attribute_list( $options, $items ) {
        $options[ 'wpfm_custom_attr' ] = array(
            'name' => 'WPFM Custom Attributes',
            'options' => array(
                'meta:_wpfm_product_brand' => 'WPFM Product Brand',
                'meta:_wpfm_product_gtin' => 'WPFM Product GTIN',
                'meta:_wpfm_product_mpn' => 'WPFM Product MPN',
                'meta:_wpfm_product_upc' => 'WPFM Product UPC',
                'meta:_wpfm_product_ean' => 'WPFM Product EAN',
                'meta:_wpfm_product_jan' => 'WPFM Product JAN',
                'meta:_wpfm_product_isbn' => 'WPFM Product ISBN',
                'meta:_wpfm_product_itf' => 'WPFM Product ITF14',
                'meta:_wpfm_product_offer_price' => 'WPFM Product Offer Price',
                'meta:_wpfm_product_offer_effective_date' => 'WPFM Product Offer Effective Date',
                'meta:_wpfm_product_additional_info' => 'WPFM Product Additional Info',
                'meta:_wpfm_product_color' => 'WPFM Product Color',
                'meta:_wpfm_product_size' => 'WPFM Product Size',
                'meta:_wpfm_product_pattern' => 'WPFM Product Pattern',
                'meta:_wpfm_product_material' => 'WPFM Product Material',
                'meta:_wpfm_product_age_group' => 'WPFM Product Age Group',
                'meta:_wpfm_product_gender' => 'WPFM Product Gender'
            )
        );
        return $options;
    }


    /**
     * @desc Renders pro features overview in feed edit page
     * @return void
     */
    public function render_pro_features_overview() {
        require_once plugin_dir_path( __FILE__ ) . 'partials/rex-product-feed-pro-features-overview.php';
    }


    /**
     * @desc Renders Combined fields attribute markup
     *
     * @param $feed_template
     * @param $key
     * @param $item
     * @return void
     */
    public function render_combined_fields_attr_markup( $feed_template, $key, $item ) {
        $hide_combined = 'style="display: none"';
        if ( isset( $item[ 'type' ] ) && 'combined' === $item[ 'type' ] ) {
            $hide_combined = '';
        }
        ?>
        <div class="combined-dropdown" <?php echo filter_var( $hide_combined )?>>
            <input type="text" name="fc[<?php echo esc_attr( $key )?>][<?php echo esc_attr( 'combined_key' )?>]" value="<?php echo trim( filter_var( isset( $item[ 'combined_key' ] ) ? $item[ 'combined_key' ] : '' ) )?>" class="disable-custom-id-dropdown attribute-val-static-field" readonly>
            <?php
            echo '<select class="combined-attr-val-dropdown" style="display: none">';
            echo "<option value=''>".esc_html__('Please Select', 'rex-product-feed-pro' )."</option>";
            echo $feed_template->print_product_attributes(); // phpcs:ignore
            echo "</select>";
            ?>
        </div>
        <?php
    }


    /**
     * @desc Add attribute separators in attributes dropdown
     *
     * @param $attributes
     * @return array|string[][]
     */
    public function add_combined_fileds_separators( $attributes ) {
        $separators = array(
                'Attributes Separator' => array(
                        '-' => 'Hyphen (-)',
                        ':' => 'Colon (:)',
                        ';' => 'Semi-Colon (;)',
                        ',' => 'Comma (,)',
                        '+' => 'Plus (+)',
                )
        );
        return array_merge( $separators, $attributes  );
    }


    /**
     * @desc Notify user by sending an email if there is an error in the feed generation process
     * @since 6.2.1
     * @param $is_valid
     * @param $xml
     * @param $errors
     * @param $feed_id
     * @return void
     */
    public function notify_user( $is_valid, $xml, $errors, $feed_id ) {
        $user_email = get_option( 'wpfm_user_email', '' );

        if ( !$is_valid && '' !== $user_email ) {
            if ( !class_exists('WC_Email', false) && defined('WC_ABSPATH') ) {
                include_once WC_ABSPATH . 'includes/emails/class-wc-email.php';
            }

            $feed_url = esc_url( admin_url( 'post.php?post=' . $feed_id . '&action=edit' ) );
            $wc_email = new WC_Email();
            $subject = 'Error on Feed Generation - ' . $feed_id;
            $body = 'There was an error when generating your feed. Read the details below:';
            $body .= '<br><br>Feed Title: ' . get_the_title( $feed_id );
            $body .= '<br>Feed URL: <a href="' . $feed_url . '" target="_blank"> ' . $feed_url .  ' </a>';

            foreach ( $errors as $error ) {
                $body .= isset( $error->message ) ? '<br><br>Error: ' . $error->message : '';
                $body .= isset( $error->file ) ? '<br>File: ' . $error->file : '';
                $body .= isset( $error->line ) ? '<br>Line: ' . $error->line : '';
            }

            $wc_email->send( $user_email, $subject, $body, $wc_email->get_headers(), $wc_email->get_attachments() );
        }
        return $is_valid;
    }


    /**
     * @desc Render export/import markups in plugins settings/controls page
     * @since 6.3.0
     * @return void
     */
    public function render_export_import_feed_fields() {
        ?>

        <div class="single-merchant detailed-product export">
            <span class="title"><?php echo esc_html__('Export Feeds', 'rex-product-feed-pro' ); ?></span>
			<div class="single-merchant-button-area">
				<button id="rex_feed_export_feed_btn" class="wpfm-export">
					<span><?php echo esc_html__('Export', 'rex-product-feed-pro' ); ?></span>
				</button>
				<a id="rex-feed-download-exported-feed-url" download></a>
			</div>
        </div>

        <div class="single-merchant import">
            <span class="title"><?php echo esc_html__('Import Feeds', 'rex-product-feed-pro' ); ?></span>

            <div class="single-input-area">
                <form id="rex_feed_import_feed_form" enctype="multipart/form-data" method="POST">
                    <input type="file" accept="text/xml" id="rex_feed_import_feed" name="rex_feed_import_feed" />
                    <button id="rex_feed_import_feed_btn" class="wpfm-import" type="submit">
                        <span><?php echo esc_html__('Import', 'rex-product-feed-pro' ); ?></span>
						<i class="fa fa-spinner fa-pulse fa-fw"></i>
                    </button>
                </form>
                <p id="rex_feed_export_import_message"></p>
            </div>

        </div>

        <?php

        $args = [
            'post_type'      => 'product-feed',
            'fields'         => 'ids',
            'posts_per_page' => -1,
        ];
        $feed_ids = get_posts( $args );

        require_once plugin_dir_path(__FILE__) . '/partials/rex-product-feed-pro-export-popup.php';
    }


    /**
     * @desc Update option if any WooCommerce product is update
     * @since 6.3.1
     * @param $id
     * @return void
     */
    public function update_product_changed_status( $data ) {
        if ( is_numeric( $data ) && ( 'product' === get_post_type( $data ) || 'shop_order' === get_post_type( $data ) ) ) {
            update_option( 'rex_feed_wc_product_updated', true );
        }
        elseif ( 'rex_feed_cron_jobs_completed' === $data ) {
            delete_option( 'rex_feed_wc_product_updated' );
        }
    }


    /**
     * @desc Render markups for Refresh only on product update
     * @since 6.3.1
     * @return void
     */
    public function render_product_update_cron_markups() {
        ?>
        <div class="rex_feed_update_on_product_change">
            <label for="rex_feed_update_on_product_change"><?php esc_html_e('Regenerate only when products changed', 'rex-product-feed-pro' )?>
                <span class="rex_feed-tooltip">
                    <?php include WPFM_PLUGIN_ASSETS_FOLDER_PATH . 'icon/icon-svg/icon-question.php';?>
                    <p>
						<?php esc_html_e( 'Auto-Generate feed only if any WooCommerce product data is updated', 'rex-product-feed-pro' ); ?>
						<a href="<?php echo esc_url( 'https://rextheme.com/docs/update-your-feed-only-if-any-changes-were-made-to-any-of-your-products/' )?>" target="_blank"><?php esc_html_e('Learn How', 'rex-product-feed-pro' )?></a>
					</p>
                </span>
            </label>
            <?php
            $update_on_pr_change = get_post_meta( get_the_ID(), '_rex_feed_update_on_product_change', true ) ?: get_post_meta( get_the_ID(), 'rex_feed_update_on_product_change', true );
            $checked = 'yes' === $update_on_pr_change ? ' checked' : '';
            ?>
            <input type="checkbox" id="rex_feed_update_on_product_change" name="rex_feed_update_on_product_change" value="yes" <?php echo esc_attr( $checked );?>>
        </div>
        <?php
    }


    /**
     * @desc Save feed setting options
     * @since 6.3.1
     * @param $feed_configs
     * @return void
     */
    public function save_feed_settings_option( $feed_id, $feed_configs ) {
        if ( isset( $feed_configs[ 'rex_feed_update_on_product_change' ] ) ) {
            $opt_val = 'yes' === $feed_configs[ 'rex_feed_update_on_product_change' ] ? 'yes' : 'no';
            update_post_meta( $feed_id, '_rex_feed_update_on_product_change', $opt_val );
        }
        else {
            delete_post_meta( $feed_id, '_rex_feed_update_on_product_change' );
            delete_post_meta( $feed_id, 'rex_feed_update_on_product_change' );
        }

		if( isset( $feed_configs[ 'rex_feed_schedule' ], $feed_configs[ 'rex_feed_custom_time' ] ) && 'custom' === $feed_configs[ 'rex_feed_schedule' ] ) {
			update_post_meta( $feed_id, '_rex_feed_custom_time', $feed_configs[ 'rex_feed_custom_time' ] );
		}
		else {
			delete_post_meta( $feed_id, '_rex_feed_custom_time' );
		}
		delete_post_meta( $feed_id, 'rex_feed_custom_time' );
    }

	/**
	 * Add a custom interval option to the auto-feed generation settings.
	 *
	 * This function extends the auto-feed generation options by adding a custom interval option.
	 * It also hooks into the rendering of a custom schedule time dropdown field.
	 *
	 * @param array $auto_feed_generation_options An array of auto-feed generation options.
	 * @return array Updated array of auto-feed generation options including the 'Custom' option.
	 * @since 6.4.6
	 */
	public function add_custom_interval_option( $auto_feed_generation_options ) {
		$auto_feed_generation_options[ 'custom' ] = __( 'Custom', 'rex-product-feed' );
		return $auto_feed_generation_options;
	}

	/**
	 * Render a custom schedule time dropdown field in a WordPress post editor.
	 *
	 * This function generates an HTML dropdown field for selecting a custom schedule time in the 12-hour format.
	 * It is typically used within a WordPress post editor for configuring scheduling options.
	 *
	 * @return void Outputs the HTML dropdown field for custom schedule time.
	 * @since 6.4.6
	 */
	public function render_custom_schedule_time_dropdown() {
		echo '<li class="rex_feed_custom_time_fields">';

		$selected_hour = get_post_meta( get_the_ID(), '_rex_feed_custom_time', true );
		$selected_hour = $selected_hour ?: get_post_meta( get_the_ID(), 'rex_feed_custom_time', true );

		echo '<select id="rex_feed_custom_time" name="rex_feed_custom_time">';
		for ( $i =1; $i <=24; $i++ ) {
			if ( 12 === (int) $i ) {
				$twelve_hr_format = '12 PM';
			}
			elseif ( 24 === (int) $i ) {
				$twelve_hr_format = '12 AM';
			}
			elseif ( (int) $i > 12 ) {
				$hr               = (int) $i - 12;
				$twelve_hr_format = $hr . ' PM';
			}
			else {
				$twelve_hr_format = $i . ' AM';
			}
			$selected = (int) $selected_hour === $i ? ' selected' : '';
			echo '<option value="' . esc_attr( $i ) . '" ' . esc_html( $selected ) . '>' . esc_attr( $twelve_hr_format ) . '</option>';
		}
		echo '</select>';
		echo '<label for="rex_feed_custom_time">' . esc_html__( 'Every Day', 'rex-product-feed' ) . '</label>';
		echo '</li>';
	}


    /**
     * @desc Add License submenu
     * @since 7.2.18
     * @param $submenu
     * @return false|string
     */
    public function add_license_submenu( $submenu ) {
        return add_submenu_page(
            'product-feed',
            __('License', 'rex-product-feed-pro' ),
            __('License', 'rex-product-feed-pro' ),
            'manage_options',
            'wpfm-license',
            __CLASS__ . '::license_sub_menu_render'
        );
    }


    /**
     * @desc Include license page in the submenu
     * @since 7.2.18
     * @return void
     */
    public static function license_sub_menu_render()
    {
        require plugin_dir_path(__FILE__) . '/partials/rex-product-feed-pro-license.php';
    }


    /**
     * @desc Include feed rules markup page
     * @since 7.2.20
     * @return void
     */
    public static function render_feed_rules_markups()
    {
        if( apply_filters('wpfm_is_premium', false) ) {
            include_once plugin_dir_path( __FILE__ ) . '/partials/rex-product-feed-pro-feed-rules-markups.php';
        }
    }


     /**
     * Fix the default json-ld structure
     *
     * @param $markup
     * @param $product
     * @return array|void
     * @throws Exception
     * @since 7.2.20
     */
    public function wpfm_structured_data( $markup, $product ) {
        if( !is_object( $product ) ) {
            global $product;
        }
        if( !is_a( $product, 'WC_Product' ) ) {
            return;
        }
        if( !is_product() ) {
            return;
        }

        $rex_wpfm_product_structured_data = get_option( 'rex-wpfm-product-structured-data' );

        if( $rex_wpfm_product_structured_data === 'yes' ) {
            $exclude_tax = get_option( 'rex-wpfm-product-structured-data-exclude-tax' );
            $shop_url    = home_url();
            $currency    = get_woocommerce_currency();
            $permalink   = get_permalink( $product->get_id() );
            $link        = isset( $_SERVER[ 'HTTPS' ] ) ? $_SERVER[ 'HTTPS' ] === 'on' ? 'https' : 'http' : 'http';
            $link        .= "://";
            $link        .= isset( $_SERVER[ 'HTTP_HOST' ] ) ? $_SERVER[ 'HTTP_HOST' ] : '';
            $link        .= isset( $_SERVER[ 'REQUEST_URI' ] ) ? $_SERVER[ 'REQUEST_URI' ] : '';
            $shop_name   = get_bloginfo( 'name' );

            if( $product->is_type( 'variable' ) ) {
                /**
                 * Check if $_GET parameter is enabled on product page
                 * if available this is a specific variation
                 */
                $get_nr = count( $_GET );
                if( $get_nr > 0 ) {
                    $variation_id = wpfm_find_matching_product_variation( wc_get_product(), $_GET );
                    if( isset( $variation_id ) ) {
                        $variation_product = wc_get_product( $variation_id );
                        if( is_object( $variation_product ) ) {
                            $markup = array(
                                "@context"    => "https://www.schema.org",
                                '@id'         => $link . '#product', // Append '#product' to differentiate between this @id and the @id generated for the Breadcrumblist.
                                '@type'       => 'Product',
                                'name'        => $variation_product->get_name(),
                                'url'         => $link,
                                'description' => wp_strip_all_tags( do_shortcode( $product->get_short_description() ? $product->get_short_description() : $product->get_description() ) ),
                            );

                            // attach image
                            $image = wp_get_attachment_url( $product->get_image_id() );
                            if( $image ) {
                                $markup[ 'image' ] = $image;
                            }

                            // attach brand
                            $brand = get_post_meta( $variation_product->get_parent_id(), '_wpfm_product_brand' );
                            if( $brand ) {
                                $markup[ 'brand' ] = array(
                                    '@type' => 'Thing',
                                    'name'  => $brand,
                                );
                            }

                            // attach mpn
                            $mpn = get_post_meta( $variation_id, '_wpfm_product_mpn', true );
                            if( $mpn ) {
                                $markup[ 'mpn' ] = $mpn;
                            }

                            // attach gtin
                            $gtin = get_post_meta( $variation_id, '_wpfm_product_gtin', true );
                            if( $gtin ) {
                                $gtin_length = strlen( $gtin );
                                if( $gtin_length == 14 ) {
                                    $markup[ 'gtin14' ] = $gtin;
                                }
                                elseif( $gtin_length == 13 ) {
                                    $markup[ 'gtin13' ] = $gtin;
                                }
                                elseif( $gtin_length == 12 ) {
                                    $markup[ 'gtin12' ] = $gtin;
                                }
                                elseif( $gtin_length == 8 ) {
                                    $markup[ 'gtin8' ] = $gtin;
                                }
                            }

                            // get sku
                            if( $variation_product->get_sku() ) {
                                $markup[ 'sku' ] = $variation_product->get_sku();
                            }
                            else {
                                $markup[ 'sku' ] = $variation_product->get_id();
                            }

                            // get the five ratings
                            if( $product->get_rating_count() && wc_review_ratings_enabled() ) {
                                $markup[ 'aggregateRating' ] = array(
                                    '@type'       => 'AggregateRating',
                                    'ratingValue' => $product->get_average_rating(),
                                    'reviewCount' => $product->get_review_count(),
                                );

                                $comments = get_comments(
                                    array(

                                        'number'      => 5,
                                        'post_id'     => $product->get_id(),
                                        'status'      => 'approve',
                                        'post_status' => 'publish',
                                        'post_type'   => 'product',
                                        'parent'      => 0,
                                        'meta_query'  => array(
                                            array(
                                                'key'     => 'rating',
                                                'type'    => 'NUMERIC',
                                                'compare' => '>',
                                                'value'   => 0,
                                            ),
                                        ),
                                    )
                                );

                                if( $comments ) {
                                    $markup[ 'review' ] = array();
                                    foreach( $comments as $comment ) {
                                        $markup[ 'review' ][] = array(
                                            '@type'         => 'Review',
                                            'reviewRating'  => array(
                                                '@type'       => 'Rating',
                                                'ratingValue' => get_comment_meta( $comment->comment_ID, 'rating', true ),
                                            ),
                                            'author'        => array(
                                                '@type' => 'Person',
                                                'name'  => get_comment_author( $comment ),
                                            ),
                                            'reviewBody'    => get_comment_text( $comment ),
                                            'datePublished' => get_comment_date( 'c', $comment ),
                                        );
                                    }
                                }
                            }

                            $price_valid_until = date( 'Y-12-31', current_time( 'timestamp', true ) + YEAR_IN_SECONDS );
                            $stock_status      = $variation_product->get_stock_status();
                            if( $stock_status == "outofstock" ) {
                                $availability = "OutOfStock";
                            }
                            else {
                                $availability = "InStock";
                            }
                            if( $variation_product->is_on_sale() && $variation_product->get_date_on_sale_to() ) {
                                $price_valid_until = date( 'Y-m-d', $variation_product->get_date_on_sale_to()
                                                                                      ->getTimestamp() );
                            }

                            $regular_price = $variation_product->get_regular_price();
                            $sale_price    = $variation_product->get_sale_price();

                            if( 'no' === get_option( 'woocommerce_prices_include_tax' ) && 'no' === $exclude_tax ) {
                                $tax_rates = WC_Tax::get_base_tax_rates( $product->get_tax_class() );

                                if( isset( $tax_rates[ 0 ][ 'rate' ] ) ) {
                                    $regular_price = (float)$regular_price + ( (float)$regular_price * (float)$tax_rates[ 0 ][ 'rate' ] ) / 100;
                                    $sale_price    = (float)$sale_price + ( (float)$sale_price * (float)$tax_rates[ 0 ][ 'rate' ] ) / 100;
                                }
                                if( isset( $tax_rates[ 1 ][ 'rate' ] ) ) {
                                    $regular_price = (float)$regular_price + ( (float)$regular_price * (float)$tax_rates[ 1 ][ 'rate' ] ) / 100;
                                    $sale_price    = (float)$sale_price + ( (float)$sale_price * (float)$tax_rates[ 1 ][ 'rate' ] ) / 100;
                                }
                            }

                            $regular_price = wc_format_decimal( $regular_price, wc_get_price_decimals() );
                            $sale_price    = wc_format_decimal( $sale_price, wc_get_price_decimals() );

                            $markup[ 'offers' ][ 0 ] = array(
                                '@type'              => 'Offer',
                                'price'              => $sale_price,
                                'priceValidUntil'    => $price_valid_until,
                                'priceSpecification' => array(
                                    '@type'                 => 'PriceSpecification',
                                    'price'                 => $regular_price,
                                    'priceCurrency'         => $currency,
                                    'valueAddedTaxIncluded' => wc_prices_include_tax() ? 'true' : 'false',
                                ),
                                'priceCurrency'      => $currency,
                                'itemCondition'      => 'https://schema.org/NewCondition',
                                'availability'       => 'https://schema.org/' . $availability,
                                'sku'                => $variation_product->get_sku(),
                                'image'              => $image,
                                'description'        => $product->get_description(),
                                'seller'             => array(
                                    '@type' => 'Organization',
                                    'name'  => $shop_name,
                                    'url'   => $shop_url,
                                ),
                                'url'                => $link
                            );
                        }
                    }
                }
                else {
                    $markup = array(
                        '@type'       => 'Product',
                        '@id'         => $permalink . '#product', // Append '#product' to differentiate between this @id and the @id generated for the Breadcrumblist.
                        'name'        => $product->get_name(),
                        'url'         => $link,
                        'description' => wp_strip_all_tags( do_shortcode( $product->get_short_description() ? $product->get_short_description() : $product->get_description() ) ),
                    );

                    // attach image
                    $image = wp_get_attachment_url( $product->get_image_id() );
                    if( $image ) {
                        $markup[ 'image' ] = $image;
                    }

                    // attach brand
                    $brand = get_post_meta( $product->get_id(), '_wpfm_product_brand', true );
                    if( $brand ) {
                        $markup[ 'brand' ] = array(
                            '@type' => 'Thing',
                            'name'  => $brand,
                        );
                    }

                    // attach mpn
                    $mpn = get_post_meta( $product->get_id(), '_wpfm_product_mpn', true );

                    if( $mpn ) {
                        $markup[ 'mpn' ] = $mpn;
                    }

                    // attach gtin
                    $gtin = get_post_meta( $product->get_id(), '_wpfm_product_gtin', true );
                    if( $gtin ) {
                        $gtin_length = strlen( $gtin );
                        if( $gtin_length == 14 ) {
                            $markup[ 'gtin14' ] = $gtin;
                        }
                        elseif( $gtin_length == 13 ) {
                            $markup[ 'gtin13' ] = $gtin;
                        }
                        elseif( $gtin_length == 12 ) {
                            $markup[ 'gtin12' ] = $gtin;
                        }
                        elseif( $gtin_length == 8 ) {
                            $markup[ 'gtin8' ] = $gtin;
                        }
                    }


                    // get sku
                    if( $product->get_sku() ) {
                        $markup[ 'sku' ] = $product->get_sku();
                    }
                    else {
                        $markup[ 'sku' ] = $product->get_id();
                    }

                    // get the five ratings
                    if( $product->get_rating_count() && wc_review_ratings_enabled() ) {
                        $markup[ 'aggregateRating' ] = array(
                            '@type'       => 'AggregateRating',
                            'ratingValue' => $product->get_average_rating(),
                            'reviewCount' => $product->get_review_count(),
                        );

                        $comments = get_comments(
                            array(
                                'number'      => 5,
                                'post_id'     => $product->get_id(),
                                'status'      => 'approve',
                                'post_status' => 'publish',
                                'post_type'   => 'product',
                                'parent'      => 0,
                                'meta_query'  => array(
                                    array(
                                        'key'     => 'rating',
                                        'type'    => 'NUMERIC',
                                        'compare' => '>',
                                        'value'   => 0,
                                    ),
                                ),
                            )
                        );

                        if( $comments ) {
                            $markup[ 'review' ] = array();
                            foreach( $comments as $comment ) {
                                $markup[ 'review' ][] = array(
                                    '@type'         => 'Review',
                                    'reviewRating'  => array(
                                        '@type'       => 'Rating',
                                        'ratingValue' => get_comment_meta( $comment->comment_ID, 'rating', true ),
                                    ),
                                    'author'        => array(
                                        '@type' => 'Person',
                                        'name'  => get_comment_author( $comment ),
                                    ),
                                    'reviewBody'    => get_comment_text( $comment ),
                                    'datePublished' => get_comment_date( 'c', $comment ),
                                );
                            }
                        }
                    }

                    $price_valid_until = '';
                    if( $product && $product->is_on_sale() && $product->get_date_on_sale_to() ) {
                        $price_valid_until = date( 'Y-m-d', $product->get_date_on_sale_to()->getTimestamp() );
                    }

                    $regular_price = $product ? $product->get_regular_price() : '';
                    $sale_price    = $product ? $product->get_sale_price() : '';

                    if( 'no' === get_option( 'woocommerce_prices_include_tax' ) && 'no' === $exclude_tax ) {
                        $tax_rates = WC_Tax::get_base_tax_rates( $product ? $product->get_tax_class() : '' );

                        if( isset( $tax_rates[ 0 ][ 'rate' ] ) ) {
                            $regular_price = (float)$regular_price + ( (float)$regular_price * (float)$tax_rates[ 0 ][ 'rate' ] ) / 100;
                            $sale_price    = (float)$sale_price + ( (float)$sale_price * (float)$tax_rates[ 0 ][ 'rate' ] ) / 100;
                        }
                        if( isset( $tax_rates[ 1 ][ 'rate' ] ) ) {
                            $regular_price = (float)$regular_price + ( (float)$regular_price * (float)$tax_rates[ 1 ][ 'rate' ] ) / 100;
                            $sale_price    = (float)$sale_price + ( (float)$sale_price * (float)$tax_rates[ 1 ][ 'rate' ] ) / 100;
                        }
                    }

                    $regular_price = wc_format_decimal( $regular_price, wc_get_price_decimals() );
                    $sale_price    = wc_format_decimal( $sale_price, wc_get_price_decimals() );

                    $markup[ 'offers' ][ 0 ] = array(
                        '@type'              => 'Offer',
                        'price'              => $sale_price,
                        'priceValidUntil'    => $price_valid_until ?: '',
                        'priceCurrency'      => $currency,
                        'priceSpecification' => array(
                            '@type'                 => 'PriceSpecification',
                            'price'                 => $regular_price,
                            'priceCurrency'         => $currency,
                            'valueAddedTaxIncluded' => wc_prices_include_tax() ? 'true' : 'false',
                        ),
                        'itemCondition'      => 'https://schema.org/NewCondition',
                        'availability'       => 'https://schema.org/' . $stock = ( $product->is_in_stock() ? 'InStock' : 'OutOfStock' ),
                        'sku'                => $product->get_sku(),
                        'image'              => wp_get_attachment_url( $product->get_image_id() ),
                        'description'        => $product->get_description(),
                        'seller'             => array(
                            '@type' => 'Organization',
                            'name'  => $shop_name,
                            'url'   => $shop_url,
                        ),
                        'url'                => $link
                    );
                }
            }
            else {
                $markup = array(
                    '@type'       => 'Product',
                    '@id'         => $permalink . '#product', // Append '#product' to differentiate between this @id and the @id generated for the Breadcrumblist.
                    'name'        => $product->get_name(),
                    'url'         => $link,
                    'description' => wp_strip_all_tags( do_shortcode( $product->get_short_description() ? $product->get_short_description() : $product->get_description() ) ),
                );

                // attach image
                $image = wp_get_attachment_url( $product->get_image_id() );
                if( $image ) {
                    $markup[ 'image' ] = $image;
                }

                // attach brand
                if( $product->get_type() == 'variation' ) {
                    $brand = get_post_meta( $product->get_parent_id(), '_wpfm_product_brand' );
                }
                else {
                    $brand = get_post_meta( $product->get_id(), '_wpfm_product_brand' );
                }

                if( $brand ) {
                    $markup[ 'brand' ] = array(
                        '@type' => 'Thing',
                        'name'  => $brand,
                    );
                }

                // attach mpn
                $mpn = get_post_meta( $product->get_id(), '_wpfm_product_mpn', true );

                if( $mpn ) {
                    $markup[ 'mpn' ] = $mpn;
                }

                // attach gtin
                $gtin = get_post_meta( $product->get_id(), '_wpfm_product_gtin', true );

                if( $gtin ) {
                    $gtin_length = strlen( $gtin );
                    if( $gtin_length == 14 ) {
                        $markup[ 'gtin14' ] = $gtin;
                    }
                    elseif( $gtin_length == 13 ) {
                        $markup[ 'gtin13' ] = $gtin;
                    }
                    elseif( $gtin_length == 12 ) {
                        $markup[ 'gtin12' ] = $gtin;
                    }
                    elseif( $gtin_length == 8 ) {
                        $markup[ 'gtin8' ] = $gtin;
                    }
                }


                // get sku
                if( $product->get_sku() ) {
                    $markup[ 'sku' ] = $product->get_sku();
                }
                else {
                    $markup[ 'sku' ] = $product->get_id();
                }

                // get the five ratings
                if( $product->get_rating_count() && wc_review_ratings_enabled() ) {
                    $markup[ 'aggregateRating' ] = array(
                        '@type'       => 'AggregateRating',
                        'ratingValue' => $product->get_average_rating(),
                        'reviewCount' => $product->get_review_count(),
                    );

                    $comments = get_comments(
                        array(
                            'number'      => 5,
                            'post_id'     => $product->get_id(),
                            'status'      => 'approve',
                            'post_status' => 'publish',
                            'post_type'   => 'product',
                            'parent'      => 0,
                            'meta_query'  => array(
                                array(
                                    'key'     => 'rating',
                                    'type'    => 'NUMERIC',
                                    'compare' => '>',
                                    'value'   => 0,
                                ),
                            ),
                        )
                    );

                    if( $comments ) {
                        $markup[ 'review' ] = array();
                        foreach( $comments as $comment ) {
                            $markup[ 'review' ][] = array(
                                '@type'         => 'Review',
                                'reviewRating'  => array(
                                    '@type'       => 'Rating',
                                    'ratingValue' => get_comment_meta( $comment->comment_ID, 'rating', true ),
                                ),
                                'author'        => array(
                                    '@type' => 'Person',
                                    'name'  => get_comment_author( $comment ),
                                ),
                                'reviewBody'    => get_comment_text( $comment ),
                                'datePublished' => get_comment_date( 'c', $comment ),
                            );
                        }
                    }
                }

                $price_valid_until = '';
                if( $product && $product->is_on_sale() && $product->get_date_on_sale_to() ) {
                    $price_valid_until = date( 'Y-m-d', $product->get_date_on_sale_to()->getTimestamp() );
                }

                $regular_price = $product->get_regular_price();
                $sale_price    = $product->get_sale_price();

                if( 'no' === get_option( 'woocommerce_prices_include_tax' ) && 'no' === $exclude_tax ) {
                    $tax_rates = WC_Tax::get_base_tax_rates( $product->get_tax_class() );

                    if( isset( $tax_rates[ 0 ][ 'rate' ] ) ) {
                        $regular_price = (float)$regular_price + ( (float)$regular_price * (float)$tax_rates[ 0 ][ 'rate' ] ) / 100;
                        $sale_price    = (float)$sale_price + ( (float)$sale_price * (float)$tax_rates[ 0 ][ 'rate' ] ) / 100;
                    }
                    if( isset( $tax_rates[ 1 ][ 'rate' ] ) ) {
                        $regular_price = (float)$regular_price + ( (float)$regular_price * (float)$tax_rates[ 1 ][ 'rate' ] ) / 100;
                        $sale_price    = (float)$sale_price + ( (float)$sale_price * (float)$tax_rates[ 1 ][ 'rate' ] ) / 100;
                    }
                }

                $regular_price = wc_format_decimal( $regular_price, wc_get_price_decimals() );
                $sale_price    = wc_format_decimal( $sale_price, wc_get_price_decimals() );

                $markup[ 'offers' ][ 0 ] = array(
                    '@type'              => 'Offer',
                    'price'              => $sale_price,
                    'priceValidUntil'    => $price_valid_until,
                    'priceCurrency'      => $currency,
                    'priceSpecification' => array(
                        '@type'                 => 'PriceSpecification',
                        'price'                 => $regular_price,
                        'priceCurrency'         => $currency,
                        'valueAddedTaxIncluded' => wc_prices_include_tax() ? 'true' : 'false',
                    ),
                    'itemCondition'      => 'https://schema.org/NewCondition',
                    'availability'       => 'https://schema.org/' . $stock = ( $product->is_in_stock() ? 'InStock' : 'OutOfStock' ),
                    'sku'                => $product->get_sku(),
                    'image'              => wp_get_attachment_url( $product->get_image_id() ),
                    'description'        => $product->get_description(),
                    'seller'             => array(
                        '@type' => 'Organization',
                        'name'  => $shop_name,
                        'url'   => $shop_url,
                    ),
                    'url'                => $link
                );
            }
        }

        return $markup;
    }

	/**
	 * Add custom time for feed schedule
	 *
	 * @param array $settings_data A list of the settings drawer data.
	 * @param array $feed_data A list of the all feed data.
	 *
	 * @return array The settings drawer data extracted from the feed data.
	 * @since 7.3.1
	 */
	public function set_settings_drawer_data( $settings_data, $feed_data ) {
		if( !empty( $feed_data[ 'rex_feed_custom_time' ] ) ) {
			$settings_data[ 'rex_feed_custom_time' ] = $feed_data[ 'rex_feed_custom_time' ];
		}
		return $settings_data;
	}
}