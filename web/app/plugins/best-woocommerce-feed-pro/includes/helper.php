<?php
if ( ! function_exists( 'wpfm_find_matching_product_variation' ) ) {
    /**
     * Find the matching variable ID
     *
     * @param $product
     * @param $attributes
     * @return mixed
     * @throws Exception
     */
    function wpfm_find_matching_product_variation($product, $attributes)
    {
        foreach ($attributes as $key => $value) {
            if (strpos($key, 'attribute_') === 0) {
                continue;
            }
            unset($attributes[$key]);
            $attributes[sprintf('attribute_%s', $key)] = $value;
        }
        if (class_exists('WC_Data_Store')) {
            $data_store = WC_Data_Store::load('product');
            return $data_store->find_matching_product_variation($product, $attributes);

        } else {
            return $product->get_matching_variation($attributes);
        }
    }
}