<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://rextheme.com
 * @since      1.0.0
 *
 * @package    Rex_Product_Feed_Pro
 * @subpackage Rex_Product_Feed_Pro/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Rex_Product_Feed_Pro
 * @subpackage Rex_Product_Feed_Pro/includes
 * @author     RexTheme <#>
 */
class Rex_Product_Feed_Pro_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
        wp_clear_scheduled_hook( 'rex_wpfm_license_check' );
	}
}
