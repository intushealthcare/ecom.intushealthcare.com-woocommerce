<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://rextheme.com
 * @since      1.0.0
 *
 * @package    Rex_Product_Feed_Pro
 * @subpackage Rex_Product_Feed_Pro/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Rex_Product_Feed_Pro
 * @subpackage Rex_Product_Feed_Pro/includes
 * @author     RexTheme <#>
 */
class Rex_Product_Feed_Pro {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Rex_Product_Feed_Pro_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'REX_PRODUCT_FEED_PRO_VERSION' ) ) {
			$this->version = REX_PRODUCT_FEED_PRO_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'rex-product-feed-pro';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Rex_Product_Feed_Pro_Loader. Orchestrates the hooks of the plugin.
	 * - Rex_Product_Feed_Pro_i18n. Defines internationalization functionality.
	 * - Rex_Product_Feed_Pro_Admin. Defines all hooks for the admin area.
	 * - Rex_Product_Feed_Pro_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

        /**
         * Get Composer Autoloader.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'vendor/autoload.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rex-product-feed-pro-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rex-product-feed-pro-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-rex-product-feed-pro-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-rex-product-feed-pro-public.php';

		$this->loader = new Rex_Product_Feed_Pro_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Rex_Product_Feed_Pro_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Rex_Product_Feed_Pro_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Rex_Product_Feed_Pro_Admin( $this->get_plugin_name(), $this->get_version() );
        $plugin_ajax = new Rex_Product_Feed_Pro_Ajax();


		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles', 99 );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts', 99 );
        $this->loader->add_action( 'admin_init', 'Rex_Product_Feed_Pro_Ajax', 'init' );

        /**
         * EDD Integration
         */
        $this->loader->add_action( 'admin_init', $plugin_admin, 'wpfm_pro_activate_license' );
        $this->loader->add_action( 'admin_init', $plugin_admin, 'wpfm_pro_deactivate_license' );
        $this->loader->add_action( 'admin_notices', $plugin_admin, 'wpfm_pro_edd_admin_notices' );

        $this->loader->add_filter( 'wpfm_get_total_number_of_products', $plugin_admin, 'wpfm_get_total_number_of_products', 10, 2 );
        $this->loader->add_filter( 'wpfm_merchant_array', $plugin_admin, 'wpfm_merchant_array' );
        $this->loader->add_filter( 'wpfm_merchant_custom', $plugin_admin, 'wpfm_merchant_custom' );
        $this->loader->add_filter( 'wpfm_autoload_file_array', $plugin_admin, 'wpfm_autoload_file_array' );


        $this->loader->add_action( 'woocommerce_product_data_tabs', $plugin_admin, 'rex_product_custom_tabs' );
        $this->loader->add_action( 'woocommerce_product_data_panels', $plugin_admin, 'rex_product_custom_fields', 999 );
        $this->loader->add_action( 'woocommerce_process_product_meta', $plugin_admin, 'rex_product_custom_fields_save' );
        $this->loader->add_action( 'woocommerce_product_after_variable_attributes', $plugin_admin, 'rex_product_variable_custom_fields', 10, 3 );
        $this->loader->add_action( 'woocommerce_save_product_variation', $plugin_admin, 'rex_product_variable_custom_fields_save', 10, 2 );

        $this->loader->add_filter( 'wpfm_is_premium', $plugin_admin, 'wpfm_is_premium');
        $this->loader->add_filter( 'wpfm_is_premium_activate', $plugin_admin, 'wpfm_is_premium_activate');

        $this->loader->add_filter( 'wpfm_merchant_fixed_format', $plugin_admin, 'wpfm_merchant_fixed_format');

        $this->loader->add_filter( 'wpfm_support_link', $plugin_admin, 'wpfm_support_link');
        $this->loader->add_filter( 'wpfm_product_filter_options', $plugin_admin, 'wpfm_update_options');

        $this->loader->add_action( 'wpfm_pro_feed_attribute_type_render', $plugin_admin, 'wpfm_pro_feed_attribute_type_render', 10, 2 );
        $this->loader->add_filter( 'wpfm_has_custom_feed_config', $plugin_admin, 'wpfm_has_custom_feed_config' );

        $this->loader->add_action( 'wpfm_product_filter_fields', $plugin_admin, 'wpfm_product_filter_field' );

        $this->loader->add_filter( 'cron_schedules', $plugin_admin, 'wpfm_weekly_cron_schedule');
        $this->loader->add_action( 'rex_wpfm_license_check', $plugin_admin, 'rex_wpfm_license_check_callback' );

        $this->loader->add_action( 'upgrader_process_complete', $plugin_admin, 'wpfm_pro_update_process_completed_callback', 10, 2 );
        $this->loader->add_action( 'rex_feed_after_upi_enable_field', $plugin_admin, 'render_upi_setting_fields_markups' );

        $this->loader->add_action( 'woocommerce_csv_product_import_mapping_options', $plugin_admin, 'update_attribute_list', 10, 2 );

        $this->loader->add_action( 'rex_feed_pro_features_overview', $plugin_admin, 'render_pro_features_overview' );

        $this->loader->add_action( 'rex_feed_after_static_input', $plugin_admin, 'render_combined_fields_attr_markup', 10, 3 );
        $this->loader->add_action( 'rex_wpfm_attributes', $plugin_admin, 'add_combined_fileds_separators', 10, 3 );

        $this->loader->add_action( 'rex_feed_is_valid_xml', $plugin_admin, 'notify_user', 10, 4 );

        $this->loader->add_action( 'rex_feed_after_log_enable_button_field', $plugin_admin, 'render_export_import_feed_fields' );
        $this->loader->add_action( 'wp_ajax_nopriv_rex_feed_save_import_xml_feed', $plugin_ajax, 'rex_feed_save_import_xml_feed' );
        $this->loader->add_action( 'wp_ajax_rex_feed_save_import_xml_feed', $plugin_ajax, 'rex_feed_save_import_xml_feed' );

        $this->loader->add_action( 'woocommerce_new_order', $plugin_admin, 'update_product_changed_status' );
        $this->loader->add_action( 'woocommerce_update_product', $plugin_admin, 'update_product_changed_status' );
        $this->loader->add_action( 'woocommerce_update_product_variation', $plugin_admin, 'update_product_changed_status' );
        $this->loader->add_action( 'woocommerce_delete_product_variation', $plugin_admin, 'update_product_changed_status' );
        $this->loader->add_action( 'woocommerce_new_product', $plugin_admin, 'update_product_changed_status' );
        $this->loader->add_action( 'wp_trash_post', $plugin_admin, 'update_product_changed_status' );
        $this->loader->add_action( 'untrash_post', $plugin_admin, 'update_product_changed_status' );
        $this->loader->add_action( 'woocommerce_delete_order_item', $plugin_admin, 'update_product_changed_status' );
        $this->loader->add_action( 'woocommerce_order_refunded', $plugin_admin, 'update_product_changed_status' );
        $this->loader->add_action( 'rex_feed_after_feed_cron_jobs_completed', $plugin_admin, 'update_product_changed_status' );

        $this->loader->add_action( 'rex_feed_after_autogenerate_options_field', $plugin_admin, 'render_product_update_cron_markups' );
        $this->loader->add_action( 'rex_feed_after_feed_config_saved', $plugin_admin, 'save_feed_settings_option', 10, 2 );
        $this->loader->add_action( 'rex_feed_before_taxonomy_fields', $plugin_admin, 'render_feed_rules_markups' );
		$this->loader->add_filter( 'rex_feed_license_submenu', $plugin_admin, 'add_license_submenu' );
		$this->loader->add_filter( 'woocommerce_structured_data_product', $plugin_admin, 'wpfm_structured_data', 99, 2 );
		$this->loader->add_filter( 'rexfeed_settings_drawer_data', $plugin_admin, 'set_settings_drawer_data', 10, 2 );
		$this->loader->add_filter( 'rexfeed_meta_attribute_types', $plugin_admin, 'set_ebay_attribute_types' );
		$this->loader->add_action( 'rexfeed_auto_generation_option_markups', $plugin_admin, 'render_custom_schedule_time_dropdown' );
		$this->loader->add_filter( 'wpfm_option_schedules', $plugin_admin, 'add_custom_interval_option' );
    }

	/**
	 * Register all the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Rex_Product_Feed_Pro_Public( $this->get_plugin_name(), $this->get_version() );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
        $this->loader->add_action( 'woocommerce_product_meta_end', $plugin_public, 'render_wpfm_custom_fields' );
	}

	/**
	 * Run the loader to execute all the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Rex_Product_Feed_Pro_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}
}
