<?php

/**
 * Fired during plugin activation
 *
 * @link       https://rextheme.com
 * @since      1.0.0
 *
 * @package    Rex_Product_Feed_Pro
 * @subpackage Rex_Product_Feed_Pro/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Rex_Product_Feed_Pro
 * @subpackage Rex_Product_Feed_Pro/includes
 * @author     RexTheme <#>
 */
class Rex_Product_Feed_Pro_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
        if (! wp_next_scheduled ( 'rex_wpfm_license_check' )) {
            wp_schedule_event(time(), 'monthly', 'rex_wpfm_license_check');
        }
    }

}
