<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://rextheme.com
 * @since      1.0.0
 *
 * @package    Rex_Product_Feed_Pro
 * @subpackage Rex_Product_Feed_Pro/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Rex_Product_Feed_Pro
 * @subpackage Rex_Product_Feed_Pro/public
 * @author     RexTheme <#>
 */
class Rex_Product_Feed_Pro_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @param string $plugin_name The name of the plugin.
     * @param string $version The version of this plugin.
     * @since    1.0.0
     */
    public function __construct( $plugin_name, $version )
    {

        $this->plugin_name = $plugin_name;
        $this->version     = $version;

    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Rex_Product_Feed_Pro_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Rex_Product_Feed_Pro_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Rex_Product_Feed_Pro_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Rex_Product_Feed_Pro_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
    }


    /**
     * Renders and shows custom fields by WooCommerce Product Feed Manager
     */
    public function render_wpfm_custom_fields()
    {
        if( apply_filters('wpfm_is_premium', false) ) {
            global $wpdb;
            $post_id             = get_the_ID();
            $like                = $wpdb->esc_like( '_wpfm_product_' ) . '%';
            $query               = $wpdb->prepare( "SELECT `meta_key`, `meta_value` FROM {$wpdb->prefix}postmeta WHERE `post_id`={$post_id} AND meta_key LIKE %s", $like );
            $fields              = $wpdb->get_results( $query );
            $need_capitalization = array( 'gtin', 'mpn', 'upc', 'ean', 'jan', 'isbn', 'itf' );
            $custom_fields       = get_option( 'wpfm_product_custom_fields_frontend', array() );

            foreach( $fields as $field ) {
                $name  = $field->meta_key;
                $value = $field->meta_value;

                if( $value !== '' ) {
                    $name = str_replace( '_wpfm_product_', '', $name );
                    if( in_array( $name, $custom_fields ) ) {
                        $name = str_replace( '_', '', $name );
                        $name = in_array( $name, $need_capitalization ) ? strtoupper( $name ) : ucfirst( $name );
                        ?>
                        <span class="wpfm-product-<?php echo strtolower( $name ); ?>"><?php echo $name . ': ' . $value ?></span>
                        <?php
                    }
                }
            }
        }
    }
}
