<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://rextheme.com
 * @since             1.0.0
 * @package           Rex_Product_Feed_Pro
 *
 * @wordpress-plugin
 * Plugin Name:       Product Feed Manager for WooCommerce PRO
 * Plugin URI:        https://rextheme.com
 * Description:       Product Feed Manager for WooCommerce PRO
 * Version:           6.4.7
 * Author:            RexTheme
 * Author URI:        https://rextheme.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       rex-product-feed-pro
 * Domain Path:       /languages
 *
 * WP Requirement & Test
 * Requires at least: 4.7
 * Tested up to: 6.3
 * Requires PHP: 7.3
 *
 * WC Requirement & Test
 * WC requires at least: 5.6.0
 * WC tested up to: 7.8.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */

define( 'REX_PRODUCT_FEED_PRO_VERSION', '6.4.7' );
define( 'WPFM_SL_STORE_URL', 'https://rextheme.com/' );
define( 'WPFM_SL_ITEM_ID', 7532 );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-rex-product-feed-pro-activator.php
 */
function activate_rex_product_feed_pro() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-rex-product-feed-pro-activator.php';
	Rex_Product_Feed_Pro_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-rex-product-feed-pro-deactivator.php
 */
function deactivate_rex_product_feed_pro() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-rex-product-feed-pro-deactivator.php';
	Rex_Product_Feed_Pro_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_rex_product_feed_pro' );
register_deactivation_hook( __FILE__, 'deactivate_rex_product_feed_pro' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-rex-product-feed-pro.php';
require plugin_dir_path( __FILE__ ) . 'includes/helper.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_rex_product_feed_pro() {

	$plugin = new Rex_Product_Feed_Pro();
	$plugin->run();

	new Rex_Product_Feed_Extend_Check( 'best-woocommerce-feed/rex-product-feed.php', __FILE__, '7.3.0', 'rex-product-feed-pro' );
}
run_rex_product_feed_pro();

/**
 * Add Google Adwords Remarketing code to footer
 */
function wpfm_add_remarketing_tags( $product = null ){
	if ( ! is_object( $product ) ) {
		global $product;
	}
	$ecomm_pagetype = wpfm_google_remarketing_pagetype();
	$drm_conversion_id = (int)get_option("rex-wpfm-product-custom-field-drm");
	if ($drm_conversion_id > 0) {
		if ($ecomm_pagetype == "product"){
			if ( '' !== $product->get_price()) {
				$ecomm_prodid = get_the_id();

				if(!empty($ecomm_prodid)){
					if ( $product->is_type( 'variable' ) ) {
						$variation_id = wpfm_find_matching_product_variation( $product, $_GET );
						$nr_get = count($_GET);

						if($nr_get > 0){
							$variable_product = wc_get_product($variation_id);

							if(is_object( $variable_product ) ) {
								$product_price = $variable_product->get_price_html();
								$ecomm_price = $product_price;
							} else {
								$prices  = $product->get_variation_prices();
								$lowest  = reset( $prices['price'] );
								$highest = end( $prices['price'] );

								if ( $lowest === $highest ) {
									$ecomm_price = wc_format_decimal( $lowest, wc_get_price_decimals() );
								} else {
									$ecomm_lowprice  = wc_format_decimal( $lowest, wc_get_price_decimals() );
									$ecomm_highprice = wc_format_decimal( $highest, wc_get_price_decimals() );
								}
							}
						} else {
							$prices  = $product->get_variation_prices();
							$lowest  = reset( $prices['price'] );
							$highest = end( $prices['price'] );

							if ( $lowest === $highest ) {
								$ecomm_price = wc_format_decimal( $lowest, wc_get_price_decimals());
							} else {
								$ecomm_lowprice = wc_format_decimal( $lowest, wc_get_price_decimals() );
								$ecomm_highprice = wc_format_decimal( $highest, wc_get_price_decimals() );
							}
						}
					} else
					{
						$ecomm_price = wc_format_decimal( $product->get_price(), wc_get_price_decimals() );
					}
				}

				?>
				<script type="text/javascript">
					var google_tag_params = {
						ecomm_prodid: '<?php print "$ecomm_prodid";?>',
						ecomm_pagetype: '<?php print "$ecomm_pagetype";?>',
						ecomm_totalvalue: <?php print "$ecomm_price";?>,
					};
				</script>
				<?php
			}
		} elseif ($ecomm_pagetype == "cart"){
			$ecomm_prodid = get_the_id();

			?>
			<script type="text/javascript">
				var google_tag_params = {
					ecomm_prodid: '<?php print "$ecomm_prodid";?>',
					ecomm_pagetype: '<?php print "$ecomm_pagetype";?>',
				};
			</script>
			<?php
		} else {
			?>
			<script type="text/javascript">
				var google_tag_params = {
					ecomm_pagetype: '<?php print "$ecomm_pagetype";?>',
				};
			</script>
			<?php
		}
		?>

		<script type="text/javascript">
			var google_conversion_id = <?php print "$drm_conversion_id";?>;
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
			<div style="display:inline;">
				<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/<?php print "$drm_conversion_id";?>/?guid=ON&amp;script=0"/>
			</div>
		</noscript>
		<?php
	}
}
add_action('wp_footer', 'wpfm_add_remarketing_tags');

function wpfm_google_remarketing_pagetype ( ) {
	$ecomm_pagetype = "other";

	if (in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		if(is_product()){
			$ecomm_pagetype = "product";
		} elseif (is_cart()){
			$ecomm_pagetype = "cart";
		} elseif (is_checkout()){
			$ecomm_pagetype = "cart";
		} elseif (is_product_category()){
			$ecomm_pagetype = "category";
		} elseif (is_front_page()){
			$ecomm_pagetype = "home";
		} elseif (is_search()){
			$ecomm_pagetype = "searchresults";
		} else {
			$ecomm_pagetype = "other";
		}
	}
	return $ecomm_pagetype;
}

/**
 * Retrieve variation product id based on it attributes
 **/
function wpfm_find_matching_product_variation( $product, $attributes ) {

	foreach( $attributes as $key => $value ) {
		if( strpos( $key, 'attribute_' ) === 0 ) {
			continue;
		}
		unset( $attributes[ $key ] );
		$attributes[ sprintf( 'attribute_%s', $key ) ] = $value;
	}

	if( class_exists('WC_Data_Store') ) {
		$data_store = WC_Data_Store::load( 'product' );
		return $data_store->find_matching_product_variation( $product, $attributes );
	} else {
		return $product->get_matching_variation( $attributes );
	}
}



if( !class_exists( 'Rex_Product_Feed_Pro_EDD_Updater' ) ) {
	include( dirname( __FILE__ ) . '/admin/class-rex-product-feed-pro-edd-updater.php' );
}

$license_key = trim( get_option( 'wpfm_pro_license_key' ) );
if($license_key) {
	$edd_updater = new Rex_Product_Feed_Pro_EDD_Updater( WPFM_SL_STORE_URL, __FILE__, array(
		'version' 	    => REX_PRODUCT_FEED_PRO_VERSION,
		'license' 	    => $license_key,
		'item_id'       => WPFM_SL_ITEM_ID,
		'author' 	    => 'RexTheme',
		'url'           => home_url(),
		'beta'          => false
	) );
}

function wpfm_pro_plugin_update_message( $data, $response ) {
	if( isset( $data['upgrade_notice'] ) ) {
		printf(
			'<div class="update-message">%s</div>',
			wpautop( $data['upgrade_notice'] )
		);
	}
}
add_action( 'in_plugin_update_message-best-woocommerce-feed-pro/rex-product-feed-pro.php', 'wpfm_pro_plugin_update_message', 10, 2 );

/**
 * @desc Redirects to PFM licence page after plugin activation
 * @param $plugin
 * @return void
 */
function rex_feed_pro_redirect_after_activation( $plugin ) {
	if ( $plugin === plugin_basename( __FILE__ ) ) {
		$url = admin_url( 'admin.php?page=wpfm-license' );
		$url = esc_url( $url, FILTER_SANITIZE_URL );
		exit( wp_redirect( $url ) );
	}
}
add_action( 'activated_plugin', 'rex_feed_pro_redirect_after_activation' );

/**
 * Declare plugin's compatibility with WooCommerce HPOS
 *
 * @return void
 * @since 6.3.9
 */
function rex_feed_pro_wc_hpos_compatibility() {
	if ( class_exists( \Automattic\WooCommerce\Utilities\FeaturesUtil::class ) ) {
		\Automattic\WooCommerce\Utilities\FeaturesUtil::declare_compatibility( 'custom_order_tables', __FILE__, true );
	}
}
add_action( 'before_woocommerce_init', 'rex_feed_pro_wc_hpos_compatibility' );