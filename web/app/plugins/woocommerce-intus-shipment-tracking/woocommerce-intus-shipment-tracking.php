<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://sweet-apple.co.uk
 * @since             0.0.1
 * @package           WooCommerce_Intus_Shipment_Tracking
 *
 * @wordpress-plugin
 * Plugin Name:       WooCommerce Intus Shipment Tracking
 * Plugin URI:        https://www.sweet-apple.co.uk/custom-wordpress-plugin-development/
 * Description:       Modify Shipments created by the WooCommerce Shipment Tracking plugin API to automatically detect and set Courier and Tracking Links
 * Version:           0.0.2
 * Author:            Clive Sweeting, Sweet-Apple
 * Author URI:        https://sweet-apple.co.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woocommerce_intus_shipment_tracking
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * Current plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 */
define( 'WOOCOMMERCE_INTUS_SHIPMENT_TRACKING', '0.0.2' );

add_action( 'added_post_meta', 'woo_intus_st_after_post_meta', 10, 4 );
add_action( 'updated_post_meta', 'woo_intus_st_after_post_meta', 10, 4 );
function woo_intus_st_after_post_meta( $meta_id, $post_id, $meta_key, $meta_value )
{
    if ( '_wc_shipment_tracking_items' == $meta_key ) {
        $meta_items = [];
        foreach ( $meta_value  as $meta_item )
        {
            $tracking_links_populated = false;
            if( strlen( $meta_item['tracking_provider'] ) > 0 ) {
                $tracking_links_populated = true;
            }
            if( strlen( $meta_item['custom_tracking_provider'] ) > 0 ) {
                $tracking_links_populated = true;
            }
            if( strlen( $meta_item['custom_tracking_link'] ) > 0 ) {
                $tracking_links_populated = true;
            }
            if( !$tracking_links_populated ){
                $meta_item = woo_intus_st_update_tracking_details( $meta_item );
            }
            $meta_items[] = $meta_item;
        }
        //Any differences?
        if( $meta_value !== $meta_items ){
            update_metadata( 'post', $post_id, $meta_key, $meta_items, $meta_value );
        }
    }
}

function woo_intus_st_update_tracking_details( $meta_item ) {
    // We should probably move to a switch if this gets more complicated...
//    switch ( strlen( $meta_item['tracking_number'] ) == 10 ) {
//        case 12 :
//            $shipping_provider = 'fedex';
//            break;
//        case 10 :
//            $shipping_provider = 'dpd';
//            break;
//        default:
//            $shipping_provider = 'royalmail';
//            break;
//    }
    $shipping_provider = 'royalmail';
    if( strlen( $meta_item['tracking_number'] ) == 10 ){
        $shipping_provider = 'dpd';
    }
    if( strlen( $meta_item['tracking_number'] ) == 12){
        $shipping_provider = 'fedex';
    }
    $shipping_details = woo_intus_st_get_shipping_details( $shipping_provider );
    $meta_item['custom_tracking_provider'] = $shipping_details['name'];
    $meta_item['custom_tracking_link'] = sprintf( $shipping_details['link'] , $meta_item['tracking_number'] );
    return $meta_item;
}


/**
 * @param $shipping_provider
 *
 * @return string[]
 */
function woo_intus_st_get_shipping_details( $shipping_provider ) {
    $shipping_array = [
        'royalmail' => [
            'name' => 'Royal Mail',
            'link' => 'https://www3.royalmail.com/track-your-item#/tracking-results/%1$s',
        ],
        'dpd' => [
            'name' => 'DPD.co.uk',
            'link' => 'https://www.dpd.co.uk/apps/tracking/?reference=%1$s#results',
        ],
        'fedex' => [
            'name' => 'FedEx',
            'link' => 'https://www.fedex.com/fedextrack/?trknbr=%1$s',
        ],
    ];
    return $shipping_array[ $shipping_provider ];
}
