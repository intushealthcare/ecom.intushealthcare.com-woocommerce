<?php

/**
 * Fired during plugin activation
 *
 * @link       https://sweet-apple.co.uk
 * @since      1.0.0
 *
 * @package    Intus_questionnaire
 * @subpackage Intus_questionnaire/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Intus_questionnaire
 * @subpackage Intus_questionnaire/includes
 * @author     Clive Sweeting, Sweet-Apple <info@sweet-apple.co.uk>
 */
class Intus_questionnaire_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
