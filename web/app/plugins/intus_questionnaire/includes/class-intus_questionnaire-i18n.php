<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://sweet-apple.co.uk
 * @since      1.0.0
 *
 * @package    Intus_questionnaire
 * @subpackage Intus_questionnaire/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Intus_questionnaire
 * @subpackage Intus_questionnaire/includes
 * @author     Clive Sweeting, Sweet-Apple <info@sweet-apple.co.uk>
 */
class Intus_questionnaire_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'intus_questionnaire',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
