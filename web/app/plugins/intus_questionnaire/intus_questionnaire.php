<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://sweet-apple.co.uk
 * @since             1.0.0
 * @package           Intus_questionnaire
 *
 * @wordpress-plugin
 * Plugin Name:       Intus Questionnaire
 * Plugin URI:        https://www.sweet-apple.co.uk/custom-wordpress-plugin-development/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Clive Sweeting, Sweet-Apple
 * Author URI:        https://sweet-apple.co.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       intus_questionnaire
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'INTUS_QUESTIONNAIRE_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-intus_questionnaire-activator.php
 */
function activate_intus_questionnaire() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-intus_questionnaire-activator.php';
	Intus_questionnaire_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-intus_questionnaire-deactivator.php
 */
function deactivate_intus_questionnaire() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-intus_questionnaire-deactivator.php';
	Intus_questionnaire_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_intus_questionnaire' );
register_deactivation_hook( __FILE__, 'deactivate_intus_questionnaire' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-intus_questionnaire.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_intus_questionnaire() {

	$plugin = new Intus_questionnaire();
	$plugin->run();

}
run_intus_questionnaire();
