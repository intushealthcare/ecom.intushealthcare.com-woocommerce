<?php
/**
 * Performs the basic functions for the plugin.
 *
 * @package Custom-order-numbers-for-WooCommerce
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
// Check if WooCommerce is active.
$plugincheck = 'woocommerce/woocommerce.php';
if (
	! in_array( $plugincheck, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ), true ) &&
	! ( is_multisite() && array_key_exists( $plugincheck, get_site_option( 'active_sitewide_plugins', array() ) ) )
) {
	return;
}

if ( 'custom-order-numbers-for-woocommerce.php' === basename( __FILE__ ) ) {
	// Check if Pro is active, if so then return.
	$plugin_name = 'custom-order-numbers-for-woocommerce-pro/custom-order-numbers-for-woocommerce-pro.php';
	if (
		in_array( $plugin_name, apply_filters( 'active_plugins', get_option( 'active_plugins', array() ) ), true ) ||
		( is_multisite() && array_key_exists( $plugin_name, get_site_option( 'active_sitewide_plugins', array() ) ) )
	) {
		return;
	}
}

if ( ! class_exists( 'Alg_WC_Custom_Order_Numbers' ) ) :

	/**
	 * Main Alg_WC_Custom_Order_Numbers Class
	 *
	 * @class   Alg_WC_Custom_Order_Numbers
	 * @version 1.2.3
	 * @since   1.0.0
	 */
	final class Alg_WC_Custom_Order_Numbers {

		/**
		 * Plugin version.
		 *
		 * @var   string
		 * @since 1.0.0
		 */
		public $version = '1.5.0';

		/**
		 * Instance
		 *
		 * @var   Alg_WC_Custom_Order_Numbers The single instance of the class.
		 * @since 1.0.0
		 */
		protected static $instance = null;

		/**
		 * Main Alg_WC_Custom_Order_Numbers Instance
		 *
		 * Ensures only one instance of Alg_WC_Custom_Order_Numbers is loaded or can be loaded.
		 *
		 * @version 1.0.0
		 * @since   1.0.0
		 * @static
		 * @return  Alg_WC_Custom_Order_Numbers - Main instance
		 */
		public static function instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}
			return self::$instance;
		}

		/**
		 * Alg_WC_Custom_Order_Numbers Constructor.
		 *
		 * @version 1.0.0
		 * @since   1.0.0
		 * @access  public
		 */
		public function __construct() {

			// Set up localisation.
			load_plugin_textdomain( 'custom-order-numbers-for-woocommerce', false, dirname( plugin_basename( __FILE__ ) ) . '/langs/' );

			add_action( 'alg_get_plugins_list', array( $this, 'con_remove_plugin_name' ), PHP_INT_MAX );

			// The filter.
			add_filter( 'alg_wc_custom_order_numbers', array( $this, 'alg_wc_custom_order_numbers' ), 9999, 3 );

			// Include required files.
			$this->includes();

			// Settings & Scripts.
			if ( is_admin() ) {
				add_filter( 'woocommerce_get_settings_pages', array( $this, 'add_woocommerce_settings_tab' ) );
				add_filter( 'plugin_action_links_' . plugin_basename( 'custom-order-numbers-for-woocommerce-pro/custom-order-numbers-for-woocommerce-pro.php' ), array( $this, 'action_links' ) );

				$this->define_constants();
				add_action( 'admin_init', array( $this, 'con_edd_handle_license' ) );
				$this->check_for_updates();
			}
		}

		/**
		 * Function alg_wc_custom_order_numbers.
		 *
		 * @param array  $value Value.
		 * @param string $type Type selected in general settings.
		 * @param array  $args Arguments of the value of Custom Order Numbers.
		 * @version 1.2.3
		 * @since   1.0.0
		 */
		public function alg_wc_custom_order_numbers( $value, $type, $args = '' ) {
			switch ( $type ) {
				case 'settings':
					return '';
				case 'value':
						$template                            = get_option( 'alg_wc_custom_order_numbers_template', '{prefix}{date_prefix}{number}{suffix}{date_suffix}' );
						$custom_order_timestamp              = $args['order_timestamp'];
						$counter_type                        = get_option( 'alg_wc_custom_order_numbers_counter_type', 'sequential' );
						$custom_order_number_no_restrictions = $args['order_number_meta'];
						$order_number_width                  = get_option( 'alg_wc_custom_order_numbers_min_width', 0 );
					if ( $order_number_width > 0 && 'hash_crc32' === $counter_type ) {
						$custom_order_number_by_width = substr( $custom_order_number_no_restrictions, 0, $order_number_width );
					} else {
						$custom_order_number_by_width = $custom_order_number_no_restrictions;
					}
					$con_prefix = sprintf( '%s', do_shortcode( get_option( 'alg_wc_custom_order_numbers_prefix', '' ) ) );
					$con_suffix = sprintf( '%s', do_shortcode( get_option( 'alg_wc_custom_order_numbers_suffix', '' ) ) );
					if ( ! is_admin() ) {
						$user_role_data = get_option( 'con_user_roles_data_array', '' );
						$current_user   = wp_get_current_user();
						if ( isset( $current_user->roles[0] ) && '' !== $current_user->roles[0] ) {
							$order_user = $current_user->roles[0];
						} else {
							$order_user = 'guest';
						}
						$user_role_present = false;
						if ( is_array( $user_role_data ) ) {
							foreach ( $user_role_data as $key => $user_role ) {
								if ( 'roles' === $key ) {
									foreach ( $user_role as $key => $role ) {
										if ( in_array( $order_user, $role, true ) ) {
											$user_role_present = true;
											$user_key_num      = str_replace( 'con_user_role_', '', $key );
										}
									}
								}
								if ( $user_role_present ) {
									if ( 'prefix' === $key ) {
										$user_role_prefix = $user_role[ 'user_role_prefix_' . $user_key_num ];
									}
									if ( 'suffix' === $key ) {
										$user_role_suffix = $user_role[ 'user_role_suffix_' . $user_key_num ];
									}
								}
							}
							if ( $user_role_present ) {
								$con_prefix = $user_role_prefix;
								$con_suffix = $user_role_suffix;
							}
						}
					}
					$data = array(
						'{prefix}'      => $con_prefix,
						'{date_prefix}' => sprintf( '%s', date_i18n( get_option( 'alg_wc_custom_order_numbers_date_prefix', '' ), $custom_order_timestamp ) ),
						'{number}'      => sprintf( '%0' . $order_number_width . 's', $custom_order_number_by_width ),
						'{suffix}'      => $con_suffix,
						'{date_suffix}' => sprintf( '%s', date_i18n( get_option( 'alg_wc_custom_order_numbers_date_suffix', '' ), $custom_order_timestamp ) ),
					);
					return str_replace( array_keys( $data ), $data, $template );
				case 'manual_counter_value':
					return get_option( 'alg_wc_custom_order_numbers_manual_enabled', 'no' );
			}
			return $value;
		}

		/**
		 * Show action links on the plugin screen
		 *
		 * @param   mixed $links Action Links.
		 * @version 1.2.0
		 * @since   1.0.0
		 * @return  array
		 */
		public function action_links( $links ) {
			$custom_links   = array();
			$custom_links[] = '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=alg_wc_custom_order_numbers' ) . '">' . __( 'Settings', 'woocommerce' ) . '</a>';
			if ( 'custom-order-numbers-for-woocommerce.php' === basename( __FILE__ ) ) {
				$custom_links[] = '<a href="https://www.tychesoftwares.com/store/premium-plugins/custom-order-numbers-woocommerce/">' . __( 'Unlock All', 'custom-order-numbers-for-woocommerce' ) . '</a>';
			}
			return array_merge( $custom_links, $links );
		}

		/**
		 * Include required core files used in admin and on the frontend.
		 *
		 * @version 1.2.0
		 * @since   1.0.0
		 */
		public function includes() {
			// Settings.
			require_once 'includes/admin/class-alg-wc-custom-order-numbers-settings-section.php';
			$this->settings            = array();
			$this->settings['general'] = require_once 'includes/admin/class-alg-wc-custom-order-numbers-settings-general.php';
			if ( is_admin() && get_option( 'alg_custom_order_numbers_version', '' ) !== $this->version ) {
				foreach ( $this->settings as $section ) {
					foreach ( $section->get_settings() as $value ) {
						if ( isset( $value['default'] ) && isset( $value['id'] ) ) {
							$autoload = isset( $value['autoload'] ) ? (bool) $value['autoload'] : true;
							add_option( $value['id'], $value['default'], '', ( $autoload ? 'yes' : 'no' ) );
						}
					}
				}
				if ( '' !== get_option( 'alg_custom_order_numbers_version', '' ) ) {
					update_option( 'alg_custom_order_numbers_show_admin_notice', 'yes' );
				}
				update_option( 'alg_custom_order_numbers_version', $this->version );
			}
			$this->settings['license'] = require_once 'includes/admin/class-alg-wc-con-settings-license.php';
			// Core.
			require_once 'includes/class-alg-wc-custom-order-numbers-core.php';
			if ( is_admin() ) {
				require_once 'includes/class-con-data-tracking.php';
				require_once 'includes/class-con-tracking-functions.php';
			}
		}

		/**
		 * Add Custom Order Numbers settings tab to WooCommerce settings.
		 *
		 * @param array $settings WooCommerce settings array.
		 * @version 1.2.2
		 * @since   1.0.0
		 */
		public function add_woocommerce_settings_tab( $settings ) {
			$hide_for_roles = get_option( 'alg_wc_custom_order_numbers_hide_tab_for_roles', array() );
			if ( ! empty( $hide_for_roles ) ) {
				$user       = wp_get_current_user();
				$user_roles = (array) $user->roles;
				$intersect  = array_intersect( $hide_for_roles, $user_roles );
				if ( ! empty( $intersect ) ) {
					return $settings;
				}
			}
			$settings[] = include 'includes/admin/class-alg-wc-settings-custom-order-numbers.php';
			return $settings;
		}

		/**
		 * Get the plugin url.
		 *
		 * @version 1.0.0
		 * @since   1.0.0
		 * @return  string
		 */
		public function plugin_url() {
			return untrailingslashit( plugin_dir_url( __FILE__ ) );
		}

		/**
		 * Get the plugin path.
		 *
		 * @version 1.0.0
		 * @since   1.0.0
		 * @return  string
		 */
		public function plugin_path() {
			return untrailingslashit( plugin_dir_path( __FILE__ ) );
		}

		/**
		 * Defined Constants.
		 */
		private function define_constants() {
			define( 'EDD_CON_STORE_URL', 'http://www.tychesoftwares.com/' );
			define( 'EDD_CON_ITEM_NAME', 'Custom Order Numbers for WooCommerce' );
		}

		/**
		 * Function to handle license activation.
		 */
		public function con_edd_handle_license() {
			if ( isset( $_REQUEST['_con_nonce'] ) && wp_verify_nonce( sanitize_key( $_REQUEST['_con_nonce'] ), 'con-activate-nonce' ) ) {
				if ( isset( $_GET['license'] ) && 'con_activate' === $_GET['license'] ) {
					self::con_activate_license();
				} elseif ( isset( $_GET['license'] ) && 'con_deactivate' === $_GET['license'] ) {
					self::con_deactivate_license();
				}
			}
		}
		/**
		 * Activate license
		 */
		public function con_activate_license() {
			// run a quick security check
			// if ( ! check_admin_referer( 'edd_sample_nonce', 'edd_sample_nonce' ) )
			// return; // get out if we didn't click the Activate button.
			// retrieve the license from the database.
			$license = trim( get_option( 'edd_license_key_con' ) );
			// data to send in our API request.
			$api_params = array(
				'edd_action' => 'activate_license',
				'license'    => $license,
				'item_name'  => rawurlencode( EDD_CON_ITEM_NAME ), // the name of our product in EDD.
			);
			// Call the custom API.
			$response = wp_remote_get(
				add_query_arg(
					$api_params,
					EDD_CON_STORE_URL
				),
				array(
					'timeout'   => 15,
					'sslverify' => false,
				)
			);
			// make sure the response came back okay.
			if ( is_wp_error( $response ) ) {
				return false;
			}
			// decode the license data.
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );

			// $license_data->license will be either "active" or "inactive"
			update_option( 'edd_license_key_con_status', $license_data->license );
		}

		/**
		 * License Deactivation function.
		 */
		public function con_deactivate_license() {
			// run a quick security check
			// if ( ! check_admin_referer( 'edd_sample_nonce', 'edd_sample_nonce' ) )
			// return; // get out if we didn't click the Activate button.
			// retrieve the license from the database.
			$license = trim( get_option( 'edd_license_key_con' ) );
			// data to send in our API request.
			$api_params = array(
				'edd_action' => 'deactivate_license',
				'license'    => $license,
				'item_name'  => rawurlencode( EDD_CON_ITEM_NAME ), // the name of our product in EDD.
			);
			// Call the custom API.
			$response = wp_remote_get(
				add_query_arg(
					$api_params,
					EDD_CON_STORE_URL
				),
				array(
					'timeout'   => 15,
					'sslverify' => false,
				)
			);
			// make sure the response came back okay.
			if ( is_wp_error( $response ) ) {
				return false;
			}
			// decode the license data.
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );
			// $license_data->license will be either "deactivated" or "failed"
			if ( 'deactivated' === $license_data->license ) {
				delete_option( 'edd_license_key_con_status' );
			}
		}

		/**
		 * Checks for updates.
		 */
		private function check_for_updates() {

			if ( ! class_exists( 'EDD_CON_Plugin_Updater' ) ) {
				// load our custom updater if it doesn't already exist.
				include dirname( __FILE__ ) . '/plugin_updates/EDD_CON_Plugin_Updater.php';
			}
			/**
			 * Retrieve our license key from the DB
			 */
			$license_key = trim( get_option( 'edd_license_key_con' ) );
			/**
			 * Setup the updater
			 */
			$edd_updater = new EDD_CON_Plugin_Updater(
				EDD_CON_STORE_URL,
				'custom-order-numbers-for-woocommerce-pro/custom-order-numbers-for-woocommerce-pro.php',
				array(
					'version'   => $this->version,    // current version number.
					'license'   => $license_key,      // license key (used get_option above to retrieve from DB).
					'item_name' => EDD_CON_ITEM_NAME, // name of this plugin.
					'author'    => 'Ashok Rane',       // author of this plugin.
				)
			);
		}
		/**
		 * Function to remove the Plugin name.
		 */
		public function con_remove_plugin_name() {

			$plugin_list = get_option( 'alg_wpcodefactory_helper_plugins' );

			if ( '' !== $plugin_list ) {
				$plugin_list = array_diff( $plugin_list, array( 'custom-order-numbers-for-woocommerce-pro' ) );
				update_option( 'alg_wpcodefactory_helper_plugins', $plugin_list );
			}
		}

	}

	endif;

if ( ! function_exists( 'alg_wc_custom_order_numbers' ) ) {
	/**
	 * Returns the main instance of Alg_WC_Custom_Order_Numbers to prevent the need to use globals.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 * @return  Alg_WC_Custom_Order_Numbers
	 */
	function alg_wc_custom_order_numbers() {
		return Alg_WC_Custom_Order_Numbers::instance();
	}
}

alg_wc_custom_order_numbers();
