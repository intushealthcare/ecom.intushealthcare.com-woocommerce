<?php
/**
 * Plugin Name: Custom Order Numbers for WooCommerce Pro
 * Plugin URI: https://www.tychesoftwares.com/store/premium-plugins/custom-order-numbers-woocommerce/
 * Description: Custom order numbers for WooCommerce.
 * Version: 1.5.0
 * Author: Tyche Softwares
 * Author URI: https://www.tychesoftwares.com/
 * Text Domain: custom-order-numbers-for-woocommerce
 * Domain Path: /langs
 * Copyright: � 2021 Tyche Softwares
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * WC tested up to: 5.8
 *
 * @package Custom-order-numbers-for-WooCommerce
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'Alg_WC_Custom_Order_Numbers' ) ) {
	include_once 'class-alg-wc-custom-order-numbers.php';
}

// Remove a recurring As action on plugin deactivtion.
register_deactivation_hook( __FILE__, 'ts_deactivate_recurring_action' );

/**
 * Remove a weekly scheduled action when plugin is deactivated.
 */
function ts_deactivate_recurring_action() {
	as_unschedule_all_actions( 'ts_send_data_tracking_usage' );
	as_unschedule_all_actions( 'alg_custom_order_numbers_weekly_reset_event' );
}
