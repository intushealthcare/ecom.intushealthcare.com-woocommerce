# Copyright (C) 2020 Tyche Softwares
# This file is distributed under the same license as the Custom Order Numbers for WooCommerce Pro plugin.
msgid ""
msgstr ""
"Project-Id-Version: Custom Order Numbers for WooCommerce Pro 1.3.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/custom-order-numbers-for-woocommerce-pro\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2020-06-01T07:19:13+02:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.4.0\n"
"X-Domain: custom-order-numbers-for-woocommerce\n"

#. Plugin Name of the plugin
msgid "Custom Order Numbers for WooCommerce Pro"
msgstr ""

#. Plugin URI of the plugin
msgid "https://www.tychesoftwares.com/store/premium-plugins/custom-order-numbers-woocommerce/"
msgstr ""

#. Description of the plugin
msgid "Custom order numbers for WooCommerce."
msgstr ""

#. Author of the plugin
msgid "Tyche Softwares"
msgstr ""

#. Author URI of the plugin
msgid "https://www.tychesoftwares.com/"
msgstr ""

#: custom-order-numbers-for-woocommerce-pro.php:154
msgid "Unlock All"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:24
msgid "General"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:65
msgid "Custom Order Numbers Options"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:67
msgid "Enable sequential order numbering, set custom number prefix, suffix and number width."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:71
msgid "WooCommerce Custom Order Numbers"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:72
msgid "Enable plugin"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:73
msgid "Custom Order Numbers for WooCommerce."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:79
msgid "Order numbers counter"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:84
msgid "Sequential"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:85
msgid "Order ID"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:86
msgid "Pseudorandom - crc32 Hash (max 10 digits)"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:90
msgid "Sequential: Next order number"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:91
msgid "Next new order will be given this number."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:92
msgid "Use \"Renumerate Orders tool\" for existing orders."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:93
#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:100
msgid "This will be ignored if sequential order numbering is disabled."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:99
msgid "Sequential: Reset counter"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:105
msgid "Disabled"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:106
msgid "Daily"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:107
msgid "Monthly"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:108
msgid "Yearly"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:112
msgid "Reset counter value."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:113
msgid "Counter value to reset to."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:114
msgid "This will be ignored if \"Sequential: Reset counter\" option is set to \"Disabled\"."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:120
msgid "Order number custom prefix"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:121
msgid "Prefix before order number (optional). This will change the prefixes for all existing orders."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:127
msgid "Order number date prefix"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:130
msgid "Date prefix before order number (optional). This will change the prefixes for all existing orders. Value is passed directly to PHP `date` function, so most of PHP date formats can be used. The only exception is using `\\` symbol in date format, as this symbol will be excluded from date. Try: Y-m-d- or mdy."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:137
msgid "Order number width"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:140
msgid "Minimum width of number without prefix (zeros will be added to the left side). This will change the minimum width of order number for all existing orders. E.g. set to 5 to have order number displayed as 00001 instead of 1. Leave zero to disable."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:147
msgid "Order number custom suffix"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:150
msgid "Suffix after order number (optional). This will change the suffixes for all existing orders."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:157
msgid "Order number date suffix"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:160
msgid "Date suffix after order number (optional). This will change the suffixes for all existing orders. Value is passed directly to PHP `date` function, so most of PHP date formats can be used. The only exception is using `\\` symbol in date format, as this symbol will be excluded from date. Try: Y-m-d- or mdy."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:167
msgid "Order number template"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:168
msgid "Replaced values: %s."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:178
msgid "Enable order tracking by custom number"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:179
#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:186
#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:193
msgid "Enable"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:185
msgid "Enable order admin search by custom number"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:192
msgid "Manual order number counter"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:194
msgid "This will add \"Order Number\" meta box to each order's edit page. \"Order Numbers Counter\" must be set to \"Sequential\"."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:203
msgid "Hide \"Renumerate Orders\" admin menu for roles"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:204
msgid "Hide \"Renumerate Orders\" admin menu for selected user roles."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:205
msgid "All user roles are listed here - even those which do not see the menu by default."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:213
msgid "Hide \"Custom Order Numbers\" admin settings tab for roles"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:214
msgid "Hide \"Custom Order Numbers\" admin settings tab for selected user roles."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:215
msgid "Tab can not be hidden for administrators."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:216
msgid "All user roles are listed here - even those which do not see the tab by default."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:228
msgid "Tools"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:229
msgid "Tool for existing orders."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-general.php:230
msgid "Renumerate Orders tool"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-license.php:24
msgid "License"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-license.php:65
msgid "Plugin License Options"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-license.php:70
msgid "License Key\t"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-license.php:71
msgid "Enter your license key."
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-license.php:77
msgid "Activate License"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-section.php:55
msgid "Reset Settings"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-section.php:60
msgid "Reset section settings"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-section.php:61
msgid "Reset"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-section.php:67
msgid "Reset Usage Tracking"
msgstr ""

#: includes/admin/class-alg-wc-custom-order-numbers-settings-section.php:68
msgid "This will reset your usage tracking settings, causing it to show the opt-in banner again and not sending any data."
msgstr ""

#: includes/admin/class-alg-wc-settings-custom-order-numbers.php:30
msgid "Custom Order Numbers"
msgstr ""

#: includes/class-alg-wc-custom-order-numbers-core.php:119
msgid "Order Number"
msgstr ""

#: includes/class-alg-wc-custom-order-numbers-core.php:159
#: includes/class-alg-wc-custom-order-numbers-core.php:160
#: includes/class-alg-wc-custom-order-numbers-core.php:185
msgid "Renumerate Orders"
msgstr ""

#: includes/class-alg-wc-custom-order-numbers-core.php:183
msgid "%d orders successfully renumerated!"
msgstr ""

#: includes/class-alg-wc-custom-order-numbers-core.php:187
msgid "Plugin settings: <a href=\"%s\">WooCommerce > Settings > Custom Order Numbers</a>."
msgstr ""

#: includes/class-alg-wc-custom-order-numbers-core.php:190
msgid "Press the button below to renumerate all existing orders."
msgstr ""

#: includes/class-alg-wc-custom-order-numbers-core.php:192
msgid "First order number will be <strong>%d</strong>."
msgstr ""

#: includes/class-alg-wc-custom-order-numbers-core.php:195
msgid "Renumerate orders"
msgstr ""

#: includes/class-alg-wc-custom-order-numbers-core.php:196
msgid "Are you sure?"
msgstr ""

#. translators: Plugin Name & Documentation Link.
#: includes/class-con-data-tracking.php:74
msgid "Want to help make %1$s even more awesome? Allow %1$s to collect non-sensitive diagnostic data and usage information and get 20%% off on your next purchase. <a href=\"%2$s\" target=\"_blank\">Find out more</a>."
msgstr ""

#: includes/class-con-data-tracking.php:82
msgid "Allow"
msgstr ""

#: includes/class-con-data-tracking.php:83
msgid "No thanks"
msgstr ""
