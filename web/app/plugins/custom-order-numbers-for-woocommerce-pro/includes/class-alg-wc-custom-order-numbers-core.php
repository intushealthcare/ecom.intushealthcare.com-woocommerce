<?php
/**
 * Custom Order Numbers for WooCommerce - Core Class
 *
 * @version 1.2.2
 * @since   1.0.0
 * @author  Tyche Softwares
 * @package Custom-order-numbers-for-WooCommerce
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'Alg_WC_Custom_Order_Numbers_Core' ) ) :
	/**
	 * Class Alg_WC_Custom_Order_Numbers_Core.
	 */
	class Alg_WC_Custom_Order_Numbers_Core {

		/**
		 * Constructor.
		 *
		 * @version 1.1.1
		 * @since   1.0.0
		 * @todo    [feature] (maybe) prefix / suffix per order (i.e. different prefix / suffix for different orders)
		 */
		function __construct() {
			if ( 'yes' === get_option( 'alg_wc_custom_order_numbers_enabled', 'yes' ) ) {
				add_action( 'woocommerce_new_order', array( $this, 'add_new_order_number' ), 10 );
				add_filter( 'woocommerce_order_number', array( $this, 'display_order_number' ), PHP_INT_MAX, 2 );
				add_action( 'admin_notices', array( $this, 'alg_custom_order_numbers_update_admin_notice' ) );
				add_action( 'admin_notices', array( $this, 'alg_custom_order_numbers_update_success_notice' ) );
				// Add a recurring As action.
				add_action( 'admin_init', array( $this, 'alg_custom_order_numbers_add_recurring_action' ) );
				// Recurring AS for Reset Counter Weekly.
				add_action( 'init', array( $this, 'alg_con_weekly_reset_recurring_action' ) );
				// Unschedule the AS for Reset Counter Weekly when the day selected is changed.
				add_filter( 'pre_update_option_alg_wc_custom_order_numbers_day_of_counter_reset_weekly', array( &$this, 'pre_alg_wc_custom_order_numbers_day_of_counter_reset_weekly' ), 10, 2 );
				add_filter( 'pre_update_option_alg_wc_custom_order_numbers_counter_reset_enabled', array( &$this, 'pre_alg_wc_custom_order_numbers_counter_reset_enabled' ), 10, 2 );
				add_action( 'admin_init', array( $this, 'alg_custom_order_numbers_stop_recurring_action' ) );
				add_action( 'alg_custom_order_numbers_weekly_reset_event', array( $this, 'alg_custom_order_numbers_weekly_reset_event_callback' ) );
				add_action( 'alg_custom_order_numbers_update_old_custom_order_numbers', array( $this, 'alg_custom_order_numbers_update_old_custom_order_numbers_callback' ) );
				// Include JS script for the notice.
				add_action( 'admin_enqueue_scripts', array( $this, 'alg_custom_order_numbers_setting_script' ) );
				add_action( 'wp_ajax_alg_custom_order_numbers_admin_notice_dismiss', array( $this, 'alg_custom_order_numbers_admin_notice_dismiss' ) );
				add_action( 'woocommerce_settings_save_alg_wc_custom_order_numbers', array( $this, 'woocommerce_settings_save_alg_wc_custom_order_numbers_callback' ), PHP_INT_MAX );
				if ( 'yes' === get_option( 'alg_wc_custom_order_numbers_order_tracking_enabled', 'yes' ) ) {
					add_action( 'init', array( $this, 'alg_remove_tracking_filter' ) );
					add_filter( 'woocommerce_shortcode_order_tracking_order_id', array( $this, 'add_order_number_to_tracking' ), PHP_INT_MAX );
				}
				add_filter( 'pre_update_option_alg_wc_custom_order_numbers_settings_to_apply', array( &$this, 'pre_alg_wc_custom_order_numbers_settings_to_apply' ), 10, 2 );
				add_action( 'woocommerce_shop_order_search_fields', array( $this, 'search_by_custom_number' ) );
				add_action( 'admin_menu', array( $this, 'add_renumerate_orders_tool' ), PHP_INT_MAX );
				if ( 'yes' === apply_filters( 'alg_wc_custom_order_numbers', 'no', 'manual_counter_value' ) ) {
					add_action( 'add_meta_boxes', array( $this, 'add_order_number_meta_box' ) );
					add_action( 'save_post_shop_order', array( $this, 'save_order_number_meta_box' ), PHP_INT_MAX, 2 );
				}
				// check if subscriptions is enabled.
				if ( in_array( 'woocommerce-subscriptions/woocommerce-subscriptions.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
					add_action( 'woocommerce_checkout_subscription_created', array( $this, 'update_custom_order_meta' ), PHP_INT_MAX, 1 );
					add_filter( 'wcs_renewal_order_created', array( $this, 'remove_order_meta_renewal' ), PHP_INT_MAX, 2 );
					// To unset the CON meta key at the time of renewal of subscription, so that renewal orders don't have duplicate order numbers.
					add_filter( 'wcs_renewal_order_meta', array( $this, 'remove_con_metakey_in_wcs_order_meta' ), 10, 3 );
				}
				// Compatibility with Germanized for WooCommerce Plugin. Passing CON meta key in their search query when return form is submitted using CON.
				add_filter( 'woocommerce_gzd_return_request_customer_order_number_meta_key', array( $this, 'alg_wc_custom_order_numbers_gzd_return_request_meta_key' ), 10 );
				// Filters to check whether the prefix/suffix are changed or not, if changed update orders in the database.
				add_filter( 'pre_update_option_alg_wc_custom_order_numbers_prefix', array( $this, 'pre_alg_wc_custom_order_numbers_prefix' ), 10, 2 );
				if ( '1' !== get_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed' ) ) {
					add_filter( 'pre_update_option_alg_wc_custom_order_numbers_date_prefix', array( $this, 'pre_alg_wc_custom_order_numbers_date_prefix' ), 10, 2 );
				}
				if ( '1' !== get_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed' ) ) {
					add_filter( 'pre_update_option_alg_wc_custom_order_numbers_min_width', array( $this, 'pre_alg_wc_custom_order_numbers_min_width' ), 10, 2 );
				}
				if ( '1' !== get_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed' ) ) {
					add_filter( 'pre_update_option_alg_wc_custom_order_numbers_suffix', array( $this, 'pre_alg_wc_custom_order_numbers_suffix' ), 10, 2 );
				}
				if ( '1' !== get_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed' ) ) {
					add_filter( 'pre_update_option_alg_wc_custom_order_numbers_date_suffix', array( $this, 'pre_alg_wc_custom_order_numbers_date_suffix' ), 10, 2 );
				}
				if ( '1' !== get_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed' ) ) {
					add_filter( 'pre_update_option_alg_wc_custom_order_numbers_template', array( $this, 'pre_alg_wc_custom_order_numbers_template' ), 10, 2 );
				}
			}
		}

		/**
		 * Function to run the Action Schedular on the day which is saved in the settings to run the weekly AS.
		 */
		public function alg_con_weekly_reset_recurring_action() {
			if ( 'weekly' === get_option( 'alg_wc_custom_order_numbers_counter_reset_enabled', 'no' ) ) {
				// Day on which the AS should run.
				$week_day_to_run_as = get_option( 'alg_wc_custom_order_numbers_day_of_counter_reset_weekly', 'mon' );
				// Current day.
				$current_day_date = date( 'd-m-Y' ); // phpcs:ignore
				$current_day      = date( "D", strtotime( $current_day_date ) ); // phpcs:ignore
				$current_day      = strtolower( $current_day ); // phpcs:ignore
				if ( function_exists( 'as_next_scheduled_action' ) ) { // Indicates that the AS library is present.
					if ( false === as_next_scheduled_action( 'alg_custom_order_numbers_weekly_reset_event' ) && $week_day_to_run_as === $current_day ) {
						as_schedule_recurring_action( time(), 86400 * 7, 'alg_custom_order_numbers_weekly_reset_event' );
					}
				}
			}
		}

		/**
		 * Callback function to run the weekly AS.
		 */
		public function alg_custom_order_numbers_weekly_reset_event_callback() {
			$current_time = current_time( 'timestamp' ); // phpcs:ignore
			update_option( 'alg_custom_order_numbers_weekly_as_time', $current_time );
		}

		/**
		 * Function to Unschedule the AS for Reset Counter Weekly when the day selected is changed.
		 *
		 * @param string $new_value New setting value which is selected.
		 * @param string $old_value Old setting value which is saved in the database.
		 */
		public function pre_alg_wc_custom_order_numbers_day_of_counter_reset_weekly( $new_value, $old_value ) {
			if ( $new_value !== $old_value ) {
				as_unschedule_all_actions( 'alg_custom_order_numbers_weekly_reset_event' );
			}
			return $new_value;
		}

		/**
		 * Function to Unschedule the AS for Reset Counter Weekly when Reset Counter is not set to weekly.
		 *
		 * @param string $new_value New setting value which is selected.
		 * @param string $old_value Old setting value which is saved in the database.
		 */
		public function pre_alg_wc_custom_order_numbers_counter_reset_enabled( $new_value, $old_value ) {
			if ( $new_value !== $old_value ) {
				if ( 'weekly' !== $new_value ) {
					as_unschedule_all_actions( 'alg_custom_order_numbers_weekly_reset_event' );
				}
			}
			return $new_value;
		}

		/**
		 * Enqueue JS script for showing fields as per the changes made in the settings.
		 *
		 * @version 1.4.0
		 * @since   1.4.0
		 */
		public static function alg_custom_order_numbers_setting_script() {
			global $wp_roles;
			$current_screen      = get_current_screen();
			$roles               = $wp_roles->roles;
			$role_keys           = array();
			$role_names          = array();
			$user_roles_selected = array();
			$prefix              = array();
			$suffix              = array();
			foreach ( $roles as $role_key => $role_name ) {
				$role_names[ $role_key ] = $role_name['name'];
			}
			if ( 'woocommerce_page_wc-settings' === $current_screen->id ) {
				$plugin_url       = plugins_url() . '/custom-order-numbers-for-woocommerce-pro';
				$numbers_instance = alg_wc_custom_order_numbers();
				wp_enqueue_script(
					'con_apply_setting',
					$plugin_url . '/assets/js/con-apply-setting.js',
					'',
					$numbers_instance->version,
					false
				);
				wp_localize_script(
					'con_apply_setting',
					'con_dismiss_param',
					array(
						'ajax_url' => admin_url( 'admin-ajax.php' ),
					)
				);
				wp_enqueue_style( 'style', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css', '', $numbers_instance->version, false );
				wp_enqueue_style( 'select2-css', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', '', $numbers_instance->version, false );
				wp_enqueue_script( 'select2-js', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', 'jquery', $numbers_instance->version, false );

				$user_roles = get_option( 'con_user_roles_data_array', '' );
				if ( is_array( $user_roles ) ) {
					foreach ( $user_roles as $key => $user_role ) {
						if ( 'roles' === $key ) {
							$user_roles_selected = $user_role;
						}
						if ( 'prefix' === $key ) {
							$prefix = $user_role;
						}
						if ( 'suffix' === $key ) {
							$suffix = $user_role;
						}
					}
				}
				wp_localize_script(
					'con_apply_setting',
					'con_user_role',
					array(
						'user_role'       => $user_roles_selected,
						'user_prefix'     => $prefix,
						'user_suffix'     => $suffix,
						'user_role_names' => $role_names,
						'user_role_keys'  => $role_keys,
					)
				);
			}
		}

		/**
		 * Function to show the admin notice to update the old CON meta key in the database when the plugin is updated.
		 *
		 * @version 1.4.0
		 * @since   1.4.0
		 */
		public static function alg_custom_order_numbers_update_admin_notice() {
			global $current_screen;
			$ts_current_screen = get_current_screen();
			// Return when we're on any edit screen, as notices are distracting in there.
			if ( ( method_exists( $ts_current_screen, 'is_block_editor' ) && $ts_current_screen->is_block_editor() ) || ( function_exists( 'is_gutenberg_page' ) && is_gutenberg_page() ) ) {
				return;
			}
			if ( 'yes' === get_option( 'alg_custom_order_numbers_show_admin_notice', '' ) ) {
				if ( '' === get_option( 'alg_custom_order_numbers_update_database', '' ) ) {
					?>
					<div class=''>
						<div class="con-pro-message notice notice-info" style="position: relative;">
							<p style="margin: 10px 0 10px 10px; font-size: medium;">
								<?php
									echo esc_html_e( 'From version 1.4.0, you can now search the orders by custom order numbers on the Orders page. In order to make the previous orders with custom order numbers searchable on Orders page, we need to update the database. Please click the “Update Now” button to do this. The database update process will run in the background.', 'custom-order-numbers-for-woocommerce' );
								?>
							</p>
							<p class="submit" style="margin: -10px 0 10px 10px;">
								<a class="button-primary button button-large" id="con-pro-update" href="edit.php?post_type=shop_order&action=alg_custom_order_numbers_update_old_con_in_database"><?php esc_html_e( 'Update Now', 'custom-order-numbers-for-woocommerce' ); ?></a>
							</p>
						</div>
					</div>
					<?php
				}
			}
		}

		/**
		 * Function to add a scheduled action when Update now button is clicked in admin notice.AS will run every 5 mins and will run the script to update the CON meta value in old orders.
		 *
		 * @version 1.4.0
		 * @since   1.4.0
		 */
		public function alg_custom_order_numbers_add_recurring_action() {
			if ( isset( $_REQUEST['action'] ) && 'alg_custom_order_numbers_update_old_con_in_database' === $_REQUEST['action'] ) {
				update_option( 'alg_custom_order_numbers_update_database', 'yes' );
				$current_time = current_time( 'timestamp' );
				update_option( 'alg_custom_order_numbers_time_of_update_now', $current_time );
				if ( function_exists( 'as_next_scheduled_action' ) ) { // Indicates that the AS library is present.
					as_schedule_recurring_action( time(), 300, 'alg_custom_order_numbers_update_old_custom_order_numbers' );
				}
				wp_safe_redirect( admin_url( 'edit.php?post_type=shop_order' ) );
				exit;
			}
		}

		/**
		 * Callback function for the AS to run the script to update the CON meta value for the old orders.
		 *
		 * @version 1.4.0
		 * @since   1.4.0
		 */
		public function alg_custom_order_numbers_update_old_custom_order_numbers_callback() {
			$args        = array(
				'post_type'      => 'shop_order',
				'posts_per_page' => 10000,
				'post_status'    => 'any',
				'meta_query'     => array(
					'relation' => 'AND',
					array(
						'key'     => '_alg_wc_custom_order_number',
						'compare' => 'EXISTS',
					),
					array(
						'key'     => '_alg_wc_custom_order_number_updated',
						'compare' => 'NOT EXISTS',
					),
				),
			);
			$loop_orders = new WP_Query( $args );
			if ( ! $loop_orders->have_posts() ) {
				update_option( 'alg_custom_order_numbers_no_old_orders_to_update', 'yes' );
				return;
			}
			foreach ( $loop_orders->posts as $order_ids ) {
				$order_id          = $order_ids->ID;
				$order_number_meta = get_post_meta( $order_id, '_alg_wc_custom_order_number', true );
				if ( '' === $order_number_meta ) {
					$order_number_meta = $order_id;
				}
				$is_wc_version_below_3 = version_compare( get_option( 'woocommerce_version', null ), '3.0.0', '<' );
				$order                 = wc_get_order( $order_id );
				$order_timestamp       = strtotime( ( $is_wc_version_below_3 ? $order->order_date : $order->get_date_created() ) );
				$time                  = get_option( 'alg_custom_order_numbers_time_of_update_now', '' );
				if ( $order_timestamp > $time ) {
					return;
				}
				$con_order_number = apply_filters( 'alg_wc_custom_order_numbers', sprintf( '%s%s', do_shortcode( get_option( 'alg_wc_custom_order_numbers_prefix', '' ) ), $order_number_meta ), 'value', array( 'order_timestamp' => $order_timestamp, 'order_number_meta' => $order_number_meta ) );
				update_post_meta( $order_id, '_alg_wc_full_custom_order_number', $con_order_number );
				update_post_meta( $order_id, '_alg_wc_custom_order_number_updated', 1 );
			}
			if ( 10000 > count( $loop_orders->posts ) ) {
				update_option( 'alg_custom_order_numbers_no_old_orders_to_update', 'yes' );
			}
		}

		/**
		 * Stop AS when there are no old orders left to update the CON meta key.
		 *
		 * @version 1.4.0
		 * @since   1.4.0
		 */
		public static function alg_custom_order_numbers_stop_recurring_action() {
			if ( 'yes' === get_option( 'alg_custom_order_numbers_no_old_orders_to_update', '' ) ) {
				as_unschedule_all_actions( 'alg_custom_order_numbers_update_old_custom_order_numbers' );
			}
		}

		/**
		 * Function to show the Success Notice when all the old orders CON meta value are updated.
		 *
		 * @version 1.4.0
		 * @since   1.4.0
		 */
		public function alg_custom_order_numbers_update_success_notice() {
			if ( 'yes' === get_option( 'alg_custom_order_numbers_no_old_orders_to_update', '' ) ) {
				if ( 'dismissed' !== get_option( 'alg_custom_order_numbers_success_notice', '' ) ) {
					?>
					<div>
						<div class="con-pro-message con-pro-success-message notice notice-success is-dismissible" style="position: relative;">
							<p>
								<?php
									echo esc_html_e( 'Database updated successfully. In addition to new orders henceforth, you can now also search the old orders on Orders page with the custom order numbers.', 'custom-order-numbers-for-woocommerce' );
								?>
							</p>
						</div>
					</div>
					<?php
				}
			}
		}

		/**
		 * Function to dismiss the admin notice.
		 *
		 * @version 1.4.0
		 * @since   1.4.0
		 */
		public function alg_custom_order_numbers_admin_notice_dismiss() {
			$admin_choice = isset( $_POST['admin_choice'] ) ? sanitize_text_field( wp_unslash( $_POST['admin_choice'] ) ) : ''; // phpcs:ignore
			update_option( 'alg_custom_order_numbers_success_notice', $admin_choice );
		}

		/**
		 * Function to save the time when the Apply to new order setting is saved.
		 *
		 * @param string $new_value New setting value which is selected.
		 * @param string $old_value Old setting value which is saved in the database.
		 * @version 1.4.0
		 * @since   1.4.0
		 */
		public function pre_alg_wc_custom_order_numbers_settings_to_apply( $new_value, $old_value ) {
			if ( $new_value !== $old_value ) {
				if ( 'new_order' === $new_value ) {
					$current_time = current_time( 'timestamp' );
					update_option( 'alg_custom_order_numbers_new_order_time', $current_time );
				}
			}
			return $new_value;
		}

		/**
		 * Function to update the prefix/suffix in the databse when settings are saved.
		 *
		 * @version 1.4.0
		 * @since   1.4.0
		 */
		public function woocommerce_settings_save_alg_wc_custom_order_numbers_callback() {
			if ( isset( $_GET['tab'] ) && 'alg_wc_custom_order_numbers' === $_GET['tab'] ) { //phpcs:ignore WordPress.Security.NonceVerification
				$user_role_data_array   = array();
				$user_role_array        = array();
				$user_role_prefix_array = array();
				$user_role_suffix_array = array();
				foreach ( $_POST as $key => $value ) { //phpcs:ignore WordPress.Security.NonceVerification
					if ( strpos( $key, 'con_user_role_' ) === 0 ) {
						$user_key_num = str_replace( 'con_user_role_', '', $key );
						$user_prefix  = $_POST['user_role_prefix_' . $user_key_num]; //phpcs:ignore
						$user_suffix  = $_POST['user_role_suffix_' . $user_key_num]; //phpcs:ignore
						if ( '' === $user_prefix && '' === $user_suffix ) {
							continue;
						}
						// foreach on current data.
						foreach ( $_POST[ $key ] as $keys => $role_value ) { //phpcs:ignore
							if ( isset( $roles[ $role_value ] ) ) { // checking if role is already added?
								foreach ( $roles[ $role_value ] as $role => $r_value ) { // loop throught the keys.
									// checks that the role is already added once.
									if ( ( $key_to_unset = array_search( $role_value, $user_role_array['roles'][ $r_value ] ) ) !== false ) { //phpcs:ignore
										unset( $user_role_array['roles'][ $r_value ][ $key_to_unset ] ); // remove that role and its data as its currently being added.
									}
									if ( empty( $user_role_array['roles'][ $r_value ] ) ) {
										unset( $user_role_array['roles'][ $r_value ] ); // if array is empty the remove the data.
									}
								}
							}
						}
						$user_role_array['roles'][ $key ] = $_POST[ $key ]; //phpcs:ignore
						// This foreach will prepare list of roles and its keys.
						foreach ( $_POST[ $key ] as $rolekey => $rolevalue ) { //phpcs:ignore
							if ( isset( $roles[ $rolevalue ] ) ) {
								$roles[ $rolevalue ] = array_push( $roles[ $rolevalue ], $key );
							} else {
								$roles[ $rolevalue ] = array( $key );
							}
						}
					}
					if ( strpos( $key, 'user_role_prefix_' ) === 0 ) {
						$user_role_prefix_array['prefix'][ $key ] = $_POST[ $key ]; //phpcs:ignore
					}
					if ( strpos( $key, 'user_role_suffix_' ) === 0 ) {
						$user_role_suffix_array['suffix'][ $key ] = $_POST[ $key ]; //phpcs:ignore
					}
				}
				$user_role_data_array = array_merge( $user_role_array, $user_role_prefix_array, $user_role_suffix_array );
				update_option( 'con_user_roles_data_array', $user_role_data_array );
			}
			if ( '1' === get_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed' ) ) {
				$args        = array(
					'post_type'      => 'shop_order',
					'post_status'    => 'any',
					'posts_per_page' => -1,
				);
				$loop_orders = new WP_Query( $args );
				if ( ! $loop_orders->have_posts() ) {
					update_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed', '' );
					return;
				}
				foreach ( $loop_orders->posts as $order_ids ) {
					$order_id          = $order_ids->ID;
					$order_number_meta = get_post_meta( $order_id, '_alg_wc_custom_order_number', true );
					if ( '' === $order_number_meta ) {
						$order_number_meta = $order_id;
					}
					$is_wc_version_below_3 = version_compare( get_option( 'woocommerce_version', null ), '3.0.0', '<' );
					$order                 = wc_get_order( $order_id );
					$order_timestamp       = strtotime( ( $is_wc_version_below_3 ? $order->order_date : $order->get_date_created() ) );
					$full_order_number     = apply_filters( 'alg_wc_custom_order_numbers', sprintf( '%s%s', do_shortcode( get_option( 'alg_wc_custom_order_numbers_prefix', '' ) ), $order_number_meta ), 'value',
						array(
							'order_timestamp'   => $order_timestamp,
							'order_number_meta' => $order_number_meta,
						)
					);
					$apply_settings        = get_option( 'alg_wc_custom_order_numbers_settings_to_apply', 'all_orders' );
					$order_con             = false;
					if ( 'new_order' === $apply_settings ) {
						update_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed', '' );
						return;
					} elseif ( 'date' === $apply_settings ) {
						$date = strtotime( get_option( 'alg_wc_custom_order_numbers_settings_to_apply_from_date' ) );
						if ( $order_timestamp >= $date ) {
							$order_con = true;
						}
					} elseif ( 'order_id' === $apply_settings ) {
						$saved_order_id = get_option( 'alg_wc_custom_order_numbers_settings_to_apply_from_order_id', '' );
						if ( $order_id >= $saved_order_id ) {
							$order_con = true;
						}
					} else {
						$order_con = true;
					}
					if ( $order_con ) {
						update_post_meta( $order_id, '_alg_wc_full_custom_order_number', $full_order_number );
						update_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed', '' );
					}
				}
			}
		}

		/**
		 * Sequential counter reset maybe_reset_sequential_counter.
		 *
		 * @version 1.2.2
		 * @since   1.1.2
		 * @todo    [dev] use MySQL transaction
		 */
		public function maybe_reset_sequential_counter( $current_order_number, $order_id ) {
			if ( 'no' != ( $reset_period = get_option( 'alg_wc_custom_order_numbers_counter_reset_enabled', 'no' ) ) ) {
				$previous_order_date   = get_option( 'alg_wc_custom_order_numbers_counter_previous_order_date', 0 );
				$order                 = wc_get_order( $order_id );
				$is_wc_version_below_3 = version_compare( get_option( 'woocommerce_version', null ), '3.0.0', '<' );
				$order_date            = ( $is_wc_version_below_3 ? $order->order_date : $order->get_date_created() );
				$current_order_date    = strtotime( $order_date );

				if ( ! $current_order_date || '' === $current_order_date ) {
					$current_order_date = current_time( 'timestamp' );
				}

				update_option( 'alg_wc_custom_order_numbers_counter_previous_order_date', $current_order_date );
				if ( 0 != $previous_order_date ) {
					$do_reset = false;
					switch ( $reset_period ) {
						case 'daily':
							$do_reset = (
								date( 'Y', $current_order_date ) != date( 'Y', $previous_order_date ) ||
								date( 'm', $current_order_date ) != date( 'm', $previous_order_date ) ||
								date( 'd', $current_order_date ) != date( 'd', $previous_order_date )
							);
							break;
						case 'monthly':
							$do_reset = (
								date( 'Y', $current_order_date ) != date( 'Y', $previous_order_date ) ||
								date( 'm', $current_order_date ) != date( 'm', $previous_order_date )
							);
							break;
						case 'yearly':
							$do_reset = (
								date( 'Y', $current_order_date ) != date( 'Y', $previous_order_date )
							);
							break;
						case 'weekly':
							$current_order_time = current_time( 'timestamp' ); // phpcs:ignore
							// Time of Weekly AS runed.
							$time_of_as_called = get_option( 'alg_custom_order_numbers_weekly_as_time' );
							if ( '' !== $time_of_as_called && $current_order_time > $time_of_as_called ) {
								$do_reset = true;
								update_option( 'alg_custom_order_numbers_weekly_as_time', '' );
							}
							break;
					}
					if ( $do_reset ) {
						return get_option( 'alg_wc_custom_order_numbers_counter_reset_counter_value', 1 );
					}
				}
			}
			return $current_order_number;
		}

		/**
		 * Save Order Number save_order_number_meta_box.
		 *
		 * @param int    $post_id Post ID.
		 * @param object $post Post object.
		 * @version 1.1.1
		 * @since   1.1.1
		 */
		public function save_order_number_meta_box( $post_id, $post ) {
			if ( ! isset( $_POST['alg_wc_custom_order_numbers_meta_box'] ) ) { // phpcs:ignore
				return;
			}
			$is_wc_version_below_3 = version_compare( get_option( 'woocommerce_version', null ), '3.0.0', '<' );
			$order                 = wc_get_order( $post_id );
			$order_timestamp       = strtotime( ( $is_wc_version_below_3 ? $order->order_date : $order->get_date_created() ) );
			$current_order_number  = '';
			if ( isset( $_POST['alg_wc_custom_order_number'] ) ) { // phpcs:ignore
				$current_order_number = sanitize_text_field( wp_unslash( $_POST['alg_wc_custom_order_number'] ) ); // phpcs:ignore
			}
			$full_custom_order_number = apply_filters(
				'alg_wc_custom_order_numbers',
				sprintf( '%s%s', do_shortcode( get_option( 'alg_wc_custom_order_numbers_prefix', '' ) ), $current_order_number ),
				'value',
				array(
					'order_timestamp'   => $order_timestamp,
					'order_number_meta' => $current_order_number,
				)
			);
			update_post_meta( $post_id, '_alg_wc_custom_order_number', $current_order_number );
			update_post_meta( $post_id, '_alg_wc_full_custom_order_number', $full_custom_order_number );
		}

		/**
		 * Add order number meta box add_order_number_meta_box.
		 *
		 * @version 1.1.1
		 * @since   1.1.1
		 */
		public function add_order_number_meta_box() {
			add_meta_box(
				'alg-wc-custom-order-numbers-meta-box',
				__( 'Order Number', 'custom-order-numbers-for-woocommerce' ),
				array( $this, 'create_order_number_meta_box' ),
				'shop_order',
				'side',
				'low'
			);
		}

		/**
		 * Function create_order_number_meta_box.
		 *
		 * @version 1.1.1
		 * @since   1.1.1
		 */
		public function create_order_number_meta_box() {
			$html  = '';
			$html .= '<input type="number" name="alg_wc_custom_order_number" style="width:100%;" value="' .
				get_post_meta( get_the_ID(), '_alg_wc_custom_order_number', true ) . '">';
			$html .= '<input type="hidden" name="alg_wc_custom_order_numbers_meta_box">';
			echo $html;
		}

		/**
		 * Add menu item.
		 *
		 * @version 1.2.2
		 * @since   1.0.0
		 */
		public function add_renumerate_orders_tool() {
			$hide_for_roles = get_option( 'alg_wc_custom_order_numbers_hide_menu_for_roles', array() );
			if ( ! empty( $hide_for_roles ) ) {
				$user       = wp_get_current_user();
				$user_roles = ( array ) $user->roles;
				$intersect  = array_intersect( $hide_for_roles, $user_roles );
				if ( ! empty( $intersect ) ) {
					return;
				}
			}
			add_submenu_page(
				'woocommerce',
				__( 'Renumerate Orders', 'custom-order-numbers-for-woocommerce' ),
				__( 'Renumerate Orders', 'custom-order-numbers-for-woocommerce' ),
				'manage_woocommerce',
				'alg-wc-renumerate-orders-tools',
				array( $this, 'create_renumerate_orders_tool' )
			);
		}

		/**
		 * Add Renumerate Orders tool to WooCommerce menu (the content).
		 *
		 * @version 1.2.0
		 * @since   1.0.0
		 * @todo    [dev] more results
		 */
		public function create_renumerate_orders_tool() {
			$html                   = '';
			$result_message         = '';
			$last_renumerated_order = 0;
			if ( isset( $_POST['alg_renumerate_orders'] ) ) {
				$total_renumerated_orders = $this->renumerate_orders();
				$last_renumerated_order   = $total_renumerated_orders[1];
				$total_renumerated_orders = $total_renumerated_orders[0];
				$result_message           = '<p><div class="updated"><p><strong>' .
					sprintf( __( '%d orders successfully renumerated!', 'custom-order-numbers-for-woocommerce' ), $total_renumerated_orders ) . '</strong></p></div></p>';
			}
			$html             .= '<h1>' . __( 'Renumerate Orders', 'custom-order-numbers-for-woocommerce' ) . '</h1>';
			$html             .= $result_message;
			$html             .= '<p>' . sprintf( __( 'Plugin settings: <a href="%s">WooCommerce > Settings > Custom Order Numbers</a>.', 'custom-order-numbers-for-woocommerce' ),
				admin_url( 'admin.php?page=wc-settings&tab=alg_wc_custom_order_numbers' ) ) . '</p>';
			$next_order_number = ( 0 != $last_renumerated_order ) ? ( $last_renumerated_order + 1 ) : get_option( 'alg_wc_custom_order_numbers_counter', 1 );
			$html             .= '<p>' . __( 'Press the button below to renumerate all existing orders.', 'custom-order-numbers-for-woocommerce' ) . '</p>';
			if ( 'sequential' === get_option( 'alg_wc_custom_order_numbers_counter_type', 'sequential' ) ) {
				$html .= '<p>' . sprintf( __( 'First order number will be <strong>%d</strong>.', 'custom-order-numbers-for-woocommerce' ), $next_order_number ) . '</p>';
			}
			$html .= '<form method="post" action="">';
			$html .= '<input class="button-primary" type="submit" name="alg_renumerate_orders" value="' . __( 'Renumerate orders', 'custom-order-numbers-for-woocommerce' ) . '"' .
				' onclick="return confirm(\'' . __( 'Are you sure?', 'custom-order-numbers-for-woocommerce' ) . '\')">';
			$html .= '</form>';
			echo '<div class="wrap">' . $html . '</div>';
		}

		/**
		 * Renumerate orders function.
		 *
		 * @version 1.1.2
		 * @since   1.0.0
		 */
		public function renumerate_orders() {
			if ( 'sequential' === get_option( 'alg_wc_custom_order_numbers_counter_type', 'sequential' ) && 'no' != get_option( 'alg_wc_custom_order_numbers_counter_reset_enabled', 'no' ) ) {
				update_option( 'alg_wc_custom_order_numbers_counter_previous_order_date', 0 );
			}
			$total_renumerated = 0;
			$last_renumerated  = 0;
			$offset            = 0;
			$block_size        = 512;
			while ( true ) {
				$args = array(
					'post_type'      => 'shop_order',
					'post_status'    => 'any',
					'posts_per_page' => $block_size,
					'orderby'        => 'date',
					'order'          => 'ASC',
					'offset'         => $offset,
					'fields'         => 'ids',
				);
				$loop = new WP_Query( $args );
				if ( ! $loop->have_posts() ) {
					break;
				}
				foreach ( $loop->posts as $order_id ) {
					$last_renumerated = $this->add_order_number_meta( $order_id, true );
					$total_renumerated++;
				}
				$offset += $block_size;
			}
			return array( $total_renumerated, $last_renumerated );
		}

		/**
		 * Function search_by_custom_number.
		 *
		 * @param array $metakeys Array of the metakeys to search order numbers on shop order page.
		 * @version 1.4.0
		 * @since   1.4.0
		 */
		public function search_by_custom_number( $metakeys ) {
			$metakeys[] = '_alg_wc_full_custom_order_number';
			return $metakeys;
		}

		/**
		 * Function add_order_number_to_tracking.
		 *
		 * @version 1.0.0
		 * @since   1.0.0
		 */
		public function add_order_number_to_tracking( $order_number ) {
			$offset     = 0;
			$block_size = 512;
			while ( true ) {
				$args = array(
					'post_type'      => 'shop_order',
					'post_status'    => 'any',
					'posts_per_page' => $block_size,
					'orderby'        => 'date',
					'order'          => 'DESC',
					'offset'         => $offset,
					'fields'         => 'ids',
				);
				$loop = new WP_Query( $args );
				if ( ! $loop->have_posts() ) {
					break;
				}
				foreach ( $loop->posts as $order_id ) {
					$_order        = wc_get_order( $order_id );
					$_order_number = $this->display_order_number( $order_id, $_order );
					if ( $_order_number === $order_number ) {
						return $order_id;
					}
				}
				$offset += $block_size;
			}
			return $order_number;
		}

		/**
		 * Display order number.
		 *
		 * @param String $order_number Order number of an order.
		 * @param Object $order order object.
		 * @version 1.4.0
		 * @since   1.0.0
		 */
		public function display_order_number( $order_number, $order ) {
			$is_wc_version_below_3 = version_compare( get_option( 'woocommerce_version', null ), '3.0.0', '<' );
			$order_id              = ( $is_wc_version_below_3 ? $order->id : $order->get_id() );
			$order_timestamp       = strtotime( ( $is_wc_version_below_3 ? $order->order_date : $order->get_date_created() ) );
			$user_role_data        = get_option( 'con_user_roles_data_array', '' );
			if ( 'yes' !== get_option( 'alg_custom_order_numbers_show_admin_notice', '' ) || 'yes' === get_option( 'alg_custom_order_numbers_no_old_orders_to_update', '' ) || ! empty( $user_role_data ) ) {
				$order_number_meta = get_post_meta( $order_id, '_alg_wc_full_custom_order_number', true );
				if ( '' === $order_number_meta ) {
					$order_number_meta = get_post_meta( $order_id, '_alg_wc_custom_order_number', true );
					if ( '' === $order_number_meta ) {
						$order_number_meta = $order_id;
					}
					$order_number_meta = apply_filters(
						'alg_wc_custom_order_numbers',
						sprintf( '%s%s', do_shortcode( get_option( 'alg_wc_custom_order_numbers_prefix', '' ) ), $order_number_meta ),
						'value',
						array(
							'order_timestamp'   => $order_timestamp,
							'order_number_meta' => $order_number_meta,
						)
					);
				}
				return $order_number_meta;
			} else {
				$order_number_meta = get_post_meta( $order_id, '_alg_wc_full_custom_order_number', true );
				if ( '' === $order_number_meta ) {
					$order_number_meta = get_post_meta( $order_id, '_alg_wc_custom_order_number', true );
					if ( '' === $order_number_meta ) {
						$order_number_meta = $order_id;
					}
					$order_number_meta = apply_filters(
						'alg_wc_custom_order_numbers',
						sprintf( '%s%s', do_shortcode( get_option( 'alg_wc_custom_order_numbers_prefix', '' ) ), $order_number_meta ),
						'value',
						array(
							'order_timestamp'   => $order_timestamp,
							'order_number_meta' => $order_number_meta,
						)
					);
				}
				return $order_number_meta;
			}
			return $order_number;
		}

		/**
		 * Function add_new_order_number.
		 *
		 * @version 1.0.0
		 * @since   1.0.0
		 */
		public function add_new_order_number( $order_id ) {
			$this->add_order_number_meta( $order_id, false );
		}

		/**
		 * Add/update order_number meta to order.
		 *
		 * @version 1.4.0
		 * @since   1.0.0
		 */
		public function add_order_number_meta( $order_id, $do_overwrite ) {
			if ( ! in_array( get_post_type( $order_id ), array( 'shop_order', 'shop_subscription' ) ) ) {
				return false;
			}
			if ( true === $do_overwrite || '' == get_post_meta( $order_id, '_alg_wc_custom_order_number', true ) ) {
				$is_wc_version_below_3 = version_compare( get_option( 'woocommerce_version', null ), '3.0.0', '<' );
				$order                 = wc_get_order( $order_id );
				$order_timestamp       = strtotime( ( $is_wc_version_below_3 ? $order->order_date : $order->get_date_created() ) );
				$counter_type          = get_option( 'alg_wc_custom_order_numbers_counter_type', 'sequential' );
				if ( 'sequential' === $counter_type ) {
					// Using MySQL transaction, so in case of a lot of simultaneous orders in the shop - prevent duplicate sequential order numbers.
					global $wpdb;
					$wpdb->query( 'START TRANSACTION' );
					$wp_options_table = $wpdb->prefix . 'options';
					$result_select    = $wpdb->get_row( "SELECT * FROM $wp_options_table WHERE option_name = 'alg_wc_custom_order_numbers_counter'" );
					if ( NULL != $result_select ) {
						$current_order_number = $this->maybe_reset_sequential_counter( $result_select->option_value, $order_id );
						$result_update = $wpdb->update(
							$wp_options_table,
							array( 'option_value' => ( $current_order_number + 1 ) ),
							array( 'option_name'  => 'alg_wc_custom_order_numbers_counter' )
						);
						if ( NULL != $result_update || $result_select->option_value == ( $current_order_number + 1 ) ) {
							$full_custom_order_number = apply_filters( 'alg_wc_custom_order_numbers', sprintf( '%s%s', do_shortcode( get_option( 'alg_wc_custom_order_numbers_prefix', '' ) ), $current_order_number ), 'value', array( 'order_timestamp' => $order_timestamp, 'order_number_meta' => $current_order_number ) );
							$wpdb->query( 'COMMIT' );   // all ok.
							update_post_meta( $order_id, '_alg_wc_custom_order_number', $current_order_number );
							update_post_meta( $order_id, '_alg_wc_full_custom_order_number', $full_custom_order_number );
						} else {
							$wpdb->query( 'ROLLBACK' ); // something went wrong, Rollback.
							return false;
						}
					} else {
						$wpdb->query( 'ROLLBACK' );  // phpcs:ignore    // something went wrong, Rollback.
						return false;
					}
				} elseif ( 'hash_crc32' === $counter_type ) {
					$current_order_number     = sprintf( "%u", crc32( $order_id ) );
					$full_custom_order_number = apply_filters( 'alg_wc_custom_order_numbers', sprintf( '%s%s', do_shortcode( get_option( 'alg_wc_custom_order_numbers_prefix', '' ) ), $current_order_number ), 'value', array( 'order_timestamp' => $order_timestamp, 'order_number_meta' => $current_order_number ) );
					update_post_meta( $order_id, '_alg_wc_custom_order_number', $current_order_number );
					update_post_meta( $order_id, '_alg_wc_full_custom_order_number', $full_custom_order_number );
				} else { // 'order_id'
					$current_order_number     = $order_id;
					$full_custom_order_number = apply_filters( 'alg_wc_custom_order_numbers', sprintf( '%s%s', do_shortcode( get_option( 'alg_wc_custom_order_numbers_prefix', '' ) ), $current_order_number ), 'value', array( 'order_timestamp' => $order_timestamp, 'order_number_meta' => $current_order_number ) );
					update_post_meta( $order_id, '_alg_wc_custom_order_number', $current_order_number );
					update_post_meta( $order_id, '_alg_wc_full_custom_order_number', $full_custom_order_number );
				}
				return $current_order_number;
			}
			return false;
		}

		/**
		 * Updates the custom order number for a renewal order created
		 * using WC Subscriptions
		 *
		 * @param WC_Order $renewal_order - Order Object of the renewed order.
		 * @param Object   $subscription - Subscription for which the order has been created.
		 * @return WC_Order $renewal_order
		 * @since 1.2.5
		 */
		public function remove_order_meta_renewal( $renewal_order, $subscription ) {
			$new_order_id = $renewal_order->get_id();
			// update the custom order number.
			$this->add_order_number_meta( $new_order_id, true );
			return $renewal_order;
		}

		/**
		 * Updates the custom order number for the WC Subscription
		 *
		 * @param Object $subscription - Subscription for which the order has been created.
		 * @since 1.2.5
		 */
		public function update_custom_order_meta( $subscription ) {
			$subscription_id = $subscription->get_id();
			// update the custom order number.
			$this->add_order_number_meta( $subscription_id, true );
		}

		/**
		 * Remove the WooCommerc filter which convers the order numbers to integers by removing the * * characters.
		 */
		public function alg_remove_tracking_filter() {

			remove_filter( 'woocommerce_shortcode_order_tracking_order_id', 'wc_sanitize_order_id' );
		}

		/**
		 * Function to unset the CON meta key at the time of renewal of subscription.
		 *
		 * @param Array  $meta Array of a meta key present in the subscription.
		 * @param Object $to_order  Order object.
		 * @param Objec  $from_order Subscription object.
		 */
		public function remove_con_metakey_in_wcs_order_meta( $meta, $to_order, $from_order ) {
			$to_order_id     = $to_order->get_id();
			$from_order_type = get_post_type( $from_order->get_id() );
			if ( 0 === $to_order_id && 'shop_subscription' === $from_order_type ) {
				foreach ( $meta as $key => $value ) {
					if ( '_alg_wc_custom_order_number' === $value['meta_key'] ) {
						unset( $meta[ $key ] );
					}
					if ( '_alg_wc_full_custom_order_number' === $value['meta_key'] ) {
						unset( $meta[ $key ] );
					}
				}
			}
			return $meta;
		}

    /**
		 * Function to pass the CON meta key in Germanized for WooCommerce Plugin, so that when the return form is submitted using the CON than it works.
		 */
		public function alg_wc_custom_order_numbers_gzd_return_request_meta_key() {
			return '_alg_wc_full_custom_order_number';
		}

		/**
		 * Function to see if prefix value is changed or not.
		 *
		 * @param string $new_value New setting value which is selected.
		 * @param string $old_value Old setting value which is saved in the database.
		 */
		public function pre_alg_wc_custom_order_numbers_prefix( $new_value, $old_value ) {
			if ( $new_value !== $old_value ) {
				update_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed', '1' );
			}
			return $new_value;
		}

		/**
		 * Function to see if date prefix value is changed or not.
		 *
		 * @param string $new_value New setting value which is selected.
		 * @param string $old_value Old setting value which is saved in the database.
		 */
		public function pre_alg_wc_custom_order_numbers_date_prefix( $new_value, $old_value ) {
			if ( $new_value !== $old_value ) {
				update_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed', '1' );
			}
			return $new_value;
		}

		/**
		 * Function to see if order number width value is changed or not.
		 *
		 * @param string $new_value New setting value which is selected.
		 * @param string $old_value Old setting value which is saved in the database.
		 */
		public function pre_alg_wc_custom_order_numbers_min_width( $new_value, $old_value ) {
			if ( $new_value !== $old_value ) {
				update_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed', '1' );
			}
			return $new_value;
		}

		/**
		 * Function to see if suffix value is changed or not.
		 *
		 * @param string $new_value New setting value which is selected.
		 * @param string $old_value Old setting value which is saved in the database.
		 */
		public function pre_alg_wc_custom_order_numbers_suffix( $new_value, $old_value ) {
			if ( $new_value !== $old_value ) {
				update_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed', '1' );
			}
			return $new_value;
		}

		/**
		 * Function to see if date suffix value is changed or not.
		 *
		 * @param string $new_value New setting value which is selected.
		 * @param string $old_value Old setting value which is saved in the database.
		 */
		public function pre_alg_wc_custom_order_numbers_date_suffix( $new_value, $old_value ) {
			if ( $new_value !== $old_value ) {
				update_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed', '1' );
			}
			return $new_value;
		}

		/**
		 * Function to see if order number template value is changed or not.
		 *
		 * @param string $new_value New setting value which is selected.
		 * @param string $old_value Old setting value which is saved in the database.
		 */
		public function pre_alg_wc_custom_order_numbers_template( $new_value, $old_value ) {
			if ( $new_value !== $old_value ) {
				update_option( 'alg_wc_custom_order_numbers_prefix_suffix_changed', '1' );
			}
			return $new_value;
		}
	}

endif;

return new Alg_WC_Custom_Order_Numbers_Core();
