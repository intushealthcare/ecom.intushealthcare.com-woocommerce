<?php
/**
 * Custom Order Numbers for WooCommerce - Data Tracking Functions
 *
 * @version 1.0.0
 * @since   1.3.0
 * @package Custom Order Numbers/Data Tracking
 * @author  Tyche Softwares
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'Con_Tracking_Functions' ) ) :

	/**
	 * Custom Order Number Data Tracking Functions.
	 */
	class Con_Tracking_Functions {

		/**
		 * Construct.
		 *
		 * @since 1.3.0
		 */
		public function __construct() {

		}

		/**
		 * Returns plugin data for tracking.
		 *
		 * @param array $data - Generic data related to WP, WC, Theme, Server and so on.
		 * @return array $data - Plugin data included in the original data received.
		 * @since 1.3.0
		 */
		public static function con_pro_plugin_tracking_data( $data ) {

			$plugin_data = array(
				'ts_meta_data_table_name' => 'ts_tracking_con_meta_data',
				'ts_plugin_name'          => 'Custom Order Numbers Pro for WooCommerce',
				'global_settings'         => self::con_get_global_settings(),
				'license_data'            => self::con_get_license_data(),
				'orders_count'            => self::con_get_custom_order_numbers_count(),
			);

			$data['plugin_data'] = $plugin_data;

			return $data;
		}

		/**
		 * Send the global settings for tracking.
		 *
		 * @since 1.3.0
		 */
		public static function con_get_global_settings() {

			$global_settings = array(
				'alg_wc_custom_order_numbers_enabled'      => get_option( 'alg_wc_custom_order_numbers_enabled' ),
				'alg_wc_custom_order_numbers_counter_type' => get_option( 'alg_wc_custom_order_numbers_counter_type' ),
				'alg_wc_custom_order_numbers_counter'      => get_option( 'alg_wc_custom_order_numbers_counter' ),
				'alg_wc_custom_order_numbers_counter_reset_enabled' => get_option( 'alg_wc_custom_order_numbers_counter_reset_enabled' ),
				'alg_wc_custom_order_numbers_counter_reset_counter_value' => get_option( 'alg_wc_custom_order_numbers_counter_reset_counter_value' ),
				'alg_wc_custom_order_numbers_prefix'       => get_option( 'alg_wc_custom_order_numbers_prefix' ),
				'alg_wc_custom_order_numbers_date_prefix'  => get_option( 'alg_wc_custom_order_numbers_date_prefix' ),
				'alg_wc_custom_order_numbers_min_width'    => get_option( 'alg_wc_custom_order_numbers_min_width' ),
				'alg_wc_custom_order_numbers_suffix'       => get_option( 'alg_wc_custom_order_numbers_suffix' ),
				'alg_wc_custom_order_numbers_date_suffix'  => get_option( 'alg_wc_custom_order_numbers_date_suffix' ),
				'alg_wc_custom_order_numbers_template'     => get_option( 'alg_wc_custom_order_numbers_template' ),
				'alg_wc_custom_order_numbers_order_tracking_enabled' => get_option( 'alg_wc_custom_order_numbers_order_tracking_enabled' ),
				'alg_wc_custom_order_numbers_search_by_custom_number_enabled' => get_option( 'alg_wc_custom_order_numbers_search_by_custom_number_enabled' ),
				'alg_wc_custom_order_numbers_manual_enabled' => get_option( 'alg_wc_custom_order_numbers_manual_enabled' ),
				'alg_wc_custom_order_numbers_hide_menu_for_roles' => get_option( 'alg_wc_custom_order_numbers_hide_menu_for_roles' ),
				'alg_wc_custom_order_numbers_hide_tab_for_roles' => get_option( 'alg_wc_custom_order_numbers_hide_tab_for_roles' ),
			);

			return wp_json_encode( $global_settings );

		}

		/**
		 * Send the license data for data tracking.
		 *
		 * @since 1.3.0
		 */
		public static function con_get_license_data() {

			return wp_json_encode(
				array(
					'license_key'    => get_option( 'edd_license_key_con' ),
					'license_status' => get_option( 'edd_license_key_con_status' ),
				)
			);
		}

		/**
		 * Send the total count for orders which have been assigned custom numbers.
		 *
		 * @since 1.3.0
		 */
		public static function con_get_custom_order_numbers_count() {

			global $wpdb;

			$orders_count = $wpdb->get_var( $wpdb->prepare( 'SELECT count(post_id) FROM ' . $wpdb->prefix . 'postmeta where meta_key = %s', '_alg_wc_custom_order_number' ) ); //phpcs:ignore
			return $orders_count;
		}

	}

endif;

$con_tracking_functions = new Con_Tracking_Functions();
