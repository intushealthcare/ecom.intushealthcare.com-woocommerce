<?php
/**
 * Currency Per Product for WooCommerce - License Settings Section
 *
 * @version 1.4.3
 * @since   1.4.3
 * @author  Tyche Softwares
 *
 * @package Custom-order-numbers-for-WooCommerce/includes/admin/license-settings
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
if ( ! class_exists( 'Alg_WC_CON_Settings_License' ) ) :
	/**
	 * Class Alg_WC_CON_Settings_License extends Alg_WC_Custom_Order_Numbers_Settings_Section
	 */
	class Alg_WC_CON_Settings_License extends Alg_WC_Custom_Order_Numbers_Settings_Section {

		/**
		 * Constructor.
		 *
		 * @version 1.4.3
		 * @since   1.4.3
		 */
		public function __construct() {
			$this->id   = 'license';
			$this->desc = __( 'License', 'custom-order-numbers-for-woocommerce' );
			parent::__construct();
		}

		/**
		 * Function settings_section.
		 *
		 * @param array $sections Settings section.
		 * @version 1.4.3
		 * @since   1.4.3
		 */
		public function settings_section( $sections ) {
			$sections[ $this->id ] = $this->desc;
			return $sections;
		}

		/**
		 * Function get_settings.
		 *
		 * @version 1.4.3
		 * @since   1.4.3
		 */
		public function get_settings() {

			$license = get_option( 'edd_license_key_con' );
			$status  = get_option( 'edd_license_key_con_status' );
			if ( isset( $_SERVER['HTTPS'] ) && 'on' === $_SERVER['HTTPS'] ) {

				if ( isset( $_SERVER['HTTP_HOST'] ) && isset( $_SERVER['REQUEST_URI'] ) ) {
					$current_link = 'https://' . sanitize_text_field( wp_unslash( $_SERVER['HTTP_HOST'] ) ) . sanitize_text_field( wp_unslash( $_SERVER['REQUEST_URI'] ) );
				}
			} else {

				if ( isset( $_SERVER['HTTP_HOST'] ) && isset( $_SERVER['REQUEST_URI'] ) ) {
					$current_link = 'http://' . sanitize_text_field( wp_unslash( $_SERVER['HTTP_HOST'] ) ) . sanitize_text_field( wp_unslash( $_SERVER['REQUEST_URI'] ) );
				}
			}

			$link = '';
			if ( false !== $license ) {
				$license_nonce = wp_create_nonce( 'con-activate-nonce' );
				if ( false !== $status && 'valid' === $status ) {
					$link = '<span style="color:green;">active</span>' .
					wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ) .
					'<a href="' . $current_link . '&_con_nonce=' . $license_nonce . '&license=con_deactivate" class="button-secondary" name="edd_con_license_deactivate">Deactivate<a/>';
				} else {
					$link = wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ) .
					'<a href="' . $current_link . '&_con_nonce=' . $license_nonce . '&license=con_activate" class="button-secondary" name="edd_con_license_activate">Activate<a/>';
				}
			}

			$license_settings = array(
				array(
					'title' => __( 'Plugin License Options', 'custom-order-numbers-for-woocommerce' ),
					'type'  => 'title',
					'id'    => 'alg_wc_con_license_options',
				),
				array(
					'title'   => __( 'License Key	', 'custom-order-numbers-for-woocommerce' ),
					'desc'    => __( 'Enter your license key.', 'custom-order-numbers-for-woocommerce' ),
					'id'      => 'edd_license_key_con',
					'default' => '',
					'type'    => 'text',
				),
				array(
					'title'   => __( 'Activate License', 'custom-order-numbers-for-woocommerce' ),
					'desc'    => __( $link, 'custom-order-numbers-for-woocommerce' ), // phpcs:ignore
					'id'      => 'edd_license_con_hidden_button',
					'default' => '',
					'type'    => 'text',
					'css'     => 'display:none;',
				),
				array(
					'type' => 'sectionend',
					'id'   => 'alg_wc_con_license_options',
				),
			);

			return $license_settings;
		}

	}

	endif;

	return new Alg_WC_CON_Settings_License();
