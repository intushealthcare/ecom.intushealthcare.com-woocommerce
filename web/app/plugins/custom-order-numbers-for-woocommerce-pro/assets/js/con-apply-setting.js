/**
 * To show fields as per the changes in the settings fields and dismiss success notice.
 *
 * @namespace custom_order_numbers_pro
 * @since 1.4.0
 */
jQuery(document).ready(function($) {
    var con_apply_setting_value = $('#alg_wc_custom_order_numbers_settings_to_apply').val();
    if ( con_apply_setting_value == 'new_order' || con_apply_setting_value == 'all_orders' ) {

        $('[id=alg_wc_custom_order_numbers_settings_to_apply_from_date]').closest('tr').hide();
        $('[id=alg_wc_custom_order_numbers_settings_to_apply_from_order_id]').closest('tr').hide();
    }
    if ( con_apply_setting_value == 'order_id' ) {
        $('[id=alg_wc_custom_order_numbers_settings_to_apply_from_date]').closest('tr').hide();
    }
    if ( con_apply_setting_value == 'date' ) {
        $('[id=alg_wc_custom_order_numbers_settings_to_apply_from_order_id]').closest('tr').hide();
    }
    $('#alg_wc_custom_order_numbers_settings_to_apply').change( function(){
        var con_apply_setting_value = $('#alg_wc_custom_order_numbers_settings_to_apply').val();
        if ( con_apply_setting_value == 'order_id' ) {
            $('[id=alg_wc_custom_order_numbers_settings_to_apply_from_order_id]').closest('tr').show();
        } else {
            $('[id=alg_wc_custom_order_numbers_settings_to_apply_from_order_id]').closest('tr').hide();
        }
        if ( con_apply_setting_value == 'date' ) {
            $('[id=alg_wc_custom_order_numbers_settings_to_apply_from_date]').closest('tr').show();
        } else {
            $('[id=alg_wc_custom_order_numbers_settings_to_apply_from_date]').closest('tr').hide();
        }
    });
    var con_reset_counter_value = $('#alg_wc_custom_order_numbers_counter_reset_enabled').val();
    if ( con_reset_counter_value == 'no' || con_reset_counter_value == 'daily' || con_reset_counter_value == 'monthly' || con_reset_counter_value == 'yearly' ) {
        $('[id=alg_wc_custom_order_numbers_day_of_counter_reset_weekly]').closest('tr').hide();
    }
    // To hide/show the dropdown of the days to select the weekly reset counter.
    $('#alg_wc_custom_order_numbers_counter_reset_enabled').change( function(){
        var con_reset_counter_value = $('#alg_wc_custom_order_numbers_counter_reset_enabled').val();
        if ( con_reset_counter_value == 'no' || con_reset_counter_value == 'daily' || con_reset_counter_value == 'monthly' || con_reset_counter_value == 'yearly' ) {
            $('[id=alg_wc_custom_order_numbers_day_of_counter_reset_weekly]').closest('tr').hide();
        }
        console.log(con_reset_counter_value);
        if ( con_reset_counter_value == 'weekly' ) {
            $('[id=alg_wc_custom_order_numbers_day_of_counter_reset_weekly]').closest('tr').show();
        }
    });
    jQuery( '.con-pro-success-message' ).on( 'click', '.notice-dismiss', function() {

		var data = {
			admin_choice: 'dismissed',
			action: 'alg_custom_order_numbers_admin_notice_dismiss'
		};
		jQuery.post( con_dismiss_param.ajax_url, data, function() {
		});
    });

    var today = new Date();
    $('#alg_wc_custom_order_numbers_settings_to_apply_from_date').datepicker({
        format: 'mm-dd-yyyy',
        autoclose:true,
        endDate: "today",
        maxDate: today
    }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
    $('#alg_wc_custom_order_numbers_settings_to_apply_from_date').keyup(function () {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9^-]/g, '');
        }
    });

    // User role options.
    let option_value = "";
    var user_roles = con_user_role.user_role_names;
    for (var key in user_roles) {
        var value = user_roles[key];
        option_value += "<option value=" + key + ">" + value + "</option>";
    }
    var user_role = '';
    if ( null !== con_user_role.user_role ) {
        var user_role = con_user_role.user_role;
    }
    if ( null !== con_user_role.user_prefix ) {
        var user_prefix = con_user_role.user_prefix;
    }
    if (  null !== con_user_role.user_suffix ) {
       var user_suffix = con_user_role.user_suffix;
    }
    // User roles selected and saved.
    if ( user_role != '' ) {
        $('[id=con_add_user_role]').hide();
        for (var key in user_role) {
            var num          = key.replace('con_user_role_','');
            var values       = user_role[key];
            var user_roles   = con_user_role.user_role_names;
            var prefix_key   = 'user_role_prefix_'+num;
            var prefix_value ='';
            if( '' != user_prefix[prefix_key] ) {
                prefix_value = 'value="'+user_prefix[prefix_key]+'"';
            }
            var suffix_key   = 'user_role_suffix_'+num;
            var suffix_value = '';
            if ( '' != user_suffix[suffix_key] ) {
                suffix_value = 'value="'+user_suffix[suffix_key]+'"';
            }
            // div id.
            div_id = 'user_role_rows_'+num;
            // Insert user role rows.
            $(" <div class='forminp forminp-multiselect'> <div id="+div_id+" style='margin-bottom:15px'> <div class='col-md-6'> <select id='con_user_role_"+num+"' name='con_user_role_"+num+"[]' style='height:15px; width:175px' class='js-states form-control' multiple='multiple' > <option value=guest>Guest</option>" + option_value + "</select> <input type='text' placeholder='Prefix' id='user_role_prefix_"+num+"' name='user_role_prefix_"+num+"' "+prefix_value+" style='width:150px'></input> <input type='text'placeholder='Suffix' id='user_role_suffix_"+num+"' name='user_role_suffix_"+num+"' "+suffix_value+" style='width:150px'></input><spam style='margin-left:10px' ><i id='con_trash_"+num+"' class='fa fa-trash-o fa-2x' ></i></spam> </div> </div> </div>").insertBefore("#con_add_user_role");
            var select_id = 'con_user_role_'+num;
            for ( var key in values ) {
                $("#"+select_id+" option[value="+values[key]+"]").prop("selected", "selected");
            }
            $("#con_user_role_"+num).select2({
                placeholder: "Select a user role",
            });
        }
        $(" <button id='user_role_adding' name='user_role_adding' class='button-secondary' type='button' style='margin-left:10px'> +Add more roles </button>").insertAfter('#user_role_suffix_'+num);
    }
    // Add rows when clicked on Add Roles button.
    $( "#con_add_user_role" ).click(function() {
        $('[id=con_add_user_role]').hide();
        $(" <div class='forminp forminp-multiselect'> <div id='user_role_rows_0' name='user_role_rows' style='margin-bottom:15px' class='container bg-danger'> <div class='col-md-6'> <select id='con_user_role_0' name='con_user_role_0[]' class='js-states form-control' multiple='multiple' ><option value=guest>Guest</option>" + option_value + "</select> <input type='text' placeholder='Prefix' id='user_role_prefix_0' name='user_role_prefix_0' style='width:150px'></input> <input type='text'placeholder='Suffix' id='user_role_suffix_0' name='user_role_suffix_0' style='width:150px'></input> <button id='user_role_adding' name='user_role_adding' class='button-secondary' type='button' disabled> +Add more roles </button><spam style='margin-left:10px' ><i id='con_trash_0' class='fa fa-trash-o fa-2x' ></i></spam> </div> </div> </div>").insertAfter("#con_add_user_role");
        $("#con_user_role_0").select2({
            placeholder: "Select a user role",
        });
    });
    // For removing the disabled attribute when role is selected.
    $('.description').on('change','[id^=con_user_role_]',function(){
        var id = this.id;
        var user_role_selected = $('#'+id).val();
        if ( 'no_role_selected' !== user_role_selected ) {
            $('#user_role_adding').removeAttr("disabled");
        }
        if ( '' == user_role_selected ) {
            $("#user_role_adding").attr('disabled','disabled'); //disable 
        }
    });
    // Counter number for adding the rows.
    var val = 0;
    let mybutton = document.querySelector('#user_role_adding');
    let counter = 0;
    if ( user_role != '' ) {
        counter = $( "[id^=user_role_rows_]" ).length - 1;
    }
    // Add user roles row when clicked on Add more roles.
    $(document).on( 'click', '#user_role_adding', function(){
        $("#user_role_adding").remove();
        row_number = counter;
        counter++;
        var row_present = document.getElementById("#user_role_rows_0");
        if($("#user_role_rows_"+row_number).length == 0) {
            row_number = $( "[id^=user_role_rows_]" ).length - 1;
            if ( $("#user_role_rows_"+row_number).length == 0 ) {
                var con_row_presents = document.querySelectorAll("[id^=user_role_rows_]");
                var last_row_value = con_row_presents[con_row_presents.length- 1];
                var last_row_id = last_row_value.id;
                var split_id = last_row_id.split("_");
                var row_number = split_id[3];
                counter = parseInt(row_number)+1;
            }
        }
        var div_id = 'user_role_rows_'+counter;
        $(" <div class='forminp forminp-multiselect'> <div id="+div_id+" style='margin-bottom:15px' class='container bg-danger'> <div class='col-md-6'> <select id='con_user_role_"+counter+"' name='con_user_role_"+counter+"[]' class='js-states form-control' multiple='multiple'> <option value=guest>Guest</option>" + option_value + "</select> <input type='text' placeholder='Prefix' id='user_role_prefix_"+counter+"' name='user_role_prefix_"+counter+"' style='width:150px'></input> <input type='text'placeholder='Suffix' id='user_role_suffix_"+counter+"' name='user_role_suffix_"+counter+"' style='width:150px'></input> <button id='user_role_adding' name='user_role_adding' class='button-secondary' type='button' disabled> +Add more roles </button> <spam style='margin-left:10px'><i id='con_trash_"+counter+"' class='fa fa-trash-o fa-2x' ></i></spam></div> </div> </div>").insertAfter("#user_role_rows_"+row_number);
        $("#con_user_role_"+counter+"").select2();
        $("#con_user_role_"+counter).select2({
            placeholder: "Select a user role",
        });
    });
    // Delete the particular row when clicked on trash icon.
    $('.description').on('click','[id^=con_trash_]',function(){
        var id = this.id;
        var split_id = id.split("_");
        var deleteindex = split_id[2];
        $("#user_role_rows_" + deleteindex).remove();
        var check_buttons = $( "#user_role_adding" ).length;
        if ( 0 == check_buttons ) {
            var div_row = deleteindex - 1;
            if ( 0 != $( "#user_role_rows_" + div_row ).length ) {
                $(" <button id='user_role_adding' name='user_role_adding' class='button-secondary' type='button' style='margin-left:10px'> +Add more roles </button>").insertAfter('#user_role_suffix_'+div_row);
            }
            if ( 0 == $( "#user_role_rows_" + div_row ).length ) {
                $(" <button id='user_role_adding' name='user_role_adding' class='button-secondary' type='button' style='margin-left:10px'> +Add more roles </button>").insertAfter('#user_role_suffix_0');
            }
        }
        var check_div = $( "[id^=user_role_rows_]" ).length;
        if ( 0 == check_div ) {
            $('[id=con_add_user_role]').show();
        }
        if ( 1 == check_div) {
            var check_button = $( "#user_role_adding" ).length;
            if ( 0 == check_button) {
                $(" <button id='user_role_adding' name='user_role_adding' class='button-secondary' type='button' style='margin-left:10px'> +Add more roles </button>").insertAfter('[id^=user_role_suffix_]');
            }
        }
    });
});