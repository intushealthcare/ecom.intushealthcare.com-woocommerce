/**
 * Data Tracking notice.
 *
 * @namespace custom_order_numbers_pro
 * @since 1.3.0
 */
// Tracking Notice dismissed.
jQuery(document).ready( function() {
	
	jQuery( '.con-pro-tracker' ).on( 'click', '.notice-dismiss', function() {

		var data = {
			admin_choice: 'dismissed',
			action: 'con_pro_admin_choice'
		};

		jQuery.post( con_dismiss_params.ajax_url, data, function() {
		});
	});

});