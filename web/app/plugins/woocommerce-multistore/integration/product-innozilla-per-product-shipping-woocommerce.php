<?php
/**
 * Integrates the plugin Product Innozilla Per Product Shipping for WooCommerce
 * 
 * Plugin URL: https://wordpress.org/plugins/innozilla-per-product-shipping-woocommerce/
 * 
 * @since 4.1.2
 */

defined( 'ABSPATH' ) || exit;

class WOO_MSTORE_INTEGRATION_PRODUCT_INNOZILLA_PER_PRODUCT_SHIPPING_WOOCOMMERCE {

	/**
	 * The array of whitelisted metadata
	 *
	 * @access   protected
	 * @var      array    $whitelist    The array of whitelisted metadata for automatic syncing.
	 */
	protected $whitelist;


	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 */
	public function __construct() {
		$this->whitelist = array(
            '_per_product_shipping',
            '_per_product_shipping_add_to_all',
        );

		if ( is_multisite() ) {
			add_filter( 'WOO_MSTORE_admin_product/slave_product_meta_to_update', array( $this, 'sync_whitelist' ), PHP_INT_MAX, 2 );
			add_action( 'WOO_MSTORE_admin_product/slave_product_updated', array( $this, 'multiste_complete_innozilla_per_product_shipping_rules_woo' ), 10, 1 );
		} else {
			add_filter( 'WOO_MSTORE_SYNC/process_json/meta', array( $this, 'sync_whitelist_standalone' ), PHP_INT_MAX, 3 );
			
			add_filter( 'WOO_MSTORE_SYNC/process_json/product', array( $this, 'sync_innozilla_per_product_shipping_rules_woo' ), PHP_INT_MAX, 3 );
			add_action( 'WOO_MSTORE_SYNC/sync_child/complete', array( $this, 'complete_innozilla_per_product_shipping_rules_woo' ), PHP_INT_MAX, 3 );
		}
	}

	/**
	 * Syncs the whitelisted metadata for multisite
	 *
	 * @return  array array of metada key and meta value.
	 */
	public function sync_whitelist( $meta_data, $data ) {
		foreach ( $this->whitelist as $whitelisted_metakey ) {
			$meta_value                        = $data['master_product']->get_meta( $whitelisted_metakey, true );
			$meta_data[ $whitelisted_metakey ] = $meta_value;
		}

		return $meta_data;
	}


	/**
	 * Sync metadata on regular WordPress site.
	 *
	 * @since 1.0.1
	 *
	 * @param mixed $_whitelisted_meta
	 * @param mixed $product_id
	 * @param mixed $wc_product
	 * @return void
	 */
	public function sync_whitelist_standalone( $_whitelisted_meta, $product_id, $wc_product ) {
		foreach ( $this->whitelist as $whitelisted_metakey ) {
			$meta_value                                = get_post_meta( $product_id, $whitelisted_metakey, true );
			$_whitelisted_meta[ $whitelisted_metakey ] = $meta_value;
		}

		return $_whitelisted_meta;
	}
	
	/* 
	Get Innozilla Per Product Shipping Rules Woo - Regular and Multisite
	*/
	public function get_innozilla_per_product_shipping_rules_woo( $product_id ) {
		
		global $wpdb;
		
		if ( is_multisite() ) {
			$curre_blog_id = get_current_blog_id();	
			/**
			 ** The function is running on child context. We need to switch to previous/parent blog
			**/
			restore_current_blog(); 
		}
		$per_product_shipping_rules = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . 'innozilla_per_product_shipping_rules_woo WHERE product_id=' . (int) $product_id, ARRAY_A );
		if ( is_multisite() ) {
			//revert to the previous child blog ID
			switch_to_blog($curre_blog_id);
		}
		return $per_product_shipping_rules;
	}
	
	/* 
	Sync Innozilla Per Product Shipping Rules Woo - Regular Site
	*/
	public function sync_innozilla_per_product_shipping_rules_woo( $product, $wc_product, $product_id ) {
		
		global $wpdb;
		$product['per_product_shipping_rules'] = $this->get_innozilla_per_product_shipping_rules_woo( $product_id );
		return $product;
	}
	
	/**
	** Complete Innozilla Per Product Shipping Rules Woo - Regular Site
	**/
	public function complete_innozilla_per_product_shipping_rules_woo( $wc_product_id, $parent_id, $product )
	{	
		global $wpdb;
		
		/* Remove all linked rules to product */
		$this->unlink_all_linked_rules_to_product($wc_product_id);	
		
		$per_product_shipping_rules = $product['per_product_shipping_rules'];		
		if ( !empty($per_product_shipping_rules) )
		{			
			foreach( $per_product_shipping_rules as $per_product_shipping_rule )
			{
				unset($per_product_shipping_rule['iz_rule_id']);
				$per_product_shipping_rule['product_id'] = $wc_product_id;				
				$wpdb->insert( $wpdb->prefix . 'innozilla_per_product_shipping_rules_woo', $per_product_shipping_rule);
			}
		}
	}
	
	/**
	** Complete Innozilla Per Product Shipping Rules Woo - MultiSite
	**/
	public function multiste_complete_innozilla_per_product_shipping_rules_woo( $data )
	{	
		global $wpdb;	
		
		/* Remove all linked rules to product */
		$this->unlink_all_linked_rules_to_product( $data['slave_product']->get_id() );
		
		$per_product_shipping_rules = $this->get_innozilla_per_product_shipping_rules_woo( $data['master_product']->get_id() );
		if ( !empty($per_product_shipping_rules) )
		{
			foreach( $per_product_shipping_rules as $per_product_shipping_rule )
			{			
				unset($per_product_shipping_rule['iz_rule_id']);
				$per_product_shipping_rule['product_id'] = $data['slave_product']->get_id();		
				$wpdb->insert( $wpdb->prefix . 'innozilla_per_product_shipping_rules_woo', $per_product_shipping_rule);
			}
		}
	}
	
	/**
	** Unlink all linked resources - Regular and Multisite
	**/
	public function unlink_all_linked_rules_to_product($wc_product_id) 
	{
		global $wpdb;
		$wpdb->delete( $wpdb->prefix . 'innozilla_per_product_shipping_rules_woo', array(
				'product_id'  =>  $wc_product_id, 
		));
	}
}
new WOO_MSTORE_INTEGRATION_PRODUCT_INNOZILLA_PER_PRODUCT_SHIPPING_WOOCOMMERCE();