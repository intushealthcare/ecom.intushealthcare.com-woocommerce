<?php
/**
 * Sync Ploylang language metadata.
 *
 * @since 4.1.6
 */

defined( 'ABSPATH' ) || exit;

class WOO_MSTORE_INTEGRATION_WC_POLYLANG {
	/**
	 * $options_manager instance
	 *
	 * @var object
	 */
	private $options_manager = null;


	/**
	 * Initialize the collections used to maintain the actions and filters.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( is_multisite() ) {
			add_action( 'WOO_MSTORE_admin_product/slave_product_updated', array( $this, 'mu_set_language_details' ), PHP_INT_MAX, 1 );
		} else {
			// Add metadata to JSON payload.
			add_filter( 'WOO_MSTORE_SYNC/process_json/product', array( $this, 'add_custom_data_payload' ), PHP_INT_MAX, 3 );
			add_action( 'WOO_MSTORE_SYNC/sync_child/complete', array( $this, 'set_language_details' ), PHP_INT_MAX, 3 );
		}

		if ( ! is_multisite() ) {
			$this->options_manager = new WC_Multistore_Functions();
		}
	}

	/**
	 * Syncs the whitelisted metadata for multisite
	 *
	 * @since    1.0.0
	 * @return  array array of metada key and meta value.
	 */
	public function mu_set_language_details( $data ) {
		$blog_id = get_current_blog_id();

		// Switch to master to get master product data.
		switch_to_blog( $data['master_product_blog_id'] );

		if ( empty( $data['master_product']->get_id() ) ) {
			return;
		}

		$lang_code = pll_get_post_language( $data['master_product']->get_id() );

		// Switch the slave product blog again.
		switch_to_blog( $blog_id );
		pll_set_post_language( $data['slave_product']->get_id(), $lang_code );
	}


	/**
	 * Sync metadata on regular WordPress site.
	 *
	 * @since 4.1.6
	 *
	 * @param mixed $product
	 * @param mixed $wc_product
	 * @param mixed $product_id
	 * @return void
	 */
	public function add_custom_data_payload( $product, $wc_product, $product_id ) {

		$lang_code = pll_get_post_language( $product_id );

		$product['__woomstore_polylang_lang_code'] = $lang_code;

		return $product;
	}

	public function set_language_details( $child_product_id, $parent_product_id, $product ) {
		if ( ! empty( $product['__woomstore_polylang_lang_code'] ) ) {
			pll_set_post_language( $child_product_id, strip_tags( $product['__woomstore_polylang_lang_code'] ) );
		}
	}
}

new WOO_MSTORE_INTEGRATION_WC_POLYLANG();