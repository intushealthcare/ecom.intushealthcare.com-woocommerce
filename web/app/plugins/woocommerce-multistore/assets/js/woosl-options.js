;(function ( $, window, document, undefined ) {
	if ( $('.row-order-import-to').attr('data-option-visible') == 'yes' ) {
		$('.row-order-import-to').css('display', 'block !important');
	}

	if ( $('.row-global-image-master').attr('data-option-visible') == 'yes' ) {
		$('.row-order-import-to').css('display', 'block !important');
	}

	$( function() {
		jQuery( '.tips' ).tipTip( {
			'attribute': 'data-tip',
			'fadeIn': 50,
			'fadeOut': 50,
			'delay': 200
		} );

		var tabs = $( "#fields-control" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
		$( "#fields-control li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
		tabs.find( ".ui-tabs-nav" ).sortable({
			axis: "y",
			stop: function() {
				tabs.tabs( "refresh" );
			}
		});
	} );
})( $ms, window, document );

(function($) {
	$('.woomulti_option_with_warning').change(function() {
		if ( this.value == 'yes' ) {
			$('.woomulti_options_warning', $(this).parent().parent()).show();
		} else {
			$('.woomulti_options_warning', $(this).parent().parent()).hide();
		}
	});

	if ( $('.row-order-import-to').attr( 'data-option-visible') == 'yes' ) {
		$('.row-order-import-to').show();
	} else {
		$('.row-order-import-to').hide();
	}

	$( "select[name=enable-order-import]" ).on( 'change', function(){
		if ( this.value == 'yes' ) {
			$('.row-order-import-to').show();
		} else {
			$('.row-order-import-to').hide();
		}
	});

	if ( $('.row-global-image-master').attr( 'data-option-visible') == 'yes' ) {
		$('.row-global-image-master').show();
	} else {
		$('.row-global-image-master').hide();
	}

	$( "select[name=enable-global-image]" ).on( 'change', function(){
		if ( this.value == 'yes' ) {
			$('.row-global-image-master').show();
		} else {
			$('.row-global-image-master').hide();
		}
	});

})( jQuery );

jQuery(document).ready(function() {
	jQuery("#synchronize-by-default").change(function() {
		if ( this.value == 'yes' ) {
			jQuery('#inherit-by-default').removeAttr('disabled');
		} else {
			jQuery('#inherit-by-default').attr('disabled', true);
		}
	});
});

jQuery(document).ready(function() {
	jQuery("#synchronize-rest-by-default").change(function() {
		if ( this.value == 'yes' ) {
			jQuery('#inherit-rest-by-default').removeAttr('disabled');
		} else {
			jQuery('#inherit-rest-by-default').attr('disabled', true);
		}
	});
});
