<?php
/**
 * Network Order Refund handler.
 *
 * This handles the network order refund functionality in Woocommerce Multistore.
 *
 */

defined( 'ABSPATH' ) || exit;

/**
 * Class WC_Multistore_Network_Order_Refund
 */
class WC_Multistore_Network_Order_Refund {

	/**
	 * Run init hook.
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'init' ), 10, 0 );
	}

	/**
	 * WordPress Init
	 */
	public function init() {
		if ( is_multisite() && is_network_admin() ) {
			add_filter( 'bulk_actions-edit-woonet-network-orders',  array( $this, 'woonet_show_refund_button' ), 10, 1 );
			add_filter( 'woocommerce_admin_order_actions', array( $this, 'ms_woonet_show_refund_button_order_row' ), 10, 2 );
		} else {
			// add_action( 'WOO_MSTORE_ORDER/handle_bulk_actions-edit-shop_order', array( $this, 'woonet_custom_order_status_handler' ), 10, 2 );
			add_filter( 'WOO_MSTORE_ORDER/bulk_actions-edit-shop_order', array( $this, 'woonet_show_refund_button' ), 10, 1 );
			add_filter( 'WOO_MSTORE_ORDER/woocommerce_admin_order_actions', array( $this, 'woonet_show_refund_button_order_row' ), 10, 2 );
		}
	}

	/**
	 * Show refund button under bulk action.
	 */
	public function woonet_show_refund_button( $actions ) {
		if ( ! isset( $actions['refund'] ) ) {
			$actions['refund'] = 'Refund entire order';
		}

		return $actions;
	}

	/**
	 * Show refund button under bulk action.
	 */
	public function woonet_show_refund_button_order_row( $actions, $the_order ) {
		if ( $the_order['status'] == 'refunded' || $the_order['status'] == 'cancelled' ) {
			// Order already refunded. Do not show refund button.
			return $actions;
		}

		$actions['refund'] = array(
			'action' => 'refund',
			'name'   => 'Refund entire order',
			'url'    => add_query_arg(
				array(
					'action' => 'refund',
					'post'   => $the_order['uuid'] . '_' . $the_order['id'],
					'paged'  => ! empty( $_REQUEST['paged'] ) ? (int) $_REQUEST['paged'] : 1,
				),
				admin_url( 'admin.php?page=network-orders' )
			),
		);

		return $actions;
	}

	/**
	 * Show refund button under bulk action.
	 */
	public function ms_woonet_show_refund_button_order_row( $actions, $the_order ) {
		global $blog_id;

		if ( $the_order->get_status() == 'refunded' || $the_order->get_status() == 'cancelled' ) {
			// Order already refunded. Do not show refund button.
			return $actions;
		}

		$actions['refund'] = array(
			'action' => 'refund',
			'name'   => 'Refund entire order',
			'url'    => add_query_arg(
				array(
					'action' => 'refund',
					'post'   => $blog_id . '_' . $the_order->get_id(),
					'paged'  => ! empty( $_REQUEST['paged'] ) ? (int) $_REQUEST['paged'] : 1,
				),
				network_admin_url( 'admin.php?page=woonet-woocommerce' )
			),
		);

		return $actions;
	}

	public function woonet_custom_order_status_handler( $status, $post_id ) {
		/**
		 * We are only handling refund orders here.
		 */
		if ( $status != 'refund' ) {
			return;
		}

		$order = wc_get_order( (int) $post_id );

		if ( $order && $order->get_status() == 'refunded' ) {
			// Order already refunded.
			// @todo show error notice.
			return;
		}

		// Refund order.
		if ( $this->__refund_order( $order ) ) {
			echo json_encode( array(
				'status' => 'return',
			));
		};
	}

	/**
	 * Handle refunds for single site version.
	 * @param WC_ORDER
	 * @since 4.4.1 
	 * @todo Consolidate refunds functions into a single class.
	 */
	// public function refund_order( $order ) {
	// 	// Don't handle WC_ORDER_REFUND.
	// 	if ( ! is_a( $order, 'WC_Order' ) ) {
	// 		return;
	// 	}

	// 	$refund_amount = 0;
	// 	$line_items    = array();

	// 	if ( $items = $order->get_items() ) {
	// 		foreach ( $items as $item_id => $item ) {
	// 			$item_meta    = $order->get_item_meta( $item_id );
	// 			$product_data = wc_get_product( $item_meta['_product_id'][0] );

	// 			$item_ids[] = $item_id;
	// 			$tax_data   = $item_meta['_line_tax_data'];
	// 			$refund_tax = 0;

	// 			if ( is_array( $tax_data[0] ) ) {
	// 				$refund_tax = array_map( 'wc_format_decimal', $tax_data[0] );
	// 			}

	// 			$refund_amount = wc_format_decimal( $refund_amount ) + wc_format_decimal( $item_meta['_line_total'][0] );

	// 			$line_items[ $item_id ] = array(
	// 				'qty'          => $item_meta['_qty'][0],
	// 				'refund_total' => wc_format_decimal( $item_meta['_line_total'][0] ),
	// 				'refund_tax'   => $refund_tax,
	// 			);
	// 		}
	// 	}

	// 	$refund = wc_create_refund(
	// 		array(
	// 			'amount'         => $refund_amount,
	// 			'reason'         => 'Refund initiated from WooMultistore Network Order Interface.',
	// 			'order_id'       => $order->get_id(),
	// 			'line_items'     => $line_items,
	// 			'refund_payment' => false,
	// 			'restock_items'  => false,
	// 		)
	// 	);

	// 	return $refund;
	// }
}

$GLOBALS['WC_Multistore_Network_Order_Refund'] = new WC_Multistore_Network_Order_Refund();
