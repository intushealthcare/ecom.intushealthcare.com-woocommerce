<?php
/**
 * Settings handler.
 *
 * This handles settings functionality in Woocommerce Multistore.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class WC_Multistore_Settings
 */
class WC_Multistore_Settings {


		var $licence;

	function __construct() {

		$this->licence = new WC_Multistore_Licence();

		if ( isset( $_GET['page'] ) && $_GET['page'] == 'woo-ms-options' ) {
			add_action( 'init', array( $this, 'options_update' ), 1 );
		}

		add_action( 'network_admin_menu', array( $this, 'network_admin_menu' ) );
		
		if ( ! $this->licence->licence_key_verify() ) {
			add_action( 'admin_notices', array( $this, 'admin_no_key_notices' ) );
			add_action( 'network_admin_notices', array( $this, 'admin_no_key_notices' ) );
		}
	}

	function __destruct() {

	}

	function network_admin_menu() {
			$parent_slug = 'settings.php';

			$hookID = add_submenu_page( $parent_slug, 'WooMultistore ', 'WooMultistore ', 'manage_options', 'woo-ms-options', array( $this, 'options_interface' ) );

			add_action( 'load-' . $hookID, array( $this, 'load_dependencies' ) );
			add_action( 'load-' . $hookID, array( $this, 'admin_notices' ) );

			add_action( 'admin_print_styles-' . $hookID, array( $this, 'admin_print_styles' ) );
			add_action( 'admin_print_scripts-' . $hookID, array( $this, 'admin_print_scripts' ) );
	}

	public function options_interface() {
		if ( ! $this->licence->licence_key_verify() ) {
			$this->licence_form();

			return;
		}

		if ( $this->licence->licence_key_verify() ) {
			$this->licence_deactivate_form();
		}

		global $WOO_MSTORE;
		$options = $WOO_MSTORE->functions->get_options();

		?>
			<div class="wrap">
				<div id="icon-settings" class="icon32"></div>
				<h2 class='woonet-general-setitngs-header'><?php esc_html_e( 'General Settings', 'woonet' ); ?></h2>
				<div class='woonet-additional-settings'>  
					<?php if ( isset( $options['sync-custom-taxonomy'] ) && $options['sync-custom-taxonomy'] == 'yes' ) : ?>
						<a class='button button-primary' href="<?php echo esc_url( network_admin_url( 'admin.php?page=woonet-set-taxonomy' ) ); ?>" class='Shipping options'>Set Taxonomy</a>
					<?php endif; ?>
					<?php if ( isset( $options['sync-custom-metadata'] ) && $options['sync-custom-metadata'] == 'yes' ) : ?>
						<a class='button button-primary' href="<?php echo esc_url( network_admin_url( 'admin.php?page=woonet-set-taxonomy#sec-metadata' ) ); ?>" class='Shipping options'>Set Metadata</a>
					<?php endif; ?>
				</div>

				<form id="form_data" name="form" method="post">
					<br/>
					<table class="form-table">
						<tbody>

						<tr valign="top">
							<th scope="row">
								<select id="synchronize-by-default" name="synchronize-by-default">
									<option value="yes" <?php selected( 'yes', $options['synchronize-by-default'] ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
									<option value="no" <?php selected( 'no', $options['synchronize-by-default'] ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label><?php esc_html_e( 'Synchronize new products with all child sites by default', 'woonet' ); ?>
									<span class='tips'
										  data-tip='<?php esc_html_e( 'When a new product is published, it is automatically synchronized with all child sites. You can still control this at a product level.', 'woonet' ); ?>'><span
												class="dashicons dashicons-info"></span></span></label>
								<label class="checkbox"><input type="checkbox" id="inherit-by-default" name="inherit-by-default" value="yes" <?php checked( 'yes', $options['inherit-by-default'] ); disabled('no', $options['synchronize-by-default']); ?>><?php esc_html_e( 'Child product inherit Parent products changes', 'woonet' ); ?></label>
							</td>
						</tr>

						<tr valign="top">
							<th scope="row">
								<select id="synchronize-rest-by-default" name="synchronize-rest-by-default">
									<option value="no" <?php selected( 'no', $options['synchronize-rest-by-default'] ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
									<option value="yes" <?php selected( 'yes', $options['synchronize-rest-by-default'] ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label><?php esc_html_e( 'Synchronize new products added via API with all child sites by default', 'woonet' ); ?>
									<span class='tips'
										  data-tip='<?php esc_html_e( 'When a new product is published via API, it is automatically synchronized with all child sites. You can still control this at a product level.', 'woonet' ); ?>'><span
												class="dashicons dashicons-info"></span></span></label>
								<label class="checkbox"><input type="checkbox" id="inherit-rest-by-default" name="inherit-rest-by-default" value="yes" <?php checked( 'yes', $options['inherit-rest-by-default'] ); disabled('no', $options['synchronize-rest-by-default']); ?>><?php esc_html_e( 'Child product inherit Parent products changes', 'woonet' ); ?></label>
							</td>
						</tr>

						<tr valign="top">
							<th scope="row">
								<select name="synchronize-stock">
									<option value="yes" <?php selected( 'yes', $options['synchronize-stock'] ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
									<option value="no" <?php selected( 'no', $options['synchronize-stock'] ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label><?php esc_html_e( 'Always maintain stock synchronization for re-published products', 'woonet' ); ?>
									<span class='tips'
										  data-tip='<?php esc_html_e( 'Stock updates either manually or on checkout will also change other shops that have the product.', 'woonet' ); ?>'><span
												class="dashicons dashicons-info"></span></span></label>
							</td>
						</tr>
                        
                        
                        <?php
                        
                        if ( empty($options['sync-by-sku']) ) {
                            $options['sync-by-sku'] = 'no';
                        }
                        
                        ?>
                        <tr valign="top">
                            <th scope="row">
                                <select name="sync-by-sku">
                                    <option value="yes" <?php selected( $options['sync-by-sku'], 'yes' ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
                                    <option value="no" <?php selected( $options['sync-by-sku'], 'no' ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
                                </select>
                            </th>
                            <td>
                                <label>
                                    <?php esc_html_e( 'Sync by SKU', 'woonet' ); ?>
                                    <span class='tips'
                                          data-tip='<?php esc_html_e( 'Choose YES if you want to switch to sync by SKU. Note that all existing product sync will be replaced by SKU sync and that any products without SKU will not sync. This choice can\'t be undone after saving.', 'woonet' ); ?>'><span
                                                class="dashicons dashicons-info"></span></span></label>
                                <p>WARNING! Read the following <a href="https://woomultistore.com/sku-sync-documentation/" target="_blank">guide</a> on how to use this option before you change it.</p>
                                </label>
                            </td>
                        </tr>

						<tr valign="top">
							<th scope="row">
								<select name="synchronize-trash">
									<option value="yes" <?php selected( 'yes', $options['synchronize-trash'] ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
									<option value="no" <?php selected( 'no', $options['synchronize-trash'] ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label><?php esc_html_e( 'Trash the child product when the parent product is trashed', 'woonet' ); ?>
									<span class='tips'
										  data-tip='<?php esc_html_e( 'When parent product is trashed, trash the child products too.', 'woonet' ); ?>'><span
												class="dashicons dashicons-info"></span></span></label>
							</td>
						</tr>

						<tr valign="top">
							<th scope="row">
								<select name="sequential-order-numbers">
									<option value="yes" <?php selected( 'yes', $options['sequential-order-numbers'] ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
									<option value="no" <?php selected( 'no', $options['sequential-order-numbers'] ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label>
									<?php esc_html_e( 'Use sequential order numbers across the multisite environment', 'woonet' ); ?>
									<span class='tips'
										  data-tip='<?php esc_html_e( 'Order numbers will be created in sequence across the network for invoices and orders.', 'woonet' ); ?>'>
										  <span class="dashicons dashicons-info"></span>
									</span>
								</label>
                                <p>WARNING! If you later deactivate this, the order numbers will revert back to the default WooCommerce order numbers.</p>
							</td>
						</tr>

						<tr valign="top">
							<th scope="row">
								<select name="publish-capability">
									<option value="super-admin" <?php selected( 'super-admin', $options['publish-capability'] ); ?>><?php esc_html_e( 'Super Admin', 'woonet' ); ?></option>
									<option value="administrator" <?php selected( 'administrator', $options['publish-capability'] ); ?>><?php esc_html_e( 'Administrator', 'woonet' ); ?></option>
									<option value="shop_manager" <?php selected( 'shop_manager', $options['publish-capability'] ); ?>><?php esc_html_e( 'Shop Manager', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label><?php esc_html_e( 'Minimum user role to allow MultiStore Publish', 'woonet' ); ?>
								<span class='tips'
										  data-tip='<?php esc_html_e( 'Set the user role that has access to Multisite features.', 'woonet' ); ?>'>
										  <span class="dashicons dashicons-info"></span>
									</span>
								</label>
							</td>
						</tr>

						<tr valign="top">
							<th scope="row">
								<select name="network-user-info">
									<option value="yes" <?php selected( $options['network-user-info'], 'yes' ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
									<option value="no" <?php selected( $options['network-user-info'], 'no' ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label>
									<?php esc_html_e( 'Show customers orders from all stores in My Account', 'woonet' ); ?>
									<span class='tips'
										  data-tip='<?php esc_html_e( 'When enabled, customers will see orders from the whole network under My Account page.', 'woonet' ); ?>'>
										  <span class="dashicons dashicons-info"></span>
									</span>
								</label>
							</td>
						</tr>


						<?php 

						 if ( empty($options['sync-coupons']) ) {
						 	$options['sync-coupons'] = 'no';
						 }
						 
						?>
						<tr valign="top">
							<th scope="row">
								<select name="sync-coupons">
									<option value="yes" <?php selected( $options['sync-coupons'], 'yes' ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
									<option value="no" <?php selected( $options['sync-coupons'], 'no' ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label><?php esc_html_e( 'Sync coupons', 'woonet' ); ?>
									<span class='tips'
										data-tip='<?php esc_html_e( 'Sync coupon codes across the network.', 'woonet' ); ?>'><span
										class="dashicons dashicons-info"></span>
									</span>
								</label>
							</td>
						</tr>

						<?php 

						 if ( empty($options['sync-custom-taxonomy']) ) {
						 	$options['sync-custom-taxonomy'] = 'no';
						 }
						 
						?>
						<tr valign="top">
							<th scope="row">
								<select name="sync-custom-taxonomy">
									<option value="yes" <?php selected( $options['sync-custom-taxonomy'], 'yes' ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
									<option value="no" <?php selected( $options['sync-custom-taxonomy'], 'no' ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label><?php esc_html_e( 'Sync custom taxonomy', 'woonet' ); ?>
									<span class='tips'
										data-tip='<?php esc_html_e( 'If enabled you can click a new button "Set Taxonomy". From there you can select which custom taxonomy will be synced with the child sites.', 'woonet' ); ?>'>
									<span class="dashicons dashicons-info"></span></span></label>
							</td>
						</tr>

						<?php 

						 if ( empty($options['sync-custom-metadata']) ) {
						 	$options['sync-custom-metadata'] = 'no';
						 }
						 
						?>
						<tr valign="top">
							<th scope="row">
								<select name="sync-custom-metadata">
									<option value="yes" <?php selected( $options['sync-custom-metadata'], 'yes' ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
									<option value="no" <?php selected( $options['sync-custom-metadata'], 'no' ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label><?php esc_html_e( 'Sync custom metadata ', 'woonet' ); ?>
									<span class='tips'
										data-tip='<?php esc_html_e( 'If enabled you can click a new button "Set Metadata". From there you can select which custom metadata will be synced with the child sites.', 'woonet' ); ?>'>
									<span class="dashicons dashicons-info"></span></span></label>
							</td>
						</tr>

						<?php 
						 if ( empty($options['background-sync']) ) {
						 	$options['background-sync'] = 'no';
						 }
						?>
						<tr valign="top">
							<th scope="row">
								<select name="background-sync">
									<option value="yes" <?php selected( $options['background-sync'], 'yes' ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
									<option value="no" <?php selected( $options['background-sync'], 'no' ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label>
								<?php esc_html_e( 'Sync products in the background', 'woonet' ); ?>
								<span class='tips'
										data-tip='<?php esc_html_e( 'If enabled, sync will run in the background.', 'woonet' ); ?>'><span
										class="dashicons dashicons-info"></span>
									</span>
								</label>
							</td>
						</tr>

						<?php 
						 if ( empty($options['disable-ajax-sync']) ) {
						 	$options['disable-ajax-sync'] = 'no';
						 }
						?>
						<tr valign="top">
							<th scope="row">
								<select name="disable-ajax-sync">
									<option value="yes" <?php selected( $options['disable-ajax-sync'], 'yes' ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
									<option value="no" <?php selected( $options['disable-ajax-sync'], 'no' ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label>
								<?php esc_html_e( 'Disable AJAX sync', 'woonet' ); ?>
								<span class='tips'
										data-tip='<?php esc_html_e( 'If enabled, AJAX sync will be disabled and no sync dialogue will be shown.', 'woonet' ); ?>'><span
										class="dashicons dashicons-info"></span>
									</span>
								</label>
							</td>
						</tr>

						<?php

						if ( empty( $options['enable-global-image'] ) ) {
						    $options['enable-global-image'] = 'no';
                        }

						?>
                        <tr valign="top">
							<th scope="row">
								<select name="enable-global-image">
									<option value="yes" <?php selected( $options['enable-global-image'], 'yes' ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
									<option value="no" <?php selected( $options['enable-global-image'], 'no' ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label><?php esc_html_e( 'Enable global image', 'woonet' ); ?>
						 			<span class='tips'
											data-tip='<?php esc_html_e( 'When enabled, product images and product category images will not be uploaded on child sites. Child products and product categories will use the images uploaded on master site', 'woonet' ); ?>'><span
											class="dashicons dashicons-info"></span>
									</span>
								</label>
							</td>
						</tr>

						<?php
						// get the stores.
						$active_stores = WC_Multistore_Functions::get_active_woocommerce_blog_ids();
						$store_options = '<option value="0"> None </option>';

						if ( ! empty( $active_stores ) ) {
							foreach( $active_stores as $store_id ) {
								$store = get_blog_details ( $store_id );
								$store_options .= "<option " . selected( $store_id, $options['global-image-master'], false ) . " value='{$store->blog_id}'>{$store->blogname}</option>";
							}
						}
						?>
                        <tr valign="top" class='row-global-image-master' data-option-visible='<?php echo $options['enable-global-image']; ?>'>
                            <th scope="row">
                                <select class='global-image-master' name="global-image-master">
									<?php echo $store_options; ?>
                                </select>
                            </th>
                            <td>
                                <label>
									<?php esc_html_e( 'Global image master site', 'woonet' ); ?>
                                    <span class='tips'
                                          data-tip='<?php esc_html_e( 'Select the master site for global images', 'woonet' ); ?>'><span
                                                class="dashicons dashicons-info"></span>
									</span>
                                    <p style='font-size: x-small; font-weight: bold; font-style: italic; text-decoration: underline;'>Must be the store you sync products from.</p>
                                </label>
                            </td>
                        </tr>

						<?php 
						 if ( empty($options['enable-order-import']) ) {
						 	$options['enable-order-import'] = 'no';
						 }
						?>
						<tr valign="top">
							<th scope="row">
								<select name="enable-order-import">
									<option value="yes" <?php selected( $options['enable-order-import'], 'yes' ); ?>><?php esc_html_e( 'Yes', 'woonet' ); ?></option>
									<option value="no" <?php selected( $options['enable-order-import'], 'no' ); ?>><?php esc_html_e( 'No', 'woonet' ); ?></option>
								</select>
							</th>
							<td>
								<label>
								<?php esc_html_e( 'Enable order import', 'woonet' ); ?>
								<span class='tips'
										data-tip='<?php esc_html_e( 'If enabled, orders from the child sites will be imported to the main site.', 'woonet' ); ?>'><span
										class="dashicons dashicons-info"></span>
									</span>
								</label>
							</td>
						</tr>

						<?php 
						 // get the stores.
						 $active_stores = WC_Multistore_Functions::get_active_woocommerce_blog_ids();
						 $store_options = '<option value="0"> None </option>';

						 if ( ! empty( $active_stores ) ) {
							foreach( $active_stores as $store_id ) {
								$store = get_blog_details ( $store_id );
								$store_options .= "<option " . selected( $store_id, $options['order-import-to'], false ) . " value='{$store->blog_id}'>{$store->blogname}</option>";
							}
						 }
						?>
						<tr valign="top" class='row-order-import-to' data-option-visible='<?php echo $options['enable-order-import']; ?>'>
							<th scope="row">
								<select class='order-import-to' name="order-import-to">
									<?php echo $store_options; ?>
								</select>
							</th>
							<td>
								<label>
								<?php esc_html_e( 'Import orders to', 'woonet' ); ?>
								<span class='tips'
										data-tip='<?php esc_html_e( 'Select the store where you want to import the orders.', 'woonet' ); ?>'><span
										class="dashicons dashicons-info"></span>
									</span>
									<p style='font-size: x-small; font-weight: bold; font-style: italic; text-decoration: underline;'>Must be the store you sync products from.</p>
								</label>
							</td>
						</tr>
						</tbody>
					</table>

				<?php
					$blog_ids = $WOO_MSTORE->functions->get_active_woocommerce_blog_ids();
				if ( isset( $options['blog_tab_order'] ) ) {
					$blog_tab_order = array_filter( array_map( 'intval', $options['blog_tab_order'] ) );
					$blog_ids       = array_merge(
						array_intersect( $blog_tab_order, $blog_ids ),
						array_diff( $blog_ids, $blog_tab_order )
					);
				}
				if ( $blog_ids ) {
					echo '<h4>' . __( 'Child product inherit Parent products changes - Field controls', 'woonet' ) . '</h4>';
					echo '<div id="fields-control">';

						echo '<ul>';
					foreach ( $blog_ids as $index => $blog_id ) {
						switch_to_blog( $blog_id );

						$blog_name = get_bloginfo( 'name' );

						$blog_ids[ $index ] = array( $blog_id, $blog_name );

						printf(
							'<li><a href="#tabs-%d">%s</a><input type="hidden" name="blog_tab_order[]" value="%d" /></li>',
							$blog_id,
							$blog_name,
							$blog_id
						);

						restore_current_blog();
					}
						echo '</ul>';

					foreach ( $blog_ids as $blog_data ) {
						list( $blog_id, $blog_name ) = $blog_data;

						printf( '<div id="tabs-%d"><h3>%s options</h3>', $blog_id, $blog_name );

							echo '<table class="form-table"><tbody>';

								$option_name = 'child_inherit_changes_fields_control__status';
								
								echo '<tr valign="top"><th scope="row">';
									printf(
										'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
										"{$option_name}[{$blog_id}]",
										selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
										__( 'Yes', 'woonet' ),
										selected( $options[ $option_name ][ $blog_id ], 'no', false ),
										__( 'No', 'woonet' )
									);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit product status changes', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

									$option_name = 'child_inherit_changes_fields_control__featured';
								
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
										echo '</th><td>';
											printf(
												'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
												__( 'Child product inherit featured status changes', 'woonet' ),
												__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
											);
										echo '</td></tr>';
                        
                                $option_name = 'child_inherit_changes_fields_control__catalogue_visibility';
                                
                                echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Child product inherit catalogue visibility changes', 'woonet' ),
                                        __( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
                                    );
                                echo '</td></tr>';

								$option_name = 'child_inherit_changes_fields_control__title';

								echo '<tr valign="top"><th scope="row">';
									printf(
										'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
										"{$option_name}[{$blog_id}]",
										selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
										__( 'Yes', 'woonet' ),
										selected( $options[ $option_name ][ $blog_id ], 'no', false ),
										__( 'No', 'woonet' )
									);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit title changes', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

									$option_name = 'child_inherit_changes_fields_control__description';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit description changes', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

									$option_name = 'child_inherit_changes_fields_control__short_description';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit short description changes', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

									$option_name = 'child_inherit_changes_fields_control__price';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit regular price changes', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

									$option_name = 'child_inherit_changes_fields_control__sale_price';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit sale price changes', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';



									$option_name = 'child_inherit_changes_fields_control__product_tag';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit product tag changes', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

									$option_name = 'child_inherit_changes_fields_control__attributes';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit product attributes', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__attribute_name';
                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Child product inherit product attribute name', 'woonet' ),
                                        __( 'This works in conjunction with <b>Child product inherit product attributes</b> being active.', 'woonet' )
                                    );
                                    echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__default_variations';
                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Child product inherit Default Form Values (default attributes)', 'woonet' ),
                                        __( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
                                    );
                                    echo '</td></tr>';

									$option_name = 'child_inherit_changes_fields_control__sku';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit product SKU', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

									$option_name = 'child_inherit_changes_fields_control__product_image';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit product image', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

									$option_name = 'child_inherit_changes_fields_control__product_gallery';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit product gallery', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

									$option_name = 'child_inherit_changes_fields_control__reviews';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit product reviews', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

									$option_name = 'child_inherit_changes_fields_control__slug';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit product URL (slug)', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

									$option_name = 'child_inherit_changes_fields_control__purchase_note';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit product purchase note', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

									/**
									* Shipping Class
									**/
									$option_name = 'child_inherit_changes_fields_control__shipping_class';

									if ( empty( $options[ $option_name ][ $blog_id ]) )
									{
										$options[ $option_name ][ $blog_id ] = 'yes';
									} 

									if ( !empty( $options[ $option_name ][ $blog_id ]) 
										 &&  $options[ $option_name ][ $blog_id ] == 'yes')
									{
										$womulti_show_warning = "style='display:block;'";
									} else {
										$womulti_show_warning = "style='display:none;'";
									}

									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select class="woomulti_option_with_warning"  name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit shipping class', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';
									/** Shipping class end **/


									/**
									* Sync upsell products
									**/
									$option_name = 'child_inherit_changes_fields_control__upsell';

									if ( empty( $options[ $option_name ][ $blog_id ]) )
									{
										$options[ $option_name ][ $blog_id ] = 'no';
									} 

									if ( !empty( $options[ $option_name ][ $blog_id ]) 
										 &&  $options[ $option_name ][ $blog_id ] == 'yes')
									{
										$womulti_show_warning = "style='display:block;'";
									} else {
										$womulti_show_warning = "style='display:none;'";
									}

									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select class="woomulti_option_with_warning"  name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit Upsells', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
										echo "<p " . $womulti_show_warning .  " class='woomulti_options_warning'> An upsell product needs to be synced with the child store before it can be synced as upsell for a child store product. </p>";
									echo '</td></tr>';
									/** Sync Upsell end **/

									/**
									* Sync cross-sells products
									**/
									$option_name = 'child_inherit_changes_fields_control__cross_sells';

									if ( empty( $options[ $option_name ][ $blog_id ]) )
									{
										$options[ $option_name ][ $blog_id ] = 'no';
									}

									if ( !empty( $options[ $option_name ][ $blog_id ]) 
										 &&  $options[ $option_name ][ $blog_id ] == 'yes')
									{
										$womulti_show_warning = "style='display:block;'";
									} else {
										$womulti_show_warning = "style='display:none;'";
									}

									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select class="woomulti_option_with_warning" name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit Cross-sells', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
										echo "<p " . $womulti_show_warning . " class='woomulti_options_warning'> A cross-sell products needs to be synced with the child store before it can be synced as cross-sell for a child store product. </p>";
									echo '</td></tr>';
									/** Sync Cross-sells end **/

									$option_name = 'child_inherit_changes_fields_control__allow_backorders';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Child product inherit product allow backorders.', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';

                                    /**
                                     * Variation settings section.
                                     */
                                    echo '<tr valign="top"><th scope="row">';
                                    echo "<h2 style='font-size:1em;'> Variation Settings </h2>";
                                    echo '</th><td>';
                                    echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__variations';
                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Child product inherit product variations', 'woonet' ),
                                        __( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
                                    );
                                    echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__variations_data';
                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Child product inherit product variations data', 'woonet' ),
                                        __( 'This works in conjunction with <b>Child product inherit parent variations</b> being active.', 'woonet' )
                                    );
                                    echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__variations_status';
                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Child product inherit product variations status', 'woonet' ),
                                        __( 'This works in conjunction with <b>Child product inherit parent variations</b> being active.', 'woonet' )
                                    );
                                    echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__variations_stock';
                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Child product inherit product variations stock', 'woonet' ),
                                        __( 'This works in conjunction with <b>Child product inherit parent variations</b> being active. This applies to manage stock, stock status, stock quantity and backorders', 'woonet' )
                                    );
                                    echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__variations_sku';
                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Child product inherit product variations sku', 'woonet' ),
                                        __( 'This works in conjunction with <b>Child product inherit parent variations</b> being active.', 'woonet' )
                                    );
                                    echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__variations_price';
                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Child product inherit product variation regular price', 'woonet' ),
                                        __( 'This works in conjunction with <b>Child product inherit parent variations</b> being active.', 'woonet' )
                                    );
                                    echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__variations_sale_price';
                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Child product inherit product variation sale price', 'woonet' ),
                                        __( 'This works in conjunction with <b>Child product inherit parent variations</b> being active.', 'woonet' )
                                    );
                                    echo '</td></tr>';

                                    /**
                                     * Category settings section.
                                     */
                                    echo '<tr valign="top"><th scope="row">';
                                    echo "<h2 style='font-size:1em;'> Category Settings </h2>";
                                    echo '</th><td>';
                                    echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__product_cat';
                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Child product inherit product category changes', 'woonet' ),
                                        __( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
                                    );
                                    echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__category_changes';
                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Child product inherit category image and description changes', 'woonet' ),
                                        __( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
                                    );
                                    echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__category_meta';
                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Child product inherit category meta data changes', 'woonet' ),
                                        __( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
                                    );
                                    echo '</td></tr>';

                                    /**
                                     * REST API settings section.
                                     */
                                    echo '<tr valign="top"><th scope="row">';
                                    echo "<h2 style='font-size:1em;'> REST API Settings </h2>";
                                    echo '</th><td>';
                                    echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__synchronize_rest_by_default';

                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Synchronize new products added via API', 'woonet' ),
                                        __( 'This works in conjunction with <b>Synchronize new products added via API with all child sites by default</b> being active.', 'woonet' )
                                    );
                                    echo '</td></tr>';

                                    /**
                                     * Import Order settings section.
                                     */
                                    echo '<tr valign="top"><th scope="row">';
                                    echo "<h2 style='font-size:1em;'> Import Order Settings </h2>";
                                    echo '</th><td>';
                                    echo '</td></tr>';

                                    $option_name = 'child_inherit_changes_fields_control__import_order';

                                    echo '<tr valign="top"><th scope="row">';
                                    printf(
                                        '<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
                                        "{$option_name}[{$blog_id}]",
                                        selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
                                        __( 'Yes', 'woonet' ),
                                        selected( $options[ $option_name ][ $blog_id ], 'no', false ),
                                        __( 'No', 'woonet' )
                                    );
                                    echo '</th><td>';
                                    printf(
                                        '<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
                                        __( 'Import Order', 'woonet' ),
                                        __( 'This works in conjunction with <b>Enable Order Import</b> being active.', 'woonet' )
                                    );
                                    echo '</td></tr>';

									/**
									 * Override default settings section.
									 */
									echo '<tr valign="top"><th scope="row">';
									echo "<h2 style='font-size:1em;'> Override General Settings </h2>";
									echo '</th><td>';
									echo '</td></tr>';

									/**
									 * Override general settings
									 */
									/** stock sync */
									$option_name = 'override__synchronize-stock';
									echo '<tr valign="top"><th scope="row">';
										printf(
											'<select name="%s"><option value="yes" %s>%s</option><option value="no" %s>%s</option></select>',
											"{$option_name}[{$blog_id}]",
											selected( $options[ $option_name ][ $blog_id ], 'yes', false ),
											__( 'Yes', 'woonet' ),
											selected( $options[ $option_name ][ $blog_id ], 'no', false ),
											__( 'No', 'woonet' )
										);
									echo '</th><td>';
										printf(
											'<label>%s<span class="tips" data-tip="%s"><span class="dashicons dashicons-info"></span></span></label>',
											__( 'Disable stock sync.', 'woonet' ),
											__( 'This works in conjunction with <b>Child product inherit Parent products changes</b> being active on individual product page.', 'woonet' )
										);
									echo '</td></tr>';
									/** end override stock sync */

									do_action( 'woo_mstore/options/options_output/child_inherit_changes_fields_control', $blog_id );

								echo '</tbody></table>';

							echo '</div>';
					}

							echo '</div>';
				}
				?>

					<?php do_action( 'woo_mstore/options/options_output' ); ?>

					<p class="submit">
						<input type="submit" name="Submit" class="button-primary"
							   value="<?php esc_html_e( 'Save Settings', 'woonet' ); ?>">
					</p>

					<?php wp_nonce_field( 'mstore_form_submit', 'mstore_form_nonce' ); ?>
					<input type="hidden" name="mstore_form_submit" value="true"/>

				</form>
			</div>
			<?php
	}

	function options_update() {

		if ( isset( $_POST['mstore_licence_form_submit'] ) ) {
				$this->licence_form_submit();
				return;
		}

		if ( isset( $_POST['mstore_form_submit'] ) ) {
				// check nonce
			if ( ! wp_verify_nonce( $_POST['mstore_form_nonce'], 'mstore_form_submit' ) ) {
				return;
			}

				global $WOO_MSTORE;
				$options = $WOO_MSTORE->functions->get_options();

				global $mstore_form_submit_messages;

				// Global options.
				$options['synchronize-by-default'] 	     = isset( $_POST['synchronize-by-default'] ) && in_array( $_POST['synchronize-by-default'], array( 'yes', 'no' ) ) ? $_POST['synchronize-by-default'] : 'no';
				$options['synchronize-rest-by-default']  = isset( $_POST['synchronize-rest-by-default'] ) && in_array( $_POST['synchronize-rest-by-default'], array( 'yes', 'no' ) ) ? $_POST['synchronize-rest-by-default'] : 'no';
				$options['inherit-by-default']  	     = isset( $_POST['inherit-by-default'] ) ? $_POST['inherit-by-default'] : 'no';
				$options['inherit-rest-by-default']  	 = isset( $_POST['inherit-rest-by-default'] ) ? $_POST['inherit-rest-by-default'] : 'no';
				$options['synchronize-stock'] 		     = $_POST['synchronize-stock'];
				$options['synchronize-trash'] 		     = isset( $_POST['synchronize-trash'] ) && in_array( $_POST['synchronize-trash'], array( 'yes', 'no' ) ) ? $_POST['synchronize-trash'] : 'no';
				$options['sequential-order-numbers']     = $_POST['sequential-order-numbers'];
				$options['publish-capability']    	     = $_POST['publish-capability'];
				$options['network-user-info']     	     = isset( $_POST['network-user-info'] ) && in_array( $_POST['network-user-info'], array( 'yes', 'no' ) ) ? $_POST['network-user-info'] : 'yes';
				$options['sync-coupons']          	     = isset( $_POST['sync-coupons'] ) && in_array( $_POST['sync-coupons'], array( 'yes', 'no' ) ) ? $_POST['sync-coupons'] : 'yes';
				$options['sync-by-sku']           	     = isset( $_POST['sync-by-sku'] ) && in_array( $_POST['sync-by-sku'], array( 'yes', 'no' ) ) ? $_POST['sync-by-sku'] : 'no';
				$options['sync-custom-taxonomy']  	     = isset( $_POST['sync-custom-taxonomy'] ) && in_array( $_POST['sync-custom-taxonomy'], array( 'yes', 'no' ) ) ? $_POST['sync-custom-taxonomy'] : 'yes';
				$options['sync-custom-metadata']  	     = isset( $_POST['sync-custom-metadata'] ) && in_array( $_POST['sync-custom-metadata'], array( 'yes', 'no' ) ) ? $_POST['sync-custom-metadata'] : 'yes';
				$options['background-sync']  	         = isset( $_POST['background-sync'] ) && in_array( $_POST['background-sync'], array( 'yes', 'no' ) ) ? $_POST['background-sync'] : 'no';
				$options['disable-ajax-sync']  	         = isset( $_POST['disable-ajax-sync'] ) && in_array( $_POST['disable-ajax-sync'], array( 'yes', 'no' ) ) ? $_POST['disable-ajax-sync'] : 'no';
			    $options['enable-global-image']          = isset( $_POST['enable-global-image'] ) && in_array( $_POST['enable-global-image'], array( 'yes', 'no' ) ) ? $_POST['enable-global-image'] : 'no';
			    $options['global-image-master']          = isset( $_POST['global-image-master'] ) ? $_POST['global-image-master'] : 0;
			    $options['enable-order-import']  	     = isset( $_POST['enable-order-import'] ) && in_array( $_POST['enable-order-import'], array( 'yes', 'no' ) ) ? $_POST['enable-order-import'] : 'no';
				$options['order-import-to']  	 	     = isset( $_POST['order-import-to'] ) ? $_POST['order-import-to'] : 0;

				// Site specific options.
				$options['child_inherit_changes_fields_control__status']            = $_POST['child_inherit_changes_fields_control__status'];
				$options['child_inherit_changes_fields_control__featured'] 			= $_POST['child_inherit_changes_fields_control__featured'];
				$options['child_inherit_changes_fields_control__title']             = $_POST['child_inherit_changes_fields_control__title'];
				$options['child_inherit_changes_fields_control__description']       = $_POST['child_inherit_changes_fields_control__description'];
				$options['child_inherit_changes_fields_control__short_description'] = $_POST['child_inherit_changes_fields_control__short_description'];
				$options['child_inherit_changes_fields_control__price']             = $_POST['child_inherit_changes_fields_control__price'];
				$options['child_inherit_changes_fields_control__sale_price']        = $_POST['child_inherit_changes_fields_control__sale_price'];
				$options['child_inherit_changes_fields_control__product_tag']       = $_POST['child_inherit_changes_fields_control__product_tag'];
				$options['child_inherit_changes_fields_control__attributes']        = $_POST['child_inherit_changes_fields_control__attributes'];
				$options['child_inherit_changes_fields_control__attribute_name']    = $_POST['child_inherit_changes_fields_control__attribute_name'];
                $options['child_inherit_changes_fields_control__default_variations'] = $_POST['child_inherit_changes_fields_control__default_variations'];

                $options['child_inherit_changes_fields_control__sku']        		 = $_POST['child_inherit_changes_fields_control__sku'];
				$options['child_inherit_changes_fields_control__allow_backorders']   = $_POST['child_inherit_changes_fields_control__allow_backorders'];

				$options['child_inherit_changes_fields_control__reviews']            = $_POST['child_inherit_changes_fields_control__reviews'];
				$options['child_inherit_changes_fields_control__slug']               = $_POST['child_inherit_changes_fields_control__slug'];
				$options['child_inherit_changes_fields_control__purchase_note']      = $_POST['child_inherit_changes_fields_control__purchase_note'];
				$options['child_inherit_changes_fields_control__upsell']     		 = $_POST['child_inherit_changes_fields_control__upsell'];
				$options['child_inherit_changes_fields_control__cross_sells'] 		 = $_POST['child_inherit_changes_fields_control__cross_sells'];
				$options['child_inherit_changes_fields_control__product_image'] 	 = $_POST['child_inherit_changes_fields_control__product_image'];
				$options['child_inherit_changes_fields_control__product_gallery'] 	 = $_POST['child_inherit_changes_fields_control__product_gallery'];
				$options['child_inherit_changes_fields_control__shipping_class'] 	 = $_POST['child_inherit_changes_fields_control__shipping_class'];
				$options['child_inherit_changes_fields_control__catalogue_visibility'] = $_POST['child_inherit_changes_fields_control__catalogue_visibility'];

			    // Variation settings
                $options['child_inherit_changes_fields_control__variations']                = $_POST['child_inherit_changes_fields_control__variations'];
                $options['child_inherit_changes_fields_control__variations_data']           = $_POST['child_inherit_changes_fields_control__variations_data'];
                $options['child_inherit_changes_fields_control__variations_status']         = $_POST['child_inherit_changes_fields_control__variations_status'];
                $options['child_inherit_changes_fields_control__variations_stock']          = $_POST['child_inherit_changes_fields_control__variations_stock'];
                $options['child_inherit_changes_fields_control__variations_sku']            = $_POST['child_inherit_changes_fields_control__variations_sku'];
                $options['child_inherit_changes_fields_control__variations_price']          = $_POST['child_inherit_changes_fields_control__variations_price'];
                $options['child_inherit_changes_fields_control__variations_sale_price']     = $_POST['child_inherit_changes_fields_control__variations_sale_price'];

			    // Category settings
                $options['child_inherit_changes_fields_control__product_cat']        = $_POST['child_inherit_changes_fields_control__product_cat'];
                $options['child_inherit_changes_fields_control__category_changes']   = $_POST['child_inherit_changes_fields_control__category_changes'];
                $options['child_inherit_changes_fields_control__category_meta']      = $_POST['child_inherit_changes_fields_control__category_meta'];

			    // REST API settings
                $options['child_inherit_changes_fields_control__synchronize_rest_by_default']            = $_POST['child_inherit_changes_fields_control__synchronize_rest_by_default'];// REST API settings

                // Import order settings
                $options['child_inherit_changes_fields_control__import_order']            = $_POST['child_inherit_changes_fields_control__import_order'];

                // Global setting overrides.
				$options['override__synchronize-stock'] = $_POST['override__synchronize-stock'];

				$options['blog_tab_order'] = $_POST['blog_tab_order'];

				$options = apply_filters( 'woo_mstore/options/options_save', $options );

				foreach( $options as $key => $value ) {
					if ( is_array( $value ) ) {
						$options[ $key ] = array_map( 'strip_tags', $value );
					} else {
						$options[ $key ] = strip_tags( $value );
					}
				}

				$WOO_MSTORE->functions->update_options( $options );

				$mstore_form_submit_messages[] = __( 'Settings Saved', 'woonet' );

				// post processing
			if ( $options['sequential-order-numbers'] == 'yes' ) {
					/**
					 * Do not update if sequential order number is ono-zero
					 */
					include_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-sequential-order-number.php';

					if ( WC_Multistore_Sequential_Order_Number::get_current_sequential_order_number() <= 0 ) {
						WC_Multistore_Sequential_Order_Number::network_update_order_numbers();
					} 
			}
		}

	}

	function load_dependencies() {

	}

	function admin_notices() {
			global $mstore_form_submit_messages;

		if ( $mstore_form_submit_messages == '' ) {
			return;
		}

			$messages = $mstore_form_submit_messages;

		if ( count( $messages ) > 0 ) {
				echo "<div id='notice' class='updated fade'><p>" . implode( '</p><p>', $messages ) . '</p></div>';
		}

	}

	public function admin_print_styles() {
		wp_enqueue_style( 'jquery-ui-ms', WOO_MSTORE_URL . '/assets/css/jquery-ui.css' );

		wp_enqueue_style( 'woosl-options', WOO_MSTORE_URL . '/assets/css/woosl-options.css' );
	}

	public function admin_print_scripts() {
		$WC_url = plugins_url() . '/woocommerce';
		wp_enqueue_script( 'jquery-tiptip', $WC_url . '/assets/js/jquery-tiptip/jquery.tipTip.js' );

		wp_enqueue_script(
			'jquery-ms',
			WOO_MSTORE_URL . '/assets/js/jquery-3.3.1.min.js',
			array()
		);
		wp_enqueue_script(
			'jquery-ui-ms',
			WOO_MSTORE_URL . '/assets/js/jquery-ui.min.js',
			array( 'jquery-ms' )
		);
		wp_add_inline_script( 'jquery-ui-ms', 'var $ms = $.noConflict(true);' );

		wp_enqueue_script(
			'woosl-options',
			WOO_MSTORE_URL . '/assets/js/woosl-options.js',
			array( 'jquery-ms', 'jquery-ui-ms', 'jquery-tiptip' ),
			WOO_MSTORE_VERSION,
			true
		);
	}

	function admin_no_key_notices() {
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

			$screen = get_current_screen();

		if ( is_multisite() ) {
			if ( isset( $screen->id ) && $screen->id == 'settings_page_woo-ms-options-network' ) {
				return;
			}
			?>
							<div class="updated fade"><p><?php esc_html_e( 'WooMultistore plugin is inactive, please enter your', 'woonet' ); ?> <a href="<?php echo network_admin_url(); ?>settings.php?page=woo-ms-options"><?php esc_html_e( 'Licence Key', 'woonet' ); ?></a></p></div>
						<?php
		}
	}

	function licence_form_submit() {
		global $mstore_form_submit_messages;

		// check for de-activation
		if ( isset( $_POST['mstore_licence_form_submit'] ) && isset( $_POST['mstore_licence_deactivate'] ) && wp_verify_nonce( $_POST['mstore_license_nonce'], 'mstore_license' ) ) {
				global $mstore_form_submit_messages;

				$response = WOO_MULTISTORE()->license_manager->deactivate();

				if ( !empty( $response['msg'] ) ) {
					$mstore_form_submit_messages[] = $response['msg'];
				}

				wp_redirect( network_admin_url( 'settings.php?page=woo-ms-options', 'relative' ) );
				die();
		}

		if ( isset( $_POST['mstore_licence_form_submit'] ) && wp_verify_nonce( $_POST['mstore_license_nonce'], 'mstore_license' ) ) {

			$license_key = isset( $_POST['license_key'] ) ? sanitize_key( trim( $_POST['license_key'] ) ) : '';

			if ( $license_key == '' ) {
				$mstore_form_submit_messages[] = __( "Licence Key can't be empty", 'woonet' );
				return;
			}

			$response = WOO_MULTISTORE()->license_manager->activate( $_POST['license_key'] );

			if ( !empty( $response['msg'] ) ) {
				$mstore_form_submit_messages[] = $response['msg'];
			}

			// redirect
			wp_redirect( network_admin_url( 'settings.php?page=woo-ms-options', 'relative' ) );
			die();
		}

	}

	function licence_form() {
		?>
						<div class="wrap"> 
							<div id="icon-settings" class="icon32"></div>
							<h2><?php esc_html_e( 'WooMultistore', 'woonet' ); ?><br />&nbsp;</h2>
							
							
							<form id="form_data" name="form" method="post">
								<div class="postbox">
									
								<?php wp_nonce_field( 'mstore_license', 'mstore_license_nonce' ); ?>
										<input type="hidden" name="mstore_licence_form_submit" value="true" />
										   
										

										 <div class="section section-text ">
											<h4 class="heading"><?php esc_html_e( 'License Key', 'woonet' ); ?></h4>
											<div class="option">
												<div class="controls">
													<input type="text" value="" name="license_key" class="text-input">
												</div>
												<div class="explain"><?php esc_html_e( 'Enter the License Key you got when bought this product. If you lost the key, you can always retrieve it from', 'woonet' ); ?> <a href="https://woomultistore.com/premium-plugins/my-account/" target="_blank"><?php esc_html_e( 'My Account', 'woonet' ); ?></a><br />
										<?php esc_html_e( 'More keys can be generate from', 'woonet' ); ?> <a href="https://woomultistore.com/premium-plugins/my-account/" target="_blank"><?php esc_html_e( 'My Account', 'woonet' ); ?></a>
												</div>
											</div> 
										</div>

									
								</div>
								
								<p class="submit">
									<input type="submit" name="Submit" class="button-primary" value="<?php esc_html_e( 'Save', 'woonet' ); ?>">
								</p>
							</form> 
						</div> 
				<?php

	}

	function licence_deactivate_form() {
			$license_data = get_site_option( 'mstore_license' );

		if ( is_multisite() ) {
			?>
								<div class="wrap"> 
									<div id="icon-settings" class="icon32"></div>
				<?php
		}

		?>
						<div id="form_data">
						<h2 class="subtitle"><?php esc_html_e( 'Software License', 'woonet' ); ?></h2>
						<div class="postbox">
							<form id="form_data" name="form" method="post">    
						<?php wp_nonce_field( 'mstore_license', 'mstore_license_nonce' ); ?>
								<input type="hidden" name="mstore_licence_form_submit" value="true" />
								<input type="hidden" name="mstore_licence_deactivate" value="true" />

								 <div class="section section-text ">
									<h4 class="heading"><?php esc_html_e( 'License Key', 'woonet' ); ?></h4>
									<div class="option">
										<div class="controls">
									<?php
									if ( $this->licence->is_local_instance() ) {
										?>
												<p>Local instance, no key applied.</p>
										<?php
									} else {
										?>
											<p><b><?php echo esc_html_e( substr( $license_data['key'], 0, 20 ) ); ?>-xxxxxxxx-xxxxxxxx</b> &nbsp;&nbsp;&nbsp;<a class="button-secondary" title="Deactivate" href="javascript: void(0)" onclick="jQuery(this).closest('form').submit();">Deactivate</a></p>
											<?php } ?>
										</div>
										<div class="explain"><?php esc_html_e( 'You can generate more keys from', 'woonet' ); ?> <a href="https://woomultistore.com/premium-plugins/my-account/" target="_blank">My Account</a>
										</div>
									</div> 
								</div>
							 </form>
						</div>
						</div> 
					<?php

					if ( is_multisite() ) {
						?>
								</div>
							<?php
					}
	}

	function licence_multisite_require_nottice() {
		?>
						<div class="wrap"> 
							<div id="icon-settings" class="icon32"></div>

							<h2 class="subtitle"><?php esc_html_e( 'Software License', 'woonet' ); ?></h2>
							<div id="form_data">
								<div class="postbox">
									<div class="section section-text ">
										<h4 class="heading"><?php esc_html_e( 'License Key Required', 'woonet' ); ?>!</h4>
										<div class="option">
											<div class="explain"><?php esc_html_e( 'Enter the License Key you got when bought this product. If you lost the key, you can always retrieve it from', 'woonet' ); ?> <a href="https://woomultistore.com/premium-plugins/my-account/" target="_blank"><?php esc_html_e( 'My Account', 'woonet' ); ?></a><br />
									<?php esc_html_e( 'More keys can be generate from', 'woonet' ); ?> <a href="https://woomultistore.com/premium-plugins/my-account/" target="_blank"><?php esc_html_e( 'My Account', 'woonet' ); ?></a>
											</div>
										</div> 
									</div>
								</div>
							</div>
						</div> 
				<?php

	}


}



?>
