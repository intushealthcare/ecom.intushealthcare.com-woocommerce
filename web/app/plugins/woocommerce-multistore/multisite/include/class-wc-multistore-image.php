<?php

defined( 'ABSPATH' ) || exit;

/**
 * Class WC Multistore Image
 **/
class WC_Multistore_Image{

	/**
	 * @var
	 */
	private $settings;

	/**
	 * @var
	 */
	private $master_site;

	/**
	 * Class instance.
	 *
	 * @var WC_Multistore_Image instance
	 */
	protected static $instance = false;

	/**
	 * Get class instance
	 */
	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Class constructor
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init function
	 */
	public function init() {
		$this->set_settings();
		$this->includes();
		$this->hooks();
	}

	/**
	 * Set global image settings
	 */
	private function set_settings() {
		$this->settings['enable-global-image']  = WC_Multistore_Functions::get_option( 'enable-global-image' ) == 'yes';
		$this->master_site  = WC_Multistore_Functions::get_option( 'global-image-master' );
	}

	/**
	 * Load required files
	 */
	public function includes(){
		$this->child_site_includes();
	}

	/**
	 * Load child site required files
	 */
	public function child_site_includes(){
		if( $this->is_master_site() || ! $this->is_enabled_global_image() ){ return; }
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Load hooks
	 */
	public function hooks(){
		$this->all_hooks();
		$this->master_site_hooks();
		$this->child_site_hooks();
	}

	/**
	 * Load hooks for all types of sites( master or child )
	 */
	public function all_hooks(){
	}

	/**
	 * Load master hooks
	 */
	public function master_site_hooks(){
		if( ! $this->is_master_site() || ! $this->is_enabled_global_image() ){ return; }

		// Product Description
		add_filter( 'wc_multistore_single_get_description' , array( $this, 'wc_multistore_single_get_description' ) );

		// Product Short Description
		add_filter( 'wc_multistore_single_get_short_description' , array( $this, 'wc_multistore_single_get_short_description' ) );
	}

	/**
	 * Load child hooks
	 */
	public function child_site_hooks(){
		if( ! $this->is_child_site() || ! $this->is_enabled_global_image() ){ return; }

		// Attachment
		add_action('wp_ajax_query-attachments', array( $this, 'ajax_query_attachments' ), 0 );
		add_action('wp_ajax_get-attachment',  array( $this, 'ajax_get_attachment' ), 0 );
		add_action('wp_ajax_send-attachment-to-editor', array( $this, 'ajax_send_attachment_to_editor' ), 0 );
		add_filter('wp_get_attachment_image_src', array( $this, 'attachment_image_src' ), 99, 4 );
		add_filter('media_view_strings', array( $this, 'media_strings') );
		remove_filter('the_content', 'wp_filter_content_tags');
		add_filter('the_content', array( $this, 'filter_content_tags' ) );
		remove_filter('woocommerce_short_description', 'wp_filter_content_tags' );
		add_filter('woocommerce_short_description', array( $this, 'filter_content_tags' ) );

		// Thumbnail
		add_action('save_post', array( $this, 'save_thumbnail_meta' ), 99);
		add_filter('admin_post_thumbnail_html', array( $this, 'admin_post_thumbnail_html' ), 99, 3);
		add_filter('post_thumbnail_html', array( $this, 'post_thumbnail_html' ), 99, 5);

		// Woocommerce
		add_action('woocommerce_product_get_image', array( $this, 'filter_woocommerce_content_tags'), 99, 2 );
		add_action('woocommerce_available_variation', array( $this, 'available_variation'), 99, 1 );
		add_action('admin_init', function(){
			remove_action( 'product_cat_edit_form_fields', array( WC_Admin_Taxonomies::get_instance(), 'edit_category_fields' ), 10 );
			remove_action( 'manage_product_cat_custom_column', array( WC_Admin_Taxonomies::get_instance(), 'product_cat_column' ), 10 );
			remove_action('wp_ajax_woocommerce_load_variations', array( 'WC_AJAX','load_variations') );
        });
		add_filter('manage_product_cat_custom_column', array( $this, 'product_cat_column'), 99, 3 );
		add_action( 'product_cat_edit_form_fields', array( $this, 'edit_category_fields' ), 11 );
		add_action( 'wp_ajax_woocommerce_load_variations', array( $this, 'load_variations' ) );
	}

	/**
	 * Enqueue Scripts
	 */
	public function enqueue_scripts(){
		if( get_current_screen()->post_type != 'product' ){ return; }
		wp_register_script( 'woomulti-single-global-image-js', plugins_url( '/assets/js/global-image.js',  dirname( __FILE__, 2 ) ), array('media-views'), WOO_MSTORE_VERSION );
		wp_enqueue_script( 'woomulti-single-global-image-js' );
	}

	/**
	 * @return bool
	 */
	public function is_child_site(){
		if ( $this->is_enabled_global_image() && ! $this->is_master_site() ) {
			return true;
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function is_master_site(){
		$global_image_master  = $this->master_site;
		$current_blog_id      = get_current_blog_id();

		if ( $this->is_enabled_global_image() && $current_blog_id == $global_image_master ) {
			return true;
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function is_enabled_global_image(){
		$global_image = $this->settings['enable-global-image'];

		if ( $global_image ) {
			return true;
		}

		return false;
	}

	/**
	 *
	 */
	public function ajax_query_attachments(){
		$query = isset($_REQUEST['query']) // csrf ok
			? (array) wp_unslash($_REQUEST['query']) // csrf ok
			: [];

		if (!empty($query['global_image'])) {
			switch_to_blog( $this->master_site );
			add_filter('wp_prepare_attachment_for_js', array( $this, 'prepare_attachment_for_js' ), 0);
		}

		wp_ajax_query_attachments();
	}

	/**
	 *
	 */
	public function ajax_get_attachment(){
		$attachmentId   = (int) wp_unslash($_REQUEST['id']);
		$idPrefix       = 1000000;

		if ($this->id_prefix_included_in_attachment_id( $attachmentId, $idPrefix ) ) {
			$attachmentId = $this->strip_site_id_prefix_from_attachment_id( $idPrefix, $attachmentId );
			$_REQUEST['id'] = $attachmentId;

			switch_to_blog( $this->master_site );
			add_filter('wp_prepare_attachment_for_js', array( $this, 'prepare_attachment_for_js' ), 0);
			restore_current_blog();
		}

		wp_ajax_get_attachment();
	}

	/**
	 * @param $response
	 *
	 * @return mixed
	 */
	public function prepare_attachment_for_js( $response ){
		$idPrefix = 1000000;

		$response['id'] = (int) ($idPrefix.$response['id']); // Unique ID, must be a number.
		$response['nonces']['update'] = false;
		$response['nonces']['edit'] = false;
		$response['nonces']['delete'] = false;
		$response['editLink'] = false;

		return $response;
	}

	/**
	 *
	 */
	public function ajax_send_attachment_to_editor(){
		$attachment     = wp_unslash( $_POST['attachment'] ); // csrf ok
		$attachmentId   = (int) $attachment['id'];
		$idPrefix       = 1000000;

		if ($this->id_prefix_included_in_attachment_id($attachmentId, $idPrefix)) {
			$attachment['id'] = $this->strip_site_id_prefix_from_attachment_id($idPrefix, $attachmentId);
			$_POST['attachment'] = wp_slash($attachment);

			switch_to_blog($this->master_site);

			add_filter( 'mediaSendToEditor', array( $this, 'media_send_to_editor' ), 10, 2);
		}

		wp_ajax_send_attachment_to_editor();
	}

	/**
	 * @param $html
	 * @param $id
	 *
	 * @return array|string|string[]
	 */
	public function media_send_to_editor( $html, $id){
		$idPrefix = 1000000;
		$newId = $idPrefix.$id; // Unique ID, must be a number.

		$search = 'wp-image-'.$id;
		$replace = 'wp-image-'.$newId;

		return str_replace($search, $replace, $html);
	}

	/**
	 * @param $image
	 * @param $attachmentId
	 * @param $size
	 * @param $icon
	 *
	 * @return mixed
	 */
	public function attachment_image_src( $image, $attachmentId, $size, $icon ){
		$attachmentId   = (int) $attachmentId;
		$idPrefix       = 1000000;

		if (!$this->id_prefix_included_in_attachment_id($attachmentId, $idPrefix)) {
			return $image;
		}

		$attachmentId = $this->strip_site_id_prefix_from_attachment_id($idPrefix, $attachmentId);
		switch_to_blog( $this->master_site );
		$image = wp_get_attachment_image_src($attachmentId, $size, $icon);
		restore_current_blog();

		return $image;
	}

	/**
	 * @param $strings
	 *
	 * @return mixed
	 */
	public function media_strings($strings){
		$strings['globalImageTitle'] = esc_html__('Global Image', 'woonet');

		return $strings;
	}

	/**
	 * @param $content
	 *
	 * @return array|mixed|string|string[]
	 */
	public function filter_content_tags( $content ){

		if ( ! preg_match_all('/<img [^>]+>/', $content, $matches ) ) {
			return $content;
		}

		$selectedImages = $attachmentIds = [];

		foreach ( $matches[0] as $image ) {
			$hasSrcset = strpos($image, ' srcset=') !== false;
			$hasClassId = preg_match('/wp-image-(\d+)/i', $image, $classId);
			$attachmentId = !$hasSrcset && $hasClassId
				? absint($classId[1])
				: null;
			if ($attachmentId) {
				// If exactly the same image tag is used more than once, overwrite it.
				// All identical tags will be replaced later with 'str_replace()'.
				$selectedImages[$image] = $attachmentId;
				// Overwrite the ID when the same image is included more than once.
				$attachmentIds[$attachmentId] = true;
			}
		}

		if (count($attachmentIds) > 1) {
			// Warm the object cache with post and meta information for all found
			// images to avoid making individual database calls.
			_prime_post_caches(array_keys($attachmentIds), false);
		}

		$idPrefix = 1000000;

		foreach ($selectedImages as $image => $attachmentId) {
			if (!$this->id_prefix_included_in_attachment_id($attachmentId, $idPrefix)) {
				$imageMeta = wp_get_attachment_metadata($attachmentId);
				$content = str_replace(
					$image,
					wp_image_add_srcset_and_sizes($image, $imageMeta, $attachmentId),
					$content
				);
				continue;
			}

			$globalAttachmentId = $this->strip_site_id_prefix_from_attachment_id($idPrefix, $attachmentId);

			switch_to_blog($this->master_site);
			$imageMeta = wp_get_attachment_metadata($globalAttachmentId);
			$content = str_replace($image, wp_image_add_srcset_and_sizes($image, $imageMeta, $attachmentId), $content);
			restore_current_blog();
		}

		return $content;
	}

	/**
	 * @param $image
	 * @param $product
	 *
	 * @return array|false|mixed|string|string[]
	 */
	public function filter_woocommerce_content_tags( $image, $product ){
		$idPrefix = 1000000;

		if ( ! $this->id_prefix_included_in_attachment_id( $product->get_image_id(), $idPrefix ) ) {
			return $image;
		}

		$globalAttachmentId = $this->strip_site_id_prefix_from_attachment_id( $idPrefix, $product->get_image_id() );

		switch_to_blog($this->master_site);
		$imageMeta = wp_get_attachment_metadata($globalAttachmentId);
		$image = str_replace($image, wp_image_add_srcset_and_sizes($image, $imageMeta, $globalAttachmentId), $image);
		restore_current_blog();

		return $image;

	}


	/**
	 * @param $available_variation
	 *
	 * @return false|mixed
	 */
	function available_variation( $available_variation ){
		$idPrefix = 1000000;

		if ( ! $this->id_prefix_included_in_attachment_id( $available_variation['image_id'], $idPrefix ) ) {
			return $available_variation;
		}

		$globalAttachmentId = $this->strip_site_id_prefix_from_attachment_id( $idPrefix, $available_variation['image_id'] );

		switch_to_blog($this->master_site);
		$image = wc_get_product_attachment_props( $globalAttachmentId );
		restore_current_blog();

		$available_variation['image'] = $image;

		return $available_variation;
	}

	/**
	 * @param $postId
	 */
	public function save_thumbnail_meta( $postId ){
		$idPrefix = 1000000;

		$attachmentId = (int)filter_input(
			INPUT_POST,
			'_thumbnail_id',
			FILTER_SANITIZE_NUMBER_INT
		);

		if ( ! $attachmentId ) {
			return;
		}

		if ( $this->id_prefix_included_in_attachment_id( $attachmentId, $idPrefix ) ) {
			update_post_meta( $postId, '_thumbnail_id', $attachmentId );
		}
	}

	/**
	 * @param $content
	 * @param $postId
	 * @param $attachmentId
	 *
	 * @return array|mixed|string|string[]
	 */
	public function admin_post_thumbnail_html( $content, $postId, $attachmentId ){
		$attachmentId = (int)$attachmentId;
		$idPrefix = 1000000;

		if (false === $this->id_prefix_included_in_attachment_id($attachmentId, $idPrefix)) {
			return $content;
		}

		$post = get_post($postId);
		$attachmentId = $this->strip_site_id_prefix_from_attachment_id($idPrefix, $attachmentId);

		switch_to_blog($this->master_site);
		$content = _wp_post_thumbnail_html($attachmentId, $post);
		restore_current_blog();

		$search = 'value="' . $attachmentId . '"';
		$replace = 'value="' . $idPrefix . $attachmentId . '"';
		$content = str_replace($search, $replace, $content);

		$post = get_post($postId);
		$postTypeObject = null;

		$removeImageLabel = _x('Remove featured image', 'post', 'woonet');
		if ($post !== null) {
			$postTypeObject = get_post_type_object($post->post_type);
		}
		if ($postTypeObject !== null) {
			$removeImageLabel = $postTypeObject->labels->remove_featured_image;
		}

		return $this->replace_remove_post_thumbnail_markup(
			$removeImageLabel,
			$content
		);
	}

	/**
	 * @param $html
	 * @param $postId
	 * @param $attachmentId
	 * @param $size
	 * @param $attr
	 *
	 * @return mixed
	 */
	public function post_thumbnail_html( $html, $postId, $attachmentId, $size, $attr ){
		$attachmentId   = (int)$attachmentId;
		$idPrefix       = 1000000;

		if ($this->id_prefix_included_in_attachment_id($attachmentId, $idPrefix)) {
			$attachmentId = $this->strip_site_id_prefix_from_attachment_id($idPrefix, $attachmentId);
			switch_to_blog($this->master_site);
			$html = wp_get_attachment_image($attachmentId, $size, false, $attr);
			restore_current_blog();
		}

		return $html;
	}

	/**
	 * @param $product_id
	 */
	public function save_gallery_ids( $product_id ){
		$productType = WC_Product_Factory::get_product_type( $product_id );
		$requestProductType = filter_input(
			INPUT_POST,
			'product-type',
			FILTER_SANITIZE_STRING
		);

		$requestProductType and $productType = sanitize_title( stripslashes( $requestProductType ) );

		$productType = $productType ?: 'simple';
		$classname = WC_Product_Factory::get_product_classname( $product_id, $productType );

		$product = new $classname( $product_id );
		$attachmentIds = filter_input(
			INPUT_POST,
			'product_image_gallery',
			FILTER_SANITIZE_STRING
		);
		update_post_meta( $product->get_id(), '_product_image_gallery', $attachmentIds );
	}

	/**
	 * @param $content
	 *
	 * @return array|mixed|string|string[]
	 */
	public function wc_multistore_single_get_description( $content ){
		if ( ! preg_match_all('/<img [^>]+>/', $content, $matches ) ) {
			return $content;
		}

		$selectedImages = $attachmentIds = [];

		foreach( $matches[0] as $image ) {
			$hasClassId     = preg_match('/wp-image-(\d+)/i', $image, $classId );
			$attachmentId   = $hasClassId ? absint( $classId[1] ) : null;

			if ( $attachmentId ) {
				$selectedImages[$image] = $attachmentId;
				$attachmentIds[$attachmentId] = true;
			}
		}

		if ( count( $attachmentIds ) > 1) {
			_prime_post_caches(array_keys($attachmentIds), false);
		}

		$idPrefix = 1000000;

		foreach ( $selectedImages as $image => $attachmentId ) {
			$newAttachmentId = $idPrefix.$attachmentId;
			$search  = 'wp-image-'.$attachmentId;
			$replace = 'wp-image-'.$newAttachmentId;

			$content = str_replace(
				$search,
				$replace,
				$content
			);
		}

		return $content;
	}

	/**
	 * @param $content
	 *
	 * @return array|mixed|string|string[]
	 */
	public function wc_multistore_single_get_short_description( $content ){
		if ( ! preg_match_all('/<img [^>]+>/', $content, $matches ) ) {
			return $content;
		}

		$selectedImages = $attachmentIds = [];

		foreach( $matches[0] as $image ) {
			$hasClassId     = preg_match('/wp-image-(\d+)/i', $image, $classId );
			$attachmentId   = $hasClassId ? absint( $classId[1] ) : null;

			if ( $attachmentId ) {
				$selectedImages[$image] = $attachmentId;
				$attachmentIds[$attachmentId] = true;
			}
		}

		if ( count( $attachmentIds ) > 1) {
			_prime_post_caches(array_keys($attachmentIds), false);
		}

		$idPrefix = 1000000;

		foreach ( $selectedImages as $image => $attachmentId ) {
			$newAttachmentId = $idPrefix.$attachmentId;
			$search  = 'wp-image-'.$attachmentId;
			$replace = 'wp-image-'.$newAttachmentId;

			$content = str_replace(
				$search,
				$replace,
				$content
			);
		}

		return $content;
	}

	/**
	 * @param $columns
	 * @param $column
	 * @param $id
	 *
	 * @return mixed|string
	 */
	public function product_cat_column( $columns, $column, $id ) {
		if ( 'thumb' === $column ) {
			// Prepend tooltip for default category.
			$default_category_id = absint( get_option( 'default_product_cat', 0 ) );

			if ( $default_category_id === $id ) {
				$columns .= wc_help_tip( __( 'This is the default category and it cannot be deleted. It will be automatically assigned to products with no category.', 'woocommerce' ) );
			}

			$thumbnail_id = get_term_meta( $id, 'thumbnail_id', true );

			if ( $thumbnail_id ) {
				$image = wp_get_attachment_image_src( $thumbnail_id );
				$image = $image[0];
			} else {
				$image = wc_placeholder_img_src();
			}

			// Prevent esc_url from breaking spaces in urls for image embeds. Ref: https://core.trac.wordpress.org/ticket/23605 .
			$image    = str_replace( ' ', '%20', $image );
			$columns .= '<img src="' . esc_url( $image ) . '" alt="' . esc_attr__( 'Thumbnail', 'woocommerce' ) . '" class="wp-post-image" height="48" width="48" />';
		}
		if ( 'handle' === $column ) {
			$columns .= '<input type="hidden" name="term_id" value="' . esc_attr( $id ) . '" />';
		}
		return $columns;
	}

	/**
	 * @param $term
	 */
	public function edit_category_fields( $term ) {

		$display_type = get_term_meta( $term->term_id, 'display_type', true );
		$thumbnail_id = absint( get_term_meta( $term->term_id, 'thumbnail_id', true ) );

		if ( $thumbnail_id ) {
			$image = wp_get_attachment_image_src( $thumbnail_id );
			$image = $image[0];
		} else {
			$image = wc_placeholder_img_src();
		}
		?>
		<tr class="form-field term-display-type-wrap">
			<th scope="row" valign="top"><label><?php esc_html_e( 'Display type', 'woocommerce' ); ?></label></th>
			<td>
				<select id="display_type" name="display_type" class="postform">
					<option value="" <?php selected( '', $display_type ); ?>><?php esc_html_e( 'Default', 'woocommerce' ); ?></option>
					<option value="products" <?php selected( 'products', $display_type ); ?>><?php esc_html_e( 'Products', 'woocommerce' ); ?></option>
					<option value="subcategories" <?php selected( 'subcategories', $display_type ); ?>><?php esc_html_e( 'Subcategories', 'woocommerce' ); ?></option>
					<option value="both" <?php selected( 'both', $display_type ); ?>><?php esc_html_e( 'Both', 'woocommerce' ); ?></option>
				</select>
			</td>
		</tr>
		<tr class="form-field term-thumbnail-wrap">
			<th scope="row" valign="top"><label><?php esc_html_e( 'Thumbnail', 'woocommerce' ); ?></label></th>
			<td>
				<div id="product_cat_thumbnail" style="float: left; margin-right: 10px;"><img src="<?php echo esc_url( $image ); ?>" width="60px" height="60px" /></div>
				<div style="line-height: 60px;">
					<input type="hidden" id="product_cat_thumbnail_id" name="product_cat_thumbnail_id" value="<?php echo esc_attr( $thumbnail_id ); ?>" />
					<button type="button" class="upload_image_button button"><?php esc_html_e( 'Upload/Add image', 'woocommerce' ); ?></button>
					<button type="button" class="remove_image_button button"><?php esc_html_e( 'Remove image', 'woocommerce' ); ?></button>
				</div>
				<script type="text/javascript">

                    // Only show the "remove image" button when needed
                    if ( '0' === jQuery( '#product_cat_thumbnail_id' ).val() ) {
                        jQuery( '.remove_image_button' ).hide();
                    }

                    // Uploading files
                    var file_frame;

                    jQuery( document ).on( 'click', '.upload_image_button', function( event ) {

                        event.preventDefault();

                        // If the media frame already exists, reopen it.
                        if ( file_frame ) {
                            file_frame.open();
                            return;
                        }

                        // Create the media frame.
                        file_frame = wp.media.frames.downloadable_file = wp.media({
                            title: '<?php esc_html_e( 'Choose an image', 'woocommerce' ); ?>',
                            button: {
                                text: '<?php esc_html_e( 'Use image', 'woocommerce' ); ?>'
                            },
                            multiple: false
                        });

                        // When an image is selected, run a callback.
                        file_frame.on( 'select', function() {
                            var attachment           = file_frame.state().get( 'selection' ).first().toJSON();
                            var attachment_thumbnail = attachment.sizes.thumbnail || attachment.sizes.full;

                            jQuery( '#product_cat_thumbnail_id' ).val( attachment.id );
                            jQuery( '#product_cat_thumbnail' ).find( 'img' ).attr( 'src', attachment_thumbnail.url );
                            jQuery( '.remove_image_button' ).show();
                        });

                        // Finally, open the modal.
                        file_frame.open();
                    });

                    jQuery( document ).on( 'click', '.remove_image_button', function() {
                        jQuery( '#product_cat_thumbnail' ).find( 'img' ).attr( 'src', '<?php echo esc_js( wc_placeholder_img_src() ); ?>' );
                        jQuery( '#product_cat_thumbnail_id' ).val( '' );
                        jQuery( '.remove_image_button' ).hide();
                        return false;
                    });

				</script>
				<div class="clear"></div>
			</td>
		</tr>
		<?php
	}

	/**
	 * Load variations via AJAX.
	 */
	public function load_variations() {
		ob_start();

		check_ajax_referer( 'load-variations', 'security' );

		if ( ! current_user_can( 'edit_products' ) || empty( $_POST['product_id'] ) ) {
			wp_die( -1 );
		}

		// Set $post global so its available, like within the admin screens.
		global $post;

		$loop           = 0;
		$product_id     = absint( $_POST['product_id'] );
		$post           = get_post( $product_id ); // phpcs:ignore
		$product_object = wc_get_product( $product_id );
		$per_page       = ! empty( $_POST['per_page'] ) ? absint( $_POST['per_page'] ) : 10;
		$page           = ! empty( $_POST['page'] ) ? absint( $_POST['page'] ) : 1;
		$variations     = wc_get_products(
			array(
				'status'  => array( 'private', 'publish' ),
				'type'    => 'variation',
				'parent'  => $product_id,
				'limit'   => $per_page,
				'page'    => $page,
				'orderby' => array(
					'menu_order' => 'ASC',
					'ID'         => 'DESC',
				),
				'return'  => 'objects',
			)
		);

		if ( $variations ) {
			wc_render_invalid_variation_notice( $product_object );

			foreach ( $variations as $variation_object ) {
				$variation_id   = $variation_object->get_id();
				$variation      = get_post( $variation_id );
				$variation_data = array_merge( get_post_custom( $variation_id ), wc_get_product_variation_attributes( $variation_id ) ); // kept for BW compatibility.
				include WOO_MSTORE_PATH. '/multisite/templates/html-variation-admin.php';
				$loop++;
			}
		}
		wp_die();
	}

	/**
	 * @param $replace
	 * @param $subject
	 *
	 * @return array|string|string[]
	 */
	public function replace_remove_post_thumbnail_markup( $replace, $subject ){
		$search = '<p class="hide-if-no-js"><a href="#" id="remove-post-thumbnail"></a></p>';
		$replace = sprintf(
			'<p class="hide-if-no-js"><a href="#" id="remove-post-thumbnail">%s</a></p>',
			$replace
		);

		return str_replace($search, $replace, $subject);
	}

	/**
	 * @param $attachmentId
	 * @param $idPrefix
	 *
	 * @return bool
	 */
	private function id_prefix_included_in_attachment_id( $attachmentId, $idPrefix ){
		return false !== strpos((string)$attachmentId, (string)$idPrefix);
	}

	/**
	 * @param $idPrefix
	 * @param $attachmentId
	 *
	 * @return int
	 */
	private function strip_site_id_prefix_from_attachment_id( $idPrefix, $attachmentId ){
		return (int)str_replace($idPrefix, '', (string)$attachmentId);
	}


}

$WC_Multistore_Image = WC_Multistore_Image::get_instance();