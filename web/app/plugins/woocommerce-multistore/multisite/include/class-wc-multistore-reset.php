<?php
/**
 * Plugin Reset handler.
 *
 * This handles plugin reset functionality in Woocommerce Multistore.
 *
 */

defined( 'ABSPATH' ) || exit;

/**
 * Class WC_Multistore_Reset
 */
class WC_Multistore_Reset {

	/**
	 * @var array
	 */
	private $system_messages = array();

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'init' ) );
		add_action( 'wp_ajax_woomulti_disconnected_from_main_site', array( $this, 'hook_woomulti_disconnected_from_main_site' ), 10, 0 );
		add_action( 'wp_ajax_nopriv_woomulti_disconnected_from_main_site', array( $this, 'hook_woomulti_disconnected_from_main_site' ), 10, 0 );
		add_action( 'wp_ajax_woomulti_disconnected_from_child_site', array( $this, 'hook_woomulti_disconnected_from_child_site' ), 10, 0 );
		add_action( 'wp_ajax_nopriv_woomulti_disconnected_from_child_site', array( $this, 'hook_woomulti_disconnected_from_child_site' ), 10, 0 );
		if ( !is_multisite() ) {
			$manager               = new WC_Multistore_Functions();
			$this->default_options = $manager->get_defaults();
		} else if ( is_multisite() ) {
			// check for other dependencies
			$this->default_options = WC_Multistore_Functions::get_options();
		}
	}
	
	/* Hook used to remove the child site those are added with Master site upon reset from Master site */
	public function hook_woomulti_disconnected_from_main_site() {
		global $wpdb;
		
		$_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();
		
		/* Before Ajax Call, Function is authenticate with Key */
		if ( ! $_engine->is_request_authenticated( $_POST ) ) {
		
			/* Returnning data in Json format */
			wp_send_json(
				array(
					'disconnect_code' => 2,
					'status'  => 'failed',
					'message' => 'Authorization failed for ' . site_url(),
				)
			);
			die;
		}
		
		if(!isset($_POST['main_site_uuid']) && $_POST['main_site_uuid'] == '') {

			/* Returnning data in Json format */
			wp_send_json(
				array(
					'disconnect_code' => 2,
					'status'  => 'failed',
					'message' => 'UUID does not found.',
				)
			);		
		}
		
		/* Get the master site infornation */
		$connected_main_sites = get_option( 'woonet_master_connect', array() );
		if (!empty( $connected_main_sites ) ) {
			if ( $connected_main_sites['uuid'] == $_POST['main_site_uuid'] ) {
				if(delete_option( 'woonet_master_connect' )) {
					
					/* Delete All Meta Data in child site those are related to Master site after reset */
					$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->postmeta WHERE meta_key LIKE %s",'_woonet%'));
		
					/* Returnning data in Json format */
					wp_send_json(
						array(
							'disconnect_code' => 1,
							'status'  => 'success',
							'message' => 'Site disconnected succesfully.',
						)
					);
				} else {
				
					/* Returnning data in Json format */
					wp_send_json(
						array(
							'disconnect_code' => 2,
							'status'  => 'failed',
							'message' => 'Site disconnected un-succesfully.',
						)
					);
				}	
			}
		}
		
		/* Returnning data in Json format */
		wp_send_json(
			array(
				'disconnect_code' => 2,
				'status'  => 'success',
				'message' => 'Child site not found.',
			)
		);
	}
	
	/* Hook used to remove the master site those are added with Child site upon reset from Child site */
	public function hook_woomulti_disconnected_from_child_site() {
		global $wpdb;
		
		$_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

		/* Before Ajax Call, Function is authenticate with Key */
		if ( ! $_engine->is_request_authenticated( $_POST ) ) {

			/* Returnning data in Json format */
			wp_send_json(
				array(
					'disconnect_code' => 2,
					'status'  => 'failed',
					'message' => 'Authorization failed for ' . site_url(),
				)
			);
			die;
		}
		
		if(isset($_POST['child_site_uuid']) && $_POST['child_site_uuid'] == '') {
	
			/* Returnning data in Json format */
			wp_send_json(
				array(
					'disconnect_code' => 2,
					'status'  => 'success',
					'message' => 'Key does not found.',
				)
			);		
		}
		$child_site_uuid = $_POST['child_site_uuid'];
		
		// Disconnect Active site with child sites
		$connected_sites = get_option( 'woonet_child_sites' );
		if (!empty( $connected_sites ) ) {
			
			/* Searching Array Key Based on Site UUID */
			$unset_key = array_search($child_site_uuid, array_column($connected_sites, 'uuid', 'site_key'));
			if(isset($unset_key) && isset($connected_sites[$unset_key]) && !empty($connected_sites[$unset_key]))
			{			
				/* Deleting metadata after reset */	
				/* translators: %s: Site UUID */	
				$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->postmeta WHERE meta_key LIKE %s",'_woonet_publish_to_'.$child_site_uuid.'%'));
				$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->postmeta WHERE meta_key LIKE %s",'_woonet_'.$child_site_uuid.'%'));
				unset($connected_sites[$unset_key]);
				update_option( 'woonet_child_sites', $connected_sites );
			}
		}
		// Disconnect Inactive site with child sites		
		$inactive_sites  = get_option( 'woonet_child_sites_deactivated', array() );
		if (!empty( $inactive_sites ) ) {
		
			/* Searching Array Key Based on Site UUID */
			$unset_key = array_search($child_site_uuid, array_column($inactive_sites, 'uuid', 'site_key'));
			if(isset($unset_key) && isset($inactive_sites[$unset_key]) && !empty($inactive_sites[$unset_key]))
			{
				/* Deleting metadata after reset */	
				/* translators: %s: Site UUID */	
				$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->postmeta WHERE meta_key LIKE %s",'_woonet_publish_to_'.$child_site_uuid.'%'));
				$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->postmeta WHERE meta_key LIKE %s",'_woonet_'.$child_site_uuid.'%'));		
				unset($inactive_sites[$unset_key]);
				update_option( 'woonet_child_sites_deactivated', $inactive_sites );
			}
		}
		wp_send_json(
			array(
				'disconnect_code' => 1,
				'status'  => 'success',
				'message' => 'Child site has been disconnected from Main Site.',
			)
		);	
	}
	
	public function network_admin_menu() {
		// only if superadmin
		if ( is_multisite() && ! current_user_can( 'manage_sites' ) ) {
			return;
		} elseif ( ! is_multisite() && ! current_user_can( 'manage_options' ) ) {
			return;
		}
		if ( is_multisite() ) {
			$licence = new WC_Multistore_Licence();

			if ( ! $licence->licence_key_verify() ) {
				return;
			}
		}
		else if ( !is_multisite() && ! woomulti_has_valid_license() && get_option( 'woonet_network_type' ) == 'master') {
			return;
		}
		else if ( get_option( 'woonet_network_type' ) == '') {
			return;
		}

		$menus_hook = add_submenu_page(
			'woonet-woocommerce',
			__( 'Reset', 'woonet' ),
			__( 'Reset', 'woonet' ),
			'manage_options',
			'woonet-multistore-reset',
			array( $this, 'interface_multistore_reset_page' ),
			99
		);

		add_action( 'load-' . $menus_hook, array( $this, 'admin_notices' ) );
		add_action( 'admin_print_styles-' . $menus_hook, array( $this, 'admin_print_styles' ) );
		add_action( 'admin_print_scripts-' . $menus_hook, array( $this, 'admin_print_scripts' ) );
	}

	public function admin_print_styles() {

		if ( is_multisite() ) {
			$url = WOO_MSTORE_URL;
		} else {
			$url = dirname( WOO_MSTORE_URL );
		}
		wp_enqueue_style( 'woonet-multistore-reset', $url . '/assets/css/woosl-reset.css' );
	}

	public function admin_print_scripts() {
		if ( is_multisite() ) {
			$url = WOO_MSTORE_URL;
		} else {
			$url = dirname( WOO_MSTORE_URL );
		}
	}

	public function init() {
		if ( is_multisite() ) {
			add_action( 'network_admin_menu', array( $this, 'network_admin_menu' ) );
		} else {
			add_action( 'admin_menu', array( $this, 'network_admin_menu' ), PHP_INT_MAX );
		}

		// check for any forms save
		if ( isset( $_POST['woo_multistore_form_submit'] ) && 'reset-multistore' == $_POST['woo_multistore_form_submit'] ) {
			
			$woonet_multistore_reset_nonce = $_POST['woonet-multistore-reset-nonce'];
			
			/* Verifying nonce */
			if ( ! wp_verify_nonce( $woonet_multistore_reset_nonce, 'woonet-multistore-reset' ) ) {
				$this->system_messages[] = array(
					'type'    => 'error',
					'message' => 'Nonce is invalid.',
				);	 
			} else {
				
				/* Checking if user checked the confirm checkbox on reset form */
				if(isset( $_POST['reset-confirm'] ) && $_POST['reset-confirm'] == '1') {
					if(is_multisite()) {
						
						/* If form is submitted from Multisite */
						$this->form_submit_settings_multisite();
					} else if(!is_multisite() && get_option( 'woonet_network_type' ) == 'master') {
					
						/* If form is submitted from Master site */
						$this->form_submit_settings_master();
					} else if(!is_multisite() && get_option( 'woonet_network_type' ) == 'child') {
					
						/* If form is submitted from Child Site */
						$this->form_submit_settings_child();				
					}
				} else {
				
					/* If user does not checked the confirm checkbox */
					$this->system_messages[] = array(
						'type'    => 'error',
						'message' => 'Please confirm before proceeding.',
					);			
				}
			}
		}
	}
	
	private function form_submit_settings_multisite() {
		global $wpdb;
		
		/* Deactivating Licence Ket upon reset from Multisite */
		$response = WOO_MULTISTORE()->license_manager->deactivate();
		
		/* If reset successfully */
		if($response['status'] == 1) {
			// Delete the Licence key
			delete_site_option( 'mstore_license' );
			
			// Delete the Setup Wizard Complete Status
			delete_site_option( 'woonet_setup_wizard_complete' );
			
			/* Update default option */
			update_site_option( 'mstore_options', $this->default_options );
			wp_redirect( network_site_url( 'wp-admin/network/settings.php?page=woo-ms-options' ) );
			die();
		} else {
			$this->system_messages[] = array(
				'type'    => 'error',
				'message' => $response['msg'],
			);
			return false;		
		}
	}
	
	private function form_submit_settings_master() {
		global $wpdb;
		
		/* Deactivating Licence Ket upon reset from Master site */
		$response = WOO_MULTISTORE()->license_manager->deactivate();
		
		/* If reset successfully */
		if($response['status'] == 1) {
			
			// Delete the Licence key
			delete_site_option( 'mstore_license' );
			
			// Delete the Setup Wizard Complete Status
			delete_option( 'woonet_setup_wizard_complete' );
				
			// Disconnect Active site with child sites
			$connected_sites = get_option( 'woonet_child_sites' );		
			if(!empty($connected_sites)) {
				foreach ( $connected_sites as $site_data ) {					
					$data = array(
					'action'             => 'woomulti_disconnected_from_main_site',
					'Authorization'      => $site_data['site_key'],
					'main_site_uuid'       => $site_data['uuid'],
					);
	
					$url = $site_data['site_url'] . '/wp-admin/admin-ajax.php';
	
					$headers = array(
						'Authorization' => $site_data['site_key'],
					);
	
					$result = wp_remote_post(
						$url,
						array(
							'headers' => $headers,
							'body'    => $data,
						)
					);
					
					if ( is_wp_error( $result ) || $result['response']['code'] != 200 ) {
						//return;
					}
					else {
						$uuid = $site_data['uuid'];
						
						$response_disconnection = json_decode( $result['body'] );
						
						if($response_disconnection->disconnect_code == 1) {

							/* Deleting metadata after reset */	
											
							/* translators: %s: Site UUID */	
							$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->postmeta WHERE meta_key LIKE %s",'_woonet_publish_to_'.$uuid.'%'));
							$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->postmeta WHERE meta_key LIKE %s",'_woonet_'.$uuid.'%'));				
						}
					}
				}
			}
			
			// Disconnect Inactive site with child sites
			
			$inactive_sites  = get_option( 'woonet_child_sites_deactivated', array() );
	
			if(!empty($inactive_sites)) {
				foreach ( $inactive_sites as $site_data ) {					
					$data = array(
					'action'             => 'woomulti_disconnected_from_main_site',
					'Authorization'      => $site_data['site_key'],
					'main_site_uuid'       => $site_data['uuid'],
					);
	
					$url = $site_data['site_url'] . '/wp-admin/admin-ajax.php';
	
					$headers = array(
						'Authorization' => $site_data['site_key'],
					);
	
					$result = wp_remote_post(
						$url,
						array(
							'headers' => $headers,
							'body'    => $data,
						)
					);
					
					if ( is_wp_error( $result ) || $result['response']['code'] != 200 ) {
						//return;
					}
					else {
						$uuid = $site_data['uuid'];
						
						$response_disconnection = json_decode( $result['body'] );
						
						if($response_disconnection->disconnect_code == 1) {
						
							/* Deleting metadata after reset */	
							/* translators: %s: Site UUID */	
							$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->postmeta WHERE meta_key LIKE %s",'_woonet_publish_to_'.$uuid.'%'));
							$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->postmeta WHERE meta_key LIKE %s",'_woonet_'.$uuid.'%'));				
						}
					}
				}
			}
			
			// Delete the NetWork type
			delete_option( 'woonet_network_type' );
			
			// Delete the Child Sites
			delete_option( 'woonet_child_sites' );
			delete_option( 'woonet_child_sites_deactivated' );
			
			// Reset the settings to default.
			delete_option( 'woonet_options' );			
			update_option( 'woonet_options', $this->default_options );
			
			wp_redirect( admin_url( 'admin.php?page=woonet-setup-wizard', 'relative' ) );
			die();
		} else {
			$this->system_messages[] = array(
				'type'    => 'error',
				'message' => $response['msg'],
			);
			return false;		
		}
	}

	private function form_submit_settings_child() {
		global $wpdb;
		// Disconnect Master site with child sites
		$master_connected_sites = get_option( 'woonet_master_connect' );
		if(!empty($master_connected_sites)) {
			if(isset($master_connected_sites['key']) && isset($master_connected_sites['master_url'])) {
			
				$data = array(
					'action'             => 'woomulti_disconnected_from_child_site',
					'Authorization'      => $master_connected_sites['key'],
					'child_site_uuid'       => $master_connected_sites['uuid'],
				);

				$url = $master_connected_sites['master_url'] . '/wp-admin/admin-ajax.php';

				$headers = array(
					'Authorization' => $master_connected_sites['master_url'],
				);

				$result = wp_remote_post(
					$url,
					array(
						'headers' => $headers,
						'body'    => $data,
					)
				);
				
				if ( is_wp_error( $result ) || $result['response']['code'] != 200 ) {
					//return;
				}
				else {
					$response_disconnection = json_decode( $result['body'] );
	
					if($response_disconnection->disconnect_code == 1) {
						$wpdb->query($wpdb->prepare("DELETE FROM $wpdb->postmeta WHERE meta_key LIKE %s",'_woonet%'));
					}
				}
			}
		}
		// Delete the Master Connect
		delete_option( 'woonet_master_connect' );
		// Delete the NetWork type
		delete_option( 'woonet_network_type' );
		// Delete the Setup Wizard Complete Status
		delete_option( 'woonet_setup_wizard_complete' );
		wp_redirect( admin_url( 'admin.php?page=woonet-setup-wizard', 'relative' ) );
		die();
	}
	public function admin_notices() {
		if ( count( $this->system_messages ) < 1 ) {
			return;
		}
		foreach ( $this->system_messages as $system_message ) {
			if ( isset( $system_message['type'] ) ) {
				echo "<div class='notice " . $system_message['type'] . "'><p>" . $system_message['message'] . '</p></div>';
			} else {
				echo "<div class='notice notice-error'><p>" . $system_message . '</p></div>';
			}
		}
	}
	public function interface_multistore_reset_page() {
//		if ( is_multisite() ) {
			$path = WOO_MSTORE_PATH;
//		} else {
//			$path = dirname( WOO_MSTORE_PATH );
//		}

		include $path . '/multisite/templates/html-multistore-reset.php';
	}
}

$GLOBALS['WC_Multistore_Reset'] = new WC_Multistore_Reset();