<?php
/**
 * API Orders
 *
 * @since: 4.1.6
 **/


defined( 'ABSPATH' ) || exit;

class WOO_MSTORE_REST_ORDERS {
    /**
     * Add action hooks on instantiation
     **/
    public function __construct() {
        // class loaded by rest_api_init hook.
        register_rest_route( 'woonet/v1', 'orders', array(
            'methods' => 'GET',
            'callback' => array( $this, 'get_child_orders' ),
            'permission_callback' => '__return_true',
        ));
    }
    
    public function get_child_orders( WP_REST_Request $request ) {
        $network_order_interface = new WOO_MSTORE_SINGLE_NETWORK_ORDERS( false );
        
        $per_page = $request->get_param( 'per_page' );
        $page = $request->get_param( 'page' );
        
        $orders = $network_order_interface->get_all_sites_orders( $per_page, $page );
        $output = array();
        
        if ( ! empty( $orders['results'] ) ) {
            foreach( $orders['results'] as $key => $value ) {
                $output[] = $value;
            }
        }
        
        return $output;
    }
    
}

new WOO_MSTORE_REST_ORDERS();