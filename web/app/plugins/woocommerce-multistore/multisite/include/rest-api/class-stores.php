<?php
/**
 * API Stores
 *
 * @since: 4.1.6
 **/

defined( 'ABSPATH' ) || exit;

class WOO_MSTORE_REST_STORES {
	/**
	 * Add action hooks on instantiation
	 **/
	public function __construct() {
		// class loaded by rest_api_init hook. 
		register_rest_route( 'woonet/v1', 'stores', array(
			'methods' => 'GET',
			'callback' => array( $this, 'get_stores' ),
			'permission_callback' => '__return_true',
		));
	}

	public function get_stores( WP_REST_Request $request ) {
        $sites = WOO_MULTISTORE()->site_manager->get_sites();
        $site_ids = array();

        foreach ( $sites as $site ) {
            $site_ids[] = array(
                'id'  => $site['uuid'],
                'url' => $site['site_url'],
            );
        }

        return $site_ids;
	}

}

new WOO_MSTORE_REST_STORES();
