<?php
/**
 * API Product Sync
 * 
 * Handles synchronization of new products added via the REST API.
 *
 * Add the right metadata, sync with the stores for products added via the REST API.
 * @since: 4.4.8
 **/

defined( 'ABSPATH' ) || exit;

class WOO_MSTORE_REST_PRODUCT_SYNC {
    /**
     * Add action hooks on instantiation
     **/
    public function __construct() {
        if ( is_multisite() ) {
            add_action( 'woocommerce_rest_insert_product_object', array( $this, 'ms_sync_product'), 10, 3 );
        } else {
            add_action( 'woocommerce_rest_insert_product_object', array( $this, 'rs_sync_product'), 10, 3 );
        }
    }

    public function rs_sync_product( $object, $request, $isCreating ) {
        // Run only on master.
        if (  WOO_MULTISTORE()->site_manager->get_type() != 'master' ) {
            return;
        }

        if ( WOO_MULTISTORE()->options_manager->get( 'synchronize-rest-by-default') == 'yes' ) {
            if ( $isCreating ) {

	            $_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();
	            $options = $_engine->get_options();

                // Mark options
                if ( WOO_MULTISTORE()->options_manager->get( 'inherit-rest-by-default') == 'yes' ) {
                    $child_inherit = 'yes';
                    $stock_sync = 'no';
                } else {
                    $child_inherit = 'no';
                    $stock_sync = 'no';
                }

               $uuids = WOO_MULTISTORE()->site_manager->get_site_uuids();

				foreach ( $uuids as $key => $uuid ){
					if( $options[$uuid]['result']['child_inherit_changes_fields_control__synchronize_rest_by_default'] == 'no' ){
						unset( $uuids[ $key ] );
					}
				}

               // Mark sync options.
               do_action( 'WOO_MSTORE_admin_product/set_sync_options', 
                    $object->get_id(), 
                    $uuids,
                    $child_inherit, 
                    $stock_sync
                );
            }

			// sync product right away.
	        do_action( 'WOO_MSTORE/Sync/QuickSync', $object->get_id() );
        }
    }

    public function ms_sync_product( $object, $request, $isCreating ) {
		global $WOO_MSTORE;
        $is_rest_on = WC_Multistore_Functions::get_option( 'synchronize-rest-by-default' );
        $is_inherit_on = WC_Multistore_Functions::get_option( 'inherit-rest-by-default' );

        if ( $is_rest_on != 'yes' ) {
            return;
        }

        if ( $isCreating ) {
            // Mark options
            if ( $is_inherit_on == 'yes' ) {
                $child_inherit = 'yes';
                $stock_sync = 'no';
            } else {
                $child_inherit = 'no';
                $stock_sync = 'no';
            }

            $ids =  WC_Multistore_Functions::get_active_woocommerce_blog_ids();
	        $options  = $WOO_MSTORE->functions->get_options();
	        foreach ( $ids as $key => $site_id ) {
		        if( $options['child_inherit_changes_fields_control__synchronize_rest_by_default'][$site_id] == 'no' ){
					unset($ids[$key]);
		        }
	        }

            // Mark sync options.
            do_action( 'WOO_MSTORE_admin_product/set_sync_options', 
                $object->get_id(), 
                $ids,
                $child_inherit, 
                $stock_sync
            );
        }

        // sync product right away.
        do_action( 'WOO_MSTORE/Sync/QuickSync', $object->get_id() );
    }
}

new WOO_MSTORE_REST_PRODUCT_SYNC();