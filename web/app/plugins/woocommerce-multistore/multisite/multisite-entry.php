<?php

defined( 'ABSPATH' ) || exit;

final class WOO_MSTORE_MULTI_INIT {

	/**
	 * License Manager Instance
	 **/
	public $license_manager = null;

	/**
	 * Instance
	 **/
	public static $_instance = null;

	/**
	 * Construct
	 */
	public function __construct() {
		if ( ! $this->is_woocomemrce_active() ) { return; }
		if ( ! $this->required_php_version() ) { return; }

		$this->define_required_constants();
		$this->includes();
		$this->hooks();

		$this->license_manager = new WC_Multistore_Licence();
	}

	/**
	 * @return WOO_MSTORE_MULTI_INIT|null
	 */
	public static function getInstance() {
		if ( self::$_instance === null ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 *
	 */
	public function define_required_constants() {
		$this->define( 'WOO_MSTORE_PATH', dirname( plugin_dir_path( __FILE__ ) ) );
		$this->define( 'WOO_MSTORE_URL', dirname( plugins_url( '', __FILE__ ) ) );
		$this->define( 'WOO_MSTORE_APP_API_URL', 'https://woomultistore.com/index.php' );

		$this->define( 'WOO_MSTORE_VERSION', '4.6.8' );
		$this->define( 'WOO_MSTORE_DB_VERSION', '1.0' );

		$this->define( 'WOO_MSTORE_PRODUCT_ID', 'WCMSTORE' );
		$this->define( 'WOO_MSTORE_INSTANCE', str_replace( array( 'https://', 'http://' ), '', network_site_url() ) );
		$this->define( 'WOO_MSTORE_ASSET_URL', dirname( plugins_url( '', __FILE__ ) ) );
	}

	/**
	 *
	 */
	public function includes(){
		require_once WOO_MSTORE_PATH . '/multisite/include/check-requirements.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-functions.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-licence.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-updater.php';

		if ( ! function_exists( 'woothemes_queue_update' ) || ! function_exists( 'is_woocommerce_active' ) ) {
			require_once WOO_MSTORE_PATH . '/woo-includes/woo-functions.php';
		}

		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-sequential-order-number.php';

		if ( defined( 'DOING_AJAX' ) ) {
			require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-ajax.php';
			new WC_Multistore_Ajax();
		}

		require_once WOO_MSTORE_PATH . '/multisite/include/class.admin.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class.admin.network-orders.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class.admin.network-products.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class.admin.product.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-product-category.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-settings.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class.admin.speed-updater.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class.admin.grouped-products.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class.admin.upsell-cross-sell.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class.admin.coupons.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class.compatibility.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-custom-taxonomy.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-network-order-refund.php';

		// export functionality
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-export.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-export-csv.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-export-xls.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-export-order.php';

		// Load reset functionality from the core multistore plugin.
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-reset.php';

		// 3rd party plugin integration
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-integration.php';

		// REST API
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-import-order.php';
		require_once WOO_MSTORE_PATH . '/multisite/include/rest-api/class-product-sync.php';

		// Bulk Sync
		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-bulk-sync.php';

		// stock sync
		require_once WOO_MSTORE_PATH . '/multisite/include/class.stock-sync.php';

		require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-multistore-image.php';

		global $WOO_MSTORE;
		$WOO_MSTORE = new WOO_MSTORE_admin();
		$WOO_MSTORE->init();

		$WOO_MSTORE_options_interface = new WC_Multistore_Settings();

		global $WOO_MSTORE_FUNCTIONS;
		$WOO_MSTORE_FUNCTIONS = new WC_Multistore_Functions();
	}

	/**
	 *
	 */
	public function hooks(){
		if ( is_admin() ) {
			add_action( 'init', array( $this, 'setup_wizard_init' ), PHP_INT_MAX, 0 );
			add_action( 'network_admin_notices', array( $this, 'network_admin_notices' ) );
			add_action( 'admin_notices', array( $this, 'admin_notices' ) );
			add_filter( 'plugin_row_meta', array( $this, 'woo_mstore_plugin_action_links' ), 10, 2 );
		} else {
			// frontend hooks.
			add_action( 'init', array( $this, 'frontend_order_interface' ), PHP_INT_MAX, 0 );
		}

		add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );
	}

	/**
	 *
	 */
	public function load_textdomain() {
		load_plugin_textdomain( 'woonet', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );
	}

	/**
	 *
	 */
	public function network_admin_notices() {
		if ( current_user_can( 'manage_woocommerce' ) ) {
			$this->setup_wizard_notice();
		}

		if ( current_user_can( 'manage_woocommerce' ) ) {
			$this->update_wizard_notice();
		}
	}

	/**
	 *
	 */
	public function admin_notices() {
		if ( current_user_can( 'manage_woocommerce' ) ) {
			$this->setup_wizard_notice();
		}
	}

	/**
	 * First time usage require a setup
	 */
	public function setup_wizard_notice() {
		if ( ! function_exists( 'is_woocommerce_active' ) ) {
			require_once WOO_MSTORE_PATH . '/woo-includes/woo-functions.php';
		}
		$setup_wizard_completed = get_site_option( 'mstore_setup_wizard_completed' );
		if ( is_multisite() && is_woocommerce_active() && empty( $setup_wizard_completed ) ) {
				include WOO_MSTORE_PATH . '/multisite/templates/html-notice-setup.php';
		}
	}

	/**
	 * Updates routines
	 */
	public function update_wizard_notice() {
		global $WOO_MSTORE;

		$current_page = isset( $_GET['page'] ) ? $_GET['page'] : '';

		if ( $WOO_MSTORE->upgrade_require === true && $current_page != 'woonet-upgrade' ) {
			include WOO_MSTORE_PATH . '/multisite/templates/html-notice-update.php';
		}

	}

	/**
	 * @param $links
	 * @param $file
	 *
	 * @return mixed
	 */
	public function woo_mstore_plugin_action_links( $links, $file ) {
		if ( strpos( $file, 'woocommerce-multistore.php' ) !== false ) {
			unset( $links[2] );
			$links[] = '<a href="' . esc_url( network_admin_url( 'settings.php?page=woo-ms-options' ) ) . '">Settings</a>';
			$links[] = '<a href="https://woomultistore.com/documentation/">Docs</a>';
			$links[] = '<a href="https://woomultistore.com/plugin-api-filters-actions/">API docs</a>';
			$links[] = '<a href="https://woomultistore.com/addons/">Addons</a>';
		}

		return $links;
	}

	/**
	 *
	 */
	public function setup_wizard_init() {
		if ( ! empty( $_GET['page'] ) ) {
			switch ( $_GET['page'] ) {
				case 'woonet-setup':
					require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-admin-setup-wizard.php';
					new WC_Admin_Setup_Wizard();
					break;
			}
		}
	}

	/**
	 *
	 */
	public function frontend_order_interface() {
		$options = WC_Multistore_Functions::get_options();

		if ( 'no' != $options['network-user-info'] ) {
			require_once WOO_MSTORE_PATH . '/multisite/include/class-wc-front-my-account.php';
			new WC_Front_My_Account();
		}
	}

	/**
	 * @return bool
	 */
	public function is_woocomemrce_active() {
		if ( ! function_exists( 'is_plugin_active' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		if ( ! is_plugin_active_for_network( 'woocommerce/woocommerce.php' ) ) {
			add_action( 'admin_notices', array( $this, 'woocommerce_inactive_admin_notice' ), 10, 0 );
			add_action( 'network_admin_notices', array( $this, 'woocommerce_inactive_admin_notice' ), 10, 0 );
			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 */
	public function required_php_version(){
		if( version_compare( phpversion(), '7.4.0', '<' ) ){
			add_action( 'admin_notices', function(){
				$class = 'notice notice-error';
				$message = __( 'Woocommerce Multistore plugin requires PHP version 7.4.0 or greater. Please update your PHP version', 'woonet' );

				printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
			} );
			add_action('network_admin_notices', function(){
				$class = 'notice notice-error';
				$message = __( 'Woocommerce Multistore plugin requires PHP version 7.4.0 or greater. Please update your PHP version', 'woonet' );

				printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
			} );

			return false;
		}

		return true;
	}

	/**
	 *
	 */
	public function woocommerce_inactive_admin_notice() {
		$class   = 'notice notice-error';
		$message = __( 'Multistore requires WooCommerce to be network activated', 'woonet' );

		printf(
			'<div class="%1$s"><p>%2$s</p></div>',
			esc_attr( $class ),
			esc_html( $message )
		);

	}

	/**
	 * Define constant if not already set.
	 *
	 * @param string      $name  Constant name.
	 * @param string|bool $value Constant value.
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}
}

function WOO_MULTISTORE() {
	return WOO_MSTORE_MULTI_INIT::getInstance();
}

$GLOBALS['WOO_MULTISTORE'] = WOO_MULTISTORE();
