<?php
/**
 * Product Category handler.
 *
 * This handles product category related functionality in Woocommerce Multistore.
 *
 */

defined( 'ABSPATH' ) || exit;

/**
 * Class WC_Multistore_Product_Category
 */
class WC_Multistore_Product_Category {
	public function __construct() {
        add_action('init', array( $this, 'init' ), 10, 0);
	}

    public function init() {
        if ( WOO_MULTISTORE()->site_manager->get_type() == 'master' ) {
            add_action( 'created_product_cat', array( $this, 'publish_category_changes' ), 10, 1 );
            add_action( 'edited_product_cat', array( $this, 'publish_category_changes' ), 20, 1 );
        } else if ( WOO_MULTISTORE()->site_manager->get_type() == 'child' ) {
            add_action( 'WOO_MSTORE_SYNC/CUSTOM/MASTER_CAT_SYNC', array( $this, 'receive_category_changes' ), 10, 1 );
        }
    }

    /**
     * Runs on master. Monitors if a category information has changed.
     *
     * @return void
     */
    public function publish_category_changes( $cat_id ) {
        if ( empty( $cat_id ) ) {
            return;
        }

        $cat_tree = WOO_MULTISTORE()->sync_engine->get_category_tree( null, $cat_id);

        // Send order details to master site.
        WOO_MULTISTORE()->sync_engine->request_child( 'woomulti_custom_payload', array(
            'payload_type' => 'MASTER_CAT_SYNC',
            'payload_contents' => $cat_tree,
        ));
    }

    /**
     * Runs on child. Receives category changes from master.
     *
     * @return void
     */
    public function receive_category_changes( $cat_data ) {
        if ( ! empty( $cat_data['payload_contents'] ) ) {
            WOO_MULTISTORE()->sync_engine->sync_product_categories( null, array(
                'categories' => $cat_data['payload_contents'],
            ), true );
        }
    }
}


new WC_Multistore_Product_Category();