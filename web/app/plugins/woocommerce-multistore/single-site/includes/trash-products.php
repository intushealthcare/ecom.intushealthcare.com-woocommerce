<?php
/**
 * Trash all linked products when a network product is trashed from the master site.
 */

defined( 'ABSPATH' ) || exit;

class WOO_MSTORE_SINGLE_TRASH_PRODUCTS {

	/**
	 * Initialize the action hooks and load the plugin classes
	 **/
	public function __construct() {
		add_action( 'init', array( $this, 'init' ), 10, 0 );
	}

	public function init() {
		$_options = get_option( 'woonet_options', array() );

		// run on master site
		if ( WOO_MULTISTORE()->site_manager->get_type() == 'master'
			&& WOO_MULTISTORE()->options_manager->get( 'synchronize-trash' ) == 'yes' ) {
			add_action( 'trashed_post', array( $this, 'trash_post' ), 10, 1 );
			add_action( 'untrashed_post', array( $this, 'untrash_post' ), 10, 1 );
			add_action( 'before_delete_post', array( $this, 'delete_post' ), 10, 2 );
			add_action( 'woomulti_scheduled_trash_untrash_delete_post', array( $this, 'run_scheduled_trash_untrash_delete_post' ), 10, 3 );
		}

		// run on child site. Look for trash post request
		add_action( 'wp_ajax_nopriv_woomulti_trash_untrash', array( $this, 'delete_post_on_child' ), 10, 0 );
	}


	/**
	 * Runs on the master. Send trash post request to the child.
	 **/
	public function trash_post( $post_id ) {
	    $post = wc_get_product($post_id);
	    if($post){
	        $sku = $post->get_sku();
        }
		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/trash_post', false ) === true ) {
			as_enqueue_async_action(
				'woomulti_scheduled_trash_untrash_delete_post',
				array(
					$post_id,
                    $sku,
					'trash',
				),
				'WooMultistore Sync Post Delete'
			);
		} else {
			$response = WOO_MULTISTORE()->sync_engine->trash_untrash_delete_post( $post_id, $sku, 'trash' );
		}
	}

	/**
	 * Runs on the master. Send untrash post request to the child.
	 **/
	public function untrash_post( $post_id ) {
	    $post = wc_get_product($post_id);
        if($post){
            $sku = $post->get_sku();
        }
		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/untrash_post', false ) === true ) {
			as_enqueue_async_action(
				'woomulti_scheduled_trash_untrash_delete_post',
				array(
					$post_id,
					$sku,
					'untrash',
				),
				'WooMultistore Sync Post Delete'
			);
		} else {
			$response = WOO_MULTISTORE()->sync_engine->trash_untrash_delete_post( $post_id, $sku, 'untrash' );
		}
	}

	/**
	 * Runs on the master. Send delete post request to the child.
	 **/
	public function delete_post( $post_id , $post ) {
	    
        $post = wc_get_product($post_id);
        
        
        if($post){
            $sku = $post->get_sku();
        }
        
        
        if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/delete_post', false ) === true ) {
			as_enqueue_async_action(
				'woomulti_scheduled_trash_untrash_delete_post',
				array(
					$post_id,
                    $sku,
					'delete',
				),
				'WooMultistore Sync Post Delete'
			);
		} else {
			$response = WOO_MULTISTORE()->sync_engine->trash_untrash_delete_post( $post_id, $sku, 'delete' );
		}
	}

	/**
	 * Runs on the child sites. Listens to the request to delete, trash or untrash a product.
	 */
	public function delete_post_on_child() {
		$_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

		if ( ! $_engine->is_request_authenticated( $_POST ) ) {
			echo json_encode(
				array(
					'status'  => 'failed',
					'message' => 'Authorization failed for ' . site_url(),
				)
			);
			die;
		}

		if ( ! empty( $_POST['parent_post_id'] ) && ! empty( $_POST['parent_post_status'] ) ) {
			$child_post_id = $_engine->get_mapped_child_post( (int) $_POST['parent_post_id'], $_POST['parent_sku'] );

			if ( ! empty( $child_post_id ) ) {

				$product = wc_get_product( $child_post_id );

				if ( $_POST['parent_post_status'] == 'delete' ) {
				 
					$product->delete( true );

				} elseif ( $_POST['parent_post_status'] == 'trash' ) {
					$product->delete();

				} elseif ( $_POST['parent_post_status'] == 'untrash' ) {
					wp_untrash_post( $product->get_id() );
				}

				// @todo: check for return value and send appropriate response.
				echo json_encode(
					array(
						'status'  => 'success',
						'message' => 'Product successfully deleted/trashed/untrashed.',
					)
				);
				die;
			}
		}
	}

	public function run_scheduled_trash_untrash_delete_post( $post_id, $sku, $action ) {
		WOO_MULTISTORE()->sync_engine->trash_untrash_delete_post( $post_id, $sku, $action );
	}
}

$GLOBALS['WOO_MSTORE_SINGLE_TRASH_PRODUCTS'] = new WOO_MSTORE_SINGLE_TRASH_PRODUCTS();
