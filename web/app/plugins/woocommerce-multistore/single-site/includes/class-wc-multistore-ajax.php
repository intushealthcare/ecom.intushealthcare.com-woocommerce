<?php
/**
 * Ajax handler.
 *
 * This handles ajax related functionality in Woocommerce Multistore.
 *
 */

defined( 'ABSPATH' ) || exit;

/**
 * Class WC_Multistore_Ajax
 */
class WC_Multistore_Ajax {
    
    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        add_action( 'init', array( $this, 'init' ), 20, 0 );
    }
    
    /**
     * init
     *
     * @return void
     */
    public function init() {
        // Run on master to send the data to child necessary for version update.
        add_action( 'wp_ajax_nopriv_send_update_data', array( $this, 'send_update_data' ) );
        add_action( 'wp_ajax_nopriv_woomulti_custom_payload', array( $this, 'woomulti_custom_payload' ) );
        
        add_action( 'wp_ajax_nopriv_woomulti_child_options_get', array( $this, 'get_options' ) );
        add_action( 'wp_ajax_nopriv_woomulti_child_options_update', array( $this, 'update_options' ) );
        
        add_action( 'wp_ajax_nopriv_woomulti_get_blognames', array( $this, 'get_blognames' ) );
        add_action( 'wp_ajax_nopriv_woomulti_get_order_exports', array( $this, 'child_order_exports' ) );
        add_action( 'wp_ajax_nopriv_woomulti_check_site_connection', array( $this, 'send_site_connection_status' ) );
        add_action( 'wp_ajax_nopriv_master_receive_updates', array( $this, 'master_receive_updates' ), 10, 0 );
        add_action( 'wp_ajax_nopriv_wc_multistore_ajax_query_attachments', array( $this, 'wc_multistore_ajax_query_attachments' ), 10, 0 );
        add_action( 'wp_ajax_nopriv_wc_multistore_ajax_get_attachment', array( $this, 'wc_multistore_ajax_get_attachment' ), 10, 0 );
        add_action( 'wp_ajax_nopriv_wc_multistore_ajax_send_attachment_to_editor', array( $this, 'wc_multistore_ajax_send_attachment_to_editor' ), 10, 0 );
        add_action( 'wp_ajax_nopriv_wc_multistore_admin_post_thumbnail_html', array( $this, 'wc_multistore_admin_post_thumbnail_html' ), 10, 0 );
        add_action( 'wp_ajax_nopriv_wc_multistore_make_content_images_responsive', array( $this, 'wc_multistore_make_content_images_responsive' ), 10, 0 );
        add_action( 'wp_ajax_nopriv_wc_multistore_post_thumbnail_html', array( $this, 'wc_multistore_post_thumbnail_html' ), 10, 0 );
    }

	public function wc_multistore_ajax_query_attachments(){
		$_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

		add_filter('wp_prepare_attachment_for_js', function ($response){
			$site_id = 1000000;
			$response['id'] = (int) ($site_id.$response['id']); // Unique ID, must be a number.
			$response['nonces']['update'] = false;
			$response['nonces']['edit'] = false;
			$response['nonces']['delete'] = false;
			$response['editLink'] = false;

			return $response;
		}, 0);

		if( is_string( $_REQUEST['data'] ) ){
			$data = json_decode( stripslashes( $_REQUEST['data'] ), true );

			if ( json_last_error() === JSON_ERROR_NONE ) {
				$_REQUEST['data'] = $data;
			}
		}

		$query = isset( $_REQUEST['data']['query'] ) ? (array) $_REQUEST['data']['query'] : array();
		$keys  = array(
			's',
			'order',
			'orderby',
			'posts_per_page',
			'paged',
			'post_mime_type',
			'post_parent',
			'author',
			'post__in',
			'post__not_in',
			'year',
			'monthnum',
		);

		foreach ( get_taxonomies_for_attachments( 'objects' ) as $t ) {
			if ( $t->query_var && isset( $query[ $t->query_var ] ) ) {
				$keys[] = $t->query_var;
			}
		}

		$query              = array_intersect_key( $query, array_flip( $keys ) );
		$query['post_type'] = 'attachment';

		if (
			MEDIA_TRASH &&
			! empty( $_REQUEST['query']['post_status'] ) &&
			'trash' === $_REQUEST['query']['post_status']
		) {
			$query['post_status'] = 'trash';
		} else {
			$query['post_status'] = 'inherit';
		}

		if ( current_user_can( get_post_type_object( 'attachment' )->cap->read_private_posts ) ) {
			$query['post_status'] .= ',private';
		}

		// Filter query clauses to include filenames.
		if ( isset( $query['s'] ) ) {
			add_filter( 'posts_clauses', '_filter_query_attachment_filenames' );
		}

		$query             = apply_filters( 'ajax_query_attachments_args', $query );
		$attachments_query = new WP_Query( $query );

		$posts       = array_map( 'wp_prepare_attachment_for_js', $attachments_query->posts );
		$posts       = array_filter( $posts );
		$total_posts = $attachments_query->found_posts;

		if ( $total_posts < 1 ) {
			// Out-of-bounds, run the query again without LIMIT for total count.
			unset( $query['paged'] );

			$count_query = new WP_Query();
			$count_query->query( $query );
			$total_posts = $count_query->found_posts;
		}

		$posts_per_page = (int) $attachments_query->get( 'posts_per_page' );

		$max_pages = $posts_per_page ? ceil( $total_posts / $posts_per_page ) : 0;

		$posts = array(
			'total_posts'  => $total_posts,
			'max_pages'    => $max_pages,
			'posts'		   => $posts
		);

		if( $_engine->is_request_authenticated( $_POST ) ){
			wp_send_json(
				array(
					'status'  => 'success',
					'result'  => $posts,
					'message' => '',
				),
				200
			);
		} else {
			echo json_encode(
				array(
					'status'  => 'error',
					'message' => 'Request is not authenticated.',
				)
			);
			woomulti_log_error( 'Child site update: Request is not authenticated.' );
			die;
		}
	}

	public function wc_multistore_ajax_get_attachment(){
		$_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

		add_filter('wp_prepare_attachment_for_js', function ($response){
			$site_id = 1000000;
			$response['id'] = (int) ($site_id.$response['id']); // Unique ID, must be a number.
			$response['nonces']['update'] = false;
			$response['nonces']['edit'] = false;
			$response['nonces']['delete'] = false;
			$response['editLink'] = false;

			return $response;
		}, 0);

		if( is_string( $_REQUEST['data'] ) ){
			$data = json_decode( stripslashes( $_REQUEST['data'] ), true );

			if ( json_last_error() === JSON_ERROR_NONE ) {
				$_REQUEST['data'] = $data;
			}
		}

		if ( ! isset( $_REQUEST['data']['id'] ) ) {
			wp_send_json_error();
		}

		$id = absint( $_REQUEST['data']['id'] );
		if ( ! $id ) {
			wp_send_json_error();
		}

		$post = get_post( $id );
		if ( ! $post ) {
			wp_send_json_error();
		}

		if ( 'attachment' !== $post->post_type ) {
			wp_send_json_error();
		}

		$attachment = wp_prepare_attachment_for_js( $id );
		if ( ! $attachment ) {
			wp_send_json_error();
		}

		if( $_engine->is_request_authenticated( $_POST ) ){
			wp_send_json(
				array(
					'status'  => 'success',
					'result'  => $attachment,
					'message' => '',
				),
				200
			);
		} else {
			echo json_encode(
				array(
					'status'  => 'error',
					'message' => 'Request is not authenticated.',
				)
			);
			woomulti_log_error( 'Child site update: Request is not authenticated.' );
			die;
		}

	}

	public function wc_multistore_ajax_send_attachment_to_editor(){
		$_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

		add_filter('mediaSendToEditor', function ( $html, $id ){
			$idPrefix = 1000000;
			$newId = $idPrefix.$id; // Unique ID, must be a number.

			$search = 'wp-image-'.$id;
			$replace = 'wp-image-'.$newId;

			return str_replace($search, $replace, $html);

		}, 10, 2);

		if( is_string( $_REQUEST['data'] ) ){
			$data = json_decode( stripslashes( $_REQUEST['data'] ), true );

			if ( json_last_error() === JSON_ERROR_NONE ) {
				$_REQUEST['data'] = $data;
			}
		}

		$attachment = wp_unslash( $_REQUEST['data']['attachment'] );

		$id = (int) $attachment['id'];

		$post = get_post( $id );

		if ( ! $post ) {
			wp_send_json_error();
		}

		if ( 'attachment' !== $post->post_type ) {
			wp_send_json_error();
		}

		// If this attachment is unattached, attach it. Primarily a back compat thing.
		$insert_into_post_id = (int) $_REQUEST['data']['post_id'];

		if ( 0 == $post->post_parent && $insert_into_post_id ) {
			wp_update_post(
				array(
					'ID'          => $id,
					'post_parent' => $insert_into_post_id,
				)
			);
		}

		$url = empty( $attachment['url'] ) ? '' : $attachment['url'];
		$rel = ( strpos( $url, 'attachment_id' ) || get_attachment_link( $id ) == $url );

		remove_filter( 'media_send_to_editor', 'image_media_send_to_editor' );
		if ( 'image' === substr( $post->post_mime_type, 0, 5 ) ) {
			$align = isset( $attachment['align'] ) ? $attachment['align'] : 'none';
			$size  = isset( $attachment['image-size'] ) ? $attachment['image-size'] : 'medium';
			$alt   = isset( $attachment['image_alt'] ) ? $attachment['image_alt'] : '';

			// No whitespace-only captions.
			$caption = isset( $attachment['post_excerpt'] ) ? $attachment['post_excerpt'] : '';
			if ( '' === trim( $caption ) ) {
				$caption = '';
			}

			$title = ''; // We no longer insert title tags into <img> tags, as they are redundant.

			$html  = get_image_send_to_editor( $id, $caption, $title, $align, $url, $rel, $size, $alt );
		} elseif ( wp_attachment_is( 'video', $post ) || wp_attachment_is( 'audio', $post ) ) {
			$html = stripslashes_deep( $_POST['html'] );
		} else {
			$html = isset( $attachment['post_title'] ) ? $attachment['post_title'] : '';
			$rel  = $rel ? ' rel="attachment wp-att-' . $id . '"' : ''; // Hard-coded string, $id is already sanitized.

			if ( ! empty( $url ) ) {
				$html = '<a href="' . esc_url( $url ) . '"' . $rel . '>' . $html . '</a>';
			}
		}

		$idPrefix = 1000000;
		$newId = $idPrefix.$id; // Unique ID, must be a number.

		$search = 'wp-image-'.$id;
		$replace = 'wp-image-'.$newId;

		$html = str_replace( $search, $replace, $html );

		/** This filter is documented in wp-admin/includes/media.php */
		$html = apply_filters( 'media_send_to_editor', $html, $id, $attachment );

		if( $_engine->is_request_authenticated( $_POST ) ){
			wp_send_json(
				array(
					'status'  => 'success',
					'result'  => $html,
					'message' => '',
				),
				200
			);
		} else {
			echo json_encode(
				array(
					'status'  => 'error',
					'message' => 'Request is not authenticated.',
				)
			);
			woomulti_log_error( 'Child site update: Request is not authenticated.' );
			die;
		}
	}

	public function wc_multistore_admin_post_thumbnail_html(){
		$_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

		if( is_string( $_REQUEST['data'] ) ){
			$data = json_decode( stripslashes( $_REQUEST['data'] ), true );

			if ( json_last_error() === JSON_ERROR_NONE ) {
				$_REQUEST['data'] = $data;
			}
		}

		$thumbnail_id   = $_REQUEST['data']['thumbnail_id'];
		$post           = $_REQUEST['data']['post'];

		$_wp_additional_image_sizes = wp_get_additional_image_sizes();

		$post               = get_post( $post );
		$post_type_object   = get_post_type_object( $post->post_type );
		$set_thumbnail_link = '<p class="hide-if-no-js"><a href="%s" id="set-post-thumbnail"%s class="thickbox">%s</a></p>';
		$upload_iframe_src  = get_upload_iframe_src( 'image', $post->ID );

		$content = sprintf(
			$set_thumbnail_link,
			esc_url( $upload_iframe_src ),
			'', // Empty when there's no featured image set, `aria-describedby` attribute otherwise.
			esc_html( $post_type_object->labels->set_featured_image )
		);

		if ( $thumbnail_id && get_post( $thumbnail_id ) ) {
			$size = isset( $_wp_additional_image_sizes['post-thumbnail'] ) ? 'post-thumbnail' : array( 266, 266 );

			$size = apply_filters( 'admin_post_thumbnail_size', $size, $thumbnail_id, $post );

			$thumbnail_html = wp_get_attachment_image( $thumbnail_id, $size );

			if ( ! empty( $thumbnail_html ) ) {
				$content  = sprintf(
					$set_thumbnail_link,
					esc_url( $upload_iframe_src ),
					' aria-describedby="set-post-thumbnail-desc"',
					$thumbnail_html
				);
				$content .= '<p class="hide-if-no-js howto" id="set-post-thumbnail-desc">' . __( 'Click the image to edit or update' ) . '</p>';
				$content .= '<p class="hide-if-no-js"><a href="#" id="remove-post-thumbnail">' . esc_html( $post_type_object->labels->remove_featured_image ) . '</a></p>';
			}
		}

		$content .= '<input type="hidden" id="_thumbnail_id" name="_thumbnail_id" value="' . esc_attr( $thumbnail_id ? $thumbnail_id : '-1' ) . '" />';

		$content = apply_filters( 'admin_post_thumbnail_html', $content, $post->ID, $thumbnail_id );

		if( $_engine->is_request_authenticated( $_POST ) ){
			wp_send_json(
				array(
					'status'  => 'success',
					'result'  => $content,
					'message' => '',
				),
				200
			);
		} else {
			echo json_encode(
				array(
					'status'  => 'error',
					'message' => 'Request is not authenticated.',
				)
			);
			woomulti_log_error( 'Child site update: Request is not authenticated.' );
			die;
		}

	}

	public function wc_multistore_make_content_images_responsive(){
		$_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

		if( is_string( $_POST['data'] ) ){
			$data = json_decode( stripslashes( $_POST['data'] ), true );

			if ( json_last_error() === JSON_ERROR_NONE ) {
				$_POST['data'] = $data;
			}
		}

		$attachment_id = (int) $_POST['data']['attachment_id'];

		if ( ! $attachment_id ) {
			$post = get_post();

			if ( ! $post ) {
				return false;
			}

			$attachment_id = $post->ID;
		}

		$data = get_post_meta( $attachment_id, '_wp_attachment_metadata', true );

		if ( ! $data ) {
			return false;
		}

		/**
		 * Filters the attachment meta data.
		 *
		 * @since 2.1.0
		 *
		 * @param array $data          Array of meta data for the given attachment.
		 * @param int   $attachment_id Attachment post ID.
		 */
		$data =  apply_filters( 'wp_get_attachment_metadata', $data, $attachment_id );

		$content = str_replace( $_POST['data']['image'], wp_image_add_srcset_and_sizes( $_POST['data']['image'], $data, $_POST['data']['attachmentId'] ), $_POST['data']['content'] );

		if ( $_engine->is_request_authenticated( $_POST ) ) {
			wp_send_json(
				array(
					'status'  => 'success',
					'result'  => $content,
					'message' => '',
				),
				200
			);
		} else {
			echo json_encode(
				array(
					'status'  => 'error',
					'message' => 'Request is not authenticated.',
				)
			);
			woomulti_log_error( 'Child site update: Request is not authenticated.' );
			die;
		}
	}

	public function wc_multistore_post_thumbnail_html(){
		$_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

		if( is_string( $_POST['data'] ) ){
			$data = json_decode( stripslashes( $_POST['data'] ), true );

			if ( json_last_error() === JSON_ERROR_NONE ) {
				$_POST['data'] = $data;
			}
		}

		$html  = '';
		$attachment_id  = $_POST['data']['attachment_id'];
		$size           = $_POST['data']['size'];
		$icon           = $_POST['data']['icon'];
		$attr           = $_POST['data']['attr'];

		$image          = wp_get_attachment_image_src( $attachment_id, $size, $icon );

		if ( $image ) {
			list( $src, $width, $height ) = $image;

			$attachment = get_post( $attachment_id );
			$hwstring   = image_hwstring( $width, $height );
			$size_class = $size;

			if ( is_array( $size_class ) ) {
				$size_class = implode( 'x', $size_class );
			}

			$default_attr = array(
				'src'   => $src,
				'class' => "attachment-$size_class size-$size_class",
				'alt'   => trim( strip_tags( get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ) ) ),
			);

			// Add `loading` attribute.
			if ( wp_lazy_loading_enabled( 'img', 'wp_get_attachment_image' ) ) {
				$default_attr['loading'] = wp_get_loading_attr_default( 'wp_get_attachment_image' );
			}

			$attr = wp_parse_args( $attr, $default_attr );

			// If the default value of `lazy` for the `loading` attribute is overridden
			// to omit the attribute for this image, ensure it is not included.
			if ( array_key_exists( 'loading', $attr ) && ! $attr['loading'] ) {
				unset( $attr['loading'] );
			}

			// Generate 'srcset' and 'sizes' if not already present.
			if ( empty( $attr['srcset'] ) ) {
				$image_meta = wp_get_attachment_metadata( $attachment_id );

				if ( is_array( $image_meta ) ) {
					$size_array = array( absint( $width ), absint( $height ) );
					$srcset     = wp_calculate_image_srcset( $size_array, $src, $image_meta, $attachment_id );
					$sizes      = wp_calculate_image_sizes( $size_array, $src, $image_meta, $attachment_id );

					if ( $srcset && ( $sizes || ! empty( $attr['sizes'] ) ) ) {
						$attr['srcset'] = $srcset;

						if ( empty( $attr['sizes'] ) ) {
							$attr['sizes'] = $sizes;
						}
					}
				}
			}

			$attr = apply_filters( 'wp_get_attachment_image_attributes', $attr, $attachment, $size );

			$attr = array_map( 'esc_attr', $attr );
			$html = rtrim( "<img $hwstring" );

			foreach ( $attr as $name => $value ) {
				$html .= " $name=" . '"' . $value . '"';
			}

			$html .= ' />';
		}

		$html = apply_filters( 'wp_get_attachment_image', $html, $attachment_id, $size, $icon, $attr );

		if ( $_engine->is_request_authenticated( $_POST ) ) {
			wp_send_json(
				array(
					'status'  => 'success',
					'result'  => $html,
					'message' => '',
				),
				200
			);
		} else {
			echo json_encode(
				array(
					'status'  => 'error',
					'message' => 'Request is not authenticated.',
				)
			);
			woomulti_log_error( 'Child site update: Request is not authenticated.' );
			die;
		}

	}

	public function send_update_data() {
        $_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

        if( is_string( $_POST['data'] ) ){
            $data = json_decode( stripslashes( $_POST['data'] ), true );

            if ( json_last_error() === JSON_ERROR_NONE ) {
                $_POST['data'] = $data;
            }
        }
        
        if ( $_engine->is_request_authenticated( $_POST ) ) {
            echo $_engine->send_data_for_update();
        } else {
            echo json_encode(
                array(
                    'status'  => 'error',
                    'message' => 'Request is not authenticated.',
                )
            );
            woomulti_log_error( 'Child site update: Request is not authenticated.' );
        }
        die;
    }
    
    public function woomulti_custom_payload() {
        $_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

        if( is_string( $_POST['data'] ) ){
            $data = json_decode( stripslashes( $_POST['data'] ), true );

            if ( json_last_error() === JSON_ERROR_NONE ) {
                $_POST['data'] = $data;
            }
        }
        
        if ( $_engine->is_request_authenticated( $_POST ) ) {
            if ( ! empty( $_POST['data']['payload_type'] ) && ! empty( $_POST['data']['payload_contents'] ) ) {
                
                // Fire the hook.
                do_action( 'WOO_MSTORE_SYNC/CUSTOM/' . sanitize_text_field( $_POST['data']['payload_type'] ), $_POST['data'] );
                
                wp_send_json(
                    array(
                        'status'  => 'succes',
                        'message' => 'Data Received.',
                    )
                );
            } else {
                woomulti_log_error( 'Custom payload empty.' );
                
                wp_send_json(
                    array(
                        'status'  => 'error',
                        'message' => 'Custom payload empty.',
                    )
                );
            }
        } else {
            woomulti_log_error( 'Child site received custom payload. But request was not authenticated.' );
            
            wp_send_json(
                array(
                    'status'  => 'error',
                    'message' => 'Request is not authenticated.',
                )
            );
        }
    }
    
    public function get_options() {
        $_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

        if( is_string( $_POST['data'] ) ){
            $data = json_decode( stripslashes( $_POST['data'] ), true );

            if ( json_last_error() === JSON_ERROR_NONE ) {
                $_POST['data'] = $data;
            }
        }
        
        if ( ! $_engine->is_request_authenticated( $_POST ) ) {
            woomulti_log_error( 'Update options requested. But request was not authenticated.' );
            
            wp_send_json(
                array(
                    'status'  => 'error',
                    'message' => 'Request is not authenticated.',
                )
            );
        }
        
        $option = get_option( 'woonet_options' );
        
        /**
         * These options are managed by master.
         * There's no need to send them back to the master site.
         * Master has these settings and sends to the child sites only for updates.
         */
        unset( $option['synchronize-stock'] );
        unset( $option['synchronize-trash'] );
        unset( $option['publish-capability'] );
        unset( $option['sequential-order-numbers'] );
        unset( $option['network-user-info'] );
        unset( $option['sync-by-sku'] );
        unset( $option['background-sync'] );
        unset( $option['disable-ajax-sync'] );
	    unset( $option['enable-global-image'] );
	    unset( $option['enable-order-import'] );
        
        if ( ! empty( $option ) ) {
            $option['blog_name'] = get_bloginfo( 'name' ); // To display on each tab of the settings panel.
        }
        
        wp_send_json(
            array(
                'status'  => 'success',
                'result'  => $option,
                'message' => '',
            ),
            200
        );
    }
    
    public function update_options() {
        $_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

        if( is_string( $_POST['data'] ) ){
            $data = json_decode( stripslashes( $_POST['data'] ), true );

            if ( json_last_error() === JSON_ERROR_NONE ) {
                $_POST['data'] = $data;
            }
        }
        
        if ( ! $_engine->is_request_authenticated( $_POST ) ) {
            woomulti_log_error( 'Update options requested. But request was not authenticated.' );
            
            wp_send_json(
                array(
                    'status'  => 'error',
                    'message' => 'Request is not authenticated for ' . get_bloginfo( 'name' ),
                )
            );
        }
        
        if ( ! empty( $_POST['data'] ) ) {
            $manager = new WC_Multistore_Functions();
            
            $connect_details = get_option( 'woonet_master_connect' );
            
            if ( ! empty( $connect_details['uuid'] )
                && ! empty( $_POST['data'][ $connect_details['uuid'] ] )
                && ! empty( $_POST['data']['master'] ) ) {
                $updated_options        = $_POST['data'][ $connect_details['uuid'] ];
                $updated_master_options = $_POST['data']['master'];
                $updated_options        = array_merge(
                    array(
                        'synchronize-stock'        => isset( $updated_master_options['synchronize-stock'] ) ? $updated_master_options['synchronize-stock'] : 'no',
                        'synchronize-trash'        => isset( $updated_master_options['synchronize-trash'] ) ? $updated_master_options['synchronize-trash'] : 'no',
                        'publish-capability'       => isset( $updated_master_options['publish-capability'] ) ? $updated_master_options['publish-capability'] : 'no',
                        'network-user-info'        => isset( $updated_master_options['network-user-info'] ) ? $updated_master_options['network-user-info'] : 'no',
                        'sync-custom-taxonomy'     => isset( $updated_master_options['sync-custom-taxonomy'] ) ? $updated_master_options['sync-custom-taxonomy'] : 'no',
                        'sync-custom-metadata'     => isset( $updated_master_options['sync-custom-metadata'] ) ? $updated_master_options['sync-custom-metadata'] : 'no',
                        'sequential-order-numbers' => isset( $updated_master_options['sequential-order-numbers'] ) ? $updated_master_options['sequential-order-numbers'] : 'no',
                        'sync-by-sku'              => isset( $updated_master_options['sync-by-sku'] ) ? $updated_master_options['sync-by-sku'] : 'no',
                        'background-sync'          => isset( $updated_master_options['background-sync'] ) ? $updated_master_options['background-sync'] : 'no',
                        'disable-ajax-sync'        => isset( $updated_master_options['disable-ajax-sync'] ) ? $updated_master_options['disable-ajax-sync'] : 'no',
                        'enable-global-image'      => isset( $updated_master_options['enable-global-image'] ) ? $updated_master_options['enable-global-image'] : 'no',
                        'enable-order-import'      => isset( $updated_master_options['enable-order-import'] ) ? $updated_master_options['enable-order-import'] : 'no',
                        'enable-coupon-sync'       => isset( $updated_master_options['enable-coupon-sync'] ) ? $updated_master_options['enable-coupon-sync'] : 'no',
                    ),
                    $updated_options
                );
                
                $manager->update( $updated_options );
                
                wp_send_json(
                    array(
                        'status'  => 'success',
                        'result'  => get_option( 'woonet_master_connect' ),
                        'message' => 'Settings updated on ' . get_bloginfo( 'name' ),
                    )
                );
            }
        }
        
        woomulti_log_error( 'Update options request failed.' );
        
        wp_send_json(
            array(
                'status'  => 'error',
                'result'  => '',
                'message' => 'Can not update options for ' . get_bloginfo( 'name' ),
            )
        );
    }
    
    /**
     * Send blogname
     *
     * @return void
     */
    public function get_blognames() {
        $_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

        if( is_string( $_POST['data'] ) ){
            $data = json_decode( stripslashes( $_POST['data'] ), true );

            if ( json_last_error() === JSON_ERROR_NONE ) {
                $_POST['data'] = $data;
            }
        }
        
        if ( ! $_engine->is_request_authenticated( $_POST ) ) {
            woomulti_log_error( 'Update options requested. But request was not authenticated.' );
            
            wp_send_json(
                array(
                    'status'  => 'error',
                    'message' => 'Request is not authenticated for ' . get_bloginfo( 'name' ),
                    'result'  => '',
                )
            );
        }
        
        wp_send_json(
            array(
                'status'  => 'success',
                'result'  => get_bloginfo( 'name' ),
                'message' => 'Successfully retrieved blogname',
            )
        );
    }
    
    /**
     * Send child order data to master site for exports function.
     */
    public function child_order_exports() {
        $_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

        if( is_string( $_POST['data'] ) ){
            $data = json_decode( stripslashes( $_POST['data'] ), true );

            if ( json_last_error() === JSON_ERROR_NONE ) {
                $_POST['data'] = $data;
            }
        }
        
        if ( ! $_engine->is_request_authenticated( $_POST ) ) {
            woomulti_log_error( 'Update options requested. But request was not authenticated.' );
            
            wp_send_json(
                array(
                    'status'  => 'error',
                    'message' => 'Request is not authenticated for ' . get_bloginfo( 'name' ),
                )
            );
        }
        
        if ( ! empty( $_POST['data'] ) ) {
	        $WC_Multistore_Export_Order = new WC_Multistore_Export_Order();

	        $_POST['export_format'] = $_POST['data']['export_format'];
	        $_POST['export_time_after'] = $_POST['data']['export_time_after'];
	        $_POST['export_time_before'] = $_POST['data']['export_time_before'];
	        $_POST['site_filter'] = $_POST['data']['site_filter'];
	        $_POST['order_status'] = $_POST['data']['order_status'];
	        $_POST['row_format'] = $_POST['data']['row_format'];
	        $_POST['export_fields'] = $_POST['data']['export_fields'];

			$WC_Multistore_Export_Order->validate_settings();

	        $orders = $WC_Multistore_Export_Order->get_orders();
			$rows = array();
	        foreach ($orders as $order){
		        if( $_POST['row_format'] == 'row_per_product'){
			        $order = wc_get_order( $order['ID'] );
			        $order_items = $order->get_items();
					if( $order_items ){
						foreach ( $order_items as $order_item ){
							$rows[] = $WC_Multistore_Export_Order->get_product_row($order, $order_item);
						}
					}
		        }else{
			        $rows[] = $WC_Multistore_Export_Order->get_order_row( $order );
		        }
	        }

            // JSON_UNESCAPED_UNICODE for chinese encoding
            wp_send_json(
                array(
                    'status'  => 'success',
                    'message' => 'Orders successfully retrieved.',
                    'result'  => $rows,
                ), null,JSON_UNESCAPED_UNICODE
            );
        } else {
            wp_send_json(
                array(
                    'status'  => 'error',
                    'message' => 'Missing parameters for ' . get_bloginfo( 'name' ),
                )
            );
        }
    }
    
    /**
     * send_site_connection_status
     *
     * @return void
     */
    public function send_site_connection_status() {
        $_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

        if( is_string( $_POST['data'] ) ){
            $data = json_decode( stripslashes( $_POST['data'] ), true );

            if ( json_last_error() === JSON_ERROR_NONE ) {
                $_POST['data'] = $data;
            }
        }
        
        if ( ! $_engine->is_request_authenticated( $_POST ) ) {
            woomulti_log_error( 'Connection status check requested, but not authenticated.' );
            
            wp_send_json(
                array(
                    'status'  => 'error',
                    'message' => 'Request is not authenticated for ' . get_bloginfo( 'name' ),
                    'result'  => '',
                )
            );
        }
        
        wp_send_json(
            array(
                'status'  => 'Success',
                'message' => 'Success',
                'result'  => array(
                    'version'    => defined( 'WOO_MSTORE_VERSION' ) ? WOO_MSTORE_VERSION : '',
                    'connection' => 'success',
                ),
            )
        );
        
    }
    
    /**
     * Receive sync request from the child site
     */
    public function master_receive_updates() {
        $_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

        if( is_string( $_POST['data'] ) ){
            $data = json_decode( stripslashes( $_POST['data'] ), true );

            if ( json_last_error() === JSON_ERROR_NONE ) {
                $_POST['data'] = $data;
            }
        }
        
        if ( ! $_engine->is_request_authenticated( $_POST ) ) {
            woomulti_log_error( 'Stock sync requested by the child. Authentication failed.' );
            
            wp_send_json(
                array(
                    'status'  => 'error',
                    'message' => 'Stock sync requested by child. Authentication failed.',
                )
            );
        }
        
        $update = false;
        
        if ( ! empty( $_POST['data']['parent_id'] ) && WOO_MULTISTORE()->options_manager->get( 'sync-by-sku', 'no' ) == 'no' ) {
            $update = true;
        }
        
        if ( ! empty( $_POST['data']['parent_sku'] ) && WOO_MULTISTORE()->options_manager->get( 'sync-by-sku', 'no' ) == 'yes' ) {
            $update = true;
        }
        
        
        if ( $update ) {
            
            $product_id = $_POST['data']['parent_id'];
            
            if( WOO_MULTISTORE()->options_manager->get( 'sync-by-sku', 'no' ) == 'yes' ){
                $product_id = wc_get_product_id_by_sku( $_POST['data']['parent_sku'] );
            }
            
            $wc_product = wc_get_product( (int) $product_id );
            $uuid       = WOO_MULTISTORE()->site_manager->get_uuid_by_key( $_POST['Authorization'] );
            
            if ( ! WOO_MULTISTORE()->sync_utils->is_stock_sync_required( $wc_product->get_id(), $uuid ) ) {
                return;
            }
            
            if ( $wc_product && $wc_product->get_id() ) {
                do_action( 'WOO_MSTORE/sync/realtime/disable' );
                
                $wc_product->set_stock_quantity( $_POST['data']['current_stock'] );
                $wc_product->set_stock_status( $_POST['data']['stock_status'] );
                $wc_product->set_manage_stock( $_POST['data']['manage_stock'] );
                
                if ( ! empty( $_POST['data']['variations'] ) ) {
                    foreach ( $_POST['data']['variations'] as $variation_data ) {
                        $variation = wc_get_product( $variation_data['parent_id'] );
                        $variation->set_stock_quantity( $variation_data['current_stock'] );
                        $variation->set_stock_status( $variation_data['stock_status'] );
                        $variation->set_manage_stock( $variation_data['manage_stock'] );
                        $variation->save();
                    }
                }
                
                $wc_product->save();
                
                WOO_MULTISTORE()->product_sync_interface->realtime_sync( (int) $product_id, $uuid, true );
                
                do_action( 'WOO_MSTORE/sync/realtime/enable' );
            }
        }
    }
}

$GLOBALS['WC_Multistore_Ajax'] = new WC_Multistore_Ajax();
