<?php
/**
 * Init coupon related hooks
 *
 * @class   WOO_MSTORE_SINGLE_COUPON_HOOKS
 * @since   4.2.0
 */

defined( 'ABSPATH' ) || exit;

class WOO_MSTORE_SINGLE_COUPON_HOOKS {

    /**
     * Hooks 
     */
    private $_hooks = array();
	
    /**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'init' ), 20, 0 );
	}

	/**
	 * init
	 *
	 * @return void
	 */
	public function init() {
        $this->_hooks = array(
            array(
                'hook'     => 'woocommerce_new_coupon',
                'callback' => array( $this, 'sync' ),
                'priority' => 10,
                'num_args' => 2,
            ), 

            array(
                'hook'     => 'woocommerce_update_coupon',
                'callback' => array( $this, 'sync' ),
                'priority' => 10,
                'num_args' => 2,
            ), 

            array(
                'hook'     => 'woocommerce_delete_coupon',
                'callback' => array( $this, 'delete' ),
                'priority' => 10,
                'num_args' => 1,
            ), 

            array(
                'hook'     => 'woocommerce_trash_coupon',
                'callback' => array( $this, 'trash' ),
                'priority' => 10,
                'num_args' => 1,
            ), 

            array(
                'hook'     => 'untrash_post',
                'callback' => array( $this, 'untrash' ),
                'priority' => 10,
                'num_args' => 1,
            ), 

            array(
                'hook'     => 'WOO_MSTORE_SYNC/CUSTOM/sync_coupon',
                'callback' => array ( $this, 'receive_sync_data' ),
                'priority' => 10,
                'num_args' => 1,
            ), 

            array(
                'hook'     => 'WOO_MSTORE_SYNC/CUSTOM/coupon_usage_count_update',
                'callback' => array ( $this, 'receive_increase_decrease_usage_count' ),
                'priority' => 10,
                'num_args' => 1,
            ),

            array(
                'hook'     => 'WOO_MSTORE_SYNC/CUSTOM/trash_coupon',
                'callback' => array ( $this, 'receive_delete_trash_untrash' ),
                'priority' => 10,
                'num_args' => 1,
            ), 

            array(
                'hook'     => 'woocommerce_increase_coupon_usage_count',
                'callback' => array ( $this, 'increase_usage_count' ),
                'priority' => 10,
                'num_args' => 3,
            ),

            array(
                'hook'     => 'woocommerce_decrease_coupon_usage_count',
                'callback' => array ( $this, 'decrease_usage_count' ),
                'priority' => 10,
                'num_args' => 3,
            ),
        );
        
        $this->toggle_hooks( 'enable' );
	}

    public function toggle_hooks( $status = 'enable' ) {
        foreach( $this->_hooks as $hook ) {
            if ( $status == 'enable' ) {
                add_action( $hook['hook'], $hook['callback'], $hook['priority'], $hook['num_args'] );
            } else if ( $status == 'disable' ) {
                remove_action( $hook['hook'], $hook['callback'], $hook['priority'], $hook['num_args'] );
            }
        }
    }

    private function is_sync_required() {
        if ( WOO_MULTISTORE()->site_manager->get_type() == 'master'
            &&  WOO_MULTISTORE()->options_manager->get( 'enable-coupon-sync' ) == 'yes' ) {
            return true;
        }

        return false;
    }

    /**
     * Run Sync between sites.
     *
     * @param array $data
     * @return void
     */
    public function sync( $id, $coupon ) {
        $this->toggle_hooks( 'disable' );

        if ( $this->is_sync_required() === false ) {
            return;
        }

        $data = $coupon->get_data();

        $data[ 'date_created' ] = method_exists( $data['date_created'], 'format' ) ? $data['date_created']->format( 'Y-m-d H:i:s' ) : null;
        $data[ 'date_modified'] = method_exists( $data['date_modified'], 'format' ) ? $data['date_modified']->format( 'Y-m-d H:i:s' ) : null;
        $data[ 'date_expires' ] = method_exists( $data['date_expires'], 'format' ) ? $data['date_expires']->format( 'Y-m-d H:i:s' ) : null;
        unset(  $data['meta_data'] );

        if ( WOO_MULTISTORE()->site_manager->get_type() == 'master' ) {
            WOO_MULTISTORE()->sync_engine->send_payload(
                array(
                    'payload_type'     => 'sync_coupon',
                    'payload_contents' => $data,
                ) );
        } else {
            // WOO_MULTISTORE()->sync_engine->send_payload_master( $data );
            WOO_MULTISTORE()->sync_engine->request_master(
                'woomulti_custom_payload',
                array(
                    'payload_type'     => 'sync_coupon',
                    'payload_contents' => $data,
                )
            );
        }

        $this->toggle_hooks( 'enable' );
    }

    public function delete_trash_untrash( $id, $action ) {
        $this->toggle_hooks( 'disable' );

        if ( WOO_MULTISTORE()->site_manager->get_type() == 'master' ) {
            WOO_MULTISTORE()->sync_engine->send_payload(
                array(
                    'payload_type'     => 'trash_coupon',
                    'payload_contents' => array(
                        'coupon' => array(
                            'id' => $id,
                        ),
                        'action' => $action,
                    )
                ) );
        }

        $this->toggle_hooks( 'enable' );
    }

    public function delete( $id ) {
        if ( WOO_MULTISTORE()->site_manager->get_type() == 'master' ) {
            $this->delete_trash_untrash( $id, 'delete' );
        }
    }

    public function trash( $id ) {
        if ( WOO_MULTISTORE()->site_manager->get_type() == 'master' ) {
            $this->delete_trash_untrash( $id, 'trash' );
        }
    }

    public function untrash( $id ) {
        if ( WOO_MULTISTORE()->site_manager->get_type() == 'master' ) {
            $this->delete_trash_untrash( $id, 'untrash' );
        }
    }

    public function receive_delete_trash_untrash( $data ) {
        if ( WOO_MULTISTORE()->site_manager->get_type() != 'child' ) {
            return;
        }

        $this->toggle_hooks( 'disable' );

        if ( ! empty( $data['payload_contents'] ) ) {
            $coupon = new WOO_MSTORE_SINGLE_COUPON(); 
            $id = $coupon->search_coupon_by_id( $data['payload_contents']['coupon']['id'] );

            if ( is_null( $id ) ) {
                return;
            }

            switch( $data['payload_contents']['action'] ) {
                case 'delete':
                    wp_delete_post( $id, true );
                    break;
                case 'trash':
                    wp_trash_post( $id );
                    break;
                case 'untrash':
                    // Check if it's a coupon as untrash_post can be fired
                    // for any post types.
                    $post_object = wp_get_post( $id );

                    if ( $post_object->post_type == 'shop_coupon' ) {
                        wp_untrash_post( $id );
                    }
                    break;
            }
        }
       
        $this->toggle_hooks( 'enable' );
    }

    public function receive_sync_data( $data ) {
        $this->toggle_hooks( 'disable' );

        if ( empty( $data['payload_contents'] ) ) {
            $this->toggle_hooks( 'enable' );
            return;
        }

        $props = $data['payload_contents'];

        if ( ! empty( $props['product_ids'] ) ) {
            $props['product_ids'] = $this->_map_objects( $props['product_ids'], 'product_ids' );
        } else {
            $props['product_ids'] = array();
        }

        if ( ! empty( $props['excluded_product_ids'] ) ) {
            $props['excluded_product_ids'] = $this->_map_objects( $props['excluded_product_ids'], 'product_ids' );
        } else {
            $props['excluded_product_ids'] = array();
        }

        if ( ! empty( $props['product_categories'] ) ) {
            $props['product_categories'] = $this->_map_objects( $props['product_categories'], 'product_cat' );
        } else {
            $props['product_categories'] = array();
        }

        if ( ! empty( $props['excluded_product_categories'] ) ) {
            $props['excluded_product_categories'] = $this->_map_objects( $props['excluded_product_categories'], 'product_cat' );
        } else {
            $props['excluded_product_categories'] = array();
        }

        $coupon = new WOO_MSTORE_SINGLE_COUPON( $props );
        $coupon->save();
        
        $this->toggle_hooks( 'enable' );
    }

    private function _map_objects( $ids, $type='product' ) {
        $_translated_ids = array();

        if ( empty( $ids ) ) {
            return $_translated_ids;
        }

        foreach( $ids as $id ) {
            switch( $type ) {
                case 'product_ids':
                    if ( $id = WOO_MULTISTORE()->sync_engine->get_mapped_child_post( $id ) ) {
                        $_translated_ids[] = $id;
                    }
                    break;
                case 'product_cat':
                    if ( $id = WOO_MULTISTORE()->sync_engine->get_mapped_child_term( $id ) ) {
                        $_translated_ids[] = $id;
                    }
                    break;
            }
        }

        return $_translated_ids;
    }

    public function increase_usage_count( $coupon, $new_count, $used_by ) {
		if( WOO_MULTISTORE()->options_manager->get( 'enable-coupon-sync' ) == 'no' ){
			return;
		}

        $this->toggle_hooks( "disable" );

	    $data = array(
            'coupon_id'   => $coupon->get_id(),
            'coupon_code' => $coupon->get_code(),
            'update_from' => WOO_MULTISTORE()->site_manager->get_type(),
            'new_count'   => $new_count,
            'site'        => get_option( 'woonet_master_connect' ),
            'used_by'     => $used_by,
            'type'        => 'increase',
        );

        if ( WOO_MULTISTORE()->site_manager->get_type() == 'master' ) {
            WOO_MULTISTORE()->sync_engine->send_payload(
                array(
                    'payload_type'     => 'coupon_usage_count_update',
                    'payload_contents' => $data,
                ) );
        } else {
            WOO_MULTISTORE()->sync_engine->request_master(
                'woomulti_custom_payload',
                array(
                    'payload_type'     => 'coupon_usage_count_update',
                    'payload_contents' => $data,
                )
            );
        }

        $this->toggle_hooks( "enable" );
    }

    public function decrease_usage_count( $coupon, $new_count, $used_by ) {
	    if( WOO_MULTISTORE()->options_manager->get( 'enable-coupon-sync' ) == 'no' ){
		    return;
	    }

	    $this->toggle_hooks( "disable" );

	    $data = array(
		    'coupon_id'   => $coupon->get_id(),
		    'coupon_code' => $coupon->get_code(),
		    'update_from' => WOO_MULTISTORE()->site_manager->get_type(),
		    'new_count'   => $new_count,
		    'site'        => get_option( 'woonet_master_connect' ),
		    'used_by'     => $used_by,
		    'type'        => 'decrease',
	    );

	    if ( WOO_MULTISTORE()->site_manager->get_type() == 'master' ) {
		    WOO_MULTISTORE()->sync_engine->send_payload(
			    array(
				    'payload_type'     => 'coupon_usage_count_update',
				    'payload_contents' => $data,
			    ) );
	    } else {
		    WOO_MULTISTORE()->sync_engine->request_master(
			    'woomulti_custom_payload',
			    array(
				    'payload_type'     => 'coupon_usage_count_update',
				    'payload_contents' => $data,
			    )
		    );
	    }

	    $this->toggle_hooks( "enable" );
    }

    public function receive_increase_decrease_usage_count( $data ) {
		$data                       = $data['payload_contents'];
		$update_from_master         = ! empty( $data['update_from'] ) && $data['update_from'] == 'master';
		$new_usage_count            = $data['new_count'];
		$coupon_code                = $data['coupon_code'];

		if( $update_from_master ){
			$child_coupon_id = wc_get_coupon_id_by_code($coupon_code);
			if( $child_coupon_id ){
				update_post_meta( $child_coupon_id, 'usage_count', $new_usage_count );
			}
		}else{
			$master_coupon_id = wc_get_coupon_id_by_code($coupon_code);
			if( $master_coupon_id ){

				update_post_meta( $master_coupon_id, 'usage_count', $new_usage_count );

				$data = array(
					'coupon_id'   => $master_coupon_id,
					'coupon_code' => $coupon_code,
					'update_from' => WOO_MULTISTORE()->site_manager->get_type(),
					'new_count'   => $new_usage_count,
					'site'        => get_option( 'woonet_master_connect' ),
					'used_by'     => $data['used_by'],
					'type'        => 'decrease',
				);

				WOO_MULTISTORE()->sync_engine->send_payload(
					array(
						'payload_type'     => 'coupon_usage_count_update',
						'payload_contents' => $data,
					)
				);
			}

		}

    }
}

$GLOBALS['WOO_MSTORE_SINGLE_COUPON_HOOKS'] = new WOO_MSTORE_SINGLE_COUPON_HOOKS();
