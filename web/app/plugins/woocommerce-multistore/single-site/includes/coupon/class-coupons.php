<?php
/**
 * @class   WOO_MSTORE_SINGLE_COUPON
 * @since   4.2.0
 */

defined( 'ABSPATH' ) || exit;

class WOO_MSTORE_SINGLE_COUPON {

    /**
     * WC_Coupon
     *
     * @param object $coupon
     */
    private $coupon = null;

    /**
     * Coupon data
     *
     * @var array
     */
    private $props = null;

    public function __construct( $props = null ) {
        $this->props = $props;
        $this->coupon = new WC_Coupon();
        
        if ( ! is_null( $props ) ) {
            $this->set_props( $props );
        }
    }

    public function set_props( $props = null ) {
        if ( is_null( $props ) ) {
            $props = $this->props;
            unset( $props['id'] );
        }

        $id = $this->maybe_link_coupon( $props );

        // Check if a mapped coupon exists.
        if ( is_null( $id ) ) {
            $props['id'] = null;
        } else {
            $props['id'] = $id;
        }
        
        $this->coupon->set_props( $props );
        $this->coupon->set_date_created( $props['date_created'] );
        $this->coupon->set_date_modified( $props['date_modified'] );
        $this->coupon->set_date_expires( $props['date_expires'] );
    }

    public function maybe_link_coupon( $coupon ) {
        if ( ! empty( $coupon['id'] ) && $id = $this->search_coupon_by_id( $coupon['id'] ) ) { 
            $this->coupon->set_id( $id );
        } else if ( ! empty( $coupon['id'] ) && $id = $this->search_coupon_by_code( $coupon['code'] ) ) {
            $this->coupon->set_id( $id );
        }

        if ( ! is_null ( $id ) ) {
            return $id;
        }

        return null;
    }

    public function search_coupon_by_code( $code ) {
        global $wpdb;
        return $wpdb->get_var( $wpdb->prepare( "SELECT id FROM $wpdb->posts WHERE post_title = %s AND post_type = 'shop_coupon' AND post_status = 'publish' ORDER BY post_date DESC LIMIT 1;", $code ) );
    }

    public function search_coupon_by_id( $id ) {
        global $wpdb;
        return $wpdb->get_var( "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key='WOONET_COUPON_MAP_{$id}'");
    }

    public function save() {
        if ( $data = $this->coupon->save() ) {
            update_post_meta( $this->coupon->get_id(),  "WOONET_COUPON_MAP_{$this->props['id']}", true );
        };
    }
}