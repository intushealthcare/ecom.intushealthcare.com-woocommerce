<?php
/**
 * Import Order handler.
 *
 * This handles import order related functionality in Woocommerce Multistore.
 *
 */

defined( 'ABSPATH' ) || exit;

use \Automattic\WooCommerce\Admin\API\Reports\Coupons\DataStore as CouponsDataStore;
use \Automattic\WooCommerce\Admin\API\Reports\Orders\Stats\DataStore as OrdersStatsDataStore;
use \Automattic\WooCommerce\Admin\API\Reports\Cache as ReportsCache;
/**
 * Class WC_Multistore_Import_Order
 **/
class WC_Multistore_Import_Order {

	/**
	 * @var array
	 */
	public array $settings;

    /**
     * initiate the action hooks and load the plugin classes
     **/
    public function __construct() {
        add_action( 'init', array( $this, 'init' ), 10, 0) ;
    }

	/**
	 *
	 */
	public function init() {
		$this->set_settings();
		$this->hooks();
    }

	/**
	 * Set order import settings
	 */
	private function set_settings() {
		$this->settings['enable-order-import'] = WOO_MULTISTORE()->options_manager->get( 'enable-order-import') == 'yes';
		$this->settings['child_inherit_changes_fields_control__import_order'] = WOO_MULTISTORE()->options_manager->get( 'child_inherit_changes_fields_control__import_order');
		$this->settings['sequential-order-numbers'] = WOO_MULTISTORE()->options_manager->get( 'sequential-order-numbers') == 'yes';
	}

	/**
	 * Load hooks
	 */
	public function hooks(){
		$this->all_hooks();
		$this->master_site_hooks();
		$this->child_site_hooks();
	}

	/**
	 * Load hooks for all types of sites( master or child )
	 */
	public function all_hooks(){
		add_action( 'pre_get_posts', array( $this, 'hide_cloned_products' ), 10, 1 );
		add_filter( 'WOO_MSTORE/network_order_query', array( $this, 'hide_imported_orders_on_network_page' ) );
	}

	/**
	 * Load master hooks
	 */
	public function master_site_hooks(){
		if( ! $this->is_master_site() ){ return; }
		add_action( 'woocommerce_update_order', array( $this, 'on_update_imported_order'), 10, 1 );
		add_action( 'woocommerce_create_order', array( $this, 'on_update_imported_order'), 10, 1 );
		add_filter( 'woocommerce_shop_order_search_fields', array( $this, 'woocommerce_shop_order_search_order_origin_id' ) );
		if ( $this->is_enabled_order_import() ) {
			add_action( 'WOO_MSTORE_SYNC/CUSTOM/MASTER_IMPORT_ORDER', array( $this, 'update_imported_order' ), 10, 1 );
			add_action( 'WOO_MSTORE_SYNC/CUSTOM/MASTER_REFUND_ORDER', array( $this, 'refund_order' ), 10, 1 );
			add_action( 'WOO_MSTORE_SYNC/CUSTOM/MASTER_DELETE_ORDER_REFUND', array( $this, 'delete_refund' ), 10, 1 );
			add_filter( 'woocommerce_order_item_get_formatted_meta_data', array( $this, 'hide_item_meta' ), 10, 2 );
			add_filter( 'manage_shop_order_posts_columns',  array( $this, 'set_custom_edit_post_columns' ), 99, 1 );
			add_action( 'manage_shop_order_posts_custom_column' , array( $this, 'print_order_originating_column' ), 99, 2 );
		}
	}

	/**
	 * Load child hooks
	 */
	public function child_site_hooks(){
		if( ! $this->is_child_site() ){	return;	}
		add_action( 'woocommerce_create_order', array( $this, 'on_update_original_order'), 10, 1 );
		add_action( 'woocommerce_update_order', array( $this, 'on_update_original_order'), 10, 1 );
		add_action( 'woocommerce_order_refunded', array( $this, 'order_refunded'), 10, 2 );
		add_filter( 'woocommerce_refund_deleted', array( $this, 'refund_deleted' ), 10, 2 );
		add_action( 'WOO_MSTORE_SYNC/CUSTOM/MASTER_UPDATE_ORIGINAL_ORDER', array( $this, 'update_original_order' ), 10, 1 );
	}

	/**
	 * @return bool
	 */
	public function is_master_site(){
		$is_master_site         = WOO_MULTISTORE()->site_manager->get_type() == 'master';
		
		if ( $is_master_site ) {
			return true;
		}

		return false;
	}


	/**
	 * @return bool
	 */
	public function is_child_site(){
		$is_child_site          = WOO_MULTISTORE()->site_manager->get_type() == 'child';
		
		if ( $this->is_enabled_order_import() && $is_child_site && $this->is_enabled_order_import_for_site() ) {
			return true;
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function is_enabled_order_import(){
		$import_order           = $this->settings['enable-order-import'];

		if ( $import_order) {
			return true;
		}

		return false;
	}

	private function is_enabled_order_import_for_site(){
		if( $this->settings['child_inherit_changes_fields_control__import_order'] == 'no' ){
			return false;
		}

		return true;
	}

	/**
	 * @return bool
	 */
	public function is_enabled_sequential_order_number(){
		$sequential_order_number  = $this->settings['sequential-order-numbers'];

		if ( $sequential_order_number ) {
			return true;
		}

		return false;
	}

	/**
	 * @param $order_id
	 * Disable emails for imported order on main site
	 * Send order data to original order
	 */
	public function on_update_imported_order( $order_id ){
		global $wpdb;
		$mapped_order = $wpdb->get_var( "SELECT meta_id FROM {$wpdb->prefix}postmeta WHERE meta_key='WOONET_PARENT_ORDER_ORIGIN_URL' AND post_id='{$order_id}'" );
		if( $mapped_order ){

			add_filter('woocommerce_email_classes', function(){
				$order_statuses = wc_get_order_statuses();

				foreach ( $order_statuses as $key => $order_status ){
					$key = str_replace('wc-', 'woocommerce_order_status_', $key );
					remove_all_actions( $key );
					foreach ( $order_statuses as $os_key => $order_s ){
						$os_key = str_replace('wc-', '', $os_key );
						$os_key = $key.'_to_'.$os_key.'_notification';
						remove_all_actions( $os_key );
					}
				}

				return [];
			}, 99, 1);

			$order_statuses = wc_get_order_statuses();

			foreach ( $order_statuses as $key => $order_status ){
				$key = str_replace('wc-', 'woocommerce_order_status_', $key );
				remove_all_actions( $key );
				foreach ( $order_statuses as $os_key => $order_s ){
					$os_key = str_replace('wc-', '', $os_key );
					$os_key = $key.'_to_'.$os_key.'_notification';
					remove_all_actions( $os_key );
				}
			}

			remove_all_actions( 'woocommerce_order_status_completed' );
			remove_all_actions('woocommerce_order_partially_refunded');
			remove_all_actions('woocommerce_order_fully_refunded');
			remove_all_actions('woocommerce_order_status_refunded_notification');
			remove_all_actions('woocommerce_order_partially_refunded_notification');
			remove_action('woocommerce_order_status_refunded', array(
				'WC_Emails',
				'send_transactional_email'
			));

			remove_action('woocommerce_order_partially_refunded', array(
				'WC_Emails',
				'send_transactional_email'
			));

			$mapped_order_string = $wpdb->get_var( "SELECT meta_key FROM {$wpdb->prefix}postmeta WHERE meta_key LIKE 'WOONET_MAP_ORDER_SID_%' AND post_id='{$order_id}'" );
			$mapped_order_array  = explode('_', $mapped_order_string);
			$mapped_order_site   = $mapped_order_array[4];
			$mapped_order_id     = $mapped_order_array[6];
			$order               = wc_get_order( $order_id );

			if( ! $mapped_order_site || ! $mapped_order_id || ! $order ){
				return $order_id;
			}

			// Format order meta data
			$order_meta          = array();
			if( ! empty( $order->get_meta_data() ) ){
				foreach ( $order->get_meta_data() as $WC_Meta_Data ) {
					$meta_data = $WC_Meta_Data->get_data();
					if( strpos( $meta_data['key'],'WOONET' ) === false ){
						$order_meta[] = $meta_data;
					}
				}
			}

			// Payload
			$order_details = array(
				'site_id' => $mapped_order_site,
				'order_id' => $mapped_order_id,
				'order_status' => $order->get_status(),
				'order_meta' => $order_meta,
			);

			// Send order status to original order
			WOO_MULTISTORE()->sync_engine->request_child( 'woomulti_custom_payload', array(
				'payload_type' => 'MASTER_UPDATE_ORIGINAL_ORDER',
				'payload_contents' => $order_details
			), $mapped_order_site );

		}

		return $order_id;
	}

	/**
	 * @param $data
	 * Updates original order when imported order updates
	 */
	public function update_original_order( $data ){
        $order_id = $data['payload_contents']['order_id'];
        $order = wc_get_order( $order_id );
        
        if( ! $order ){
            return;
        }
        
        if( ! is_a( $order, 'WC_Order') ) {
            return;
        }
        
        remove_action( 'woocommerce_update_order', array( $this, 'on_update_original_order'), 10, 1 );

	    // Set order status
        $order->set_status($data['payload_contents']['order_status']);

	    // Set order meta data
		$order_meta_data = apply_filters( 'woonet_imported_order_metadata', $data['payload_contents']['order_meta'] );
	    if( ! empty( $order_meta_data ) && is_array( $order_meta_data ) ){
			foreach ( $order_meta_data  as $meta_data ){
				if( $meta_data['key'] == '_order_number' && $this->is_enabled_sequential_order_number() ){
					continue;
				}
				update_post_meta( $order->get_id(), $meta_data['key'] ,  $meta_data['value'] );
			}
		}
		
        $order->save();
        
        add_action( 'woocommerce_update_order', array( $this, 'on_update_original_order'), 10, 1 );
    }

	/**
	 * @param $order_id
	 * Collects order data for import
	 *
	 * @throws WC_Data_Exception
	 */
	public function on_update_original_order( $order_id ) {

		if( ! $this->should_import_order( $order_id ) ){
			return $order_id;
		}

		$this->import_order( $this->get_original_order_data( $order_id ) );
        
        return $order_id;
    }

	/**
	 * @param $order_data
	 *
	 * @throws WC_Data_Exception
	 */
	public function update_imported_order( $order_data ) {
		$order_data = $order_data['payload_contents'];

		$this->disable_emails();

		remove_all_actions('woocommerce_update_product');

		$imported_order = $this->get_imported_order( $order_data );

		$this->import_tax_class();
		$this->import_tax_rates( $order_data );

		$new_tax_items_rate_id = $this->add_tax_items( $order_data, $imported_order );

		$this->add_shipping_items( $order_data, $new_tax_items_rate_id, $imported_order );
		$this->add_coupon_items( $order_data, $imported_order );
		$this->add_fee_items( $order_data, $new_tax_items_rate_id, $imported_order );
		$this->add_order_items( $order_data, $new_tax_items_rate_id, $imported_order );

		// Add order shipping
		$imported_order->set_address( $order_data['order_data']['billing'] , 'billing' );
		$imported_order->set_address( $order_data['order_data']['shipping'], 'shipping' );

		$imported_order->update_status( $order_data['order_data']['status'], 'Imported order via API from child site.', TRUE);

		$imported_order->set_currency( $order_data['order_data']['currency'] );
		$imported_order->set_prices_include_tax( $order_data['order_data']['prices_include_tax'] );
		$imported_order->set_discount_total( $order_data['order_data']['discount_total'] );
		$imported_order->set_discount_tax( $order_data['order_data']['discount_tax'] );
		$imported_order->set_shipping_total( $order_data['order_data']['shipping_total'] );
		$imported_order->set_shipping_tax( $order_data['order_data']['shipping_tax'] );
		$imported_order->set_cart_tax( $order_data['order_data']['cart_tax'] );
		$imported_order->set_total( $order_data['order_data']['total'] );

		// Add order customer details.
		if( apply_filters('wc_multistore_import_order_customer', true ) ) {
			$customer_id = $this->add_update_customer( $order_data['customer_data'] );
			$imported_order->set_customer_id( $customer_id );
		}

		// Add order payment methods.
		$imported_order->set_payment_method( $order_data['order_data']['payment_method'] );
		$imported_order->set_payment_method_title( $order_data['order_data']['payment_method_title'] );
		$imported_order->set_transaction_id( $order_data['order_data']['transaction_id'] );
		$imported_order->set_customer_ip_address( $order_data['order_data']['customer_ip_address'] );
		$imported_order->set_customer_user_agent( $order_data['order_data']['customer_user_agent'] );
		$imported_order->set_created_via( $order_data['order_data']['created_via'] );
		$imported_order->set_customer_note( $order_data['order_data']['customer_note'] );
		$imported_order->set_date_paid( $order_data['order_data']['date_paid'] );

		if ( $imported_order->save() ) {
			$this->remove_order_meta( $imported_order );
			$this->add_order_meta( $order_data, $imported_order );
			$this->add_woonet_order_meta( $imported_order, $order_data );

			if ( empty( $this->get_mapped_order_id( $order_data ) ) ) {
				wc_reduce_stock_levels( $imported_order->get_id() );
			}

			// Analytics data update
			OrdersStatsDataStore::sync_order( $imported_order->get_id() );
			CouponsDataStore::sync_order_coupons( $imported_order->get_id() );
			ReportsCache::invalidate();

		}
    }


	/**
	 * @param $original_product
	 * @param $original_site_id
	 * Imports the products from imported order that are not synced
	 * @throws WC_Data_Exception
	 */
    public function clone_product( $original_product, $original_site_id ){
        global $wpdb;
        $original_item_id   = $original_product['unsynced_product']['product']['ID'];
        $secondary_site_id  = $original_site_id;
        $product_type       = $original_product['unsynced_product']['product_type'];
        
        if( $cloned_product_id = $wpdb->get_var( "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key='_is_clone_of_id_{$original_item_id}_sid_{$secondary_site_id}'" ) ){
            $cloned_product = wc_get_product( $cloned_product_id );
        }else{
            //create new product if not exists
            switch ( $product_type ) {
                case 'simple':
                    $cloned_product = new WC_Product_Simple();
                    break;
                
                case 'variable':
                    $cloned_product = new WC_Product_Variable();
                    break;
                
                case 'external':
                    $cloned_product = new WC_Product_External();
                    break;
                
                case 'booking':
                    // If class 'WC_Product_Booking' exist then this class is defined into the Woocommerce Booking Plugin.
                    if ( class_exists( 'WC_Product_Booking' ) ) {
                        $cloned_product = new WC_Product_Booking();
                    } else {
                        $cloned_product = new WC_Product();
                    }
                    break;
                
                default :
                    $cloned_product = new WC_Product();
                    break;
            }
        }
        
        //set product data
        $cloned_product->set_name( 'Cloned '.$original_product['unsynced_product']['product']['post_title'] );
        $cloned_product->set_status( 'private' );
        $cloned_product->set_description( $original_product['unsynced_product']['product']['post_content'] );
        if( ! wc_get_product_id_by_sku( $original_product['unsynced_product']['product']['sku'] ) ){
            $cloned_product->set_sku( $original_product['unsynced_product']['product']['sku'] );
        }else{
            $logger = wc_get_logger();
            $message = 'Duplicate sku found:'. $original_product['unsynced_product']['product']['sku'] . ' for parent product id: ' . $original_product['unsynced_product']['product']['ID'];
            $logger->add('woocommerce-multistore', $message );
        }
        $cloned_product->set_price( $original_product['unsynced_product']['meta']['_price'] );
        $cloned_product->set_regular_price( $original_product['unsynced_product']['meta']['_regular_price'] );
        
        if( $original_product['unsynced_product']['meta']['_sale_price'] ){
            $cloned_product->set_sale_price( $original_product['unsynced_product']['meta']['_sale_price'] );
        }
        
        $cloned_product->update_meta_data('_is_clone_of_id_' . $original_product['unsynced_product']['product']['ID'] .'_sid_' . $secondary_site_id , 'yes' );
        $cloned_product->update_meta_data('_woonet_is_clone', 'yes' );
        $cloned_product->save();
        
        if( $product_type == 'variable' ){
            if( $original_product['unsynced_product']['product_variations'] ){
                foreach ( $original_product['unsynced_product']['product_variations'] as $original_variation ){
                    if( $cloned_variation_id = $wpdb->get_var( "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key='_woonet_cloned_variation_id_{$original_variation['product']['ID']}_sid_{$secondary_site_id}'" ) ){
                        $variation = wc_get_product($cloned_variation_id);
                    }else{
                        $variation = new WC_Product_Variation();
                    }
                    
                    $variation->set_parent_id( $cloned_product->get_id() );
                    $variation->set_name('Clone '.$original_variation['product']['post_title']);
                    $variation->set_status('private');
                    if( ! wc_get_product_id_by_sku( $original_variation['sku'] ) ){
                        $variation->set_sku( $original_variation['sku'] );
                    }else{
                        $logger = wc_get_logger();
                        $message = 'Duplicate sku found:'.$original_variation['sku'] . ' for variation parent id: ' . $original_variation['product']['ID'];
                        $logger->add('woocommerce-multistore', $message );
                    }
                    $variation->set_price($original_variation['meta']['_price'][0]);
                    $variation->set_regular_price($original_variation['meta']['_regular_price'][0]);
                    $variation->set_sale_price($original_variation['meta']['_sale_price'][0]);
                    $variation->set_attributes($original_variation['attributes']);
                    $variation->save();
                    
                    foreach ( $original_variation['meta'] as $key => $original_variation_value ){
                        if ( strpos( $key, 'attribute_pa_' ) === 0){
                            update_post_meta( $variation->get_id(), $key, $original_variation_value[0] );
                        }
                    }
                    
                    update_post_meta( $variation->get_id(), '_woonet_cloned_variation_id_' . $original_variation['product']['ID'] .'_sid_' . $secondary_site_id, 'yes' );
                    
                    // Set variation image
                    if( ! $cloned_variation_image_id = $wpdb->get_var( "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key='_woonet_cloned_attachment_id_{$original_variation['variation_image']['ID']}_sid_{$secondary_site_id}'" ) ){
                        // create new image and set it as product thumbnail
                        $cloned_variation_image_id = media_sideload_image( trim( $original_variation['variation_image']['image_src'] ), $variation->get_id(), null, 'id' );
                        
                        if ( ! empty( $cloned_variation_image_id ) && ! is_wp_error( $cloned_variation_image_id ) ) {
                            set_post_thumbnail( $variation->get_id(), $cloned_variation_image_id );
                            update_post_meta( $cloned_variation_image_id, '_woonet_cloned_attachment_id_' . $original_variation['variation_image']['ID'] .'_sid_' . $secondary_site_id, 'yes' );
                        } else {
                            error_log( $cloned_variation_image_id->get_error_message() . ' Supplied URL: ' . $original_variation['variation_image']['image_src'] );
                        }
                    }else{
                        set_post_thumbnail( $variation->get_id(), $cloned_variation_image_id );
                    }
                    
                    $cloned_product->save();
                }
            }
        }
        
        // Set product image
        if( ! $cloned_image_id = $wpdb->get_var( "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key='_woonet_cloned_attachment_id_{$original_product['unsynced_product']['product_image']['ID']}_sid_{$secondary_site_id}'" ) ){
            // create new image and set it as product thumbnail
            $cloned_image_id = media_sideload_image( trim( $original_product['unsynced_product']['product_image']['image_src'] ), $cloned_product->get_id(), null, 'id' );
            
            if ( ! empty( $cloned_image_id ) && ! is_wp_error( $cloned_image_id ) ) {
                set_post_thumbnail( $cloned_product->get_id(), $cloned_image_id );
                update_post_meta( $cloned_image_id, '_woonet_cloned_attachment_id_' . $original_product['unsynced_product']['product_image']['ID'] .'_sid_' . $secondary_site_id, 'yes' );
            } else {
                error_log( $cloned_image_id->get_error_message() . ' Supplied URL: ' . $original_product['unsynced_product']['product_image']['image_src'] );
            }
        }else{
            set_post_thumbnail( $cloned_product->get_id(), $cloned_image_id );
        }
        
        return $cloned_product->get_id();
    }

	/**
	 * @param $child_product_id
	 * @param $product
	 * Imports terms that are not synced
	 */
	public function clone_terms( $child_product_id, $product ){
		if( empty( $product ) || ! $attributes = $product['product_attributes'] ){
			return;
		}

		$product_attributes_array = array();
		$_product_attributes = array();

		foreach ( $attributes as $attr ) {
			// process taxonomy
			if ( ! empty( $attr['taxonomy'] ) ) {
				$id = wc_attribute_taxonomy_id_by_name( $attr['name'] ); //in effect its similar to by_slug

				if ( ! $id ) {
					$id = wc_create_attribute(
						array(
							'name'  => $attr['taxonomy']->attribute_label,
							'label' => $attr['taxonomy']->attribute_label,
							'slug'  => $attr['name'],
							'type'  => 'select',
						)
					);
				}

				/**
				 * If taxonomy slug on the child is different from the master,
				 * call to term_exists will fail and terms will not be added correctly.
				 * So, we get the taxonomy name on the child by the taxonomy ID.
				 */
				$_tax_name = $attr['name'];

				// If taxonomy doesn't exists we create it
				if ( ! taxonomy_exists( $_tax_name ) ) {
					register_taxonomy(
						$_tax_name,
						'product_variation',
						array(
							'hierarchical' => false,
							'label'        => ucfirst( $attr['taxonomy']->attribute_label ),
							'query_var'    => true,
							'rewrite'      => array( 'slug' => sanitize_title( $attr['name'] ) ), // The base slug
						)
					);
				}

				if ( ! is_wp_error( $id ) ) {
					$post_terms_to_add = array();

					foreach ( $attr['terms'] as $term ) {
						if ( ! term_exists( $term['name'], $_tax_name ) ) {
							$term_id = wp_insert_term( $term['name'], $_tax_name, array(
								//'slug' => $term['slug'],
							));
						}

						if ( ! array_key_exists( $term['slug'], $product_attributes_array ) ) {
							// fetch the term again to get its slug
							$_trm = get_term_by( 'name', $term['name'], $_tax_name );

							if ( $_trm->slug ) {
								$product_attributes_array[ $term['slug'] ] = $_trm->slug;
							}
						}

						$post_terms_to_add[] = $term['name'];
					}

					$set_terms = wp_set_object_terms( $child_product_id, $post_terms_to_add, $_tax_name, false );

					$_product_attributes[$_tax_name] = array(
						'name' => $_tax_name,
						'value' => '',
						'is_visible' => '0',
						'is_taxonomy' => '1',
						'is_variation' => wc_string_to_bool( $attr['variation'] )
					);

				}
			}
		}

		update_post_meta( $child_product_id, '_product_attributes', $_product_attributes );

	}

	/**
	 * @param $data
	 * @param $site_name
	 * Imports coupons that are not synced
	 * @return WC_Coupon
	 */
	public function clone_coupon( $data, $site_name ){
        if( $this->coupon_exists( $data['code'] ) ){
            return new WC_Coupon( $data['code'] );
        }
        
        $coupon = new WC_Coupon( $site_name.' '.$data['code'] );
        $coupon->set_code( $site_name.' '.$data['code'] );
        $coupon->set_amount($data['discount']);
        if( $data['meta_data'] ){
            $coupon->set_discount_type( $data['meta_data']['discount_type'] );
            $coupon->set_date_expires( strtotime( $data['meta_data']['date_expires']->date ) );
            $coupon->set_description( $data['meta_data']['description'] );
        }
        $coupon->save();
        
        return $coupon;
    }

	/**
	 * @param $coupon_code
	 * Checks if coupon exists
	 * @return bool
	 */
	public function coupon_exists( $coupon_code ) {
        global $wpdb;
        $sql = $wpdb->prepare( "SELECT post_name FROM $wpdb->posts WHERE post_type = 'shop_coupon' AND post_name = '%s'", $coupon_code );
        $coupon_codes = $wpdb->get_results($sql);
        if (count($coupon_codes)> 0) {
            return true;
        }
        else {
            return false;
        }
    }

	/**
	 * @param $customer
	 * Imports or updates customer info for imported order
	 * @return mixed
	 */
	public function add_update_customer( $customer ) {
		global $wpdb;

		$site_id            = $_POST['data']['payload_contents']['site_uuid'];
		$customer_id        = $customer['ID'];
		$mapp_id            = "WOONET_UMAP_SID{$site_id}_CID_{$customer_id}";
		$mapped_customer    = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}usermeta WHERE meta_key='{$mapp_id}'", OBJECT );

		if ( ! empty( $mapped_customer->user_id ) ) {
			$id = $mapped_customer->user_id;
		} else {
			// Create customer.
			$id = wp_create_user( 'childsite_' . $customer['user_nicename'], 'childsite_' . $customer['user_nicename'], 'childsite_' . '+' . $customer['user_email'] );

			if ( $id ) {
				update_user_meta( $id, $mapp_id, true );
			}
		}

		if ( ! empty($id) && ! empty( $_POST['data']['payload_contents']['customer_meta_data'] ) ) {
			// update all metadata.
			foreach( $_POST['data']['payload_contents']['customer_meta_data'] as $key => $value ) {
				update_user_meta( $id, $key, $value[0] );
			}
		}

		return $id;
	}

	/**
	 * @param $refund_id
	 * @param $order_id
	 */
	public function refund_deleted( $refund_id, $order_id ){
		$order_details = array(
			'order_id'  => $order_id,
			'refund_id' => $refund_id,
			'site_id'   => WOO_MULTISTORE()->site_manager->get_master_uuid()
		);

		// Send order details to master site.
		WOO_MULTISTORE()->sync_engine->request_master( 'woomulti_custom_payload', array(
			'payload_type' => 'MASTER_DELETE_ORDER_REFUND',
			'payload_contents' => $order_details
		) );
	}

	/**
	 * @param $order_details
	 */
	public function delete_refund( $order_details ){
		global $wpdb;
		$order_details      = $order_details['payload_contents'];
		$site_id            = $order_details['site_id'];
		$refund_id          = $order_details['refund_id'];
		$order_id           = $order_details['order_id'];
		$child_refund_id    = $wpdb->get_var( "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key='_woonet_refund_id_{$refund_id}_sid_{$site_id}'" );
		$child_refund       = wc_get_order( $child_refund_id );

		if( $child_refund ){
			$child_refund->delete(true );
		}
	}

	/**
	 * @param $order_id
	 * @param $refund_id
	 * Prepares refund data and sends it for update
	 *
	 * @throws Exception
	 */
    public function order_refunded( $order_id, $refund_id ){
        $order = wc_get_order( $order_id );
        
        if ( empty( $order) ) {
            return;
        }
        
        // If it's something else, we don't want that.
        if( ! is_a( $order, 'WC_Order') ) {
            return;
        }
        
        $order_details = array(
            'site_uuid'         => WOO_MULTISTORE()->site_manager->get_master_uuid(),
            'refund_id'         => $refund_id,
            'order_data'        => $order->get_data(),
            'order_site_name'   => get_bloginfo( 'name' ),
        );
        
        $refunded_order         = wc_get_order( $refund_id );
        $refund_amount          = isset( $_POST['refund_amount'] ) ? wc_format_decimal( sanitize_text_field( wp_unslash( $_POST['refund_amount'] ) ), wc_get_price_decimals() ) : 0;
        $refund_reason          = isset( $_POST['refund_reason'] ) ? sanitize_text_field( wp_unslash( $_POST['refund_reason'] ) ) : '';
        $line_item_qtys         = isset( $_POST['line_item_qtys'] ) ? json_decode( sanitize_text_field( wp_unslash( $_POST['line_item_qtys'] ) ), true ) : array();
        $line_item_totals       = isset( $_POST['line_item_totals'] ) ? json_decode( sanitize_text_field( wp_unslash( $_POST['line_item_totals'] ) ), true ) : array();
        $line_item_tax_totals   = isset( $_POST['line_item_tax_totals'] ) ? json_decode( sanitize_text_field( wp_unslash( $_POST['line_item_tax_totals'] ) ), true ) : array();
        
        // Prepare line items which we are refunding.
        $line_items = array();
        
        // For full programaticaly refunded orders we don't have $_POST
        if( $refunded_order->get_amount() ==  $order->get_total() && ! $_POST['refund_amount'] ){
            
            if ( $items = $order->get_items( array( 'line_item', 'fee', 'shipping' ) ) ) {
                foreach ( $items as $item_id => $item ) {
                    $line_total = $order->get_line_total( $item, false, false );
                    $qty        = $item->get_quantity();
                    $tax_data   = wc_get_order_item_meta( $item_id, '_line_tax_data' );
                    
                    $refund_tax = array();
                    
                    // Check if it's shipping costs. If so, get shipping taxes.
                    if ( $item instanceof \WC_Order_Item_Shipping ) {
                        $tax_data = wc_get_order_item_meta( $item_id, 'taxes' );
                    }
                    
                    // If taxdata is set, format as decimal.
                    if ( ! empty( $tax_data['total'] ) ) {
                        $refund_tax = array_filter( array_map( 'wc_format_decimal', $tax_data['total'] ) );
                    }
                    
                    // Calculate line total, including tax.
                    $line_total_inc_tax = wc_format_decimal( $line_total ) + ( is_numeric( reset( $refund_tax ) ) ? wc_format_decimal( reset( $refund_tax ) ) : 0 );
                    
                    // Add the total for this line tot the grand total.
                    $refund_amount = wc_format_decimal( $refund_amount ) + round( $line_total_inc_tax, 2 );
                    
                    // Fill item per line.
                    $line_items[ $item_id ] = array(
                        'qty'          => $qty,
                        'refund_total' => wc_format_decimal( $line_total ),
                        'refund_tax'   => array_map( 'wc_round_tax_total', $refund_tax )
                    );
                    
                    // Add taxes data for sync
                    $taxes_data = array();
                    if( $line_items[ $item_id ]['refund_tax'] ){
                        foreach ( $line_items[ $item_id ]['refund_tax']  as $tax_id => $tax ){
                            $taxes_data[] = WC_Tax::_get_tax_rate( $tax_id, ARRAY_A );
                        }
                    }
                    $line_items[ $item_id ]['tax_data'] = $taxes_data;
                }
            }
            
        }else{
            $item_ids   = array_unique( array_merge( array_keys( $line_item_qtys ), array_keys( $line_item_totals ) ) );
            
            foreach ( $item_ids as $item_id ) {
                $line_items[ $item_id ] = array(
                    'qty'          => 0,
                    'refund_total' => 0,
                    'refund_tax'   => array(),
                );
            }
            foreach ( $line_item_qtys as $item_id => $qty ) {
                $line_items[ $item_id ]['qty'] = max( $qty, 0 );
            }
            foreach ( $line_item_totals as $item_id => $total ) {
                $line_items[ $item_id ]['refund_total'] = wc_format_decimal( $total );
            }
            foreach ( $line_item_tax_totals as $item_id => $tax_totals ) {
                $line_items[ $item_id ]['refund_tax'] = array_filter( array_map( 'wc_format_decimal', $tax_totals ) );
                $taxes_data = array();
                if( $line_items[ $item_id ]['refund_tax'] ){
                    foreach ( $line_items[ $item_id ]['refund_tax']  as $tax_id => $tax ){
                        $taxes_data[] = WC_Tax::_get_tax_rate( $tax_id, ARRAY_A );
                    }
                }
                $line_items[ $item_id ]['tax_data'] = $taxes_data;
            }
        }
        
        $order_details['order_data']['line_items'] = $line_items;
        $order_details['order_data']['refund_amount'] = $refund_amount;
        $order_details['order_data']['refund_reason'] = $refund_reason;
        
        // Send order details to master site.
        WOO_MULTISTORE()->sync_engine->request_master( 'woomulti_custom_payload', array(
            'payload_type' => 'MASTER_REFUND_ORDER',
            'payload_contents' => $order_details
        ) );
    }

	/**
	 * @param $order_data
	 * Sets the imported order refund data
	 */
	public function refund_order( $order_data ){
        global $wpdb;
        
        $site_id                = $order_data['payload_contents']['site_uuid'];
        $order                  = $order_data['payload_contents']['order_data'];
        $order_site_name        = $order_data['payload_contents']['order_site_name'];
        $order_refund_id        = $order_data['payload_contents']['refund_id'];
        $original_order_id      = $order['id'];
        $refund_amount          = wc_format_decimal( $order['refund_amount'] );
        $order_id               = $wpdb->get_var( "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key='WOONET_MAP_ORDER_SID_{$site_id}_OID_{$original_order_id}'" );
        $refund_reason          = $order['refund_reason'];
        $items                  = array();
        $this->disable_emails();
        
        foreach ( $order['line_items'] as $key => $line_item ){
            $cloned_line_item_id = $wpdb->get_var( "SELECT order_item_id FROM {$wpdb->prefix}woocommerce_order_itemmeta WHERE meta_key='_woonet_line_item_site_{$site_id}_id_{$key}'" );
            $tax_rate_name = '';
            $tax_rate_id   = 0;
            $refund_tax_data = array();
            
            if( $line_item['refund_tax'] ){
                foreach( $line_item['refund_tax'] as $refund_tax_key => $refund_item_tax ){
                    if( $line_item['tax_data'] ){
                        foreach ( $line_item['tax_data'] as $line_item_tax_data){
                            if( $line_item_tax_data['tax_rate_id'] == $refund_tax_key ){
                                
                                $tax_rate_name              = $line_item_tax_data['tax_rate_name'];
                                $tax_rate_name              = $order_site_name . '_' . $tax_rate_name . '_' . $line_item_tax_data['tax_rate_id'];
                                $tax_rate_exists            = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}woocommerce_tax_rates WHERE tax_rate_name='{$tax_rate_name}'", OBJECT );
                                
                                if( $tax_rate_exists ){
                                    $tax_rate_id = $tax_rate_exists->tax_rate_id;
                                    $refund_tax_data[ $tax_rate_id ] = $refund_item_tax;
                                }
                                
                            }
                        }
                    }
                }
            }
            
            $line_item[ 'refund_tax' ]      = $refund_tax_data;
            $items[ $cloned_line_item_id ]  = $line_item;
            
        }
        
        $refund = wc_create_refund( array(
            'amount'         => $refund_amount,
            'reason'         => $refund_reason,
            'order_id'       => $order_id,
            'line_items'     => $items,
            'refund_payment' => false
        ));

        if( ! is_wp_error( $refund ) ){
            update_post_meta( $refund->get_id(), '_woonet_refund_id_' . $order_refund_id .'_sid_' . $site_id , 'yes' );
        }
    }

	/**
	 * @param $product_id
	 * Formats the products that are not synced, before importing them
	 */
	public function product_to_json( $product_id ) {
		$wc_product = wc_get_product( $product_id );

		if( ! $wc_product ){
			return;
		}

		$product = array(
			'_woomulti_version'        => defined( 'WOO_MSTORE_VERSION' ) ? WOO_MSTORE_VERSION : '',
			'_woomulti_sync_init_time' => time(),
		);

		$product['product'] = array(
			'ID'                    => $wc_product->get_id(),
			'post_content'          => $wc_product->get_description(),
			'post_title'            => $wc_product->get_name(),
			'post_name'             => $wc_product->get_slug(),
			'post_parent'           => $wc_product->get_parent_id(),
			'post_type'             => 'product',
			'product_type'          => $wc_product->get_type(),
			'sku'                   => $wc_product->get_sku(),
		);

		$product['product_type'] = $wc_product->get_type();

		$product['product_image'] = array(
			'image_src'  => wp_get_attachment_url( get_post_thumbnail_id( $product_id ) ),
			'ID' =>  get_post_thumbnail_id( $product_id ),
		);

		$product['meta'] = array();

		$_meta = get_post_meta( $wc_product->get_id() );

		foreach ( $_meta as $key => $value ) {
			if( $key == '_price' || $key == '_sale_price' || $key == '_regular_price'){
				if ( is_array( $value ) ) {
					$product['meta'][ $key ] = maybe_unserialize( $value[0] );
				} else {
					$product['meta'][ $key ] = maybe_unserialize( $value );
				}
			}
		}

		if ( $product_attributes = $wc_product->get_attributes() ) {

			$product['product_attributes'] = array();

			foreach ( $product_attributes as $pa ) {
				$terms       = $pa->get_terms();
				$terms_array = array();

				if ( ! empty( $terms ) ) {
					foreach ( $terms as $term ) {
						$terms_array[] = (array) $term;
					}
				}

				$attr = array(
					'id'       => $pa->get_id(),
					'name'     => $pa->get_name(),
					'slug'     => $pa->get_name(), // name is slug
					'options'  => $pa->get_options(),
					'terms'    => $terms_array,
					'taxonomy' => $pa->get_taxonomy_object(),
					'variation' => $pa->get_variation(),
				);

				$product['product_attributes'][] = $attr;
			}

		}

		if ( $wc_product->get_type() == 'variable' ) {

			$product['product_variations'] = array();

			$variations = $wc_product->get_available_variations();
			$variations = wp_list_pluck( $variations, 'variation_id' );

			foreach ( $variations as $variation ) {
				$wc_variation  = wc_get_product( $variation );
				$shipping_data = null;

				if ( $wc_variation->get_shipping_class() ) {
					$shipping_class = wp_get_post_terms( $variation, 'product_shipping_class' );

					if ( ! empty( $shipping_class[0]->term_id ) ) {
						$shipping_data = array(
							'id'          => $shipping_class[0]->term_id,
							'name'        => $shipping_class[0]->name,
							'slug'        => $shipping_class[0]->slug,
							'description' => $shipping_class[0]->name,
						);
					}
				}

				$thumb_id = get_post_thumbnail_id( $wc_variation->get_id() );

				if ( ! empty( $thumb_id ) ) {
					$variation_image = array(
						'image_src'  => wp_get_attachment_url( $thumb_id ),
						'ID'         => $thumb_id,
					);
				} else {
					$variation_image = false;
				}

				$variation_meta = array(
					'_regular_price'    => get_post_meta( $variation, '_regular_price', false ),
					'_price'            => get_post_meta( $variation, '_price', false ),
					'_sale_price'       => get_post_meta( $variation, '_sale_price', false )
				);

				$product['product_variations'][] = array(
					'product'         => get_post( $variation ),
					'meta'            => $variation_meta,
					'shipping_class'  => isset( $shipping_data ) ? $shipping_data : array(),
					'stock_status'    => $wc_variation->get_stock_status(),
					'manage_stock'    => $wc_variation->get_manage_stock(),
					'stock_quantity'  => $wc_variation->get_stock_quantity(),
					'backorders'      => $wc_variation->get_backorders(),
					'attributes'      => $wc_variation->get_attributes(),
					'low_stock'       => $wc_variation->get_low_stock_amount(),
					'sku'             => ! empty( $wc_product->get_sku() ) && $wc_product->get_sku() == $wc_variation->get_sku() ? '' : $wc_variation->get_sku(),
					'variation_image' => $variation_image,
				);
			}
		}

		return  $product;
	}

	/**
	 * Disable emails being sent for cloned orders
	 */
	public function disable_emails() {

		add_filter('woocommerce_email_classes', function(){
			$order_statuses = wc_get_order_statuses();

			foreach ( $order_statuses as $key => $order_status ){
				$key = str_replace('wc-', 'woocommerce_order_status_', $key );
				remove_all_actions( $key );
				foreach ( $order_statuses as $os_key => $order_s ){
					$os_key = str_replace('wc-', '', $os_key );
					$os_key = $key.'_to_'.$os_key.'_notification';
					remove_all_actions( $os_key );
				}
			}

			return [];
		}, 99, 1);

		remove_all_actions('woocommerce_order_partially_refunded');
		remove_all_actions('woocommerce_order_fully_refunded');
		remove_all_actions('woocommerce_order_status_refunded_notification');
		remove_all_actions('woocommerce_order_partially_refunded_notification');
		remove_action('woocommerce_order_status_refunded', array(
			'WC_Emails',
			'send_transactional_email'
		));

		remove_action('woocommerce_order_partially_refunded', array(
			'WC_Emails',
			'send_transactional_email'
		));

	}

	/**
	 * @param $data
	 * Hides imported orders on network page
	 * @return array
	 */
	function hide_imported_orders_on_network_page( $data ){
		$meta = array(
			'meta_key' => 'WOONET_PARENT_ORDER_ORIGIN_URL',
			'meta_compare' => 'NOT EXISTS'
		);
		return array_merge( $data , $meta );
	}

	/**
	 * @param $formatted_meta
	 * @param $data
	 * Hide woonet item meta from order display
	 * @return array
	 */
	public function hide_item_meta( $formatted_meta, $data ){
		foreach( $formatted_meta as $key => $meta ){
			if( strpos( $meta->key, '_woonet_' ) !== false ){
				unset( $formatted_meta[$key] );
			}
		}
		return $formatted_meta;
	}
    
    /**
     * @param $q
     * Hide cloned products from query
     */
    public function hide_cloned_products( $q ) {
	    $meta_query = $q->get('meta_query');

	    if( empty( $meta_query ) ){
		    $meta_query = array();
	    }

	    if( 'product' == $q->get( 'post_type' ) ){
		    $meta_query[] = array(
			    'key' => '_woonet_is_clone',
			    'compare' => 'NOT EXISTS'
		    );

		    $q->set( 'meta_query', $meta_query );
	    }
    }

	/**
	 * @param $columns
	 * Order column for imported orders
	 * @return mixed
	 */
	public function set_custom_edit_post_columns( $columns ) {
        $columns['woonet-order-originating'] = __( 'Originating Site', 'Site where the order originated.' );
        return $columns;
    }

	/**
	 * @param $column
	 * @param $post_id
	 */
	public function print_order_originating_column( $column, $post_id ) {
        switch ( $column ) {
            case 'woonet-order-originating':
                echo "<a target='_blank' href='" . get_post_meta( $post_id, 'WOONET_PARENT_ORDER_ORIGIN_URL', true ) . "'>" . get_post_meta( $post_id, 'WOONET_PARENT_ORDER_ORIGIN_TEXT', true ) . "</a>";
                break;
        }
    }

	/**
	 * @param $search_fields
	 * Search by origin order id
	 * @return mixed
	 */
	public function woocommerce_shop_order_search_order_origin_id( $search_fields ) {
		$search_fields[] = 'WOONET_PARENT_ORDER_ORIGIN_ID';
		return $search_fields;
	}

	/**
	 * @param $order_id
	 *
	 * @return bool
	 */
	private function should_import_order( $order_id ){
		$order = wc_get_order( $order_id );

		if( empty( $order ) ){
			return false;
		}

		if( ! is_a ($order, 'WC_Order' ) ){
			return false;
		}

		if( 'refunded' == $order->get_status() ){
			return false;
		}

		return true;
	}

	/**
	 * @param $order_data
	 */
	private function import_order( $order_data ){
		// Send order details to master site.
		WOO_MULTISTORE()->sync_engine->request_master( 'woomulti_custom_payload', array(
				'payload_type' => 'MASTER_IMPORT_ORDER',
				'payload_contents' => $order_data
			)
		);
	}

	/**
	 * @param $order_id
	 *
	 * @return array
	 */
	private function get_original_order_data( $order_id ) {
		$order                                      = wc_get_order( $order_id );
		$order_data                                 = array();
		$order_data['order_data']                   = $order->get_data();
		$order_data['order_data']['date_created']   = ! empty( $order->get_date_created() ) ? $order->get_date_created()->date( 'Y-m-d H:i:s' ) : '';
		$order_data['order_data']['date_modified']  = ! empty( $order->get_date_modified() ) ? $order->get_date_modified()->date( 'Y-m-d H:i:s' ) : '';
		$order_data['order_data']['date_paid']      = ! empty( $order->get_date_paid() ) ? $order->get_date_paid()->date( 'Y-m-d H:i:s' ) : '';
		$order_data['order_data']['meta_data']      = $this->get_original_order_meta_data( $order );
		$order_data['order_data']['line_items']     = $this->get_order_items( $order );
		$order_data['order_data']['shipping_items'] = $this->get_order_shipping_items( $order );
		$order_data['order_data']['tax_items']      = $this->get_order_tax_items( $order );
		$order_data['order_data']['tax_rates']      = $this->get_order_tax_rates( $order );
		$order_data['order_data']['coupon_items']   = $this->get_order_coupon_items( $order );
		$order_data['order_data']['fee_items']      = $this->get_order_fee_items( $order );
		$order_data['site_uuid']                    = WOO_MULTISTORE()->site_manager->get_master_uuid();
		$order_data['origin_order_id']              = $order_id;
		$order_data['origin_order_url']             = site_url( "/wp-admin/post.php?post={$order_id}&action=edit" );
		$order_data['origin_order_text']            = site_url() . ' | #' . $order_id;
		$order_data['order_site_name']              = get_bloginfo( 'name' );


		//get customer data
		if ( $customer = get_userdata( $order->get_customer_id() ) ) {
			$order_data['customer_data']      = $customer->to_array();
			$order_data['customer_meta_data'] = get_user_meta( $order->get_customer_id() );
		}

		unset( $order_data['order_data']['shipping_lines'] );
		unset( $order_data['order_data']['coupon_lines'] );
		unset( $order_data['order_data']['fee_lines'] );
		unset( $order_data['order_data']['tax_lines'] );

		return $order_data;
	}

	private function get_imported_order( $data ){
		$mapped_order = $this->get_mapped_order_id( $data );

		if ( ! empty( $mapped_order->post_id ) ) {
			$imported_order = wc_get_order( $mapped_order->post_id );
		} else {
			$imported_order = wc_create_order();
		}
		return $imported_order;
	}

	private function get_mapped_order_id( $data ){
		global $wpdb;

		$site_id        = $data['site_uuid'];
		$mapp_id        = "WOONET_MAP_ORDER_SID_{$site_id}_OID_{$data['order_data']['id']}";

		return $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key='{$mapp_id}'", OBJECT );
	}

	/**
	 * @param $order
	 *
	 * @return array
	 */
	private function get_original_order_meta_data($order){
		$meta_data = array();

		foreach ( $order->get_meta_data() as $meta ) {
			$meta_data[ $meta->key ] = $meta->value;
		}
		return $meta_data;
	}

	/**
	 * @param $order
	 *
	 * @return array
	 */
	private function get_order_items( $order ){
		$line_items = array();

	    foreach ( $order->get_items() as $item ) {

		    //  Product needs to be cloned on master site if not a child product
		    $json_product = array();
		    if ( ! get_post_meta( $item->get_product_id(), '_woonet_master_product_id', true ) && ! get_post_meta( $item->get_product_id(), '_woonet_master_product_sku', true ) ) {
			    $json_product = $this->product_to_json( $item->get_product_id() );
		    }

		    $item_data = $item->get_data();

		    $line_items[] = array_merge(
			    $item_data,
			    array(
				    'parent_product_id'    => get_post_meta( $item->get_product_id(), '_woonet_master_product_id', true ),
				    'parent_product_sku'   => get_post_meta( $item->get_product_id(), '_woonet_master_product_sku', true ),
				    'parent_variation_id'  => get_post_meta( $item->get_variation_id(), '_woonet_master_product_id', true ),
				    'parent_variation_sku' => get_post_meta( $item->get_variation_id(), '_woonet_master_product_sku', true ),
				    'quantity'             => $item->get_quantity(),
				    'unsynced_product'     => $json_product,
			    )
		    );
	    }

		return $line_items;
    }

	/**
	 * @param $order
	 *
	 * @return array
	 */
	private function get_order_shipping_items( $order ){
		$order_shipping_items = array();

		if ( $order->get_items( 'shipping' ) ) {
			foreach ( $order->get_items( 'shipping' ) as $shipping_item ) {
				$shipping_item_data     = $shipping_item->get_data();
				$order_shipping_items[] = $shipping_item_data;
			}
		}

		return $order_shipping_items;
	}

	/**
	 * @param $order
	 *
	 * @return array
	 */
	private function get_order_tax_items( $order ){
		$tax_items = array();

		if ( $order->get_items( 'tax' ) ) {
			foreach ( $order->get_items( 'tax' ) as $tax_item ) {
				$tax_items[] = $tax_item->get_data();
			}
		}

		return $tax_items;
	}

	/**
	 * @param $order
	 *
	 * @return array
	 */
	private function get_order_tax_rates( $order ){
		$tax_rates = array();

		if ( $order->get_items( 'tax' ) ) {
			foreach ( $order->get_items( 'tax' ) as $tax_item ) {
				$tax_rates[] = WC_Tax::_get_tax_rate( $tax_item->get_rate_id(), ARRAY_A );
			}
		}

		return $tax_rates;
	}

	/**
	 * @param $order
	 *
	 * @return array
	 */
	private function get_order_coupon_items( $order ){
		$coupon_items = array();

		if ( $order->get_items( 'coupon' ) ) {
			foreach ( $order->get_items( 'coupon' ) as $coupon_item ) {
				$coupon_details = $coupon_item->get_data();
				if ( $coupon_details['meta_data'] ) {
					$coupon_details['meta_data'] = $coupon_details['meta_data'][0]->get_data();
				}

				if ( ! $coupon_item['meta_data'] ) {
					$coupon                      = new WC_COUPON( $coupon_item['code'] );
					$coupon_details['meta_data'] = $coupon->get_data();
				}
				$coupon_items[] = $coupon_details;
			}
		}

		return $coupon_items;
	}

	/**
	 * @param $order
	 *
	 * @return array
	 */
	private function get_order_fee_items( $order ){
		$fee_items = array();

		if ( $order->get_items( 'fee' ) ) {
			foreach ( $order->get_items( 'fee' ) as $fee_item ) {
				$fee_items[] = $fee_item->get_data();
			}
		}

		return $fee_items;
	}

	private function import_tax_class() {
		$tax_classes = WC_Tax::get_tax_classes();
		if ( ! in_array( 'Child tax rates', $tax_classes ) ) {
			WC_Tax::create_tax_class( 'Child tax' );
		}
	}

	/**
	 * @param $order_data
	 */
	private function import_tax_rates( $order_data ) {
		global $wpdb;

		if ( $order_data['order_data']['tax_rates'] ) {
			foreach ( $order_data['order_data']['tax_rates'] as $tax_rate ) {
				$tax_rate_id = $tax_rate['tax_rate_id'];
				unset( $tax_rate['tax_rate_id'] );

				$tax_rate['tax_rate_class'] = 'child-tax';
				$tax_rate_name              = $order_data['order_site_name'] . '_' . $tax_rate['tax_rate_name'] . '_' . $tax_rate_id;
				$tax_rate['tax_rate_name']  = $tax_rate_name;
				$tax_rate_exists            = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}woocommerce_tax_rates WHERE tax_rate_name='{$tax_rate_name}'", OBJECT );

				if ( $tax_rate_exists ) {
					WC_Tax::_update_tax_rate( $tax_rate_exists->tax_rate_id, $tax_rate );
				} else {
					WC_Tax::_insert_tax_rate( $tax_rate );
				}
			}
		}
	}

	/**
	 * @param $imported_order
	 *
	 * @throws
	 */
	private function remove_tax_items( $imported_order ) {
		// Remove tax items
		$order_tax_items = $imported_order->get_items( 'tax' );
		if ( $order_tax_items ) {
			foreach ( $order_tax_items as $order_tax_item ) {
				wc_delete_order_item( $order_tax_item->get_id() );
			}
		}
	}

	/**
	 * @throws Exception
	 */
	private function add_tax_items( $order_data, $imported_order ){
		global $wpdb;

		$this->remove_tax_items( $imported_order );

		$new_tax_items_rate_id = array();
		if( $order_data['order_data']['tax_items'] ){
			foreach ( $order_data['order_data']['tax_items'] as $tax_item ){
				$tax_name       = $order_data['order_site_name'] . ' ' . $tax_item['rate_code'] . '_' . $tax_item['rate_id'];
				$tax_item_id    = wc_add_order_item( $imported_order->get_id(), array('order_item_name' => $tax_name, 'order_item_type' => 'tax'));

				if( $tax_item_id ){
					$tax_item_name      = $order_data['order_site_name'] . '_' . $tax_item['label'] . '_' . $tax_item['rate_id'];
					$tax_rate_exists    = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}woocommerce_tax_rates WHERE tax_rate_name='{$tax_item_name}'", OBJECT );

					if( $tax_rate_exists ){
						wc_add_order_item_meta( $tax_item_id, 'rate_id', $tax_rate_exists->tax_rate_id );
						$new_tax_items_rate_id[$tax_item['rate_id']] = $tax_rate_exists->tax_rate_id;
					}else{
						wc_add_order_item_meta( $tax_item_id, 'rate_id', $tax_item['rate_id'] );
						$new_tax_items_rate_id[$tax_item['rate_id']] = $tax_item['rate_id'];
					}

					wc_add_order_item_meta( $tax_item_id, 'label', $order_data['order_site_name']. ' ' . $tax_item['label'] . '_' . $tax_item['rate_id'] );
					wc_add_order_item_meta( $tax_item_id, 'compound', $tax_item['compound'] );
					wc_add_order_item_meta( $tax_item_id, 'tax_amount', $tax_item['tax_total'] );
					wc_add_order_item_meta( $tax_item_id, 'total_tax', $tax_item['tax_total'] );
					wc_add_order_item_meta( $tax_item_id, 'shipping_tax_amount', $tax_item['shipping_tax_total'] );
					wc_add_order_item_meta( $tax_item_id, 'rate_percent', $tax_item['rate_percent'] );
				}
			}
		}

		return $new_tax_items_rate_id;
	}

	/**
	 * @param $imported_order
	 *
	 * @throws
	 */
	private function remove_shipping_items( $imported_order ) {
		// Remove shipping items
		$order_shipping_items = $imported_order->get_items( 'shipping' );
		if ( $order_shipping_items ) {
			foreach ( $order_shipping_items as $order_shipping_item ) {
				wc_delete_order_item( $order_shipping_item->get_id() );
			}
		}
	}

	/**
	 * @param $order_data
	 * @param $new_tax_items_rate_id
	 * @param $imported_order
	 *
	 * @throws Exception
	 */
	private function add_shipping_items( $order_data, $new_tax_items_rate_id, $imported_order ) {
		$shipping_items         = $order_data['order_data']['shipping_items'];
		$site_id                = $order_data['site_uuid'];

		$this->remove_shipping_items( $imported_order );

		// Add shipping items
		if ( $shipping_items ) {
			foreach ( $shipping_items as $shipping_item ) {
				if ( $shipping_item['taxes'] ) {
					foreach ( $shipping_item['taxes'] as $shipping_tax_main_key => $shipping_item_tax ) {
						foreach ( $shipping_item_tax as $shipping_tax_key => $value ) {
							if ( array_key_exists( $shipping_tax_key, $new_tax_items_rate_id ) ) {
								unset( $shipping_item['taxes'][ $shipping_tax_main_key ][ $shipping_tax_key ] );
								$shipping_item['taxes'][ $shipping_tax_main_key ][ $new_tax_items_rate_id[ $shipping_tax_key ] ] = $value;
							}
						}
					}
				}

				$shipping_item_id = wc_add_order_item( $imported_order->get_id(), array(
					'order_item_name' => $shipping_item['name'],
					'order_item_type' => 'shipping'
				) );
				if ( $shipping_item_id ) {
					wc_add_order_item_meta( $shipping_item_id, 'method_id', $shipping_item['method_id'] );
					wc_add_order_item_meta( $shipping_item_id, 'instance_id', $shipping_item['instance_id'] );
					wc_add_order_item_meta( $shipping_item_id, 'cost', $shipping_item['total'] );
					wc_add_order_item_meta( $shipping_item_id, 'total_tax', $shipping_item['total_tax'] );
					wc_add_order_item_meta( $shipping_item_id, 'taxes', $shipping_item['taxes'] );
					wc_add_order_item_meta( $shipping_item_id, '_woonet_line_item_site_' . $site_id . '_id_' . $shipping_item['id'], 'yes' );
					if ( $shipping_item['meta_data'] ) {
						foreach ( $shipping_item['meta_data'] as $shipping_meta_data ) {
							if ( $shipping_meta_data['key'] == 'Items' ) {
								wc_add_order_item_meta( $shipping_item_id, 'Items', $shipping_meta_data['value'] );
							}
						}
					}
				}
			}
		}
		
	}

	/**
	 * @param $imported_order
	 *
	 * @throws Exception
	 */
	private function remove_coupon_items( $imported_order ) {
		// Remove coupon items
		$order_coupon_items = $imported_order->get_items( 'coupon' );
		if ( $order_coupon_items ) {
			foreach ( $order_coupon_items as $order_coupon_item ) {
				wc_delete_order_item( $order_coupon_item->get_id() );
			}
		}
	}

	/**
	 * @param $order_data
	 * @param $imported_order
	 *
	 * @throws Exception
	 */
	private function add_coupon_items( $order_data, $imported_order ) {
		
		$this->remove_coupon_items( $imported_order );

		// Add coupon items
		if ( $order_data['order_data']['coupon_items'] ) {
			foreach ( $order_data['order_data']['coupon_items'] as $coupon_item ) {
				$coupon = $this->clone_coupon( $coupon_item, $order_data['order_site_name'] );
				$coupon_item_id = wc_add_order_item( $imported_order->get_id(), array(
					'order_item_name' => $coupon_item['code'],
					'order_item_type' => 'coupon'
				) );
				if ( $coupon_item_id ) {
					wc_add_order_item_meta( $coupon_item_id, 'discount_amount', $coupon_item['discount'] );
					wc_add_order_item_meta( $coupon_item_id, 'discount_amount_tax', $coupon_item['discount_tax'] );
					wc_add_order_item_meta( $coupon_item_id, 'coupon_data', $coupon->get_data() );
				}
			}
		}
	}

	/**
	 * @param $imported_order
	 *
	 * @throws Exception
	 */
	private function remove_fee_items( $imported_order ) {
		// Remove fee items
		if ( $order_fees = $imported_order->get_items( 'fee' ) ) {
			foreach ( $order_fees as $order_fee ) {
				wc_delete_order_item( $order_fee->get_id() );
			}
		}
	}

	/**
	 * @param $order_data
	 * @param $new_tax_items_rate_id
	 * @param $imported_order
	 * @throws Exception
	 */
	private function add_fee_items( $order_data, $new_tax_items_rate_id, $imported_order ) {
		$fee_items = $order_data['order_data']['fee_items'];
		$site_id   = $order_data['site_uuid'];
		
		$this->remove_fee_items( $imported_order );

		// Add fee items
		if ( $fee_items ) {
			// Match the original tax rate id with the new tax rate id
			foreach ( $fee_items as $order_fee_item ) {
				if ( $order_fee_item['taxes'] ) {
					foreach ( $order_fee_item['taxes'] as $fee_tax_main_key => $fee_item_tax ) {
						foreach ( $fee_item_tax as $fee_tax_key => $value ) {
							if ( array_key_exists( $fee_tax_key, $new_tax_items_rate_id ) ) {
								unset( $order_fee_item['taxes'][ $fee_tax_main_key ][ $fee_tax_key ] );
								$order_fee_item['taxes'][ $fee_tax_main_key ][ $new_tax_items_rate_id[ $fee_tax_key ] ] = $value;
							}
						}
					}
				}
				$fee_item_id = wc_add_order_item( $imported_order->get_id(), array(
					'order_item_name' => $order_fee_item['name'],
					'order_item_type' => 'fee'
				) );
				if ( $fee_item_id ) {
					wc_add_order_item_meta( $fee_item_id, '_fee_amount', $order_fee_item['amount'] );
					wc_add_order_item_meta( $fee_item_id, '_tax_class', $order_fee_item['tax_class'] );
					wc_add_order_item_meta( $fee_item_id, '_tax_status', $order_fee_item['tax_status'] );
					wc_add_order_item_meta( $fee_item_id, '_line_total', $order_fee_item['total'] );
					wc_add_order_item_meta( $fee_item_id, '_line_tax', $order_fee_item['total_tax'] );
					wc_add_order_item_meta( $fee_item_id, '_line_tax_data', $order_fee_item['taxes'] );
					wc_add_order_item_meta( $fee_item_id, '_woonet_line_item_site_' . $site_id . '_id_' . $order_fee_item['id'], 'yes' );
				}
			}
		}
	}

	/**
	 * @param $imported_order
	 *
	 * @throws Exception
	 */
	private function remove_order_items( $imported_order ) {
		// Remove line items
		$order_items = $imported_order->get_items();
		if ( $order_items ) {
			foreach ( $order_items as $order_item ) {
				wc_delete_order_item( $order_item->get_id() );
			}
		}
	}

	/**
	 * @param $order_data
	 * @param $new_tax_items_rate_id
	 * @param $imported_order
	 *
	 * @throws WC_Data_Exception
	 * @throws Exception
	 */
	private function add_order_items( $order_data, $new_tax_items_rate_id, $imported_order ) {
		global $wpdb;

		$this->remove_order_items( $imported_order );

		// Add order line items
		foreach ( $order_data['order_data']['line_items'] as $item ) {
			$linked_product_id = ( $item['parent_product_sku'] ) ? wc_get_product_id_by_sku( $item['parent_product_sku'] ) : $item['parent_product_id'];
			$linked_variation_id = ( $item['parent_variation_sku'] ) ? wc_get_product_id_by_sku( $item['parent_variation_sku'] ) : $item['parent_variation_id'];

			// Clone product if it's not linked
			if ( ! $linked_product_id ) {
				$linked_product_id = $this->clone_product( $item, $order_data['site_uuid'] );
			}

			// Clone attributes to main site
			$this->clone_terms( $linked_product_id, $item['unsynced_product'] );

			// If variation is not synced get cloned variation id
			if ( ! $linked_variation_id ) {
				$linked_variation_id = $wpdb->get_var( "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key='_woonet_cloned_variation_id_{$item['variation_id']}_sid_{$order_data['site_uuid']}'" );
			}

			// Match the original tax rate id with the new tax rate id
			if ( $item['taxes'] ) {
				foreach ( $item['taxes'] as $main_key => $item_tax ) {
					foreach ( $item_tax as $key => $value ) {
						if ( array_key_exists( $key, $new_tax_items_rate_id ) ) {
							unset( $item['taxes'][ $main_key ][ $key ] );
							$item['taxes'][ $main_key ][ $new_tax_items_rate_id[ $key ] ] = $value;
						}
					}
				}
			}

			// Add order line items data
			$item_id = wc_add_order_item( $imported_order->get_id(), array(
				'order_item_name' => $item['name'],
				'order_item_type' => 'line_item'
			) );
			if ( $item_id ) {
				wc_add_order_item_meta( $item_id, '_qty', $item['quantity'] );
				wc_add_order_item_meta( $item_id, '_tax_class', $item['tax_class'] );
				wc_add_order_item_meta( $item_id, '_product_id', $linked_product_id );
				wc_add_order_item_meta( $item_id, '_variation_id', $linked_variation_id );
				wc_add_order_item_meta( $item_id, '_line_subtotal', $item['subtotal'] );
				wc_add_order_item_meta( $item_id, '_line_subtotal_tax', $item['subtotal_tax'] );
				wc_add_order_item_meta( $item_id, '_line_total', $item['total'] );
				wc_add_order_item_meta( $item_id, '_line_tax', $item['total_tax'] );
				wc_add_order_item_meta( $item_id, '_line_tax_data', $item['taxes'] );
				wc_add_order_item_meta( $item_id, '_woonet_line_item_site_' . $order_data['site_uuid'] . '_id_' . $item['id'], 'yes' );
				if ( ! empty( $item['meta_data'] ) ) {
					foreach ( $item['meta_data'] as $item_meta_data ) {
						wc_add_order_item_meta( $item_id, $item_meta_data['key'], $item_meta_data['value'] );
					}
				}
			}

		}
	}

	/**
	 * @param $imported_order
	 */
	private function remove_order_meta( $imported_order ) {
		// Remove order meta
		if ( $child_order_meta = $imported_order->get_data() ) {
			if ( isset( $child_order_meta['meta_data'] ) ) {
				foreach ( $child_order_meta['meta_data'] as $child_order_meta ) {
					if( $child_order_meta->key == '_order_number' && $this->is_enabled_sequential_order_number() ){
						continue;
					}
					delete_post_meta( $imported_order->get_id(), $child_order_meta->key );
				}
			}
		}
	}

	/**
	 * @param $order_data
	 * @param $imported_order
	 */
	private function add_order_meta( $order_data, $imported_order ) {
		// Add order meta
		if ( isset( $order_data['order_data']['meta_data'] ) ) {
			foreach ( $order_data['order_data']['meta_data'] as $order_meta_key => $order_meta_value ) {
				if( $order_meta_key == '_order_number' && $this->is_enabled_sequential_order_number() ){
					continue;
				}
				update_post_meta( $imported_order->get_id(), $order_meta_key, $order_meta_value );
			}
		}
	}

	/**
	 * @param $imported_order
	 * @param mixed $order_data
	 */
	private function add_woonet_order_meta( $imported_order, $order_data ) {
		add_post_meta( $imported_order->get_id(), "WOONET_MAP_ORDER_SID_{$order_data['site_uuid']}_OID_{$order_data['order_data']['id']}", 1 );
		add_post_meta( $imported_order->get_id(), 'WOONET_PARENT_ORDER_ORIGIN_URL', $order_data['origin_order_url'] );
		add_post_meta( $imported_order->get_id(), 'WOONET_PARENT_ORDER_ORIGIN_TEXT', $order_data['origin_order_text'] );
		add_post_meta( $imported_order->get_id(), 'WOONET_PARENT_ORDER_ORIGIN_ID', $order_data['origin_order_id'] );
	}

}

// Init.
new WC_Multistore_Import_Order();