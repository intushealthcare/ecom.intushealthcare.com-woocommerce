<?php

/**
 * Network Bulk Updater
 *
 * @class   WOO_MSTORE_BULK_SYNC
 * @since   2.0.20
 */

defined( 'ABSPATH' ) || exit;

class WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE {

	/**
	 * Source of update
	 * WOO_MULTISTORE | WooCommerce
	 *
	 * If source is the WooMultistore plugin itself, do not run stock sync hook on the child as
	 * that may lead to a sync loop.
	 *
	 * @var array
	 */
	private $_source = '';

	/**
	 * Store attributes synced.
	 *
	 * @since 3.0.4
	 * @var array
	 */
	public $synced_attributes = array();

	/**
	 * Only metadata in the array will be synced by the plugin when sync
	 * all metadata is disabled.
	 *
	 * @since 3.0.4
	 * @var array
	 */
	private $whitelisted_metadata = array(
		// '_sku',
		'total_sales',
		'_tax_status',
		'_tax_class',
		// "_manage_stock",
		// S"_backorders",
		'_sold_individually',
		'_virtual',
		'_downloadable',
		'_download_limit',
		'_download_expiry',
		// "_stock",
		// "_stock_status",
		'_sale_price_dates_from',
		'_sale_price_dates_to',
		'_wc_average_rating',
		'_wc_review_count',
		'_product_version',
		'_wpcom_is_markdown',
		'_wp_old_slug',
		'_price',
		'_regular_price',
		'_sale_price',
		'_length',
		'_width',
		'_weight',
		'_height',
		'_thumbnail_id',
		'_product_attributes',
		'_default_attributes',
		'_edit_last',
		'_product_url',
		'_button_text',
		// "_low_stock_amount",
		'default_attributes',
		'_upsell_ids',
		'_crosssell_ids',
		'_purchase_note',
		'_downloadable_files',
		'_children',
		'_product_image_gallery',
		'_variation_description',
		'attribute_pa_%',
		'attribute_%',
		'_woonet_%',
		'woonet_%',
	);

	/**
	 * Settings defined on the settings page
	 *
	 * @since 3.0.4
	 * @var array
	 */
	private $options = null;


	public function __construct( $source = 'WooCommerce' ) {
		$this->set_source( $source );
	}

	/**
	 * Run the sync operation
	 */
	public function sync( $product_id, $site_id, $background_sync = false ) {
		$product = self::product_to_json( $product_id );

		if ( $background_sync === false && ! woomulti_has_min_user_role() ) {
			return;
		}

		if ( ! WOO_MULTISTORE()->license_manager->licence_key_verify() ) {
			return;
		}

		if ( $product ) {
			$response = self::sync_child_sites( $product, $site_id );
		}
	}

	/**
	 * Set Update source
	 *
	 * @param [type] $source
	 * @return void
	 */
	public function set_source( $source ) {
		$this->_source = $source;
	}

	/**
	 * Get Update Source
	 *
	 * @return string
	 */
	public function get_source() {
		return $this->_source;
	}

	/**
	 * Convert product details and metadata to JSON, which will be sent to child sites
	 */
	public function product_to_json( $product_id ) {
		$wc_product = wc_get_product( $product_id );

		$product = array(
			'_woomulti_source'         => $this->get_source(),
			'_woomulti_version'        => defined( 'WOO_MSTORE_VERSION' ) ? WOO_MSTORE_VERSION : '',
			'_woomulti_sync_init_time' => time(),
		);

		$product['product'] = array(
			'ID'                    => $wc_product->get_id(),
			'post_author'           => null,
			'post_date'             => method_exists( $wc_product->get_date_created(), 'getTimestamp' ) ? $wc_product->get_date_created()->getTimestamp() : null,
			'date_modified'         => method_exists( $wc_product->get_date_modified(), 'getTimestamp' ) ? $wc_product->get_date_modified()->getTimestamp() : null,
			'post_date_gmt'         => null,
			'post_content'          => apply_filters( 'wc_multistore_single_get_description', $wc_product->get_description() ),
			'post_title'            => $wc_product->get_name(),
			'post_excerpt'          => apply_filters( 'wc_multistore_single_get_short_description', $wc_product->get_short_description() ),
			'post_status'           => $wc_product->get_status(),
			'comment_status'        => null,
			'ping_status'           => null,
			'post_password'         => $wc_product->get_post_password(),
			'post_name'             => $wc_product->get_slug(),
			'to_ping'               => null,
			'pinged'                => null,
			'post_modified'         => $wc_product->get_date_modified(),
			'post_modified_gmt'     => null,
			'post_content_filtered' => null,
			'post_parent'           => $wc_product->get_parent_id(),
			'guid'                  => null,
			'menu_order'            => $wc_product->get_menu_order(),
			'post_type'             => 'product',
			'post_mime_type'        => null,
			'comment_count'         => null,
			'filter'                => null,
			'catalog_visibility'    => $wc_product->get_catalog_visibility(),
			'product_type'          => $wc_product->get_type(),
			'is_featured'           => $wc_product->get_featured(),
			'sku'                   => $wc_product->get_sku(),
		);

		$product['product_type']  = $wc_product->get_type();
		$product['tags']          = $this->get_product_tags( $product_id );
		$product['categories']    = $this->get_category_tree( $product_id );
		$product['product_image'] = array(
			'image_src'  => wp_get_attachment_url( get_post_thumbnail_id( $product_id ) ),
			'attachment' => get_post( get_post_thumbnail_id( $product_id ) ),
			'meta'       => get_post_meta( get_post_thumbnail_id( $product_id ), '_wp_attachment_metadata', true ),
			'metadata'   => array(
				'_wp_attachment_image_alt' => get_post_meta( get_post_thumbnail_id( $product_id ), '_wp_attachment_image_alt', true ),
			),
		);
		$product['meta']            = $this->get_white_listed_metadata( $product_id, $wc_product );
		$product['product_gallery'] = array();

		if ( $gallery_images = $wc_product->get_gallery_image_ids() ) {
			foreach ( $gallery_images as $id ) {
				$product['product_gallery'][] = array(
					'image_src'  => wp_get_attachment_url( $id ),
					'attachment' => get_post( $id ),
					'meta'       => get_post_meta( $id, '_wp_attachment_metadata', true ),
					'metadata'   => array(
						'_wp_attachment_image_alt' => get_post_meta( $id, '_wp_attachment_image_alt', true ),
					),
				);
			}
		}

		if ( $product_attributes = $wc_product->get_attributes() ) {
			$product['product_attributes'] = $this->_get_product_attributes( $product_attributes );
		}

		if ( $wc_product->get_type() == 'variable' ) {
			$product['product_variations'] = array();

			$variations = $this->get_all_variation_ids( $product_id );

			foreach ( $variations as $variation ) {
				$wc_variation  = wc_get_product( $variation );
				$shipping_data = null;

				if ( $wc_variation->get_shipping_class() ) {
					$shipping_class = wp_get_post_terms( $variation, 'product_shipping_class' );

					if ( ! empty( $shipping_class[0]->term_id ) ) {
						$shipping_data = array(
							'id'          => $shipping_class[0]->term_id,
							'name'        => $shipping_class[0]->name,
							'slug'        => $shipping_class[0]->slug,
							'description' => $shipping_class[0]->name,
						);
					}
				}

				$thumb_id = get_post_thumbnail_id( $wc_variation->get_id() );

				if ( ! empty( $thumb_id ) ) {
					$variation_image = array(
						'image_src'  => wp_get_attachment_url( $thumb_id ),
						'ID'         => $thumb_id,
						'attachment' => get_post( $thumb_id ),
						'meta'       => get_post_meta( $thumb_id, '_wp_attachment_metadata', true ),
						'metadata'   => array(
							'_wp_attachment_image_alt' => get_post_meta( $thumb_id, '_wp_attachment_image_alt', true ),
						),
					);
				} else {
					$variation_image = false;
				}

                $product['product_variations'][] = apply_filters( 'WOO_MSTORE_SYNC/process_json/product_variation',  array(
                    'product'         => get_post( $variation ),
                    'meta'            => $this->get_white_listed_metadata( $variation, $wc_variation ),
                    'shipping_class'  => isset( $shipping_data ) ? $shipping_data : array(),
                    'stock_status'    => $wc_variation->get_stock_status(),
                    'manage_stock'    => $wc_variation->get_manage_stock(),
                    'stock_quantity'  => $wc_variation->get_stock_quantity(),
                    'backorders'      => $wc_variation->get_backorders(),
                    'low_stock'       => $wc_variation->get_low_stock_amount(),
                    'sku'             => ! empty( $wc_product->get_sku() ) && $wc_product->get_sku() == $wc_variation->get_sku() ? '' : $wc_variation->get_sku(),
                    'variation_image' => $variation_image,
                    'wp_uploads_dir'  => wp_get_upload_dir(),
                ),  $wc_product, $wc_variation->get_id() );
			}
		}

		/**
		 * Get grouped products Id's and Sku's
		 */
		if ( $wc_product->get_type() == 'grouped' ) {
			$product['grouped_product_skus'] = array();
			$product['grouped_product_ids']  = $wc_product->get_children();

			if ( $product['grouped_product_ids'] ) {
				foreach ( $product['grouped_product_ids'] as $grouped_product_id ) {
					$grouped_product                   = wc_get_product( $grouped_product_id );

					if ( empty( $grouped_product ) ) {
						continue;
					}

					$grouped_product_sku               = $grouped_product->get_sku();
					$product['grouped_product_skus'][] = $grouped_product_sku;
				}
			}
		}

		/**
		 * Get upsells products Id's and Sku's
		 */
		if ( $upsell = $wc_product->get_upsell_ids() ) {
			$product['upsell_skus'] = array();
			$product['upsell']      = $upsell;

			if ( $product['upsell'] ) {
				foreach ( $product['upsell'] as $upsell_id ) {
					$upsell_product           = wc_get_product( $upsell_id );

					if ( empty( $upsell_product ) ) {
						continue;
					}

					$upsell_product_sku       = $upsell_product->get_sku();
					$product['upsell_skus'][] = $upsell_product_sku;
				}
			}
		} else {
			$product['upsell'] = array();
		}

		/**
		 * Get cross sells products Id's and Sku's
		 */
		if ( $crosssell = $wc_product->get_cross_sell_ids() ) {
			$product['crosssell_skus'] = array();
			$product['crosssell']      = $crosssell;

			if ( $product['crosssell'] ) {
				foreach ( $product['crosssell'] as $crossell_id ) {
					$crossell_product            = wc_get_product( $crossell_id );

					if ( empty( $crossell_product ) ) {
						continue;
					}

					$crossell_product_sku        = $crossell_product->get_sku();
					$product['crosssell_skus'][] = $crossell_product_sku;
				}
			}
		} else {
			$product['crosssell'] = array();
		}

		if ( $wc_product->get_shipping_class() ) {
			$shipping = wp_get_post_terms( $wc_product->get_id(), 'product_shipping_class' );

			if ( ! empty( $shipping[0]->term_id ) ) {
				$product['shipping_class'] = array(
					'id'          => $shipping[0]->term_id,
					'name'        => $shipping[0]->name,
					'slug'        => $shipping[0]->slug,
					'description' => $shipping[0]->name,
				);
			}
		} else {
			$product['shipping_class'] = array();
		}

		$product['wp_uploads_dir'] = wp_get_upload_dir();

		/**
		 * Main product stock settings.
		 */
		$product['stock_status']   = $wc_product->get_stock_status();
		$product['manage_stock']   = $wc_product->get_manage_stock();
		$product['stock_quantity'] = $wc_product->get_stock_quantity();
		$product['backorders']     = $wc_product->get_backorders();
		$product['low_stock']      = $wc_product->get_low_stock_amount();

		$product = apply_filters( 'WOO_MSTORE_SYNC/process_json/product', $product, $wc_product, $product_id );

		return wp_json_encode( $product );
	}

	/**
	 * Send JSON payload to remote sites
	 *
	 * @param string $product Product JSON
	 * @param string $site_id Site ID
	 * @return array Array containing reponse from child site
	 **/
	public function sync_child_sites( $product, $site_id ) {
		$sites    = get_option( 'woonet_child_sites' );
		$response = array();

		foreach ( $sites as $site ) {
			if ( $site['uuid'] == $site_id ) {
				$data = array(
					'action'        => 'woomulti_child_payload',
					'post_data'     => $product,
					'Authorization' => $site['site_key'],
				);

				$url = $site['site_url'] . '/wp-admin/admin-ajax.php';

				$headers = array(
					'Authorization' => $site['site_key'],
				);

				$result = wp_remote_post(
					$url,
					array(
						'headers' => $headers,
						'body'    => $data,
					)
				);

				/**
				 * If debug is enabled, fire up this action to assist in debugging
				 * connection issues.
				 * On client sites, one can enable debugging and hook into this action
				 * to examine the results.
				 */
				if ( defined( 'WP_DEBUG' ) && WP_DEBUG == true ) {
					do_action( 'WOO_MSTORE_SYNC/HTTP_REQUEST_RESULT', $data, $url, $result );
				}

				if ( is_wp_error( $result ) ) {
					$error_message = $result->get_error_message();
					// response received from child site.
					$response = array(
						'site_url' => $site['site_url'],
						'status'   => 'request_error',
						'error'    => $error_message,
					);

					woomulti_log_error( 'sync_child_sites: Failed.' );
					woomulti_log_error( $response );
				} else {
					// response received from child site.
					$response = array(
						'site_url'    => $site['site_url'],
						'status'      => 'request_success',
						'status_code' => $result['response']['code'],
						'headers'     => $result['headers']->getAll(),
						'response'    => $result['body'],
					);
				}
			}
		}

		return $response;
	}

	/**
	 * Run on the child site. Updates/creates product from JSON received
	 *
	 * @return void
	 */
	public function sync_child() {
		if ( ! $this->is_request_authenticated( $_POST ) ) {
			wp_send_json(
				array(
					'error'   => 1,
					'message' => 'Authentication failed',
				)
			);
		}

		do_action( 'WOO_MSTORE/sync/stock/disable' );

		if ( ! empty( $_POST['post_data'] ) ) {
			global $wpdb;
			$product = json_decode( stripslashes( $_POST['post_data'] ), JSON_OBJECT_AS_ARRAY );

			if ( is_null( $product ) ) {
				$product = json_decode( $_POST['post_data'], JSON_OBJECT_AS_ARRAY );
			}

			if( WOO_MULTISTORE()->options_manager->get('sync-by-sku') == 'yes' ){
				$was_synced = $wpdb->get_row(
					$wpdb->prepare(
						"SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key='_woonet_master_product_sku' AND meta_value =%s",
						$product['product']['sku']
					)
				);
			}else{
				$was_synced = $wpdb->get_row(
					$wpdb->prepare(
						"SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key='_woonet_master_product_id' AND meta_value =%s",
						$product['product']['ID']
					)
				);
			}

			// Check if product should be synced
            $parent_id          = $product['product']['ID'];
            $site_key           = get_option('woonet_master_connect');
            $site_key           = $site_key['uuid'];
            $inherit_meta       = '_woonet_publish_to_'.$site_key.'_child_inheir';
            $inherit            = ( $product['meta'][$inherit_meta] != 'no' );
			$_options           = get_option( 'woonet_options' );
			$inherit_variations = ( 'no' != $_options['child_inherit_changes_fields_control__variations'] );
			$inherit_attributes = ( 'no' != $_options['child_inherit_changes_fields_control__attributes'] );

			if( $was_synced && $product['meta'][$inherit_meta] == 'no' ){
				return;
			}


			$wc_product = $this->sync_product_attributes( $product );

			if ( ! empty( $wc_product->get_id() ) ) {
				$this->sync_attributes_meta( $wc_product, $product );
				$this->sync_product_meta( $wc_product->get_id(), $product, $wc_product );
				$this->sync_product_image( $wc_product->get_id(), $product );
				$this->sync_product_gallery( $wc_product->get_id(), $product );
				$this->sync_product_tags( $wc_product->get_id(), $product );
				$this->sync_product_categories( $wc_product->get_id(), $product );

				if ( $wc_product->get_type() == 'variable' && $inherit_variations && $inherit_attributes && $inherit ) {
					$this->sync_product_variations( $wc_product->get_id(), $product );
				}

				$this->sync_upsell_cross_sell( $wc_product->get_id(), $product );
				$this->sync_grouped_products( $wc_product->get_id(), $product );
				$this->sync_shipping_class( $wc_product->get_id(), $product );

				// @todo: refactor
				$this->stock_status_sync_fix( $wc_product->get_id(), $product );

				do_action( 'WOO_MSTORE_SYNC/sync_child/complete', $wc_product->get_id(), $parent_id, $product );
			}
		}

		do_action( 'WOO_MSTORE/sync/stock/enable' );
	}

	/**
	 * Creates or updates main product details.
	 *
	 * @param mixed $product
	 * @return void
	 */
	public function sync_product_attributes( $product ) {
		$_syncable_attributes = $product['product'];
		$parent_id            = $product['product']['ID'];
		$parent_sku           = $product['product']['sku'];
		$id                   = $this->get_mapped_child_post( $parent_id, $parent_sku );

		if ( $id = $this->get_mapped_child_post( $parent_id, $parent_sku ) ) {
			$wc_product = wc_get_product( $id );

			/**
			 * Fixes the variation bug that was introduced in 4.1.1
			 */
			$this->__fix_variation_sku_duplication_bug( $wc_product );

			/**
			 * If the master product type was been changed,
			 * force change the child product type
			 */
			if ( ! empty( $product['product']['product_type'] )
				&& $product['product']['product_type'] != $wc_product->get_type() ) {
				$this->force_product_type_convertion( $wc_product, $product['product']['product_type'] );
				wp_cache_flush();
				// fetch the product again with updated details.
				$wc_product = wc_get_product( $id );
			}
		} else {

			switch ( $product['product_type'] ) {
				case 'simple':
					$wc_product = new WC_Product_Simple();
					break;

				case 'variable':
					$wc_product = new WC_Product_Variable();
					break;

				case 'grouped':
					$wc_product = new WC_Product_Grouped();
					break;

				case 'external':
					$wc_product = new WC_Product_External();
					break;

				case 'booking':
					// If class 'WC_Product_Booking' exist then this class is defined into the Woocommerce Booking Plugin.
					if ( class_exists( 'WC_Product_Booking' ) ) {
						$wc_product = new WC_Product_Booking();
					} else {
						$wc_product = new WC_Product();
					}
					break;

				default:
					$wc_product = new WC_Product();
					break;
			}
		}

		if ( $this->get_option( 'child_inherit_changes_fields_control__status', 'yes' ) == 'yes' ) {
			$wc_product->set_status( $_syncable_attributes['post_status'] );
		} elseif ( $wc_product->get_id() === 0 && $this->get_option( 'child_inherit_changes_fields_control__status', 'yes' ) == 'no' ) {
			$wc_product->set_status( 'draft' );
		}

		if ( $this->get_option( 'child_inherit_changes_fields_control__title', 'yes' ) == 'yes' || $wc_product->get_id() === 0 ) {
			$wc_product->set_name( $_syncable_attributes['post_title'] );
		}

		if ( $this->get_option( 'child_inherit_changes_fields_control__description', 'yes' ) == 'yes' ) {
			$wc_product->set_description( $_syncable_attributes['post_content'] );
		}

		if ( $this->get_option( 'child_inherit_changes_fields_control__short_description', 'yes' ) == 'yes' ) {
			$wc_product->set_short_description( $_syncable_attributes['post_excerpt'] );
		}

		if ( $this->get_option( 'child_inherit_changes_fields_control__slug', 'yes' ) == 'yes' || $wc_product->get_id() === 0 ) {
			$wc_product->set_slug( $_syncable_attributes['post_name'] );
		}

		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_post_date', true ) ) {
			$wc_product->set_date_created( $_syncable_attributes['post_date'] );
		}

		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_date_modified', true ) ) {
			$wc_product->set_date_modified( $_syncable_attributes['date_modified'] );
		}

		if ( $this->get_option( 'child_inherit_changes_fields_control__catalogue_visibility', 'yes' ) == 'yes'
			&& apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_catalog_visibility', true ) ) {
			$wc_product->set_catalog_visibility( $_syncable_attributes['catalog_visibility'] );
		}

		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_menu_order', true ) ) {
			$wc_product->set_menu_order( $_syncable_attributes['menu_order'] );
		}

		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_post_password', true ) ) {
			$wc_product->set_post_password( $_syncable_attributes['post_password'] );
		}

		if ( $this->get_option( 'child_inherit_changes_fields_control__sku', 'yes' ) == 'yes' ) {
			if ( empty( $_syncable_attributes['sku'] ) ) {
				$wc_product->set_sku( '' );
			} elseif ( ! empty( $_syncable_attributes['sku'] ) && $wc_product->get_sku() != $_syncable_attributes['sku'] ) {
                if( ! wc_get_product_id_by_sku( $_syncable_attributes['sku'] ) ){
                    $wc_product->set_sku( $_syncable_attributes['sku'] );
                }else{
                    $logger = wc_get_logger();
                    $message = 'Duplicate sku found:'. $_syncable_attributes['sku'] . ' for parent product id: ' . $parent_id;
                    $logger->add('woocommerce-multistore', $message );
                }
			}
		}

		if ( $this->get_option( 'child_inherit_changes_fields_control__featured', 'yes' ) == 'yes' ) {
			if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_is_featured', true ) ) {
				$wc_product->set_featured( $_syncable_attributes['is_featured'] );
			}
		}

		// link with the parent based on product sku or custom id.
		if ( $this->get_option( 'sync-by-sku', 'no' ) == 'yes' ) {
			$wc_product->update_meta_data( '_woonet_master_product_sku', $parent_sku );
			$wc_product->update_meta_data( '_woonet_master_product_sku_' . $parent_sku, $parent_sku );

			// delete the id link if user switched from sku to default id
			$wc_product->delete_meta_data( '_woonet_master_product_id' );
			$wc_product->delete_meta_data( '_woonet_master_product_id_' . $parent_id );
		} else {
			$wc_product->update_meta_data( '_woonet_master_product_id', $parent_id );
			$wc_product->update_meta_data( '_woonet_master_product_id_' . $parent_id, $parent_id );

			// delete the sku link if user switched from id to sku
			$wc_product->delete_meta_data( '_woonet_master_product_sku' );
			$wc_product->delete_meta_data( '_woonet_master_product_sku_' . $parent_sku );
		}

		if ( $wc_product->save() ) {

			return $wc_product;
		}

		return false;
	}

	/**
	 * Get mapped child post on the child site.
	 *
	 * @param integer $parent_post_id Master post ID.
	 * @return mixed
	 */
	public function get_mapped_child_post( $parent_post_id, $sku ) {
		global $wpdb;

		/**
		 * if sku sync in enabled
		 */
		if ( $this->get_option( 'sync-by-sku', 'no' ) == 'yes' ) {
			if ( ! $sku ) {
				return false;
			}

			$meta = $wpdb->get_row(
				$wpdb->prepare(
					"SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key='_sku' AND meta_value =%s",
					$sku
				)
			);

			// check if product was synced by id before. Update that product instead of creating a new one
			// if( empty( $meta->post_id ) ){
			// $meta = $wpdb->get_row(
			// $wpdb->prepare(
			// "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key='_woonet_master_product_id' AND meta_value=%d",
			// $parent_post_id
			// )
			// );
			// }

			if ( ! empty( $meta->post_id ) ) {
				return $meta->post_id;
			}

			return false;
		}

		/**
		 * New metadata bypass search by meta_value to prevent full table scanning.
		 */
		$meta = $wpdb->get_row(
			$wpdb->prepare(
				"SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key='_woonet_master_product_id_%d'",
				$parent_post_id
			)
		);

		if ( ! empty( $meta->post_id ) ) {
			return $meta->post_id;
		}

		/**
		 * Fallback to old metadata.
		 */
		$meta = $wpdb->get_row(
			$wpdb->prepare(
				"SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key='_woonet_master_product_id' AND meta_value=%d",
				$parent_post_id
			)
		);

		if ( ! empty( $meta->post_id ) ) {
			return $meta->post_id;
		}
		return false;
	}

	public function get_mapped_child_attachment( $parent_post_id ) {
		global $wpdb;

		$meta = $wpdb->get_row(
			$wpdb->prepare(
				"SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key='_woonet_master_attachment_id' AND meta_value=%d",
				$parent_post_id
			)
		);

		if ( ! empty( $meta->post_id ) ) {
			return $meta->post_id;
		}

		return false;
	}

	public function get_mapped_child_term( $parent_term_id ) {
		global $wpdb;
		$meta = $wpdb->get_row(
			$wpdb->prepare(
				"SELECT * FROM {$wpdb->prefix}termmeta WHERE meta_key='_woonet_master_term_id' AND meta_value=%d",
				$parent_term_id
			)
		);

		if ( ! empty( $meta->term_id ) ) {
			return $meta->term_id;
		}

		return false;
	}

	public function sync_product_meta( $child_product_id, $product, $wc_product ) {
		$_options       = get_option( 'woonet_options' );
		$_syncable_meta = $product['meta'];
		$wc_product     = wc_get_product( $child_product_id );

		unset( $_syncable_meta['_product_image_gallery'] );
		// unset( $_syncable_meta['_product_url'] );
		unset( $_syncable_meta['_thumbnail_id'] );
		unset( $_syncable_meta['_edit_lock'] );
		unset( $_syncable_meta['_upsell_ids'] );
		unset( $_syncable_meta['_crosssell_ids'] );

		/**
		 * If users migrated the product from a child store and then imported
		 * it back into a parent store, there could be some meta we use to identify
		 * a child product. So here we unset the meta.
		 */
		unset( $_syncable_meta['_woonet_master_product_id'] );

		/**
		 * Hook for the child site to add additional metadata to the sync
		 */
		$_syncable_meta = apply_filters( 'WOO_MSTORE_SYNC/sync_child/meta', $_syncable_meta, $child_product_id, $product );

		if ( WOO_MULTISTORE()->options_manager->get( 'child_inherit_changes_fields_control__price' ) == 'yes' ) {
			$wc_product->set_price( $_syncable_meta['_price'][0] );
			$wc_product->set_regular_price( $_syncable_meta['_regular_price'][0] );
		}

		if ( WOO_MULTISTORE()->options_manager->get( 'child_inherit_changes_fields_control__sale_price' ) == 'yes' ) {
			if ( ! empty( $_syncable_meta['_sale_price'] ) ) {
				$wc_product->set_sale_price( $_syncable_meta['_sale_price'][0] );
			} else {
				$wc_product->set_sale_price( '' );
			}

			if ( ! empty( $_syncable_meta['_sale_price_dates_from'] ) ) {
				$wc_product->set_date_on_sale_from( $_syncable_meta['_sale_price_dates_from'][0] );
			} else {
				$wc_product->set_date_on_sale_from( '' );
			}

			if ( ! empty( $_syncable_meta['_sale_price_dates_to'] ) ) {
				$wc_product->set_date_on_sale_to( $_syncable_meta['_sale_price_dates_to'][0] );
			} else {
				$wc_product->set_date_on_sale_to( '' );
			}

		}

		unset( $_syncable_meta['_price'] );
		unset( $_syncable_meta['_regular_price'] );
		unset( $_syncable_meta['_sale_price'] );
		unset( $_syncable_meta['_sale_price_dates_from'] );
		unset( $_syncable_meta['_sale_price_dates_to'] );

		/**
		 * Individual price filters
		 */
		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_product_price', true ) === false ) {
			unset( $_syncable_meta['_price'] );
		}

		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_product_sale_price', true ) === false ) {
			unset( $_syncable_meta['_sale_price'] );
		}

		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_product_regular_price', true ) === false ) {
			unset( $_syncable_meta['_regular_price'] );
		}

		if ( WOO_MULTISTORE()->options_manager->get( 'child_inherit_changes_fields_control__reviews' ) == 'no' ) {
			$_syncable_meta['_wc_review_count'] = get_comments_number( $child_product_id );
			unset( $_syncable_meta['_wc_average_rating'] );
		}

		if ( WOO_MULTISTORE()->options_manager->get( 'child_inherit_changes_fields_control__default_variations' ) != 'yes' ) {
			unset( $_syncable_meta['_default_attributes'] );
		}else{
			if( empty( $_syncable_meta['_default_attributes'] ) ){
				$wc_product->set_default_attributes( '' );
			}
		}

		if ( $this->get_option( 'child_inherit_changes_fields_control__attributes', 'yes' ) == 'no' ) {
			unset( $_syncable_meta['_product_attributes'] );
		}else{
			if( empty( $_syncable_meta['_product_attributes'] ) ){
				delete_post_meta( $child_product_id, '_product_attributes' );
			}
		}

        if( empty( $_syncable_meta['_weight'] ) ){
            $wc_product->set_weight( '' );
        }

        if( empty( $_syncable_meta['_length'] ) ){
            $wc_product->set_length( '' );
        }

        if( empty( $_syncable_meta['_width'] ) ){
            $wc_product->set_width( '' );
        }

        if( empty( $_syncable_meta['_height'] ) ){
            $wc_product->set_height( '' );
        }

        //external product
        if( $wc_product->get_type() == 'external' ){
            if( ! $_syncable_meta['_product_url'] ){
                $wc_product->set_product_url( '' );
            }

            if( ! $_syncable_meta['_button_text'] ){
                $wc_product->set_button_text( '' );
            }
        }

		foreach ( $_syncable_meta as $key => $meta_value ) {
			if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_meta_' . sanitize_key( $key ), true ) ) {
				if( is_array( $meta_value ) ){
					delete_post_meta( $child_product_id, $key );
					if( ! empty( $meta_value ) ){
						foreach ( $meta_value as $value ){
							if( ! empty( $value ) ){
								add_post_meta( $child_product_id, $key, $value );
							}
						}
					}
				}else{
					update_post_meta( $child_product_id, $key, $meta_value );
				}
			}
		}
		$wc_product->save();
	}

	public function sync_product_image( $child_product_id, $product ) {
		$_options = get_option( 'woonet_options' );

		/**
		 * If image sync is explicitly disabled, skip syncing
		 */
		if ( isset( $_options['child_inherit_changes_fields_control__product_image'] )
			&& $_options['child_inherit_changes_fields_control__product_image'] == 'no' ) {
			return;
		}

		$product_image = $product['product_image'];

		if ( empty( $product_image['image_src'] ) ) {
			delete_post_meta( $child_product_id, '_thumbnail_id' );
			return;
		}

		/**
		 * Check if global image is enabled.
		 * If global image is enabled, there's no need to download images on the child.
		 */
		 if ( ! empty( $_options['enable-global-image'] ) && $_options['enable-global-image'] == 'yes' ) {
			 $idPrefix              = 1000000;
			 $newId                 = $idPrefix.$product_image['attachment']['ID']; // Unique ID, must be a number.
			 $global_images         = get_option( 'wc_multistore_global_images', array() );

			 $global_image_data     = array(
				 'meta_data'    => $product_image['meta'],
				 'uploads_dir'  => $product['wp_uploads_dir'],
				 'src'          => $product_image['image_src'],
				 'alt'          => $product_image['metadata']['_wp_attachment_image_alt']
			 );

			 $global_images[ $product_image['attachment']['ID'] ] = $global_image_data;

		     update_post_meta( $child_product_id, '_thumbnail_id', $newId );
			 update_option('wc_multistore_global_images', $global_images );

			 return true;
		 }

		if ( $id = $this->get_mapped_child_attachment( $product_image['attachment']['ID'] ) ) {
			// check for update
			set_post_thumbnail( $child_product_id, $id );
		} else {
			// create new image and set it as prodouct thumbnail
			$id = media_sideload_image( trim( $product_image['image_src'] ), $child_product_id, null, 'id' );

			if ( ! empty( $id ) && ! is_wp_error( $id ) ) {
				set_post_thumbnail( $child_product_id, $id );
				update_post_meta( $id, '_woonet_master_attachment_id', $product_image['attachment']['ID'] );
			} else {
				error_log( $id->get_error_message() . ' Supplied URL: ' . $product_image['image_src'] );
			}
		}

		/**
		 * By default don't sync image data.
		 */
		if ( ! apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_attachment_meta_data', false ) ) {
			return;
		}

		if ( ! empty( $id ) && ! is_wp_error( $id ) ) {
			$post_array = $product_image['attachment'];

			$post_array['ID'] = $id;
			unset( $post_array['post_author'] );
			unset( $post_array['guid'] );
			unset( $post_array['post_type'] );
			unset( $post_array['post_mime_type'] );

			wp_update_post( $post_array );

			update_post_meta( $id, '_wp_attachment_image_alt', $product_image['metadata']['_wp_attachment_image_alt'] );
		}
	}

	public function sync_product_gallery( $child_product_id, $product ) {
		$_options = get_option( 'woonet_options' );

		/**
		 * If image sync is explicitly disabled, skip syncing
		 */
		if ( $this->get_option( 'child_inherit_changes_fields_control__product_gallery', 'yes' ) == 'no' ) {
			return;
		}

		$product_image      = $product['product_gallery'];
		$media_ids          = array();

		/**
		 * Check if global image is enabled.
		 * If global image is enabled, there's no need to download images on the child.
		 */
		if ( ! empty( $_options['enable-global-image'] ) && $_options['enable-global-image'] == 'yes' ) {
			$idPrefix = 1000000;
			foreach ( $product_image as $key => $value ) {
				$global_images  = get_option( 'wc_multistore_global_images', array() );
				$newId          = $idPrefix.$value['attachment']['ID'];
				$media_ids[]    = $newId;

				$global_image_data = array(
					'meta_data'    => $value['meta'],
					'uploads_dir'  => $product['wp_uploads_dir'],
					'src'          => $value['image_src'],
					'alt'          => $product_image['metadata']['_wp_attachment_image_alt']
				);

				$global_images[ $value['attachment']['ID'] ] = $global_image_data;
				update_option( 'wc_multistore_global_images', $global_images );
			}
		}else{
			foreach ( $product_image as $key => $value ) {
				if ( $id = $this->get_mapped_child_attachment( $value['attachment']['ID'] ) ) {
					// check for update
					$media_ids[] = $id;
				} else {
					// create new image and set it as prodouct thumbnail
					$id = media_sideload_image( trim( $value['image_src'] ), $child_product_id, null, 'id' );

					if ( ! empty( $id ) && ! is_wp_error( $id ) ) {
						$media_ids[] = $id;
						update_post_meta( $id, '_woonet_master_attachment_id', $value['attachment']['ID'] );
					} else {
						error_log( $id->get_error_message() . ' Supplied URL: ' . $value['image_src'] );
					}
				}
				/**
				 * By default don't sync image data.
				 */
				if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_attachment_meta_data', false ) === true
				     && ! empty( $id ) && ! is_wp_error( $id ) ) {
					$post_array       = $value['attachment'];
					$post_array['ID'] = $id;
					unset( $post_array['post_author'] );
					unset( $post_array['guid'] );
					unset( $post_array['post_type'] );
					unset( $post_array['post_mime_type'] );
					update_post_meta( $id, '_wp_attachment_image_alt', $value['metadata']['_wp_attachment_image_alt'] );
				}
			}
		}

		update_post_meta( $child_product_id, '_product_image_gallery', implode( ',', $media_ids ) );
	}

	/**
	 * Sync variation image for variable product.
	 *
	 * @param mixed $child_variation_id
	 * @param mixed $variation
	 * @return void
	 */
	public function sync_variation_image( $child_variation_id, $variation ) {
		$_options = get_option( 'woonet_options' );
		$variation_image = $variation['variation_image'];

		if ( empty( $variation_image['image_src'] ) ) {
			delete_post_meta( $child_variation_id, '_thumbnail_id' );
			return;
		}

		/**
		 * Check if global image is enabled.
		 * If global image is enabled, there's no need to download images on the child.
		 */
		if ( ! empty( $_options['enable-global-image'] ) && $_options['enable-global-image'] == 'yes' ) {
			$idPrefix              = 1000000;
			$newId                 = $idPrefix.$variation_image['attachment']['ID']; // Unique ID, must be a number.
			$global_images         = get_option( 'wc_multistore_global_images', array() );

			$global_image_data     = array(
				'meta_data'    => $variation_image['meta'],
				'uploads_dir'  => $variation['wp_uploads_dir'],
				'src'          => $variation_image['image_src'],
				'alt'          => $variation_image['metadata']['_wp_attachment_image_alt']
			);

			$global_images[ $variation_image['attachment']['ID'] ] = $global_image_data;

			update_post_meta( $child_variation_id, '_thumbnail_id', $newId );
			update_option('wc_multistore_global_images', $global_images );

			return;
		}

		if ( $id = $this->get_mapped_child_attachment( $variation_image['ID'] ) ) {
			// check for update
			set_post_thumbnail( $child_variation_id, $id );
		} else {
			// create new image and set it as prodouct thumbnail
			$id = media_sideload_image( trim( $variation_image['image_src'] ), $child_variation_id, null, 'id' );

			if ( ! empty( $id ) && ! is_wp_error( $id ) ) {
				set_post_thumbnail( $child_variation_id, $id );
				update_post_meta( $id, '_woonet_master_attachment_id', $variation_image['ID'] );
			} else {
				error_log( $id->get_error_message() . ' Supplied URL for variation image: ' . $variation_image['image_src'] );
			}
		}
		/**
		 * By default don't sync image data.
		 */
		if ( ! apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_attachment_meta_data', false ) ) {
			return;
		}
		if ( ! empty( $id ) && ! is_wp_error( $id ) ) {
			$post_array       = $variation_image['attachment'];
			$post_array['ID'] = $id;
			unset( $post_array['post_author'] );
			unset( $post_array['guid'] );
			unset( $post_array['post_type'] );
			unset( $post_array['post_mime_type'] );
			wp_update_post( $post_array );
			update_post_meta( $id, '_wp_attachment_image_alt', $variation_image['metadata']['_wp_attachment_image_alt'] );
		}
	}

	public function sync_category_thumbnail( $term_id, $data ) {
		$media_id = null;
		$_options = get_option( 'woonet_options' );

		/**
		 * Check if global image is enabled.
		 * If global image is enabled, there's no need to download images on the child.
		 */
		if ( ! empty( $_options['enable-global-image'] ) && $_options['enable-global-image'] == 'yes' ) {
			$idPrefix              = 1000000;
			$newId                 = $idPrefix.$data['attachment']['ID']; // Unique ID, must be a number.
			$global_images         = get_option( 'wc_multistore_global_images', array() );

			$global_image_data     = array(
				'meta_data'    => $data['meta'],
				'uploads_dir'  => $data['wp_uploads_dir'],
				'src'          => $data['url'],
				'alt'          => $data['metadata']['_wp_attachment_image_alt']
			);

			$global_images[ $data['attachment']['ID'] ] = $global_image_data;

			update_term_meta( $term_id, 'thumbnail_id', $newId );
			update_option('wc_multistore_global_images', $global_images );

			return;
		}

		/**
		 * Upload the image if global image is not yes
		 */
		if ( $attachment_id = $this->get_mapped_child_attachment( $data['id'] ) ) {
			// check for update
			$media_id = $attachment_id;
		} else {
			// create new image and set it as prodouct thumbnail
			$id = media_sideload_image( trim( $data['url'] ), null, null, 'id' );

			if ( ! empty( $id ) && ! is_wp_error( $id ) ) {
				$media_id = $id;
				update_post_meta( $id, '_woonet_master_attachment_id', $data['id'] );
			} else {
				error_log( $id->get_error_message() . ' Supplied URL: ' . $data['url'] );
			}
		}

		update_term_meta( $term_id, 'thumbnail_id', $media_id );
	}

	public function sync_product_tags( $child_product_id, $product ) {
		$_options = get_option( 'woonet_options' );

		if ( $_options['child_inherit_changes_fields_control__product_tag'] != 'yes' ) {
			return;
		}

		$tags         = $product['tags'];
		$terms_to_add = array();

		if ( ! empty( $tags ) ) {
			foreach ( $tags as $tag ) {
				if ( $id = $this->get_mapped_child_term( $tag['term_id'] ) ) {
					// term exists update it
					unset( $tag['term_id'] );
					unset( $tag['term_taxonomy_id'] );
					unset( $tag['taxonomy'] );
					$terms_to_add[] = (int) $id;
					wp_update_term( $id, 'product_tag', $tag );

					do_action( 'WOO_MSTORE_SYNC/sync_child/tag', $id, $tag );
				} else {
					/**
					 * Check if a tag with the same name exists.
					 */
					$matching_tag = get_term_by( 'name', $tag['name'], 'product_tag' );

					if ( ! empty( $matching_tag->term_id ) ) {
						$terms_to_add[] = (int) $matching_tag->term_id;
						add_term_meta( $matching_tag->term_id, '_woonet_master_term_id', $tag['term_id'] );
					} else {
						// add new term
						$parent_term_id = $tag['term_id'];
						unset( $tag['term_id'] );
						unset( $tag['term_taxonomy_id'] );
						unset( $tag['taxonomy'] );
						$id = wp_insert_term( $tag['name'], 'product_tag', $tag );

						if ( ! is_wp_error( $id ) ) {
							add_term_meta( $id['term_id'], '_woonet_master_term_id', $parent_term_id );
							$terms_to_add[] = (int) $id['term_id'];
							do_action( 'WOO_MSTORE_SYNC/sync_child/tag', $id['term_id'], $tag );
						} else {
							woomulti_log_error( 'Term (product_tag) can not be added. ' . $id->get_error_message() );
						}
					}
				}
			}
		}

		wp_set_post_terms( $child_product_id, $terms_to_add, 'product_tag' );
	}

	public function sync_product_categories( $child_product_id, $product, $update_only_if_exists = false ) {
		$_options = get_option( 'woonet_options' );

		if ( $_options['child_inherit_changes_fields_control__product_cat'] != 'yes' ) {
			return;
		}

		$cat_tree     = $product['categories'];
		$terms_to_add = array();

		foreach ( $cat_tree as $key => $categories ) {
			foreach ( $categories as $category ) {
				$category_thumb = false;
				if ( ! empty( $category['__thumbnail'] ) ) {
					$category_thumb = $category['__thumbnail'];
					unset( $category['__thumbnail'] );
				}

				if ( $child_term_id = $this->get_mapped_child_term( $category['term_id'] ) ) {
					// term exists update it
					unset( $category['term_id'] );
					unset( $category['term_taxonomy_id'] );
					unset( $category['taxonomy'] );

					if ( $_options['child_inherit_changes_fields_control__category_changes'] != 'yes' ) {
						unset( $category['description'] );
					}

					$category['parent'] = $this->get_mapped_child_term( $category['parent'] );
					wp_update_term( $child_term_id, 'product_cat', $category );

					if ( ! empty( $category['order'] ) ) {
						update_term_meta( $child_term_id, 'parent_order', $category['order'] );
					}

					do_action( 'WOO_MSTORE_SYNC/sync_child/cat', $child_term_id, $category );
				} else {
					/**
					 * If update only, don't create when there is no linked category
					 */
					if ( $update_only_if_exists === true ) {
						continue;
					}
					/**
					/**
					 * Check if a tag with the same name exists.
					 */
                    $matching_cat = get_term_by( 'slug', $category['slug'], 'product_cat' );

					if ( ! empty( $matching_cat->term_id ) ) {
						$terms_to_add[] = (int) $matching_cat->term_id;
						add_term_meta( (int) $matching_cat->term_id, '_woonet_master_term_id', $category['term_id'] );

						if ( ! empty( $category['order'] ) ) {
							update_term_meta( $matching_cat->term_id, 'parent_order', $category['order'] );
						}
						$child_term_id = $matching_cat->term_id;

						do_action( 'WOO_MSTORE_SYNC/sync_child/cat', $matching_cat->term_id, $category );
					} else {
						// add new term
						$parent_term_id = $category['term_id'];
						unset( $category['term_id'] );
						unset( $category['term_taxonomy_id'] );
						unset( $category['taxonomy'] );

						$category['parent'] = $this->get_mapped_child_term( $category['parent'] );
						$id                 = wp_insert_term( $category['name'], 'product_cat', $category );

						if ( ! is_wp_error( $id ) ) {
							add_term_meta( $id['term_id'], '_woonet_master_term_id', $parent_term_id );
							$child_term_id = $id['term_id'];

							if ( ! empty( $category['order'] ) ) {
								update_term_meta( $id['term_id'], 'parent_order', $category['order'] );
							}

							do_action( 'WOO_MSTORE_SYNC/sync_child/cat', $id['term_id'], $category );
						} else {
							woomulti_log_error( 'Term (product_cat) can not be added. ' . $id->get_error_message() );
							continue;
						}
					}
				}

				$terms_to_add[] = $this->get_mapped_child_term( $key );

				// add or update category thumbnail.
				if ( $category_thumb ) {
					if ( $_options['child_inherit_changes_fields_control__category_changes'] != 'no' ) {
						$this->sync_category_thumbnail( $child_term_id, $category_thumb );
					}
				} else {
					delete_term_meta( $child_term_id, 'thumbnail_id' );
				}

				// add or update meta data.
				if ( 'no' != $_options['child_inherit_changes_fields_control__category_meta'] ) {
					$exclude_meta = array(
						'order',
						'product_count_product_cat',
						'thumbnail_id'
					);
					$master_term_metas = $category['meta'];
					if( ! empty( $master_term_metas ) ){
						foreach ( $master_term_metas as $meta_key => $meta_value ){
							if( !is_serialized( $meta_value[0] ) ) {
								$meta_value = maybe_serialize($meta_value[0]);
							}
							if( ! in_array( $meta_key, $exclude_meta ) ){
								update_term_meta( $child_term_id, $meta_key, $meta_value );
							}
						}
					}
				}

			}
		}
		if ( ! is_null( $child_product_id ) ) {
			wp_set_post_terms( $child_product_id, $terms_to_add, 'product_cat' );
		}
	}

	public function sync_product_variations( $child_product_id, $product ) {
		$_existing_variations = $this->get_all_variation_ids( $child_product_id ); // get all current variations before the sync.
		$_synced_variations   = array();
		$_options             = get_option( 'woonet_options' );

		if ( empty( $product['product_variations'] ) ) {
			woomulti_log_error( 'No product variation found for product type variable.' );
			return;
		} else {
			$variations = $product['product_variations'];
		}

		// loop through variations and create them
		foreach ( $variations as $variation ) {

			$variation_data                 = $variation['product'];
			$variation_meta_data            = $variation['meta'];
			$variation_parent_id            = $variation['product']['ID'];
			$variation_parent_sku           = $variation['sku'];
			$variation_data['post_parent']  = $child_product_id;
			$synced_var_id                  = $this->get_mapped_child_post( $variation_parent_id, $variation_parent_sku );

			unset( $variation_data['ID'] );
			unset( $variation_data['guid'] );
			unset( $variation_data['post_author'] );

			/**
			 * Only disable variation status if the variation exists, otherwise the status is needed to create the variable
			 **/
			if( $_options['child_inherit_changes_fields_control__variations_status'] == 'no' && $synced_var_id ){
				unset( $variation_data['post_status'] );
			}

			/**
			 * Update or Create if the variation was not synced before
			**/
			if( $synced_var_id ){
				$variation_data['ID'] = $synced_var_id;
				$resp = wp_update_post( $variation_data, true );
			}else{
				$resp = wp_insert_post( $variation_data, true );
			}

			/**
			 * Continue if error
			 **/
			if ( is_wp_error( $resp ) ) {
				woomulti_log_error( 'Failed to update/create variation: ' . $resp->get_error_message() );
				continue;
			}

			/**
			 * If variation data is supposed to sync
			 **/
			if (  apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_variation_meta', true ) && $_options['child_inherit_changes_fields_control__variations_data'] != 'no' ) {
				// Data
				$this->sync_variation_meta( $resp, $variation_meta_data, $product );

				// Image
				if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_variation_image', true ) ) {
					$this->sync_variation_image( $resp, $variation );
				}

				// Shipping Class
				$this->sync_shipping_class( $resp, $variation );
			}

			/**
			 * If Sku is supposed to sync
			 **/
			if ( $_options['child_inherit_changes_fields_control__variations_sku'] != 'no' ) {
				update_post_meta( $resp, '_sku', $variation['sku'] );
			}

			/**
			 * If stock qty is supposed to sync
			 **/
			if ( $_options['child_inherit_changes_fields_control__variations_stock'] != 'no' ) {
				$this->stock_status_sync_fix( $resp, $variation );
			}

			/**
			 * If Price is supposed to sync
			 **/
			if ( $_options['child_inherit_changes_fields_control__variations_price'] != 'no' ) {
				if( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_variation_price', true ) === true ){
					update_post_meta( $resp, '_price', $variation_meta_data['_price'][0] );
				}
			}

			/**
			 * If Sale Price is supposed to sync
			 **/
			if ( $_options['child_inherit_changes_fields_control__variations_sale_price'] != 'no' ) {
				if( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_variation_sale_price', true ) === true ){
					update_post_meta( $resp, '_sale_price', $variation_meta_data['_sale_price'][0] );
				}
			}

			/**
			 * If Price is supposed to sync
			 **/
			if ( $_options['child_inherit_changes_fields_control__variations_price'] != 'no' ) {
				if( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_variation_regular_price', true ) === true ){
					update_post_meta( $resp, '_regular_price', $variation_meta_data['_regular_price'][0] );
				}
			}

			/**
			 * Master variation id
			 **/
			update_post_meta( $resp, '_woonet_master_product_id', $variation_parent_id );

			$_synced_variations[] = $resp;


            do_action( 'WOO_MSTORE_SYNC/sync_child/complete', $resp, $variation_parent_id, $variation );

		}

		if ( ! empty( $_existing_variations ) ) {
			foreach ( $_existing_variations as $variation_id ) {
				if ( ! in_array( $variation_id, $_synced_variations ) ) {
					wp_delete_post( $variation_id, true );
				}
			}
		}
	}

	/**
	 * Sync shipping class for products and variations.
	 */
	public function sync_shipping_class( $id, $product ) {
		if ( $this->get_option( 'child_inherit_changes_fields_control__shipping_class', 'no' ) != 'yes' ) {
			return;
		}

		if ( empty( $product['shipping_class'] ) ) {
			return wp_set_post_terms( $id, '', 'product_shipping_class' ); // delete post terms if exists
		}

		if ( ! empty( $product['shipping_class']['id'] ) ) {
			$term_id = $this->get_mapped_child_term( (int) $product['shipping_class']['id'] );

			if ( ! empty( $term_id ) ) {
				wp_update_term(
					$term_id,
					'product_shipping_class',
					array(
						'name'        => $product['shipping_class']['name'],
						'slug'        => $product['shipping_class']['slug'],
						'description' => $product['shipping_class']['description'],
					)
				);
			} else {
				$term = wp_insert_term(
					$product['shipping_class']['name'],
					'product_shipping_class',
					array(
						'slug'        => $product['shipping_class']['slug'],
						'description' => $product['shipping_class']['description'],
					)
				);

				if ( is_wp_error( $term ) ) {
					woomulti_log_error( 'Failed to insert shipping class.' );
					woomulti_log_error( $term->get_error_message() );
					return false;
				}

				if ( ! empty( $term['term_id'] ) ) {
					$term_id = $term['term_id'];
					update_term_meta( $term_id, '_woonet_master_term_id', $product['shipping_class']['id'] );
				} else {
					woomulti_log_error( 'Failed to create term for shipping_class.' );
					woomulti_log_error( $product['shipping_class'] );
					return;
				}
			}
		}

		if ( ! empty( $term_id ) ) {
			$term = get_term_by( 'id', $term_id, 'product_shipping_class' );

			if ( is_wp_error( $term ) ) {
				woomulti_log_error( 'get_term failed with error. Can not set shipping class.' );
				woomulti_log_error( $term->get_error_message() );
				return false;
			} elseif ( ! empty( $term->term_id ) ) {
				wp_set_post_terms( $id, $term->slug, 'product_shipping_class' );
			}
		}
	}

	/*
	 * Stock meta is commented out and synced from this function since 4.1.5.
	 * @todo refactor
	 */
	public function stock_status_sync_fix( $id, $product ) {

        $wp                         = wc_get_product( $id );
        $woonet_site_data           = get_option('woonet_master_connect');
        $woonet_site_options        = get_option('woonet_options');
        $is_variation               = false;

        if( $product['product'] && $product['product']['post_type'] && $product['product']['post_type'] == 'product_variation' ){
            $variation    = wc_get_product( $id );
            $parent       = wc_get_product( $variation->get_parent_id() );
            $id           = $parent->get_id();
	        $is_variation = true;
        }

        $product_meta_sync_stock    = get_post_meta( $id, '_woonet_' . $woonet_site_data['uuid'] . '_child_stock_synchronize', true );
        $site_sync_stock            = $woonet_site_options['override__synchronize-stock'];
        $global_sync_stock          = $woonet_site_options['synchronize-stock'];

        if( $site_sync_stock == 'yes' ){
            return;
        }

        if( $global_sync_stock == 'no' && $product_meta_sync_stock == 'no' && ! $is_variation){
            return;
        }

		/**
		 * Set false to stop syncing stock status
		 */
		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_stock_status', true ) ) {
			if ( ! empty( $product['stock_status'] ) ) {
				$wp->set_stock_status( $product['stock_status'] );
			} else {
				$wp->set_stock_status();
			}
		}

		/**
		 * Set false to stop syncing manage stock status.
		 */
		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_is_manage_stock', true ) ) {
			if ( ! empty( $product['manage_stock'] ) ) {
				$wp->set_manage_stock( $product['manage_stock'] );
			} else {
				$wp->set_manage_stock( null );
			}
		}

		/**
		 * Set false to stop syncing stock_quantity with the child
		 */
		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_is_stock_quantity', true ) ) {
			if ( ! empty( $product['stock_quantity'] ) ) {
				$wp->set_stock_quantity( $product['stock_quantity'] );
			} else {
				$wp->set_stock_quantity( null );
			}
		}

		/**
		 * Set false to stop syncing backorders with the child
		 */
		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_is_backorders', true ) === true
			&& WOO_MULTISTORE()->options_manager->get( 'child_inherit_changes_fields_control__allow_backorders' ) == 'yes' ) {
			if ( ! empty( $product['backorders'] ) ) {
				$wp->set_backorders( $product['backorders'] );
			} else {
				$wp->set_backorders( false );
			}
		}

		/**
		 * Set false to stop syncing low stock amount status with the child
		 */
		if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_low_stock', true ) ) {
			if ( ! empty( $product['low_stock'] ) ) {
				$wp->set_low_stock_amount( $product['low_stock'] );
			} else {
				$wp->set_low_stock_amount( null );
			}
		}

		$wp->save();
	}

	/**
	 * Sync product attributes
	 *
	 * @param mixed $child_product_id
	 * @param mixed $product
	 * @return void
	 */
	public function sync_attributes_meta( $wc_product, $product ) {
		if ( $this->get_option( 'child_inherit_changes_fields_control__attributes', 'yes' ) == 'no' ) {
			return;
		}

		if ( empty( $product['product_attributes'] ) ) {
			return;
		}

		$attributes               = $product['product_attributes'];
		$product_attributes_array = array();
		global $wpdb;
		global $blog_prefix;

		foreach ( $attributes as $attr ) {
			// process taxonomy.
			if ( ! empty( $attr['taxonomy'] ) ) {
				// check if taxonomy eixsts.
				$id =  $wpdb->get_var("SELECT attribute_id FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name='{$attr['taxonomy']['attribute_name']}'");

				if ( ! empty( $id ) ) {
					$attr_data = array(
						'label' => $attr['taxonomy']['attribute_label'],
						'name'  => $attr['taxonomy']['attribute_label'],
						'slug'  => $attr['taxonomy']['attribute_name'],
						'type'  => $attr['taxonomy']['attribute_type']
					);

					if ( $this->get_option( 'child_inherit_changes_fields_control__attribute_name', 'yes' ) == 'no' ) {
						unset( $attr_data['name'] );
					}

					$attr_data = apply_filters('wc_multistore_attribute_data', $attr_data );

					wc_update_attribute(
						$id,
						$attr_data
					);
				}

				if ( ! $id ) {
					$id = wc_create_attribute(
						array(
							'name'  => $attr['taxonomy']['attribute_label'],
							'label' => $attr['taxonomy']['attribute_label'],
							'slug'  => $attr['taxonomy']['attribute_name'],
							'type'  => $attr['taxonomy']['attribute_type']
						)
					);
				}

				do_action( 'WOO_MSTORE_SYNC/sync_child/attribute', $id, $attr );

				/**
				 * If taxonomy slug on the child is different from the master,
				 * call to term_exists will fail and terms will not be added correctly.
				 * So, we get the taxonomy name on the child by the taxonomy ID.
				 */
				// $_tax_name = wc_attribute_taxonomy_name_by_id( $id );
				$_tax_name = $attr['name'];

				// If taxonomy doesn't exists we create it.
				if ( ! taxonomy_exists( $_tax_name ) ) {
					register_taxonomy(
						$_tax_name,
						'product_variation',
						array(
							'hierarchical' => false,
							'label'        => ucfirst( $attr['taxonomy']['attribute_label'] ),
							'query_var'    => true,
							'rewrite'      => array( 'slug' => sanitize_title( $attr['name'] ) ), // The base slug
						)
					);
				}

				if ( ! is_wp_error( $id ) ) {
					$post_terms_to_add = array();

					foreach ( $attr['terms'] as $term ) {
						if ( ! term_exists( $term['name'], $_tax_name ) ) {
							$_trm = wp_insert_term(
								$term['name'],
								$_tax_name,
								array(
									// 'slug' => $term['slug'],
								)
							);
						}

						if ( ! array_key_exists( $term['slug'], $this->synced_attributes ) ) {

                            //wpml fix
                            if ( class_exists( 'SitePress' ) ) {
                                global $sitepress;
                                remove_filter( 'get_term', array( $sitepress, 'get_term_adjust_id' ), 1 );
                            }

                            // fetch the term again to get its slug
                            $_trm = get_term_by( 'name', $term['name'], $_tax_name );

                            //wpml fix
                            if ( class_exists( 'SitePress' ) ) {
                                global $sitepress;
                                add_filter( 'get_term', array( $sitepress, 'get_term_adjust_id' ), 1, 1 );
                            }

							if ( $_trm->slug ) {
								$this->synced_attributes[ $term['slug'] ] = $_trm->slug;
							}
						}

						// $post_term_names =  wp_get_post_terms( $child_product_id, $term['taxonomy'], array('fields' => 'names') );
						// Check if the post term exist and if not we set it in the parent variable product.
						// if( ! in_array( $term['name'], (array) $post_term_names ) ) {
						// $post_terms_to_add[] = $term['name'];
						// }
						$post_terms_to_add[] = $term['name'];

						do_action( 'WOO_MSTORE_SYNC/sync_child/product_term', $_trm->term_id, $term );
					}

					wp_set_post_terms( $wc_product->get_id(), $post_terms_to_add, $_tax_name, false );
				}
			}
		}
	}

	public function sync_variation_meta( $child_variation_id, $variation, $product = null ) {
		$_syncable_meta = $variation;

		unset( $_syncable_meta['_thumbnail_id'] );
		unset( $_syncable_meta['_price'] );
		unset( $_syncable_meta['_sale_price'] );
		unset( $_syncable_meta['_regular_price'] );

        if( empty( $_syncable_meta['_weight'] ) ){
            $_syncable_meta['_weight'] = '';
        }

        if( empty( $_syncable_meta['_length'] ) ){
            $_syncable_meta['_length'] = '';
        }

        if( empty( $_syncable_meta['_width'] ) ){
            $_syncable_meta['_width'] = '';
        }

        if( empty( $_syncable_meta['_height'] ) ){
            $_syncable_meta['_height'] = '';
        }

		/**
		 * If users migrated the product from a child store and then imported
		 * it back into a parent store, there could be some meta we use to identify
		 * a child product. So here we unset the meta.
		 */
		unset( $_syncable_meta['_woonet_master_product_id'] );

		foreach ( $_syncable_meta as $key => $meta_value ) {
			/**
			 * Sometimes child and master site may have different attributes slug.
			 * If variation meta contains a slug for attributes term that doesn't match
			 * the child term, linking will fail.
			 * This checks the term slugs and replace them if necessary.
			 */


			if ( strpos( $key, 'attribute_pa_' ) === 0
				&& isset( $this->synced_attributes[ $meta_value[0] ] )
				&& $this->synced_attributes[ $meta_value[0] ] != $meta_value[0] ) {
				$meta_value = $this->synced_attributes[ $meta_value[0] ];
			}

			if ( apply_filters( 'WOO_MSTORE_SYNC/sync_child/sync_variation_meta_' . sanitize_key( $key ), true ) ) {
				if( is_array( $meta_value ) ){
					delete_post_meta( $child_variation_id, $key );
					if( ! empty( $meta_value ) ){
						foreach ( $meta_value as $value ){
							add_post_meta( $child_variation_id, $key, $value );
						}
					}
				}else{
					update_post_meta( $child_variation_id, $key, $meta_value );
				}
			}
		}
	}

	public function get_category_tree( $product_id, $cat_id = null ) {
		if ( ! is_null( $product_id ) ) {
			$cats = get_the_terms( $product_id, 'product_cat' );
		} else {
			$cats = array(
				get_term( $cat_id ),
			);
		}
		$cats_tree = array();

		foreach ( $cats as $cat ) {
			$ancestors = get_ancestors( $cat->term_id, 'product_cat' );
			$ancestors = array_reverse( $ancestors );

			if ( ! empty( $ancestors ) ) {
				foreach ( $ancestors as $ancestor ) {
					$thumbnail = array(
						'__thumbnail' => array(),
					);

					if ( ! empty( $thumbnail_id = get_term_meta( $ancestor, 'thumbnail_id', true ) ) ) {
						$thumbnail['__thumbnail'] = array(
							'id'  => $thumbnail_id,
							'url' => wp_get_attachment_url( $thumbnail_id ),
							'attachment' => get_post( $thumbnail_id ),
							'meta'       => get_post_meta( $thumbnail_id, '_wp_attachment_metadata', true ),
							'metadata'   => array(
								'_wp_attachment_image_alt' => get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true ),
							),
							'wp_uploads_dir'  => wp_get_upload_dir(),
						);
					}

					$cats_tree[ $cat->term_id ][] = array_merge(
						(array) get_term( $ancestor ),
						(array) $thumbnail,
						array(
							'order' => get_term_meta( $ancestor, 'order', true ),
						),
						array(
							'meta' => get_term_meta( $ancestor ),
						)
					);
				}
			}

			$thumbnail = array(
				'__thumbnail' => array(),
			);

			if ( $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true ) ) {
				$thumbnail['__thumbnail'] = array(
					'id'  => $thumbnail_id,
					'url' => wp_get_attachment_url( $thumbnail_id ),
					'attachment' => get_post( $thumbnail_id ),
					'meta'       => get_post_meta( $thumbnail_id, '_wp_attachment_metadata', true ),
					'metadata'   => array(
						'_wp_attachment_image_alt' => get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true ),
					),
					'wp_uploads_dir'  => wp_get_upload_dir(),
				);
			}

			$cat_data = array_merge(
				(array) $cat,
				(array) $thumbnail,
				array(
					'order' => get_term_meta( $cat->term_id, 'order', true ),
				),
				array(
					'meta' => get_term_meta( $cat->term_id ),
				)
			);

			$cats_tree[ $cat->term_id ][] = apply_filters(
				'WOO_MSTORE_SYNC/process_json/cat',
				$cat_data,
				$cat->term_id
			);
		}

		return $cats_tree;
	}

	/**
	 * Check if the request to master or child site is authenticated
	 */
	public function is_request_authenticated( $auth_in_post_body = false ) {
		$headers = getallheaders();

		if ( ! empty( $auth_in_post_body['Authorization'] ) ) {
			$headers['Authorization'] = $auth_in_post_body['Authorization'];
		}

		if ( empty( $headers['Authorization'] ) ) {
			woomulti_log_error( 'Authentication Error: Authorization header does not exists.' );
			return false;
		}

		if ( get_option( 'woonet_network_type' ) == 'master' ) {
			$data = get_option( 'woonet_child_sites' );

			foreach ( $data as $value ) {
				if ( $value['site_key'] == $headers['Authorization'] ) {
					return true;
				}
			}
		} else {
			$data = get_option( 'woonet_master_connect' );

			if ( $data['key'] == $headers['Authorization'] ) {
				return true;
			}
		}

		return false;
	}

	public function fetch_child_orders( $page = 1, $per_page = 10, $post_status = '', $search = '', $site_id = null, $post_type = '' ) {
		/**
		 * If all no site UUId is sent, we need to unset.
		 */
		if ( $site_id == 'all' ) {
			$site_id = null;
		}
		$data = array(
			'page'        => $page,
			'per_page'    => $per_page,
			'post_status' => $post_status,
			'search'      => $search,
            'post_type'   => $post_type
		);

		return $this->request_child( 'woomulti_orders', $data, $site_id );
	}

	public function stock_sync( $site, $payload, $network_type = 'master' ) {
		if ( $network_type == 'master' ) {
			$data = array(
				'action'        => 'child_receive_stock_updates',
				'post_data'     => $payload,
				'Authorization' => $site['site_key'],
			);

			$url = trim( $site['site_url'] ) . '/wp-admin/admin-ajax.php';

			$headers = array(
				'Authorization' => $site['site_key'],
			);

			$result = wp_remote_post(
				$url,
				array(
					'headers' => $headers,
					'body'    => $data,
				)
			);

			/**
			 * If debug is enabled, fire up this action to assist in debugging
			 * connection issues.
			 * On client sites, one can enable debugging and hook into this action
			 * to examine the results.
			 */
			if ( defined( 'WP_DEBUG' ) && WP_DEBUG == true ) {
				do_action( 'WOO_MSTORE_SYNC/HTTP_REQUEST_RESULT', $data, $url, $result );
			}
		} else {
			$data = array(
				'action'        => 'master_receive_stock_updates',
				'post_data'     => $payload,
				'Authorization' => $site['key'], // the index is key on the child.
			);

			$url = trim( $site['master_url'] ) . '/wp-admin/admin-ajax.php';

			$headers = array(
				'Authorization' => $site['key'], // the index is key on the child.
			);

			$result = wp_remote_post(
				$url,
				array(
					'headers' => $headers,
					'body'    => $data,
				)
			);

			/**
			 * If debug is enabled, fire up this action to assist in debugging
			 * connection issues.
			 * On client sites, one can enable debugging and hook into this action
			 * to examine the results.
			 */
			if ( defined( 'WP_DEBUG' ) && WP_DEBUG == true ) {
				do_action( 'WOO_MSTORE_SYNC/HTTP_REQUEST_RESULT', $data, $url, $result );
			}
		}

		if ( is_wp_error( $result ) ) {
			$error_message = $result->get_error_message();
			woomulti_log_error( 'Stock Reduce: HTTP ERROR ' . $error_message );
			woomulti_log_error( $result );
			return;
		} else {
			// response received from child site
			if ( isset( $result['response']['code'] ) && $result['response']['code'] != 200 ) {
				woomulti_log_error( 'Stock Reduce: HTTP ERROR' );
				woomulti_log_error( $result['body'] );
				return $result['body'];
			}
		}
	}

	/**
	 * Sync upsell and crossell
	 *
	 * Translate the upsell and crosssell product IDs received from the parent sites
	 * to corresponding child site IDs and update the cross-sell and upsells.
	 *
	 * @param integer $id Mapped product ID on the child site
	 * @param array   $product Product JSON (array) received from parent
	 * @return null;
	 * @since 3.0.1
	 */
	public function sync_upsell_cross_sell( $id, $product ) {
		$types    = array( 'upsell', 'crosssell' );
		$_options = get_option( 'woonet_options' );

		foreach ( $types as $type ) {
			if ( ! isset( $product[ $type ] ) ) {
				continue;
			}

			if ( $this->get_option( 'child_inherit_changes_fields_control__upsell', 'no' ) == 'no' && $type == 'upsell' ) {
				continue;
			}

			if ( $this->get_option( 'child_inherit_changes_fields_control__cross_sells', 'no' ) == 'no' && $type == 'crosssell' ) {
				continue;
			}

			$product_ids             = $product[ $type ];
			$_translated_product_ids = array();
			$parent_sku              = $product['product']['sku'];
			$parent_upsell_skus      = $product['upsell_skus'];
			$parent_crossell_skus    = $product['crosssell_skus'];

			if ( $this->get_option( 'sync-by-sku', 'no' ) == 'yes' ) {
				if ( $type == 'upsell' ) {
					if ( ! empty( $parent_upsell_skus ) ) {
						foreach ( $parent_upsell_skus as $parent_upsell_sku ) {
							if ( $mapped_id = $this->get_mapped_child_post( $id, $parent_upsell_sku ) ) {
								$_translated_product_ids[] = $mapped_id;
							}
						}
					}
				}

				if ( $type == 'crosssell' ) {
					if ( ! empty( $parent_crossell_skus ) ) {
						foreach ( $parent_crossell_skus as $parent_crossell_sku ) {
							if ( $mapped_id = $this->get_mapped_child_post( $id, $parent_crossell_sku ) ) {
								$_translated_product_ids[] = $mapped_id;

							}
						}
					}
				}
			} else {
				if ( ! empty( $product_ids ) ) {
					foreach ( $product_ids as $product_id ) {
						if ( $mapped_id = $this->get_mapped_child_post( $product_id, $parent_sku ) ) {
							$_translated_product_ids[] = $mapped_id;
						}
					}
				}
			}

			if ( ! empty( $_translated_product_ids ) ) {
				update_post_meta( $id, '_' . $type . '_ids', $_translated_product_ids );
			} else {
				delete_post_meta( $id, '_' . $type . '_ids' );
			}
		}
	}

	/**
	 * Sync grouped products
	 *
	 * Translate the grouped product IDs received from the master site
	 * and sync them with the child site
	 *
	 * @param integer $id Mapped product ID on the child site
	 * @param array   $product Product JSON (array) received from parent
	 * @return null;
	 * @since 3.0.1
	 */
	public function sync_grouped_products( $id, $product ) {
		if ( $product['product_type'] != 'grouped' ) {
			// This is not a grouped product.
			return;
		}

		$product_ids             = $product['grouped_product_ids'];
		$_translated_product_ids = array();
		$parent_skus             = $product['grouped_product_skus'];
		$parent_sku              = $product['product']['sku'];

		if ( $this->get_option( 'sync-by-sku', 'no' ) == 'yes' ) {
			if ( ! empty( $parent_skus ) ) {
				foreach ( $parent_skus as $product_sku ) {
					if ( $mapped_id = $this->get_mapped_child_post( $id, $product_sku ) ) {
						$_translated_product_ids[] = $mapped_id;
					}
				}
			}
		} else {
			if ( ! empty( $product_ids ) ) {
				foreach ( $product_ids as $product_id ) {
					if ( $mapped_id = $this->get_mapped_child_post( $product_id, $parent_sku ) ) {
						$_translated_product_ids[] = $mapped_id;
					}
				}
			}
		}

		if ( ! empty( $_translated_product_ids ) ) {
			update_post_meta( $id, '_children', $_translated_product_ids );
		} else {
			delete_post_meta( $id, '_children' );
		}
	}

	/**
	 * Sync order status
	 *
	 * Send the updated order status on the child site from the master.
	 *
	 * @param array $posts_data A multidimentional array with post data grouped into site ID
	 * @since 3.0.3
	 */
	public function sync_order_status( $posts_data ) {
		$sites         = get_option( 'woonet_child_sites' );
		$site_response = array();

		if ( ! empty( $sites ) ) {
			foreach ( $sites as $site_data ) {
				if ( empty( $posts_data[ $site_data['uuid'] ] ) ) {
					continue;
				}

				$data = array(
					'action'        => 'woomulti_order_status',
					'Authorization' => $site_data['site_key'],
					'post_data'     => $posts_data[ $site_data['uuid'] ],
				);

				$url = $site_data['site_url'] . '/wp-admin/admin-ajax.php';

				$headers = array(
					'Authorization' => $site_data['site_key'],
				);

				$result = wp_remote_post(
					$url,
					array(
						'headers' => $headers,
						'body'    => $data,
					)
				);

					/**
				 * If debug is enabled, fire up this action to assist in debugging
				 * connection issues.
				 * On client sites, one can enable debugging and hook into this action
				 * to examine the results.
				 */
				if ( defined( 'WP_DEBUG' ) && WP_DEBUG == true ) {
					do_action( 'WOO_MSTORE_SYNC/HTTP_REQUEST_RESULT', $data, $url, $result );
				}

				if ( ! is_wp_error( $result ) ) {
					$resp = json_decode( stripslashes( $result['body'] ), JSON_OBJECT_AS_ARRAY );

					/**
					 * Sometimes the string may not require un-quoting, in which case the above will return null.
					 * If null, then run json_decode without stripslashes.
					 */
					if ( is_null( $resp ) ) {
						$resp = json_decode( $result['body'], JSON_OBJECT_AS_ARRAY );
					}

					if ( ! empty( $resp['status'] ) && ! empty( $resp['message'] ) ) {
						$site_response[ $site_data['uuid'] ] = array(
							'status'  => $resp['status'],
							'message' => $resp['message'],
						);
					} else {
						$site_response[ $site_data['uuid'] ] = array(
							'status'  => 'failed',
							'message' => 'Child site (' . esc_url( $site_data['site_url'] ) . ') did not send a response. Please check that you are running version 3.0.3 or greater on the child site. You may need to manually update the plugin on the child site.',
						);
					}
				} else {
					$site_response[ $site_data['uuid'] ] = array(
						'status'  => 'failed',
						'message' => $result->get_error_message(),
					);

					woomulti_log_error( 'HTTP ERROR: Order status update failed.' );
					woomulti_log_error( $result );
				}
			}
		}

		return $site_response;
	}

	/**
	 * Get all variation IDs of a product
	 */
	public function get_all_variation_ids( $product_id ) {
		$all_args = array(
			'post_parent' => $product_id,
			'post_type'   => 'product_variation',
			'orderby'     => array(
				'menu_order' => 'ASC',
				'ID'         => 'ASC',
			),
			'fields'      => 'ids',
			'post_status' => array( 'publish', 'private' ),
			'numberposts' => -1, // phpcs:ignore WordPress.VIP.PostsPerPage.posts_per_page_numberposts
		);

		$ids = get_posts( $all_args );

		if ( $ids ) {
			return wp_parse_id_list( (array) $ids );
		}

		return null;
	}

	/**
	 * Check if meta key is whitelisted
	 *
	 * @since 3.0.4
	 *
	 * @param string $meta_key The meta key
	 * @return bool
	 */
	public function is_meta_white_listed( $meta_key ) {
		if ( in_array( $meta_key, $this->whitelisted_metadata ) ) {
			return true;
		}

		foreach ( $this->whitelisted_metadata as $_meta ) {
			if ( substr( $_meta, 0 ) == '%' || substr( $_meta, -1 ) == '%' ) {
				$_match = str_replace( '%', '', $_meta );
				if ( strpos( $meta_key, $_match ) !== false ) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Return all whitelisted metadata
	 *
	 * @since 3.0.4
	 *
	 * @param string $product_id Product ID
	 * @return array
	 */
	public function get_white_listed_metadata( $product_id, $wc_product ) {
		$_meta             = get_post_meta( $product_id );
		$_whitelisted_meta = array();

		if ( empty( $_meta ) ) {
			woomulti_log_error( 'Failed to retrieve metadata.' );
			return array();
		}

		foreach ( $_meta as $key => $value ) {
			if ( $this->is_meta_white_listed( $key ) ) {
				if( is_array( $value ) ){
					foreach ( $value as $meta_value ){
						$_whitelisted_meta[ $key ][] = maybe_unserialize( $meta_value );
					}
				}else{
					$_whitelisted_meta[ $key ] = maybe_unserialize( $value );
				}
			}
		}

		$_whitelisted_meta = apply_filters( 'WOO_MSTORE_SYNC/process_json/meta', $_whitelisted_meta, $product_id, $wc_product );

		return $_whitelisted_meta;
	}

	/**
	 * Get the value of a setting
	 *
	 * @since 3.0.4
	 *
	 * @param string $option_name The key of the option defined on the settings page
	 * @param string $default Default value to return, if key does not exist.
	 * @return string Either the value defined in settings or the default
	 */
	public function get_option( $option_name, $default = 'no' ) {
		if ( empty( $this->options ) ) {
			$this->options = get_option( 'woonet_options' );
		}

		if ( isset( $this->options[ $option_name ] ) ) {
			return $this->options[ $option_name ];
		}

		return $default;
	}

	/**
	 * Sync order status
	 *
	 * Send the updated order status on the child site from the master.
	 *
	 * @param integer $parent_post_id The post id of the parent post
	 * @param string  $status The changed status of the post
	 * @since 3.0.3
	 */
	public function trash_untrash_delete_post( $parent_post_id, $sku, $status ) {
		$sites         = get_option( 'woonet_child_sites' );
		$site_response = array();

		if ( ! empty( $sites ) ) {
			foreach ( $sites as $site_data ) {
				$data = array(
					'action'             => 'woomulti_trash_untrash',
					'Authorization'      => $site_data['site_key'],
					'parent_post_id'     => $parent_post_id,
					'parent_post_status' => $status,
					'parent_sku'         => $sku,
				);

				$url = $site_data['site_url'] . '/wp-admin/admin-ajax.php';

				$headers = array(
					'Authorization' => $site_data['site_key'],
				);

				$result = wp_remote_post(
					$url,
					array(
						'headers' => $headers,
						'body'    => $data,
					)
				);

				/**
				 * If debug is enabled, fire up this action to assist in debugging
				 * connection issues.
				 * On client sites, one can enable debugging and hook into this action
				 * to examine the results.
				 */
				if ( defined( 'WP_DEBUG' ) && WP_DEBUG == true ) {
					do_action( 'WOO_MSTORE_SYNC/HTTP_REQUEST_RESULT', $data, $url, $result );
				}

				if ( ! is_wp_error( $result ) ) {
					$resp = json_decode( stripslashes( $result['body'] ), JSON_OBJECT_AS_ARRAY );

					/**
					 * Sometimes the string may not require un-quoting, in which case the above will return null.
					 * If null, then run json_decode without stripslashes.
					 */
					if ( is_null( $resp ) ) {
						$resp = json_decode( $result['body'], JSON_OBJECT_AS_ARRAY );
					}

					if ( ! empty( $resp['status'] ) && ! empty( $resp['message'] ) ) {
						$site_response[ $site_data['uuid'] ] = array(
							'status'  => $resp['status'],
							'message' => $resp['message'],
						);
					} else {
						$site_response[ $site_data['uuid'] ] = array(
							'status'  => 'failed',
							'message' => 'Child site (' . esc_url( $site_data['site_url'] ) . ') did not send a response. Please check that you are running version 3.0.5 or greater on the child site. You may need to manually update the plugin on the child site.',
						);
					}
				} else {
					$site_response[ $site_data['uuid'] ] = array(
						'status'  => 'failed',
						'message' => $result->get_error_message(),
					);

					woomulti_log_error( 'HTTP ERROR: Trash product failed.' );
					woomulti_log_error( $result );
				}
			}
		}

		return $site_response;
	}

	/**
	 * Run on the master network. Send the site URL and the license key to check for updates.
	 *
	 * @return string
	 */
	public function send_data_for_update() {
		$license_data = get_option( 'mstore_license' );

		return json_encode(
			array(
				'key'    => $license_data['key'],
				'domain' => WOO_MSTORE_INSTANCE,
			)
		);
	}

	public function get_data_for_update() {
		$site = get_option( 'woonet_master_connect' );

		$data = array(
			'action'        => 'send_update_data',
			'Authorization' => $site['key'],
		);

		$url = trim( $site['master_url'] ) . '/wp-admin/admin-ajax.php';

		$headers = array(
			'Authorization' => $site['key'],
		);

		$result = wp_remote_post(
			$url,
			array(
				'headers' => $headers,
				'body'    => $data,
			)
		);

		/**
		 * If debug is enabled, fire up this action to assist in debugging
		 * connection issues.
		 * On client sites, one can enable debugging and hook into this action
		 * to examine the results.
		 */
		if ( defined( 'WP_DEBUG' ) && WP_DEBUG == true ) {
			do_action( 'WOO_MSTORE_SYNC/HTTP_REQUEST_RESULT', $data, $url, $result );
		}

		if ( is_wp_error( $result ) ) {
			$error_message = $result->get_error_message();
			woomulti_log_error( 'Child site Update: HTTP ERROR ' . $error_message );
			woomulti_log_error( $result );
			return;
		} else {
			// response received from child site.
			if ( isset( $result['response']['code'] ) && $result['response']['code'] != 200 ) {
				woomulti_log_error( 'Child site Update: HTTP ERROR' );
				woomulti_log_error( $result['body'] );
				return;
			} else {
				return $result['body'];
			}
		}
	}

	private function get_product_tags( $product_id ) {
		$terms       = get_the_terms( $product_id, 'product_tag' );
		$terms_array = array();

		if ( ! empty( $terms ) ) {
			foreach ( $terms as $term ) {
				$terms_array[] = (array) apply_filters(
					'WOO_MSTORE_SYNC/process_json/tag',
					(array) $term,
					$product_id
				);
			}
		}

		return $terms_array;
	}

	public function _get_product_attributes( $product_attributes ) {
		$product_attributes_array = array();

		foreach ( $product_attributes as $pa ) {
			$terms       = $pa->get_terms();
			$terms_array = array();

			if ( ! empty( $terms ) ) {
				foreach ( $terms as $term ) {
					$terms_array[] = apply_filters( 'WOO_MSTORE_SYNC/process_json/product_term', (array) $term, $term->term_id );
				}
			}

			$attr = array(
				'id'       => $pa->get_id(),
				'name'     => $pa->get_name(),
				'slug'     => $pa->get_name(), // name is slug
				'options'  => $pa->get_options(),
				'terms'    => $terms_array,
				'taxonomy' => $pa->get_taxonomy_object(),
			);

			$product_attributes_array[] = apply_filters( 'WOO_MSTORE_SYNC/process_json/attribute', $attr, $pa->get_id() );
		}

		return $product_attributes_array;
	}

	/**
	 * Send data payload to a connected site with custom action.
	 *
	 * @param mixed $data
	 * @param mixed $site_id
	 * @return void
	 */
	public function send_payload( $payload_data, $site_id = null ) {
		return $this->request_child( 'woomulti_custom_payload', $payload_data, $site_id );
	}


	/**
	 * Send a request to the child site or the master site.
	 *
	 * @param string $action AJAX action name
	 * @param string $payload_data data to send with POST request
	 * @param mixed  $site_id If the request is for a particular site ID, set site ID
	 * @param string $request_type HTTP Request type - GET or POST.
	 * @return array Array with respose from each site requested
	 *
	 * @since 4.0.0
	 */
	public function request_child( $action, $payload_data = null, $site_id = null, $request_type = 'post' ) {
		$sites         = get_option( 'woonet_child_sites' );
		$site_response = array();

		if ( ! empty( $sites ) ) {
			foreach ( $sites as $site_data ) {
				if ( $site_id !== null && ! is_array( $site_id ) ) {
					$site_id = array( $site_id );
				}

				if ( ! empty( $site_id ) && ! in_array( $site_data['uuid'], $site_id ) ) {
					continue;
				}

				$data = array(
					'action'        => $action,
					'Authorization' => $site_data['site_key'],
					'data'          => $payload_data,
				);

				$url = $site_data['site_url'] . '/wp-admin/admin-ajax.php';

				$headers = array(
					'Authorization' => $site_data['site_key'],
				);

				if ( 'post' === $request_type ) {
					$result = wp_remote_post(
						$url,
						array(
							'headers' => $headers,
							'body'    => $data,
						)
					);

					/**
					 * If debug is enabled, fire up this action to assist in debugging
					 * connection issues.
					 * On client sites, one can enable debugging and hook into this action
					 * to examine the results.
					 */
					if ( defined( 'WP_DEBUG' ) && WP_DEBUG == true ) {
						do_action( 'WOO_MSTORE_SYNC/HTTP_REQUEST_RESULT', $data, $url, $result );
					}
				} else {
					// @todo: implement get request and other requst methods.
				}

				if ( ! is_wp_error( $result ) ) {
					$resp = json_decode( stripslashes( $result['body'] ), JSON_OBJECT_AS_ARRAY );

					/**
					 * Sometimes the string may not require un-quoting, in which case the above will return null.
					 * If null, then run json_decode without stripslashes.
					 */
					if ( is_null( $resp ) ) {
						$resp = json_decode( $result['body'], JSON_OBJECT_AS_ARRAY );
					}

					if ( ! empty( $resp['status'] ) && ! empty( $resp['result'] ) ) {
						$site_response[ $site_data['uuid'] ] = array(
							'status'  => 'success',
							'result'  => $resp['result'],
							'message' => ! empty( $resp['message'] ) ? $resp['message'] : '',
						);
					} else {
						$site_response[ $site_data['uuid'] ] = array(
							'status'  => 'failed',
							'result'  => null,
							'message' => 'Child site (' . esc_url( $site_data['site_url'] ) . ') did not send a response. Please check if all your child sites are up and you are running the same version on all of them. You may need to manually update the plugin if you are running a version older than 4.0.0',
						);
					}
				} else {
					$site_response[ $site_data['uuid'] ] = array(
						'status'  => 'failed',
						'result'  => null,
						'message' => $result->get_error_message(),
					);

					woomulti_log_error( "HTTP ERROR: action:{$action}, site_id:{$site_id}, request_type:{$request_type}" );
					woomulti_log_error( $result );
				}
			}
		}

		return $site_response;
	}

	/**
	 * Send a request to the master site.
	 *
	 * @param string $action AJAX action name
	 * @param string $payload_data data to send with POST request
	 * @param string $request_type HTTP Request type - GET or POST.
	 * @return array Array with respose from each site requested
	 *
	 * @since 4.1.2
	 */
	public function request_master( $action, $payload_data = null, $request_type = 'post' ) {
		$master_site   = get_option( 'woonet_master_connect' );
		$site_response = array();

		if ( ! empty( $master_site ) ) {
			$data = array(
				'action'        => $action,
				'Authorization' => $master_site['key'],
				'data'          => json_encode( $payload_data ),
			);

			$url = $master_site['master_url'] . '/wp-admin/admin-ajax.php';

			$headers = array(
				'Authorization' => $master_site['key'],
			);

			if ( 'post' === $request_type ) {
				$result = wp_remote_post(
					$url,
					array(
						'headers' => $headers,
						'body'    => $data,
					)
				);

				/**
				 * If debug is enabled, fire up this action to assist in debugging
				 * connection issues.
				 * On client sites, one can enable debugging and hook into this action
				 * to examine the results.
				 */
				if ( defined( 'WP_DEBUG' ) && WP_DEBUG == true ) {
					do_action( 'WOO_MSTORE_SYNC/HTTP_REQUEST_RESULT', $data, $url, $result );
				}
			} else {
				// @todo: implement get request and other requst methods.
			}

			if ( ! is_wp_error( $result ) ) {
				$resp = json_decode( stripslashes( $result['body'] ), JSON_OBJECT_AS_ARRAY );

				/**
				 * Sometimes the string may not require un-quoting, in which case the above will return null.
				 * If null, then run json_decode without stripslashes.
				 */
				if ( is_null( $resp ) ) {
					$resp = json_decode( $result['body'], JSON_OBJECT_AS_ARRAY );
				}

				if ( ! empty( $resp['status'] ) && ! empty( $resp['result'] ) ) {
					$site_response = array(
						'status'  => 'success',
						'result'  => $resp['result'],
						'message' => ! empty( $resp['message'] ) ? $resp['message'] : '',
					);
				} else {
					$site_response = array(
						'status'  => 'failed',
						'result'  => null,
						'message' => 'Child site (' . esc_url( $master_site['master_url'] ) . ') did not send a response. Please check if all your child sites are up and you are running the same version on all of them. You may need to manually update the plugin if you are running a version older than 4.0.0',
					);
				}
			} else {
				$site_response = array(
					'status'  => 'failed',
					'result'  => null,
					'message' => $result->get_error_message(),
				);

				woomulti_log_error( "HTTP ERROR: action:{$action}, request_type:{$request_type}" );
				woomulti_log_error( $result );
			}
		}

		return $site_response;
	}

	/**
	 * Get options/settings from each site.
	 *
	 * @return array
	 */
	public function get_options() {
		return $this->request_child( 'woomulti_child_options_get' );
	}

	/**
	 * Update options/settings from each site.
	 *
	 * @param $data options for all sites
	 * @return array
	 */
	public function update_options( $data ) {
		return $this->request_child( 'woomulti_child_options_update', $data );
	}

	/**
	 * Get version from the child sites
	 *
	 * @param $data options for all sites
	 * @return array
	 */
	public function get_versions() {
		return $this->request_child( 'woomulti_version' );
	}

	/**
	 * Get blog names.
	 *
	 * @return array
	 */
	public function get_blogname() {
		$network_type = get_option( 'woonet_network_type' );

		if ( $network_type == 'master' ) {
			return $this->request_child( 'woomulti_get_blognames' );
		} else if( $network_type == 'child' ) {
			return $this->request_master( 'woomulti_get_blognames' );
		} else {
			woomulti_log_error( 'Unknown network type.' );
		}
	}

	/**
	 * Get order data for export function.
	 *
	 * @return array
	 */
	public function get_order_exports( $data, $site_id ) {
		return $this->request_child( 'woomulti_get_order_exports', $data, $site_id );
	}

	/**
	 * check_connection_details
	 *
	 * @param mixed $data
	 * @return void
	 */
	public function check_connection_details() {
		return $this->request_child( 'woomulti_check_site_connection' );
	}

	/**
	 * Fix duplicate SKU bug
	 *
	 * Version 4.0.0 switched to WooCommerce's get_sku() function to get
	 * variable SKU instead of directly updating the metadata. This introduced
	 * a duplicate SKU bug on the child.
	 *
	 * Check if a product is a variable product. And if variations have the same SKU as the main product,
	 * remove the SKU from variations.
	 */
	private function __fix_variation_sku_duplication_bug( $wc_product ) {
		if ( empty( $wc_product->get_sku() ) || $wc_product->get_type() != 'variable' ) {
			return;
		}

		$_existing_variations = $this->get_all_variation_ids( $wc_product->get_id() );

		if ( ! empty( $_existing_variations ) ) {
			foreach ( $_existing_variations as $vid ) {
				$_sku = get_post_meta( $vid, '_sku', true );

				if ( ! empty( $_sku ) && $_sku == $wc_product->get_sku() ) {
					delete_post_meta( $vid, '_sku' );
				}
			}
		}
	}

	private function force_product_type_convertion( $wc_product, $master_type ) {
		wp_remove_object_terms( $wc_product->get_id(), $wc_product->get_type(), 'product_type' );
		wp_set_object_terms( $wc_product->get_id(), $master_type, 'product_type' );
	}
}
