<?php
/**
 * Install handler.
 *
 * This handles plugin install related functionality in Woocommerce Multistore.
 *
 */

defined( 'ABSPATH' ) || exit;

/**
 * Class WC_Multistore_Install
 */
class WC_Multistore_Install {
	/**
	 * install
	 **/
	public static function install() {
		$manager               = new WC_Multistore_Functions();
		$default_options = $manager->get_defaults();
		$options = get_option( 'woonet_options', array() );

		foreach ( $default_options as $key => $value ) {
			if ( ! isset( $options[ $key ] ) ) {
				$options[ $key ] = $value;
			}
		}

		update_option( 'woonet_options', $options );
	}
}
