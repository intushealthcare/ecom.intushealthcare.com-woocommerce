<?php

defined( 'ABSPATH' ) || exit;

/**
 * Class WC Multistore Export Order
 */
class WC_Multistore_Export_Order{

	/**
	 * @var array
	 */
	private $system_messages = array();

	/**
	 * @var string[]
	 */
	private $network_fields = array(
		'site_id'  => 'Site Name',
	);

	/**
	 * @var string[]
	 */
	private $order_fields = array(
		'id'                   => '',
		'order_number'         => '',
		'items'                => '',
		'parent_id'            => '',
		'status'               => '',
		'currency'             => '',
		'version'              => '',
		'prices_include_tax'   => '',
		'date_created'         => '',
		'date_modified'        => '',
		'discount_total'       => '',
		'discount_tax'         => '',
		'shipping_total'       => '',
		'shipping_tax'         => '',
		'cart_tax'             => '',
		'total'                => '',
		'total_tax'            => '',
		'customer_id'          => '',
		'order_key'            => '',
		'billing_first_name'   => 'Billing First Name',
		'billing_last_name'    => 'Billing Last Name',
		'billing_company'      => 'Billing Company',
		'billing_address_1'    => 'Billing Address 1',
		'billing_address_2'    => 'Billing Address 2',
		'billing_city'         => 'Billing City',
		'billing_postcode'     => 'Billing Postal/Zip Code',
		'billing_state'        => 'Billing State',
		'billing_country'      => 'Billing Country',
		'billing_phone'        => 'Phone Number',
		'billing_email'        => 'Email Address',
		'shipping_first_name'  => 'Shipping First Name',
		'shipping_last_name'   => 'Shipping Last Name',
		'shipping_company'     => 'Shipping Company',
		'shipping_address_1'   => 'Shipping Address 1',
		'shipping_address_2'   => 'Shipping Address 2',
		'shipping_city'        => 'Shipping City',
		'shipping_postcode'    => 'Shipping Postal/Zip Code',
		'shipping_state'       => 'Shipping State',
		'shipping_country'     => 'Shipping Country',
		'payment_method'       => '',
		'payment_method_title' => '',
		'transaction_id'       => '',
		'customer_ip_address'  => '',
		'customer_user_agent'  => '',
		'created_via'          => '',
		'customer_note'        => '',
		'date_completed'       => '',
		'date_paid'            => '',
		'cart_hash'            => '',
		'meta_data'            => '',
	);

	/**
	 * @var string[]
	 */
	private $order_item_fields = array(
		'product_id'   => '',
		'variation_id' => '',
		'quantity'     => '',
		'tax_class'    => '',
		'subtotal'     => '',
		'subtotal_tax' => '',
		'total'        => '',
		'total_tax'    => '',
		'taxes'        => '',
		'meta'         => '',
	);

	/**
	 * @var string[]
	 */
	private $order_item_product_fields = array(
		'name'               => '',
		'slug'               => '',
		'date_created'       => '',
		'date_modified'      => '',
		'status'             => '',
		'featured'           => '',
		'catalog_visibility' => '',
		'description'        => '',
		'short_description'  => '',
		'sku'                => '',
		'price'              => '',
		'regular_price'      => '',
		'sale_price'         => '',
		'date_on_sale_from'  => '',
		'date_on_sale_to'    => '',
		'total_sales'        => '',
		'tax_status'         => '',
		'tax_class'          => '',
		'manage_stock'       => '',
		'stock_quantity'     => '',
		'stock_status'       => '',
		'backorders'         => '',
		'low_stock_amount'   => '',
		'sold_individually'  => '',
		'weight'             => '',
		'length'             => '',
		'width'              => '',
		'height'             => '',
		'upsell_ids'         => '',
		'cross_sell_ids'     => '',
		'parent_id'          => '',
		'reviews_allowed'    => '',
		'purchase_note'      => '',
		'attributes'         => '',
		'default_attributes' => '',
		'menu_order'         => '',
		'virtual'            => '',
		'downloadable'       => '',
		'category_ids'       => '',
		'tag_ids'            => '',
		'shipping_class_id'  => '',
		'downloads'          => '',
		'image_id'           => '',
		'gallery_image_ids'  => '',
		'download_limit'     => '',
		'download_expiry'    => '',
		'rating_counts'      => '',
		'average_rating'     => '',
		'review_count'       => '',
		'meta'               => '',
	);

	/**
	 * @var string[]
	 */
	private $order_item_shipping_fields = array(
		'method_title' => '',
		'method_id'    => '',
		'instance_id'  => '',
		'total'        => '',
		'total_tax'    => '',
		'taxes'        => '',
	);

	/**
	 * @var string[]
	 */
	private $order_item_tax_fields = array(
		'rate_code'          => '',
		'rate_id'            => '',
		'label'              => '',
		'compound'           => '',
		'tax_total'          => '',
		'shipping_tax_total' => '',
	);

	/**
	 * @var string[]
	 */
	private $order_item_coupon_fields = array(
		'code'         => '',
		'discount'     => '',
		'discount_tax' => '',
	);

	/**
	 * @var string[]
	 */
	private $order_item_fee_fields = array(
		'tax_class'  => '',
		'tax_status' => '',
		'amount'     => '',
		'total'      => '',
		'total_tax'  => '',
		'taxes'      => '',
	);

	/**
	 * @var array
	 */
	public $errors_log = array();

	/**
	 * @var string
	 */
	private $export_type = '';

	/**
	 * @var string
	 */
	private $export_time_after = '';

	/**
	 * @var string
	 */
	private $export_time_before = '';

	/**
	 * @var array
	 */
	private $site_filter = array();

	/**
	 * @var array
	 */
	private $order_status = array();

	/**
	 * @var string
	 */
	private $row_format = '';

	/**
	 * @var array
	 */
	private $export_fields = array();

	/**
	 *
	 */
	public function __construct() {
		$this->includes();
		$this->hooks();
	}

	/**
	 *
	 */
	public function includes(){}

	/**
	 *
	 */
	public function hooks() {
		add_action( 'init', array( $this, 'add_menu_page' ) );
		add_action( 'init', array( $this, 'export' ) );
	}

	/**
	 *
	 */
	public function add_menu_page(){
		add_action( 'admin_menu', array( $this, 'add_submenu_page' ), PHP_INT_MAX );
	}

	/**
	 *
	 */
	public function add_submenu_page() {
		$licence = new WC_Multistore_Licence();

		if ( ! $licence->licence_key_verify() ) {
			return;
		}

		if ( get_option( 'woonet_network_type' ) != 'master' ) {
			return;
		}

		$menus_hook = add_submenu_page('woonet-woocommerce', __( 'Order Export', 'woonet' ), __( 'Order Export', 'woonet' ),'manage_product_terms','woonet-woocommerce-orders-export', array( $this, 'orders_export_page' ),5 );

		add_action( 'load-' . $menus_hook, array( $this, 'admin_notices' ) );
		add_action( 'admin_print_styles-' . $menus_hook, array( $this, 'admin_print_styles' ) );
		add_action( 'admin_print_scripts-' . $menus_hook, array( $this, 'admin_print_scripts' ) );
	}

	/**
	 *
	 */
	public function export() {
		if ( empty( $_POST['evcoe_form_submit'] ) || 'export' != $_POST['evcoe_form_submit'] ) {
			return;
		}

		$this->validate_settings();

		ini_set( 'max_execution_time', 500 );

		$exporter_class_name = 'WC_Multistore_Export_'.ucfirst( $this->export_type );
		$filename = 'network_orders_export';
		$filename .= empty( $this->export_time_after ) ? '' : '_from_' . date( 'Ymd', $this->export_time_after );
		$filename .= empty( $this->export_time_before ) ? '' : '_to_' . date( 'Ymd', $this->export_time_before );


		$exporter = new $exporter_class_name( $filename );
		$exporter->initialize();

		// Add header
		$header = $this->get_header();
		$exporter->addRow($header);

		// Add master site rows
		if ( in_array( 'master', $this->site_filter ) ) {
			$master_orders = $this->get_orders();
			foreach ( $master_orders as $master_order ){
				if( ! empty( $master_orders ) ){
					if( $this->row_format == 'row_per_product'){
						$order = wc_get_order( $master_order['ID'] );
						$order_items = $order->get_items();
						foreach ( $order_items as $order_item ){
							$row = $this->get_product_row($order, $order_item);
							$exporter->addRow($row);
						}
					}else{
						$row = $this->get_order_row( $master_order );
						$exporter->addRow($row);
					}
				}
			}
		}

		// Add child sites rows
		if( ! empty( $this->site_filter ) ){
			$engine      = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();
			foreach ( $this->site_filter as $site ){
				$query_rows = $engine->get_order_exports(
					array(
						'export_format'      => $this->export_type,
						'export_time_after'  => $_POST['export_time_after'],
						'export_time_before' => $_POST['export_time_before'],
						'site_filter'        => $this->site_filter,
						'order_status'       => $this->order_status,
						'row_format'         => $this->row_format,
						'export_fields'      => $this->export_fields,
					),
					$site
				);

				$child_site_rows = $query_rows[$site]['result'];
				if( !empty($child_site_rows) ){
					foreach ($child_site_rows as $child_site_row){
						$exporter->addRow($child_site_row);
					}
				}
			}
		}

		// Output
		$exporter->finalize();

		exit;
	}

	/**
	 *
	 */
	public function admin_print_styles() {
//		if ( is_multisite() ) {
			$url = WOO_MSTORE_URL;
//		} else {
//			$url = dirname( WOO_MSTORE_URL );
//		}

		wp_enqueue_style( 'jquery-ui-ms', $url . '/assets/css/jquery-ui.css' );
		wp_enqueue_style( 'select2', WC()->plugin_url() . '/assets/css/select2.css' );
		wp_enqueue_style( 'woonet-woocommerce-orders-export', $url . '/assets/css/woosl-export.css' );
	}

	/**
	 *
	 */
	public function admin_print_scripts() {
//		if ( is_multisite() ) {
			$url = WOO_MSTORE_URL;
//		} else {
//			$url = dirname( WOO_MSTORE_URL );
//		}

		wp_enqueue_script('jquery-ms',$url . '/assets/js/jquery-3.3.1.min.js', array() );
		wp_enqueue_script('jquery-ui-ms',$url . '/assets/js/jquery-ui.min.js', array( 'jquery-ms' ) );
		wp_add_inline_script('jquery-ui-ms', 'var $ms = $.noConflict(true);' );

		wp_enqueue_script('woonet-woocommerce-orders-export',$url . '/assets/js/woosl-export.js', array( 'jquery-ms', 'jquery-ui-ms', 'select2' ) );
		wp_localize_script('woonet-woocommerce-orders-export','woonet_woocommerce_orders_export',	array( 'site_filter_placeholder' => __( 'Please select sites to export', 'woonet' ), ) );
	}

	/**
	 *
	 */
	public function admin_notices() {
		if ( count( $this->system_messages ) < 1 ) {
			return;
		}

		foreach ( $this->system_messages as $system_message ) {
			if ( isset( $system_message['type'] ) ) {
				echo "<div class='notice " . $system_message['type'] . "'><p>" . $system_message['message'] . '</p></div>';
			} else {
				echo "<div class='notice notice-error'><p>" . $system_message . '</p></div>';
			}
		}
	}

	/**
	 *
	 */
	public function orders_export_page() {
//		if ( is_multisite() ) {
			$path = WOO_MSTORE_PATH;
//		} else {
//			$path = dirname( WOO_MSTORE_PATH );
//		}

		if ( ! is_multisite() ) {
			$blog_names_array = get_transient( 'woomulti_blognames' );

			if ( empty( $blog_names_array ) ) {
				$engine           = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();
				$blog_names_array = array(
					'master' => get_bloginfo( 'name' ),
				);
				$blog_names       = $engine->get_blogname();

				if ( ! empty( $blog_names ) ) {
					foreach ( $blog_names  as $key => $value ) {
						if ( ! empty( $value['status'] ) && $value['status'] == 'success' ) {
							$blog_names_array[ $key ] = $value['result'];
						}
					}
				}

				if ( ! empty( $blog_names_array ) && count( $blog_names_array ) >= 1 ) {
					set_transient( 'woomulti_blognames', $blog_names_array, 24 * 60 * 60 );
				}
			}
		}

		include $path . '/single-site/templates/html-order-export.php';
	}

	/**
	 *
	 */
	public function validate_settings() {

		if ( isset( $_POST['export_format'] ) && in_array( $_POST['export_format'], array( 'csv', 'xls' ) ) ) {
			$this->export_type = $_POST['export_format'];
		} else {
			$this->errors_log[] = "Invalid export format";
		}

		if ( empty( $_POST['export_time_after'] ) ) {
			$this->export_time_after = 0;
		} else {
			$this->export_time_after = strtotime( $_POST['export_time_after'] );

			if ( false === $this->export_time_after ) {
				$this->errors_log[] = "Invalid time After";
			}
		}

		if ( empty( $_POST['export_time_before'] ) ) {
			$this->export_time_before = 9999999999;
		} else {
			$this->export_time_before = strtotime( $_POST['export_time_before'] );

			if ( false === $this->export_time_before ) {
				$this->errors_log[] = "Invalid time Before";
			}
		}

		if ( isset( $_POST['site_filter'] ) && is_array( $_POST['site_filter'] ) ) {
			$this->site_filter = array_filter( array_map( 'sanitize_key', $_POST['site_filter'] ) );

			if ( empty( $this->site_filter ) ) {
				$this->errors_log[] = "Empty site filter";
			}
		} else {
			$this->errors_log[] = "Empty site filter";
		}

		if ( isset( $_POST['order_status'] ) && in_array( $_POST['order_status'], array_keys( wc_get_order_statuses() ) ) ) {
			$this->order_status = array( $_POST['order_status'] );
		} else {
			$this->order_status = array_keys( wc_get_order_statuses() );
		}

		if ( isset( $_POST['row_format'] ) && in_array( $_POST['row_format'], array(
				'row_per_order',
				'row_per_product'
			) ) ) {
			$this->row_format = $_POST['row_format'];
		} else {
			$this->errors_log[] = "Invalid row export format";
		}

		$this->export_fields = empty( $_POST["export_fields"] ) ? array() : $_POST["export_fields"];

		update_option( 'mstore_orders_export_options', array(
			'export_type'        => $this->export_type,
			'export_time_after'  => $this->export_time_after,
			'export_time_before' => $this->export_time_before,
			'site_filter'        => $this->site_filter,
			'order_status'       => $this->order_status,
			'row_format'         => $this->row_format,
			'export_fields'      => $this->export_fields,
		) );
	}

	/**
	 * @return array
	 */
	public function get_orders() {
		global $wpdb;

		$status = array_map( 'esc_sql', $this->order_status );
		$status = "'" . implode( "', '", $status ) . "'";

		$query = $wpdb->prepare(
			"
			SELECT *
			FROM {$wpdb->prefix}posts
			WHERE post_type = 'shop_order'
				AND post_status IN ({$status})
				AND post_date BETWEEN %s AND %s
				ORDER BY ID ASC
			",
			array(
				date( 'Y-m-d', $this->export_time_after ),
				date( 'Y-m-d', $this->export_time_before ),
			)
		);

		return $wpdb->get_results( $query, ARRAY_A );
	}

	/**
	 * @param $order_data
	 *
	 * @return array
	 */
	public function get_order_row( $order_data ){
		$row = array();

		$order = wc_get_order( $order_data['ID'] );
		foreach ( $this->export_fields as $export_field => $export_field_column_name ) {
			if( 0 === strpos( $export_field, 'order_item_' ) ){
				continue;
			}
			$cell =  $this->get_order_cell( $export_field, $order );
			$row[] = $cell;
		}

		return $row;
	}

	/**
	 * @param $order_data
	 *
	 * @return array
	 */
	public function get_product_row( $order_data, $order_item ){
		$row = array();

		foreach ( $this->export_fields as $export_field => $export_field_column_name ) {
			$cell =  $this->get_product_cell( $export_field, $order_data, $order_item );
			$row[] = $cell;
		}

		return $row;
	}

	/**
	 * @param $field
	 * @param $order
	 *
	 * @return array|mixed|string
	 */
	function get_order_cell( $field, $order ){
		$cell = '';

		if( $this->is_network_field( $field ) ){
			$cell = $this->get_network_field( $field );
		}

		if( $this->is_order_field( $field ) ){
			$cell = $this->get_order_field( $field, $order );
		}

		if( $this->is_order_items_field( $field ) ){
			$cell = $this->get_order_items_field( $field, $order );
		}

		return $cell;
	}

	/**
	 * @param $field
	 * @param $order
	 * @param $order_item
	 *
	 * @return array|mixed|string
	 */
	function get_product_cell( $field, $order, $order_item ){
		$cell = '';

		if( $this->is_network_field( $field ) ){
			$cell = $this->get_network_field( $field );
		}

		if( $this->is_order_field( $field ) ){
			$cell = $this->get_order_field( $field, $order );
		}

		if( $this->is_order_item_field( $field ) ){
			$cell = $this->get_order_item_field( $field, $order_item );
		}

		if( $this->is_order_item_product_field( $field ) ){
			$cell = $this->get_order_item_product_field( $field, $order_item );
		}

		if( $this->is_order_item_shipping_field( $field ) ){
			$cell = array();
			$shipping_items = $order->get_items('shipping');
			if( ! empty( $shipping_items ) ){
				foreach ($shipping_items as $shipping_item){
					$cell[] = $this->get_order_item_shipping_field( $field, $shipping_item );
				}
			}
		}

		if( $this->is_order_item_tax_field( $field ) ){
			$cell = array();
			$tax_items = $order->get_items('tax');
			if( ! empty( $tax_items ) ){
				foreach ($tax_items as $tax_item){
					$cell[] = $this->get_order_item_tax_field( $field, $tax_item );
				}
			}
		}

		if( $this->is_order_item_coupon_field( $field ) ){
			$cell = array();
			$coupon_items = $order->get_items('coupon');
			if( ! empty( $coupon_items ) ){
				foreach ($coupon_items as $coupon_item){
					$cell[] = $this->get_order_item_coupon_field( $field, $coupon_item );
				}
			}
		}

		if( $this->is_order_item_fee_field( $field ) ){
			$cell = array();
			$fee_items = $order->get_items('fee');
			if( ! empty( $fee_items ) ){
				foreach ($fee_items as $fee_item){
					$cell[] = $this->get_order_item_fee_field( $field, $fee_item );
				}
			}
		}

		if( $this->is_order_items_field( $field ) ){
			$cell = $this->get_order_items_field( $field, $order );
		}

		return $cell;
	}

	/**
	 * @param $field
	 *
	 * @return bool
	 */
	function is_network_field( $field ){
		if( 0 === strpos( $field, 'network__' ) ){
			return true;
		}

		return false;
	}

	/**
	 * @param $field
	 *
	 * @return mixed
	 */
	function get_network_field( $field ){
		list($class,$field_name) = explode('__', $field );
		$function_name = 'get_'. $field_name;

		return $this->$function_name();
	}

	/**
	 * @param $field
	 *
	 * @return bool
	 */
	function is_order_field( $field ){
		if( 0 === strpos( $field, 'order__' ) ){
			return true;
		}

		return false;
	}

	/**
	 * @param $field
	 * @param $order
	 *
	 * @return mixed
	 */
	function get_order_field( $field, $order ){
		list($class,$field_name) = explode('__', $field );
		$function_name = 'get_'. $field_name;

		return $order->$function_name();
	}

	/**
	 * @param $field
	 *
	 * @return bool
	 */
	function is_order_items_field( $field ){
		if( 0 === strpos( $field, 'order__items' ) ){
			return true;
		}

		return false;
	}

	/**
	 * @param $field
	 *
	 * @return bool
	 */
	function is_order_item_field( $field ){
		if( 0 === strpos( $field, 'order_item__' ) ){
			return true;
		}

		return false;
	}

	/**
	 * @param $field
	 * @param $order_item
	 *
	 * @return mixed
	 */
	function get_order_item_field( $field, $order_item ){
		list($class,$field_name) = explode('__', $field );
		$function_name = 'get_'. $field_name;
		return $order_item->$function_name();
	}

	/**
	 * @param $field
	 *
	 * @return bool
	 */
	function is_order_item_product_field($field){
		if( 0 === strpos( $field, 'order_item_product__' ) ){
			return true;
		}

		return false;
	}

	/**
	 * @param $field
	 * @param $order_item
	 *
	 * @return mixed|string
	 */
	function get_order_item_product_field( $field, $order_item ){
		list($class,$field_name) = explode('__', $field );
		$function_name = 'get_'. $field_name;

		$product = $order_item->get_product();

		if( ! $product ){
			return '';
		}

		return $product->$function_name();
	}

	/**
	 * @param $field
	 *
	 * @return bool
	 */
	function is_order_item_shipping_field($field){
		if( 0 === strpos( $field, 'order_item_shipping__' ) ){
			return true;
		}

		return false;
	}

	/**
	 * @param $field
	 * @param $order_item
	 *
	 * @return string
	 */
	function get_order_item_shipping_field( $field, $order_item ){
		list($class,$field_name) = explode('__', $field );
		$function_name = 'get_'. $field_name;

		$shipping = new WC_Order_Item_Shipping( $order_item->get_id() );

		if( ! $shipping ){
			return '';
		}

		return $shipping->$function_name();
	}

	/**
	 * @param $field
	 *
	 * @return bool
	 */
	function is_order_item_tax_field($field){
		if( 0 === strpos( $field, 'order_item_tax__' ) ){
			return true;
		}

		return false;
	}

	/**
	 * @param $field
	 * @param $order_item
	 *
	 * @return string
	 */
	function get_order_item_tax_field( $field, $order_item ){
		list($class,$field_name) = explode('__', $field );
		$function_name = 'get_'. $field_name;

		$tax = new WC_Order_Item_Tax( $order_item->get_id() );

		if( ! $tax ){
			return '';
		}

		return $tax->$function_name();
	}

	/**
	 * @param $field
	 *
	 * @return bool
	 */
	function is_order_item_coupon_field($field){
		if( 0 === strpos( $field, 'order_item_coupon__' ) ){
			return true;
		}

		return false;
	}

	/**
	 * @param $field
	 * @param $order_item
	 *
	 * @return string
	 */
	function get_order_item_coupon_field( $field, $order_item ){
		list($class,$field_name) = explode('__', $field );
		$function_name = 'get_'. $field_name;

		$coupon = new WC_Order_Item_Coupon( $order_item->get_id() );

		if( ! $coupon ){
			return '';
		}

		return $coupon->$function_name();
	}

	/**
	 * @param $field
	 *
	 * @return bool
	 */
	function is_order_item_fee_field($field){
		if( 0 === strpos( $field, 'order_item_fee__' ) ){
			return true;
		}

		return false;
	}

	/**
	 * @param $field
	 * @param $order_item
	 *
	 * @return string
	 */
	function get_order_item_fee_field( $field, $order_item ){
		list($class,$field_name) = explode('__', $field );
		$function_name = 'get_'. $field_name;

		$fee = new WC_Order_Item_Fee( $order_item->get_id() );

		if( ! $fee ){
			return '';
		}

		return $fee->$function_name();
	}

	/**
	 * @param $field
	 * @param $order
	 *
	 * @return array|string
	 */
	function get_order_items_field( $field, $order ){
		$order_items = $order->get_items( array( 'line_item', 'fee', 'shipping', 'tax', 'coupon' ) );

		if( ! $order_items ){ return ''; }

		$order__items = array();
		foreach ( $order_items as $order_item ){
			$order__item = array();
			foreach ( $this->export_fields as $export_field => $export_field_column_name ) {
				if( $this->is_order_item_field($export_field) && $order_item->get_type() == 'line_item' ){
					$order__item[$export_field] = $this->get_order_item_field( $export_field, $order_item );
				}

				if( $this->is_order_item_product_field($export_field) && $order_item->get_type() == 'line_item' ){
					$order__item[$export_field] = $this->get_order_item_product_field( $export_field, $order_item );
				}

				if( $this->is_order_item_shipping_field($export_field) && $order_item->get_type() == 'shipping' ){
					$order__item[$export_field] = $this->get_order_item_shipping_field( $export_field, $order_item );
				}

				if( $this->is_order_item_tax_field($export_field) && $order_item->get_type() == 'tax' ){
					$order__item[$export_field] = $this->get_order_item_tax_field( $export_field, $order_item );
				}

				if( $this->is_order_item_coupon_field($export_field) && $order_item->get_type() == 'coupon' ){
					$order__item[$export_field] = $this->get_order_item_coupon_field( $export_field, $order_item );
				}

				if( $this->is_order_item_fee_field($export_field) && $order_item->get_type() == 'fee' ){
					$order__item[$export_field] = $this->get_order_item_fee_field( $export_field, $order_item );
				}
			}

			$order__items[] = $order__item;
		}

		return $order__items;
	}

	/**
	 * @return array
	 */
	private function get_header() {
		$header = array();

		foreach ( $this->export_fields as $key => $value ) {
			if ( 'row_per_order' != $this->row_format || 0 !== strpos( $value, 'order_item_' ) ) {
				$header[ $key ] = $value;
			}
		}

		return $header;
	}

	/**
	 * @return string|void
	 */
	private function get_site_id() {
		return get_bloginfo( 'name' );
	}

	/**
	 * @return string
	 */
	private function get_blogname() {
		$blog_details = get_blog_details( get_current_blog_id() );

		if ( ! empty( $blog_details ) && ! empty( $blog_details->blogname ) ) {
			return $blog_details->blogname;
		}

		return "(Empty Site Title)";
	}

}
$globals['WC_Multistore_Export_Order'] = new WC_Multistore_Export_Order();