<?php
/**
 * Deactivate handler.
 *
 * This handles plugin deactivate related functionality in Woocommerce Multistore.
 *
 */

defined( 'ABSPATH' ) || exit;

/**
 * Class WC_Multistore_Deactivate
 */
class WC_Multistore_Deactivate {
	public static function deactivate(){
	}
}
