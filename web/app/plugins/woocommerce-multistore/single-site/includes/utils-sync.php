<?php
/**
 * Different utiligy functions related to product Sync
 *
 * @class   WOO_MSTORE_SINGLE_UTILS_SYNC
 * @since   4.2.0
 */

defined( 'ABSPATH' ) || exit;

class WOO_MSTORE_SINGLE_UTILS_SYNC {
	/**
	 * Check if stock sync is enabled and required.
	 */
	public function is_stock_sync_required( $product_id, $site_id ) {
        $wp                         = wc_get_product( $product_id );
        $woonet_site_data           = get_option('woonet_master_connect');
        $woonet_site_options        = get_option('woonet_options');
        
        $product_meta_sync_stock    = get_post_meta( $product_id, '_woonet_' . $woonet_site_data['uuid'] . '_child_stock_synchronize', true );
        $site_sync_stock            = $woonet_site_options['override__synchronize-stock'];
        $global_sync_stock          = $woonet_site_options['synchronize-stock'];
        
        if( $global_sync_stock == 'no' && $product_meta_sync_stock == 'no' ){
            return false;
        }
        
        return true;
	}
}

