<?php

defined( 'ABSPATH' ) || exit;

/**
 * Final Class WOO_MSTORE_SINGLE_MAIN
 **/
final class WOO_MSTORE_SINGLE_MAIN {

	/**
	 * Site Manager Instance
	 **/
	public $site_manager = null;

	/**
	 * Asset Manager Instance
	 **/
	public $asset_manager = null;

	/**
	 * License Manager Instance
	 **/
	public $license_manager = null;

	/**
	 * Options Manager Instance
	 **/
	public $options_manager = null;

	/**
	 * Sync Engine
	 **/
	public $sync_engine = null;

	/**
	 * Sync Utils
	 **/
	public $sync_utils = null;

	/**
	 * Network sync product interface
	 */
	public $product_sync_interface = null;

	/**
	 * Coupon Interface
	 */
	public $coupon = null;

	/**
	 * Instance
	 **/
	public static $_instance = null;

	/**
	 * initiate the action hooks and load the plugin classes
	 **/
	private function __construct() {
		if ( ! $this->is_woocomemrce_active() ) { return false;	}
		if ( ! $this->required_php_version() ) { return false; }

		$this->define_required_constants();
		$this->include_required_classes();
		$this->setup_action_hooks();

		// License Manager
		$this->license_manager = new WC_Multistore_Licence();

		// Site Manager
		$this->site_manager = new WOO_MSTORE_SINGLE_SITE_MANAGER();

		// Asset Manager
		$this->asset_manager = new WOO_MSTORE_SINGLE_ASSETS_MANAGER();

		// Options Manager
		$this->options_manager = new WC_Multistore_Functions();

		// Sync Engine
		$this->sync_engine = new WOO_MSTORE_SINGLE_NETWORK_SYNC_ENGINE();

		// Sync Utility
		$this->sync_utils = new WOO_MSTORE_SINGLE_UTILS_SYNC();

		// Network product sync interface
		$this->product_sync_interface = new WOO_MSTORE_SINGLE_NETWORK_PRODUCTS_SYNC();

		// Fire woomultistore_loaded action
		do_action( 'woomultistore_loaded' );
	}

	public static function getInstance() {
		if ( self::$_instance === null ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public function define_required_constants(){
		$this->define( 'WOO_MSTORE_PATH', dirname( plugin_dir_path( __FILE__ ) ) );
		$this->define( 'WOO_MSTORE_URL', dirname( plugins_url( '', __FILE__ ) ) );
		$this->define( 'WOO_MSTORE_APP_API_URL', 'https://woomultistore.com/index.php' );

		$this->define( 'WOO_MSTORE_PLUGIN_NAME', 'WooMultistore' );
		$this->define( 'WOO_MSTORE_VERSION', '4.6.8' );
		$this->define( 'WOO_MSTORE_DB_VERSION', '1.0' );

		$this->define( 'WOO_MSTORE_PRODUCT_ID', 'WCMSTORE' );
		$this->define( 'WOO_MSTORE_INSTANCE', str_replace( array( 'https://', 'http://' ), '', site_url() ) );

		$this->define( 'WOO_MSTORE_SINGLE_TEMPLATES_PATH', dirname( __FILE__ ) . '/templates/' );
		$this->define( 'WOO_MSTORE_SINGLE_INCLUDES_PATH', dirname( __FILE__ ) . '/includes/' );

		$this->define( 'WOO_MSTORE_ASSET_URL', plugins_url( '', dirname( __FILE__ ) ) );
	}

	/**
	 * Include required classes
	 **/
	public function include_required_classes() {

		// Shared Files
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-install.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-deactivate.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-licence.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-updater.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-functions.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-custom-taxonomy.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-network-order-refund.php';

		// Single site files
		require_once WOO_MSTORE_PATH . '/single-site/includes/assets-manager.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/site-manager.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/utils-sync.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/functions.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/menu.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/setup-wizard.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/connected-sites.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/editor-integration.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/network-products.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/network-orders.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/network-products-sync.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/network-sync-engine.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-settings.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/trash-products.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/version.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-ajax.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-sequential-order-number.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/order-meta.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-product-category.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/coupon/hooks.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/coupon/class-coupons.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-image.php';

		// Load 3rd party integration support
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-integration.php';

		/**
		 * If dev constant is defined, disable some security features.
		 */
		if ( defined( 'WOO_MOSTORE_DEV_ENV' ) && WOO_MOSTORE_DEV_ENV == true ) {
			require_once WOO_MSTORE_PATH . '/single-site/includes/dev-env.php';
		}

		// Load export functionality from the core multistore plugin.
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-export.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-export-csv.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-export-xls.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-export-order.php';

		// Load reset functionality from the core multistore plugin.
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-reset.php';

		// REST Helpers
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-import-order.php';
		require_once WOO_MSTORE_PATH . '/single-site/includes/rest-api/class-product-sync.php';

		// Bulk Sync
		require_once WOO_MSTORE_PATH . '/single-site/includes/class-wc-multistore-bulk-sync.php';
	}

	/**
	 * Sets up common action hooks
	 **/
	public function setup_action_hooks() {
		register_activation_hook(  WC_MULTISTORE_FILE  , array( 'WC_Multistore_Install','install' ) );
		register_deactivation_hook( WC_MULTISTORE_FILE, array( 'WC_Multistore_Deactivate','deactivate' ) );

		if ( get_option( 'woonet_setup_wizard_complete' ) != 'yes' && ! $this->check_if_plugin_page() ) {
			add_action( 'admin_notices', array( $this, 'show_setup_instructions' ) );
		}

		// Show additional information in plugin row.
		add_filter( 'plugin_row_meta', array( $this, 'action_links' ), 10, 2 );

		// REST INIT HOOK
		add_action( 'rest_api_init', array( $this, 'load_rest_api_classes' ), 10, 0 );
	}

	/**
	 * REST API Classes.
	 */
	public function load_rest_api_classes() {
		require_once WOO_MSTORE_PATH . '/single-site/includes/network-orders.php';

		if ( WOO_MULTISTORE()->site_manager->get_type() == 'master' ) {
			// REST API classes
			require_once WOO_MSTORE_PATH . '/single-site/includes/rest-api/class-orders.php';
			require_once WOO_MSTORE_PATH . '/single-site/includes/rest-api/class-stores.php';
		}
	}

	/**
	 * Cloning is forbidden.
	 *
	 * @since 4.2.0
	 */
	public function __clone() {
		wc_doing_it_wrong( __FUNCTION__, __( 'Cloning is forbidden.', 'woocommerce' ), '4.2.0' );
	}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 4.2.0
	 */
	public function __wakeup() {
		wc_doing_it_wrong( __FUNCTION__, __( 'Unserializing instances of this class is forbidden.', 'woocommerce' ), '4.2.0' );
	}

	/**
	 * Show set up instructions
	 **/
	public function show_setup_instructions() {
		woomulti_get_template_parts( 'admin-notice-setup-wizard' );
	}

	/**
	 * Hide the setup wizard warning from plugin page
	 **/
	public function check_if_plugin_page() {
		if ( isset( $_GET['page'] ) && strpos( $_GET['page'], 'woonet' ) !== false ) {
			return true;
		}

		return false;
	}

	/**
	 * Modify plugin row action links
	 */
	public function action_links( $links, $file ) {
		if ( strpos( $file, 'woocommerce-multistore.php' ) !== false ) {

			if ( get_option( 'woonet_network_type' ) == 'child' ) {
				/**
				 * Child site settings panel has been moved to the master site. Link to it.
				 */

				$master_data = get_option( 'woonet_master_connect' );

				if ( ! empty( $master_data['master_url'] ) ) {
					$links[2] = '<a target="_blank" href="' . esc_url( $master_data['master_url'] . '/wp-admin/admin.php?page=woonet-woocommerce-settings' ) . '">Settings</a>';
				}
			} else {
				$links[2] = '<a href="' . esc_url( get_admin_url( null, 'admin.php?page=woonet-woocommerce-settings' ) ) . '">Settings</a>';

			}

			$links[3] = '<a href="https://woomultistore.com/documentation/">Docs</a>';
			$links[4] = '<a href="https://woomultistore.com/plugin-api-filters-actions/">API docs</a>';
		}

		return $links;
	}

	public function is_woocomemrce_active() {
		if ( ! function_exists( 'is_plugin_active' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			add_action( 'admin_notices', array( $this, 'woocommerce_inactive_admin_notice' ), 10, 0 );
			return false;
		}

		return true;
	}

	public function required_php_version(){
		if( version_compare( phpversion(), '7.4.0', '<' ) ){
			add_action( 'admin_notices', function(){
				$class = 'notice notice-error';
				$message = __( 'Woocommerce Multistore plugin requires PHP version 7.4.0 or greater. Please update your PHP version', 'woonet' );

				printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
			} );

			return false;
		}

		return true;
	}

	public function woocommerce_inactive_admin_notice() {
		$class   = 'notice notice-error';
		$message = __( 'Multistore requires WooCommerce to be activated', 'woonet' );

		printf(
			'<div class="%1$s"><p>%2$s</p></div>',
			esc_attr( $class ),
			esc_html( $message )
		);
	}

	/**
	 * Shortcut to check license.
	 *
	 * @return boolean
	 */
	public function is_license_valid() {
		return $this->license_manager->licence_key_verify();
	}

	/**
	 * Define constant if not already set.
	 *
	 * @param string      $name  Constant name.
	 * @param string|bool $value Constant value.
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}
}

function WOO_MULTISTORE() {
	return WOO_MSTORE_SINGLE_MAIN::getInstance();
}

$GLOBALS['WOO_MULTISTORE'] = WOO_MULTISTORE();

