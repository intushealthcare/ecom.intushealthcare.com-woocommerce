<?php
/* Tell search engines that the site is temporarily unavilable */
$protocol = $_SERVER[ "SERVER_PROTOCOL" ];
if ( 'HTTP/1.1' != $protocol && 'HTTP/1.0' != $protocol )
{
    $protocol = 'HTTP/1.0';
}
header( "$protocol 503 Service Unavailable", true, 503 );
header( 'Content-Type: text/html; charset=utf-8' );
header( 'Retry-After: 600' );
?>
<html>
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500&display=swap"
        rel="stylesheet">
    <style type="text/css">
        body {
            font-family: Montserrat, sans-serif;
            color: #30357c;
        }
        h1{
            font-size: 1.5rem;
        }
        a {
            color: #30357c;
            font-weight: bold;
        }
        .content{
            margin: 0 auto;
            padding: 1rem;
            max-width: 36rem;
            text-align: left;
        }
        .logo{
            max-width: 10rem;
            margin-left: -1rem;
        }
    </style>
</head>
<body>
<div class="jumbotron d-flex align-items-center min-vh-100">
    <div class="container text-center">

        <div class="content">
            <img class="logo mb-5"
                 src="https://www.intushealthcare.com/app/uploads/sites/2/2020/02/intus-healthcare-logo-300x160.png" alt="">
            <h1 class="mb-2">We're doing some brief maintenance :-)</h1>
            <p>Please pop back in a few minutes and we expect we'll be back up and running.</p>
            <p>If you need to contact us, please send an email to <a href="mailto:contact@intushealthcare.com">contact@intushealthcare.com</a> and we'll be happy to help.
            </p>
            <p>Thanks,<br>
                The <strong>Intus Healthcare</strong> Team</p>
        </div>
    </div>
</div>
</body>
</html>
<?php
/* This passes control back to the wordpress upgrade routine */
die();
/* Don't change this */
?>
