<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body {
                font-family: "DejaVu Sans", "DejaVu Sans Mono", "DejaVu", sans-serif, monospace;
                font-size: 11px;
            }

            @page {
                margin: 480px 50px 100px 50px;
            }

            #header {
                position: fixed;
                left: 0px;
                top: -440px;
                right: 0px;
                height: 280px;
                text-align: center;
            }

            #footer {
                position: fixed;
                left: 0px;
                bottom: -150px;
                right: 0px;
                height: 100px;
                font-size:11px;
                text-align: center;
            }

            #content {
                font-size:10px;
            }

            #logo {
                text-align: right;
            }

            #logo img {
                display: block;
                max-width: 150px;
            }

            #line-items table th {
                background: #e4e4e4;
                word-wrap: break-word;
            }

            #line-items table td {
                background: #f1f1f1;
                word-wrap: break-word;
            }

            #line-items table tr:first-of-type th {
                background: transparent;
                padding-top: 30px;
            }

            .barcode {
                text-align:center;
                width: 50%;
            }

            .text-normal {
                font-weight: normal;
            }

            [[PDFCURRENCYSYMBOLFONT]]
        </style>
    </head>
    <body>
        <div id="header">
            <table table width="100%">
                <tr>
                    <td valign="top" width="50%" id="company-info"></td>
                    <td valign="top" width="50%" id="logo">[[PDFLOGO]]</td>
                </tr>
            </table>
            <br /><br />
            <table table width="100%">
                <tr>
                    <td valign="top" colspan="2">
                        <h3><?php echo apply_filters( 'pdf_template_billing_details_text', __( 'Billing Details', 'woocommerce-pdf-invoice' ) ); ?></h3>
                        [[PDFBILLINGADDRESS]]<br /><br /><br />
                        <strong><span>T: </span> [[PDFBILLINGTEL]]<br /></strong>
                        <strong><span>E: </span>[[PDFBILLINGEMAIL]]</strong>
                    </td>
                    <td valign="top" colspan="2">
                        <h3><?php echo apply_filters( 'pdf_template_shipping_details_text', __( 'Shipping Details', 'woocommerce-pdf-invoice' ) ); ?></h3>
                        [[PDFSHIPPINGADDRESS]]<br /><br />
                        <h3><?php echo apply_filters( 'pdf_template_shipping_method_text', __( 'Shipping Method :', 'woocommerce-pdf-invoice' ) ); ?></h3>
                        [[PDFSHIPPINGMETHOD]]
                    </td>
                </tr>
            </table>
        </div>
        <div id="content">
            <table table width="100%">
                <tr>
                    <td valign="top"><h3>Invoice Details: </h3></td>
                </tr>
                <tr>
                    <td valign="top"><?php echo apply_filters( 'pdf_template_invoice_number_text', __( 'Invoice No: ', 'woocommerce-pdf-invoice' ) ); ?>[[PDFINVOICENUM]]</td>
                </tr>
                <tr>
                    <td valign="top"><?php echo apply_filters( 'pdf_template_invoice_date_text', __( 'Invoice Date: ', 'woocommerce-pdf-invoice' ) ); ?>[[PDFINVOICEDATE]]</td>
                </tr>
                <tr>
                    <td valign="top"><?php echo apply_filters( 'pdf_template_order_number_text', __( 'Order No:  ', 'woocommerce-pdf-invoice' ) ); ?>[[PDFORDERENUM]]</td>
                </tr>
                <tr>
                    <td valign="top"><?php echo apply_filters( 'pdf_template_order_date_text', __( 'Order Date: ', 'woocommerce-pdf-invoice' ) ); ?>[[PDFORDERDATE]]</td>
                </tr>
                <tr>
                    <td valign="top"><?php echo apply_filters( 'pdf_template_payment_method_text', __( 'Payment Method: ', 'woocommerce-pdf-invoice' ) ); ?>[[PDFINVOICEPAYMENTMETHOD]]</td>
                </tr>
            </table>
            <div id="line-items">
                [[ORDERINFOHEADER]]
                [[ORDERINFO]]
                [[PDFBARCODES]]
            </div>
            <table table width="100%">
                <tr>
                    <td width="60%" valign="top">
                        [[PDFORDERNOTES]]
                    </td>
                    <td width="40%" valign="top" align="right">
                    <br /><br />
                    <table width="100%">
                        [[PDFORDERTOTALS]]
                    </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="footer">
            <div class="copyright"><?php echo apply_filters( 'pdf_template_registered_name_text', __( 'Registered Name : ', 'woocommerce-pdf-invoice' ) ); ?>[[PDFREGISTEREDNAME]] <?php echo apply_filters( 'pdf_template_registered_office_text', __( 'Registered Office : ', 'woocommerce-pdf-invoice' ) ); ?>[[PDFREGISTEREDADDRESS]]</div>
            <div class="copyright"><?php echo apply_filters( 'pdf_template_company_number_text', __( 'Company Number : ', 'woocommerce-pdf-invoice' ) ); ?>[[PDFCOMPANYNUMBER]] <?php echo apply_filters( 'pdf_template_vat_number_text', __( 'VAT Number : ', 'woocommerce-pdf-invoice' ) ); ?>[[PDFTAXNUMBER]]</div>
        </div>
    </body>
</html>
