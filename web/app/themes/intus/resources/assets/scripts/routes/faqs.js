export default {
    init() {

        this.form = $('#faq-search-form')
        this.searchInput = $('#faq-search-input')
        this.searchContainer = $('#faq-search-container')
        this.ajaxOuter = $('#faq-ajax-outer')
        this.ajaxContainer = $('#faq-ajax-container')
        this.back = $('#faq-search-back')
        this.featuredQuestion = $('.faq-featured-question')
        this.defaultContent = this.ajaxContainer.html()

        // Submit form
        this.form.submit( (e) => {
            e.preventDefault()
            if (this.searchInput.val() !== '') {
                this.searchFaqs()
            } else {
                this.resetFaqs()
            }
        })

        // Featured questions
        this.featuredQuestion.click( (e) => {
            e.preventDefault()
            this.searchInput.val($(e.currentTarget).attr('data-value'))
            this.form.submit()
        })

        // Reset to default
        this.back.click( () => {
            this.resetFaqs()
        })

        this.searchInput.on('search', (e) => {
            if ($(e.currentTarget).val() == '') {
                this.resetFaqs()
            }
        })


    },
    searchFaqs() {

        const self = this
        const searchQuery = this.searchInput.val()

        this.searchContainer.addClass('search-results')
        this.ajaxOuter.addClass('posts-loading')
        this.ajaxContainer.empty()

        $.ajax({
            url: MyAjax.ajaxurl,
            type: 'POST',
            dataType: 'html',
            data: {
                action: 'intus_search_faqs',
                query: searchQuery,
            },
            success: (data) => {
                self.ajaxContainer.append(data)
            },
            complete: () => {
                this.ajaxOuter.removeClass('posts-loading').addClass('posts-loaded')
            },
            error: (errorThrown) => {
                console.log(errorThrown)
            },
        })

    },
    resetFaqs() {

        this.searchContainer.removeClass('search-results')
        this.ajaxContainer.empty().append(this.defaultContent)
        this.searchInput.val('')
        $('.nav-tabs li:first-child a').tab('show')

    },
};
