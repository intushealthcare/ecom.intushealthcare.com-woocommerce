import common from './common';

export default {
    init() {
        this.claimCartVatRelief();
        this.showCartVatReliefTerms();
    },

    claimCartVatRelief() {
        let cart_form =  $('form.woocommerce-cart-form');
        cart_form.on('change', 'input[name="vat_exempt_declaration_checkbox"]', function ( event ) {
            let vat_exemption_checkbox = event.target;
            if( vat_exemption_checkbox.checked ) {
                common.setVatExemptionClaimed(true);
            }else{
                common.setVatExemptionClaimed(false);
            }
            let btn_update_cart = $('button[name="update_cart"]');
            btn_update_cart.prop('disabled', false).attr('aria-disabled', false);
            btn_update_cart.trigger('click');
        });
    },

    showCartVatReliefTerms() {
        common.showCartVatReliefTerms( 'cart' );
    },
}
