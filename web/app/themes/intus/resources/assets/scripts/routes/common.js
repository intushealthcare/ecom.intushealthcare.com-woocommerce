import inView from './includes/inview';

export default {
    init() {
        // this.cachebusting();
        this.searchTimeout = null
        this.modalTimeout = null
        this.body = $('body')
        this.overlay = $('.offcanvas-overlay')
        this.searchPopup = $('.search-popup')
        this.modalGlobal = $('#modal-global')
        this.mailchimpEl = $('#mc_embed_signup')
        this.sliderBannerEl = $('.slider-banner')
        this.headerBasketLink = $('.header-quick-basket')

        this.offcanvas()
        this.search()
        inView.init()

        if (this.modalGlobal.length) {
            this.promoModalGlobal()
        }

        if (this.mailchimpEl.length) {
            this.mailchimp()
        }

        if (this.sliderBannerEl.length) {
            this.sliderBanner()
        }

        if (this.headerBasketLink.length) {
            this.headerBasketLinks()
        }

        $('.quick-basket-toggle').click((e) => {
            e.preventDefault()
            $('.quick-basket-wrap').toggleClass('quick-basket-open')
        })

    },
    sliderBanner() {

        this.sliderBannerEl.slick({
            dots: true,
            arrows: true,
            infinite: true,
            speed: 500,
            fade: true,
            rows: 0,
            autoplay: true,
            autoplaySpeed: 5000,
            cssEase: 'linear',
            prevArrow: '<button type="button" class="slick-arrow slick-arrow--prev"><i class="fal fa-angle-left fa-4x color-white"></i></button>',
            nextArrow: '<button type="button" class="slick-arrow slick-arrow--next"><i class="fal fa-angle-right fa-4x color-white"></i></button>',
        })

    },
    mailchimp() {

        const acceptance = document.getElementById('signup-acceptance')
        const submit = document.getElementById('mc-embedded-subscribe')

        acceptance.onchange = (e) => {
            if (e.currentTarget.checked) {
                submit.disabled = false
            } else {
                submit.disabled = true
            }
        }

    },
    offcanvas() {

        $('.toggle-offcanvas').click((e) => {
            e.preventDefault()
            this.body.toggleClass('offcanvas-open')
            this.overlay.fadeToggle()
        })

        $('.offcanvas-overlay, .offcanvas__close').click((e) => {
            e.preventDefault()
            this.body.removeClass('offcanvas-open')
            this.overlay.fadeOut()
        })

        $('.nav--offcanvas li.menu-item-has-children > a').click((e) => {
            e.preventDefault()
            const self = $(e.currentTarget)
            const subMenu = self.next('.sub-menu')
            self.toggleClass('menu-item-expanded')
            subMenu.slideToggle()
        })

    },
    search() {

        $('.toggle-search').click((e) => {
            e.preventDefault()

            const hiddenClass = 'search-popup--hidden'
            const visibleClass = 'search-popup--visible'
            const animationTime = 300

            if (this.searchPopup.hasClass(hiddenClass)) {
                this.searchPopup.removeClass(hiddenClass).addClass(visibleClass)
                document.getElementById('search-keyword-header').focus()
            } else {
                this.searchPopup.removeClass(visibleClass)
                this.searchTimeout = setTimeout(() => {
                    this.searchPopup.addClass(hiddenClass)
                    clearTimeout(this.searchTimeout)
                }, animationTime)
            }
        })

        $('.search-popup__submit').click(() => {
            this.searchPopup.submit()
        })

        $('.form-search__submit').click(() => {
            $('.form-search').submit()
        })

    },
    isPopupHidden(item) {

        // Get session data
        return sessionStorage.getItem(item);

    },
    setPopupStatus(item) {

        // Set session data
        sessionStorage.setItem(item, true)

    },
    promoModalGlobal() {

        const globalModalSeen = 'global-modal-seen'

        $(window).load(() => {
            if (!this.isPopupHidden(globalModalSeen)) {
                this.modalTimeout = setTimeout(() => {
                    this.modalGlobal.modal('show')
                    this.setPopupStatus(globalModalSeen)
                    clearTimeout(this.modalTimeout)
                }, 3000)
            }
        })

    },

    headerBasketLinks() {
        const basketLink = this.headerBasketLink.find('.basket-link');
        const cartTotal = basketLink.find('.cart-total');

        let hasItemsInCart = this.getCookie('woocommerce_items_in_cart');
        if (hasItemsInCart) {
            cartTotal.html('<i class="fas fa-spinner fa-spin"></i>');
            $.ajax({
                url: MyAjax.ajaxurl,
                type: 'POST',
                dataType: 'html',
                data: {
                    action: 'intus_mini_cart',
                },
                success: (data) => {
                    cartTotal.html(data);
                },
                complete: () => {
                    //this.ajaxOuter.removeClass('posts-loading').addClass('posts-loaded')
                },
                error: (errorThrown) => {
                    console.log(errorThrown)
                },
            })
        }
    },

    getCookie(name) {
        let cookieArr = document.cookie.split(';');
        for (let i = 0; i < cookieArr.length; i++) {
            let cookiePair = cookieArr[i].split('=');
            if (name == cookiePair[0].trim()) {
                return decodeURIComponent(cookiePair[1]);
            }
        }
        return null;
    },

    cachebusting() {
        //We only need to regenerate nonces for non-logged in users...
        if ( !document.body.classList.contains( 'logged-in' ) ) {
            $.ajax({
                url     : MyAjax.ajaxurl,
                type    : 'POST',
                dataType: 'json',
                data    : {
                    action: 'cachebusting_generate_nonces',
                },
                success : (result) => {
                    if( result.data !== undefined ) {
                        let data = result.data;
                        for (const property in data) {
                            try {
                                console.log( 'Setting ' + property  );
                                console.log( eval('window.' + property + '.nonce' ) );
                                eval( 'window.' + property + '.nonce = "' + data[property]  + '"' );
                                console.log( eval('window.' + property + '.nonce' ) );

                            }catch (e) {
                                console.log( e.message );
                            }
                        }
                    }
                },
                complete: () => {
                    console.log('Completed rebuilding nonces');
                },
                error   : (errorThrown) => {
                    console.log(errorThrown)
                },
            })

        }
    },

    setVatExemptionClaimed( checked ){
        if( checked ) {
            document.cookie = 'vat_exempt_declaration_checkbox=1;path=/;expires=;SameSite=Strict;secure';
        }else{
            document.cookie = 'vat_exempt_declaration_checkbox=0;path=/;SameSite=Strict;secure';
        }
    },

    showCartVatReliefTerms( location ) {
        let cartShowFullTermsLinkShow = $('#cart-vat-declaration-full-terms-show');
        let cartHideFullTermsLinkHide = $('#cart-vat-declaration-full-terms-hide');
        let cartFullTerms = $('#cart-vat-declaration-full-terms');
        if( cartFullTerms.length > 0 ){
            cartShowFullTermsLinkShow.on('click', () => {
                cartShowFullTermsLinkShow.removeClass('d-block').addClass('d-none');
                cartHideFullTermsLinkHide.removeClass('d-none').addClass('d-block');
                cartFullTerms.removeClass('d-none');
            });
            cartHideFullTermsLinkHide.on('click', () => {
                cartHideFullTermsLinkHide.removeClass('d-block').addClass('d-none');
                cartShowFullTermsLinkShow.removeClass('d-none').addClass('d-block');
                cartFullTerms.addClass('d-none');
                if( location == 'cart' ){
                    $('html, body').scrollTop($('#shop-cart-totals').offset().top );
                }
                if( location == 'checkout' ){
                    $('html, body').scrollTop($('#checkout-vat-declaration').offset().top );
                }

            })
        }
    },

};
