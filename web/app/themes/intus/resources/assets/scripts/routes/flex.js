export default {
    init() {

        // DOM Elements
        this.sliderFeaturedEl = $('.slider-featured')
        this.sliderCarouselEl = $('.slider-carousel')
        this.sliderTestimonialsEl = $('.slider-testimonials')
        this.sliderFeatProductEl = $('.slider-featured-product')
        this.sliderAlternatingEl = $('.slider-alternating')

        if (this.sliderFeaturedEl.length) {
            this.sliderFeatured()
        }

        if (this.sliderCarouselEl.length) {
            this.sliderCarousel()
        }

        if (this.sliderTestimonialsEl.length) {
            this.sliderTestimonials()
        }

        if (this.sliderFeatProductEl.length) {
            this.sliderFeatProduct()
        }

        if (this.sliderAlternatingEl.length) {
            this.sliderAlternating()
        }

    },
    sliderAlternating() {

        this.sliderAlternatingEl.slick({
            dots: true,
            arrows: false,
            infinite: false,
            rows: 0,
            fade: true,
            autoplay: true,
            slidesToScroll: 1,
            slidesToShow: 1,
        })

    },
    sliderFeatured() {

        this.sliderFeaturedEl.slick({
            dots: false,
            arrows: true,
            infinite: false,
            rows: 0,
            slidesToScroll: 1,
            slidesToShow: 2,
            prevArrow: '<button type="button" class="slick-arrow slick-arrow--prev"><i class="fal fa-long-arrow-left fa-2x"></i></button>',
            nextArrow: '<button type="button" class="slick-arrow slick-arrow--next"><i class="fal fa-long-arrow-right fa-2x"></i></button>',
            appendArrows: '.slider-featured-nav',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToScroll: 1,
                        slidesToShow: 1,
                    },
                },
            ],
        })

    },
    sliderTestimonials() {

        this.sliderTestimonialsEl.slick({
            dots: true,
            arrows: false,
            infinite: false,
            rows: 0,
            fade: true,
            autoplay: true,
            slidesToScroll: 1,
            slidesToShow: 1,
        })

    },
    sliderCarousel() {

        this.sliderCarouselEl.each((i, e) => {

            let self = $(e)
            let slidesToScroll = self.data('slides-to-scroll')

            self.slick({
                dots: true,
                arrows: true,
                infinite: false,
                autoplay: true,
                rows: 0,
                prevArrow: '<button type="button" class="slick-arrow slick-arrow--prev"><i class="fal fa-angle-left fa-3x color-mid-grey"></i></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-arrow--next"><i class="fal fa-angle-right fa-3x color-mid-grey"></i></button>',
                slidesToScroll: slidesToScroll,
                slidesToShow: slidesToScroll,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToScroll: 2,
                            slidesToShow: 2,
                        },
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToScroll: 1,
                            slidesToShow: 1,
                        },
                    },
                ],
            })
        })

    },
    sliderFeatProduct() {

        this.sliderFeatProductEl.slick({
            dots: true,
            arrows: false,
            infinite: false,
            rows: 0,
            fade: true,
            autoplay: true,
            slidesToScroll: 1,
            slidesToShow: 1,
        })

    },
};
