import 'odometer';

export default {
    init() {

        // @ref: https://blog.adriaan.io/check-if-elements-are-in-viewport-in-vanilla-javascript.html

        let self = this
        let sections = document.querySelectorAll('.iv-wp')
        let statBlocks = document.querySelectorAll('.odometer-block')

        // On load show elements currently in viewport
        this.elementsInView(sections)
        this.animateStats(statBlocks)

        // On scroll check any new elements coming into viewport
        window.onscroll = function() {
            self.elementsInView(sections)
            self.animateStats(statBlocks)
        }

    },
    elementsInView(sections) {

        // Counter
        let i

        // Don't run the rest of the code if every section is already visible
        if (document.querySelectorAll('.iv-wp:not(.el-iv)').length === 0) return

        // Run this code for every section in sections
        for (i = 0; i < sections.length; ++i) {
            if (sections[i].getBoundingClientRect().top <= window.innerHeight * 0.85 && sections[i].getBoundingClientRect().top > 0) {
                sections[i].classList.add('el-iv')
            }
        }

    },
    animateStats(sections) {

        // Counter
        let i

        // Don't run the rest of the code if every section is already visible
        if (document.querySelectorAll('.odometer-block:not(.odometer-block--animated)').length === 0) return

        // Run this code for every section in sections
        for (i = 0; i < sections.length; ++i) {
            if (sections[i].getBoundingClientRect().top <= window.innerHeight * 0.85 && sections[i].getBoundingClientRect().top > 0) {
                sections[i].classList.add('odometer-block--animated')

                const statsToAnimate = $(sections[i]).find('.odometer')

                if (statsToAnimate.length) {
                    statsToAnimate.each( (index, element) => {
                        const self = $(element)
                        setTimeout( () => {
                            self.text(self.attr('data-stat'))
                        }, 1000)
                    })
                }

            }
        }

    },
};
