import flex from './flex';

export default {
    init() {

        flex.init()

        this.notice = $('.woocommerce-notices-wrapper')
        this.formToggle = $('.checkout-login-accordion')

        if (this.notice.length) {
            $(window).load( () => {
                this.hideNotices()
            })
        }

        if (this.formToggle.length) {
            this.formToggle.click( (e) => {
                e.preventDefault()
                const self = e.currentTarget
                $(self).toggleClass('accordion-open')
                $('.checkout-login-form').slideToggle()
            })
        }

    },
    hideNotices() {

        setTimeout( () => {
            this.notice.addClass('hide-notice')
        }, 5000)

    },
};
