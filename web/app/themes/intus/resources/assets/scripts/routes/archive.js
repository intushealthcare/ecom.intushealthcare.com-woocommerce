import noUiSlider from 'nouislider';
import wNumb from 'wnumb';

export default {
    init() {

        this.advertCarousel = $('.product-gallery')
        this.priceSliderEl = document.getElementById('filter-price')
        this.productFilter = $('.product-filter')
        this.productsContainer = $('#products-container')
        this.keywordSearch = $('.product-filter__search')
        this.archiveSelect = $('#author-archive')
        this.archiveContent = $('#product-listing-content')
        this.body = $('body')
        this.overlay = $('.offcanvas-overlay')

        if (this.advertCarousel.length) {
            this.initAdvertCarousel();
        }

        if ($('#filter-price').length) {
            this.priceSlider()
        }

        if (this.productFilter.length) {
            this.initFilters()
        }

        if (this.keywordSearch.length) {
            this.keywordFilter()
        }

        if (this.archiveContent.length) {
            this.readMore()
        }

        if (this.archiveSelect.length) {
            this.archiveSelect.on('change', (e) => {
                const val = $(e.currentTarget).val()
                if (val) {
                    window.location = val
                }
            })
        }

        this.initEventListeners()

    },
    readMore() {

        const maxHeight = 200 // max height we want content not to exceed
        const readMore = this.archiveContent.find('.product-listing-content__more') // read more button
        const contentInner = this.archiveContent.find('.term-description') // inner content
        const contentHeight = contentInner[0].offsetHeight // height of inner content

        // content exceeds our max height
        if (contentHeight > maxHeight) {

            // set container to max height
            this.archiveContent.css('height', maxHeight)

            // click function to animate content to normal height and hide button
            readMore.click( () => {
                this.archiveContent.animate({
                    height: contentHeight,
                }, 500)

                this.archiveContent.addClass('expanded')
                readMore.fadeOut()
            })

        // hide read more button as content is not over max height
        } else {
            readMore.hide()
        }

        // On resize of browser set expanded block to auto height to avoid clipping
        $(window).resize( () => {
            if (this.archiveContent.hasClass('expanded')) {
                this.archiveContent.css('height', 'auto')
            }
        })

    },
    initEventListeners() {

        // Mobile button for toggling filter menu
        $('.filter-toggle').click( () => {
            this.body.toggleClass('filter-open')
            this.overlay.fadeToggle()
        })

        // Extra click function for offcanvas overlay and close button to close filter menu
        $('.offcanvas-overlay, .product-filter__close').click( () => {
            this.hideFilterOverlay()
        })

        // Prevent default form submission of order by
        $(document).on('submit', '.woocommerce-ordering', (e) => {
            e.preventDefault()
        })

        // When order by select box is changed
        $(document).on('change', '.orderby', (e) => {
            e.preventDefault()
            this.filterQuery()
        })

        // When pagination items are clicked
        $(document).on('click', '.pagination-product', (e) => {
            e.preventDefault()
            const self = $(e.currentTarget)
            const page = self.attr('data-page')
            $('html, body').animate({
                scrollTop: $('.product-listing-container').offset().top,
            }, 1000)
            this.filterQuery(page)
        })

    },
    initAdvertCarousel() {

        this.advertCarousel.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            infinite: true,
            fade: true,
            rows: 0,
        })

        this.advertCarousel.slick('refresh')

    },
    hideFilterOverlay() {

        this.body.removeClass('filter-open')
        this.overlay.fadeOut()

    },
    keywordFilter() {

        this.keywordSearch.submit( (e) => {
            e.preventDefault()
            this.filterQuery()
            this.hideFilterOverlay()
        })

        $('.product-filter__search-submit').click( (e) => {
            e.preventDefault()
            this.filterQuery()
            this.hideFilterOverlay()
        })

        $('#filter-keyword').keypress( (e) => {
            if (e.which == 13) {
                e.preventDefault()
                this.filterQuery()
                this.hideFilterOverlay()
            }
        })

    },
    priceSlider() {

        // Slider value elements
        const sliderVal = document.getElementById('filter-price-value')
        const sliderMin = document.getElementById('filter-price-min')
        const sliderMax = document.getElementById('filter-price-max')
        const priceBtn = $('#filter-price-btn')

        // Init slider
        noUiSlider.create(this.priceSliderEl, {
            start: [0, 1000],
            connect: true,
            step: 1,
            decimals: 0,
            range: {
                'min': 0,
                'max': 1500,
            },
            format: wNumb({
                prefix: '£',
                decimals: 0,
            }),
        })

        // If values already set on load
        if (sliderMin.value !== '' && sliderMax.value !== '') {
            this.priceSliderEl.noUiSlider.set([
                sliderMin.value,
                sliderMax.value,
            ])
        }

        // On change slider event
        this.priceSliderEl.noUiSlider.on('update', (values) => {
            sliderVal.innerHTML = values.join(' - ')
        })

        // Apply new filter
        priceBtn.click( () => {
            const values = this.priceSliderEl.noUiSlider.get()
            sliderMin.value = values[0].replace(/^£+/i, '')
            sliderMax.value = values[1].replace(/^£+/i, '')
            this.filterQuery()
            this.hideFilterOverlay()
        })

    },
    initFilters() {

        // Filter checkbox click function
        $('.product-filter__checkbox-item').change( (e) => {
            const self = $(e.currentTarget)
            const slug = self.val()
            const label = self.attr('data-label')

            if (self.is(':checked')) {
                this.addFilter(slug, label)
            } else {
                this.removeFilter(slug, false)
            }

            // Filter products
            this.filterQuery()
        })

        // Remove filter click function
        $(document).on('click', '.product-filter__applied-item', (e) => {
            this.removeFilter($(e.currentTarget).attr('data-slug'), true)
        })

    },
    addFilter(slug, label) {

        $('.product-filter__applied').append(`
            <div class="product-filter__applied-item" id="applied-filter-${slug}" data-slug="${slug}">
                <span class="product-filter__applied-item-label">${label}</span>
                <span class="product-filter__applied-item-remove">
                    <i class="far fa-times"></i>
                </span>
            </div>
        `)

    },
    removeFilter(slug, runQuery) {

        $('#applied-filter-' + slug).remove()
        $('#filter-checkbox-' + slug).prop('checked', false)

        // Filter products
        if (runQuery){
            this.filterQuery()
        }

    },
    filterQuery(paged) {

        let filters = {}
        let filterString = ''
        let filtersQuery = []
        const checkedItems = $('.product-filter__checkbox-item:checked')
        const minPrice = $('#filter-price-min')
        const maxPrice = $('#filter-price-max')
        const keyword = $('#filter-keyword')
        const ordering = $('.orderby')
        let filtersExist = false

        // Organise filter items into clean array
        if (checkedItems.length) {
            filters['tax'] = {};
            for (let i = 0; i < checkedItems.length; i++) {
                const key = checkedItems[i].name
                const val = checkedItems[i].value
                if (filters['tax'][key] === undefined) {
                    filters['tax'][key] = [val]
                } else {
                    filters['tax'][key].push(val)
                }
            }
        }

        // Order min and max price into a simple array
        if (minPrice.length && maxPrice.length && minPrice.val() !== '' && maxPrice.val() !== '') {
            filters['price'] = {
                price_min: minPrice.val(),
                price_max: maxPrice.val(),
            }
        }

        // Add keyword
        if (keyword.val() !== '') {
            filters['keyword'] = keyword.val()
        }

        // Add order values
        if (ordering !== '') {
            const orderVal = ordering.val()
            const orderBy = (orderVal == 'menu_order') ? 'menu_order' : 'price'
            const order = (orderVal == 'menu_order' || orderVal == 'price-desc') ? 'DESC' : 'ASC'

            filters['orderby'] = orderBy
            filters['order'] = order
        }

        // Add paged
        if (paged !== '') {
            filters['paged'] = paged
        }

        // If any filters exist begin our search query
        if (filters['tax'] !== undefined
        || filters['price'] !== undefined
        || filters['keyword'] !== undefined
        || filters['orderby'] !== undefined
        || filters['order'] !== undefined
        || filters['paged'] !== undefined)
        {
            filtersExist = true
            filterString = '?'
        }

        // If keyword parameter exists then add to query
        if (filters['keyword'] !== undefined) {
            filtersQuery.push('keyword=' + filters['keyword'])
        }

        // If taxonomy fields exist build a search query string
        if (filters['tax'] !== undefined) {
            Object.keys(filters['tax']).forEach( function(name) {
                filtersQuery.push(name + '=' + filters['tax'][name].join(','))
            })
        }

        // If price fields exist build a search query string
        if (filters['price'] !== undefined) {
            Object.keys(filters['price']).forEach( function(name) {
                filtersQuery.push(name + '=' + filters['price'][name])
            })
        }

        // If order parameters exist then add to query
        if (filters['orderby'] !== undefined) {
            filtersQuery.push('orderby=' + filters['orderby'])
        }

        if (filters['order'] !== undefined) {
            filtersQuery.push('order=' + filters['order'])
        }

        // If paged parameter exists then add to query
        if (filters['paged'] !== undefined) {
            filtersQuery.push('paged=' + filters['paged'])
        }

        // If any filters exist finish our search query
        if (filtersExist) {
            filterString += filtersQuery.join('&')
        }

        // Replace URL state to include our search query
        history.replaceState( null , null, filterString)

        // Filter products query
        this.filterProducts(filters)

    },
    setSelectOption(filters) {

        // Finds out what the active sort by item is
        const ordering = $('.orderby')
        const order = filters.order
        const orderBy = filters.orderby
        const activeItem = (order == 'DESC' && orderBy == 'price') ? orderBy + '-desc' : orderBy

        ordering.val(activeItem)

    },
    filterProducts(filters) {

        const filtersJson = JSON.stringify(filters)

        this.productsContainer.addClass('products-container--loading')

        $.ajax({
            url: MyAjax.ajaxurl,
            type: 'POST',
            dataType: 'html',
            data: {
                action: 'intus_filter_product_query',
                queried_object: MyAjax.queried_object,
                filters: filtersJson,
            },
            success: (data) => {
                this.productsContainer.empty().append(data)
            },
            complete: () => {
                this.productsContainer.removeClass('products-container--loading')
                this.setSelectOption(filters)
            },
            error: () => {
            },
        })

    },
};
