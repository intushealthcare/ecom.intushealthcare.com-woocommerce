export default {
    init() {

        this.variationFormEl = $('.variations_form');
        this.productGalleryMain = $('.product-gallery__slider--main');
        this.productGalleryNav = $('.product-gallery__slider--nav');
        this.relatedProducts = $('.related-products');
        this.variationWrap = $('.single_variation_wrap');
        this.productNoticeQuicks = $('.product-notice-quicklinks');
        //this.tabInit = false
        this.dropDownInit = false;

        this.sliderProductEl = $('.product-gallery__slider--main')

        if (this.variationFormEl.length) {
            this.fromPrice();
        }

        if (this.productGalleryMain.length) {
            this.productGallerySlider();
        }

        if (this.relatedProducts) {
            this.productRelatedSlider();
        }

        if (this.variationWrap.length) {
            this.getVariationSku();
        }

        $('.woocommerce-review-link').click((e) => {
            e.preventDefault();
            $('#nav-tab-reviews').tab('show');
            $('#collapse-heading-reviews button').trigger('click');
            $('html, body').animate({
                scrollTop: $('.tabs-container').offset().top,
            }, 1000)
        })

        $('#modal-share').on('show.bs.modal', () => {
            $('.share-buttons').find(':hidden').addBack().show();
        })

        if (this.productNoticeQuicks.length) {
            this.productNoticeQuicklinks();
        }

        if (this.sliderProductEl.length) {
            this.sliderProduct()
        }

    },

    fromPrice() {
        // Hide / Show the "from" price on variable product
        this.variationFormEl.on('hide_variation', function () {
            $('.single-variation_from').fadeIn('fast');
        })

        this.variationFormEl.on('show_variation', function () {
            $('.single-variation_from').fadeOut('fast');
        })

    },

    productRelatedSlider() {

        this.relatedProducts.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            infinite: true,
            fade: false,
            rows: 0,
            prevArrow: '<button type="button" class="slick-arrow slick-arrow--prev"><i class="far fa-2x fa-angle-left"></i></button>',
            nextArrow: '<button type="button" class="slick-arrow slick-arrow--next"><i class="far fa-2x fa-angle-right"></i></button>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                    },
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                    },
                },
                {
                    breakpoint: 564,
                    settings: {
                        slidesToShow: 1,
                    },
                },
            ],
        })

    },

    productGallerySlider() {

        this.productGalleryMain.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            infinite: false,
            fade: true,
            rows: 0,
            asNavFor: '.product-gallery__slider--nav',
            prevArrow: '<button type="button" class="slick-arrow slick-arrow--prev"><i class="far fa-2x fa-angle-left"></i></button>',
            nextArrow: '<button type="button" class="slick-arrow slick-arrow--next"><i class="far fa-2x fa-angle-right"></i></button>',
        })

        if (this.productGalleryNav.length) {
            this.productGalleryNav.slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                asNavFor: '.product-gallery__slider--main',
                dots: false,
                rows: 0,
                draggable: false,
                arrows: false,
                infinite: false,
                centerMode: false,
                focusOnSelect: true,
            })
        }

        $('.product-gallery__slider-item-enlarge').click((e) => {
            const self = $(e.currentTarget);
            const imageSrc = self.attr('data-image');
            $('#modal-enlarge-image').attr('src', imageSrc);
            $('#modal-enlarge').modal('show');
        })

        this.productGalleryMain.slick('refresh');

        if (this.productGalleryNav.length) {
            this.productGalleryNav.slick('refresh');
        }

        this.productGalleryMain.on( 'afterChange', () => {
            this.loadDeferredVideos();
        });

    },

    getVariationSku() {

        this.variationWrap.on('show_variation', function (event, variation) {
            $('.product-sku').text('SKU: ' + variation.sku);
        })

    },

    productNoticeQuicklinks() {

        $('a.product-notice-quicklink').on('click', (e) => {
            e.preventDefault();

            let scrollToQuickLink = (targetId) => {
                $('html, body').animate(
                    {
                        scrollTop: $(targetId).offset().top,
                    },
                    500,
                    'linear'
                );
            };

            let showTargetTabContent = (targetId) => {
                $('#collapse-heading-description button').removeClass('collapsed');
                $('#collapse-content-description').addClass('show');
                scrollToQuickLink(targetId);
            };

            let targetId = $(e.target).attr('href');
            let targetTab = $('#tab-description');
            let targetTabToggle = $('#nav-tab-description');

            if (targetTab.is(':visible')) {
                showTargetTabContent(targetId);
            } else {
                targetTabToggle.tab('show').on('shown.bs.tab', function () {
                    showTargetTabContent(targetId);
                });
            }
        });
    },


    loadDeferredVideos() {
        let iframes = document.getElementsByTagName('iframe');
        for ( let i=0; i<iframes.length; i++ ) {
            if( iframes[i].getAttribute('data-src') ) {
                iframes[i].setAttribute('src',iframes[i].getAttribute('data-src'));
            }
        }
    },

    pauseYoutubeVideo() {
        let productGalleryVideo = $('.product-gallery-video');
        let command = [];
        if( productGalleryVideo.hasClass( 'youtube' ) ){
            command = {
                'event': 'command',
                'func': 'pauseVideo',
                'args':'',
            };
        }

        if( productGalleryVideo.hasClass( 'vimeo' ) ){
            command = {
                'method': 'pause',
                'value': 'true',
            };
        }
        productGalleryVideo.find('iframe')[0].contentWindow.postMessage(JSON.stringify(command), '*');

    },
    sliderProduct() {
        let self = this;
        $('.slick-arrow').on('click', function() {
            self.pauseYoutubeVideo();
        });
        $('.product-gallery__slider-item-image').on('click', function() {
            self.pauseYoutubeVideo();
        });
    },
};
