import common from './common';


export default {
    init() {
        this.claimCheckoutVatRelief();
        this.showCartVatReliefTerms();
    },

    claimCheckoutVatRelief() {

        $('form.checkout').on('change', 'input[name="vat_exempt_declaration_checkbox"]', function ( event ) {
            let vat_exemption_checkbox = event.target;
            if( vat_exemption_checkbox.checked ){
                vat_exemption_checkbox.value = 1;
                common.setVatExemptionClaimed(true);
            }else{
                vat_exemption_checkbox.value = 0;
                common.setVatExemptionClaimed(false);
            }
            $('body').trigger('update_checkout');
        });

        $('form.checkout').on('change', 'select[name="billing_country"]', function () {
            let wrapper = $('#checkout-vat-declaration');
            let country = $('select[name="billing_country"]').val();

            $.ajax({
                type: 'POST',
                dataType: 'HTML',
                crossDomain: true,
                url: MyAjax.ajaxurl,
                data: {
                    action: 'woo_show_vat_declaration',
                    billing_country: country,
                },
                success: function (response) {
                    wrapper.fadeOut('fast', function () {
                        wrapper.empty().append(response);
                    });
                },
                complete: function () {
                    wrapper.fadeIn();
                },
            });

        });
    },

    showCartVatReliefTerms() {
        common.showCartVatReliefTerms('checkout' );
    },
}
