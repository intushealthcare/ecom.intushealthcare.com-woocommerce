export default {
    init() {

        this.categoryFilter = $('.blog-category-filter select')

        if (this.categoryFilter.length) {
            this.categoryFilter.change( (e) => {
                e.preventDefault()
                const category = $(e.currentTarget).val()

                if (category) {
                    window.location.href = category
                }
            })
        }

    },
};
