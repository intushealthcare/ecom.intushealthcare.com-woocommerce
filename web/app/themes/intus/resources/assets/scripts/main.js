// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// Import NPM packages
import 'slick-carousel';

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import templateFlexible from './routes/flex';
import templateFaqs from './routes/faqs';
import templateBuying from './routes/flex';
import templateBlog from './routes/blog';
import category from './routes/blog';
import woocommercePage from './routes/woo';
import archive from './routes/archive';
import singleProduct from './routes/product';
import woocommerceCart from './routes/cart';
import woocommerceCheckout from './routes/checkout';

/** Populate Router instance with DOM routes */
const routes = new Router({
    // All pages
    common,
    // Flex template
    templateFlexible,
    // Flex template
    templateFaqs,
    // Buying template
    templateBuying,
    // Blog template
    templateBlog,
    // Woo Template
    woocommercePage,
    // Archive Template
    archive,
    // Product single
    singleProduct,
    // Category
    category,
    // Cart
    woocommerceCart,
    // Checkout
    woocommerceCheckout,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());

