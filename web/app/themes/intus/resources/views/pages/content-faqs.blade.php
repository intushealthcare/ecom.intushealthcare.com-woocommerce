@php $parent_terms = TemplateFaqs::faqCategories() @endphp

<div class="container-fluid bg-secondary-light">
    <div class="container position-relative z-index-1 pst-1 d-flex flex-column">
        <div class="row justify-content-md-center">
            <div
                id="faq-search-container"
                class="col-12 col-lg-10 col-xl-9 faq-search-container"
            >

                {{-- Title --}}
                @if (!empty($faq_fields['title']))
                    <h1 class="h2 text-lighter text-center">
                        {!! $faq_fields['title'] !!}
                    </h1>
                @endif

                {{-- Search form --}}
                <div class="text-center pt-5 pb-4">
                    <form
                        action="{{ get_bloginfo('url') }}"
                        id="faq-search-form"
                        class="faq-search"
                    >
                        <label
                            for="faq-search-input"
                            class="sr-only"
                        >
                            {!! __('Search FAQs', 'sage') !!}
                        </label>
                        <input
                            type="search"
                            name="s"
                            value="{{ the_search_query() }}"
                            id="faq-search-input"
                            placeholder="{{ __('Type your question', 'sage') }}"
                        >
                        <button
                            type="submit"
                            class="faq-search__submit"
                        >
                            <i class="fal fa-lg fa-search"></i>
                        </button>
                    </form>
                </div>

                {{-- Featured Questions --}}
                @if (!empty($faq_fields['featured_questions']))
                    <div class="row justify-content-md-center d-none d-lg-flex">
                        <div class="col-12 col-md-8 col-lg-7 col-xl-6">
                            <ul class="list-clear-all mt-5">
                                @foreach ($faq_fields['featured_questions'] as $question)
                                    <li>
                                        <p>
                                            <a
                                                href="#"
                                                class="faq-featured-question d-flex align-items-center color-secondary"
                                                data-value="{{ str_replace(array('?'), '', $question['text']) }}"
                                            >
                                                <i class="fal fa-lg fa-angle-right"></i>
                                                <span class="pl-4">{!! $question['text'] !!}</span>
                                            </a>
                                        </p>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif

                {{-- Tab links --}}
                @if (!empty($parent_terms))
                    <div class="pst-2 mt-auto position-relative">
                        <button
                            id="faq-search-back"
                            type="button"
                            class="btn btn--white faq-search-back"
                        >
                            {!! __('Back to all FAQs', 'sage') !!}
                        </button>

                        <ul
                            class="nav nav-tabs"
                            role="tablist"
                        >
                            @foreach ($parent_terms as $term)
                                <li class="nav-item">
                                    <a
                                        class="nav-link{{ ($loop->iteration == 1) ? ' active' : '' }}"
                                        id="faq-type-{{ $loop->iteration }}-tab"
                                        data-toggle="tab"
                                        href="#faq-type-{{ $loop->iteration }}"
                                        role="tab"
                                        aria-controls="faq-type-{{ $loop->iteration }}"
                                        aria-selected="{{ ($loop->iteration == 1) ? 'true' : 'false' }}"
                                    >
                                        {!! $term->name !!}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            </div>
        </div>
    </div>

    {{-- Image --}}
    @if (!empty($faq_fields['image']))
        <img
            src="{{ $faq_fields['image']['sizes']['large'] }}"
            alt="{{ $faq_fields['image']['alt'] }}"
            class="faq-image"
            draggable="false"
        >
    @endif

</div>

<div
    id="faq-ajax-outer"
    class="container-fluid bg-white px-0 faq-ajax-outer"
>
    <div class="container psy-1">
        <div class="row justify-content-md-center">
            <div
                id="faq-ajax-container"
                class="col-12 col-lg-10 col-xl-9 faq-ajax-container"
            >
                <div
                    class="tab-content"
                >

                    {{-- Tab content --}}
                    @php $x = 1 @endphp
                    @foreach ($parent_terms as $parent_term)
                        @php $child_terms = TemplateFaqs::faqCategories($parent_term->term_id) @endphp
                        <div
                            class="tab-pane fade{{ ($x == 1) ? ' show active' : '' }}"
                            id="faq-type-{{ $x }}"
                            role="tabpanel"
                            aria-labelledby="faq-type-{{ $x }}-tab"
                        >
                            @if (!empty($child_terms))
                                @php $y = 1 @endphp
                                @foreach ($child_terms as $child_term)
                                    @php $faqs = TemplateFaqs::getFaqPosts($child_term) @endphp
                                    @if ($faqs->have_posts())
                                        <h3 class="text-lighter">{!! $child_term->name !!}</h3>
                                        <div
                                            id="accordion-{{ $x }}-{{ $y }}"
                                            class="accordion accordion--faqs mb-5"
                                        >
                                            @php $z = 1 @endphp
                                            @while($faqs->have_posts())
                                                @php $faqs->the_post() @endphp
                                                <div class="card">
                                                    <div
                                                        class="card-header"
                                                        id="accordion-{{ $x }}-{{ $y }}-item-header-{{ $z }}"
                                                    >
                                                        <button
                                                            class="collapsed"
                                                            data-toggle="collapse"
                                                            data-target="#accordion-{{ $x }}-{{ $y }}-item-collapse-{{ $z }}"
                                                            aria-expanded="{{ ($z == 1) ? 'true' : 'false' }}"
                                                            aria-controls="accordion-{{ $x }}-{{ $y }}-item-collapse-{{ $z }}"
                                                        >
                                                            <span>@php the_title() @endphp</span>
                                                            <i class="fal fa-2x fa-angle-down down-arrow"></i>
                                                            <i class="fal fa-2x fa-angle-up up-arrow"></i>
                                                        </button>
                                                    </div>
                                                    <div
                                                        id="accordion-{{ $x }}-{{ $y }}-item-collapse-{{ $z }}"
                                                        class="collapse"
                                                        aria-labelledby="accordion-{{ $x }}-{{ $y }}-item-header-{{ $z }}"
                                                        data-parent="#accordion-{{ $x }}-{{ $y }}"
                                                    >
                                                        <div class="pt-5">
                                                            @php the_content() @endphp
                                                        </div>
                                                    </div>
                                                </div>
                                                @php $z++ @endphp
                                            @endwhile
                                            @php wp_reset_postdata() @endphp
                                        </div>
                                    @endif
                                    @php $y++ @endphp
                                @endforeach
                            @endif
                        </div>
                        @php $x++ @endphp
                    @endforeach

                </div>
            </div>
        </div>
    </div>
    <div class="faq-loader"><i class="fal fa-2x fa-circle-notch fa-spin"></i></div>
</div>
