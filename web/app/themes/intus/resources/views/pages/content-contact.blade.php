<div class="container psy-1">
    <div class="row">
        <div class="col-12 col-lg-8">

            @if (!empty($main_content))
                {!! $main_content !!}
            @endif

        </div>
        <div class="col-12 col-lg-4 mt-5 mt-lg-0">

            @if (!empty($sidebar_content))
                <div class="contact-sidebar">
                    {!! $sidebar_content !!}
                </div>
            @endif

        </div>
    </div>
</div>
