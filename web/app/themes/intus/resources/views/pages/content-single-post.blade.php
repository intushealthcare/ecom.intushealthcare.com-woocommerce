<article
    itemscope
    itemtype="http://schema.org/NewsArticle"
>

    {{-- Header Content --}}
    <div class="container">
        <div class="position-relative post-banner">
            <div class="container psy-3 z-index-1">
                <div class="row justify-content-md-center">
                    <div class="col-12 col-md-11 col-lg-10 col-xl-9">

                        {{-- Title --}}
                        <h1
                            class="h2"
                            itemprop="headline"
                        >
                            @php echo get_the_title() @endphp
                        </h1>

                        {{-- Date --}}
                        <time
                            datetime="{{ get_the_date('c') }}"
                            itemprop="datePublished"
                            class="color-white"
                        >
                            @php echo get_the_date() @endphp
                        </time>
                    </div>
                </div>
            </div>

            @if (!empty($post_image['image']))
                {{-- Banner image --}}
                <img
                    class="stretch-container img-cover"
                    src="{{ $post_image['image'] }}"
                    alt="{{ $post_image['alt'] }}"
                    height="{{ $post_image['height'] }}"
                    width="{{ $post_image['width'] }}"
                    itemscope
                    itemprop="image"
                    itemtype="https://schema.org/ImageObject"
                >
            @endif

        </div>

    </div>

    <div class="container post-reviewer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 my-3 my-md-4 post-reviewer-user post-reviewer-user__author">
                    @include('partials.author-biog',['user' =>  $post_author ] )
                </div>
                @if (!empty($post_reviewer))
                <div class="col-12 col-md-6  my-3 my-md-4 post-reviewer-user post-reviewer-user__reviewer">
                    @include('partials.author-biog',['user' =>  $post_reviewer ] )
                </div>
                @endif
            </div>
            @if (!empty($post_reviewer))
            <div class="row">
                <div class="col-12 post-reviewer-note">
                    {!!  $reviewer_note['note'] !!}
                </div>
            </div>
            @endif
        </div>
    </div>

    {{-- Content --}}
    <div class="container">
        <div class="row justify-content-md-center psy-1">
            <div class="col-12 col-md-11 col-lg-10 col-xl-9">

                {{-- Main content --}}
                @if (!empty($post_content))
                    <div class="post-single-content">
                        {!! $post_content !!}
                    </div>
                @endif

            </div>
        </div>
    </div>

</article>

{{-- Navigation --}}
@include('partials.post-navigation')
