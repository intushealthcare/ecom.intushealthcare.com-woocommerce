<div class="container psy-3">
    <div class="row justify-content-md-center">
        <div class="col-12 col-md-9 col-lg-7 col-xl-6 text-center">
            <h1>404</h1>
            <h3>{!! __('Sorry, we could not find the page you were looking for', 'sage') !!}</h3>
            <div class="mt-5">
                <a
                    href="{{ get_bloginfo('url') }}"
                    class="btn"
                >
                    {!! __('Back to home', 'sage') !!}
                </a>
            </div>
        </div>
    </div>
</div>
