@php
    $current = (is_category()) ? get_queried_object() : '';
    $year = (is_date()) ? get_query_var('year') : '';
    $month = (is_date()) ? get_query_var('monthnum') : '';

    $blog_posts = App::getBlogPosts($current, $year, $month);
    $categories = App::getBlogCategories();
    $content = App::getBlogContent();
@endphp

{{-- Main content --}}
@if (!empty($content))
    <div class="container-fluid px-0 bg-light-grey">
        <div class="container psy-1">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-10 col-lg-9 col-xl-8 text-center">
                    {!! $content !!}
                </div>
            </div>
        </div>
    </div>
@endif

<div class="container psy-1">

    {{-- Category navigation --}}
    <ul class="d-none d-md-flex flex-md-wrap list-clear-all align-items-center justify-content-center">
        <li class="px-4">
            <a
                class="blog-category-link px-4{{ (!$current) ? ' active' : ''}}"
                href="{{ get_permalink($global_fields['blog_page']) }}"
            >
                {!! __('All', 'sage') !!}
            </a>
        </li>
        @foreach ($categories as $category)
            <li class="px-4">
                <a
                    class="blog-category-link px-4{{ ((!empty($current)) && $category->term_id == $current->term_id) ? ' active' : ''}}"
                    href="{{ get_category_link($category->term_id) }}"
                >
                    {!! $category->name !!}
                </a>
            </li>
        @endforeach
    </ul>

    {{-- Mobile category navigation --}}
    <div class="d-block d-md-none">
        <h5>{!! __('Filter By Category', 'sage') !!}</h5>
        <div class="blog-category-filter">
            <select>
                <option value="{{ get_permalink($global_fields['blog_page']) }}">{!! __('All', 'sage') !!}</option>
                @foreach ($categories as $category)
                    <option
                        value="{{ get_category_link($category->term_id) }}"
                        {{ ((!empty($current)) && $category->term_id == $current->term_id) ? 'selected' : ''}}
                    >
                        {!! $category->name !!}
                    </option>
                @endforeach
            </select>
            <div class="blog-category-filter__icon">
                <i class="fal fa-angle-down fa-lg"></i>
            </div>
        </div>
    </div>

</div>

<div class="container psb-1">

    {{-- Posts loop --}}
    <div class="row">
        @if ($blog_posts->have_posts())
            @while ($blog_posts->have_posts())
                @php $blog_posts->the_post() @endphp
                <div class="col-12 col-md-6 col-lg-4 mb-5">
                    @include('partials.blog-listing-item')
                </div>
            @endwhile
            @php wp_reset_postdata() @endphp
        @endif
    </div>

    {{-- Pagination --}}
    {!! App::numericPagination($blog_posts) !!}

</div>
