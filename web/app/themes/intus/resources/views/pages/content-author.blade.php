<div class="psy-1 container">
    <div class="row">
        <div class="col-12 col-lg-8">

            {{-- Author --}}
            <div class="d-flex align-items-center">
                <img
                    class="author-image"
                    src="{{ $post_author['image']['sizes']['medium'] }}"
                    alt="{{ $post_author['image']['alt'] }}"
                >
                <div class="pl-4">
                    <h1 class="h3 mb-0">
                        @php printf( __( '%s', 'sage' ), get_the_author() ) @endphp
                    </h1>
                    @if ( !empty($post_author['jobtitle'] ) )
                    <p class="author-job-title mb-0">{{ $post_author['jobtitle'] }}</p>
                    @endif
                </div>
            </div>

            {{-- Author description --}}
            @if ( !empty($post_author['description'] ) )
                <div class="mt-5">
                    <p>{{ $post_author['description'] }}</p>
                </div>
            @endif
            @if( !empty( $post_author['linkedin'] ) )
                <p class="mt-4 text-bold">Visit <a href="{{ $post_author['linkedin'] }}">@php printf( __( "%s's", 'sage' ), get_the_author() ) @endphp LinkedIn page</a> to see their credentials</p>
            @endif

            @if (have_posts())

                {{-- Global data --}}
                @php
                    global $wp_query;
                    $categories = App::getBlogCategories();
                @endphp

                {{-- Post data --}}
                @php
                    the_post();
                    global $post;
                    $avatar = get_field('profile_image', 'user_' . $post->post_author);
                    $image = (!empty($avatar)) ? $avatar : get_field('user_profile_image', 'option');
                @endphp

                {{-- Posts --}}
                @php rewind_posts() @endphp
                <div class="author-posts">
                    @while(have_posts())
                        @php
                            the_post();
                            $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail');
                            $terms = App::getPostCategories();
                        @endphp
                        <a
                            class="author-posts__item"
                            href="{{ get_permalink() }}"
                        >
                            <div
                                class="author-posts__item-image"
                                style="background-image: url({{ (!empty($image)) ? $image[0] : wc_placeholder_img_src('thumbnail') }});"
                            >
                            </div>
                            <div class="pl-5 d-flex flex-column justify-content-center">
                                <h3 class="h4 mb-2 color-secondary">
                                    {!! get_the_title() !!}
                                </h3>
                                <div class="d-flex align-items-center">
                                    <time
                                        datetime="{{ get_the_date('c') }}"
                                        itemprop="datePublished"
                                        class="color-primary p-sm mb-0"
                                    >
                                        {!! get_the_date() !!}
                                    </time>
                                    @if (!empty($terms))
                                        <ul class="author-posts__item-terms">
                                            @foreach ($terms as $term)
                                                <li>
                                                    <p class="p-sm mb-0">{!! $term->name !!}</p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        </a>
                    @endwhile
                </div>

                {{-- Pagination --}}
                {!! App::numericPagination($wp_query) !!}

                @php wp_reset_postdata() @endphp
            @endif
        </div>
        <div class="col-12 col-lg-4 col-xl-3 offset-xl-1 mt-5 mt-lg-0">

            <div class="authors-sidebar">

                @if (!empty($categories))
                    <div class="pb-4">
                        <h2 class="p text-bold authors-sidebar__title">{!! __('Categories', 'sage') !!}</h2>
                        <ul class="list-clear-all d-flex flex-column">
                            @foreach ($categories as $category)
                                <li class="mb-4">
                                    <a href="{{ get_term_link($category->term_id, 'category') }}">
                                        {!! $category->name !!}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="pt-5">
                    <h2 class="p text-bold authors-sidebar__title">{!! __('Archive', 'sage') !!}</h2>
                    <div class="field-wrap field-wrap--select">
                        <div class="field-wrap__input">
                            <select
                                name="author-archive"
                                id="author-archive"
                            >
                                <option value="" selected disabled>{!! __('Select Month', 'sage') !!}</option>
                                @php
                                    wp_get_archives([
                                        'type' => 'monthly',
                                        'format' => 'option'
                                    ]);
                                @endphp
                            </select>
                            <i class="fal fa-lg fa-angle-down"></i>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
