{{--
Template Name: Blog Template
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts())
        @php the_post() @endphp
        @include('pages.content-blog')
    @endwhile
@endsection
