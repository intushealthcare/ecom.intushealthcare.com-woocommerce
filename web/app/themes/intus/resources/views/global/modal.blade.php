@if ($global_fields['add_global_modal'] && !empty($global_fields['global_modal']))
    <div
        class="modal modal--global fade"
        id="modal-global"
        tabindex="-1"
        role="dialog"
        aria-hidden="true"
    >
        <div
            class="modal-dialog modal-lg"
            role="document"
        >
            <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
            >
                <span aria-hidden="true">
                    <i class="fal fa-2x fa-times"></i>
                </span>
            </button>
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row no-gutters">
                        <div class="col-12 col-lg-6">

                            @if (!empty($global_fields['global_modal']['image']))
                                <div
                                    class="stretch-container bg-cover h-100 w-100"
                                    style="background-image: url({{ $global_fields['global_modal']['image']['sizes']['large'] }});"
                                >
                                </div>
                            @endif

                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="modal__inner">

                                @if (!empty($global_fields['global_modal']['content']))
                                    {!! $global_fields['global_modal']['content'] !!}
                                @endif

                                @if (!empty($global_fields['global_modal']['button']))
                                    <div class="mt-4">
                                        @include('partials.button', [
                                            'button' => $global_fields['global_modal']['button'],
                                        ])
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
