<div class="quick-basket">
    <div class="quick-basket__items">
        @foreach ( WC()->cart->get_cart() as $cart_item )
            @php
                $data = $cart_item['data'];
                $item_name = $data->get_title();
                $quantity = $cart_item['quantity'];
                $price = $data->get_price_html();
                $image = $data->get_image();
            @endphp
            <div class="d-flex align-items-center mb-3">
                {!! $image !!}
                <div>
                    <p class="text-bold d-block p-xs">{!! $item_name  !!}</p>
                    <div class="d-flex justify-content-between">
                        <p class="p-xs">{!! __('Qty:', 'sage') !!}&nbsp;{!! $quantity !!}</p>
                        <p class="p-xs">{!! $price !!}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row row--sm">
        <div class="col-6">
            <a
                href="{{ wc_get_cart_url() }}"
                class="btn btn--nomin btn--sm w-100"
            >
                {!! __('Basket', 'sage') !!}
            </a>
        </div>
        <div class="col-6">
            <a
                href="{{ wc_get_checkout_url() }}"
                class="btn btn--nomin btn--sm w-100"
            >
                {!! __('Checkout', 'sage') !!}
            </a>
        </div>
    </div>

</div>
