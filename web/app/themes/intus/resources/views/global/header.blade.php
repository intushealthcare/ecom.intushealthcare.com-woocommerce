<header class="main-header">

    {{-- Top header bar --}}
    <div class="container-fluid px-0 bg-secondary">
        <div class="container py-1 py-lg-3 d-block d-lg-flex justify-content-lg-between color-white">
            <div class="d-none d-lg-flex align-items-center">
                @include('partials.social-channels', [
                    'color' => 'color-white',
                ])
                <span class="color-white px-5">|</span>
                @if (!empty($header_fields['phone_number']))
                    <a
                        href="tel:{{ str_replace(' ', '', $header_fields['phone_number']) }}"
                        class="color-white text-bold d-none d-lg-flex align-items-center"
                    >
                        <span>{!! $header_fields['phone_text'] !!} {!! $header_fields['phone_number'] !!}</span><i class="fas fa-phone pl-3"></i>
                    </a>
                @endif
            </div>
            <div>
                {!! do_shortcode('[wcas-search-form]') !!}
            </div>
        </div>
    </div>

    {{-- Main navigation bar --}}
    <div class="container-fluid px-0 bg-white">

        {!! \App\trustpilot_get_widget_html('horizontal') !!}

        <div class="container d-flex justify-content-between">
        {{-- Logo --}}
        <div class="d-flex align-items-center">
            @if (!empty($header_fields['logo']))
                <a
                    href="{{ get_bloginfo('url') }}"
                    class="main-header__logo"
                >
                    <img
                        src="{{ $header_fields['logo']['sizes']['medium'] }}"
                        alt="{{ $header_fields['logo']['alt'] }}"
                        width="{{ $header_fields['logo']['width'] }}"
                        height="{{ $header_fields['logo']['height'] }}"
                        class="img-fluid"
                    >
                </a>
            @endif
        </div>

        <div class="d-flex justify-content-end align-items-center">

            {{-- Navigation  --}}
            @if (has_nav_menu('primary_navigation'))
                <nav class="main-header__nav d-none d-xl-block">
                    {!!
                        wp_nav_menu([
                            'theme_location' => 'primary_navigation',
                            'menu_class' => 'nav nav--primary',
                            'walker' => new App\Intus_Meganav_Walker(),
                            'depth' => 0
                        ])
                    !!}
                </nav>
            @endif

            {{-- Icons --}}
            <ul class="main-header__icons">
                <li class="d-none d-xl-inline-block">
                    <a href="{{ get_permalink(get_option('woocommerce_myaccount_page_id')) }}">
                        <i class="far fa-lg fa-user-alt"></i>
                    </a>
                </li>
                <li class="d-inline-block d-xl-none">
                    <a href="#" class="toggle-offcanvas">
                        <i class="fal fa-2x fa-bars"></i>
                    </a>
                </li>
                <li class="d-inline-block header-quick-basket">
                    <a href="{{ wc_get_cart_url() }}" class="basket-link d-inline-block">
                        <i class="far fa-lg fa-shopping-cart"></i>
                        <span class="cart-total ml-2 d-inline-block">0</span>
                    </a>
                </li>
            </ul>

        </div>

    </div>
    </div>
</header>

{{-- Store notice --}}
@if (!empty($header_fields['store_notice']['text']))
    @if ($header_fields['store_notice']['basket_checkout_only'])
        @if (is_cart() || is_checkout())
            @include('partials.store-notice')
        @endif
    @else
        @include('partials.store-notice')
    @endif
@endif
