<footer class="main-footer">
    @if ($is_woo_page)
        @if (!empty($footer_fields['icon_list']))
            @include('blocks.carousel', [
                'options' => 'pst-1 psb-1',
                'fields' => [
                    'style' => 'image-content',
                    'content' => (!empty($footer_fields['icon_list_content'])) ? $footer_fields['icon_list_content'] : '',
                    'bg_colour' => 'lightest-grey',
                    'slides' => $footer_fields['icon_list']
                ]
            ])
        @endif
    @endif

    @if (!$is_woo_page)
        @include('partials.newsletter-signup')
    @endif

    <div class="container-fluid px-0 bg-white">
        <div class="container psy-1">
            <div class="row">
                <div class="col-12 col-lg-3 mb-5 mb-lg-0">

                    @if (!empty($header_fields['logo']))
                        <a
                            href="{{ get_bloginfo('url') }}"
                            class="main-footer__logo text-center text-lg-left"
                        >
                            <img
                                src="{{ $header_fields['logo']['sizes']['medium'] }}"
                                alt="{{ $header_fields['logo']['alt'] }}"
                                width="{{ $header_fields['logo']['width'] }}"
                                height="{{ $header_fields['logo']['height'] }}"
                                class="img-fluid"
                            >
                        </a>
                    @endif

                    <div class="my-5 ml-4">
                        @include('partials.social-channels', [
                            'size' => 'fa-lg',
                            'class' => 'justify-content-center justify-content-lg-start'
                        ])
                    </div>

                </div>
                <div class="col-12 col-lg-6 mb-5 mb-lg-0 pt-lg-4">
                    <div class="row">
                        @if (has_nav_menu('footer_navigation'))
                            <div class="col-12">
                                <nav class="main-footer__nav">
                                    {!!
                                        wp_nav_menu([
                                            'theme_location' => 'footer_navigation',
                                            'menu_class' => 'nav nav--footer nav--footer_2col'
                                        ])
                                    !!}
                                </nav>
                            </div>
                        @endif

                        @if (has_nav_menu('footer_navigation_1'))
                            <div class="col-12 col-lg-4">
                                <nav class="main-footer__nav">
                                    {!!
                                        wp_nav_menu([
                                            'theme_location' => 'footer_navigation_1',
                                            'menu_class' => 'nav nav--footer'
                                        ])
                                    !!}
                                </nav>
                            </div>
                        @endif

                        @if (has_nav_menu('footer_navigation_2'))
                            <div class="col-12 col-lg-4">
                                <nav class="main-footer__nav">
                                    {!!
                                        wp_nav_menu([
                                            'theme_location' => 'footer_navigation_2',
                                            'menu_class' => 'nav nav--footer'
                                        ])
                                    !!}
                                </nav>
                            </div>
                        @endif

                        @if (has_nav_menu('footer_navigation_3'))
                            <div class="col-12 col-lg-4">
                                <nav class="main-footer__nav">
                                    {!!
                                        wp_nav_menu([
                                            'theme_location' => 'footer_navigation_3',
                                            'menu_class' => 'nav nav--footer'
                                        ])
                                    !!}
                                </nav>
                            </div>
                        @endif
                    </div>

                </div>
                <div class="col-12 col-lg-3 color-secondary pt-4 text-center text-lg-left">

                    @if (!empty($footer_fields['contact']))
                        {!! $footer_fields['contact'] !!}
                    @endif

                    @if (!empty($footer_fields['card_images']))
                        <ul class="main-footer__cards">
                            @foreach ($footer_fields['card_images'] as $card_image)
                                <li>
                                    <i class="fa-3x {{ $card_image['icon'] }}"></i>
                                </li>
                            @endforeach
                        </ul>
                    @endif

                </div>
            </div>

            @if (!empty($footer_fields['copyright']))
                <div class="text-center mst-2 d-flex justify-content-center align-items-center">
                    <p class="p-sm mb-0">{!! $footer_fields['copyright'] !!}</p>

                    @if (shortcode_exists('cookie_settings'))
                        <p class="p-sm mb-0 mx-4">|</p>
                        {!! do_shortcode('[cookie_settings]') !!}
                    @endif
                </div>
            @endif
        </div>
    </div>
</footer>
