<div class="offcanvas">
    <div class="offcanvas__inner">

        {{-- Navigation  --}}
        @if (has_nav_menu('offcanvas_navigation'))
            <nav class="offcanvas__nav">
                {!!
                    wp_nav_menu([
                        'theme_location' => 'offcanvas_navigation',
                        'menu_class' => 'nav nav--offcanvas',
                    ])
                !!}
            </nav>
        @endif

    </div>
    <div class="offcanvas__close">
        <i class="fal fa-angle-left fa-2x"></i>
    </div>
</div>

<div class="offcanvas-overlay"></div>
