@php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

@endphp

<div class="container psy-1">
    @php wc_print_notices() @endphp

    @php do_action( 'woocommerce_before_customer_login_form' ) @endphp

    <div class="row justify-content-md-center">
        <div class="col-12 col-md-8 col-lg-7 col-xl-5">

            {{-- Nav tabs --}}
            @if (get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes')
                <ul
                    class="nav nav-pills"
                    id="login-tabs"
                    role="tablist"
                >
                    <li class="nav-item">
                        <a
                            class="nav-link active"
                            id="tab-login"
                            data-toggle="tab"
                            href="#tab-content-login"
                            role="tab"
                            aria-controls="tab-content-login"
                            aria-selected="true"
                        >
                            {!! __('Login', 'woocommerce') !!}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a
                            class="nav-link"
                            id="tab-register"
                            data-toggle="tab"
                            href="#tab-content-register"
                            role="tab"
                            aria-controls="tab-content-register"
                            aria-selected="false"
                        >
                            {!! __('Register', 'woocommerce') !!}
                        </a>
                    </li>
                </ul>
                <div
                    class="tab-content"
                    id="login-tabs-content"
                >
                    <div
                        class="tab-pane fade show active"
                        id="tab-content-login"
                        role="tabpanel"
                        aria-labelledby="tab-content-login"
                    >
                        @endif

                        {{-- Login form --}}
                        <h2 class="text-center mb-5">@php esc_html_e('My Account', 'woocommerce') @endphp</h2>

                        <form
                            class="woocommerce-form woocommerce-form-login login"
                            method="post"
                        >

                            @php do_action('woocommerce_login_form_start') @endphp

                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                <label for="username">
                                    @php esc_html_e('Username or email address', 'woocommerce') @endphp
                                    &nbsp;<span class="required">*</span>
                                </label>
                                <input
                                    type="text"
                                    class="woocommerce-Input woocommerce-Input--text input-text"
                                    name="username"
                                    id="username"
                                    autocomplete="username"
                                    value="{{ ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : '' }}"
                                />
                            </p>
                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                <label for="password">
                                    @php esc_html_e('Password', 'woocommerce') @endphp
                                    &nbsp;<span class="required">*</span>
                                </label>
                                <input
                                    class="woocommerce-Input woocommerce-Input--text input-text"
                                    type="password"
                                    name="password"
                                    id="password"
                                    autocomplete="current-password"
                                />
                            </p>

                            @php do_action('woocommerce_login_form') @endphp

                            <p class="form-row">
                                @php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ) @endphp
                                <button
                                    type="submit"
                                    class="btn btn--primary"
                                    name="login"
                                    value="@php esc_attr_e( 'Log in', 'woocommerce' ) @endphp"
                                >
                                    @php esc_html_e( 'Log in', 'woocommerce' ) @endphp
                                </button>
                            </p>

                            <div class="d-flex align-items-center justify-content-between">
                                <p>
                                    <label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
                                        <input
                                            class="woocommerce-form__input woocommerce-form__input-checkbox"
                                            name="rememberme"
                                            type="checkbox"
                                            id="rememberme"
                                            value="forever"
                                        />
                                        <span>@php esc_html_e( 'Remember me', 'woocommerce' ) @endphp</span>
                                    </label>
                                </p>
                                <p class="woocommerce-LostPassword lost_password">
                                    <a
                                        href="{{ esc_url( wp_lostpassword_url() ) }}"
                                        class="text-underline color-dark-grey"
                                    >
                                        @php esc_html_e( 'Forgot your password?', 'woocommerce' ) @endphp
                                    </a>
                                </p>
                            </div>

                            @php do_action( 'woocommerce_login_form_end' ) @endphp
                        </form>


                        @if (get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes')
                    </div>
                    {{-- Close login tab pane --}}

                    <div
                        class="tab-pane fade"
                        id="tab-content-register"
                        role="tabpanel"
                        aria-labelledby="tab-content-register"
                    >

                        {{-- Register form --}}
                        <h2 class="text-center mb-5">@php esc_html_e( 'Register', 'woocommerce' ) @endphp</h2>

                        <form
                            method="post"
                            class="woocommerce-form woocommerce-form-register register"
                        >

                            @php do_action( 'woocommerce_register_form_start' ) @endphp

                            @if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) )

                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="reg_username">
                                        @php esc_html_e( 'Username', 'woocommerce' ) @endphp
                                        &nbsp;<span class="required">*</span>
                                    </label>
                                    <input
                                        type="text"
                                        class="woocommerce-Input woocommerce-Input--text input-text"
                                        name="username"
                                        id="reg_username"
                                        autocomplete="username"
                                        value="{{ ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : '' }}"
                                    />
                                </p>

                            @endif

                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                <label for="reg_email">
                                    @php esc_html_e( 'Email address', 'woocommerce' ) @endphp
                                    &nbsp;<span class="required">*</span>
                                </label>
                                <input
                                    type="email"
                                    class="woocommerce-Input woocommerce-Input--text input-text"
                                    name="email"
                                    id="reg_email"
                                    autocomplete="email"
                                    value="{{ ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : '' }}"
                                />
                            </p>

                            @if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) )

                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="reg_password">
                                        @php esc_html_e( 'Password', 'woocommerce' ) @endphp
                                        &nbsp;<span class="required">*</span>
                                    </label>
                                    <input
                                        type="password"
                                        class="woocommerce-Input woocommerce-Input--text input-text"
                                        name="password"
                                        id="reg_password"
                                        autocomplete="new-password"
                                    />
                                </p>

                            @endif

                            @php do_action( 'woocommerce_register_form' ) @endphp

                            <p class="woocommerce-FormRow form-row">
                                @php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ) @endphp
                                <button
                                    type="submit"
                                    class="btn btn--primary"
                                    name="register"
                                    value="@php esc_attr_e( 'Register', 'woocommerce' ) @endphp"
                                >
                                    @php esc_html_e( 'Register', 'woocommerce' ) @endphp
                                </button>
                            </p>

                            @php do_action( 'woocommerce_register_form_end' ) @endphp

                        </form>
                    </div>


                </div>
                {{-- Close #login-tabs-content --}}
            @endif

        </div>
    </div>

    @php do_action( 'woocommerce_after_customer_login_form' ) @endphp
</div>
