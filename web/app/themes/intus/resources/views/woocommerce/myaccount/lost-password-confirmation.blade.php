@php
/**
 * Lost password confirmation text.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/lost-password-confirmation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.9.0
 */

defined( 'ABSPATH' ) || exit;

@endphp

<div class="container psy-1">
    <div class="row justify-content-md-center">
        <div class="col-12 col-md-8 col-lg-7 col-xl-5">
            <h2 class="text-center mb-5">@php esc_html_e('Lost Password', 'woocommerce') @endphp</h2>
            <div class="mb-5">
                @php wc_print_notice( esc_html__( 'Password reset email has been sent.', 'woocommerce' ) ); @endphp
            </div>

            @php do_action( 'woocommerce_before_lost_password_confirmation_message' ); @endphp

            <p>@php echo esc_html( apply_filters( 'woocommerce_lost_password_confirmation_message', esc_html__( 'A password reset email has been sent to the email address on file for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.', 'woocommerce' ) ) ); @endphp</p>

            @php do_action( 'woocommerce_after_lost_password_confirmation_message' ); @endphp
        </div>
    </div>
</div>
