@php
    defined( 'ABSPATH' ) || exit;
@endphp

{{-- Header --}}
<header class="bg-secondary-light">
    <div class="container text-center py-5">

        {{-- Breadcrumbs --}}
        @php
            do_action('woocommerce_before_main_content');
        @endphp

        <div class="row justify-content-md-center archive-description">
            <div class="col-12 col-md-10 col-lg-9 col-xl-8">

                {{-- Title & Description --}}
                @if (is_shop())
                    {!! get_field('main_content', wc_get_page_id('shop')) !!}
                @elseif (apply_filters('woocommerce_show_page_title', true))
                    <h1 class="h2 mt-md-5">{!! woocommerce_page_title() !!}</h1>
                    <div
                        id="product-listing-content"
                        class="product-listing-content text-justify"
                    >
                        @php do_action('woocommerce_archive_description') @endphp
                        <button
                            class="product-listing-content__more"
                            type="button"
                        >
                            Read More
                        </button>
                    </div>
                @endif


            </div>
        </div>

    </div>
</header>

<div class="container psy-1 product-listing-container">
    <div class="row">
        <div class="col-12  ">

            {{-- Product loop --}}
            <div
                id="products-container"
                class="products-container"
            >
                @include('partials.woo.product-loop')
            </div>

            @include('partials.woo.archive-additional')
        </div>
    </div>
</div>

<footer>
    <div class="container">

        @php
            do_action('woocommerce_after_main_content');
        @endphp

    </div>
</footer>
