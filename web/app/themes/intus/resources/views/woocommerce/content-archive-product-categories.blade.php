@php
    defined( 'ABSPATH' ) || exit;
    $term = get_queried_object();
    $title = get_the_archive_title();
    $content = apply_filters('the_content', get_the_archive_description());
    $image = ($banner = get_field('banner_image', $term)) ? $banner : false;
    $mobile_image = ($banner_mob = get_field('banner_image_mobile', $term)) ? $banner_mob : false;
@endphp

<div class="bg-lightest-grey">
    <header>

        @if (!empty($image))
            {{-- Header with image --}}
            @include('blocks.banner', [
                'static' => 'true',
                'fields' => [
                    'slides' => [
                        [
                            'title' => $title,
                            'title_colour' => 'white',
                            'icon_type' => 'pink',
                            'content' => $content,
                            'image' => $image,
                            'mobile_image' => $mobile_image,
                        ]
                    ]
                ]
            ])
        @else
            {{-- Header without image --}}
            <div class="container pst-1">
                <div class="row justify-content-center justify-content-lg-start">
                    <div class="col-12 col-md-10 col-xl-8">
                        <h1 class="intus-icon-title color-secondary">
                            @include('partials.intus-icon', [
                                'class' => 'icon-lg',
                                'type' => 'pink'
                            ])
                            <span>{!! $title !!}</span>
                        </h1>

                        {!! $content !!}
                    </div>
                </div>
            </div>
        @endif

        @php
            do_action('woocommerce_before_main_content');
        @endphp

    </header>
    <div class="container psy-1">

        {{-- Products --}}
        <div class="row">
            <div class="col-12">
                <div
                    id="products-container"
                    class="products-container products-container--categories"
                >
                    @include('partials.woo.product-loop')
                </div>

                @include('partials.woo.archive-additional')

            </div>
        </div>

    </div>

    <footer>
        <div class="container">

            {{-- Footer --}}
            @php
                do_action('woocommerce_after_main_content');
            @endphp

        </div>
    </footer>
</div>
