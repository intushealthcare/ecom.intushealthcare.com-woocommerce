@php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$order_val = (isset($_GET['order'])) ? $_GET['order'] : '';
$orderby_val = (isset($_GET['orderby'])) ? $_GET['orderby'] : '';
$active_order = ($order_val == 'DESC' && $orderby_val == 'price') ? $orderby_val . '-desc' : $orderby_val;

@endphp

<form class="woocommerce-ordering mb-4 mb-lg-0" method="get">
    <div class="woocommerce-ordering__icon">
        <i class="fal fa-sort-amount-down-alt d-inline-block d-lg-none"></i>
        <i class="fal fa-angle-down d-none d-lg-inline-block"></i>
    </div>

	<select name="orderby" class="orderby">
		@foreach ($catalog_orderby_options as $id => $name)
			<option
                value="{{ esc_attr( $id ) }}"
                @php selected( $active_order, $id ) @endphp
            >
                {!! esc_html( $name ) !!}
            </option>
		@endforeach
	</select>
</form>
