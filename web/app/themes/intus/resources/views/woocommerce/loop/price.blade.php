@php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$price_html = $product->get_price_html()

@endphp

@if ($price_html)
	@if ($product->is_type('variable'))
		@php
			$min_price = App\woo_get_variation_from_price($product);
		@endphp
		{!! '<span class="price">' . wc_get_price_html_from_text() . '<span class="currencySymbol">' . get_woocommerce_currency_symbol() . '</span>' . $min_price . '</span>' !!}
	@else
		<span class="price">{!! $price_html !!}</span>
	@endif
@endif
