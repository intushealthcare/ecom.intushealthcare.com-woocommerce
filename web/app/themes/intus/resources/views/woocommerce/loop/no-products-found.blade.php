@php
/**
 * Displayed when no products are found matching the current query
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/no-products-found.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.0.0
 */

defined( 'ABSPATH' ) || exit;

@endphp
@if ( \App\category_has_products() )
<p class="woocommerce-info no-products">
    <span class="pb-5 pb-lg-0">
        <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-2x color-error"></i>
            <i class="fas fa-times fa-stack-1x fa-inverse"></i>
        </span>
    </span>
    <span class="pl-0 pl-lg-5 text-normal">
        <span class="h3 d-block">{!! __('Sorry, we couldn\'t find any results matching your search', 'woocommerce') !!}</span>
        <span class="d-block">{!!  __('Please first check your spelling in case a simple typo has stopped us finding what you need. If that doesn\'t help, then please <a href="/contact  ">Contact Us</a> and we will be happy to help you find the item or information you need.', 'woocommerce') !!}</span>
    </span>
</p>
@endif
