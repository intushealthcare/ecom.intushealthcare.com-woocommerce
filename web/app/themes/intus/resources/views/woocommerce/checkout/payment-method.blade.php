@php
/**
 * Output a single payment method
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment-method.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
@endphp

<li class="wc_payment_method payment_method_{{ $gateway->id }}">
    <div class="d-flex align-items-center mb-3">
    	<input
            id="payment_method_{{ $gateway->id }}"
            type="radio"
            class="input-radio"
            name="payment_method"
            value="{{ esc_attr( $gateway->id ) }}"
            data-order_button_text="{{ esc_attr( $gateway->order_button_text ) }}"
            @php checked( $gateway->chosen, true ) @endphp
        />

    	<label for="payment_method_{{ $gateway->id }}">
    		<span>{!! $gateway->get_title() !!}</span>
    	</label>
    </div>
    <div class="payment_method_icon">
        {!! $gateway->get_icon() !!}
    </div>
	@if ($gateway->has_fields() || $gateway->get_description())
		<div
            class="payment_box payment_method_{{ $gateway->id }}"
            style="{{ (!$gateway->chosen) ? 'display:none;' : '' }}"
        >
			{!! $gateway->payment_fields() !!}
		</div>
	@endif
</li>
