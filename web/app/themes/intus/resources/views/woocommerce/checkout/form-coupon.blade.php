@php
/**
 * Checkout coupon form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-coupon.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

if ( ! wc_coupons_enabled() ) { // @codingStandardsIgnoreLine.
	return;
}

@endphp

<div
    class="modal fade"
    id="modal-coupon"
    tabindex="-1"
    role="dialog"
    aria-hidden="true"
>
    <div
        class="modal-dialog"
        role="document"
    >
    <button
        type="button"
        class="close"
        data-dismiss="modal"
        aria-label="Close"
    >
        <span aria-hidden="true">
            <i class="fal fa-2x fa-times"></i>
        </span>
    </button>
        <div class="modal-content">
            <div class="modal-body">
                <div class="p-5 text-center">
                    <form class="woocommerce-form-coupon" method="post">

                        <p class="form-row form-row-first mb-4">
                            <input
                                type="text"
                                name="coupon_code"
                                class="input-text"
                                placeholder="{{ esc_attr_e( 'Coupon code', 'woocommerce' ) }}"
                                id="coupon_code"
                                value=""
                            />
                        </p>

                        <p class="form-row form-row-last mb-0">
                            <button
                                type="submit"
                                class="btn btn--outline"
                                name="apply_coupon"
                                value="{{ esc_attr_e( 'Apply coupon', 'woocommerce' ) }}"
                            >
                                {!! esc_html_e( 'Apply coupon', 'woocommerce' ) !!}
                            </button>
                        </p>

                        <div class="clear"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
