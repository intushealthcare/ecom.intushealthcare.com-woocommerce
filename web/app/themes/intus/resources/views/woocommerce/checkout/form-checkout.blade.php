@php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

@endphp

<div class="container psy-1">

	<h1 class="h2 mt-0 mb-5">{!! __('Checkout', 'woocommerce') !!}</h1>

    @php do_action('woocommerce_checkout_vat_declaration_warning', $checkout ) @endphp

    <form
        name="checkout"
        method="post"
        class="checkout woocommerce-checkout woocommerce-checkout-form"
        action="{{ esc_url(wc_get_checkout_url()) }}"
        enctype="multipart/form-data"
    >

        <div class="row">
            <div class="col-12 col-lg-7 col-xl-7">

                @if ($checkout->get_checkout_fields())

            		@php do_action('woocommerce_checkout_before_customer_details') @endphp

            		<div id="customer_details">
        				@php
                            do_action('woocommerce_checkout_billing');
            				do_action('woocommerce_checkout_shipping');
                        @endphp
            		</div>

            		@php do_action('woocommerce_checkout_after_customer_details') @endphp

            	@endif

            </div>
            <div class="col-12 col-lg-5 col-xl-4 offset-xl-1">

                @php do_action( 'tdr_after_totals_title' ) @endphp

                <h3 id="order_review_heading">{!! _e( 'Your order', 'woocommerce' ) !!}</h3>

            	@php do_action('woocommerce_checkout_before_order_review') @endphp

            	<div
                    id="order_review"
                    class="woocommerce-checkout-review-order"
                >
            		@php do_action('woocommerce_checkout_order_review') @endphp
            	</div>

                @php do_action('woocommerce_checkout_after_order_review') @endphp

            </div>
        </div>

    </form>

    @php do_action('woocommerce_after_checkout_form', $checkout) @endphp
</div>
