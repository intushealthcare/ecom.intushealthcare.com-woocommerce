@php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_cart' );

@endphp

<div class="container psy-1">
	<h1 class="h2 mt-0 mb-5">{!! __('Basket', 'woocommerce') !!}</h1>
    <form class="woocommerce-cart-form" action="{{ esc_url(wc_get_cart_url()) }}" method="post">

        {{-- Before cart --}}
        @php do_action('woocommerce_before_cart_table') @endphp

        <table class="shop-table">

            {{-- Cart table headings  --}}
            <thead>
                <th>{!! __('Product', 'woocommerce') !!}</th>
                <th></th>
                <th>{!! __('Price', 'woocommerce') !!}</th>
                <th>{!! __('Quantity', 'woocommerce') !!}</th>
                <th>{!! __('Total', 'woocommerce') !!}</th>
                <th></th>
            </thead>

            {{-- Cart table body --}}
            <tbody>

                {{-- Before cart contents --}}
                {!! do_action('woocommerce_before_cart_contents') !!}

                {{-- Cart items --}}
                <?php
                foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
    				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

    				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
    					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
    					?>
    					<tr class="shop-table__item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

    						<td class="shop-table__item-thumb"><?php
    						// $thumbnail = apply_filters( 'woocommerce_single', $_product->get_image(), $cart_item, $cart_item_key );
                            $thumbnail = $_product->get_image('medium');
    						if ( ! $product_permalink ) {
    							echo $thumbnail;
    						} else {
    							printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
    						}
    						?></td>

    						<td class="shop-table__item-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>"><?php
    						if ( ! $product_permalink ) {
    							echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
    						} else {
    							echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key );
    						}

    						// Meta data.
    						echo wc_get_formatted_cart_item_data( $cart_item );

							do_action('woo_cart_item_vat_exempt_status', $_product);

    						// Backorder notification.
    						if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
    							echo '<p class="backorder_notification p-sm text-bold">' . esc_html__('Backordered item – allow a few days to dispatch', 'woocommerce') . '</p>';
    						}
    						?></td>

    						<td class="shop-table__item-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
    							<?php
    								echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
    							?>
    						</td>

    						<td class="shop-table__item-quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>"><?php
    						if ( $_product->is_sold_individually() ) {
    							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
    						} else {
    							$product_quantity = woocommerce_quantity_input( array(
    								'input_name'    => "cart[{$cart_item_key}][qty]",
    								'input_value'   => $cart_item['quantity'],
    								'max_value'     => $_product->get_max_purchase_quantity(),
    								'min_value'     => '0',
    								'product_name'  => $_product->get_name(),
    							), $_product, false );
    						}

    						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
    						?></td>

    						<td class="shop-table__item-subtotal" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>">
    							<?php
    								echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
    							?>
    						</td>

                            <td class="shop-table__item-remove">
    							<?php
    								// @codingStandardsIgnoreLine
    								echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
    									'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><i class="far fa-lg fa-times color-off-white"></i></a>',
    									esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
    									__( 'Remove this item', 'woocommerce' ),
    									esc_attr( $product_id ),
    									esc_attr( $_product->get_sku() )
    								), $cart_item_key );
    							?>
    						</td>
    					</tr>
    					<?php
    				}
    			}
                ?>

                {{-- After cart contents --}}
                {!! do_action('woocommerce_after_cart_contents') !!}

            </tbody>
        </table>
        <div class="d-flex flex-column flex-md-row align-items-md-center justify-content-md-between mt-0 mt-md-5">
            <div class="d-none d-lg-inline-block order-2 order-md-1">
                <a href="/continue-shopping" class="btn btn--outline btn--full-mob mb-2">{!! __('Continue Shopping', 'woocommerce') !!}</a>
            </div>
            <div class="order-1 order-md-2 mb-4 mb-md-0">
                <button type="submit" class="btn btn--outline btn--full-mob mb-2" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update Basket', 'woocommerce' ); ?></button>
                @php do_action( 'woocommerce_cart_actions' ) @endphp
                @php wp_nonce_field( 'woocommerce-cart' ) @endphp
            </div>
        </div>
        <div class="row mst-2 justify-content-lg-end">
            <div id="shop-cart-vat-exemption" class="col-12 col-lg-6 col-xl-6 shop-cart-vat-exemption">
                {{-- Coupons --}}
                <?php if ( wc_coupons_enabled() ) { ?>
                <div class="coupon-input mb-5">
                    <label for="coupon_code" class="sr-only"><?php esc_html_e( 'Got a Code?', 'woocommerce' ); ?></label>
                    <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="Coupon code">
                    <input type="submit" class="btn btn--outline btn--full" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>">
                        <?php do_action( 'woocommerce_cart_coupon' ); ?>
                </div>
                <?php } ?>

                {{-- VAT Exemption --}}
                @php do_action('woo_cart_vat_exempt_message') @endphp

            </div>
            <div id="shop-cart-totals" class="col-12 col-lg-6 col-xl-6 shop-cart-totals">

				{{-- Totals --}}
                @php do_action('woocommerce_cart_collaterals') @endphp

            </div>
        </div>


        {{-- After cart --}}
        @php do_action('woocommerce_after_cart_table') @endphp

    </form>
</div>
