@php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

@endphp

@if ( post_password_required() )
    @php
        echo get_the_password_form();
        return;
    @endphp
@endif

<div id="product-@php the_ID() @endphp" @php wc_product_class() @endphp>
    <div class="container-fluid bg-light-grey px-0">
        <div class="container pt-5 psb-2">

            @php
                /**
                 * Hook: woocommerce_before_single_product.
                 *
                 * @hooked wc_print_notices - 10
                 */
                do_action( 'woocommerce_before_single_product' );
            @endphp

            <div class="row justify-content-between">
                <div class="product-single-col-gallery col-12 col-lg-6">

                    @php
                		/**
                		 * Hook: woocommerce_before_single_product_summary.
                		 *
                		 * @hooked woocommerce_show_product_sale_flash - 10
                		 * @hooked woocommerce_show_product_images - 20
                		 */
                		do_action( 'woocommerce_before_single_product_summary' );
                	@endphp

                </div>
                <div class="product-single-col-details col-12 col-lg-6 mt-5 mt-lg-0">
                    <div class="product-single-main">
                        @php
                			/**
                			 * Hook: woocommerce_single_product_summary.
                			 *
                			 * @hooked woocommerce_template_single_title - 5
                             * @hooked \App\trustpilot_get_widget_html - 1
                			 * @hooked woocommerce_template_single_rating - 8
                			 * @hooked woocommerce_template_single_price - 10
                			 * @hooked woocommerce_template_single_excerpt - 20
                			 * @hooked woocommerce_template_single_add_to_cart - 30
                			 * @hooked woocommerce_template_single_meta - 40
                			 * @hooked woocommerce_template_single_sharing - 50
                			 * @hooked WC_Structured_Data::generate_product_data() - 60
                			 */
                			do_action( 'woocommerce_single_product_summary' );
                		@endphp
                    </div>
                </div>
            </div>

        </div>
    </div>

	@php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	@endphp
</div>

@php do_action( 'woocommerce_after_single_product' ) @endphp
