@php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

@endphp

@if (!empty($tabs))
    <div class="container-fluid bg-white px-0">
        <div class="container tabs-container">
            <ul
                class="nav nav-tabs"
                id="single-product-tabs"
                role="tablist"
            >
                @foreach ($tabs as $key => $tab)
                    <li class="nav-item">
                        <a
                            class="nav-link{{ ($loop->index == 0) ? ' active' : '' }}"
                            id="nav-tab-{{ $key }}"
                            data-toggle="tab"
                            href="#tab-{{ $key }}"
                            role="tab"
                            aria-controls="tab-{{ $key }}"
                            aria-selected="true"
                        >
                            {!! $tab['title'] !!}
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content" id="single-product-tabs-content">
                @foreach ($tabs as $key => $tab)
                    <div
                        class="tab-pane fade{{ ($loop->index == 0) ? ' show active' : '' }}"
                        id="tab-{{ $key }}"
                        role="tabpanel"
                        aria-labelledby="nav-tab-{{ $key }}"
                    >
                        <div class="card">
                            <div
                                class="card-header"
                                id="collapse-heading-{{ $key }}"
                            >
                                <h5 class="mb-0">
                                    <button
                                        class="collapsed"
                                        data-toggle="collapse"
                                        data-target="#collapse-content-{{ $key }}"
                                        aria-expanded="false"
                                        aria-controls="collapse-content-{{ $key }}"
                                    >
                                        {!! $tab['title'] !!}
                                    </button>
                                </h5>
                            </div>
                            <div
                                id="collapse-content-{{ $key }}"
                                class="collapse"
                                aria-labelledby="collapse-heading-{{ $key }}"
                                data-parent="#single-product-tabs-content"
                            >
                                <div class="card-body">
                                    @if (isset($tab['callback']))
                                         {!! call_user_func( $tab['callback'], $key, $tab ) !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
