@php

/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.2
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$feat_image_id = $product->get_image_id();
$gall_image_ids = $product->get_gallery_image_ids();

// Product Video Details
$video_id = get_field('product_youtube_id');
$video_source = get_field('product_video_source') ?? 'youtube';
$video_source = ( !empty( $video_source ) ) ? $video_source : 'youtube';
$video_position = get_field('product_video_position') ?? 2;
$video_position = (int) $video_position;
$video_thumbnail = get_field('product_youtube_thumbnail');
$video_thumbnail_medium = ($video_thumbnail ) ? $video_thumbnail['sizes']['medium'] : App\asset_path('images/product-video-thumbnail.png');
if (!empty($feat_image_id) || !empty($gall_image_ids)) {
    array_unshift($gall_image_ids, (int) $feat_image_id);
}

@endphp
@if (!empty($gall_image_ids))
    {{-- Product Gallery --}}
    <div class="product-gallery">

        {{-- Main Slider --}}
        <div class="product-gallery__slider product-gallery__slider--main">
            @php $i = 1; @endphp
            @foreach ($gall_image_ids as $id)

                @if ( $video_id && ( $video_position === $i ) )
                    <div class="product-gallery__slider-item product-gallery__slider-item-video">
                        <div>
                            <div class="product-gallery__slider-item-image">
                                <div class="product-gallery-video <?= $video_source ?>">
                                    <div class="embed-responsive embed-responsive-16by9 width-100">
                                        <?php if ( $video_source == 'youtube' ) : ?>
                                        <iframe
                                            loading="lazy"
                                            class="product-video product-video__<?= $video_source ?>"
                                            data-src="https://www.youtube.com/embed/<?= $video_id ?>?enablejsapi=1&version=3&playerapiid=ytplayer"
                                            src=""
                                            modestbranding="0"
                                            frameborder="0"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                                        </iframe>
                                        <?php else : ?>
                                            <?php echo wp_oembed_get( "https://vimeo.com/{$video_id}", array( 'width' => 400 ) ) ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @php $image = wp_get_attachment_image_src($id, 'large') @endphp
                <div class="product-gallery__slider-item">
                    <div>
                        @if (!empty($image))
                            <div
                                class="product-gallery__slider-item-image"
                                style="background-image: url({{ $image[0] }});"
                            >
                            </div>
                            <div
                                class="product-gallery__slider-item-enlarge"
                                data-image="{{ $image[0] }}"
                            >
                                <i class="far fa-lg fa-search-plus"></i>
                            </div>
                        @endif
                    </div>
                </div>

                @php $i++; @endphp
            @endforeach
        </div>

        @if (count($gall_image_ids) > 1)
            {{-- Nav Slider --}}
            <div class="product-gallery__slider product-gallery__slider--nav">
                @php $i = 1; @endphp
                @foreach ($gall_image_ids as $id)

                    @if ( $video_id && ( $video_position === $i ) )
                        <div class="product-gallery__slider-item product-gallery__slider-item-video">
                            <div>
                                @if (!empty($video_thumbnail_medium))
                                    <div
                                        class="product-gallery__slider-item-image"
                                        style="background-image: url({{ $video_thumbnail_medium }});"
                                    >
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endif

                    @php $image = wp_get_attachment_image_src($id, 'medium') @endphp
                    <div class="product-gallery__slider-item">
                        <div>
                            @if (!empty($image))
                                <div
                                    class="product-gallery__slider-item-image"
                                    style="background-image: url({{ $image[0] }});"
                                >
                                </div>
                            @endif
                        </div>
                    </div>
                   @php $i++; @endphp
                @endforeach
            </div>
        @endif

    </div>

    <div
        class="modal modal--image fade"
        id="modal-enlarge"
        tabindex="-1"
        role="dialog"
        aria-hidden="true"
    >
        <div
            class="modal-dialog modal-xl"
            role="document"
        >
        <button
            type="button"
            class="close"
            data-dismiss="modal"
            aria-label="Close"
        >
            <span aria-hidden="true">
                <i class="fal fa-2x fa-times"></i>
            </span>
        </button>
            <div class="modal-content">
                <div class="modal-body">
                    <img
                        id="modal-enlarge-image"
                        src=""
                        class="img-fluid m-auto"
                    >
                </div>
            </div>
        </div>
    </div>
@endif
