<div class="container-fluid product-back-to-top-wrap text-center">
    <a href="#main" class="product-back-to-top-button">
        <i class="fas fa-3x fa-chevron-circle-up"></i><br/>
        Back to top
    </a>
</div>
