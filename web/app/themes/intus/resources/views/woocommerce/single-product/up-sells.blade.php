@php
/**
 * Single Product Up-Sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/up-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
@endphp

@if ( $upsells )
	<section class="container-fluid px-0 pb-5 bg-secondary-light up-sells upsells products related-products-wrap">
        <div class="container py-6">
            <h2 class="h3 color-secondary text-normal mb-5">{!! esc_html_e( 'You may also like&hellip;', 'woocommerce' ) !!}</h2>
            <ul class="related-products">
                @foreach ( $upsells as $upsell )
                    @php
                        $post_object = get_post( $upsell->get_id() );
                        setup_postdata( $GLOBALS['post'] =& $post_object );
                        wc_get_template_part( 'content', 'product' );
                    @endphp
                @endforeach
            </ul>
        </div>
	</section>
@endif

@php
wp_reset_postdata()
@endphp
