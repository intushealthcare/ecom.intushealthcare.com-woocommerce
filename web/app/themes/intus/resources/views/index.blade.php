@extends('layouts.app')

@section('content')
    @if (!have_posts())
        @include('pages.content-search')
    @else
        @while(have_posts()) @php the_post() @endphp
            @include('pages.content-single-'.get_post_type())
        @endwhile
    @endif
@endsection
