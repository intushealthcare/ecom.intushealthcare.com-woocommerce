@if($magento_order_id && $magneto_order_status === 'success')
    <p style="color: green;"><strong><span class="dashicons dashicons-yes"></span> Synchronised</strong></p>
    <p style="margin-bottom: 0;"><strong>Magento Order ID:</strong> {{ $magento_order_id }}</p>
@elseif ($magneto_order_status === 'processing')
    <p style="margin-bottom: 0;"><strong><span class="spinner is-active"></span> Synchronisation currently in progress...</strong></p>
@else
    @if($magneto_order_status === 'failed')
        <p style="color: red;"><strong><span class="dashicons dashicons-no-alt"></span> Synchronisation Failed</strong></p>
    @endif
    <button class="button button-show-form js-magento-sync-manually" type="button">Sync Order Manually</button>
@endif

<script>
    (function($) {
        $('.js-magento-sync-manually').on('click', function (e) {
            e.preventDefault();

            var self = this;

            $(self).prop('disabled', true);

            $.ajax({
                url: ajaxurl,
                type: 'post',
                dataType: 'json',
                data: {
                    action: 'intus_order_sync_manual',
                    security: '{!! wp_create_nonce('intus_order_sync_manual') !!}',
                    id: {!! $wc_order_id !!}
                },
                success: function (response) {
                    try {
                        window.location.reload(true);
                    } catch (e) {
                        alert('Manual syncronisation of Magento order has started, please refresh the page to see progress.')
                    }
                },
                error: function (response) {
                    $(self).prop('disabled', false);
                    alert('An error occured while attempting to synchronise the order with Magento manually.')
                }
            })
        });
    })(jQuery);
</script>
