{{--
Template Name: FAQs Template
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts())
        @php the_post() @endphp
        @include('pages.content-faqs')
    @endwhile
@endsection
