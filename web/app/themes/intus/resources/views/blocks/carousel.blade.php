{{--
  Title: Carousel
  Description: Columns carousel that can contain images, key points or steps
  Category: flexible-content
  Icon: format-image
  Keywords: carousel image key points steps
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    $options = $options ?? TemplateFlexible::options();
    $container_width = TemplateFlexible::width_styles();
    $backgrounds = TemplateFlexible::backgrounds();

    $fields = $fields ?? TemplateFlexible::carousel();
    if( empty($fields['slides']) ){
        return;
    }
	$slidesCount = count($fields['slides']);
    $style = $fields['style'];
    $slidesScroll = (!empty($fields['slides_to_scroll'])) ? $fields['slides_to_scroll'] : 4;
    // Available styles: image, image-content, steps, circles
@endphp

<div class="flex-row carousel bg-{{ $fields['bg_colour'] }} {{ $options }}" {!! $backgrounds !!}>
    <div class="{!! $container_width !!}">

        {{-- Top content --}}
        @if (isset($fields['add_content']) && $fields['add_content'] && !empty($fields['content']))
            <div class="iv-wp fade-up">
                <div class="row justify-content-lg-center mb-5">
                    <div class="col-12 col-lg-10 col-xl-9 text-center">
                        {!! $fields['content'] !!}
                    </div>
                </div>
            </div>
        @endif

        {{-- Slides --}}
        @if (!empty($fields['slides']))
            <div
                class="slider-carousel{{ ($slidesCount > $slidesScroll) ? ' slick-arrowed' : '' }}"
                data-slides-to-scroll="{{ $slidesScroll }}"
            >
                @foreach ($fields['slides'] as $slide)
                    <div class="slider-carousel__item">

                        {{-- Image --}}
                        @if ($style == 'image' || $style == 'image-content' && !empty($slide['image']))
                           <div class="slider-carousel__item-image">
                               @if ( !empty( $slide['link'] ) )
                                   <a href="{{ $slide['link']['url'] }}" title="{{ $slide['link']['title'] }}">
                               @endif
                               <img
                                   src="{{ $slide['image']['sizes']['medium'] }}"
                                   loading="lazy"
                                   alt="{{ $slide['image']['alt'] }}"
                                   height="{{ $slide['image']['sizes']['medium-height'] }}"
                                   width="{{ $slide['image']['sizes']['medium-width'] }}"
                               >
                               @if ( !empty( $slide['link'] ) )
                                   </a>
                               @endif
                           </div>
                       @endif

                       {{-- Counter --}}
                       @if ($style == 'steps')
                           <p class="h2 slider-carousel__item-step">{{ $loop->iteration }}</p>
                       @endif

                       {{-- Circle wrap --}}
                       @if ($style == 'circles')
                           <div class="slider-carousel__item-circle">
                       @endif

                       {{-- Large title --}}
                       @if ($style == 'steps' && !empty($slide['large_title']))
                           <div class="slider-carousel__item-content">
                               <h4>{!! $slide['large_title'] !!}</h4>
                           </div>
                       @endif

                       {{-- Small title --}}
                       @if ($style !== 'image' && !empty($slide['small_title']))
                           <div class="slider-carousel__item-content">
                               <p class="text-bold">{!! $slide['small_title'] !!}</p>
                           </div>
                       @endif

                       {{-- Circle wrap --}}
                       @if ($style == 'circles')
                           </div>
                       @endif

                   </div>
               @endforeach
           </div>
       @endif

   </div>
</div>
