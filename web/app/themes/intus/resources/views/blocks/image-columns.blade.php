{{--
  Title: Image Columns
  Description: Image columns
  Category: flexible-content
  Icon: format-image
  Keywords: image column
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    $options = TemplateFlexible::options();
    $fields = TemplateFlexible::imageColumns();
@endphp

<div class="flex-row image-columns {{ $options }}">
    <div class="container">
        <div class="row">

            {{-- Images --}}
            @foreach ($fields['images'] as $image_item)
                <div class="col-12 col-md-6 mb-5 mb-lg-0 d-lg-flex align-items-lg-center">
                    @if (!empty($image_item['image']))
                        <div class="image-columns__item">
                            <div style="background-image: url({{ $image_item['image']['sizes']['large'] }});"></div>
                        </div>
                    @endif
                </div>
            @endforeach
            
        </div>
    </div>
</div>
