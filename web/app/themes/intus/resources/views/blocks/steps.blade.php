{{--
  Title: Steps
  Description: Step by step content marked by a number
  Category: flexible-content
  Icon: format-image
  Keywords: steps content
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    $options = TemplateFlexible::options();
    $fields = TemplateFlexible::steps();
@endphp

<div class="flex-row steps {{ $options }}">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-10 col-lg-8 col-xl-7">

                {{-- Title --}}
                @if (!empty($fields['title']))
                    <div class="msb-3 text-center iv-wp fade-up">
                        <h2 class="intus-icon-title text-lighter">
                            @if (!empty($fields['icon']))
                                @include('partials.intus-icon', [
                                    'type' => 'pink',
                                    'class' => 'icon-lg',
                                ])
                            @endif
                            <span>{!! $fields['title'] !!}</span>
                        </h2>
                    </div>
                @endif

            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-10 col-lg-9 col-xl-8">



                {{-- Steps --}}
                @if (!empty($fields['steps']))
                    <ul class="steps__list list-clear-all">
                        @foreach ($fields['steps'] as $step)
                            <li class="steps__list-item">
                                <div>

                                    {{-- Number --}}
                                    <p class="steps__list-item-number">
                                        0{{ $loop->iteration }}
                                    </p>

                                </div>
                                <div class="text-center text-md-left iv-wp fade-up">

                                    {{-- Step title --}}
                                    @if (!empty($step['title']))
                                        <h3 class="h1">{!! $step['title'] !!}</h3>
                                    @endif

                                    {{-- Step sub title --}}
                                    @if (!empty($step['sub_title']))
                                        <p class="h4">{!! $step['sub_title'] !!}</p>
                                    @endif

                                    {{-- Step content --}}
                                    @if (!empty($step['content']))
                                        {!! $step['content'] !!}
                                    @endif

                                </div>
                            </li>
                        @endforeach
                    </ul>
                @endif

            </div>
        </div>
    </div>
</div>
