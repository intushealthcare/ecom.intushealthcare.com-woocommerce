{{--
  Title: Featured Product
  Description: featured product
  Category: flexible-content
  Icon: format-image
  Keywords: featured product
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php $fields = TemplateFlexible::featuredProduct() @endphp

@if (!empty($fields['product']))
    @php
        $product = wc_get_product($fields['product']->ID);
        $feat_image = ($product->get_image_id()) ? [(int) $product->get_image_id()] : [];
        $gallery = ($product->get_gallery_image_ids()) ? $product->get_gallery_image_ids() : [];
        $images = array_merge($feat_image, $gallery);
    @endphp
    <div class="flex-row featured-product bg-light-grey">
        <div class="container psy-1">
            <div class="row">
                <div class="col-12 col-lg-6">

                    @if (!empty($images))
                        <div class="slider-featured-product">
                            @foreach ($images as $image_id)
                                @php $image_src = wp_get_attachment_image_src($image_id, 'large') @endphp
                                <div class="slider-featured-product__item">
                                    @if (!empty($image_src))
                                        <div class="slider-featured-product__item-image">
                                            <div style="background-image: url({{ $image_src[0] }});"></div>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    @endif

                </div>
                <div class="col-12 col-lg-6 d-lg-flex align-items-center">
                    <div class="px-2 px-lg-5 pt-5 pt-lg-0 iv-wp fade-up">

                        <h3>{!! $product->get_title() !!}</h3>
                        <p>{!! $product->get_short_description() !!}</p>
                        <p class="h5 my-5">{!!  \App\woo_get_intus_product_price($product) !!}</p>
                        <div>
                            <a
                                href="{{ $product->get_permalink() }}"
                                class="btn"
                            >
                                {!! $fields['btn_text'] !!}
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
