{{--
  Title: Key Point Header
  Description: Header block with content and a key point circle
  Category: flexible-content
  Icon: format-image
  Keywords: header content key point circle
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: false
--}}

@php
    $options = TemplateFlexible::options();
    $fields = TemplateFlexible::keyPointHeader();
@endphp

<div class="flex-row key-point-header {{ $options }}">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4 offset-lg-1 order-lg-2 d-lg-flex align-items-lg-center">

                {{-- Circle with key point --}}
                @if (!empty($fields['key_title']) || !empty($fields['key_desc']))
                    <div class="key-point-header__circle">

                        @if (!empty($fields['key_title']))
                            <p class="h3 text-bold mb-3">{!! $fields['key_title'] !!}</p>
                        @endif

                        @if (!empty($fields['key_desc']))
                            <p class="mb-0">{!! $fields['key_desc'] !!}</p>
                        @endif

                    </div>
                @endif

            </div>
            <div class="col-12 col-lg-7 order-lg-1">

                {{-- Title --}}
                @if (!empty($fields['title']))
                    <h1 class="intus-icon-title mb-5">
                        @if (!empty($fields['icon']))
                            @include('partials.intus-icon', [
                                'type' => 'pink',
                                'class' => 'icon-lg',
                            ])
                        @endif
                        <span>{!! $fields['title'] !!}</span>
                    </h1>
                @endif

                {{-- Content --}}
                @if (!empty($fields['content']))
                    {!! $fields['content'] !!}
                @endif

            </div>
        </div>
    </div>
</div>
