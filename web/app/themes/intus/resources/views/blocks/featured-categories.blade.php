{{--
  Title: Featured Categories
  Description: Featured categories on a slider with accompanying content
  Category: flexible-content
  Icon: format-image
  Keywords: featured categories slider
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    $options = TemplateFlexible::options();
    $fields = TemplateFlexible::featuredCategories();
@endphp

<div class="flex-row featured-categories {{ $fields['style'] }} {{ $options }}">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4 mb-5 mb-lg-0 d-flex flex-column justify-content-center">

                {{-- Title --}}
                @if (!empty($fields['title']))
                    <h2 class="text-lighter mb-5 iv-wp fade-up">{!! $fields['title'] !!}</h2>
                @endif

                {{-- Content --}}
                @if (!empty($fields['content']))
                    <div class="iv-wp fade-up">
                        {!! $fields['content'] !!}
                    </div>
                @endif

            </div>
            <div class="col-12 col-lg-8 col-xl-7 offset-xl-1">

                {{-- Categories slider --}}
                @if (!empty($fields['categories']))
                    <div class="slider-featured">
                        @foreach ($fields['categories'] as $category)
                            @php
                                $image = wp_get_attachment_image_src(get_term_meta($category->term_id, 'thumbnail_id', true), 'medium');
                                $tagline = get_field('tagline', $category);
                            @endphp

                            <div class="slider-featured__item">
                                <a
                                    class="color-secondary d-block"
                                    href="{{ get_term_link($category->term_id) }}"
                                >
                                    <h3 class="slider-featured__item-title">{{ $category->name }}</h3>
                                    <div class="slider-featured__item-inner">

                                        {{-- Image --}}
                                        @if (!empty($image))
                                            <div
                                                class="slider-featured__item-image"
                                                style="background-image: url({{ $image[0] }});"
                                            >
                                            </div>
                                        @endif

                                    </div>
                                </a>
                            </div>

                        @endforeach
                    </div>

                @endif

            </div>
        </div>
        <div class="slider-featured-nav"></div>
    </div>
</div>
