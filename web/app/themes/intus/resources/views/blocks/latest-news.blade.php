{{--
  Title: Latest News
  Description: Block that automatically pulls in the latest three posts
  Category: flexible-content
  Icon: format-image
  Keywords: latest posts news blog
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    $latest_news = TemplateFlexible::latestNews();
@endphp

@if ($latest_news->have_posts())
    <div class="flex-row latest-news psy-1">
        <div class="container">
            <div class="row">

                {{-- Title --}}
                <div class="col-12 col-lg-3">
                    <h2 class="text-lighter iv-wp fade-up">{!! __('Our Blog & News', 'sage') !!}</h2>
                </div>

                {{-- Posts --}}
                <div class="col-12 col-lg-8 offset-lg-1">
                    <ul class="latest-news__posts">
                        @while ($latest_news->have_posts())
                            @php
                                $latest_news->the_post();
                                $image = App::wpImageSizes();
                            @endphp
                            <li class="latest-news__posts-item">
                                <div class="row">
                                    <div class="col-3 col-md-2">

                                        {{-- Image --}}
                                        @if (!empty($image))
                                            <a href="{{ get_permalink() }}">
                                                <img
                                                    src="{{ $image['sm'] }}"
                                                    alt="{{ $image['alt'] }}"
                                                    class="img-fluid"
                                                >
                                            </a>
                                        @endif

                                    </div>
                                    <div class="col-9 col-md-10">
                                        <div class="d-flex align-items-center justify-content-between h-100">
                                            <div>

                                                {{-- Date --}}
                                                <time
                                                    class="d-block pb-2 text-bold"
                                                    datetime="{{ get_the_date('c') }}"
                                                >
                                                    {!! get_the_date('M d') !!}
                                                </time>

                                                {{-- Post title --}}
                                                {!! the_title('<h3 class="text-normal mb-0 h4">', '</h3>') !!}

                                            </div>
                                            <div class="h-100 align-items-end d-none d-md-flex">

                                                {{-- Link --}}
                                                <a
                                                    href="{{ get_permalink() }}"
                                                    class="latest-news__posts-item-link"
                                                >
                                                    {!! __('Read More', 'sage') !!}
                                                </a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endwhile
                    </ul>
                    @php wp_reset_postdata() @endphp
                </div>
            </div>
        </div>
    </div>
@endif
