{{--
  Title: Category Columns
  Description: Two column category layout
  Category: flexible-content
  Icon: format-image
  Keywords: column image content category
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    $options = TemplateFlexible::options();
    $backgrounds = TemplateFlexible::backgrounds();
    $fields = TemplateFlexible::categoryColumns();
@endphp

@if (!empty($fields['columns']))
    <div class="flex-row category-columns {{ $options }}" {!! $backgrounds !!}>
        <div class="container">
            <div class="row">
                @foreach ($fields['columns'] as $column)
                    <div class="col-12 col-lg-6 mb-5 mb-lg-0">
                        <div class="category-columns__item">
                            <div class="category-columns__item-inner">
                                <div class="mb-auto">

                                    {{-- Title --}}
                                    @if (!empty($column['title']))
                                        <div class="iv-wp fade-up category-columns__item-title">
                                            <h2>{!! $column['title'] !!}</h2>
                                        </div>
                                    @endif

                                </div>
                                <div class="mt-auto">

                                    {{-- Button --}}
                                    @if (!empty($column['button']))
                                        @include('partials.button', [
                                            'button' => $column['button'],
                                            'class' => 'btn--outline'
                                        ])
                                    @endif

                                </div>
                            </div>

                            {{-- Background Image --}}
                            @if (!empty($column['image']))
                                <div
                                    class="category-columns__item-image"
                                    style="background-image: url({{ $column['image']['sizes']['large'] }});"
                                >
                                </div>
                            @endif

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
