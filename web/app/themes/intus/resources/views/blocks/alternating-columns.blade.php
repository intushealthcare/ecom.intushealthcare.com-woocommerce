{{--
  Title: Alternating Columns
  Description: Image and Content columns that can be alternated left and right
  Category: flexible-content
  Icon: format-image
  Keywords: column image content alternating
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    $options = TemplateFlexible::options();
    $container_width = TemplateFlexible::width_styles();
    $backgrounds = TemplateFlexible::backgrounds();
    $fields = TemplateFlexible::alternating();
@endphp

<div class="flex-row alternating-columns {{ $options }}" {!! $backgrounds !!}>
    <div class="{{ $container_width  }}">
        <div class="row">
            <div class="col-12 col-lg-6{{ ($fields['flip']) ? ' order-lg-2' : '' }}">

                @if ($fields['image_type'] == 'slides')
                    {{-- Slides --}}
                    <div class="slider-alternating">
                        @foreach ($fields['image_slides'] as $slide)
                            <div class="slider-alternating__item">
                                @if (!empty($slide['image']))
                                    <div class="slider-alternating__item-image">
                                        <div
                                            style="background-image: url({{ $slide['image']['sizes']['large'] }});"></div>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>
                @else
                    {{-- Image --}}
                    @if (!empty($fields['image']))
                        <img
                            src="{{ $fields['image']['sizes']['large'] }}"
                            loading="lazy"
                            alt="{{ $fields['image']['alt'] }}"
                            height="{{ $fields['image']['sizes']['large-height'] }}"
                            width="{{ $fields['image']['sizes']['large-width'] }}"
                            class="w-100 img-fluid"
                        >
                    @endif
                @endif

            </div>
            <div class="col-12 col-lg-6{{ ($fields['flip']) ? ' order-lg-1' : '' }}">
                <div
                    class="alternating-columns__content pst-2 px-lg-5 {{ ($fields['flip']) ? 'alternating-columns__content--left pr-lg-5 pl-lg-0' : 'alternating-columns__content--right pl-lg-5 pr-lg-0' }}">

                    {{-- Title --}}
                    @if (!empty($fields['title']))
                        <div class="iv-wp fade-up">
                            <h3 class="intus-icon-title alternating-columns__title h2 mb-5 {{ ($fields['bold_title']) ? 'text-bold' : 'text-lighter' }}">
                                @if ($fields['icon'])
                                    @include('partials.intus-icon')
                                @endif
                                <span>{!! $fields['title'] !!}</span>
                            </h3>
                        </div>
                    @endif

                    {{-- Content --}}
                    @if (!empty($fields['content']))
                        <div class="iv-wp fade-up content">
                            {!! $fields['content'] !!}
                        </div>
                    @endif

                    @if ($fields['add_extra'])
                        <div class="iv-wp fade-up">
                            <div
                                class="alternating-columns__lists alternating-columns__lists--{{ $fields['content_type'] }}">

                                @if ($fields['content_type'] == 'double-list')
                                    <div class="row ">
                                        <div class="col-12 col-xl-6 mt-4">

                                            @if (!empty($fields['list_title']))
                                                <p class="text-bold">{!! $fields['list_title'] !!}</p>
                                            @endif
                                            @endif

                                            @if (!empty($fields['list']))
                                                <ul>
                                                    @foreach ($fields['list'] as $list_item)
                                                        @if (!empty($list_item['text']))
                                                            <li>
                                                                {!! $list_item['text'] !!}
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @endif

                                            @if ($fields['content_type'] == 'double-list' && !empty($fields['list_two']))
                                        </div>
                                        <div class="col-12 col-xl-6 mt-4">

                                            @if (!empty($fields['list_two_title']))
                                                <p class="text-bold">{!! $fields['list_two_title'] !!}</p>
                                            @endif

                                            <ul>
                                                @foreach ($fields['list_two'] as $list_item)
                                                    @if (!empty($list_item['text']))
                                                        <li>
                                                            {!! $list_item['text'] !!}
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif

                            </div>
                        </div>
                    @endif

                    {{-- Buttons --}}
                    @if (!empty($fields['button']))
                        <div class="iv-wp fade-up mt-5">
                            @include('partials.button', [
                                'button' => $fields['button'],
                                'class' => 'btn--outline btn--full-mob',
                            ])
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
