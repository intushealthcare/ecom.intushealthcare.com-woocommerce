{{--
  Title: Image
  Description: Image
  Category: flexible-content
  Icon: format-image
  Keywords: image
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    $options = TemplateFlexible::options();
    $fields = TemplateFlexible::flexImage();
@endphp

<div class="flex-row image-block {{ $options }}">
    <div class="container py-5">
        <div class="iv-wp fade-up">
            <div class="row justify-content-lg-center">
                <div class="col-12 col-lg-10 col-xl-9">

                    @if (!empty($fields['link']))
                        <a href="{{ $fields['link'] }}">
                    @endif

                    @if (!empty($fields['image']))
                        <img
                            src="{{ $fields['image']['sizes']['large'] }}"
                            loading="lazy"
                            alt="{{ $fields['image']['alt'] }}"
                            height="{{ $fields['image']['sizes']['large-height'] }}"
                            width="{{ $fields['image']['sizes']['large-width'] }}"
                            class="img-fluid w-100"
                        >
                    @endif

                    @if (!empty($fields['link']))
                        </a>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
