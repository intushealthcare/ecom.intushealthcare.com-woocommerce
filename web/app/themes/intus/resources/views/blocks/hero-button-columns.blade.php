{{--
  Title: Hero Image & Button : 2-4 Columns
  Description: Two to four column layout
  Category: flexible-content
  Icon: format-image
  Keywords: column image button content
  Mode: edit
  PostTypes: page post
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    use App\Controllers\TemplateFlexible;$options = TemplateFlexible::options();
    $container_width = TemplateFlexible::width_styles();
    $backgrounds = TemplateFlexible::backgrounds();
    $fields = TemplateFlexible::heroimagebuttonColumns();
@endphp

@if (!empty($fields['columns']))
    <div class="flex-row heroimagebutton-columns {{ $options }}" {!! $backgrounds !!}>
        <div class="{{ $container_width  }}">
            <div class="row">
                @foreach ($fields['columns'] as $column)
                    <div class="d-flex {{ $fields['css_class'] }}">
                        <div class="heroimagebutton-columns__item bg-light-grey {!! TemplateFlexible::border_radius_styles() !!}">

                            {{-- Background Image --}}
                            @if (!empty($column['image']))
                                <div class="heroimagebutton-columns__item-image">
                                    <img
                                        src="{{ $column['image']['sizes']['large'] }}"
                                        loading="lazy"
                                        alt="{{ $column['image']['alt'] }}"
                                        height="{{ $column['image']['sizes']['large-height'] }}"
                                        width="{{ $column['image']['sizes']['large-width'] }}"
                                        class="heroimagebutton-columns__item-image"
                                    >
                                </div>
                            @endif

                            {{-- Title and text --}}
                            @if (!empty($column['title']))
                                <h2>{{ $column['title']  }}</h2>
                            @endif

                            @if (!empty($column['text']))
                                <div class="heroimagebutton-columns__item-inner content">
                                    <div class="mt-auto">
                                        {!! $column['text'] !!}
                                    </div>
                                </div>
                            @endif

                            <div class="heroimagebutton-columns__item-inner">
                                <div class="mt-auto">
                                    {{-- Button --}}
                                    @if (!empty($column['button']))
                                        @include('partials.button', [
                                            'button' => $column['button'],
                                            'class' => 'btn--outline'
                                        ])
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
