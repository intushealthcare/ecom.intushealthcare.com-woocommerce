{{--
  Title: Full Width Content
  Description: Full width content block with style options
  Category: flexible-content
  Icon: format-image
  Keywords: full width content
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    $options = $options ?? TemplateFlexible::options();
    $backgrounds = TemplateFlexible::backgrounds();
    $fields = $fields ?? TemplateFlexible::fullWidth();
    $titleTag = $fields['title_tag'] ?? 'h2';
    $headingTextAlignment = ( $titleTag != 'h1' ) ? 'text-' . $fields['alignment'] : 'text-center';
@endphp

<div class="flex-row full-width full-width--{{ $fields['style'] }} {{ $options }}" {!! $backgrounds !!}>
    <div class="container {{ !empty( $fields['alignment'] ) ? "text-" . $fields['alignment'] : "text-center" }}  position-relative z-index-2">
        <div class="row justify-content-lg-center">
            <div class="col-12 col-lg-11 col-xl-10">

                {{-- Title --}}
                @if (!empty($fields['title']))
                    <{{ $titleTag }} class="intus-icon-title text-lighter msb-1 iv-wp fade-up {{ $headingTextAlignment }}">
                        @if ($fields['icon'])
                            @include('partials.intus-icon')
                        @endif
                        <span>{!! $fields['title'] !!}</span>
                    </{{ $titleTag }}>
                @endif

                {{-- Content --}}
                @if (!empty($fields['content']))
                    <div class="iv-wp fade-up content">
                        {!! $fields['content'] !!}
                    </div>
                @endif

                {{-- Buttons --}}
                @if (!empty($fields['buttons']))
                    <div class="mt-5 iv-wp fade-up">
                        <ul class="list-buttons justify-content-center">
                            @foreach ($fields['buttons'] as $button_item)
                                <li>
                                    @include('partials.button', [
                                        'button' => $button_item['button'],
                                        'class' => 'btn--white',
                                        'button_outline' => $fields['button_outline'],
                                    ])
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            </div>
        </div>
    </div>

    {{-- Plus icon --}}
    @if ($fields['style'] == 'pink')
        <img
            src="@asset('images/intus-cross-white-lightpink.png')"
            alt="pink cross icon"
            class="full-width__plus"
            width="349"
            height="396"
        >
    @elseif ($fields['style'] == 'blue')
        <img
            src="@asset('images/intus-cross-white-blue.png')"
            alt="blue cross icon"
            class="full-width__plus"
            width="349"
            height="396"
        >
    @endif
</div>
