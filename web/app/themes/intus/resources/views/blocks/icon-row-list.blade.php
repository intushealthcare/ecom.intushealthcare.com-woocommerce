{{--
  Title: Icon Row List
  Description: One row grid of icons with descriptions
  Category: flexible-content
  Icon: format-image
  Keywords: icon row list
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    $options = $options ?? TemplateFlexible::options();
    $fields = $fields ?? TemplateFlexible::iconRowList();
@endphp

<div class="flex-row icon-row-list {{ $options }}">
    <div class="container">
        {{-- Statistics --}}
        @if (!empty($fields))
            <ul class="icon-row-list__list">
                @foreach ($fields as $icon)
                    <li class="icon-row-list__list-item">
                        <div class="icon-row-list__list-item-inner">
                            {{-- Icon --}}
                            <div class="mb-5">
                                <img
                                    src="{{ $icon['file']['url'] }}"
                                    loading="lazy"
                                    alt="{{ $icon['file']['alt'] }}"
                                    width="{{ $icon['file']['width'] }}"
                                    height="{{ $icon['file']['height'] }}"
                                    >
                            </div>

                            {{-- Description --}}
                            <p class="text-bold mb-0">
                                {!! $icon['description'] !!}
                            </p>

                        </div>
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
</div>
