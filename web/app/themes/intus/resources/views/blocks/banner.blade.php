{{--
  Title: Banner
  Description: Large banner that can contain multiple slides
  Category: flexible-content
  Icon: format-image
  Keywords: banner slider carousel
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: false
--}}

@php
    $static = $static ?? false;
    $fields = $fields ?? TemplateFlexible::banner();
@endphp

@if (!empty($fields['slides']))
    <div class="flex-row banner">
        <div class="slider-banner{{ ($static) ? ' slider-banner--static' : '' }}">
            @foreach ($fields['slides'] as $slide)
                <div class="slider-banner__item">

                    <div class="slider-banner__item-content">
                        <div class="container h-100 text-center text-lg-left d-flex flex-column justify-content-md-center">
                            <div class="pt-5 pt-md-0">
                                <div class="row justify-content-center justify-content-lg-start">
                                    <div class="col-12 col-md-10 col-xl-8">

                                        {{-- Title --}}
                                        @if (!empty($slide['title']))
                                            <h2 class="intus-icon-title h1--lg color-{{ $slide['title_colour']}}">
                                                @include('partials.intus-icon', [
                                                    'class' => 'icon-lg',
                                                    'type' => $slide['icon_type']
                                                ])
                                                <span>{!! $slide['title'] !!}</span>
                                            </h2>
                                        @endif

                                    </div>
                                </div>
                                <div class="row justify-content-center justify-content-lg-start">
                                    <div class="col-12 col-md-10 col-xl-8">

                                        {{-- Content --}}
                                        @if (!empty($slide['content']))
                                            {!! $slide['content'] !!}
                                        @endif

                                        {{-- Buttons --}}
                                        @if (!empty($slide['button']))
                                            <div class="slider-banner__item-button">
                                                @include('partials.button', [
                                                    'button' => $slide['button'],
                                                    'class' => 'btn--white',
                                                ])
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Background image --}}
                    @if (!empty($slide['image']))
                        @include('partials.picture', [
                            'image' => $slide['image'],
                            'mobile_image' => (!empty($slide['mobile_image'])) ? $slide['mobile_image'] : false,
                            'class' => 'picture-bg picture-bg--overlay stretch-container',
                        ])
                    @endif

                </div>
            @endforeach
        </div>
    </div>
@endif
