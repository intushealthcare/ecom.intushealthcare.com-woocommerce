{{--
  Title: Flexible Row - Single Column
  Description: Variable width content block with style options
  Category: flexible-content
  Icon: format-image
  Keywords: full width content
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    use App\Controllers\TemplateFlexible;
   $options = TemplateFlexible::options();
   $container_width = TemplateFlexible::width_styles();
   $backgrounds = TemplateFlexible::backgrounds();
   $fields = $fields ?? TemplateFlexible::single_column_row();
   $titleTag = $fields['title_tag'] ?? 'h2';
   $headingTextAlignment = ( $titleTag != 'h1' ) ? 'text-' . $fields['alignment'] : 'text-center';
@endphp

<div class="flex-row full-width full-width--{{ $fields['style'] }} {{ $options }}" {!! $backgrounds !!}>
    <div class="{{ $container_width  }} {{ !empty( $fields['alignment'] ) ? "text-" . $fields['alignment'] : "text-center" }}  position-relative z-index-2">
        <div class="row justify-content-lg-center">
            <div class="d-flex flexible-columns-column {{ $fields['css_class'] }}" >
                <div class="intus-row-column d-flex flex-column {{ $fields['column_classes']  }} {{ ($fields['flip'] === true ) ? 'ml-auto' : 'mr-auto' }} {!! TemplateFlexible::border_radius_styles() !!} {{ $fields['column_background'] ? 'ps-1' : '' }}" {!! $fields['inline_styles'] !!}>

                        {{-- Title --}}
                        @if (!empty($fields['title']))
                            <{{ $titleTag }} class="intus-icon-title text-lighter msb-1 iv-wp fade-up {{ $headingTextAlignment }}">
                            @if ($fields['icon'])
                                @include('partials.intus-icon')
                            @endif
                            <span>{!! $fields['title'] !!}</span>
                    </{{ $titleTag }}>
                    @endif

                    {{-- Content --}}
                    @if (!empty($fields['content']))
                        <div class="iv-wp fade-up content">
                            {!! $fields['content'] !!}
                        </div>
                    @endif

                    {{-- Buttons --}}
                    @if (!empty($fields['buttons']))
                        <div class="mt-5 iv-wp fade-up">
                            <ul class="list-buttons px-0 mx-0">
                                @foreach ($fields['buttons'] as $button_item)
                                    <li>
                                        @include('partials.button', [
                                            'button' => $button_item['button'],
                                            'button_style' => $fields['button_style'],
                                            'button_outline' => $fields['button_outline'],
                                        ])
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>

    {{-- Plus icon --}}
    @if ($fields['style'] == 'pink')
        <img
            src="@asset('images/intus-cross-white-lightpink.png')"
            alt="pink cross icon"
            class="full-width__plus"
            width="349"
            height="396"
        >
    @elseif ($fields['style'] == 'blue')
        <img
            src="@asset('images/intus-cross-white-blue.png')"
            alt="blue cross icon"
            class="full-width__plus"
            width="349"
            height="396"
        >
    @endif
</div>
