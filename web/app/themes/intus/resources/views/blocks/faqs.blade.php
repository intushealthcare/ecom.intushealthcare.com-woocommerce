{{--
  Title: FAQs
  Description: Accordion FAQ layout
  Category: flexible-content
  Icon: format-image
  Keywords: faq frequently asked question accordion
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: false
--}}

@php
    $options = TemplateFlexible::options();
    $fields = TemplateFlexible::featuredFaqs();
@endphp

<div class="flex-row block-faqs {{ $options }}">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 col-lg-10 col-xl-9">

                @if (!empty($fields['title']))
                    <h2 class="text-lighter mb-5">{!! $fields['title'] !!}</h2>
                @endif

                @if (!empty($fields['faqs']))
                    <div
                        class="accordion accordion--faqs"
                        id="accordion-flex"
                    >
                        @php $i = 1 @endphp
                        @foreach ($fields['faqs'] as $faq_item)
                            @php
                                global $post;
                                $post = $faq_item;
                                setup_postdata($post);
                            @endphp
                            <div class="card">
                                <div
                                    class="card-header"
                                    id="accordion-flex-item-header-{{ $i }}"
                                >
                                    <button
                                        class="collapsed"
                                        data-toggle="collapse"
                                        data-target="#accordion-flex-item-collapse-{{ $i }}"
                                        aria-expanded="false"
                                        aria-controls="accordion-flex-item-collapse-{{ $i }}"
                                    >
                                        <span>@php the_title() @endphp</span>
                                        <i class="fal fa-2x fa-angle-down down-arrow"></i>
                                        <i class="fal fa-2x fa-angle-up up-arrow"></i>
                                    </button>
                                </div>
                                <div
                                    id="accordion-flex-item-collapse-{{ $i }}"
                                    class="collapse"
                                    aria-labelledby="accordion-flex-item-header-{{ $i }}"
                                    data-parent="#accordion-flex"
                                >
                                    <div class="pt-4">
                                        @php the_content() @endphp
                                    </div>
                                </div>
                            </div>
                            @php $i++ @endphp
                        @endforeach
                        @php wp_reset_postdata() @endphp
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>
