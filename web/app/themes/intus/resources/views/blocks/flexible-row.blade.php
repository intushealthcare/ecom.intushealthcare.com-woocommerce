{{--
  Title: Flexible Row - 1-6 Columns
  Description: One to six column layout
  Category: flexible-content
  Icon: format-image
  Keywords: column image button content
  Mode: edit
  PostTypes: page post
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    use App\Controllers\TemplateFlexible;
    $options = TemplateFlexible::options();
    $container_width = TemplateFlexible::width_styles();
    $backgrounds = TemplateFlexible::backgrounds();
    $fields = TemplateFlexible::flexibleRow();
@endphp

@if (!empty($fields['columns']))
    <div class="flex-row flexible-columns intus-row {{ $options }}" {!! $backgrounds !!}>
        <div class="{{ $container_width  }}">
            <div class="row">
                @foreach ($fields['columns'] as $column)
                    <div class="d-flex flexible-columns-column {{ $fields['css_class'] }}" >
                        <div class="intus-row-column d-flex flex-column {!! TemplateFlexible::border_radius_styles() !!}" {!! TemplateFlexible::column_inline_styles($column) !!}>

                            {{-- Background Image --}}
                            @if (!empty($column['image']))
                                <div class="intus-row-column-image">
                                    <img
                                        src="{{ $column['image']['sizes']['large'] }}"
                                        loading="lazy"
                                        alt="{{ $column['image']['alt'] }}"
                                        height="{{ $column['image']['sizes']['large-height'] }}"
                                        width="{{ $column['image']['sizes']['large-width'] }}"
                                        class="heroimagebutton-columns__item-image"
                                    >
                                </div>
                            @endif

                            <div class="intus-row-column-text">
                                {{-- Title --}}
                                @if (!empty($column['title']))
                                    @php
                                        $titleTag = $column['title_tag'] ?? 'h2';
                                        $headingTextAlignment = ( $titleTag != 'h1' ) ? 'text-' . $column['alignment'] : 'text-center';
                                    @endphp
                                    <{{ $titleTag }} class="intus-row-column-text-title title text-lighter msb-1 iv-wp fade-up {{ $headingTextAlignment }}">
                                    <span>{!! $column['title'] !!}</span>
                                    </{{ $titleTag }}>
                                    @endif

                                    @if (!empty($column['content']))
                                        <div class="intus-row-column-text-content content">
                                            <div class="mt-auto iv-wp fade-up">
                                                {!! $column['content'] !!}
                                            </div>
                                        </div>
                                    @endif
                            </div>

                            @if (!empty($column['button']))
                            <div class="intus-row-column-buttons mt-auto">
                                {{-- Button --}}

                                    @include('partials.button', [
                                        'button' => $column['button'],
                                        'button_style' => $column['button_style'],
                                        'button_outline' => $column['button_outline'],
                                    ])
                            </div>
                             @endif

                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
