{{--
  Title: Statistics
  Description: One row grid of animating statistics
  Category: flexible-content
  Icon: format-image
  Keywords: statistics grid
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
    $options = TemplateFlexible::options();
    $fields = TemplateFlexible::statistics();
@endphp

<div class="flex-row statistics odometer-block {{ $options }}">
    <div class="container">

        {{-- Title --}}
        @if (!empty($fields['title']))
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-9 col-lg-8 col-xl-7">

                    <h2 class="intus-icon-title msb-3 text-center">
                        @if (!empty($fields['icon']))
                            @include('partials.intus-icon', [
                                'type' => 'pink',
                                'class' => 'icon-lg',
                            ])
                        @endif
                        {!! $fields['title'] !!}
                    </h2>

                </div>
            </div>
        @endif

        {{-- Statistics --}}
        @if (!empty($fields['statistics']))
            <ul class="statistics__list">
                @foreach ($fields['statistics'] as $statistic)
                    <li class="statistics__list-item">
                        <div class="statistics__list-item-inner">

                            {{-- Stat --}}
                            <p class="h2 color-primary d-flex justify-content-center">
                                <span
                                    class="odometer"
                                    data-stat="{{ $statistic['statistic'] }}"
                                >
                                    0
                                </span>
                                <span class="pt-2 pl-1">
                                    {!! $statistic['append'] !!}
                                </span>
                            </p>

                            {{-- Description --}}
                            <p class="text-bold mb-0">
                                {!! $statistic['description'] !!}
                            </p>

                        </div>
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
</div>
