@if (get_previous_post() || get_next_post())
    <footer class="post-navigation">
        <div class="container psy-1">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-11 col-lg-10 col-xl-9">

                    <div class="row">
                        <div class="col">
                            <div class="post-navigation__item">
                                @if (get_previous_post())
                                    @php previous_post_link('%link', '<span class="post-navigation__item-label">Previous</span><i class="far fa-long-arrow-left fa-2x"></i><span class="post-navigation__item-title">%title</span>') @endphp
                                @endif
                            </div>
                        </div>
                        <div class="col">
                            <div class="post-navigation__item post-navigation__item--end">
                                @if (get_next_post())
                                    @php next_post_link('%link', '<span class="post-navigation__item-label">Next</span><span class="post-navigation__item-title">%title</span><i class="far fa-long-arrow-right fa-2x"></i>') @endphp
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </footer>
@endif
