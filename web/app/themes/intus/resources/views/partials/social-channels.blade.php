@php
    $channels = $channels ?? $social_channels;
    $size = $size ?? '';
    $color = $color ?? '';
    $class = $class ?? '';
@endphp

@if (!empty($channels))
    <ul class="social-channels{{ (!empty($class)) ? ' ' . $class : '' }}">
        @foreach ($channels as $channel)
            <li>
                <a
                    href="{{ $channel['link'] }}"
                    class="{{ (!empty($color)) ? $color : '' }}"
                    target="_blank"
                >
                    <i class="{{ $channel['icon'] }}{{ (!empty($size)) ? ' ' . $size : '' }}"></i>
                </a>
            </li>
        @endforeach
    </ul>
@endif
