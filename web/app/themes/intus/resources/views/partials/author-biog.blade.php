{{-- Author --}}
@if (!$user['hide'])
    <a href="{{ get_author_posts_url($user['user_data']->ID) }}"
        class="d-flex align-items-center color-secondary">
        <img
            class="rounded-circle"
            width="70"
            height="70"
            src="{{ $user['image']['sizes']['thumbnail'] }}"
            alt="{{ $user['image']['alt'] }}"
        >
        <p class="d-flex flex-column mb-0 pl-4">
            <span class="p-sm">
                <span class="post-user-task">
                    @if ($user['jobtitle'])
                        {!! __('Expert review', 'sage') !!}
                    @else
                        {!! __('Written', 'sage') !!}
                    @endif
                </span>
                by
                <br>
                <span class=" font-weight-bold">
                    <span class="post-user-name">{{ $user['user_data']->display_name }}</span>,
                    <span class="post-user-job-title">
                        @if ($user['jobtitle'])
                            {{ $user['jobtitle'] }}
                        @else
                            {!! __('Staff Writer', 'sage') !!}
                        @endif
                    </span>
                </span>
            </span>
        </p>
    </a>
@endif
