@php
    $class = $class ?? '';
    $type = $type ?? false;
    $pinkIcon = 'intus-cross-pink-blue.png';
    $blueIcon = 'intus-cross-white-blue.png';
    $smallIcon = 'intus-cross-small.jpg';
    $icon = $smallIcon;

    if ($type == 'pink') {
        $icon = $pinkIcon;
    } elseif ($type == 'blue') {
        $icon = $blueIcon;
    }
@endphp

<span class="intus-icon{{ (!empty($class)) ? ' ' . $class : '' }}">
    <img
        src="@asset('images/' . $icon)"
        alt="icon cross"
        width="529"
        height="604"
    >
</span>
