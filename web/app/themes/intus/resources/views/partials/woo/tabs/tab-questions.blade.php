@php
    $schemaType = "itemscope itemtype='https://schema.org/FAQPage'";
	$schemaQuestion = "itemscope itemprop='mainEntity' itemtype='https://schema.org/Question'";
	$schemaQuestionProp = "itemprop='name'";
	$schemaAnswer = "itemscope itemprop='acceptedAnswer' itemtype='https://schema.org/Answer'";
	$schemaAnswerProp = "itemprop='text'";
@endphp

@if ($faq_type == 'global')

    {{-- FAQ Post objects --}}
    @if (!empty($questions))
        <div class="faq-body" {!! $schemaType !!}>
            @foreach($questions as $item)
                @php
                    global $post;
                    $post = $item;
                    setup_postdata($post);
                @endphp
                <div class="mb-5"
                     itemscope
                     itemprop="mainEntity"
                     itemtype="https://schema.org/Question"
                     >
                    <h5 class="mb-3"
                        itemprop="name">
                        @php the_title() @endphp
                    </h5>
                    <div itemscope
                         itemprop="acceptedAnswer"
                         itemtype="https://schema.org/Answer">
                        <div itemprop="text">
                            @php the_content() @endphp
                        </div>
                    </div>
                </div>
            @endforeach
            @php wp_reset_postdata() @endphp
        </div>
    @endif

@elseif ($faq_type == 'custom')

    {{-- FAQ Custom text --}}
    @if (!empty($custom_questions))
        <div class="card-body" {!! $schemaType !!} >
            @foreach ($custom_questions as $item)
                @php
                    $showSchema = false;
					if ( !empty( $item['question'] ) && !empty( $item['answer'] ) ){ $showSchema = true; }
                @endphp
                <div class="mb-5" @if( $showSchema ) {!! $schemaQuestion  !!} @endif>
                    <h5 class="mb-3" @if( $showSchema ) {!! $schemaQuestionProp  !!} @endif>
                        {!! $item['question'] !!}
                    </h5>
                    <div @if( $showSchema ) {!! $schemaAnswer  !!} @endif>
                        <div @if( $showSchema ) {!! $schemaAnswerProp  !!} @endif>
                            {!! $item['answer'] !!}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif

@endif
