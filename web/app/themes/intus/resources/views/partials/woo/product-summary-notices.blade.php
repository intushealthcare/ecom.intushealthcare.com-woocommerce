<div class="product-notice-quicklinks row">
    <div class="col-12">
    @if (!empty($messages))
            @foreach ($messages as $message)
                @php $message_shown = false @endphp
                @if (!empty($message['product_categories']))
                    @foreach ($message['product_categories'] as $category)
                        @if (in_array($category, $terms) && !$message_shown)
                            @include('partials.woo.product-notice-quicklink', [
                                'classes' => 'product-notice--information mt-5',
                                'icon' => 'info',
                                'content' => $message['content'],
                            ])
                            @php $message_shown = true @endphp
                        @endif
                    @endforeach
                @endif
            @endforeach
        @endif

    </div>
</div>
