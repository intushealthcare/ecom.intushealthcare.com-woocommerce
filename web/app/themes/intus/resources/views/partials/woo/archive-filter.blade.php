<div
    id="form-product-filter"
    class="product-filter"
>
    <div class="product-filter__header">
        <div class="d-flex align-items-center justify-content-between">
            <div>
                <h3 class="mb-0 mb-lg-4">{!! __('Shop By', 'sage') !!}</h3>
            </div>
            <div>
                <button
                    type="button"
                    class="product-filter__close d-inline-block d-lg-none"
                >
                    <i class="fal fa-lg fa-times"></i>
                </button>
            </div>
        </div>
    </div>

    <form class="product-filter__search mb-4">
        <input
            type="search"
            placeholder="{{ is_shop() ? __('Search products', 'sage') : __('Search category', 'sage') }}"
            value="{{ (isset($_GET['keyword'])) ? $_GET['keyword'] : '' }}"
            id="filter-keyword"
        >
        <button
            type="button"
            class="product-filter__search-submit"
        >
            <i class="far fa-search"></i>
        </button>
    </form>

    <div class="product-filter__applied mt-4">
        {{-- Applied filters append here with AJAX --}}
        @include('partials.woo.active-filters')
    </div>

    @if (!empty($product_filters))
        <div
            class="accordion accordion--filter mt-lg-4"
            id="accordion-filter"
        >
            @foreach ($product_filters as $filter)
                <div class="card">
                    <div
                        class="card-header"
                        id="card-heading-{{ $loop->iteration }}"
                    >
                        <button
                            class="collapsed"
                            type="button"
                            data-toggle="collapse"
                            data-target="#card-collapse-{{ $loop->iteration }}"
                            aria-expanded="false"
                            aria-controls="collapse-{{ $loop->iteration }}"
                        >
                            {!! $filter['title'] !!}
                        </button>
                    </div>
                    <div
                        id="card-collapse-{{ $loop->iteration }}"
                        class="collapse"
                        aria-labelledby="card-heading-{{ $loop->iteration }}"
                        data-parent="#accordion-filter"
                    >
                        <div class="card-body">

                            @if (!empty($filter['tax']) && taxonomy_exists($filter['tax']))
                                @php
                                    $args = [
                                        'hide_empty' => true
                                    ];

                                    if ($filter['tax'] == 'product_cat') {
                                        if (is_shop()) {
                                            $args['parent'] = 0;
                                        } elseif (is_tax()) {
                                            $args['parent'] = get_queried_object_id();
                                        }
                                    }

                                    $terms = get_terms($filter['tax'], $args);
                                @endphp

                                @if (!empty($terms))
                                    @foreach ($terms as $term)
                                        @include('partials.woo.filter-item', [
                                            'tax' => $filter['tax'],
                                            'term' => $term,
                                        ])
                                    @endforeach
                                @endif
                            @elseif (!empty($filter['path']))
                                @if(file_exists(get_stylesheet_directory() . '/views/partials/woo/filter-' . $filter['path'] . '.blade.php'))
                                    @include('partials.woo.filter-' . $filter['path'])
                                @endif
                            @endif

                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif

</div>
