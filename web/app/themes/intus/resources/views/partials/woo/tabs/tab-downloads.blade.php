@if (!empty($files))
    <div class="tab-downloads row">
        <div class="col-12">
            @foreach($files as $item)
                @if( !empty($item[ 'file' ])  )
                    <div class="mb-5">
                        <h5 class="mb-3">
                            <a href="{{ $item['file']['url'] }}" title="{{ __('Download', 'sage') }} {{ $item['title'] }}" class="tab-downloads__link" target="_blank">
                                <i class="fal fa-file-alt mr-4"></i> {{ $item['title'] }}
                            </a>
                        </h5>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endif
