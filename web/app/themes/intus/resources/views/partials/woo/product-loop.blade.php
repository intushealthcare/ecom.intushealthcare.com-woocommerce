@php
    global $wp_query;
    $custom_query = $custom_query ?? $wp_query;
    $wp_query = $custom_query;
    $paged = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;

    wc_set_loop_prop('current_page', $paged);
    wc_set_loop_prop('is_paginated', wc_string_to_bool(true));
    wc_set_loop_prop('page_template', get_page_template_slug());
    wc_set_loop_prop('per_page', get_option('posts_per_page'));
    wc_set_loop_prop('total', $wp_query->found_posts);
    wc_set_loop_prop('total_pages', $wp_query->max_num_pages);
@endphp

{{-- Product loop --}}
@if($wp_query->have_posts())
    @php
        do_action('woocommerce_before_shop_loop');
    @endphp

    @include('partials/woo/archive-adverts')

    @php
        woocommerce_product_loop_start();
    @endphp

    @while($wp_query->have_posts())
        @php
            $wp_query->the_post();
            do_action('woocommerce_shop_loop');
            wc_get_template_part('content', 'product');
        @endphp
    @endwhile

    @php
        woocommerce_product_loop_end();
        do_action('woocommerce_after_shop_loop');
    @endphp
@else
    @php
        do_action('woocommerce_no_products_found');
    @endphp
@endif
