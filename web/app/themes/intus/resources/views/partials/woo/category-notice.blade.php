<div class="container psy-1 category-notice-vat">
    <div class="row">
        <div class="col-12 text-center">
            {!! __('Prices shown include VAT at 0% for VAT relief eligible products, and 20% otherwise. Please note VAT on relief eligible products will be charged at 20% if relief is not claimed during the checkout', 'sage') !!}
        </div>
    </div>
</div>
