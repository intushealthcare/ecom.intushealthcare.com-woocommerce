@php
    $term = $term ?? false;
    $tax = $tax ?? false;
    $checked = false;
@endphp

@if (!empty($term))
    @php
        // If tax is product_cat set it to "cat" otherwise WooCommerce changes queried_object to the child
        $tax = ($tax == 'product_cat') ? 'cat' : $tax;

        if (isset($_GET[$tax])) {
            $activeTerms =  explode(',', $_GET[$tax]);
            if (in_array($term->slug, $activeTerms)) {
                $checked = true;
            }
        }
        $tooltip = get_field('tooltip', $term)
    @endphp
    <div class="product-filter__item">
        <div class="product-filter__checkbox">
            <input
                type="checkbox"
                id="filter-checkbox-{{ $term->slug }}"
                class="product-filter__checkbox-item"
                name="{{ $tax }}"
                value="{{ $term->slug }}"
                data-label="{{ $term->name }}"
                @if ($checked)
                    checked
                @endif
            >
            <label for="filter-checkbox-{{ $term->slug }}">
                {!! $term->name !!}
            </label>
        </div>
        @if (!empty($tooltip))
            <div
                data-toggle="tooltip"
                data-placement="right"
                title="{{ $tooltip }}"
                class="d-flex align-items-center justify-content-center"
            >
                <i class="fas fa-question-circle fa-lg color-primary-dark"></i>
            </div>
        @endif
    </div>
@endif
