<div id="cart-vat-declaration" class="cart-vat-declaration">
    <div class="product-notice product-notice--declaration product-notice--declaration-short mb-4">
        {!! $checkbox !!}
    </div>
    <span id="cart-vat-declaration-full-terms-show"
          class="cart-vat-declaration-full-terms-show d-block m-4 text-center text-underline">{{ __('Click here to view the full disclaimer', 'sage') }}</span>
    <div id="cart-vat-declaration-full-terms" class="cart-vat-declaration-full-terms d-none my-4">
        {!! $declaration !!}
        <span id="cart-vat-declaration-full-terms-hide"
              class="cart-vat-declaration-full-terms-hide m-4 text-center text-underline d-none"> {{ __('Hide disclaimer', 'sage')  }}</span>
    </div>
</div>
