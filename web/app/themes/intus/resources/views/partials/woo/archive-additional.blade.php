@if (is_tax())
    @php
        $term = get_queried_object();
        $content = get_field('additional_content', $term);
        $faqs = get_field('faqs', $term);
    @endphp
    @if (!empty($content) || isset($faqs['type']))
        <div class="archive-additional">

            {{-- Content --}}
            @if (!empty($content))
                <div class="mst-1">
                    {!! $content !!}
                </div>
            @endif

            {{-- FAQs --}}
            @php
                $z = 1;
                $faq_items = ($faqs['type'] == 'global') ? $faqs['global_faqs'] : (($faqs['type'] == 'custom') ? $faqs['custom_faqs'] : '');
            @endphp
            @if (isset($faqs['type']) && !empty($faq_items))
                <h3 class="mb-5">{!! $term->name !!} {!! __('FAQs', 'sage') !!}</h3>
                <div
                    id="accordion-category"
                    class="accordion accordion--faqs"
                    itemscope
                    itemtype="https://schema.org/FAQPage"
                >
                    @foreach ($faq_items as $faq)
                        @if ($faqs['type'] == 'global')
                            @php
                                global $post;
                                $post = $faq;
                                setup_postdata($post);
                            @endphp
                        @endif
                        <div class="card"
                             itemscope
                             itemprop="mainEntity"
                             itemtype="https://schema.org/Question">
                            <div
                                class="card-header"
                                id="accordion-category-item-header-{{ $z }}"
                            >
                                <button
                                    class="collapsed"
                                    data-toggle="collapse"
                                    data-target="#accordion-category-item-collapse-{{ $z }}"
                                    aria-expanded="{{ ($z == 1) ? 'true' : 'false' }}"
                                    aria-controls="accordion-category-item-collapse-{{ $z }}"
                                >
                                    <span class="h5 mb-0"
                                          itemprop="name">
                                        @if ($faqs['type'] == 'global')
                                            @php the_title() @endphp
                                        @else
                                            {!! $faq['question'] !!}
                                        @endif
                                    </span>
                                    <i class="fal fa-2x fa-angle-down down-arrow"></i>
                                    <i class="fal fa-2x fa-angle-up up-arrow"></i>
                                </button>
                            </div>
                            <div
                                id="accordion-category-item-collapse-{{ $z }}"
                                class="collapse"
                                aria-labelledby="accordion-category-item-header-{{ $z }}"
                                data-parent="#accordion-category"
                            >
                                <div class="pt-5"
                                     itemscope
                                     itemprop="acceptedAnswer"
                                     itemtype="https://schema.org/Answer">
                                    <div itemprop="text">
                                    @if ($faqs['type'] == 'global')
                                        @php the_content() @endphp
                                    @else
                                        {!! $faq['answer'] !!}
                                    @endif
                                </div>
                            </div>

                        </div>
                        </div>
                        @php $z++ @endphp
                    @endforeach
                    @php wp_reset_postdata() @endphp
                </div>
            @endif

        </div>
    @endif
@endif
