@php
    $notice_id = null;
    if( !empty( $category ) ) {
        $notice_id = "product-tab-description-category-notice-id-{$category}";
    }
@endphp
@if ( $notice_id )
<div class="product-notice d-flex flex-row product-notice--quicklink mb-3">
    <div class="mb-4 mr-3 mb-sm-0 mr-md-5">
        <i class="far fa-3x fa-exclamation-circle"></i>
    </div>
    <div>
        <p class="p-sm">
            <strong><?= __( 'Important Information', 'sage' ) ?>:</strong>
        </p>
        <p class="p-sm">
            <?php echo __( 'Please ensure you ', 'sage' ); ?>
            <strong><a href="#{{ $notice_id }}" class="product-notice-quicklink"><?php echo __( 'click here', 'sage' ); ?></a></strong>
            <?php echo __( ' to read some important information about purchasing a machine before you place your order', 'sage' ); ?>
        </p>
    </div>
</div>
@endif

