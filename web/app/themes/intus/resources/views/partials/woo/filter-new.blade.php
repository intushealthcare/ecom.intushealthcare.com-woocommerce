<div class="product-filter__item">
    <div class="product-filter__checkbox">
        <input
            type="checkbox"
            id="filter-checkbox-new"
            class="product-filter__checkbox-item"
            name="new"
            value="new"
            data-label="New"
        >
        <label for="filter-checkbox-new">{!! __('New Arrivals', 'sage') !!}</label>
    </div>
</div>
