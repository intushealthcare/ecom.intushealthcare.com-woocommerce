<div class="product-filter__item">
    <div class="product-filter__checkbox">
        <input
            type="checkbox"
            id="filter-checkbox-sale"
            class="product-filter__checkbox-item"
            name="sale"
            value="sale"
            data-label="Sale"
        >
        <label for="filter-checkbox-sale">{!! __('Sale Products', 'sage') !!}</label>
    </div>
</div>
