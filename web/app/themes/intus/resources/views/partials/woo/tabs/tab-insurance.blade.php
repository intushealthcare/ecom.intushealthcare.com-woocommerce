@if (!empty($enabled))
    {!! $use_default_content ? get_field('mask_fit_insurance_content', 'option') : $content !!}
@endif

@if ($content)
    {!! $content !!}
@endif
