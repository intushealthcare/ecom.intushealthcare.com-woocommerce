<div id="checkout-vat-declaration-warning" class="mb-5">
    <div class="product-notice product-notice--declaration">
        <p><?= sprintf( __( 'VAT relief can be claimed on this page – if you are eligible and wish you claim VAT relief on your order,
            please %sensure you have checked the box at the bottom of this page%s before completing payment.', 'sage' ), '<strong>', '</strong>') ?></p>
    </div>
</div>
