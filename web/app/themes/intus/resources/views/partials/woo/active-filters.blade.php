{{-- NOTE: Need to add "New" and "Sale" in, as they are not taxonomies --}}

@if (!empty($_GET))
    @foreach ($_GET as $key => $termString)
        @php
            // Set product category back to correct name
            if ($key == 'cat') {
                $key = 'product_cat';
            }
            // Get array of terms
            $termSlugs =  explode(',', $termString);
        @endphp
        @foreach ($termSlugs as $slug)
            @php $term = get_term_by('slug', $slug, $key) @endphp
            @if (!empty($term))
                <div class="product-filter__applied-item" id="applied-filter-{{ $term->slug }}" data-slug="{{ $term->slug }}">
                    <span class="product-filter__applied-item-label">{!! $term->name !!}</span>
                    <span class="product-filter__applied-item-remove">
                        <i class="far fa-times"></i>
                    </span>
                </div>
            @endif
        @endforeach
    @endforeach
@endif
