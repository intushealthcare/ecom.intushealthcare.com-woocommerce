@php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
@endphp

@if ($product)
    {!! '<a class="product-more-info" href="'. $product->get_permalink() . '">' . __('More info', 'sage') . '</a>' !!}
@endif
