@php
    $notice_id= "";
    $baseClasses = [
        'product-notice',
        'd-flex',
        'flex-row'
    ];

    if (!empty($classes)) {
        if (!is_array($classes)) {
            $classes = explode(' ', $classes);
        }

        $classes = implode(' ', array_unique(array_merge($baseClasses, $classes)));
    }


    if( !empty( $id ) ){
        $notice_id = "id='$id'";
    }

    if( !empty( $category ) ){
        $notice_id = "id='product-tab-description-category-notice-id-{$category}'";
    }
@endphp
<div {!! $notice_id !!} class="{{ $classes }}">
    <div class="mb-4 mr-3 mb-sm-0 mr-md-5">
        <i class="far fa-3x fa-{{ $icon }}-circle"></i>
    </div>
    <div>
        {!! $content !!}
    </div>
</div>
