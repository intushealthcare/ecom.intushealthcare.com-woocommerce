{{-- Range slider --}}
<div class="pl-2 pr-3">
    <div id="filter-price"></div>
</div>

{{-- Range slider value --}}
<div
    id="filter-price-value"
    class="mt-4"
>
</div>

{{-- Range min --}}
<input
    id="filter-price-min"
    type="hidden"
    value="{{ (isset($_GET['price_min'])) ? $_GET['price_min'] : '' }}"
>

{{-- Range max --}}
<input
    id="filter-price-max"
    type="hidden"
    value="{{ (isset($_GET['price_max'])) ? $_GET['price_max'] : '' }}"
>

<button
    type="button"
    id="filter-price-btn"
>
    {!! __('Apply Filter', 'sage') !!}
</button>
