{{-- Advert --}}
@if (is_tax())
    @php
        $term = get_queried_object();
        $adverts = get_field('advert_carousel', $term);
    @endphp

    @if ($adverts)
        <div class="product-gallery mb-5">
            @foreach ($adverts as $advert)
                @if (!empty($advert['link']))
                    <a
                        href="{{ $advert['link'] }}"
                        class="d-block"
                    >
                @endif
                    @if (!empty($advert['image']['sizes']['large']))
                        <div class="product-gallery__slider-item">
                            <div>
                                <div
                                    class="product-gallery__slider-item-image"
                                    style="background-image: url({{ $advert['image']['sizes']['large'] }});"
                                >
                                </div>
                            </div>
                        </div>
                    @endif
                @if (!empty($advert['link']))
                    </a>
                @endif
            @endforeach
        </div>
    @endif
@endif
