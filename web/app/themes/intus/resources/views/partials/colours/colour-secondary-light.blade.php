@php
    $colour = $colour ?? false;
@endphp

@if (!empty($colour))
    <style>
        /* Begin style overrides for secondary-light colour */
        .color-secondary-light {
            color: {{ $colour }} !important;
        }

        .bg-secondary-light {
            background: {{ $colour }} !important;
        }

        .icon-row-list {
            background: {{ $colour }} !important;
        }

        .post-navigation {
            background: {{ $colour }} !important;
        }

        .full-width--blue {
            background: {{ $colour }} !important;
        }

        .product-listing-content__more {
            background: {{ $colour }} !important;
        }
    </style>
@endif
