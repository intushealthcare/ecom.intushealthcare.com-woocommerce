@php
    $colour = $colour ?? false;
@endphp

@if (!empty($colour))
    <style>
        /* Begin style overrides for tertiary colour */
        .color-tertiary {
            color: {{ $colour }} !important;
        }

        .bg-tertiary {
            background: {{ $colour }} !important;
        }

        .template-faqs li a {
            background: {{ $colour }} !important;
        }

        .tabs-container .nav-tabs .nav-link {
            background: {{ $colour }} !important;
        }

        .tabs-container .nav-tabs .nav-link.active {
            color: {{ $colour }} !important;
        }

        .product .newproduct {
            background: {{ $colour }} !important;
        }
    </style>
@endif
