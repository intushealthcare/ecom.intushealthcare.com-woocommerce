@php
    $colour = $colour ?? false;
@endphp

@if (!empty($colour))
    <style>
        /* Begin style overrides for secondary colour */
        .color-secondary {
            color: {{ $colour }} !important;
        }

        .bg-secondary {
            background: {{ $colour }} !important;
        }

        body {
            color: {{ $colour }};
        }

        .btn--white {
            color: {{ $colour }};
        }

        .btn--white:hover {
            background: {{ $colour }} !important;
        }

        .btn--secondary {
            background: {{ $colour }};
        }

        .btn--outline {
            color: {{ $colour }};
            border-color: {{ $colour }};
            background: transparent;
        }

        .btn--outline:hover {
            color: white;
            background: {{ $colour }};
        }

        .btn--outline[disabled]:hover {
            color: {{ $colour }};
            border-color: {{ $colour }};
        }

        a.bg-secondary:hover {
            background: {{ $colour }} !important;
        }

        a.bg-sale--secondary {
            background: {{ $colour }} !important;
        }

        a.bg-sale--outline,
        a.bg-sale--outline:hover {
            color: {{ $colour }} !important;
            border-color: {{ $colour }} !important;
        }

        .cc-window {
            color: {{ $colour }} !important;
        }

        .cc-window .cc-message .cc-link {
            color: {{ $colour }} !important;
        }

        .cc-window .cc-btn.cc-allow {
            color: {{ $colour }} !important;
        }

        .blog-category-link {
            color: {{ $colour }} !important;
        }

        .pagination-blog ul li a {
            color: {{ $colour }} !important;
        }

        .post-navigation__item-title {
            color: {{ $colour }} !important;
        }

        .post-navigation__item i {
            color: {{ $colour }} !important;
        }

        .faq-search input[type="search"] {
            border-color: {{ $colour }} !important;
        }

        .template-faqs .nav-tabs li a.active {
            color: {{ $colour }} !important;
        }

        .accordion.accordion--faqs .card .card-header button {
            color: {{ $colour }} !important;
        }

        .flex-row--navigation ul li a.active::after {
            background: {{ $colour }} !important;
        }

        .nav--footer li a {
            color: {{ $colour }} !important;
        }

        .modal .close {
            background: {{ $colour }} !important;
        }

        .pagination ul li a {
            color: {{ $colour }} !important;
        }

        .main-header__icons li a:not(.btn) {
            color: {{ $colour }} !important;
        }

        .nav--primary li a {
            color: {{ $colour }} !important;
        }

        .nav--primary > li.menu-item-has-children > .meganav .meganav__inner > .sub-menu > li > span,
        .nav--primary > li.menu-item-has-children > .meganav .meganav__inner > .sub-menu > li > a {
            color: {{ $colour }} !important;
        }

        .nav--offcanvas li a {
            color: {{ $colour }} !important;
        }

        .nav--offcanvas li.menu-item-has-children > a {
            border-left-color: {{ $colour }} !important;
            border-bottom-color: {{ $colour }} !important;
        }

        .border-secondary {
            border-color: {{ $colour }} !important;
        }

        .category-columns__item-title h2 {
            color: {{ $colour }} !important;
        }

        .slider-featured__item-title {
            border-bottom-color: {{ $colour }} !important;
        }

        .slider-featured-nav .slick-arrow {
            color: {{ $colour }} !important;
        }

        .latest-news__posts-item-link {
            color: {{ $colour }} !important;
        }

        .latest-news__posts-item-link::before {
            background: {{ $colour }} !important;
        }

        .woocommerce-MyAccount-navigation ul li a {
            color: {{ $colour }} !important;
        }

        .product-listing-content__more {
            color: {{ $colour }} !important;
        }

        .product.product-category > a .woocommerce-loop-category__title {
            color: {{ $colour }} !important;
        }

        .shop-table__item td::before {
            color: {{ $colour }} !important;
        }

        .shop-table__item-name a {
            color: {{ $colour }} !important;
        }

        .shop-table__item-subtotal {
            color: {{ $colour }} !important;
        }

        .shop-table__item-quantity input {
            color: {{ $colour }} !important;
        }

        .shop-table__item-remove a {
            color: {{ $colour }} !important;
        }

        .form-row input[type="text"],
        .form-row input[type="email"],
        .form-row input[type="tel"],
        .form-row input[type="password"],
        .form-row select,
        .form-row textarea {
            color: {{ $colour }} !important;
        }

        .wc_payment_method p,
        .wc_payment_method a {
            color: {{ $colour }} !important;
        }

        .accordion--filter .card-header button {
            color: {{ $colour }} !important;
        }

        .single-product .product_title {
            color: {{ $colour }} !important;
        }

        .single-product select,
        .single-product input[type="number"] {
            border-color: {{ $colour }} !important;
        }

        .single-product .related-products .slick-arrow {
            border-color: {{ $colour }} !important;
        }

        .woocommerce-breadcrumb a {
            color: {{ $colour }} !important;
        }

        .woocommerce-ordering .orderby {
            color: {{ $colour }} !important;
        }

        .single-product .woocommerce-review-link {
            color: {{ $colour }} !important;
        }
    </style>
@endif
