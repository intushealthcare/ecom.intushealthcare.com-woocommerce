@php
    $colour = $colour ?? false;
@endphp

@if (!empty($colour))
    <style>
        /* Begin style overrides for primary colour */
        a {
            color: {{ $colour }};
        }

        .color-primary {
            color: {{ $colour }} !important;
        }

        .bg-primary {
            background: {{ $colour }} !important;
        }

        .h-quote::before {
            color: {{ $colour }} !important;
        }

        .h-quote::after {
            color: {{ $colour }} !important;
        }

        .btn {
            background: {{ $colour }};
        }

        .btn--white {
            background: white;
        }

        .btn--outline {
            background: transparent;
        }

        .btn--primary {
            background: {{ $colour }};
            color: white;
        }

        a.bg-primary:hover {
            background-color: {{ $colour }} !important;
        }

        .cc-window {
            border-color: {{ $colour }} !important;
        }

        .cc-window .cc-btn.cc-allow:hover {
            color: {{ $colour }} !important;
        }

        .form-signup .response {
            background: {{ $colour }} !important;
        }

        .steps__list-item::before {
            border-color: {{ $colour }} !important;
        }

        .steps__list-item-number {
            background: {{ $colour }} !important;
        }

        .blog-category-link:hover {
            color: {{ $colour }} !important;
        }

        .blog-category-link.active {
            border-color: {{ $colour }} !important;
        }

        .pagination-blog ul li a i {
            border-color: {{ $colour }} !important;
        }

        .author-posts__item-terms::before {
            color: {{ $colour }} !important;
        }

        .author-posts__item-terms li {
            color: {{ $colour }} !important;
        }

        .slider-banner .slick-dots li.slick-active button {
            background: {{ $colour }} !important;
        }

        .main-footer {
            border-color: {{ $colour }} !important;
        }

        .slick-dots li.slick-active button {
            background: {{ $colour }} !important;
        }

        .bg-primary-half::before {
            background: {{ $colour }} !important;
        }

        .pagination ul li.iterator a {
            border-color: {{ $colour }} !important;
        }

        .main-header {
            border-color: {{ $colour }} !important;
        }

        .main-header__icons {
            border-color: {{ $colour }} !important;
        }

        .main-header__icons li a:not(.btn).basket-link {
            background: {{ $colour }} !important;
        }

        .nav--primary > li > a::after {
            border-bottom-color: {{ $colour }} !important;
        }

        .nav--primary > li.menu-item-has-children > .meganav {
            border-top-color: {{ $colour }} !important;
        }

        .sleep-test-block__content {
            border-color: {{ $colour }} !important;
        }

        .sleep-test-block__image::before {
            background: rgba({{ $colour }}, 0.2) !important;
        }

        .offcanvas__close {
            color: {{ $colour }} !important;
        }

        .nav--offcanvas > li {
            border-bottom-color: {{ $colour }} !important;
        }

        .unsubscribe-content {
            border-color: {{ $colour }} !important;
        }

        .slider-carousel__item-step {
            color: {{ $colour }} !important;
        }

        .slider-carousel__item-circle {
            background: {{ $colour }} !important;
        }

        .alternating-columns__lists ul li::before {
            background: {{ $colour }} !important;
        }

        .slider-testimonials__item-stars li {
            color: {{ $colour }} !important;
        }

        .woocommerce-MyAccount-navigation ul li.is-active a {
            background: {{ $colour }} !important;
        }

        .woocommerce-MyAccount-content h2 {
            color: {{ $colour }} !important;
        }

        .product .onsale {
            background: {{ $colour }} !important;
        }

        .shop-table__item-name a:hover {
            color: {{ $colour }} !important;
        }

        .shop-table__item-remove a:hover {
            color: {{ $colour }} !important;
        }

        .woocommerce-order {
            border-color: {{ $colour }} !important;
        }

        .product-filter__applied-item-label {
            background: {{ $colour }} !important;
        }

        .single-product .cart button[type="submit"] {
            background: {{ $colour }} !important;
        }

        #review_form_wrapper .stars > span > a {
            border-color: {{ $colour }} !important;
            color: {{ $colour }} !important;
        }

        #review_form_wrapper .stars > span > a:hover,
        #review_form_wrapper .stars > span > a.active {
            background: {{ $colour }} !important;
        }

        .woocommerce-MyAccount-content button[type="submit"],
        .checkout-login-form button,
        .shipping-calculator-form .button,
        .form-row.place-order button,
        .woocommerce-cart-form .wc-proceed-to-checkout a,
        .woocommerce-MyAccount-content button[type="submit"],
        .order-again a {
            background: {{ $colour }} !important;
        }
    </style>
@endif
