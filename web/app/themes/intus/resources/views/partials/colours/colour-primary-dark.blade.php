@php
    $colour = $colour ?? false;
@endphp

@if (!empty($colour))
    <style>
        /* Begin style overrides for primary-dark colour */
        .color-primary-dark {
            color: {{ $colour }} !important;
        }

        .bg-primary-dark {
            background: {{ $colour }} !important;
        }

        a:hover {
            color: {{ $colour }};
        }

        .btn:hover {
            background: {{ $colour }};
        }

        .btn--primary:hover {
            background: {{ $colour }};
        }

        .tooltip-inner {
            background: {{ $colour }} !important;
        }

        .tooltip.bs-tooltip-right .arrow::before,
        .tooltip.bs-tooltip-auto[x-placement^=right] .arrow::before {
            border-right-color: {{ $colour }} !important;
        }

        .tooltip.bs-tooltip-left .arrow::before,
        .tooltip.bs-tooltip-auto[x-placement^="left"] .arrow::before {
            border-left-color: {{ $colour }} !important;
        }

        .single-product .product-single-main .price ins {
            color: {{ $colour }} !important;
        }

        .single-product select, input[type="number"]:focus {
            border-color: {{ $colour }} !important;
        }

        .single-product .woocommerce-grouped-product-list-item__price ins {
            color: {{ $colour }} !important;
        }

        .product-notice--declaration {
            border-color: {{ $colour }} !important;
        }
    </style>
@endif
