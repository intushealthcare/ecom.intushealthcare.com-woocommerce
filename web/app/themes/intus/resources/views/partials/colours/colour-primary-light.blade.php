@php
    $colour = $colour ?? false;
@endphp

@if (!empty($colour))
    <style>
        /* Begin style overrides for primary-light colour */
        .color-primary-light {
            color: {{ $colour }} !important;
        }

        .bg-primary-light {
            background: {{ $colour }} !important;
        }

        .slick-dots li button {
            background: {{ $colour }} !important;
        }

        .store-notice-header {
            background: {{ $colour }} !important;
        }

        .full-width--pink {
            background: {{ $colour }} !important;
        }

        .form-row--declaration {
            border-color: {{ $colour }} !important;
        }

        .product-notice--declaration {
            background: {{ $colour }} !important;
        }
    </style>
@endif
