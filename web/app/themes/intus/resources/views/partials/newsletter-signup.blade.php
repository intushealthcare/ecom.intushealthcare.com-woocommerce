<div class="container-fluid px-0 bg-lightest-grey">
    <div class="container psy-1">
        <div id="mc_embed_signup">
            <form
              action="https://intushealthcare.us2.list-manage.com/subscribe/post?u=b4bd9c2a2d0db35629fa2e231&amp;id=6147592da2"
              method="post"
              id="mc-embedded-subscribe-form"
              name="mc-embedded-subscribe-form"
              class="validate form-signup"
              target="_blank"
              novalidate
            >
                <!-- Begin Mailchimp Signup Form -->
                <div id="mc_embed_signup_scroll">
                    @if (!empty($footer_fields['newsletter']))
                        <div class="text-center mb-5 mx-lg-5">
                            {!! $footer_fields['newsletter'] !!}
                        </div>
                    @endif
                    <div class="mb-4 w-100">
                        <div class="field-wrap">
                            <label
                              for="mce-FNAME"
                              class="sr-only"
                            >
                                {!! __('First Name', 'sage') !!}
                            </label>
                            <div class="field-wrap__input">
                                <input
                                  type="text"
                                  value=""
                                  name="FNAME"
                                  class=""
                                  id="mce-FNAME"
                                  placeholder="{{ __('Enter your first name', 'sage') }}"
                                >
                            </div>
                        </div>
                    </div>
                    <div class="mb-4 w-100">
                        <div class="field-wrap">
                            <label
                              for="mce-LNAME"
                              class="sr-only"
                            >
                                {!! __('Last Name', 'sage') !!}
                            </label>
                            <div class="field-wrap__input">
                                <input
                                  type="text"
                                  value=""
                                  name="LNAME"
                                  class=""
                                  id="mce-LNAME"
                                  placeholder="{{ __('Enter your last name', 'sage') }}"
                                >
                            </div>
                        </div>
                    </div>
                    <div class="mb-4 w-100">
                        <div class="field-wrap">
                            <label
                              for="mce-EMAIL"
                              class="sr-only"
                            >
                                {!! __('Email Address', 'sage') !!}
                            </label>
                            <div class="field-wrap__input">
                                <input
                                  type="email"
                                  value=""
                                  name="EMAIL"
                                  class="required email"
                                  id="mce-EMAIL"
                                  placeholder="{{ __('Enter your email', 'sage') }}"
                                >
                            </div>
                        </div>
                    </div>
                    <div class="mb-4 w-100">
                        <div class="field-wrap field-wrap--checkbox">
                            <input
                              type="checkbox"
                              name="signup-acceptance"
                              id="signup-acceptance"
                              required
                            >
                            </input>
                            <div>
                                @if (!empty($footer_fields['newsletter_terms']))
                                    {!! $footer_fields['newsletter_terms'] !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div
                      id="mce-responses"
                      class="clear"
                    >
                        <div
                          class="response"
                          id="mce-error-response"
                          style="display:none"
                        >
                        </div>
                        <div
                          class="response"
                          id="mce-success-response"
                          style="display:none"
                        >
                        </div>
                    </div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div
                      style="position: absolute; left: -5000px;"
                      aria-hidden="true"
                    >
                        <input
                          type="text"
                          name="b_b4bd9c2a2d0db35629fa2e231_6147592da2"
                          tabindex="-1"
                          value=""
                        >
                    </div>
                    <div class="field-wrap field-wrap--submit">
                        <input
                          type="submit"
                          value="Join Newsletter"
                          name="subscribe"
                          id="mc-embedded-subscribe"
                          class="btn btn--outline"
                          disabled
                        >
                    </div>
                </div>
            </form>
        </div>
        <!--End mc_embed_signup-->
    </div>
</div>
