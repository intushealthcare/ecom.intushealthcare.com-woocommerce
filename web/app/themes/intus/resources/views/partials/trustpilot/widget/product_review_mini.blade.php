<!-- TrustBox widget - Product Mini -->
<div class="trustpilot-widget-container trustpilot-widget-container__product_mini_review d-flex mb-4">
    <div class="trustpilot-widget"
         id="trustpilot-widget__product_mini_review"
         data-locale="en-GB"
         data-template-id="{{ $config['widget_id']  }}"
         data-businessunit-id="{{ $config['id'] }}"
         data-style-height="24px"
         data-style-width="180px"
         data-theme="light"
         data-sku="{{ $config['product_id'] }}"
         data-star-color="#fc9894"
         data-font-family="Montserrat"
         data-text-color="#30357c"
         data-no-reviews="hide"
         data-scroll-to-list="false"
         data-style-alignment="left">
        <a href="{{ $config['url']  }}" target="_blank" rel="noopener" style="color: transparent">@php _e('Trustpilot', 'intus') @endphp</a>
    </div>
    <a
        href="#reviews"
        class="trustpilot woocommerce-review-link text-underline"
        rel="nofollow"
    >@php _e('View reviews', 'intus') @endphp</a>
</div>





<!-- End TrustBox widget -->
