<!-- TrustBox widget - Product Reviews -->
<div class="trustpilot-widget-container trustpilot-widget-container__product_review" style="width: 100%; height: 300px;">
    <div class="trustpilot-widget"
         data-locale="en-GB"
         data-template-id="{{ $config['widget_id']  }}"
         data-businessunit-id="{{ $config['id'] }}"
         data-style-height="300px"
         data-style-width="100%"
         data-theme="light"
         data-sku="{{ $config['product_id'] }}"
         data-review-languages="en"
         data-no-reviews="show"
         data-fullwidth="true"
         data-text-color="#30357c"
         data-star-color="#fc9894"
         data-link-color="#fc9894">
        <a href="{{ $config['url']  }}" target="_blank" rel="noopener" style="color: white">@php _e('Trustpilot', 'intus') @endphp</a>
    </div>
</div>
<!-- End TrustBox widget -->

