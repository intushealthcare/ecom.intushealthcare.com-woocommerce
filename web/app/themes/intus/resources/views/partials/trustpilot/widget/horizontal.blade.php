<!-- TrustBox widget - Horizontal -->
<div class="trustpilot-widget-container trustpilot-widget-container__horizontal" style="margin: 0.5rem 0; width: 100%; min-height:28px;">
    <div class="trustpilot-widget" data-locale="en-GB" data-template-id="{{ $config['widget_id']  }}" data-businessunit-id="{{ $config['id'] }}" data-style-height="28px" data-style-width="100%" data-theme="light" data-font-family="Montserrat" data-text-color="#30357c">
        <a href="{{ $config['url']  }}" target="_blank" rel="noopener" style="display: block; margin: 0 auto; height: 28px; color: purple; text-align:center">@php _e('Trustpilot', 'intus') @endphp</a>
    </div>
</div>
<!-- End TrustBox widget -->
