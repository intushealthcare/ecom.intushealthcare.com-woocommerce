@php
    $button = $button ?? false;
    $class = $class ?? '';
	$classColour = ( isset($button_style) && ( $button_style ) ) ? ' btn--' .$button_style : '' ;
    $classOutline = ( isset($button_outline) && ( $button_outline ) ) ? ' btn--outline' : 'no-button-outline' ;
@endphp

@if (!empty($button))
    <a
        href="{{ $button['url'] }}"
        class="btn {{ $class }} {{ $classColour  }} {{ $classOutline }}"
        target="{{ (!empty($button['target'])) ? $button['target'] : '_self' }}"
    >
        {!! $button['title'] !!}
    </a>
@endif
