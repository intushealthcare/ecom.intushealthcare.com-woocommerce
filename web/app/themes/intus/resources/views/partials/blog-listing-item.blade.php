@php
    $image_id = get_post_thumbnail_id();
    $image  = wp_get_attachment_image( $image_id, 'medium', false, ['class' => 'stretch-container img-cover']);
@endphp

<article
    class="blog-listing"
    itemscope
    itemtype="http://schema.org/NewsArticle"
>
    {{-- Image --}}
    @if (!empty($image))
        <a
            class="blog-listing__image"
            href="{{ get_permalink() }}"
            itemscope
            itemprop="image"
            itemtype="https://schema.org/ImageObject"
        >
            {!! $image !!}
        </a>
    @endif

    {{-- Content --}}
    <div class="blog-listing__content py-5">
        <time
            datetime="{{ get_the_date('c') }}"
            itemprop="datePublished"
            class="p color-mid-grey p-sm d-block mb-3"
        >
            @php the_date() @endphp
        </time>
        <h5 class="blog-listing__content-title color-secondary pb-4">
            <a
                href="{{ get_permalink() }}"
                class="color-secondary"
                itemprop="headline"
            >
                @php the_title() @endphp
            </a>
        </h5>
        <a
            class="color-primary text-bold"
            href="{{ get_permalink() }}"
        >
            {!! __('Read more', 'sage') !!}
        </a>
    </div>

</article>
