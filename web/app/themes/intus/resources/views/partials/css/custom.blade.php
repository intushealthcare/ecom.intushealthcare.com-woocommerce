<?php
// Add custom CSS variables from the ACF Colour options that are defined

/**
 * Increases or decreases the brightness of a color by a percentage of the current brightness.
 *
 * @param   string  $hexCode        Supported formats: `#FFF`, `#FFFFFF`, `FFF`, `FFFFFF`
 * @param   float   $adjustPercent  A number between -1 and 1. E.g. 0.3 = 30% lighter; -0.4 = 40% darker.
 *
 * @return  string
 *
 * @author  maliayas
 */
function adjustBrightness($hexCode, $adjustPercent) {
    $hexCode = ltrim($hexCode, '#');

    if (strlen($hexCode) == 3) {
        $hexCode = $hexCode[0] . $hexCode[0] . $hexCode[1] . $hexCode[1] . $hexCode[2] . $hexCode[2];
    }

    $hexCode = array_map('hexdec', str_split($hexCode, 2));

    foreach ($hexCode as & $color) {
        $adjustableLimit = $adjustPercent < 0 ? $color : 255 - $color;
        $adjustAmount = ceil($adjustableLimit * $adjustPercent);
        $newColour = ( int ) ( $color + $adjustAmount );
        $color = str_pad(dechex($newColour), 2, '0', STR_PAD_LEFT);
    }

    return '#' . implode($hexCode);
}
?>
<style id="theme-styles-inline-css" type="text/css">
    :root {
    <?php
        if( !empty( $theme_colors ) ) {
             foreach ( $theme_colors as $theme_color ) {
				$key = str_replace( "_", "-", $theme_color['slug']);
                echo "--{$key}: {$theme_color['color']};" . PHP_EOL;
                echo ".has-text-color.has-{$key}-color { color: var(--{$key}) !important };" . PHP_EOL;
                echo ".has-background.has-{$key}-background-color { background-color: var(--{$key}) !important };" . PHP_EOL;
                echo ".has-border.has-{$key}-border-color { background-color: var(--{$key}) !important };" . PHP_EOL;
            }
        }
     ?>
    <?php
    if( !empty($theme_typography ) ) {
        foreach ( $theme_typography as $key => $theme_color ) {
            echo "--{$key}: {$theme_color};" . PHP_EOL;
        }
    }
    ?>
    }
</style>
