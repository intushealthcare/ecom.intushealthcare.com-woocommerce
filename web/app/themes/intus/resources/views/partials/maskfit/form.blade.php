 @php
 /* @var $product WC_Product */
 @endphp

 <div class="product-maskfit-enquiry">
     <div class="product-maskfit-enquiry-messages d-none"></div>
     <div class="product-maskfit-enquiry-form-container">

         <div class="d-flex flex-column flex-md-row">
             <div class="mb-4 mb-md-0 mr-md-5">
                 <i class="far fa-3x fa-info-circle"></i>
             </div>
             <div class="pb-4">
                 <h2><?php echo __( 'Need help choosing a size or mask?', 'sage' ); ?></h2>
             </div>
         </div>

         <div class="pb-4">
             <p><?php echo __( 'Our online tool can help! Answer a few quick questions about your CPAP prescription, then use your phone\'s camera or webcam to scan your face.', 'sage' ); ?></p>
             <p><?php echo __( 'You\'ll receive personalised recommendations for suitable sizes of this mask, or suggestions for other suitable masks and size.', 'sage' ); ?></p>
             <p><?php echo __( 'Simply enter your email or mobile number and we\'ll send you a link', 'sage' ); ?></p>
         </div>

         <form id="maskfit-form" class="product-maskfit-form">
             <div class="mb-3">
                 <label for="exampleInputEmail1" class="form-label"><?php echo __( 'Your email address', 'sage' ); ?></label>
                 <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
             </div>
             <div class="mb-3">
                 <label for="exampleInputPassword1" class="form-label"><?php echo __( 'Your mobile', 'sage' ); ?></label>
                 <input type="phone" name="phone" class="form-control" id="exampleInputPassword1">
             </div>
             <div class="product-maskfit-form-actions">
                 <input type="hidden" name="sku" value="<?= $product->get_sku() ?>"/>
                 <button type="submit" id="maskfit-form-submit" class="btn btn-primary btn-maskfit-form-submit"><?php echo __( 'Let\'s Go!', 'sage' ); ?></button>
             </div>
         </form>
     </div>
 </div>


