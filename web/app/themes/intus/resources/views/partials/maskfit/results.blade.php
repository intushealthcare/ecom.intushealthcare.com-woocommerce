@if ( count($products) > 0 )
    {{-- Maskfit Products --}}

    <div class="container psy-1 product-listing-container">

        <div id="products-container" class="products-container">

            <ul class="maskfit-products products columns-4">
                @foreach ($products as $product)
                    <li {{ wc_product_class('maskfit-product', $product) }}>
                        @php
                            /* @var $product WC_Product */
                          $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_parent_id() ), 'woocommerce_thumbnail' );
                          $srcset = wp_get_attachment_image_srcset( get_post_thumbnail_id( $product->get_parent_id() ) );
                        @endphp
                        <a href="{{ App\woo_get_product_direct_link( $product ) }}">
                            <img src="{{ $image[0] }}" width="{{ $image[1]  }}" height="{{ $image[2] }}" srcset="{{esc_attr( $srcset ) }}" alt="">
                        </a>
                        <h3 class="product__title p pt-4 mb-2 color-secondary text-bold">
                            <a href="{{ App\woo_get_product_direct_link( $product ) }}">{{ $product->get_name() }}</a>
                        </h3>
                        <span class="price"><span class="woocommerce-Price-amount amount"><bdi><span
                                        class="woocommerce-Price-currencySymbol">£</span>{{ number_format( $product->get_price(), 2) }}</bdi></span></span>
                    </li>
                @endforeach
            </ul><!-- .maskfit-products -->

        </div><!-- #products-container -->

    </div><!-- .product-listing-container -->

@endif
