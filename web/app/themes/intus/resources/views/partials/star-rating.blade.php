@php
    $star_review = $star_review ?? false;

    if (!$star_review) {
        return;
    }

    $stars_total = 5;
    $stars_whole = (int) floor($star_review);
    $stars_partial = false;
@endphp

<ul class="star-rating-list">
    @for ($i = 1; $i <= $stars_total; $i++)
        @if ($i <= $stars_whole)
            <li>
                <i class="fas fa-star"></i>
            </li>
        @elseif ($stars_whole !== $star_review && !$stars_partial)
            <li>
                <i class="fas fa-star-half-alt"></i>
            </li>
            @php $stars_partial = true @endphp
        @else
            <li>
                <i class="fal fa-star"></i>
            </li>
        @endif
    @endfor
</ul>
