@php
    $class = $class ?? ''; // String of classes
    $gif = $gif ?? false; // boolean value
    $image = $image ?? false; // ACF image array
    $mobile_image = $mobile ?? false; // ACF image array
    $picture = (!$image) ? App::wpImageSizes() : App::acfImageSizes($image);
    $picture_mob = (!$mobile_image) ? $picture : App::acfImageSizes($mobile_image);
	$srcset = [];
    if( !empty( $picture['full'] ) ) {
		$srcset[] = $picture['full'] . " " .$picture['full-width'] . "w";
    }
    if( !empty( $picture['md'] ) ) {
		$srcset[] = $srcset[] = $picture['md'] . " " .$picture['md-width'] . "w";
    }
	if( !empty( $picture['sm'] ) ) {
		$srcset[] = $srcset[] = $picture['sm'] . " " .$picture['sm-width'] . "w";
    }
	$srcset = implode( ', ', $srcset);
@endphp

<picture class="{{ (!empty($class)) ? $class : '' }}">
    {{-- Banner Large --}}
    @if ( !empty($picture['full']) && ( (int) $picture['full-width'] >= 1900 ) )
        <source
            srcset="{{ (!$gif) ? $picture['full'] : $picture['full'] }}"
            media="(min-width: 1900px)"
        >
    @endif
    {{-- Large --}}
    @if ( !empty($picture['lg']) && ( (int) $picture['lg-width'] >= 1200 ) )
        <source
            srcset="{{ (!$gif) ? $picture['lg'] : $picture['full'] }}"
            media="(min-width: 768px)"
        >
    @endif
    {{-- Medium --}}
    <img
        srcset="{{ (!$gif) ? $picture_mob['md'] : $picture_mob['full'] }}"
        draggable="false"
        class="img-fluid object-fit-item"
        alt="{{ (!empty($picture_mob['alt'])) ? $picture_mob['alt'] : '' }}"
    >
</picture>
