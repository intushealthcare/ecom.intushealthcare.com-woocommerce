<div class="store-notice-header">
    <div class="container text-center">
        <p class="mb-0 text-bold">{!! $header_fields['store_notice']['text'] !!}</p>
    </div>
</div>
