{{--
Template Name: Contact Template
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts())
        @php the_post() @endphp
        @include('pages.content-contact')
    @endwhile
@endsection
