<!doctype html>
<html {!! get_language_attributes() !!}>
@include('global.head')
<body @php body_class() @endphp>
    @php do_action('after_body') @endphp
    @php do_action('get_header') @endphp
    @include('global.modal')
    @include('global.offcanvas')
    @include('global.header')
    <div class="wrap" role="document">
        <div class="content">
            @yield('content')
        </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('global.footer')
    @php wp_footer() @endphp
    @if (!$is_woo_page)
        @php do_action('mailchimp_validate') @endphp
    @endif
</body>
</html>
