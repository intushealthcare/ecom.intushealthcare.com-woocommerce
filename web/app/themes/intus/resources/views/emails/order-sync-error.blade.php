@extends('emails.layouts.default')

@section('content')
    <p>WooCommerce order number #{{ $orderId }} has failed to synchronise to Magento.</p>
    @if (!empty($errorMessage))
        <p>The following error message was reported:</p>
        <p><em>{{ $errorMessage }}</em></p>
    @endif
    <p>Please create the order manually in Magento.</p>
@endsection
