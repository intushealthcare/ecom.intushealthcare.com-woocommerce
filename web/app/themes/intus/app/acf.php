<?php

namespace App;

add_action('after_setup_theme', function () {
    add_theme_support('align-wide');
    add_theme_support('appearance-tools');
    add_theme_support('border');
    add_theme_support('custom-line-height');
    add_theme_support('custom-spacing');
    add_theme_support('custom-units', 'rem', 'vw', 'wh');
    add_theme_support('editor-color-palette', gutenberg_color_palette());
    add_theme_support('responsive-embeds');
    add_theme_support('wp-block-styles');
}, 30);

/**
 * Define custom colours for the ACF colour pickers
 * https://support.advancedcustomfields.com/forums/topic/customise-color-picker-swatches/
 * https://www.advancedcustomfields.com/resources/adding-custom-javascript-fields/
 */
add_action('acf/input/admin_head', function () {
    // Adds client custom colors to WYSIWYG editor and ACF color picker.
    $colors = array_column(gutenberg_color_palette(), 'color');
    $jsColors = json_encode($colors);
    ?>
    <script>
        acf.add_filter('color_picker_args', function (args, $field) {
            args.palettes = <?= $jsColors ?>;
            return args;
        });
    </script>
    <?php
});

/**
 * Disables all default gutenberg blocks and allows only our custom ACF ones
 * NOTE: This may need to be customised further to only apply certain blocks on certain pages.
 * e.g. only allow these blocks on the flex template, only allow shortcode block on woo templates etc
 *
 * @ref https://rudrastyh.com/gutenberg/remove-default-blocks.html
 *
 * @return html
 */

add_filter('allowed_block_types_all', function ($allowed_blocks) {

    $registered_blocks = \WP_Block_Type_Registry::get_instance()->get_all_registered();
    $allowed_gutenberg_blocks = [
        'core/group',
        'core/column',
        'core/columns',
        'core/embed',
        'core/button',
        'core/buttons',
        'core/quote',
        'core/heading',
        'core/paragraph',
        'core/list',
        'core/shortcode',
        'core/code',
        'core/html',
        'core/table',
        'core/video',
        'core/cover',
        'woocommerce/handpicked-products',
        'woocommerce/featured-category',
        'woocommerce/featured-tag',
        'woocommerce/product-add-to-cart'
    ];

    if (!empty($GLOBALS['post_type']) && $GLOBALS['post_type'] === 'post') {
        return $allowed_blocks;
    }

    $template_base_name = basename(get_page_template());

    if ($registered_blocks && (($template_base_name == 'template-flexible.blade.php') || ($template_base_name == 'page.blade.php'))) {
        return array_keys(array_filter($registered_blocks, function ($name) use ($allowed_gutenberg_blocks) {
            if (in_array($name, $allowed_gutenberg_blocks)) {
                return true;
            }
            return strpos($name, 'acf/') !== false;
        }, ARRAY_FILTER_USE_KEY));
    } else {
        return [];
    }
});


/**
 * We want to display at least some Gutenberg blocks wrapped with the normal surrounding constraining <div>s
 */
add_filter('render_block', function ($block_content, $block) {

    if (!empty($GLOBALS['post_type']) && $GLOBALS['post_type'] === 'post') {
        return $block_content;
    }
    if (basename(get_page_template()) == 'template-flexible.blade.php' || basename(get_page_template()) == 'page.blade.php') {

        $wrappableBlocks = ['core/group'];

        if (in_array($block['blockName'], $wrappableBlocks)) {

            $classCollection = [];
            if( isset( $block['attrs']['align'] ) ){
                $classCollection[] = "align{$block['attrs']['align']}";
                $classCollection[] = "container-fluid";
            }else{
                $classCollection[] = "container";
            }

            if( isset( $block['attrs']['backgroundColor'] ) ){
                $classCollection[] = "has-{$block['attrs']['backgroundColor']}-background-color";
                $classCollection[] = "has-background";
            }

            if( isset( $block['attrs']['textColor'] ) ){
                $classCollection[] = "has-{$block['attrs']['textColor']}-color";
                $classCollection[] = "has-text-color";
            }

            $wrapperBlockClasses = implode(" ", $classCollection);
            $block_content = sprintf('<div class="flex-row full-width %1$s"><div class="position-relative z-index-2"><div class="row justify-content-lg-center"><div class="col-12 col-lg-11 col-xl-10">%2$s</div></div></div></div>', $wrapperBlockClasses, $block_content);
        }

        $wrappableWooBlocks = [
            'woocommerce/handpicked-products',
            'woocommerce/featured-category',
            'woocommerce/featured-tag',
            'woocommerce/product-add-to-cart'
        ];

        if (in_array($block['blockName'], $wrappableWooBlocks)) {
            $block_content = sprintf('<div class="flex-row full-width"><div class="container position-relative z-index-2"><div class="row justify-content-lg-center"><div class="col-12">%1$s</div></div></div></div>', $block_content);
        }
    }
    return $block_content;
}, 10, 2);
