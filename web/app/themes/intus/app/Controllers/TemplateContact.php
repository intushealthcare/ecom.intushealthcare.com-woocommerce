<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateContact extends Controller
{
    public function mainContent()
    {
        return get_field('main_content');
    }

    public function sidebarContent()
    {
        return get_field('sidebar_content');
    }
}
