<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Author extends Controller {

    public function postAuthor()
    {
        global $authordata;
        $user_data      = get_userdata( $authordata->ID );
        $nickname       = get_user_meta( $authordata->ID, 'nickname', true );
        $description    = get_user_meta( $authordata->ID, 'description', true );
        $linkedin       = get_field( 'author_linkedin', 'user_' . $authordata->ID );
        $jobtitle       = get_field( 'author_jobtitle', 'user_' . $authordata->ID );
        $profile_image  = get_field( 'profile_image', 'user_' . $authordata->ID );
        $fallback_image = get_field( 'user_profile_image', 'option' );

        if ( ! empty( $profile_image ) )
        {
            $image = $profile_image;
        } else
        {
            $image = $fallback_image;
        }

        return [
            'user_data'   => $user_data,
            'image'       => $image,
            'description' => $description,
            'linkedin'    => $linkedin,
            'jobtitle'    => $jobtitle,
            'nickname'    => $nickname,
        ];
    }

}
