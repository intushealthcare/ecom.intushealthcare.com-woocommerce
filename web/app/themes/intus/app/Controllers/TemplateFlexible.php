<?php

namespace App\Controllers;

use WP_Query;
use Sober\Controller\Controller;

class TemplateFlexible extends Controller {

    public static function options() {
        $classes = [];
        $sides   = [ 'top' => 't', 'bottom' => 'b', 'left' => 'l', 'right' => 'r' ];

        $options = get_field( 'options' );

        foreach ( $sides as $side => $value ) {
            if ( !empty( $options["padding_{$side}"] ) ) {
                $classes[] = "ps{$value}-" . $options["padding_{$side}"];
            }
        }

        $options = get_field( 'options_margin' );
        foreach ( $sides as $side => $value ) {
            if ( !empty( $options["margin_{$side}"] ) ) {
                $classes[] = "ms{$value}-" . $options["margin_{$side}"];
            }
        }

//        $classes[] = self::border_radius_styles();
        $classes[] = self::list_icons_styles();
        $classes[] = self::responsive_hide_styles();

        return ( !empty( $classes ) ) ? implode( ' ', $classes ) : '';
    }

    public static function list_icons_styles() {
        $field_key  = 'options_list_icons';
        $class_base = 'container-item-list-icons';
        $classes    = [];
        $options    = get_field( $field_key );
        if ( !empty( $options ) ) {
            $classes[] = "{$class_base} {$class_base}__{$options}";
        }
        return ( !empty( $classes ) ) ? implode( ' ', $classes ) : '';
    }

    public static function border_radius_styles() {
        $field_key  = 'options_border_radius';
        $class_base = 'container-item-border-radius';
        $classes    = [];
        $options    = get_field( $field_key );
        if ( !empty( $options ) ) {
            $classes[] = "{$class_base} {$class_base}__{$options}";
        }
        return ( !empty( $classes ) ) ? implode( ' ', $classes ) : '';
    }

    public static function width_styles() {
        $field_key  = 'options_width';
        $class_base = 'container-fluid';
        $classWidth = 'container';

        $width = get_field( $field_key );
        if ( !empty( $width ) ) {
            if ( $width == 'fluid' ) {
                $classWidth = $class_base;
            }
            if ( $width == 'wide' ) {
                $classWidth = "{$class_base} {$class_base}__wide";
            }
        }
        return $classWidth;
    }

    public static function backgrounds() {
        $style       = "";
        $styles      = [];
        $backgrounds = get_field( 'options_background' );

        if ( !empty( $backgrounds['bg_colour'] ) ) {
            $styles[] = "background-color: {$backgrounds['bg_colour']};";
        }

        if ( !empty( $backgrounds['bg_image'] ) ) {
            $bg_image = $backgrounds['bg_image'];
            $styles[] = "background-image: url('{$bg_image['url']}');";
            $styles[] = "background-repeat: no-repeat;";

            if ( !empty( $backgrounds['bg_position'] ) ) {
                $bg_position = $backgrounds['bg_position'];
                $styles[]    = "background-position: {$bg_position};";
            }

            if ( !empty( $backgrounds['bg_size'] ) ) {
                $bg_size  = $backgrounds['bg_size'];
                $styles[] = "background-size: {$bg_size};";
            }
            else {
                $styles[] = "background-size: cover;";
            }

        }

        if ( !empty( $styles ) ) {
            $styles = implode( ' ', $styles );
            $style  = "style=\"{$styles}\"";
        }
        return $style;
    }


    public static function has_column_background( $data  )
    {
        $requires_padding = false;
        if( strlen( $data['bg_colour'] ) > 0 ){
            return true;
        }
        return $requires_padding;
    }


    /**
     * @param $data
     *
     * @return string
     */
    public static function column_inline_styles( $data ) {
        $style  = "";
        $styles = [];

        if ( !empty( $data['bg_colour'] ) ) {
            $styles[] = "background-color: {$data['bg_colour']};";
        }

        if ( !empty( $data['text_colour'] ) ) {
            $styles[] = "color: {$data['text_colour']};";
        }

        if ( !empty( $data['column_width'] ) ) {
            $styles[] = "flex-basis: {$data['column_width']};";
            $styles[] = "flex-grow: unset;";
        }

        if ( !empty( $data['column_width_mobile'] ) ) {
//            $styles[] = "width: {$data['column_width_mobile']} !important;";
        }

        if ( !empty( $styles ) ) {
            $styles = implode( ' ', $styles );
            $style  = "style='{$styles}'";
        }
        return $style;
    }

    public static function columns_classes( $per_row ) {
        $column_classes = [ 'mb-5' ];

        if ( $per_row === 1 ) {
            $column_classes[] = 'col-12 mb-lg-5';
        }
        elseif ( $per_row === 3 ) {
            $column_classes[] = 'col-12 col-md-6 col-lg-4';
        }
        elseif ( $per_row === 4 ) {
            $column_classes[] = 'col-12 col-md-6 col-lg-3';
        }
        elseif ( $per_row === 5 ) {
            $column_classes[] = 'col-12 col-md-6 col-lg-4 col-xl-1_5';
        }
        elseif ( $per_row === 6 ) {
            $column_classes[] = 'col-12 col-md-6 col-lg-4 col-xxl-2';
        }
        else {
            $column_classes[] = 'col-12 col-md-6 mb-lg-5';
        }
        return implode( ' ', $column_classes );
    }

    public static function background_classes( $column ) {
        $style = $column['bg_colour'];
        return "has-primary-background-color has-background";
    }

    public static function responsive_hide_styles() {
        $hide_classes = "";
        $breakpoints  = [
            'xs', 'sm', 'md', 'lg', 'xl', 'xxl', 'xxl',
        ];
        $hide         = get_field( 'options_responsive_hide' );
        if ( !empty( $hide ) ) {
            $hide_size = str_replace( 'show-', '', $hide );
            if ( in_array( $hide_size, $breakpoints ) ) {
                $position = array_search( $hide_size, $breakpoints );
                if ( strpos( $hide, "show-" ) === false ) {
                    $hide_classes = "d-block d-" . $breakpoints[$position] . "-none";
                }
                else {
                    $hide_classes = "d-none d-" . $breakpoints[$position + 1] . "-block";
                }
            }
        }
        return $hide_classes;
    }

    public static function latestNews() {

        $query = new WP_Query( [
            'post_type'      => 'post',
            'posts_per_page' => 3,
        ] );

        return $query;

    }

    public static function flip() {
        return [
            'flip' => get_field( 'flip_column' ),
        ];
    }

    public static function column_classes() {
        return get_field( 'column_width' );
    }


    public static function alternating() {
        $data = [
            'video'          => get_field( 'video' ),
            'image_type'     => get_field( 'image_type' ),
            'image_slides'   => get_field( 'image_slides' ),
            'image'          => get_field( 'image' ),
            'title'          => get_field( 'title' ),
            'content'        => get_field( 'content' ),
            'button'         => get_field( 'button' ),
            'icon'           => get_field( 'add_icon' ),
            'bold_title'     => get_field( 'bold_title' ),
            'add_extra'      => get_field( 'add_extra_content' ),
            'content_type'   => get_field( 'content_type' ),
            'list'           => get_field( 'list' ),
            'list_two'       => get_field( 'list_two' ),
            'list_title'     => get_field( 'list_title' ),
            'list_two_title' => get_field( 'list_two_title' ),
        ];
        $flip = self::flip();
        return array_merge( $data, $flip );
    }


    /**
     * @return array
     */
    public static function categoryColumns() {
        return [
            'columns' => get_field( 'columns' ),
        ];
    }


    public static function heroimagebuttonColumns() {
        $data              = [
            'columns' => get_field( 'columns' ),
            'per_row' => count( get_field( 'columns' ) ),
        ];
        $data['css_class'] = self::columns_classes( $data['per_row'] );
        return $data;
    }

    public static function fullWidth() {
        return [
            'title'          => get_field( 'title' ),
            'title_tag'      => get_field( 'title_tag' ),
            'icon'           => get_field( 'add_icon' ),
            'content'        => get_field( 'content' ),
            'buttons'        => get_field( 'buttons' ),
            'style'          => get_field( 'style' ),
            'alignment'      => get_field( 'alignment' ),
            'button_style'   => get_field( 'button_style' ),
            'button_outline' => get_field( 'button_outline' ),
        ];
    }


    public static function single_column_row() {
        $data                   = [
            'bg_colour'   => get_field( 'bg_colour' ),
            'text_colour' => get_field( 'text_colour' ),
        ];
        $data['inline_styles']  = self::column_inline_styles( $data );
        $data['column_background'] = self::has_column_background( $data );
        $data['column_classes'] = self::column_classes();
        $full_width             = self::fullWidth();
        $flip                   = self::flip();
        return array_merge( $full_width, $flip, $data );
    }


    public static function flexibleRow() {
        $data              = [
            'columns' => get_field( 'columns' ),
            'per_row' => count( get_field( 'columns' ) ),
        ];
        $data['css_class'] = self::columns_classes( $data['per_row'] );
        return $data;
    }

    public static function featuredCategories() {
        return [
            'title'      => get_field( 'title' ),
            'content'    => get_field( 'content' ),
            'categories' => get_field( 'categories' ),
            'style'      => get_field( 'style' ),
        ];
    }

    public static function carousel() {
        return [
            'add_content'      => get_field( 'add_content' ),
            'content'          => get_field( 'content' ),
            'style'            => get_field( 'style' ),
            'slides_to_scroll' => get_field( 'slides_to_scroll' ),
            'slides'           => get_field( 'slides' ),
            'bg_colour'        => get_field( 'bg_colour' ),
        ];
    }

    public static function banner() {
        return [
            'slides' => get_field( 'slides' ),
        ];
    }

    public static function imageColumns() {
        return [
            'images' => get_field( 'images' ),
        ];
    }

    public static function keyPointHeader() {
        return [
            'title'     => get_field( 'title' ),
            'icon'      => get_field( 'add_icon' ),
            'content'   => get_field( 'content' ),
            'key_title' => get_field( 'key_point_title' ),
            'key_desc'  => get_field( 'key_point_description' ),
        ];
    }

    public static function steps() {
        return [
            'title' => get_field( 'title' ),
            'icon'  => get_field( 'add_icon' ),
            'steps' => get_field( 'steps' ),
        ];
    }

    public static function statistics() {
        return [
            'title'      => get_field( 'title' ),
            'icon'       => get_field( 'add_icon' ),
            'statistics' => get_field( 'statistics' ),
        ];
    }

    public static function featuredProduct() {
        return [
            'product'  => get_field( 'product' ),
            'btn_text' => get_field( 'button_text' ),
        ];
    }

    public static function iconRowList() {
        return get_field( 'icons' );
    }

    public static function featuredFaqs() {
        return [
            'title' => get_field( 'title' ),
            'faqs'  => get_field( 'faqs' ),
        ];
    }

    public static function flexImage() {
        return [
            'image' => get_field( 'image' ),
            'link'  => get_field( 'link' ),
        ];
    }
}
