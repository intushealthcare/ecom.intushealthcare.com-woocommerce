<?php

namespace App\Controllers;

use WP_Query;
use Sober\Controller\Controller;

class TemplateFaqs extends Controller
{
    public function faqFields()
    {
        return [
            'title' => get_field('main_title'),
            'image' => get_field('banner_image'),
            'featured_questions' => get_field('featured_questions'),
        ];
    }

    public static function faqCategories($parent = 0)
    {

        $args = [
            'taxonomy' => 'faq-type',
            'hide_empty' => false,
            'parent' => $parent
        ];

        $terms = get_terms($args);

        return $terms;
    }

    public static function getFaqPosts($term)
    {

        if (empty($term)) {
            return false;
        }

        $args = [
            'post_type' => 'faqs',
            'posts_per_page' => -1,
            'tax_query' => [
                [
                    'taxonomy' => 'faq-type',
                    'field' => 'id',
                    'terms' => [$term->term_id],
                ],
            ]
        ];

        $the_query = new WP_Query($args);

        return $the_query;
    }
}
