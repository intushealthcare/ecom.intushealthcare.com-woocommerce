<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SinglePost extends Controller {

    public function postImage()
    {
        $banner        = '';
        $image         = get_field( 'image' );
        $feat_image_id = get_post_thumbnail_id();
        $alt           = null;

        if ( ! empty( $image ) )
        {

            // ACF image
            $banner = $image[ 'sizes' ][ 'large' ];
            $alt    = $image[ 'alt' ];

        } elseif ( ! empty( $feat_image_id ) )
        {
            // Featured image
            $feat_image     = wp_get_attachment_image_src( $feat_image_id, 'large' );
            $feat_image_meta = wp_get_attachment_metadata( $feat_image_id );
            $feat_image_alt = get_post_meta( $feat_image_id, '_wp_attachment_image_alt', true );

            $banner = $feat_image[ 0 ];
            $alt    = $feat_image_alt;
        }

        return [
            'image' => $banner,
            'alt'   => $alt,
            'height'   => $feat_image_meta['sizes']['large']['width'],
            'width'   => $feat_image_meta['sizes']['large']['height'],
        ];
    }

    public function postAuthor()
    {
        global $post;

        $user_data      = get_userdata( $post->post_author );
        $hide_author    = get_field( 'hide_author' );
        $profile_image  = get_field( 'profile_image', 'user_' . $post->post_author );
        $fallback_image = get_field( 'user_profile_image', 'option' );

        if ( ! empty( $profile_image ) )
        {
            $image = $profile_image;
        } else
        {
            $image = $fallback_image;
        }

        return [
            'user_data' => $user_data,
            'image'     => $image,
            'hide'      => $hide_author,
        ];
    }


    public function postReviewer()
    {
        global $post;
        $reviewer = get_field( 'reviewer', $post->ID );
        if ( $reviewer )
        {
            $user_data      = get_userdata( $reviewer->ID );
            $linkedin       = get_field( 'author_linkedin', 'user_' . $reviewer->ID );
            $jobtitle       = get_field( 'author_jobtitle', 'user_' . $reviewer->ID );
            $profile_image  = get_field( 'profile_image', 'user_' . $reviewer->ID );
            $fallback_image = get_field( 'user_profile_image', 'option' );

            if ( ! empty( $profile_image ) )
            {
                $image = $profile_image;
            } else
            {
                $image = $fallback_image;
            }

            return [
                'user_data' => $user_data,
                'image'     => $image,
                'linkedin'  => $linkedin,
                'jobtitle'  => $jobtitle,
            ];
        }

        return null;

    }


    public function reviewerNote()
    {
        $reviewer_note = get_field( 'blog_reviewer_note', 'option' );
        if ( $reviewer_note ){
            return [
                'note' => $reviewer_note,
            ];
        }
        return null;
    }

    public function postContent()
    {
        return apply_filters( 'the_content', get_the_content() );
    }
}
