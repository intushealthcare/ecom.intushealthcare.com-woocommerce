<?php

namespace App\Controllers;

use WP_Query;
use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public function isWooPage()
    {
        return (is_woocommerce() || is_shop() || is_product_category() || is_cart() || is_checkout()) ? true : false;
    }

    public function socialChannels()
    {
        return get_field('social_channels', 'option');
    }

    public function headerFields()
    {
        return [
            'logo' => get_field('header_logo', 'option'),
            'phone_text' => get_field('phone_text', 'option'),
            'phone_number' => get_field('phone_number', 'option'),
            'store_notice' => get_field('store_notice', 'option'),
        ];
    }

    public function globalFields()
    {
        return [
            'blog_page' => get_field('blog_page', 'option'),
            'add_global_modal' => get_field('add_global_modal', 'option'),
            'global_modal' => get_field('global_modal', 'option'),
        ];
    }

    public function footerFields()
    {
        return [
            'contact' => get_field('contact_information', 'option'),
            'copyright' => get_field('copyright', 'option'),
            'newsletter' => get_field('newsletter_content', 'option'),
            'newsletter_terms' => get_field('newsletter_terms', 'option'),
            'icon_list_content' => get_field('footer_icon_list_content', 'option'),
            'icon_list' => get_field('footer_icon_list', 'option'),
            'card_images' => get_field('card_images', 'option')
        ];
    }

    public static function wpImageSizes()
    {
        $thumb_id = get_post_thumbnail_id();
        $imgBanLg = wp_get_attachment_image_src($thumb_id, 'banner-large') ?? '';
        $imgBan = wp_get_attachment_image_src($thumb_id, 'banner') ?? '';
        $imgLg = wp_get_attachment_image_src($thumb_id, 'large') ?? '';
        $imgMd = wp_get_attachment_image_src($thumb_id, 'medium') ?? '';
        $imgSm = wp_get_attachment_image_src($thumb_id, 'thumbnail') ?? '';
        $imgFull = wp_get_attachment_image_src($thumb_id, 'full') ?? '';
        $imgAlt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true) ?? '';

        return [
            'full' => $imgFull[0],
            'full-width' => $imgFull[1],
            'bnl' => $imgBanLg[0],
            'bnl-width' => $imgBanLg[1],
            'bn' => $imgBan[0],
            'bn-width' => $imgBan[1],
            'lg' => $imgLg[0],
            'lg-width' => $imgLg[1],
            'md' => $imgMd[0],
            'md-width' => $imgMd[1],
            'sm' => $imgSm[0],
            'sm-width' => $imgSm[1],
            'alt' => $imgAlt,
        ];

    }

    public static function acfImageSizes($image)
    {
		if (!empty($image)){
			return [
                'full' => (!empty($image['url'])) ? $image['url'] : '' ,
                'full-width' => (!empty($image['width'])) ? $image['width'] : '' ,
                'bnl' => (!empty($image['sizes']['banner-large'])) ? $image['sizes']['banner-large'] : '',
                'bnl-width' => (!empty($image['sizes']['banner-large-width'])) ? $image['sizes']['banner-large-width'] : '',
				'bn' => (!empty($image['sizes']['banner'])) ? $image['sizes']['banner'] : '',
                'bn-width' => (!empty($image['sizes']['banner-width'])) ? $image['sizes']['banner-width'] : '',
				'lg' => (!empty($image['sizes']['large'])) ? $image['sizes']['large'] : '',
                'lg-width' => (!empty($image['sizes']['large-width'])) ? $image['sizes']['large-width'] : '',
                'md' => (!empty($image['sizes']['medium'])) ? $image['sizes']['medium'] : '',
                'md-width' => (!empty($image['sizes']['medium-width'])) ? $image['sizes']['medium-width'] : '',
                'alt' => (!empty($image['alt'])) ? $image['alt'] : '',
			];
		}
	}

    public static function ajaxPagination($query)
    {

        if(empty($query)) {
            return;
        }

        /** Stop execution if there's only 1 page */
        if( $query->max_num_pages <= 1 ) {
            return;
        }

        global $wp_query;

        $temp_query = $wp_query;
        $wp_query = NULL;
        $wp_query = $query;

        $output = '';
        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
        $max   = intval( $wp_query->max_num_pages );

        /** Add current page to the array */
        if ( $paged >= 1 )
            $links[] = $paged;

        /** Add the pages around the current page to the array */
        if ( $paged >= 3 ) {
            $links[] = $paged - 1;
            // $links[] = $paged - 2;
        }

        if ( ( $paged + 2 ) <= $max ) {
            // $links[] = $paged + 2;
            $links[] = $paged + 1;
        }

        $output .= '<div class="pagination"><ul>' . "\n";

        if ($paged > 1) {
            $output .= sprintf( '<li class="iterator"><a href="#" class="pagination-product" data-page="%s">%s</a></li>' . "\n", $paged - 1, '<i class="fal fa-lg fa-angle-left"></i>');
        }

        /** Link to first page, plus ellipses if necessary */
        if ( ! in_array( 1, $links ) ) {
            $class = 1 == $paged ? ' class="active"' : '';

            $output .= sprintf( '<li%s><a href="#" class="pagination-product" data-page="%s">%s</a></li>' . "\n", $class, '1', '1' );

            if ( ! in_array( 2, $links ) ) {
                $output .= '<li>…</li>';
            }
        }

        /** Link to current page, plus 2 pages in either direction if necessary */
        sort( $links );

        foreach ( (array) $links as $link ) {
            $class = $paged == $link ? ' class="active"' : '';
            $output .= sprintf( '<li%s><a href="#" class="pagination-product" data-page="%s">%s</a></li>' . "\n", $class, $link, $link );
        }

        /** Link to last page, plus ellipses if necessary */
        if ( ! in_array( $max, $links ) ) {
            if ( ! in_array( $max - 1, $links ) ) {
                $output .= '<li>…</li>' . "\n";
            }

            $class = $paged == $max ? ' class="active"' : '';
            $output .= sprintf( '<li%s><a href="#" class="pagination-product" data-page="%s">%s</a></li>' . "\n", $class, $max, $max );
        }

        if ($paged !== $max) {
            $output .= sprintf( '<li class="iterator"><a href="#" class="pagination-product" data-page="%s">%s</a></li>' . "\n", $paged + 1, '<i class="fal fa-lg fa-angle-right"></i>');
        }

        $output .= '</ul></div>' . "\n";

        $wp_query = NULL;
        $wp_query = $temp_query;

        return $output;

    }

    public function productFilters()
    {
        // Standard filters that apply to most products (fallback)
        $defaultFilters = [
            [
                'title' => 'Categories',
                'tax'   => 'product_cat',
            ],
            [
                'title' => 'Sale',
                'path' => 'sale',
            ]
        ];

        // Empty active filters array
        $activeFilters = [];

        // If taxonomy page get the object ID
        $tax = (is_tax()) ? get_queried_object(): '';

        // See if there are custom filters set on the page
        $filters = get_field('filters', $tax);

        if (!empty($filters)) {
            // Add filters to our active filters array
            foreach ($filters as $filter) {
                $type = ($filter['type'] == 'taxonomy') ? 'tax' : 'path';

                $activeFilters[] = [
                    'title' => $filter['title'],
                    $type => $filter['name'],
                ];
            }
        } else {
            // Fallback to default filters
            $activeFilters = $defaultFilters;
        }

        return $activeFilters;
    }

    public function categoryDisplayType()
    {
        return get_term_meta(get_queried_object_id(), 'display_type', true);
    }

    public static function getBlogPosts($category = false, $year = false, $month = false)
    {
        $args = [
            'post_type' => 'post',
            'posts_per_page' => get_option('posts_per_page'),
            'paged' => (get_query_var('paged')) ? absint(get_query_var('paged')) : 1,
        ];

        if (!empty($category)) {
            $args['tax_query'] = [
                [
                    'taxonomy' => 'category',
                    'field'    => 'slug',
                    'terms'    => $category->slug,
                ]
            ];
        }

        if (!empty($year)) {
            $args['year'] = $year;
        }

        if (!empty($month)) {
            $args['monthnum'] = $month;
        }

        $query = new WP_Query($args);

        return $query;
    }

    public static function getBlogCategories()
    {
        return get_terms('category', ['hide_empty' => true, 'exclude' => [1]]);
    }

    public static function getPostCategories()
    {
        return wp_get_post_terms(get_the_ID(), 'category', ['exclude' => [1]]);
    }

    public static function getBlogContent()
    {
        $blog_page = get_field('blog_page', 'option');

        if (empty($blog_page)) {
            return false;
        }

        return get_field('content', $blog_page);
    }

    public static function numericPagination($query) {

        if(empty($query)) {
            return;
        }

        /** Stop execution if there's only 1 page */
        if( $query->max_num_pages <= 1 ) {
            return;
        }

        global $wp_query;

        $temp_query = $wp_query;
        $wp_query = NULL;
        $wp_query = $query;

        $output = '';
        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
        $max   = intval( $wp_query->max_num_pages );

        /** Add current page to the array */
        if ( $paged >= 1 )
        $links[] = $paged;

        /** Add the pages around the current page to the array */
        if ( $paged >= 3 ) {
            $links[] = $paged - 1;
            $links[] = $paged - 2;
        }

        if ( ( $paged + 2 ) <= $max ) {
            $links[] = $paged + 2;
            $links[] = $paged + 1;
        }

        $output .= '<div class="pagination-blog"><ul>' . "\n";

        /** Previous Post Link */
        if ( get_previous_posts_link() ) {
            $output .= sprintf( '<li>%s</li>' . "\n", get_previous_posts_link('<i class="fal fa-chevron-left"></i>') );
        }

        /** Link to first page, plus ellipses if necessary */
        if ( ! in_array( 1, $links ) ) {
            $class = 1 == $paged ? ' class="active"' : '';

            $output .= sprintf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

            if ( ! in_array( 2, $links ) ) {
                $output .= '<li>…</li>';
            }
        }

        /** Link to current page, plus 2 pages in either direction if necessary */
        sort( $links );

        foreach ( (array) $links as $link ) {
            $class = $paged == $link ? ' class="active"' : '';
            $output .= sprintf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
        }

        /** Link to last page, plus ellipses if necessary */
        if ( ! in_array( $max, $links ) ) {
            if ( ! in_array( $max - 1, $links ) ) {
                $output .= '<li>…</li>' . "\n";
            }

            $class = $paged == $max ? ' class="active"' : '';
            $output .= sprintf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
        }

        /** Next Post Link */
        if ( get_next_posts_link() ) {
            $output .= sprintf( '<li>%s</li>' . "\n", get_next_posts_link('<i class="fal fa-chevron-right"></i>') );
        }

        $output .= '</ul></div>' . "\n";

        $wp_query = NULL;
        $wp_query = $temp_query;

        return $output;

    }
}
