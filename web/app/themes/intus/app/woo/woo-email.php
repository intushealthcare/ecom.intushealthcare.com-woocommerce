<?php

namespace App;

use WC_Email;

add_filter( 'woocommerce_email_headers', function ( $header, $email_id, $order ) {
    if ( ! in_array( $email_id, [ 'new_order', 'cancelled_order', 'failed_order' ], true ) && $reply_to_email = get_field( 'email_reply_to', 'option' ) ) {
        $reply_to_name = get_field( 'email_reply_name', 'option' );

        // Get the WC_Email instance Object
        $email = new WC_Email( $email_id );

        $header = "Content-Type: " . $email->get_content_type() . "\r\n";
        $header .= 'Reply-to: ' . ($reply_to_name ? $reply_to_name . ' ' : '') . '<' . $reply_to_email . ">\r\n";
    }

    return $header;
}, 20, 3 );
