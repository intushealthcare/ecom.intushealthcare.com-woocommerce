<?php
/**
 * Include tax lines in the order_item_totals
 */

 function moveKeyBefore($arr, $find, $move) {

    if (!isset($arr[$find], $arr[$move])) {
        return $arr;
    }

    $elem = [$move=>$arr[$move]];  // cache the element to be moved
    $start = array_splice($arr, 0, array_search($find, array_keys($arr)));
    unset($start[$move]);  // only important if $move is in $start
    return $start + $elem + $arr;

}

add_filter( 'woocommerce_get_order_item_totals', function($total_rows, $order_obj) {

    // Currency symbol
    $currency_symbol = get_woocommerce_currency_symbol( get_woocommerce_currency() );

    // Tax rates
    $tax_rates = $order_obj->get_tax_totals();

    // Total tax
    $total_rows['total_tax'] = [
        'label' => __('Tax:', 'woocommerce'),
        'value' => $currency_symbol . number_format($order_obj->get_total_tax(), 2),
    ];

    $total_rows = moveKeyBefore($total_rows, 'shipping', 'total_tax');

    foreach($tax_rates as $key => $tax_rate) {
        $total_rows['tax_rate_' . $key] = [
            'label' => __("Tax Rate ($tax_rate->label):", 'woocommerce'),
            'value' => \WC_Tax::get_rate_percent($tax_rate->rate_id)
        ];
        $total_rows = moveKeyBefore($total_rows, 'shipping', 'tax_rate_' . $key);
    }

    $total_rows['cart_subtotal']['value'] = $currency_symbol . number_format( $order_obj->get_subtotal(), 2 );

    return $total_rows;

}, 10, 2 );

/**
 * Amend the order columns in the admin user login
 */

add_filter( 'woocommerce_account_orders_columns', function($columns = []) {

    if (isset($columns['order-actions'])) {
        unset($columns['order-actions']);
    }

    return $columns;

});

/**
 * Customise parts of the checkout page
 */

add_action('tdr_after_totals_title', function() {
    if (wc_coupons_enabled()) {
        echo '
            <h5 class="mb-5 mt-5 mt-lg-0">Got a Coupon?</h5>
            <div class="mb-5">
                <a
                    class="btn btn--outline w-100"
                    href="#modal-coupon"
                    data-toggle="modal"
                >
                    <span>Add Coupon</span>
                </a>
            </div>
        ';
    }
}, 10);

/**
 *  Add a suffix to SKU for order items with insurance add-on
 */

add_action('woocommerce_checkout_create_order', function(WC_Order $order) {
    $items = $order->get_items();
    $skuSuffixes = [
        'Add Mask Fit Insurance'
    ];

    foreach ($items as $item) {
        $metaData = $item->get_meta_data();

        if (!empty($metaData)) {
            foreach ($metaData as $key => $metaItem) {
                $match = array_filter(array_map(function ($partialKey) use ($metaItem) {
                    return strpos($metaItem->key, $partialKey) !== false;
                }, $skuSuffixes));

                if ($match && strtolower($metaItem->value) === 'yes' && $currentSku = ($metaData['SKU']->value ?? $item->get_product()->get_sku())) {
                    $item->update_meta_data('SKU', $currentSku . '-MFI');
                    continue(2);
                }
            }
        }
    }
});

/**
 *  Always show tax totals
 */

add_filter( 'woocommerce_cart_hide_zero_taxes', '__return_false' );
add_filter( 'woocommerce_order_hide_zero_taxes', '__return_false' );

/*
* Add notify_url to the checkout for IPN
*
*/

add_action( 'woocommerce_checkout_before_order_review', function() {

    // Output hidden field in the checkout form with our custom data
    echo '<input type="hidden" name="notify_url" value="' . get_bloginfo('url') . '/?wc-api=WC_Gateway_Paypal">';
    echo '<input type="hidden" name="return" value="' . get_bloginfo('url') . '/checkout/order-received/">';

}, 50 );

/*
* Customise certain order lines from the order totals table on the confirmation page and emails
*
*/

add_filter('woocommerce_get_order_item_totals', function($items, $order) {

    // Get values
    $subtotal = $order->get_subtotal(); // excl tax
    $subtotal_tax = $order->get_total_tax() - $order->get_shipping_tax(); // excl shipping
    $shipping = $order->get_shipping_to_display('incl'); // incl tax

    // Add total row
    $total_row = [
        'Total' => [
            'label' => __( 'Total:', 'woocommerce' ),
            'value' => wc_price($subtotal + $subtotal_tax),
        ],
    ];

    $items = array_slice($items, 0, 3, true) + $total_row + array_slice($items, 3, count($items) - 1, true);

    // Remove tax lines as we have them per product
    unset($items['tax_rate_VAT-1']);
    unset($items['vat-1']);

    // Change subtotal to display with tax
    $items['cart_subtotal']['value'] = wc_price($subtotal);

    // Change cart tax to not include shipping
    $items['total_tax']['value'] = wc_price($subtotal_tax);

    // Change shipping to display with tax
    $items['shipping']['value'] = $shipping;

    // Change total label
    $items['order_total']['label'] = __( 'Grand Total:', 'woocommerce' );

    return $items;

}, 10, 3);

/*
* Always show customer country
*
*/

add_filter( 'woocommerce_formatted_address_force_country_display', '__return_true' );

/**
 * Add SKU to PDF order heading on WooCommerce PDF Plugin
 */

add_filter ( 'pdf_template_table_headings' , function() {

    $headers =  '<table class="shop_table orderdetails" width="100%">' .
                '<thead>' .
                '<tr><th colspan="7" align="left"><h3>' . esc_html__('Order Details', PDFLANGUAGE) . '</h3></th></tr>' .
                '<tr>' .
                '<th width="20%" valign="top" align="left">' . __( 'SKU', PDFLANGUAGE )         . '</th>' .
                '<th width="35%" valign="top" align="left">'  . __( 'Product', PDFLANGUAGE )    . '</th>' .
                '<th width="5%" valign="top" align="right">'  . __( 'Qty', PDFLANGUAGE )        . '</th>' .
                '<th width="10%" valign="top" align="right">'  . __( 'Subtotal', PDFLANGUAGE )  . '</th>' .
                '<th width="10%" valign="top" align="right">'  . __( 'Rate', PDFLANGUAGE )   . '</th>' .
                '<th width="10%" valign="top" align="right">'  . __( 'Tax', PDFLANGUAGE )        . '</th>' .
                '<th width="10%" valign="top" align="right">' . __( 'Total', PDFLANGUAGE )  . '</th>' .
                '</tr>' .
                '</thead>' .
                '</table>';

    return $headers;

});

/**
 * Add SKU to order line on WooCommerce PDF Plugin
 */

add_filter ( 'pdf_template_line_output' , function($line, $order_id) {

    global $woocommerce;

    $order = new WC_Order( $order_id );

    // Check WC version – changes for WC 3.0.0
    $pre_wc_30 = version_compare( WC_VERSION, '3.0', '<' );
    $order_currency = $pre_wc_30 ? $order->get_order_currency() : $order->get_currency();

    $pdflines = '<table width="100%">';
    $pdflines .= '<tbody>';

    if (sizeof($order->get_items()) > 0) {

        foreach ($order->get_items() as $item) {

            if ($item['qty']) {

                $line = '';

                $_product = $order->get_product_from_item($item);
                $item_name = $item['name'];
                $item_id = $pre_wc_30 ? $item['variation_id'] : $item->get_id();

                $meta_display = '';

                if (version_compare( WC_VERSION, '3.0', '<')) {
                    $item_meta = new WC_Order_Item_Meta( $item );
                    $meta_display = $item_meta->display( true, true );
                    $meta_display = $meta_display ? ( ' ( ' . $meta_display . ' )' ) : '';
                } else {
                    foreach ($item->get_formatted_meta_data() as $meta_key => $meta) {
                        $meta_display .= '<br /><small>(' . $meta->display_key . ':' . wp_kses_post( strip_tags( $meta->display_value ) ) . ')</small>';
                    }
                }

                if ($meta_display) {
                    $meta_output = apply_filters( 'pdf_invoice_meta_output', $meta_display );
                    $item_name .= $meta_output;
                }

                $line = '<tr>' .
                '<td valign="top" width="20%">' . $_product->get_sku() . '</td>' .
                '<td valign="top" width="35%">' . stripslashes( $item_name ) . '</td>' .
                '<td valign="top" width="5%" align="right">' . $item['qty'] . '</td>' .
                '<td valign="top" width="10%" align="right">' . wc_price( $item['line_subtotal'], array( 'currency' => $order_currency ) ) . '</td>' .
                '<td valign="top" width="10%" align="right">' . floor(100 * ( $item['line_subtotal_tax'] / $item['line_subtotal'] )) . '%' . '</td>' .
                '<td valign="top" width="10%" align="right">' . wc_price( $item['line_subtotal_tax'] / $item['qty'], array( 'currency' => $order_currency ) ). '</td>' .
                '<td valign="top" width="10%" align="right">' . wc_price( $item['line_subtotal'] + $item['line_subtotal_tax'], array( 'currency' => $order_currency ) ). '</td>' .
                '</tr>';

                $pdflines .= $line;
            }

        }

    }

    $pdflines .= '</tbody>';
    $pdflines .= '</table>';

    return $pdflines;

}, 10, 2 );

/**
 * Overwrite the shipping options if a custom class is found on products in the cart
 */

add_filter( 'woocommerce_package_rates', function($rates) {

    global $woocommerce;

    // Get cart items
    $items = $woocommerce->cart->get_cart();

    // ACF Fields
    $delivery_overwrite = get_field('special_delivery_overwrite', 'option');

    if (empty($delivery_overwrite['class_slug']) || empty($delivery_overwrite['shipping_label'])) {
        return $rates;
    }

    // Loop through cart items and checking for the specific defined shipping class
    $found = false;

    if (!empty($items)) {
        foreach($items as $item => $values) {
            if ($values['data']->get_shipping_class() == $delivery_overwrite['class_slug']) {
                $found = true;
                break;
            }
        }
    }

    // Custom class found, remove all except special delivery
    if ($found) {
        $allowed_rates = array();

    	foreach ($rates as $rate_id => $rate) {
    		if ($rate->label === $delivery_overwrite['shipping_label']) {
    			$allowed_rates[$rate_id] = $rate;
    			break;
    		}
    	}

    	return (!empty($allowed_rates)) ? $allowed_rates : $rates;
    }

    // Return shipping rates
    return $rates;

}, 99);

/**
 * On the checkout show a custom message if there are backordered items in the cart
 */

add_action('woocommerce_review_order_before_payment', function() {

    global $woocommerce;

    // Get cart items
    $items = $woocommerce->cart->get_cart();

    // Loop through cart items and check if there are back-ordered items
    $found = false;

    if (!empty($items)) {
        foreach($items as $item => $values) {
            $stock_qty = $values['data']->get_stock_quantity();
            if ( is_int( $stock_qty ) && ( $stock_qty < 1 ) )  {
                $found = true;
                break;
            }
        }
    }

    // Back ordered items content
    $content = get_field('back_ordered_items_content', 'option');

    if ($found && !empty($content)) {
        echo '<div class="product-notice product-notice--information mt-5">' . $content . '</div>';
    }

}, 99);

/**
 * Register a new order status
 */

add_action( 'init', function() {
    register_post_status( 'wc-awaiting-shipment', array(
        'label'                     => 'Awaiting shipment',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Awaiting shipment (%s)', 'Awaiting shipment (%s)' )
    ) );
});

/**
 * Add new order status to dropdown
 */

add_filter( 'wc_order_statuses', function($order_statuses) {

    $new_order_statuses = array();

    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {

        $new_order_statuses[ $key ] = $status;

        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-awaiting-shipment'] = 'Awaiting shipment';
        }
    }

    return $new_order_statuses;

});

/**
 * Add SKU to cart page
 */

add_filter( 'woocommerce_cart_item_name', function($item_name, $cart_item, $cart_item_key) {

    $product = $cart_item['data'];
    $sku = $product->get_sku();

    if (empty($sku)) {
        return $item_name;
    }

    // Add the sku
    $item_name .= '<p class="p-sm pt-3">' . __( "SKU: ", "woocommerce") . $sku . '</p>';

    return $item_name;

}, 99, 3 );

/**
 * Disable the BACs payment gateway if the user is not on our allowed list
 *
 */

add_filter( 'woocommerce_available_payment_gateways', function($available_gateways) {

    $allowed = false;
    $allowed_users = get_field('allowed_bacs_payment_users', 'option');

    if (is_user_logged_in() && !empty($allowed_users)) {

        $current = wp_get_current_user();
        $current_email = $current->user_email;

        foreach ($allowed_users as $allowed_user) {
            if ($allowed_user['email_address'] == $current_email) {
                $allowed = true;
                break;
            }
        }

    }

    if (isset($available_gateways['bacs']) && !$allowed) {
        unset($available_gateways['bacs']);
    }

    return $available_gateways;

});



//add_action( 'wp_head', 'intus_microsoft_ads_conversion_tracking' );
add_action( 'woocommerce_thankyou', 'intus_microsoft_ads_conversion_tracking' );
function intus_microsoft_ads_conversion_tracking(){
    // On Order received endpoint only
    if( is_wc_endpoint_url( 'order-received' ) ) :
        $order_id = absint( get_query_var('order-received') ); // Get order ID

        if( get_post_type( $order_id ) !== 'shop_order' ){
            return;
        }

        $order = wc_get_order( $order_id ); // Get the WC_Order Object instance
        ?>
        <script>
            function intus_microsoft_ads_conversion_tracking_revenue(){
                return <?php echo $order->get_total(); ?>
            }
        </script>
        <script>
            window.uetq = window.uetq || [];
            window.uetq.push('event', 'purchase', {'revenue_value': intus_microsoft_ads_conversion_tracking_revenue(), 'currency': '<?php echo $order->get_currency(); ?>'});
        </script>
    <?php
    endif;
}
