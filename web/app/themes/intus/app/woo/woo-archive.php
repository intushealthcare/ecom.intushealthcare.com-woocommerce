<?php
namespace App;

/**
 * @param \WP_Query|null $wp_query
 *
 * @return void
 */
function category_has_products( \WP_Query $wp_query = null )
{
    if( !$wp_query ){
        global $wp_query;
    }
    $category = $wp_query->get_queried_object();
    /* @var WP_Term $category  */
    if( $category->name != "product"){
        // gets the number of products published in the current category
        $args = array(
            'status'   => 'publish',
            'category' => [ $category->slug ],
            'return'   => 'ids',
            'limit'    => -1,
        );
        $products = wc_get_products( $args );
        if( count($products) == 0 ){
            return false;
        }
    }
    return true;
}


/**
 * Shop loop ordering wrap
 *
 * @return null
 */

add_action('woocommerce_before_shop_loop', function() {
    echo '<div class="d-flex align-items-center justify-content-between mb-4 pt-3">';
}, 15);

add_action('woocommerce_before_shop_loop', function() {
    echo '</div>';
}, 40);

/**
 * Customise the product listing item titles
 *
 * @return null
 */

remove_action( 'woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title', 10 );

add_action('woocommerce_shop_loop_item_title', function() {
    global $product;
    $brand_text = "";
    $meta  = get_post_meta( $product->get_id(), '_woocommerce_gpf_data', true );
    $brand = $meta['brand'] ?? false;
    if ($product instanceof \WC_Product && $brand ) {
        $brand_text .= '<br><span class="text-normal"> by&nbsp;' . $brand . '</span>';
    }
    $title = '<h3 class="product__title p pt-4 mb-2 color-secondary text-bold"><a href="' . get_the_permalink() . '" class="color-secondary">' . get_the_title() . $brand_text . '</a></h3>';
    echo $title;

//    $title = '<h3 class="product__title p pt-4 mb-2 color-secondary text-bold"><a href="' . get_the_permalink() . '" class="color-secondary">' . get_the_title() . '</a></h3>';
//    $meta  = get_post_meta( $product->get_id(), '_woocommerce_gpf_data', true );
//    $brand = $meta['brand'] ?? false;
//    if ($product instanceof \WC_Product && $brand ) {
//        $title .= '<div class="text-small color-secondary text-bold mb-3">by ' . $brand . '</div>';
//    }
//    echo $title;
}, 10 );

/**
 * Conditionally remove add to cart button from product archive
 *
 * @return null
 */
if( !get_field( 'woo_category_product_link_buy', 'option' ) ) {
    remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
}

/**
 * Conditionally add a more info link to product's on an archive page
 */
if( get_field( 'woo_category_product_link_more_info', 'option' ) ) {
    add_action( 'woocommerce_after_shop_loop_item_title', function(){
        global $product;
        echo template( locate_template(' views/partials/woo/loop/product-more-info' ), [ 'product' => $product] );
    }, 15 );
}

/**
 * Remove reviews from product listing items
 *
 * @return null
 */

remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);

add_action('woocommerce_after_shop_loop_item_title', function() {

    global $product;

    $average = $product->get_average_rating();

    echo '<div class="mb-3">';
    echo template(locate_template('views/partials/star-rating'), [
        'star_review' => $average
    ]);
    echo '</div>';

}, 5);

/**
 * Reorder the ordering/total posts blocks
 *
 * @return null
 */

remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
add_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 35);

/**
 * Remove woocommerce pagination from archive page
 *
 * @return null
 */
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );

add_action( 'woocommerce_after_shop_loop', function() {
    global $wp_query;
    echo \App::ajaxPagination($wp_query);
}, 10 );

/**
 * Change sale text
 *
 * @return null
 */

add_filter('woocommerce_sale_flash', function($content) {
    $content = '<span class="onsale">' . __( 'On Sale', 'woocommerce' ) . '</span>';
    return $content;
}, 10, 3);

/**
 * Add 'new' label to products within a month
 *
 * @return null
 */

add_action('woocommerce_before_shop_loop_item_title', function() {

    global $post;

    $product = wc_get_product($post->ID);

    if ($product->is_featured()) {
        echo '<span class="newproduct">' . __( 'New', 'woocommerce' ) . '</span>';
    }
}, 5);

/**
 * Set default order by value on the products page and remove popularity
 *
 * @return null
 */

add_filter('woocommerce_catalog_orderby', function($orderby) {
    unset($orderby['popularity']);
    unset($orderby['date']);
    return $orderby;
}, 20 );

/**
 * Disable the Wordpress URL rewriting for pagination e.g. /page/2/ (as we are using custom)
 *
 * @return null
 */

add_action( 'parse_query', function($query){
    if (is_archive() && $query->is_main_query() && !is_admin()) {
        remove_filter( 'template_redirect', 'redirect_canonical' );
    }
});

/**
 * Change number of products that are displayed per page (shop page)
 *
 * @return integer
 */
add_filter( 'loop_shop_per_page', function($cols) {
    $products_per_page = 80;
    return $products_per_page;
}, 20, 1 );

/**
 * Add button after sub category titles
 *
 * @return null
 */

add_action( 'woocommerce_after_subcategory_title', function( $category ) {
//    $cat_min_price = get_min_price_for_product_in_category( $category->term_id );
//    if( $cat_min_price > 0 ){
//        echo '<div class="price category_from_price mb-5 color-dark-grey">';
//        echo '<span class="from category_from_price-label d-inline-block mr-2">' . __( 'From:' ) . '</span><span class="d-inline-block category_from_price-value">';
//        echo '<span class="currencySymbol">£</span>' . $cat_min_price . '</span></div>';
//    }

    echo '<div class="btn btn-primary d-none d-md-block">' . __('Shop Now') .'</div>';
    echo '<div class="btn btn-primary d-block d-md-none">' . __('Shop') .'</div>';
}, 50);

/**
 * Remove the word "Category" from archive titles
 *
 * @return null
 */

add_filter( 'get_the_archive_title', function($title) {
    if ( is_product_category() ) {
        $title = single_cat_title( '', false );
    }
    return $title;
});


/**
 * On Category pages show non VAT exempt products as VAT included
 */
add_filter( 'woocommerce_after_main_content', function($title) {
    if( is_product_category() ) {
        if( woo_is_intus_vat_exempt_enabled() ){
            echo template(locate_template('views/partials/woo/category-notice'));
        }
    }
});


function get_min_price_for_product_in_category( $term_id )
{
    global $wpdb;

    $sql = "
    SELECT  MIN( meta_value+0 ) as minprice
    FROM {$wpdb->posts}
    INNER JOIN {$wpdb->term_relationships} ON ({$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id)
    INNER JOIN {$wpdb->postmeta} ON ({$wpdb->posts}.ID = {$wpdb->postmeta}.post_id)
    WHERE
      ( {$wpdb->term_relationships}.term_taxonomy_id IN (%d) )
        AND {$wpdb->posts}.post_type = 'product'
        AND {$wpdb->posts}.post_status = 'publish'
        AND {$wpdb->postmeta}.meta_key = '_price'
        AND {$wpdb->posts}.ID IN (SELECT posts.ID
                FROM {$wpdb->posts} AS posts
                INNER JOIN {$wpdb->term_relationships} AS term_relationships ON posts.ID = term_relationships.object_id
                INNER JOIN {$wpdb->term_taxonomy} AS term_taxonomy ON term_relationships.term_taxonomy_id = term_taxonomy.term_taxonomy_id
                INNER JOIN {$wpdb->terms} AS terms ON term_taxonomy.term_id = terms.term_id
                WHERE term_taxonomy.taxonomy = 'product_type'
                AND terms.slug = 'simple'
                AND posts.post_type = 'product'
                )
                AND {$wpdb->posts}.ID NOT IN (SELECT posts2.ID
				FROM {$wpdb->posts} AS posts2
    			INNER JOIN {$wpdb->term_relationships} AS term_relationships2 ON posts2.ID = term_relationships2.object_id
    			LEFT JOIN {$wpdb->terms} AS terms2 ON term_relationships2.term_taxonomy_id = terms2.term_id
				WHERE posts2.post_type = 'product'
    			AND terms2.name = 'exclude-from-catalog'
    			AND term_relationships2.object_id IS NOT NULL)
       ";

    return $wpdb->get_var($wpdb->prepare($sql, $term_id));
}

