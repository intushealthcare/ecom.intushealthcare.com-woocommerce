<?php

namespace App;

use WP_Query;

/**
 * Custom function to alter WP_Query arguments
 *
 * @return null
 */

function intus_customise_wp_args( $filters, $category = '' ) {

    // Default arguments
    $args = [
        'post_type'      => [ 'product', 'product_variation' ],
        'posts_per_page' => get_option( 'posts_per_page' ),
        'post_status'    => 'publish',
        'tax_query'      => [
            'relation' => 'AND',
            [
                'taxonomy' => 'product_visibility',
                'field'    => 'name',
                'terms'    => 'exclude-from-catalog',
                'operator' => 'NOT IN',
            ]
        ],
        'orderby'        => [
            'menu_order' => 'DESC',
            'title'      => 'ASC'
        ]
    ];

    // Set pagination value
    $args['paged'] = ( !empty( $filters['paged'] ) ) ? $filters['paged'] : 1;

    // Passed if we are on a top level category page
    if ( !empty( $category ) ) {
        $args['tax_query'][] = [
            'taxonomy' => 'product_cat',
            'field'    => 'slug',
            'terms'    => $category['slug'],
            'operator' => 'IN',
        ];
    }

    // Customise the meta and tax queries based off filters passed
    if ( !empty( $filters['tax'] ) ) {
        foreach ( $filters['tax'] as $taxonomy => $terms ) {

            // Set product category back to correct name
            if ( $taxonomy == 'cat' ) {
                $taxonomy = 'product_cat';
            }

            if ( taxonomy_exists( $taxonomy ) ) {
                $args['tax_query'][] = [
                    'taxonomy' => $taxonomy,
                    'field'    => 'slug',
                    'terms'    => $terms,
                    'operator' => ( count( $terms ) > 1 ) ? 'OR' : 'IN',
                ];
            }

            if ( $taxonomy == 'sale' ) {
                $args['meta_query'][] = [
                    'relation' => 'OR',
                    [
                        'key'     => '_sale_price',
                        'value'   => 0,
                        'compare' => '>',
                        'type'    => 'numeric',
                    ],
                    [
                        'key'     => '_min_variation_sale_price',
                        'value'   => 0,
                        'compare' => '>',
                        'type'    => 'numeric',
                    ],
                    [
                        'key'   => '_child_on_sale',
                        'value' => 1,
                        'type'  => 'numeric',
                    ],
                ];
            }

        }
    }

    // price filter
    if ( !empty( $filters['price'] ) ) {
        $args['meta_query'][] = [
            'key'     => '_price',
            'value'   => [
                (int) $filters['price']['price_min'],
                (int) $filters['price']['price_max'],
            ],
            'type'    => 'NUMERIC',
            'compare' => 'BETWEEN'
        ];
    }

    // order type
    if ( !empty( $filters['orderby'] ) ) {
        if ( $filters['orderby'] == 'price' ) {
            $args['orderby']  = 'meta_value_num';
            $args['meta_key'] = '_price';
        }

        if ( $filters['orderby'] == 'menu_order' ) {
            $args['order'] = 'menu_order title';
            unset( $args['order'] );
        }
    }

    // ordering
    if ( !empty( $filters['order'] ) ) {
        $args['order'] = $filters['order'];
    }

    // multiple meta queries
    if ( isset( $args['meta_query'] ) && count( $args['meta_query'] ) > 1 ) {
        $args['meta_query']['relation'] = 'AND';
    }

    // multiple tax queries
    if ( isset( $args['tax_query'] ) && count( $args['tax_query'] ) > 1 ) {
        $args['tax_query']['relation'] = 'AND';
    }

    // keyword search
    if ( !empty( $filters['keyword'] ) ) {
        $args['s'] = $filters['keyword'];
    }

    return $args;

}

/**
 * AJAX function to alter product query
 *
 * @return null
 */

add_action( 'wp_ajax_nopriv_intus_filter_product_query', 'App\\intus_filter_product_query' );
add_action( 'wp_ajax_intus_filter_product_query', 'App\\intus_filter_product_query' );

function intus_filter_product_query() {

    // POST data
    $filters  = ( isset( $_POST['filters'] ) ) ? json_decode( stripslashes( $_POST['filters'] ), true ) : '';
    $category = ( isset( $_POST['queried_object'] ) ) ? $_POST['queried_object'] : '';

    // Customise wp_query arguments based on filters passed
    $args         = \App\intus_customise_wp_args( $filters, $category );
    $custom_query = new WP_Query( $args );

    // Default loop
    echo \App\template( locate_template( 'views/partials/woo/product-loop' ), [
        'custom_query' => $custom_query
    ] );

    die();

}

/**
 * Checks if there are GET parameters passed on the page to alter the product query
 *
 * @return null
 */
add_action( 'pre_get_posts', function ( $query ) {
    if ( is_archive() && $query->is_main_query() && !is_admin() ) {

        // Get form parameters
        $params = $_GET;

        // Create empty array so we can format the data correctly
        $filters  = [];
        $category = '';

        if ( !empty( $params ) ) {

            // Format filter data
            foreach ( $params as $key => $termString ) {

                // Set product category back to correct name
                if ( $key == 'cat' ) {
                    $key = 'product_cat';
                }

                if ( taxonomy_exists( $key ) ) {
                    $terms                = explode( ',', $termString );
                    $filters['tax'][$key] = $terms;
                }
                elseif ( $key == 'new' || $key == 'sale' ) {
                    $filters['tax'][$key] = [ $termString ];
                }
                elseif ( $key == 'price_min' || $key == 'price_max' ) {
                    $filters['price'][$key] = $termString;
                }
                else {
                    $filters[$key] = $termString;
                }
            }

        }

        // Product category
        if ( is_product_category() ) {
            $current  = get_queried_object();
            $category = [
                'slug' => $current->slug,
            ];
        }

        // Get query arguments
        $args = \App\intus_customise_wp_args( $filters, $category );

        // Set arguments
        if ( !empty( $args['meta_query'] ) ) {
            $query->set( 'meta_query', $args['meta_query'] );
        }

        if ( !empty( $args['tax_query'] ) ) {
            $query->set( 'tax_query', $args['tax_query'] );
        }

        if ( !empty( $args['meta_key'] ) ) {
            $query->set( 'meta_key', $args['meta_key'] );
        }

        if ( !empty( $args['order'] ) ) {
            $query->set( 'order', $args['order'] );
        }

        if ( !empty( $args['orderby'] ) ) {
            $query->set( 'orderby', $args['orderby'] );
        }

        if ( !empty( $args['s'] ) ) {
            $query->set( 's', $args['s'] );
        }

    }
} );

/**
 * Limit the WP search behaviour to only search post titles
 *
 * @return null
 */

add_filter( 'posts_search', function ( $search, $wp_query ) {

    if ( !empty( $search ) && !empty( $wp_query->query_vars['search_terms'] ) ) {

        global $wpdb;

        $q      = $wp_query->query_vars;
        $n      = !empty( $q['exact'] ) ? '' : '%';
        $search = array();

        foreach ( (array) $q['search_terms'] as $search_term ) {
            $search[] = $wpdb->prepare( "$wpdb->posts.post_title LIKE %s", $n . $wpdb->esc_like( $search_term ) . $n );
        }

        if ( !is_user_logged_in() ) {
            $search[] = "$wpdb->posts.post_password = ''";
        }

        $search = ' AND ' . implode( ' AND ', $search );
    }

    return $search;

}, 10, 2 );


/**
 * Remove Order Comments field on checkout
 */
add_filter( 'woocommerce_enable_order_notes_field', '__return_false', 9999 );


/**
 * Filter the structured data array for offers created by WooCommerce to change every price to inc vat and modify according to is vat exempt status
 *
 * @param array      $markup          The array representing the JSON+LD as
 *                                    generated by WooCommerce
 * @param WC_Product $offer_product   The specific product being listed as
 *                                    an offer.
 *
 * @return array                The modified array.
 */
add_filter( 'woocommerce_structured_data_product', function ( $markup, $offer_product ) {
    if ( isset( $markup['offers'] ) ) {

        if ( get_field( 'product_schema_prices_with_vat', 'option' ) ) {
            $i                = 0;
            foreach ( $markup['offers'] as $offer ) {
                $vat_rate = 1;
                // We only want to modify products that are not VAT Exempt
                if ( !\App\woo_is_product_vat_exempt( $offer_product ) ) {
                    $vat_rate_percent = get_field( 'product_schema_prices_vat_rate', 'option' );
                    $vat_rate         = ( 100 + $vat_rate_percent ) / 100;
                }
                // Mark all products as being explicitly inc vat. Somewhat disingenuous, but it solves a problem...
                if ( isset( $offer['priceSpecification'] ) ) {
                    $newPriceSpecification                          = $offer['priceSpecification'];
                    $priceIncVat                                    = number_format( $newPriceSpecification['price'] * $vat_rate, 2 );
                    $offer['price']                                 = $priceIncVat;
                    $newPriceSpecification['price']                 = $priceIncVat;
                    $newPriceSpecification['valueAddedTaxIncluded'] = "true";
                    $offer['priceSpecification']                    = $newPriceSpecification;
                }
                else {
                    if ( isset( $offer['@type'] ) ) {
                        $offer['lowPrice']           = number_format( $offer['lowPrice'] * $vat_rate, 2 );
                        $offer['highPrice']          = number_format( $offer['highPrice'] * $vat_rate, 2 );
                        $offer['priceSpecification'] = [
                            'valueAddedTaxIncluded' => "true"
                        ];
                    }
                }
                $markup['offers'][$i] = $offer;
            }
        }
    }

    return $markup;
}, 7, 2 );


/**
 * Add brand name as a suffix to a product's title
 */
add_filter( 'the_title', function ( $title, $id = null ) {
    if ( is_singular( 'product' ) && in_the_loop() ) {
        $meta  = get_post_meta( $id, '_woocommerce_gpf_data', true );
        $brand = $meta['brand'] ?? false;
        if ( $brand ) {
            return $title . " <span class='prefix'>from</span>&nbsp;<span class='brand'>{$brand}</span>";
        }
    }
    return $title;
}, 10, 2 );



/**
 * Disable Cheque Payment Method for Specific SKU
 */
add_filter( 'woocommerce_available_payment_gateways', function ( $available_gateways ) {
    if ( is_admin() ) {
        return $available_gateways;
    }
    if ( ! is_checkout() ) {
        return $available_gateways;
    }
    if( !isset($available_gateways['cheque'] ) ){
        return $available_gateways;
    }

    $unset = true;
    foreach ( WC()->cart->get_cart_contents() as $key => $values ) {
        $product = $values['data'];
        /* @var $product \WC_Product */
        $sku = $product->get_sku();
        if( strpos( $sku, 'phone-payment') !== false ){
            $unset = false;
            break;
        }
    }
    if ( $unset === true ) {
        unset( $available_gateways['cheque'] );
    }
    return $available_gateways;
} );
