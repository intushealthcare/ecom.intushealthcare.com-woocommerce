<?php
namespace App;

use WC_Tax;
use WCS_ATT_Product_Schemes;
//
///**
// * @param $product \WC_Product
// *
// * @return int:null
// */
//function woo_get_parent_product_id( \WC_Product $product ) {
//    /* @var int $product_id */
//    $product_id = $product->get_id();
//    $parent_product_id = ( $product->is_type( 'variation' ) ) ? $product->get_parent_id() : $product->get_id();
//    if( $product_id != $parent_product_id ){
//        return $parent_product_id;
//    }
//    return null;
//}
//
//
///**
// * @param $product \WC_Product
// *
// * @return boolean
// */
//function woo_has_parent_product( \WC_Product $product ) {
//    if( woo_get_parent_product_id( $product ) ){
//        return true;
//    }
//    return false;
//}
//
//
///**
// * @param $product \WC_Product
// *
// * @return \WC_Product|null
// */
//function woo_get_parent_product( \WC_Product $product ) {
//    if( $parent_product_id = woo_get_parent_product_id( $product ) ){
//        return wc_get_product( $parent_product_id );
//    }
//    return null;
//}
//
//
///**
// * A link to product that includes SKU parameters to prepopulate product options
// * @param $product \WC_Product
// *
// * @return void
// */
//function woo_get_product_direct_link( $product ) {
//    $url = $product->get_permalink();
//    if( woo_has_parent_product( $product ) ){
//        $parent_product = woo_get_parent_product( $product );
//        $url = $parent_product->get_permalink();
//        $variations = $product->get_variation_attributes();
//        $params = ['sku' => $product->get_sku() ];
//        foreach ( $variations as $key => $value){
//            $params[$key] = $value;
//        }
//        $url .= "?=" . http_build_query($params);
//    }
//    return $url;
//}


/**
 * Add Trustpilot widget between single product title and SKU
 */
add_action('woocommerce_single_product_summary', function(){
    echo \App\trustpilot_get_widget_html('product_review_mini');
}, 1);

/**
 * Add SKU to product page under title
 */
add_action('woocommerce_single_product_summary', function(){
    global $product;
    echo "<span class='product_sku'><span class='product_sku__label'>" . __('SKU', 'sage') . "</span><span class='product_sku__value'>{$product->get_sku()}</span></span>";
}, 7);


/**
 * Get html to display for ex or inc prices on Single Product Form
 * @param $product
 * @param $tax_ex_inc
 *
 * @return string
 */
function woo_get_product_price_markup( $product, $tax_ex_inc = 'ex', $price_markup = null ) {
    $vat_exempt = woo_is_product_vat_exempt( $product );
    $product_exempt_class = woo_get_product_vat_exempt_suffix( $product );

    $price_html = '<div class="price-wrapper price-wrapper__' . $tax_ex_inc . ' price-wrapper' . $product_exempt_class . '">';
    $price_html .= $price_markup;

    $get_vat_suffix_function = "\App\woo_get_vat_{$tax_ex_inc}_suffix";
    //Get the relevant VAT Suffix from dynamic function name..
    $price_html .= $get_vat_suffix_function( $product );
    $price_html .= '</div>';

    return $price_html;
}

/**
 * Get html to display for ex or inc prices on Single Product Form
 * @param $product
 * @param $tax_ex_inc
 *
 * @return string
 */
function woo_get_product_single_price_markup( $product, $tax_ex_inc = 'ex' ) {
    $display_inc_vat_prices = woo_is_product_show_inc_vat_enabled();
    $product_vat_exempt = woo_is_product_vat_exempt( $product );

    if( $display_inc_vat_prices === false ) {
        if( $product_vat_exempt && ( $tax_ex_inc === 'inc' ) ){
            return false;
        }

        if( !$product_vat_exempt && ( $tax_ex_inc === 'ex' ) ){
            return false;
        }
    }

    $get_price_function = "wc_get_price_excluding_tax";
    if( $tax_ex_inc === 'inc' ){
        $get_price_function = "wc_get_price_including_tax";
    }

    $price_regular = $get_price_function($product, ['price' => $product->get_regular_price()]);
    $price = $get_price_function( $product );
    $formatted_price = wc_price( $price );

    if ($product->is_on_sale()) {
        $price_markup = wc_format_sale_price( $price_regular, $formatted_price );
    }else{
        $price_markup = $formatted_price;
    }
    $price_html =  woo_get_product_price_markup( $product, $tax_ex_inc, $price_markup );

    return $price_html;
}

/**
 * Remove the price from the product single page and only show if it's a simple product
 *
 * @return null
 */

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
add_action('woocommerce_single_product_summary', 'App\\intus_simple_form_price', 10);

function intus_simple_form_price() {

    global $product;

    $price_html = "";

    if ($product->is_type('simple') && !empty($product->get_regular_price())) {
        $prices_ex_vat = \App\woo_are_prices_displayed_ex_vat();
        if( $prices_ex_vat ){
            $price_html .= woo_get_product_single_price_markup( $product, 'ex');
            $price_html .= woo_get_product_single_price_markup( $product, 'inc');
        }else{
            $price_html = $product->get_price_html();
        }

        echo '<div class="pb-4 product-simple-price">';
        echo '<div class="d-flex flex-column flex-md-row justify-content-md-between pt-4">';
        echo '<div class="price mb-2 mb-md-0">';
        echo $price_html;
        echo '</div>';
        echo wc_get_stock_html( $product );
        echo '</div>';
        echo '</div>';
    }

}


/**
 * Get Variation From price
 *
 * @return null
 */

function woo_get_variation_from_price( $product ) {

    if (!$product->is_type('variable')) {
        return false;
    }

    if( !is_product() ){
        $min_price = $product->get_variation_price('min');
//        $min_price .= " " . \App\woo_get_vat_ex_suffix( $product );
        return $min_price;
    }

    // Minimum variation price
    $min_price = $product->get_variation_price('min');
    $min_price_markup = wc_price($product->get_variation_price('min') );

    // Tax rates
    $tax_rates = WC_Tax::get_rates($product->get_tax_class());
    $taxes = WC_Tax::calc_tax($min_price, $tax_rates, false);

    // If shop is VAT exempt enabled do different stuff
    if ( \App\woo_is_intus_vat_exempt_enabled() ) {

        $price_html = '<div class="price">';

        $prices_ex_vat = \App\woo_are_prices_displayed_ex_vat();
        if( $prices_ex_vat ){
            $price_html .= woo_get_product_price_markup( $product, 'ex', $min_price_markup);
            if( woo_is_product_show_inc_vat_enabled() ){
                $min_price_inc_markup = wc_price($min_price + array_sum($taxes));
                $price_html .= woo_get_product_price_markup( $product, 'inc', $min_price_inc_markup);
            }
        }else{
            $price_html .= $product->get_price_html();
        }
        $price_html .= '</div>';

        return $price_html;
    }

    return $min_price_markup;

}

/**
 * Remove the variation price container and add a custom section at the top of the form that shows
 * both "from" price and the dynamic variation price (ajax)
 *
 * @return null
 */

remove_action('woocommerce_single_variation', 'woocommerce_single_variation', 10);
add_action('woocommerce_before_variations_form', 'App\\intus_variation_form_price', 10);

function intus_variation_form_price () {

    global $product;

    if (!empty($product->get_available_variations())) {

        // Minimum variation price
        $from_price_markup = woo_get_variation_from_price( $product );

        // Display minimum variation price
        echo '
            <div class="single-variation__prices">
                <div class="single-variation_from">' . wc_get_price_html_from_text() . $from_price_markup . '</div>
                <div class="single_variation_wrap"><div class="woocommerce-variation single_variation"></div></div>
            </div>
        ';
    }

}


/**
 * @param $product
 *
 * @return bool
 */
function woo_product_is_magnetic( $product ) {
    $is_magnetic = get_field('product_is_magnetic', $product->get_id() );
    if( $is_magnetic ) {
       return true;
    }
    return false;
}

/**
 * Find a way to modify the variation select price html...
 */
add_filter('woocommerce_available_variation', function ( $data, $product, $variation ) {
    if ( \App\woo_is_intus_vat_exempt_enabled() ) {
        $price_html = "";
        $prices_ex_vat = \App\woo_are_prices_displayed_ex_vat();

        $price_html .= '<div class="price">';
        if( $prices_ex_vat ){
            $price_html .= woo_get_product_single_price_markup( $variation, 'ex');
            $price_html .= woo_get_product_single_price_markup( $variation, 'inc');
        }else{
            $price_html = $variation->get_price_html();
        }
        $price_html .= '</div>';

        $data['price_html'] = $price_html;
    }
    return $data;
} ,100, 3);


/**
 * Add a simple disclaimer under Grouped Product price tables showing that prices are without VAT or with Relief applied
 */
//add_action( 'woocommerce_before_add_to_cart_button', function(){
//    global $product, $post;
//    if ( $product->is_type('grouped') ) {
//        echo '
//            <div class="product-grouped-tax-notice">
//                <p>' . sprintf(  __('Please see below for further information on prices with %s applied', 'sage'), \App\woo_get_vat_ex_suffix( $product, false )  ) . '</p>
//            </div>
//        ';
//
//    }
//
//}, 10 );


/**
 * Move the simple product description and wrap price / stock
 *
 * @return null
 */

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 7 );

/**
 * Move the simple product star rating up so we can insert the Product Category Quicklinks between this and the price
 *
 * @return null
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );

/**
 * Customise the sections below 'add to cart'
 *
 * @return null
 */

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

/**
 * Customise the product single tabs
 *
 * @return null
 */

add_filter( 'woocommerce_product_tabs', function($tabs) {

    global $product, $post;

    // Product tagged categories
    $terms = wp_get_post_terms($post->ID, 'product_cat', ['fields' => 'ids']);

    // Product messages
    $notices = [
        'terms' => $terms,
        'messages' => get_field('product_category_messages', 'option')
    ];

    //
    if( \App\woo_product_is_magnetic( $product ) ){
        if( $product_magnetic_message = get_field('product_magnetic_message_long', 'option') ) {
            $notices['magnetic'] = $product_magnetic_message;
        }
    }


    // Product insurance
    $insurance = get_field('product_mask_insurance', 'option');

    // Change existing tab titles
    $tabs['additional_information']['title'] = __('Details');

    // Reviews tab (for the Trustpilot review widget)
    $tabs['reviews'] = [
        'title'     => __('Reviews', 'woocommerce'),
//        'priority'  => 10,
        'callback'  => function () use ($notices) {
            echo template(locate_template('views/partials/woo/tabs/tab-reviews') );
        }
    ];

    // Description tab (pass extra content if product in certain categories)
    $tabs['description'] = [
        'title'     => __('Description', 'woocommerce'),
        'priority'  => 5,
        'callback'  => function () use ($notices) {
            echo template(locate_template('views/partials/woo/tabs/tab-description'), $notices);
        }
    ];

    // Mask Fit Insurance tab (only show if in certain categories)
    if (!empty($insurance['product_categories'])) {
        $message_shown = false;
        foreach ($insurance['product_categories'] as $category) {
            if (in_array($category, $terms) && !empty($insurance['content']) && !$message_shown) {
                $tabs['insurance'] = [
                    'title'     => __('Mask-Fit Insurance', 'woocommerce'),
                    'priority'  => 30,
                    'callback'  => function () use ($insurance) {
                        echo template(locate_template('views/partials/woo/tabs/tab-insurance'), $insurance);
                    }
                ];
                $message_shown = true;
            }
        }
    }

    // FAQs tab
    if (($faqs = get_field('tab_faqs')) && $faqs['enabled']) {
        $tabs['faqs'] = [
            'title'     => __('FAQs', 'woocommerce'),
            'priority'  => 50,
            'callback'  => function () use ($faqs) {
                echo template(locate_template('views/partials/woo/tabs/tab-questions'), $faqs);
            }
        ];
    }

    // Downloads tab
    if (($downloads = get_field('tab_downloads')) && $downloads['enabled']) {
        $tabs['downloads'] = [
            'title'     => __('Downloads', 'woocommerce'),
            'priority'  => 50,
            'callback'  => function () use ($downloads) {
                echo template(locate_template('views/partials/woo/tabs/tab-downloads'), $downloads);
            }
        ];
    }



    // Tabs array
    return array_filter([
        'description' => $tabs['description'] ?? false,
        'insurance' => $tabs['insurance'] ?? false,
        'faqs' => $tabs['faqs'] ?? false,
        'downloads' => $tabs['downloads'] ?? false,
        'additional_information' => $tabs['additional_information'] ?? false,
        'reviews' => $tabs['reviews'] ?? false
    ]);
}, 98);


/**
 * Add quicklinks to the Product Category Messages
 */
function woo_add_product_summary_notices() {
    global $product, $post;

    if( $messages = get_field('product_category_messages', 'option') ) {
        // Product tagged categories
        $terms = wp_get_post_terms($post->ID, 'product_cat', ['fields' => 'ids']);
        // Product messages
        $notices = [
            'terms' => $terms,
            'messages' => get_field('product_category_messages', 'option')
        ];

        echo template(locate_template('views/partials/woo/product-summary-notices'), $notices);
    }
}
add_action('woocommerce_single_product_summary', 'App\\woo_add_product_summary_notices', 8);
add_action( 'woocommerce_before_single_product_summary', 'App\\woo_add_product_summary_notices', 25 );

/**
 * Increase number of related products
 *
 * @ref https://docs.woocommerce.com/document/change-number-of-related-products-output/
 *
 */

add_filter('woocommerce_output_related_products_args', function() {

    $args['posts_per_page'] = 12;
    return $args;

}, 20);

/**
 * Show variation price when they are all the same
 * @ref https://github.com/woocommerce/woocommerce/issues/11827
 */

add_filter('woocommerce_show_variation_price', '__return_true');

/*
 * Change order of layout on single product page
 */

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

add_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 30);
add_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 20);

/**
 * Add a back to top link after product
 */
add_action('woocommerce_after_single_product_summary', function () {
    echo template(locate_template('views/woocommerce/single-product/back-to-top'));
}, 21);


/*
 * Remove sharing / SKU from product single
 */

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);

/*
 * Remove breadcrumbs from category page displaying subcategories only
 */

add_action('wp', function () {
    if( !is_admin() ){
        if( function_exists('is_product_category') ){
            if ( \is_product_category() && (\get_term_meta(get_queried_object_id(), 'display_type', true) === 'subcategories' ) ) {
                remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
            }
        }
    }
});

/**
 * Hide category product count in product archives
 */

add_filter( 'woocommerce_subcategory_count_html', '__return_false' );

/**
* Add custom fields
*/

add_action( 'woocommerce_product_options_inventory_product_data', function($post_id) {

    // Solus product
    woocommerce_wp_checkbox([
        'id' => '_solus_product',
        'label' => __( 'Solus product', 'woocommerce' ),
        'description' => __( 'Enable this if the product is only allowed in the basket on its own.', 'woocommerce' )
    ]);

});

/**
* Save custom fields
*/

add_action( 'woocommerce_process_product_meta', function($post_id) {

    // Product object
    $product = wc_get_product($post_id);

    // Ideal stock level
    $vat_exempt = isset($_POST['_solus_product']) ? $_POST['_solus_product'] : '';
    $product->update_meta_data('_solus_product', $vat_exempt);

    // Save product data
    $product->save();
});

/**
* Disable add to basket if a solus product exists in the basket
*/

add_filter( 'woocommerce_add_to_cart_validation', function($passed, $product_id) {

    global $woocommerce;

    // Cart items
    $solus_in_cart = false;
    $cart_items = $woocommerce->cart->get_cart();

    // Check if current item is solus product
    $is_current_solus = get_post_meta($product_id, '_solus_product', true);

    if (!empty($cart_items)) {
        if ($is_current_solus) {

            // Current item is a solus item
            wc_add_notice( __( 'This item can only be ordered as a single item; Please remove other items from the basket.', 'woocommerce' ), 'error' );
            $passed = false;

        } else {
            foreach($cart_items as $item => $values) {
                if (get_post_meta($values['data']->get_id(), '_solus_product', true)) {
                    $solus_in_cart = $values['data']->get_name();
                }
            }

            // There is a solus item in the basket already
            if (!empty($solus_in_cart)) {
                wc_add_notice( __( '"' . $solus_in_cart . '" is already in your basket and can only be ordered as a single item.', 'woocommerce' ), 'error' );
                $passed = false;
            }
        }
    }

    return $passed;

}, 10, 5 );

/**
* Change stock label text
*/

add_filter('woocommerce_get_availability_text', function($text, $product) {

    $stock = $product->get_stock_quantity();

    if ($stock < 1 && $product->backorders_allowed()) {
        $text = __('Back-order', 'woocommerce');
    } elseif ($product->is_in_stock()) {
        $text = __('In Stock', 'woocommerce');
    } elseif (!$product->is_in_stock()) {
        $text = __('Out of Stock', 'woocommerce');
    }

    return $text;

}, 10, 2 );



/**
* Add a new meta field for grouped and variable products that have children on sale
*/

add_action('woocommerce_update_product', 'App\\set_product_sale_meta', 10, 2);
add_action('woocommerce_create_product', 'App\\set_product_sale_meta', 10, 2);

function set_product_sale_meta($product_id, $product) {

    // Only for variable and grouped products
    if ($product->is_type('variable') || $product->is_type('grouped')) {

        // Set false variable to update and get parent product
        $child_on_sale = false;

        // Get children attached to product
        $children = $product->get_children();

        // Loop through children and update our variable if any are on sale
        if (!empty($children)) {
            foreach ($children as $child_id) {
                $child_product = wc_get_product($child_id);

                if (!empty($child_product) && $child_product->is_on_sale()) {
                    $child_on_sale = true;
                    break;
                }
            }
        }

        // Add custom meta field if any children on sale
        if ($child_on_sale) {
            update_post_meta($product_id, '_child_on_sale', 1);
        } elseif (!empty(get_post_meta($product_id, '_child_on_sale'))) {
            update_post_meta($product_id, '_child_on_sale', 0);
        }
    }
}

/**
* Change stock label text
*/

add_action('woocommerce_single_product_summary', function() {

    global $product;

    $link = '<a href="' . get_permalink(get_option('woocommerce_myaccount_page_id')) . '" class="text-underline color-secondary" target="_blank">' . __('logged in or create an account', 'sage') . '</a>';

    // We can't do a normal is_type() check, because we have a plugin that lets you make normal variable products into subscription without changing the actual product type
    if (class_exists('WCS_ATT_Product_Schemes') && WCS_ATT_Product_Schemes::has_subscription_schemes($product)) {
        echo '<p class="mb-0 p-3 text-center text-italic">';
            echo __('If you choose a recurring subscription of this product, you must be', 'sage');
            echo '&nbsp;' . $link . '&nbsp;';
            echo __('to complete your purchase in the checkout area.', 'sage');
        echo '</p>';
    }

}, 30);


add_action('woocommerce_single_product_summary', function() {
    global $product;
    if( \App\woo_product_is_magnetic( $product ) ) {
        $field = "product_magnetic_message_short";
        if( $message = get_field($field, 'option') ) {
            echo "<div class='{$field}'>$message</div>";
        }
    }
}, 7);


/**
 * Change woocommerce_ajax_variation_threshold to allow for very complex variable products
 */
add_filter( 'woocommerce_ajax_variation_threshold', function( $qty, $product ){
    return 100;
}, 100, 2);
