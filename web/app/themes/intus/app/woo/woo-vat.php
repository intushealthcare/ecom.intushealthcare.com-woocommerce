<?php

namespace App;

use WC_Countries;


/**
 * @return mixed
 */
function woo_is_intus_vat_exempt_enabled()
{
    return get_field( 'enable_vat_exempt', 'option' );
}


/**
 * Is WooCommerce configured to show prices inc or ex vat?
 * @return false|mixed|void
 */
function woo_are_prices_displayed_ex_vat()
{

    return ( get_option( 'woocommerce_tax_display_shop' ) == 'excl' ) ? true : false;
}

/**
 * Reusable function to get the correct product ID to use for checking VAT status
 *
 * @param $product
 *
 * @return bool
 */
function woo_is_product_vat_exempt( $product ) {
    /** @var \WC_Product $product_id */
    $product_id = ( $product->is_type( 'variation' ) ) ? $product->get_parent_id() : $product->get_id();
    return ( get_post_meta( $product_id, '_vat_exempt', true ) === 'yes' ) ? true : false;
}


/**
 * @return bool
 */
function woo_is_product_show_inc_vat_enabled() {
    return (boolean) get_field('product_show_inc_vat', 'option');
}



/**
 * @param $product
 *
 * @return string
 */
function woo_get_product_vat_exempt_suffix( $product ) {
    $vat_exempt = woo_is_product_vat_exempt( $product );
    $product_exempt_class = "__notexempt";
    if( $vat_exempt ){
        $product_exempt_class = "__exempt";
    }
    return $product_exempt_class;
}


/**
 * Get the appropriate VAT / VAT Relief suffix according to WooCommerce price display options
 *
 * @param $product
 * @param $long
 *
 * @return string
 */
function woo_get_vat_suffix( $product = null, $long = true )
{
    if ( woo_are_prices_displayed_ex_vat() ) {
        return woo_get_vat_ex_suffix( $product, $long = true );
    } else {
        return woo_get_vat_inc_suffix( $product, $long = true );
    }
}


/**
 * @param $product
 * @param $long
 *
 * @return string
 */
function woo_get_vat_ex_suffix( $product = null, $long = true )
{
    $vat_suffix = __( 'Ex. VAT', 'sage' );
    $vat_exempt = woo_is_product_vat_exempt( $product );
    $product_exempt_class = woo_get_product_vat_exempt_suffix( $product );
    if ( $product && $vat_exempt ) {
        $vat_suffix = woo_get_ex_vat_label();
        if ( is_product() && $long ) {
//            $vat_suffix .= " " . __( '(see below)', 'sage' );
        }
    }

    return "<small class='woocommerce-price-suffix woocommerce-price-suffix__ex woocommerce-price-suffix{$product_exempt_class}'>{$vat_suffix}</small>";
}


/**
 * @param $product
 * @param $long
 *
 * @return string
 */
function woo_get_vat_inc_suffix( $product = null, $long = true )
{
    $vat_suffix = __( 'Inc. VAT', 'sage' );
    $vat_exempt = woo_is_product_vat_exempt( $product );
    $product_exempt_class = woo_get_product_vat_exempt_suffix( $product );
    if( $vat_exempt ){
        $vat_suffix = woo_get_inc_vat_label();
//        if( is_product() && $long ){
//            $vat_suffix .= " " . __('(see below)', 'sage');
//        }
    }

    return "<small class='woocommerce-price-suffix woocommerce-price-suffix__inc woocommerce-price-suffix{$product_exempt_class}'>{$vat_suffix}</small>";
}


/**
 * @param $product
 *
 * @return void
 */
function woo_get_intus_product_price( $product ) {

    $vat_exempt = woo_is_product_vat_exempt($product);

    if ($vat_exempt == 'yes') {
        $before_price = wc_get_price_excluding_tax($product, ['price' => $product->get_regular_price()]);
        $vat_price = wc_price(wc_get_price_excluding_tax($product));
    } else {
        $before_price = wc_get_price_including_tax($product, ['price' => $product->get_regular_price()]);
        $vat_price = wc_price(wc_get_price_including_tax($product));
    }

    if ($product->is_on_sale()) {
        $price = wc_format_sale_price($before_price, $vat_price);
    } else {
        $price = $vat_price;
    }
    return $price;
}



/**
 * Reusable function to check if there are eligible VAT relief products in cart
 *
 * @return bool
 */
function woo_check_vat_eligible_in_cart()
{

    global $woocommerce;

    // Get cart items
    $items = $woocommerce->cart->get_cart();

    // Determine if there are any eligible products in the cart
    if ( ! empty( $items ) ) {
        foreach ( $items as $item => $values ) {
            $product = wc_get_product( $values[ 'data' ]->get_id() );
            if ( woo_is_product_vat_exempt( $product ) ) {
                return true;
            }
        }
    }

    return false;
}

/**
 * Reusable function to set cart items to zero vat
 */

function woo_set_vat_eligible_rate( $rate = 'zero-rate' )
{

    global $woocommerce;

    $items = $woocommerce->cart->get_cart();

    // Set eligible items to zero vat
    if ( ! empty( $items ) ) {
        foreach ( $items as $item => $values ) {

            $product = wc_get_product( $values[ 'data' ]->get_id() );

            if ( woo_is_product_vat_exempt( $product ) ) {
                $values[ 'data' ]->set_tax_class( $rate );
            }
        }
    }

}


/**
 * @param bool $value
 * @return void
 */
function set_is_vat_claimed_cookie( bool $value ){
    $checkbox_name = 'vat_exempt_declaration_checkbox';
    if( $value ){
        setcookie($checkbox_name, true, null, '/');
    }else{
        setcookie($checkbox_name, false, -1, '/');
    }
}

/**
 * @return bool
 */
function is_vat_exemption_claimed() {
    $checkbox_name = 'vat_exempt_declaration_checkbox';
    $checked = false;
    if ( isset( $_COOKIE[$checkbox_name] ) ){
        $checked = (bool) $_COOKIE[$checkbox_name];
    }
    return $checked;
}


/**
 * Add a custom Intus Specific Options tab to the Admin Product Edit panel
 */
add_filter( 'woocommerce_product_data_tabs', function ( $tabs ) {
    global $post;

    $tab = array(
        'intus_tab' => array(
            'label'       => esc_html__( 'Intus Specific Options', 'sage' ),
            'target'      => 'intus_product_data', // ID of tab field
            'priority'    => 10000,
            'class'       => [],
        ),
    );

    return array_merge( $tabs, $tab );

}, 10, 1 );


/**
 * Product custom fields to the Admin Product Edit Intus Specific Options tab
 *
 * @return void
 */
add_action( 'woocommerce_product_data_panels', function () {
    ?>
    <div id="intus_product_data" class="panel woocommerce_options_panel">
        <div class="options_group _vat_exempt">
            <?php woocommerce_wp_checkbox( [
                'id'          => '_vat_exempt',
                'label'       => __( 'VAT Exempt', 'sage' ),
                'description' => __( 'Enable this if the product should have the VAT exempt status option in the checkout.', 'woocommerce' ),
            ] ) ?>
        </div>
    </div>
    <?php
} );


/**
 * Save the custom fields
 */

add_action( 'woocommerce_process_product_meta', function ( $post_id ) {

    // Product object
    $product = wc_get_product( $post_id );

    // Ideal stock level
    $vat_exempt = isset( $_POST[ '_vat_exempt' ] ) ? $_POST[ '_vat_exempt' ] : '';
    $product->update_meta_data( '_vat_exempt', $vat_exempt );

    // Save product data
    $product->save();
} );

/**
 * Display product VAT status on basket item
 */

add_action( 'woo_cart_item_vat_exempt_status', function ( $product ) {
    // VAT custom field
    $vat_exempt = woo_is_product_vat_exempt( $product );
    $message = __( 'Not eligible for VAT relief', 'sage' );
    if ( $vat_exempt ) {
        $message = __( 'Eligible for VAT relief, claim below', 'sage' );
    }
    $color      = ( $vat_exempt ) ? 'color-success' : 'color-error';
    echo "<div class='d-flex align-items-center my-4 {$color}'>";
    echo "<i class='fas fa-check-circle mr-3'></i><span>{$message}</span>";
    echo '</div>';
} );


/**
 * Add VAT relief notice to product single
 */
function woo_add_product_notice_vat_relief() {
    // Vat content
    $vat_content = get_field( 'product_vat_relief', 'option' );

    // VAT custom field
    $vat_exempt = get_post_meta( get_queried_object_id(), '_vat_exempt', true );

    if ( ! empty( $vat_content[ 'has_relief_content' ] ) && ! empty( $vat_content[ 'no_relief_content' ] ) ) {

        // Add VAT relief statement to product
        echo \App\template( locate_template( 'views/partials/woo/product-notice' ), [
            'classes' => 'product-notice--vat-relief mb-5',
            'icon'    => ( $vat_exempt === 'yes' ) ? 'check' : 'times',
            'content' => ( $vat_exempt === 'yes' ) ? $vat_content[ 'has_relief_content' ] : $vat_content[ 'no_relief_content' ],
        ] );

    }
}
add_action( 'woocommerce_single_product_summary', 'App\\woo_add_product_notice_vat_relief', 8 );
add_action( 'woocommerce_before_single_product_summary', 'App\\woo_add_product_notice_vat_relief', 26 );


/**
 * Add VAT relief message to cart page
 */
add_action( 'woo_cart_vat_exempt_message', function () {
    // Check eligible items in cart
    $show_declaration = woo_check_vat_eligible_in_cart();

    // Checkbox
    $checkbox_name = 'vat_exempt_declaration_checkbox';
    $checked = is_vat_exemption_claimed();

    if( $show_declaration ){
        // VAT message content
        $vat_message = \get_field( 'cart_vat_message', 'option' );
        if ( $show_declaration && ! empty( $vat_message ) ) {
            $checkbox_copy = __('Tick the box to declare you are eligible for VAT&nbsp;relief.', 'sage');
            woo_show_short_vat_declaration( $checked, $checkbox_name, $checkbox_copy );
        }
    }

} );


/**
 * Filter the shipping rates and modify tax on shipping applied as appropriate
 */
add_action( 'woocommerce_before_calculate_totals', function ( $cart ) {
    if ( is_admin() || wp_doing_ajax() ){
        return;
    }

    $checked = is_vat_exemption_claimed();
    if ( $checked ) {
        woo_set_vat_eligible_rate();
        add_filter( 'woocommerce_package_rates', 'App\\reduce_shipping_tax_to_zero', 100 );
    } else {
        woo_set_vat_eligible_rate( 'standard' );
    }

}, 10, 1 );


/**
 * Show the checkout VAT relief statement on checkout if eligible products, or not eligible content via AJAX
 */

add_action( 'wp_ajax_woo_show_vat_declaration', 'App\\woo_ajax_is_country_vat_exempt' );
add_action( 'wp_ajax_nopriv_woo_show_vat_declaration', 'App\\woo_ajax_is_country_vat_exempt' );

function woo_ajax_is_country_vat_exempt()
{
    // Check eligible items in cart
    $show_declaration = woo_check_vat_eligible_in_cart();

    // Checkbox
    $checkbox_name = 'vat_exempt_declaration_checkbox';
    $checked = is_vat_exemption_claimed();

    // Un-eligible countries
    $exclude_countries = get_field( 'disable_vat_countries', 'option' );

    // Billing country
    $billing_country =  $_POST[ 'billing_country' ] ?? '';

    if ( ! empty( $billing_country ) && in_array( $billing_country, $exclude_countries ) ) {
        set_is_vat_claimed_cookie( false );
        woo_show_country_not_eligible();
    } elseif ( $show_declaration ) {
        woo_show_vat_declaration( $checked, $checkbox_name );
    }

    die();
}


/**
 * Show the checkout VAT relief statement on checkout if eligible products, or not eligible content
 */

add_action( 'woocommerce_after_order_notes', function ( $checkout ) {
    // Check eligible items in cart
    $show_declaration = woo_check_vat_eligible_in_cart();

    // Checkbox
    $checkbox_name = 'vat_exempt_declaration_checkbox';
    $checked = is_vat_exemption_claimed();

    // Un-eligible countries
    $exclude_countries = get_field( 'disable_vat_countries', 'option' );

    // Billing country
    $billing_country = ( $checkout->get_value( 'billing_country' ) ) ? $checkout->get_value( 'billing_country' ) : '';

    echo '<div id="checkout-vat-declaration">';
    if ( ! empty( $billing_country ) && in_array( $billing_country, $exclude_countries ) ) {
        set_is_vat_claimed_cookie( false );
        woo_show_country_not_eligible();
    } elseif ( $show_declaration ) {
        woo_show_vat_declaration( $checked, $checkbox_name );
    }
    echo '</div>';

}, 10, 1 );



/**
 * Show a note at the top of the cart to remind people to check the VAT declaration if eligible
 */
add_action( 'woocommerce_checkout_vat_declaration_warning', function ( $checkout ) {
    // Check eligible items in cart
    $show_declaration = woo_check_vat_eligible_in_cart();
    if ( $show_declaration ) {
        echo \App\template( locate_template( 'views/partials/woo/product-notice-checkout-claim-vat' ) );
    }
}, 10, 1 );


/**
 * Functions used in the on load and ajax responses for eligible status
 */
function woo_show_short_vat_declaration( $checked, $checkbox_name, $checkbox_text = null )
{
    if( is_null( $checkbox_text ) ){
        $checkbox_text = __( 'I submit the above declaration and wish to claim VAT relief', 'sage' );
    }
    // Declaration fields
    $declaration = get_field( 'checkout_vat_declaration', 'option' );

    if ( ! empty( $declaration ) ) {
        // Checkbox field
        $checkbox = woocommerce_form_field( $checkbox_name, [
            'return'     => true,
            'type'     => 'checkbox',
            'class'    => [ 'form-row--declaration form-row--declaration-short' ],
            'label'    => $checkbox_text,
            'required' => false,
        ], $checked );

        echo \App\template( locate_template( 'views/partials/woo/product-notice-cart-claim-vat' ), [
            'declaration' => $declaration,
            'checkbox' => $checkbox,
        ] );
    }

}



/**
 * Functions used in the on load and ajax responses for eligible status
 */
function woo_show_vat_declaration( $checked, $checkbox_name, $checkbox_text = null )
{
    if( is_null( $checkbox_text ) ){
        $checkbox_text = __( 'I declare that I am eligible to claim VAT relief', 'sage' );
    }
    // Declaration fields
    $declaration = get_field( 'checkout_vat_declaration', 'option' );

    if ( ! empty( $declaration ) ) {
        // Checkbox field
        $checkbox = woocommerce_form_field( $checkbox_name, [
            'return'     => true,
            'type'     => 'checkbox',
            'class'    => [ 'form-row--declaration form-row--declaration-short' ],
            'label'    => $checkbox_text,
            'required' => false,
        ], $checked );

        echo \App\template( locate_template( 'views/partials/woo/product-notice-cart-claim-vat' ), [
            'declaration' => $declaration,
            'checkbox' => $checkbox,
        ] );
    }

//    if ( ! empty( $declaration ) ) {
//
//        // Declaration
//        echo '<div class="product-notice product-notice--declaration">';
//
//        // Content
//        echo $declaration;
//
//        // Checkbox field
//        woocommerce_form_field( $checkbox_name, [
//            'type'     => 'checkbox',
//            'class'    => [ 'form-row--declaration' ],
//            'label'    => $checkbox_text,
//            'required' => false,
//        ], $checked );
//
//        echo '</div>';
//
//    }

}

function woo_show_country_not_eligible()
{

    // Not eligible fields
    $not_eligible = get_field( 'checkout_not_eligible', 'option' );

    if ( ! empty( $not_eligible ) ) {
        echo '<div class="product-notice product-notice--declaration">' . $not_eligible . '</div>';
    }

}

function woo_get_ex_vat_label()
{
    return get_field( 'ex_vat_label', 'option');
}

function woo_get_inc_vat_label()
{
    return get_field( 'inc_vat_label', 'option');
}

/**
 * Set the eligible items to zero vat when declaration checked
 */
add_action( 'woocommerce_checkout_update_order_review', function ( $post_data ) {

    // Parse data
    parse_str( $post_data, $result );

    $checkbox_name = 'vat_exempt_declaration_checkbox';
    $checked  = is_vat_exemption_claimed();

    // VAT exempt declaration checked
    if ( ( isset( $result[ $checkbox_name ] ) && $result[ $checkbox_name ] == '1' ) || $checked ) {
        woo_set_vat_eligible_rate();
        add_filter( 'woocommerce_package_rates', 'App\\reduce_shipping_tax_to_zero', 100 );
    } else {
        woo_set_vat_eligible_rate( 'standard' );
    }

} );

/**
 * Save the correct vat declaration values when user has checked out
 */

add_action( 'woocommerce_checkout_process', function () {

    global $woocommerce;

    // Check post data for declaration
    $vat_declaration = isset( $_POST[ 'vat_exempt_declaration_checkbox' ] ) ? $_POST[ 'vat_exempt_declaration_checkbox' ] : 0;

    // Set items to zero and calculate totals
    if ( isset( $vat_declaration ) && $vat_declaration == '1' ) {
        woo_set_vat_eligible_rate();
        $woocommerce->cart->calculate_totals();
    }

} );


/**
 * Save custom meta data to order
 */

add_action( 'woocommerce_checkout_update_order_meta', function ( $order_id ) {

    $declaration_name = get_field( 'vat_declaration_name', 'option' );
    if ( ! empty( $_POST[ 'vat_exempt_declaration_checkbox' ] ) && ! empty( $declaration_name ) ) {
        update_post_meta( $order_id, '_vat_exempt_declaration', sanitize_text_field( $declaration_name ) );
    }

} );

/**
 * Display the VAT declaration in the Orders area
 */

add_action( 'woocommerce_admin_order_data_after_shipping_address', function ( $order ) {

    $declaration = get_post_meta( $order->get_id(), '_vat_exempt_declaration', true );

    if ( ! empty( $declaration ) ) {
        echo '<h3>' . __( 'VAT Declaration', 'sage' ) . '</h3>';
        echo '<p>';
        echo __( 'Customer e-signed declaration: ', 'sage' );
        echo '<br><strong>' . $declaration . '</strong>';
        echo '</p>';
    }

}, 10, 1 );

/**
 * Dynamically load the WooCommerce countries into an ACF Select box
 */
add_filter( 'acf/load_field/name=disable_vat_countries', function ( $field ) {

    // reset choices
    $field[ 'choices' ] = array();

    // Get allowed countries for shop
    $country_obj = new WC_Countries();
    $countries   = $country_obj->get_allowed_countries();

    // loop through array and add to field 'choices'
    if ( is_array( $countries ) ) {
        foreach ( $countries as $key => $country ) {
            $field[ 'choices' ][ $key ] = $country;
        }
    }
    return $field;

} );

/**
 * Show non VAT exempt products as VAT included
 */
add_filter('woocommerce_get_price_html', function($price, $product) {
    if( is_product_category() ) {
        if (!$product->is_type('grouped')) {
            return \App\woo_get_intus_product_price( $product );
        }
    }
    return $price;

}, 100, 2);

/**
 * Modify price output for grouped products
 */
add_filter('woocommerce_get_price_html', function( $price, $product) {
    global $wp;
    if( is_product() ) {
        $url = home_url( $wp->request );
        $post_id = url_to_postid( $url );
        $parent_product = wc_get_product( $post_id );
        if ( $parent_product && ( $parent_product->is_type( 'grouped' ) ) ) {
            return \App\woo_get_intus_product_price( $product );
        }
    }
    return $price;
}, 110, 2);


/**
 * Alter shipping costs depending on if the user has declared VAT exempt
 */

function reduce_shipping_tax_to_zero( $rates )
{

    foreach ( $rates as $rate_key => $rate ) {
        $new_taxes = [];

        if ( ! empty( $rates[ $rate_key ]->taxes ) ) {
            foreach ( $rates[ $rate_key ]->taxes as $key => $tax ) {
                $new_taxes[ $key ] = 0;
            }

            $rates[ $rate_key ]->taxes = $new_taxes;
        }
    }

    return $rates;

}


/**
 * For some user-agents we want to automatically minimise tax rates
 * @return bool
 */
function is_user_agent_set_to_minimise_tax() {
    if( is_admin() ){
        return false;
    }

    $minimise_tax = false;
    $disallowed_partial_matches = [ 'mozilla', 'chrome', 'safari', 'iphone', 'android' ];
    if ( get_field( 'bot_vat_enabled', 'option' ) === true ) {
        $user_agent = strtolower( $_SERVER['HTTP_USER_AGENT'] ) ?? null;
        $bot_user_agent_strings = [];

        $bot_user_agents = get_field( 'bot_user_agents', 'option' );

        foreach ( $bot_user_agents as $bot_user_agents ) {
            $bot_user_agent = strtolower( trim($bot_user_agents['user_agent']) );
            if( !in_array( $bot_user_agent, $disallowed_partial_matches ) ){
                $bot_user_agent_strings[] = $bot_user_agent;
            }
        }

        if( $user_agent ){
            foreach ( $bot_user_agent_strings as $bot_user_agent_string ) {
                if( str_contains( $user_agent, $bot_user_agent_string ) ){
                    $minimise_tax = true;
                    break;
                }
            }
        }
    }
    return $minimise_tax;
}

/**
 * Change the tax rate of VAT Exempt products to 0 if specific user-agent matches are detected
 */
add_filter( 'woocommerce_cart_totals_get_item_tax_rates', function (  $item_tax_rates, $item, \WC_Cart $cart ) {
    if( \App\woo_is_intus_vat_exempt_enabled() ){
        if( \App\is_user_agent_set_to_minimise_tax() ){
            $reduced_tax_rate_for_user_agent = 0.0;
            $new_tax_rates = [];
            if( \App\woo_is_product_vat_exempt( $item->product ) ) {
                foreach ( $item_tax_rates as $item_tax_rate ) {
                    $item_tax_rate['rate'] = $reduced_tax_rate_for_user_agent;
                    $new_tax_rates[] = $item_tax_rate;
                }
                return $new_tax_rates;
            }
        }
    }

    return  $item_tax_rates;
}, 10, 3 );
