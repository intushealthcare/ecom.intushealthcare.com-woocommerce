<?php

namespace App;

/**
* Remove downloads from the account area
*/

add_filter( 'woocommerce_account_menu_items', function($items) {
    unset($items['downloads']);
    return $items;
}, 999 );

/**
* Add new columns to the woocommerce order screen
*/

add_filter( 'manage_edit-shop_order_columns', function($columns) {

    $new_columns = [];

    foreach ($columns as $key=>$column) {
        $new_columns[$key] = $columns[$key];

        if ($key === 'order_status') {
            $new_columns['order_lineitems'] = __('Line Items', 'sage');
        }
    }
    return $new_columns;

});

add_action('manage_shop_order_posts_custom_column', function($column) {

    global $the_order, $post, $woocommerce;

    if ($column == 'order_lineitems') {

        $order_items = $the_order->get_items();

        if (!empty($order_items)) {

            $lines = '';

            foreach ($order_items as $order_item) {

                $product_id = $order_item['product_id'];
                $stock_location = get_post_meta($product_id, '_stock_location', false);

                $lines .= $order_item['quantity'] . '&nbsp;&times;&nbsp;';
                $lines .= '<a href="' . get_edit_post_link($product_id) . '">' . $order_item['name'] . '</a>';

                if (!empty($stock_location)) {
                    if( is_array( $stock_location ) ){
                        $str_location = "";
                        foreach ( $stock_location as $location ){
                            if( !empty( $location) ){
                                $str_location .= $location;
                            }
                        }
                        $stock_location = $str_location;
                    }
                    if( $stock_location ){
                        $lines .= '&nbsp;<i>(' . $stock_location . ')</i>';
                    }
                }

                $lines .= '<br>';
            }

            echo $lines;
        }
    }

}, 99);

/**
* Add the custom fields to the WooCommerce inventory area (all products)
*/

add_action( 'woocommerce_product_options_general_product_data', function() {

    // Ideal stock level
    woocommerce_wp_text_input([
        'id' => '_ideal_stock',
        'type' => 'number',
        'label' => __( 'Ideal stock quantity', 'woocommerce' ),
        'description' => __( 'Ideal stock quantity. This value will be used to determine the default quantity of this product in purchase orders.', 'woocommerce' ),
        'desc_tip' => true
    ]);

    // Stock Location
    woocommerce_wp_text_input([
        'id' => '_stock_location',
        'type' => 'text',
        'label' => __( 'Stock location', 'woocommerce' ),
        'description' => __( 'Warehouse stock location.', 'woocommerce' ),
        'desc_tip' => true
    ]);

});

/**
* Save the custom fields in the WooCommerce meta area
*/

add_action( 'woocommerce_process_product_meta', function($post_id) {

    // Product object
    $product = wc_get_product($post_id);

    // Ideal stock level
    $ideal_stock_qty = isset($_POST['_ideal_stock']) ? $_POST['_ideal_stock'] : '';
    $product->update_meta_data('_ideal_stock', $ideal_stock_qty);

    // Stock location
    $field_stock_location = isset($_POST['_stock_location']) ? $_POST['_stock_location'] : '';
    $product->update_meta_data('_stock_location', $field_stock_location);

    // Save product data
    $product->save();
});

/**
* Add the custom fields to the WooCommerce top area (variable products)
*/

add_action( 'woocommerce_variation_options_inventory', function($loop, $variation_data, $variation) {

    // Ideal stock
    woocommerce_wp_text_input([
        'id' => '_ideal_stock[' . $loop . ']',
        'wrapper_class' => 'form-row form-row-first',
        'type' => 'number',
        'label' => __( 'Ideal stock quantity', 'woocommerce' ),
        'description' => __( 'Ideal stock quantity. This value will be used to determine the default quantity of this product in purchase orders.', 'woocommerce' ),
        'desc_tip' => true,
        'value' => get_post_meta($variation->ID, '_ideal_stock', true)
    ]);

    // Stock location
    woocommerce_wp_text_input([
        'id' => '_stock_location[' . $loop . ']',
        'wrapper_class' => 'form-row form-row-last',
        'type' => 'text',
        'label' => __( 'Stock location', 'woocommerce' ),
        'description' => __( 'Product warehouse location', 'woocommerce' ),
        'desc_tip' => true,
        'value' => get_post_meta($variation->ID, '_stock_location', true)
    ]);

}, 10, 3);

/**
* Save the custom fields in the WooCommerce variation area (variable products)
*/

add_action( 'woocommerce_save_product_variation', function($variation_id, $i) {

    // Ideal stock level
    $ideal_stock_qty = isset($_POST['_ideal_stock'][$i]) ? $_POST['_ideal_stock'][$i]: '';
    update_post_meta($variation_id, '_ideal_stock', $ideal_stock_qty);

    // Stock location
    $field_stock_location = isset($_POST['_stock_location'][$i]) ? $_POST['_stock_location'][$i] : '';
    update_post_meta($variation_id, '_stock_location', esc_attr( $field_stock_location ));

}, 10, 2 );

/**
* ATUM Inventory hook
*/

add_action( 'atum/atum_order/item_values', function(\WC_Product $product, \WC_Order_Item_Product $item) {
    $current_stock = (int) $product->get_stock_quantity() ?: 0;
    $ideal_stock = (int) $product->get_meta('_ideal_stock') ?: 0;
    $target_stock = max($product->get_min_purchase_quantity(), $ideal_stock - $current_stock);
    $product_price = apply_filters( 'atum/order/add_product/price', wc_get_price_excluding_tax( $product, [ 'qty' => $target_stock ] ), $target_stock, $product, null );

    $item->set_subtotal($product_price);
    $item->set_total($product_price);
    $item->set_quantity($target_stock);
}, 20, 2 );


/**
 * Performance issues mean we should limit woocommerce order search fields.
 *
 * @param $search_fields
 *
 * @return array
 */
add_filter( 'woocommerce_shop_order_search_fields', function( $search_fields ){
    unset( $search_fields );
    $search_fields[] = '_shipping_address_index';
    $search_fields[] = '_invoice_number';
//    $search_fields[] = '_invoice_number_display';
    $search_fields[] = '_billing_email';
    $search_fields[] = '_billing_last_name';
    return $search_fields;
}, 100, 1);


/**
 * Enable product revisions
 */
add_filter( 'woocommerce_register_post_type_product', function ( $args ) {
    $args['supports'][] = 'revisions';
    return $args;
} );
