<?php

namespace App;

// Check whether WordPress and ACF are available; bail if not.
if ( !function_exists( 'acf_register_block_type' ) ) {
    return;
}
if ( !function_exists( 'add_filter' ) ) {
    return;
}
if ( !function_exists( 'add_action' ) ) {
    return;
}


//These named colours will be made available when choosing text or background colours in Gutenberg blocks
define('THEME_GUTENBERG_BLOCK_COLORS', [
    'primary',
    'primary_light',
    'primary_dark',
    'secondary',
    'secondary_light',
    'tertiary',
]);

/**
 * Adds a list of predefined colors to the Gutenberg editor
 * @return array
 */
function gutenberg_color_palette()
{
    $custom_colors = [
        [
            'name' => __('Primary', 'sage'),
            'slug' => 'primary',
            'color' => '#FC9894',
        ],
        [
            'name' => __('Primary Light', 'sage'),
            'slug' => 'primary_light',
            'color' => '#FEDBDA',
        ],
        [
            'name' => __('Primary Dark', 'sage'),
            'slug' => 'primary_dark',
            'color' => '#F1726D',
        ],
        [
            'name' => __('Secondary', 'sage'),
            'slug' => 'secondary',
            'color' => '#30357C',
        ],
        [
            'name' => __('Secondary Light', 'sage'),
            'slug' => 'secondary_light',
            'color' => '#dbe7e8',
        ],
        [
            'name' => __('Tertiary', 'sage'),
            'slug' => 'tertiary',
            'color' => '#778FA9',
        ],
        [
            'name' => __('White', 'sage'),
            'slug' => 'white',
            'color' => 'rgb(252,252,252)',
        ],
        [
            'name' => __('Black', 'sage'),
            'slug' => 'black',
            'color' => '#000000',
        ],
        [
            'name' => __('Dark Grey', 'sage'),
            'slug' => 'dark-grey',
            'color' => '#2c2c2c',
        ],
        [
            'name' => __('Mid Grey', 'sage'),
            'slug' => 'mid-grey',
            'color' => '#979797',
        ],
        [
            'name' => __('Light Grey', 'sage'),
            'slug' => 'light-grey',
            'color' => '#E9EDEE',
        ],

    ];

    //Maybe we have defined some custom colours in ACF?
    if (function_exists('get_field')) {
        //We don't want to insert every possible custom color int othe
        $important_colors = THEME_GUTENBERG_BLOCK_COLORS;
        $acf_colors = [];
        $theme_colors = get_field('theme_colours', 'option');
        if ($theme_colors) {
            foreach ($theme_colors as $key => $color) {
                if (in_array($key, $important_colors)) {
                    $acf_colors[] = [
                        'name' => ucwords($key),
                        'slug' => $key,
                        'color' => $color,
                    ];
                }
            }
            // Merge and flip the array so the ACF defined colours appear first
            $custom_colors = array_reverse(array_merge($custom_colors, $acf_colors));
        }
    }
    return $custom_colors;
}


/**
 * All Gutenberg blocks can have additional classes added to them...
 *
 * @param $block
 *
 * @return string
 */
function sacouk_get_block_additional_classes($block)
{
    $classes = [];

    if (!empty($block['className'])) {
        $classes = array_merge($classes, explode(' ', $block['className']));
    }
    if (!empty($block['align'])) {
        $classes[] = 'align' . $block['align'];
    }
    if (!empty($block['backgroundColor'])) {
        $classes[] = 'has-background';
        $classes[] = 'has-' . $block['backgroundColor'] . '-background-color';
    }
    if (!empty($block['textColor'])) {
        $classes[] = 'has-text-color';
        $classes[] = 'has-' . $block['textColor'] . '-color';
    }
    $classes = array_map('trim', $classes);
    $classes = implode(' ', $classes);
    return $classes;
}


//Add CSS variables derived from ACF Colour options
add_action('wp_head', function() {
    if( function_exists('get_field') ){
        $theme_colors = gutenberg_color_palette();
        if ($theme_colors) {
            echo \App\template( locate_template( 'views/partials/css/custom'), [
                'theme_colors' => $theme_colors,
            ] );
        }
    }
}, 9999);
