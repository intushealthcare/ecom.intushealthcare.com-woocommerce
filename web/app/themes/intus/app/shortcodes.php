<?php

namespace App;

add_shortcode( 'intus_signup_form', function( $atts, $content ){
    if( strlen( $content ) === 0 ){
        $content = "Let’s stay in touch";
    }

    $tandc = get_field('newsletter_terms', 'option');

    return \App\template( locate_template( 'views/partials/newsletter-signup' ), [
        'footer_fields' => [
            'newsletter'        => "<h2><span class='text-lighter'>{$content}</span></h2>",
            'newsletter_terms'  =>$tandc
        ]
    ] );
});

