<?php

namespace App;


/**
 * @return array
 */
function trustpilot_get_configuration( $widget_name = null )
{
    $config = [];
    $config['url'] = get_field( 'trustpilot_reviews_url' ,'option' );
    $config['id'] = get_field( 'trustpilot_business_unit_id' ,'option' );
    $config['widget_id'] = get_field( 'trustpilot_' . $widget_name . '_widget_id' ,'option' );
    return $config;
}


/**
 * @param $widget_name
 *
 * @return mixed|void
 */
function trustpilot_get_widget_html( $widget_name = 'horizontal')
{
    $config = trustpilot_get_configuration( $widget_name );
    if( !empty( $config['widget_id'] ) ){
        if ( is_product() ){
            global $product;
            $product_ids = [];
            $product_ids[] = $product->get_sku();
            $product_type = $product->get_type();
            if( $product_type == 'variable' ){
                $child_products = $product->get_children();
                foreach ( $child_products as $child_product ) {
                    $child = wc_get_product( $child_product );
                    $product_ids[] = $child->get_sku();
                }
            }
            $config['product_id'] = implode(',' , $product_ids );
        }
        return \App\template( locate_template( 'views/partials/trustpilot/widget/' . $widget_name ), ['config' => $config] );
    }
}
