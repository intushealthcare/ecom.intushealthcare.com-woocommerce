<?php
// Register Custom Post Type
function tdr_register_faqs_pt() {

	$labels = array(
		'name'                  => _x( 'FAQs', 'Post Type General Name', 'sage' ),
		'singular_name'         => _x( 'FAQ', 'Post Type Singular Name', 'sage' ),
		'menu_name'             => __( 'FAQs', 'sage' ),
		'name_admin_bar'        => __( 'FAQs', 'sage' ),
		'archives'              => __( 'Item Archives', 'sage' ),
		'attributes'            => __( 'Item Attributes', 'sage' ),
		'parent_item_colon'     => __( 'Parent Item:', 'sage' ),
		'all_items'             => __( 'All Items', 'sage' ),
		'add_new_item'          => __( 'Add New Item', 'sage' ),
		'add_new'               => __( 'Add New', 'sage' ),
		'new_item'              => __( 'New Item', 'sage' ),
		'edit_item'             => __( 'Edit Item', 'sage' ),
		'update_item'           => __( 'Update Item', 'sage' ),
		'view_item'             => __( 'View Item', 'sage' ),
		'view_items'            => __( 'View Items', 'sage' ),
		'search_items'          => __( 'Search Item', 'sage' ),
		'not_found'             => __( 'Not found', 'sage' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
		'featured_image'        => __( 'Featured Image', 'sage' ),
		'set_featured_image'    => __( 'Set featured image', 'sage' ),
		'remove_featured_image' => __( 'Remove featured image', 'sage' ),
		'use_featured_image'    => __( 'Use as featured image', 'sage' ),
		'insert_into_item'      => __( 'Insert into item', 'sage' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'sage' ),
		'items_list'            => __( 'Items list', 'sage' ),
		'items_list_navigation' => __( 'Items list navigation', 'sage' ),
		'filter_items_list'     => __( 'Filter items list', 'sage' ),
	);
	$args = array(
		'label'                 => __( 'FAQ', 'sage' ),
		'description'           => __( 'Frequently Asked Questions', 'sage' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-chat',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'faqs', $args );

}
add_action( 'init', 'tdr_register_faqs_pt', 0 );


// Register Custom Taxonomy
function tdr_register_faq_type_tax() {

	$labels = array(
		'name'                       => _x( 'FAQ Types', 'Taxonomy General Name', 'sage' ),
		'singular_name'              => _x( 'FAQ Type', 'Taxonomy Singular Name', 'sage' ),
		'menu_name'                  => __( 'FAQ Types', 'sage' ),
		'all_items'                  => __( 'All Items', 'sage' ),
		'parent_item'                => __( 'Parent Item', 'sage' ),
		'parent_item_colon'          => __( 'Parent Item:', 'sage' ),
		'new_item_name'              => __( 'New Item Name', 'sage' ),
		'add_new_item'               => __( 'Add New Item', 'sage' ),
		'edit_item'                  => __( 'Edit Item', 'sage' ),
		'update_item'                => __( 'Update Item', 'sage' ),
		'view_item'                  => __( 'View Item', 'sage' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'sage' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'sage' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'sage' ),
		'popular_items'              => __( 'Popular Items', 'sage' ),
		'search_items'               => __( 'Search Items', 'sage' ),
		'not_found'                  => __( 'Not Found', 'sage' ),
		'no_terms'                   => __( 'No items', 'sage' ),
		'items_list'                 => __( 'Items list', 'sage' ),
		'items_list_navigation'      => __( 'Items list navigation', 'sage' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'faq-type', array( 'faqs' ), $args );

}
add_action( 'init', 'tdr_register_faq_type_tax', 0 );
