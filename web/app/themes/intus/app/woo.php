<?php
/**
 * Disable default Woo Styles
 *
 * @ref https://docs.woocommerce.com/document/disable-the-default-stylesheet/
 * @return null
 */

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/**
 * Woocommerce product archive listing actions
 *
 * @return null
 */

require_once(dirname(__FILE__) . '/woo/woo-archive.php');
require_once(dirname(__FILE__) . '/woo/woo-filter.php');

/**
 * Woocommerce product single actions
 *
 * @return null
 */

require_once(dirname(__FILE__) . '/woo/woo-product.php');

/**
 * Woocommerce order customisation
 *
 * @return null
 */

require_once(dirname(__FILE__) . '/woo/woo-orders.php');

/**
 * Woocommerce admin customisation
 *
 * @return null
 */

require_once(dirname(__FILE__) . '/woo/woo-admin.php');

/**
 * Woocommerce VAT exempt customisation
 *
 * @return null
 */

if (  function_exists( 'get_field' ) && ( get_field('enable_vat_exempt', 'option') ) ) {
    require_once(dirname(__FILE__) . '/woo/woo-vat.php');
}

require_once(dirname(__FILE__) . '/woo/woo-email.php');

//require_once(dirname(__FILE__) . '/woo/woo-checkout.php');
