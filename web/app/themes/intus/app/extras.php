<?php

namespace App;

use WP_Query;
use Walker_Nav_Menu;
use WP_Block_Type_Registry;

/**
 * Register ACF Options pages
 *
 * @link http://www.advancedcustomfields.com/resources/acf_add_options_page/
 */

if ( function_exists( 'acf_add_options_page' ) ) {

    acf_add_options_page();
    acf_add_options_sub_page( 'Global' );
    acf_add_options_sub_page( 'Header' );
    acf_add_options_sub_page( 'Footer' );
    acf_add_options_sub_page( 'WooCommerce' );
    acf_add_options_sub_page( 'VAT Exempt' );
    acf_add_options_sub_page( 'Blog' );
    acf_add_options_sub_page( 'Developer' );
}

if ( function_exists( 'acf_set_options_page_title' ) ) {
    acf_set_options_page_title( __( 'Site Options' ) );
}


/**
 * Meganav Walker
 *
 * @return html
 */
class Intus_Meganav_Walker extends Walker_Nav_Menu {

    private $sleepExists;
    private $sleepFields;

    public function __construct() {
        $this->sleepFields = [
            'image'     => get_field( 'sleep_test_image', 'option' ),
            'title'     => get_field( 'sleep_test_title', 'option' ),
            'link'      => get_field( 'sleep_test_link', 'option' ),
            'link_text' => get_field( 'sleep_test_link_text', 'option' )
        ];

        $this->sleepExists = ( !empty( $this->sleepFields['image'] ) && !empty( $this->sleepFields['link'] ) ) ? true : false;
    }

    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        $title     = $item->title;
        $permalink = $item->url;
        $output    .= "<li class='" . implode( " ", $item->classes ) . "'>";

        // If woocommerce category look for thumbnail image
        if ( $item->object == 'product_cat' ) {
            $image = get_field( 'icon', 'term_' . $item->object_id );

            if ( !empty( $image ) ) {
                $output .= '<img src="' . $image['sizes']['medium'] . '" alt="category icon" class="img-fluid">';
            }
        }

        //Add SPAN if no Permalink
        if ( $permalink && $permalink != '#' ) {
            $output .= '<a href="' . $permalink . '">';
        }
        else {
            $output .= '<span>';
        }

        $output .= ( $title == '#' ) ? '' : $title;

        if ( $permalink && $permalink != '#' ) {
            $output .= '</a>';
        }
        else {
            $output .= '</span>';
        }

        if ( in_array( 'menu-item-has-children', $item->classes ) && $depth == 0 ) {

            $navWidth = ( $this->sleepExists ) ? 'col-lg-8 col-xl-9 ' : 'col-12 ';

            // meganav output
            $output .= '<div class="meganav">';
            $output .= '<div class="container container--wide">';
            $output .= '<div class="row">';
            $output .= '<div class="' . $navWidth . 'meganav__inner">';
        }
    } // start_el

    public function end_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        $sleepOutput = '';

        if ( $this->sleepExists ) {
            $sleepOutput = '
                <div class="sleep-test-block">
                    <a
                        class="sleep-test-block__content"
                        href="' . $this->sleepFields['link'] . '"
                    >
                        <span class="text-bold color-secondary d-block h3 mb-4">' . $this->sleepFields['title'] . '</span>
                        <span class="text-bold color-secondary d-flex align-items-center">
                            <span>' . $this->sleepFields['link_text'] . '</span>
                            <i class="far fa-angle-right pl-3"></i>
                        </span>
                    </a>
                    <div
                        class="sleep-test-block__image"
                        style="background-image: url(' . $this->sleepFields['image']['sizes']['medium'] . ');"
                    >
                    </div>
                </div>
            ';
        }

        if ( in_array( 'menu-item-has-children', $item->classes ) && $depth == 0 ) {
            $output .= '</div>'; // col-8

            if ( $this->sleepExists ) {
                $output .= '<div class="col-lg-4 col-xl-3">';
                $output .= $sleepOutput;
                $output .= '</div>'; // col-4
            }

            $output .= '</div>'; // row
            $output .= '</div>'; // container
            $output .= '</div>'; // meganav
        }

        $output .= '</li>';
    } // end_el

} // Intus_Meganav_Walker

/**
 * AJAX function to search FAQs
 *
 * @return null
 */

add_action( 'wp_ajax_nopriv_intus_search_faqs', 'App\\intus_search_faqs' );
add_action( 'wp_ajax_intus_search_faqs', 'App\\intus_search_faqs' );

function intus_search_faqs() {

    $output = '';
    $query  = ( isset( $_POST['query'] ) ) ? $_POST['query'] : '';

    $custom_query = new WP_Query( [
        'post_type'      => 'faqs',
        'posts_per_page' => - 1,
        's'              => $query
    ] );

    if ( $custom_query->have_posts() ):
        $output .= '<h3 class="text-lighter mb-5">Search Results for <span class="text-bold">' . $query . '</span></h3>';
        while ( $custom_query->have_posts() ):
            $custom_query->the_post();
            global $post;
            $output .= '
                <div class="faq-search-result mb-4">
                    <p class="text-bold d-block pb-4">' . apply_filters( 'the_title', $post->post_title ) . '</p>
                    ' . apply_filters( 'the_content', $post->post_content ) . '
                </div>
            ';
        endwhile;
        wp_reset_postdata();
    endif;

    if ( !empty( $output ) ) {
        echo $output;
    }
    else {
        echo '<p class="faq-no-results">Sorry there were no results for your search term</p>';
    }

    die();

}


add_action( 'wp_ajax_nopriv_intus_mini_cart', 'App\\intus_mini_cart' );
add_action( 'wp_ajax_intus_mini_cart', 'App\\intus_mini_cart' );

function intus_mini_cart() {
    $output     = '';
    $cart_count = WC()->cart->get_cart_contents_count();
    $output     .= $cart_count;
    echo $output;
    die();
}

/**
 * Add mailchimp scripts to footer
 *
 * @return null
 */

add_action( 'mailchimp_validate', function () {
    echo '
        <script
          type="text/javascript"
          src="//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js"
        ></script>
        <script type="text/javascript">
            (function($) {
                window.fnames = new Array();
                window.ftypes = new Array();
                fnames[0] = "EMAIL";
                ftypes[0] = "email";
                fnames[1] = "FNAME";
                ftypes[1] = "text";
                fnames[2] = "LNAME";
                ftypes[2] = "text";
                fnames[3] = "MMERGE4";
                ftypes[3] = "text";
            }(jQuery));
            var $mcj = jQuery.noConflict(true);
        </script>
    ';
} );

/**
 * Attach callback to existing 'tiny_mce_before_init'
 */

add_filter( 'tiny_mce_before_init', function ( $init_array ) {

    $current_formats = json_decode( $init_array['style_formats'] );

    $current_formats[] = [
        'title'    => 'Paragraph Large',
        'selector' => 'p',
        'classes'  => 'p-lg',
        'wrapper'  => false,
    ];

    $current_formats[] = [
        'title'    => 'Paragraph Small',
        'selector' => 'p',
        'classes'  => 'p-sm',
        'wrapper'  => false,
    ];

    $current_formats[] = [
        'title'    => 'Paragraph X-Small',
        'selector' => 'p',
        'classes'  => 'p-xs',
        'wrapper'  => false,
    ];

    $init_array['style_formats'] = json_encode( $current_formats );

    return $init_array;

} );

/**
 * Tracking scripts
 */

add_action( 'wp_head', function () {

    if ( $head = get_field( 'inside_head', 'option' ) ) {
        echo $head . PHP_EOL;
    }

    if ( !current_user_can( 'edit_posts' ) && $tracking = get_field( 'tracking_head', 'option' ) ) {
        echo $tracking . PHP_EOL;
    }
} );

add_action( 'after_body', function () {
    if ( !current_user_can( 'edit_posts' ) && $tracking = get_field( 'tracking_after_body', 'option' ) ) {
        echo $tracking . PHP_EOL;
    }
} );

add_action( 'wp_footer', function () {
    if ( !current_user_can( 'edit_posts' ) && $tracking = get_field( 'tracking_footer', 'option' ) ) {
        echo $tracking . PHP_EOL;
    }
} );

/**
 * Style overrides
 */

add_action( 'wp_head', function () {

    if ( function_exists( 'get_field' ) ) {
        $change_colours = get_field( 'change_theme_colours', 'option' );

        if ( $change_colours ) {
            $theme_colours = get_field( 'theme_colours', 'option' );

            if ( !empty( $theme_colours['primary'] ) ) {
                echo \App\template( locate_template( 'views/partials/colours/colour-primary' ), [
                    'colour' => $theme_colours['primary']
                ] );
            }

            if ( !empty( $theme_colours['primary_light'] ) ) {
                echo \App\template( locate_template( 'views/partials/colours/colour-primary-light' ), [
                    'colour' => $theme_colours['primary_light']
                ] );
            }

            if ( !empty( $theme_colours['primary_dark'] ) ) {
                echo \App\template( locate_template( 'views/partials/colours/colour-primary-dark' ), [
                    'colour' => $theme_colours['primary_dark']
                ] );
            }

            if ( !empty( $theme_colours['secondary'] ) ) {
                echo \App\template( locate_template( 'views/partials/colours/colour-secondary' ), [
                    'colour' => $theme_colours['secondary']
                ] );
            }
        }
    }

}, 99 );
