<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {

    /** Add the name of the theme to the body tag */
    $theme = wp_get_theme();
    /*@var $theme WP_Theme */
    $classes[] = sanitize_title( $theme->Name );

    /** Add page slug if it doesn't exist */
    if ( ( is_single() || is_page() ) && !is_front_page() ) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

/**
 * Add custom block categories
 */
add_filter('block_categories_all', function ($categories, $post) {
    return array_merge(
        [
            [
                'slug' => 'flexible-content',
                'title' => __('Intus Blocks', 'intus')
            ]
        ],
        $categories
    );
}, 10, 2);


/**
 * Add a last updated section to post types
 * @param $content
 *
 * @return string
 */

add_filter( 'the_content', function ( $content ) {
    $custom_content = "";
    if( get_post_type() == 'post' ){
        $u_time = get_the_time('U');
        $u_modified_time = get_the_modified_time('U');
        if ($u_modified_time >= $u_time + 86400) {
            $updated_date = get_the_modified_time('F jS, Y');
            $updated_time = get_the_modified_time('h:i a');
            $custom_content .= '<p class="last-updated py-3"><em>Last updated on <strong>'. $updated_date . '</strong> at <strong>'. $updated_time .'</strong></em></p>';
        }
    }
    return $custom_content . $content;
}, 20, 1 );


/**
 *
 */
add_filter( 'woocommerce_continue_shopping_redirect', function( $return_to ) {
    return wc_get_page_permalink( 'shop' );
}, 10, 1);


/**
 * Remove the 'autocomplete' attribute from multiple fields during checkout...
 */
add_filter( 'woocommerce_checkout_fields' , function( $fields ){

    $sections = [
        'billing',
        'shipping'
    ];

    $fields_To_remove_autofill = array(
        'company',
        'address_1',
        'address_2',
        'city',
        'postcode',
        'country',
        'state',
    );

    foreach ( $sections as $section ) {
        foreach ( $fields_To_remove_autofill as $field ) {
            $field_name = "{$section}_{$field}";
            // Because Chrome is an absolute PITA we cannot just remove the autocomplete attribute, we have to mangle it with the one-time-code.
            $fields[$section][$field_name]['autocomplete'] ="off";
        }
    }

    return $fields;
} );


/**
 * Remove '/blog' from any category urls
 */
add_filter( 'category_link' , function( $link, $term_id ){
    if( !is_admin() ){
        $link = str_replace( "/blog", "", $link );
    }
    return $link;
},10 ,2);


/**
 *
 */
add_filter('woocommerce_csv_product_import_mapping_options', function( $options, $item ){
    $options['_product_affiliate_description'] = __('ACF - Product_Affiliate Description key');
    $options['product_affiliate_description'] = __('ACF - Product_Affiliate Description');
    return $options;
}, 10, 2);


/**
 * Only add Recaptcha javascript on pages where a Contact Form is present...
 */
//add_action('wp_print_scripts', function () {
//    global $post;
//    if ( is_a( $post, 'WP_Post' ) && !has_shortcode( $post->post_content, 'contact-form-7') ) {
//        wp_dequeue_script( 'google-recaptcha' );
//        wp_dequeue_script( 'wpcf7-recaptcha' );
//    }
//});

//add_action( 'wp_enqueue_scripts', function () {
//    // Dequeue cf7 and recaptcha scripts and styles, preventing them from loading everywhere
//    wp_dequeue_script( 'contact-form-7' );
//    wp_dequeue_script( 'google-recaptcha' );
//    wp_dequeue_style( 'contact-form-7' );
//    // If current post has cf7 shortcode, enqueue!
//    global $post;
//    if ( isset( $post->post_content ) AND has_shortcode( $post->post_content, 'contact-form-7' ) ) {
//        if ( function_exists( 'wpcf7_do_enqueue_scripts' ) ) {
//            wpcf7_do_enqueue_scripts();
//            wp_enqueue_script( 'google-recaptcha' );
//        }
//    }
//}, 9999, 0 );
