module.exports = (shipit) => {
    require('shipit-deploy')(shipit)
    require('shipit-shared')(shipit)
    require('shipit-assets')(shipit)

    shipit.initConfig({
        default: {
            user: 'www-data',
            restart_webserver: 'sudo /etc/init.d/apache2 graceful',
            restart_phpfpm: 'sudo /etc/init.d/php7.4-fpm reload',
            themePath: 'web/app/themes/intus',
            webrootPath: 'web',
            workspace: '/tmp/intus-workspace',
            repositoryUrl: 'git@bitbucket.org:intushealthcare/ecom.intushealthcare.com-woocommerce.git',
            keepReleases: 5,
            deleteOnRollback: false,
            shallowClone: true,
            updateSubmodules: true,
            assets: {
                paths: ['web/app/uploads']
            },
            shared: {
                overwrite: true,
                dirs: [
                    'logs',
                    'web/app/uploads'
                ],
                files: [
                    '.env',
                    'auth.json',
                    'web/.htaccess'
                ]
            },
        },
        live_linode: {
            servers: 'root@139-162-225-228.ip.linodeusercontent.com',
            deployTo: '/srv/users/live-intushealthcare-com/apps/live-intushealthcare-com/site',
            branch: 'master',
            user: 'live-intushealthcare-com',
            restart_webserver: 'service apache-sp restart',
            restart_phpfpm: 'service php7.4-fpm-sp restart',
        },
        staging_linode: {
            servers: 'root@139.162.255.228',
            deployTo: '/srv/users/dev-intushealthcare-com/apps/dev-intushealthcare-com/site',
            branch: 'develop',
            user: 'dev-intushealthcare-com',
            restart_webserver: 'service apache-sp restart',
            restart_phpfpm: 'service php7.4-fpm-sp restart',
        },
        staging_global_linode: {
            servers: 'root@139.162.255.228',
            deployTo: '/srv/users/world-dev-intushealthcare-com/apps/world-dev-intushealthcare-com/site',
            branch: 'develop',
            user: 'world-dev-intushealthcare-com',
            restart_webserver: 'service apache-sp restart',
            restart_phpfpm: 'service php7.4-fpm-sp restart',
        }
    })

    shipit.on('updated', a => {
        shipit.start([
            'root-composer-install',
            'theme-install',
            'translations-install',
            'translations-install-plugins',
        ])
    })

    shipit.on('published', a => {
        const tasks = [
            'remove-cache',
            'set-permissions'
        ]

        if (shipit.environment === 'live') {
            tasks.push('restart-apache')
            tasks.push('restart-php-fpm')
        }

        if (shipit.environment === 'staging_linode') {
            tasks.push('restart-apache')
            tasks.push('restart-php-fpm')
        }

        shipit.start(tasks)
    })

    // Theme 1 tasks
    shipit.blTask('theme-composer-install', () => shipit.remote('composer install --no-dev -o', {cwd: shipit.releasePath + '/' + shipit.config.themePath}))
    shipit.blTask('theme-yarn-install', () => shipit.remote('yarn', {cwd: shipit.releasePath + '/' + shipit.config.themePath}))
    shipit.blTask('theme-yarn-build', () => shipit.remote('yarn build:production', {cwd: shipit.releasePath + '/' + shipit.config.themePath}))

    // Composer install
    shipit.blTask('root-composer-install', () => shipit.remote('composer install --no-dev -o', {cwd: shipit.releasePath}))

    // Translation Installation
    let languages = 'es_ES it_IT en_CA fr_CA nl_BE fr_BE';
    shipit.blTask('translations-install', () => shipit.remote(`wp language core install ${languages} --allow-root`, {cwd: shipit.releasePath + '/' + shipit.config.webrootPath}))
    shipit.blTask('translations-install-plugins', () => shipit.remote(`wp language plugin install --all ${languages} --allow-root`, {cwd: shipit.releasePath + '/' + shipit.config.webrootPath}))

    // Theme Install
    shipit.blTask('theme-install', ['theme-composer-install', 'theme-yarn-install', 'theme-yarn-build'])

    // Clear cache and set permissions
    shipit.blTask('remove-cache', () => shipit.remote('rm -rf shared/web/app/uploads/cache/compiled', {cwd: shipit.config.deployTo}))
    shipit.blTask('set-permissions', () => shipit.remote(`chown -R ${shipit.config.user}:${shipit.config.user} .`, {cwd: shipit.releasePath}))

    // Gracefully restart Apache and PHP-FPM
    shipit.blTask('restart-apache', () => shipit.remote(`${shipit.config.restart_webserver}`))
    shipit.blTask('restart-php-fpm', () => shipit.remote(`${shipit.config.restart_phpfpm}`))
}
