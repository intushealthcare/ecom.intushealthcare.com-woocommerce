<?php
/**
 * Your base production configuration goes in this file. Environment-specific
 * overrides go in their respective config/environments/{{WP_ENV}}.php file.
 *
 * A good default policy is to deviate from the production config as little as
 * possible. Try to define as much of your configuration in this file as you
 * can.
 */

use Roots\WPConfig\Config;
use function Env\env;

/** @var string Directory containing all of the site's files */
$root_dir = dirname(__DIR__);

/** @var string Document Root */
$webroot_dir = $root_dir . '/web';

if (!defined('WP_ROOT')) {
    define('WP_ROOT', $root_dir);
}

/**
 * Use Dotenv to set required environment variables and load .env file in root
 */
$dotenv = Dotenv\Dotenv::createUnsafeImmutable($root_dir);
if (file_exists($root_dir . '/.env')) {
    $dotenv->load();
    $dotenv->required([
        'WP_HOME',
        'WP_SITEURL',
    ]);
    if (!env('DATABASE_URL')) {
        $dotenv->required([
            'DB_NAME',
            'DB_USER',
            'DB_PASSWORD',
        ]);
    }
}

if (!defined('WP_WEB_ROOT')) {
    define('WP_WEB_ROOT', $webroot_dir);
}

/** @var string Web Root Hostname */
$webroot_host = !empty($_SERVER['HTTP_HOST']) ? ( env('WP_SSL') == 'TRUE' ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] : '';

/**
 * Set up our global environment constant and load its config first
 * Default: production
 */
define('WP_ENV', env('WP_ENV') ?: 'production');

/**
 * URLs
 */
Config::define('WP_HOME', env('WP_HOME'));
Config::define('WP_SITEURL', env('WP_SITEURL'));

/**
 * Custom Content Directory
 */
Config::define('CONTENT_DIR', '/app');
Config::define('WP_CONTENT_DIR', $webroot_dir . Config::get('CONTENT_DIR'));

if ($webroot_host) {
    Config::define('WP_CONTENT_URL', $webroot_host . Config::get('CONTENT_DIR'));
}

/**
 * DB settings
 */
Config::define('DB_NAME', env('DB_NAME'));
Config::define('DB_USER', env('DB_USER'));
Config::define('DB_PASSWORD', env('DB_PASSWORD'));
Config::define('DB_HOST', env('DB_HOST') ?: 'localhost');
Config::define('DB_CHARSET', 'utf8mb4');
Config::define('DB_COLLATE', '');
$table_prefix = env('DB_PREFIX') ?: 'wp_';

if (env('DATABASE_URL')) {
    $dsn = (object)parse_url(env('DATABASE_URL'));

    Config::define('DB_NAME', substr($dsn->path, 1));
    Config::define('DB_USER', $dsn->user);
    Config::define('DB_PASSWORD', isset($dsn->pass) ? $dsn->pass : null);
    Config::define('DB_HOST', isset($dsn->port) ? "{$dsn->host}:{$dsn->port}" : $dsn->host);
}

/**
 * Authentication Unique Keys and Salts
 */
Config::define('AUTH_KEY', env('AUTH_KEY'));
Config::define('SECURE_AUTH_KEY', env('SECURE_AUTH_KEY'));
Config::define('LOGGED_IN_KEY', env('LOGGED_IN_KEY'));
Config::define('NONCE_KEY', env('NONCE_KEY'));
Config::define('AUTH_SALT', env('AUTH_SALT'));
Config::define('SECURE_AUTH_SALT', env('SECURE_AUTH_SALT'));
Config::define('LOGGED_IN_SALT', env('LOGGED_IN_SALT'));
Config::define('NONCE_SALT', env('NONCE_SALT'));

/**
 * Multisite
 */
Config::define('WP_ALLOW_MULTISITE', true);
Config::define('MULTISITE', true);
Config::define('SUBDOMAIN_INSTALL', true);
Config::define('DOMAIN_CURRENT_SITE', env('DOMAIN_CURRENT_SITE'));
Config::define('PATH_CURRENT_SITE', '/');
Config::define('SITE_ID_CURRENT_SITE', 1);
Config::define('BLOG_ID_CURRENT_SITE', 1);

if (!empty($_SERVER['HTTP_HOST'])) {
    Config::define('COOKIE_DOMAIN', $_SERVER['HTTP_HOST']);
}

/**
 * Custom Settings
 */
Config::define('AUTOMATIC_UPDATER_DISABLED', true);
Config::define('DISABLE_WP_CRON', env('DISABLE_WP_CRON') ?: false);
// Disable the plugin and theme file editor in the admin
if (!defined('DISALLOW_FILE_EDIT')) {
    Config::define('DISALLOW_FILE_EDIT', true);
}
// Disable plugin and theme updates and installation from the admin
if (!defined('DISALLOW_FILE_MODS')) {
    Config::define('DISALLOW_FILE_MODS', true);
}
Config::define('WP_POST_REVISIONS', 7);
Config::define('ALLOW_UNFILTERED_UPLOADS', env('ALLOW_UNFILTERED_UPLOADS') === true);
Config::define('INTUS_MAGENTO_API_WSDL', env('INTUS_MAGENTO_API_WSDL') ?: 'https://ecom.intushealthcare.com/api/v2_soap?wsdl=1');
Config::define('STOCK_SYNC_DEBUG', env('STOCK_SYNC_DEBUG'));

// WP Super Cache
if (!defined('WPCACHEHOME')) {
    Config::define('WPCACHEHOME', env('WPCACHEHOME') );
}
if (!defined('WP_CACHE')) {
    Config::define('WP_CACHE', env('WP_CACHE') );
}

/**
 * Debugging Settings
 */
Config::define('WP_DEBUG_DISPLAY', false);
Config::define('SCRIPT_DEBUG', false);
ini_set('display_errors', '0');
ini_set('error_log', $root_dir . '/logs/debug.log');

/**
 * Allow WordPress to detect HTTPS when used behind a reverse proxy or a load balancer
 * See https://codex.wordpress.org/Function_Reference/is_ssl#Notes
 */
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
    $_SERVER['HTTPS'] = 'on';
}

$env_config = __DIR__ . '/environments/' . WP_ENV . '.php';

if (file_exists($env_config)) {
    require_once $env_config;
}

Config::apply();

/**
 * Bootstrap WordPress
 */
if (!defined('ABSPATH')) {
    define('ABSPATH', $webroot_dir . '/wp/');
}

/**
 * Increase the memory limit for requests for sitemap urls. To avoid products sitemap generation failing
 */
if( $url = $_SERVER['REQUEST_URI'] ?? null ) {
    $extension = pathinfo($url)['extension'] ?? null;
    $is_probably_sitemap      = strpos( $url, 'sitemap');
    if( ($extension == 'xml') && ($is_probably_sitemap !== false ) ){
        ini_set('memory_limit', '2G');
    }
}

